/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 18 January 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.freedroidz.driving;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import org.evolvis.freedroidz.RobotControl;
import org.evolvis.freedroidz.command.Command;
import org.evolvis.freedroidz.command.CommandAdapter;
import org.evolvis.freedroidz.command.CommandListener;
import org.evolvis.freedroidz.command.providers.XMPPCommandProvider;

/**
 * @author fabian
 *
 */
public class AdvancedDrivingRobot {
	
	private static String CTRL = KeyEvent.getKeyModifiersText(InputEvent.CTRL_MASK);
	
	protected CommandListener commandListener;

    public static Command CMD_CLOSE = new CommandAdapter("close", KeyEvent.VK_Q, null) {
			public void execute(Object arg) {
				RobotControl.close();
			}
		};

    public static Command CMD_FAST = new CommandAdapter("fast", KeyEvent.VK_UP, CTRL) {
			public void execute(Object arg) {
				RobotControl.fast();
			}
		};

    public static Command CMD_FASTER = new CommandAdapter("faster", KeyEvent.VK_UP, null) {
			public void execute(Object arg) {
				RobotControl.faster();
			}
		};

    
    public static Command CMD_SLOWER = new CommandAdapter("slower", KeyEvent.VK_DOWN, null) {
			public void execute(Object arg) {
				RobotControl.slower();
			}
		};

    public static Command CMD_FAST_BACK = new CommandAdapter("fastBack", KeyEvent.VK_DOWN, CTRL) {
            public void execute(Object arg) {
                RobotControl.fastBack();
            }
        };
		
    public static Command CMD_LEFT = new CommandAdapter("left", KeyEvent.VK_LEFT, null) {
			public void execute(Object arg) {
				RobotControl.left();
			}
		};
    
    public static Command CMD_TURN_LEFT = new CommandAdapter("turnLeft", KeyEvent.VK_LEFT, CTRL) {
            public void execute(Object arg) {
                if (arg != null)
                    RobotControl.turnRight(Integer.parseInt(""+arg));
                else 
                    RobotControl.turnRight();
            }
        };

    public static Command CMD_RIGHT = new CommandAdapter("right", KeyEvent.VK_RIGHT, CTRL) {
			public void execute(Object arg) {
				RobotControl.right();
			}
		};

    public static Command CMD_TURN_RIGHT = new CommandAdapter("turnRight", KeyEvent.VK_RIGHT, CTRL) {
			public void execute(Object arg) {
                if (arg != null)
                    RobotControl.turnRight(Integer.parseInt(""+arg));
                else 
                    RobotControl.turnRight();
			}
		};

    public static Command CMD_FLT = new CommandAdapter("flt", KeyEvent.VK_S, null) {
			public void execute(Object arg) {
				RobotControl.flt();
			}
		};

    public static Command CMD_STOP = new CommandAdapter("stop", KeyEvent.VK_SPACE, null) {
			public void execute(Object arg) {
				RobotControl.stop();
			}
		};

    public static Command CMD_PAUSE = new CommandAdapter("pause", KeyEvent.VK_SPACE, CTRL) {
			public void execute(Object arg) {
				RobotControl.pause();
			}
		};

    public static Command CMD_SEARCH_BARRIER = new CommandAdapter("searchBarrier", KeyEvent.VK_SPACE, null) {
			public void execute(Object arg) {
				RobotControl.turnToBarrier();
			}
		};
    
    public static Command CMD_DRIVE_TO = new CommandAdapter("driveTo", -1, null) {
			public void execute(Object arg) {
				RobotControl.startProgramDriveTo(RobotControl.strToPos((String)arg));
			}
		};

    public static Command CMD_DRIVE_ARROUND = new CommandAdapter("driveArround", -1, null) {
			public void execute(Object arg) {
				RobotControl.startProgramDriveArround();
			}
		};

    public static Command CMD_POSITION_IS = new CommandAdapter("positionIs", -1, null) {
			public void execute(Object arg) {
				RobotControl.positionIs(RobotControl.strToPos((String)arg));
			}
		};
    
	public final static Command[] COMMANDS = new Command[] {
        CMD_SEARCH_BARRIER,
        CMD_TURN_LEFT,
        CMD_TURN_RIGHT,
        CMD_FAST,
        CMD_FAST_BACK,
        CMD_STOP,
        CMD_FASTER,
        CMD_SLOWER,
        CMD_LEFT,
        CMD_RIGHT,
        CMD_FLT,
        CMD_PAUSE,
        CMD_SEARCH_BARRIER,
        CMD_DRIVE_TO,
        CMD_DRIVE_ARROUND,
        CMD_POSITION_IS,
        CMD_CLOSE
	};
	
	public static void main(String [] args) throws Exception {
		new AdvancedDrivingRobot(args[0], args[1], args[2]);
	}
	
	public AdvancedDrivingRobot(String xmppHost, String xmppUserName, String xmppPassword) {
        try {
            new XMPPCommandProvider(getCommandListener(), COMMANDS, xmppHost, xmppUserName, xmppPassword);
		
            //DriveControlGTK gui = new DriveControlGTK(new String[0], getCommandListener(), COMMANDS);
            
            RobotControl.setVerify();
            System.out.println("started");

		} catch (Exception e) {
            System.out.println("error while startup");
            e.printStackTrace();
        }
	}
	
	protected CommandListener getCommandListener() {
		if(commandListener == null)
		{
			commandListener = new CommandListener() {

				public void processCommand(Command command, Object arg) {
					command.execute(arg);					
				}
			};
		}
		return commandListener;
	}
}
