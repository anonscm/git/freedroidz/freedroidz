/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 18 January 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.freedroidz.ui;

import java.util.logging.Logger;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.evolvis.freedroidz.NXJTest;

/**
 * Class to start the GUI of freedroidz.
 * 
 * @author Thomas Schmitz, tarent GmbH
 */
public class Starter {
	
	protected final static Logger logger = Logger.getLogger(Starter.class.getName());

	public static void main(String[] args) {
		Display.setAppName("freedroidz");
		Shell shell = new Shell();
		
		Image windowIcon = new Image(shell.getDisplay(), "/usr/share/icons/hicolor/26x26/apps/freedroidz.png");
		
		if(windowIcon != null)
			shell.setImage(windowIcon);
		else
			logger.warning("could not find window icon");
		
		shell.setLayout(new FillLayout());
		StrategyPanel gui = new StrategyPanel(shell);
		shell.open();
		
		try {
			NXJTest.main(new String[0]);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		while (!gui.isDisposed()) {
			if (!gui.getDisplay().readAndDispatch()) {
				gui.getDisplay().sleep();
			}
		}
	}
}
