/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 18 January 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.freedroidz.command.providers;

import org.evolvis.freedroidz.command.Command;
import org.evolvis.freedroidz.command.CommandListener;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;

/**
 * @author fabian
 *
 */
public class XMPPCommandProvider extends AbstractCommandProvider {
	
	public final static int XMPP_DEFAULT_PORT = 5222;
	
	protected XMPPConnection connection;
	
	public XMPPCommandProvider(CommandListener listener, Command[] commandSet, String host, String userName, String password)
        throws XMPPException {

		this(listener, commandSet, host, userName, password, XMPP_DEFAULT_PORT);
	}
	
	public XMPPCommandProvider(CommandListener listener, Command[] commandSet, String host, String userName, String password, int port) 
        throws XMPPException {
		
		addCommandListener(listener);
		setAvailableCommands(commandSet);

		ConnectionConfiguration config = new ConnectionConfiguration(host, port);
		
		config.setTLSEnabled(false);

        connection = connectToXMPPServer(config, userName, password);

		//    	 Create a packet filter to listen for new messages from a particular
		//    	 user. We use an AndFilter to combine two other filters.
		PacketFilter filter = new PacketTypeFilter(Message.class);
        
		//    	 Next, create a packet listener. We use an anonymous inner class for brevity.
		PacketListener myListener = new PacketListener() {
			public void processPacket(Packet packet) {

				Message message = (Message)packet;
				String textCommand = message.getBody();
				
				if(textCommand == null)
					return;
				
				System.out.println("received xmpp-command:" + textCommand);
                
                String[] params = textCommand.split(": ");
                System.out.println("textCommand: "+textCommand +" - "+ params.length);                    
                textCommand = params[0];				
                String arg = params.length > 1 ? params[1] : null;
                System.out.println("cmd: "+textCommand+" arg: "+arg);                
				for(int i=0; i < getCommandSet().length; i++) {
                    if(getCommandSet()[i].accept(textCommand))
						fireCommand(getCommandSet()[i], arg);
				}
			}
		};
		// Register the listener.
		connection.addPacketListener(myListener, filter);
	}
	
	protected XMPPConnection connectToXMPPServer(ConnectionConfiguration config, String userName, String password) throws XMPPException {
		XMPPConnection connection = new XMPPConnection(config);
		connection.login(userName, password);
		
		//Chat chat = connection.createChat("fkoest@fabian-thinkpad");
		//chat.sendMessage("No.5 is awaiting your commands!");

		Chat chat2 = connection.createChat("sebmob@sebmob");
		chat2.sendMessage("No.5 is awaiting your commands!");
		
		return connection;
	}

}
