/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 18 January 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.freedroidz.command;

/**
 * @author fabian
 *
 */
public abstract class CommandAdapter implements Command {

    String cmdName;
    int keyCode;
    String keyModifier;

    public CommandAdapter(String cmdName, int keyCode, String keyModifier) {
        this.cmdName = cmdName;
        this.keyCode = keyCode;
        this.keyModifier = keyModifier;
    }

    public boolean accept(int eventKeyCode, String eventKeyModifier) {
        return this.keyCode == eventKeyCode 
            && ((keyModifier == null && (eventKeyModifier == null || "".equals(eventKeyModifier))) 
                || (keyModifier != null && keyModifier.equals(eventKeyModifier)));
    }

    public boolean accept(String eventCmdName) {
        return cmdName.startsWith(eventCmdName);
    }

    public String getName() {
        return cmdName;
    }

}
