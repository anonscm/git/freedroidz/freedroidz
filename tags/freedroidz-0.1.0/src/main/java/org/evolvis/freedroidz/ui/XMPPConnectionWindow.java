/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 18 January 2008
 * Elmar Geese, CEO tarent GmbH.
 */

//package org.evolvis.freedroidz.ui;
//
//import org.evolvis.freedroidz.command.providers.XMPPCommandProvider;
//import org.gnome.gtk.*;
//import org.maemo.hildon.Window;
//import java.util.prefs.Preferences;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.IOException;
//import java.util.*;
//
//
//
//public class XMPPConnectionWindow {
//
//    Window w;
//    Entry host;
//    Entry port;
//    Entry username;
//    Entry password;
//    
//    public XMPPConnectionWindow(final DriveControlGTK parent) {
//        w = new Window();
//		w.setTitle("XMPP Connection");
//
//		VBox labels = new VBox(true, 0);
//		VBox entries = new VBox(true, 0);
//
//        
//		labels.packStart(new Label("Host"), true, true, 10);
//        host = new Entry();
//		entries.packStart(host, true, true, 10);
//        
//		labels.packStart(new Label("Port"), true, true, 10);
//        port = new Entry();
//		entries.packStart(port, true, true, 10);
//
//		labels.packStart(new Label("User"), true, true, 10);
//        username = new Entry();
//		entries.packStart(username, true, true, 10);
//
//		labels.packStart(new Label("Password"), true, true, 10);
//        password = new Entry();
//		entries.packStart(password, true, true, 10);
//        
//		VBox buttonBar = new VBox(false, 0);
//        buttonBar.packStart(new Label(""), true, true, 10);
//        Button connectButton = new Button("connect");
//        connectButton.connect(new Button.CLICKED() {
//                public void onClicked(Button source) {
//                    w.hide();
//                    new Thread() {
//                        public void run() {
//                            parent.connectToXMPPServer(host.getText(), Integer.parseInt(port.getText()), username.getText(), password.getText());
//                        }
//                    }.start();
//                }
//            });
//        buttonBar.packStart(connectButton, true, true, 10);
//
//		HBox formBar = new HBox(false, 10);        
//        formBar.packStart(labels, false, false, 0);
//        formBar.packStart(entries, true, true, 0);
//        
//		VBox totalList = new VBox(false, 0);
//        totalList.packStart(formBar, true, true, 0);
//        totalList.packStart(new HSeparator(), false, false, 0);
//        totalList.packStart(buttonBar, false, false, 0);
//        
//        w.addWithScrollbar(totalList);
//        System.out.println("init now");
//        
//        initFields();
//        System.out.println("init done");
//
//    }
//
//    void initFields() {
//        System.out.println("init initFields");
//
////         host.setText("10.0.0.1");
////         System.out.println("host set");
////         port.setText(""+XMPPCommandProvider.XMPP_DEFAULT_PORT);
////         username.setText("sebrob");
////         System.out.println("username set");
////         password.setText("sebrob1107");
////         System.out.println("pwd set");
//
//    	Properties props = new Properties();
//        try {
//            props.load(new FileInputStream(new File("config.properties")));
//
//
//            host.setText(props.getProperty("jabber.host"));
//            port.setText(props.getProperty("jabber.port"));
//            username.setText(props.getProperty("jabber.username"));
//            password.setText(props.getProperty("jabber.password"));       
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//        }
//        
//    }
//
//    
//    public void show() {
//        w.showAll();
//    }
//}