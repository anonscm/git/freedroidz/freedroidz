/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 18 January 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.freedroidz.ui;

import org.evolvis.freedroidz.RobotControl;
import org.evolvis.freedroidz.command.Command;
import org.evolvis.freedroidz.command.CommandListener;
import org.evolvis.freedroidz.command.providers.AbstractCommandProvider;
import org.evolvis.freedroidz.command.providers.XMPPCommandProvider;
import org.evolvis.freedroidz.driving.AdvancedDrivingRobot;
import org.gnome.gdk.EventKey;
import org.gnome.gtk.Button;
import org.gnome.gtk.ComboBox;
import org.gnome.gtk.Entry;
import org.gnome.gtk.Gtk;
import org.gnome.gtk.HBox;
import org.gnome.gtk.Label;
import org.gnome.gtk.Menu;
import org.gnome.gtk.MenuItem;
import org.gnome.gtk.Notebook;
import org.gnome.gtk.ScrolledWindow;
import org.gnome.gtk.VBox;
import org.gnome.gtk.Widget;
import org.gnome.gtk.Window;
import org.jivesoftware.smack.XMPPException;

public class DriveControlGTK extends AbstractCommandProvider {


    Window window;

    Entry statusEntry;

    CommandListener bluezListener;
    XMPPCommandProvider xmppCommandProvider;
    boolean connected = false;

    public static void main(String[] args) {        
        // direct excecution command listener
        CommandListener commandListener = new CommandListener() {
				public void processCommand(Command command, java.lang.Object arg) {
                    System.out.println("execute: "+command);                    
                    command.execute(arg);					
				}
			};
        
        DriveControlGTK drivingControl = new DriveControlGTK(args, commandListener, AdvancedDrivingRobot.COMMANDS);
        if (args.length > 0 && "start".equals(args[0])) {
            drivingControl.connectRobot();
            drivingControl.connectToXMPPServer("10.0.0.1", 5222, "sebrob", "sebrob1107");
        }
        drivingControl.start();                
    }

	public DriveControlGTK(String[] args, CommandListener listener, Command[] commandSet) {
        super();
        bluezListener = listener;

        setAvailableCommands(commandSet);

		Gtk.init(args);        
		window = createWindow();
//        window.setMenu(createMenu());
		VBox v = new VBox(false, 0);
        Notebook notebook = new Notebook();
        notebook.appendPage(createDriveControl(commandSet), new Label("Control"));
        notebook.appendPage(createInfos(), new Label("Info"));
		v.packStart(notebook, true, true, 10);
		v.packStart(createStatusBar(), false, false, 0);

//		window.addWithScrollbar(v);
    }

    public void start() {
		window.showAll();
		System.out.println("started");        
		Gtk.main();
    }

    Widget createDriveControl(Command[] commandSet) {
		VBox v = new VBox(false, 15);

        HBox h = new HBox(false, 5);
        
        
        final ComboBox comboBox = new ComboBox();
        comboBox.appendText("driveArround");
        comboBox.appendText("----------");
        for (int i = 0; i < commandSet.length; i++) {
            comboBox.appendText(commandSet[i].getName());
        }
        
        comboBox.setActive(0);
        //         comboBox.connect(new ComboBox.CHANGED() {
        //                 public void onChanged(ComboBox source) {
        //                 }
        //             });
        
        final Entry args = new Entry();

        Button doIt = new Button("go");
        doIt.connect(new Button.CLICKED() {
                public void onClicked(Button source) {
                    fireCmdByName(comboBox.getActiveText(), args.getText());
                    setStatus("cmd "+comboBox.getActiveText() +" sent.");
                }
            });
        
        h.packStart(comboBox, true, true, 5);
        h.packStart(args, true, true, 5);
        h.packStart(doIt, false, false, 5);
        

        Button stop = new Button("Stop");
        stop.connect(new Button.CLICKED() {
                public void onClicked(Button source) {
                    fireCmdByName(AdvancedDrivingRobot.CMD_STOP.getName(), null);
                }
            });

        HBox hButton = new HBox(false, 5);
		hButton.packStart(stop, true, true, 5);
        
		v.packStart(h, false, false, 5);
		v.packStart(hButton, false, false, 20);

        Label text = new Label("<markup><b>Key assignment:</b> \n\t<i>One</i> click to toggle button\n\t drives fast. \n\tMiddle button stops. \n\tZoom button drives slow.</markup>");
        text.setUseMarkup(true);
		v.packStart(text, true, true, 0);

        return v;
        //return new Label("TODO: Drive Control");
    }


    Widget createInfos() {
		VBox v = new VBox(false, 10);

        Label bluez = new Label("<markup><span foreground=\"blue\" size=\"x-large\">Bluez status:</span> <span foreground=\"black\" size=\"x-large\">OK</span></markup>");
        bluez.setUseMarkup(true);
		v.packStart(makeInfoLine(bluez), true, true, 5);

        Label xmpp = new Label("<markup><span foreground=\"blue\" size=\"x-large\">Xmpp connection:</span> <span foreground=\"red\" size=\"x-large\">NOT Connected</span></markup>");
        xmpp.setUseMarkup(true);
		v.packStart(makeInfoLine(xmpp), true, true, 5);


        Label direction = new Label("<markup><span foreground=\"blue\" size=\"x-large\">Direction:</span> <span foreground=\"black\" size=\"x-large\">0,0</span></markup>");
        direction.setUseMarkup(true);
		v.packStart(makeInfoLine(direction), true, true, 5);

        Label position = new Label("<markup><span foreground=\"blue\" size=\"x-large\">Position:</span> <span foreground=\"black\" size=\"x-large\">0</span></markup>");
        position.setUseMarkup(true);
		v.packStart(makeInfoLine(position), true, true, 5);

        Label battery = new Label("<markup><span foreground=\"blue\" size=\"x-large\">Battery status:</span> <span foreground=\"red\" size=\"x-large\">0</span></markup>");
        battery.setUseMarkup(true);
		v.packStart(makeInfoLine(battery), true, true, 5);


        Label objects = new Label("<markup><span foreground=\"blue\" size=\"x-large\">Objects found:</span> <span foreground=\"black\" size=\"x-large\">0</span></markup>");
        objects.setUseMarkup(true);
		v.packStart(makeInfoLine(objects), true, true, 5);

        Label distance = new Label("<markup><span foreground=\"blue\" size=\"x-large\">Radio distance:</span> <span foreground=\"black\" size=\"x-large\">0</span></markup>");
        distance.setUseMarkup(true);
		v.packStart(makeInfoLine(distance), true, true, 5);

        Label noise = new Label("<markup><span foreground=\"blue\" size=\"x-large\">Noise:</span> <span foreground=\"black\" size=\"x-large\">0</span></markup>");
        noise.setUseMarkup(true);
		v.packStart(makeInfoLine(noise), true, true, 5);
        

        ScrolledWindow scrolledWindow = new ScrolledWindow();
        scrolledWindow.addWithViewport(v);

        return scrolledWindow;
    }

    Widget makeInfoLine(Label label) {
		HBox h = new HBox(false, 0);
        h.packStart(label, false, false, 5);
        h.packStart(new Label(""), true, true, 5);
        return h;
    }
    
    Menu createMenu() {
        Menu menu = new Menu();
        MenuItem connectJabber = new MenuItem("Connect to XMPP");
        connectJabber.connect(new MenuItem.ACTIVATE() {
                public void onActivate(MenuItem source) {
                    connectJabber();
                }
            });

        MenuItem disconnectJabber = new MenuItem("Disconnect from XMPP");
        disconnectJabber.connect(new MenuItem.ACTIVATE() {
                public void onActivate(MenuItem source) {
                    disconnectJabber();
                }
            });


        MenuItem connectRobot = new MenuItem("Connect to Robot");
        connectRobot.connect(new MenuItem.ACTIVATE() {
                public void onActivate(MenuItem source) {
                    connectRobot();
                }
            });

        MenuItem disconnectRobot = new MenuItem("Disconnect from Robot");
        disconnectRobot.connect(new MenuItem.ACTIVATE() {
                public void onActivate(MenuItem source) {
                    disconnectRobot();
                }
            });

        MenuItem exit = new MenuItem("Exit");
        exit.connect(new MenuItem.ACTIVATE() {
                public void onActivate(MenuItem source) {
                    System.out.println("exit clicked");
                    exit();
                }
           });
        menu.append(connectRobot);
        menu.append(disconnectRobot);
        menu.append(connectJabber);
        menu.append(disconnectJabber);
        menu.append(exit);        

        return menu;
    }

    public void exit() {
        Gtk.mainQuit();
        System.exit(0);
    }
    
    public void disconnectJabber() {
        setStatus("not implemented yet!");            
    }

    public void connectJabber() {
        if (bluezListener == null) {
            setStatus("please connect to robot first");
            return;
        }
        if (xmppCommandProvider != null) {
            setStatus("already connected");
            return;
        }
//        new XMPPConnectionWindow(this).show();
    }

    public void connectToXMPPServer(final String xmppHost, final int port, final String xmppUserName, final String xmppPassword) {
        setStatus("connecting ...");
//         new Thread(new Runnable() {
//                 public void run() {
                     try {
                        xmppCommandProvider = new XMPPCommandProvider(bluezListener, getCommandSet(), xmppHost, xmppUserName, xmppPassword, port);
                        setStatus("connected to XMPP Server");
                    } catch(XMPPException excp) {
                        setStatus("XMPP connection failed: "+excp);
                        excp.printStackTrace();
                        return;
                    }                        
//                 }
//             }).start();
	}

    
    public void disconnectRobot() {
        setStatus("close connection ...");
        RobotControl.close();
        connected = false;
        setStatus("close closed");
    }

    public void connectRobot() {
        if (connected) {
            setStatus("already connected");
            return;
        }

        new Thread() {
            public void run() {
                try {
                    setStatus("connecting ...");
                    RobotControl.init();
                    RobotControl.setVerify();
                    addCommandListener(bluezListener);
                    setStatus("connected");
                    connected = true;
                } catch (Exception e) {
                    connected = false;
                    setStatus("error connecting to robot: "+ e);
                    e.printStackTrace();
                }
            }
        }.start();            
    }

    Widget createStatusBar() {
        statusEntry = new Entry();
        statusEntry.setEditable(false);
        return statusEntry;
    }

    
    Window createWindow() {

        Window w = new Window();
		w.setTitle("DriveControlGTK");
//        JalimoGUIApplication app = new JalimoGUIApplication();
//        app.setTitle("JaLiRob");
		
		w.connect(new Window.KEY_PRESS_EVENT() {


                public boolean onKeyPressEvent(Widget source, EventKey event) {
//                    System.out.println("key pressed: "+event.getKeyVal()+", "+event.getString());
                    
                    String commandName = "";
                    
//                    if (event.getKeyVal() == 65361) // nokia left button
//                        commandName = AdvancedDrivingRobot.CMD_LEFT.getName();
//                    else if (event.getKeyVal() == 65363) // nokia right button
//                        commandName = AdvancedDrivingRobot.CMD_RIGHT.getName();
//                    else if (event.getKeyVal() == 65362) // nokia up button
//                        commandName = AdvancedDrivingRobot.CMD_FAST.getName();
//                    else if (event.getKeyVal() == 65364) // nokia down button
//                        commandName = AdvancedDrivingRobot.CMD_FAST_BACK.getName();
//                    else if (event.getKeyVal() == 65293) //nokia middle button
//                        commandName = AdvancedDrivingRobot.CMD_STOP.getName();
//                    else if (event.getKeyVal() == 65476) //nokia zoom in button
//                        commandName = AdvancedDrivingRobot.CMD_FASTER.getName();
//                    else if (event.getKeyVal() == 65475) //nokia middle zoom button
//                        commandName = AdvancedDrivingRobot.CMD_TURN_RIGHT.getName();
//                    else if (event.getKeyVal() == 65477) //nokia zoom out button
//                        commandName = AdvancedDrivingRobot.CMD_SLOWER.getName();
//                    else
//                        System.out.println("no command for keyCode: "+event.getKeyVal());

                    System.out.println("command is: "+commandName);
                    fireCmdByName(commandName, null);
                    
                    return true;
                }
            });

//        w.connect(new Window.DELETE() {
//                public boolean onDeleteEvent(Widget source, EventAny event) {
//                    exit();
//                    return false;
//                }
//            });
        return w;
	}

    private void fireCmdByName(final String commandName, final String arg) {
        for(int i=0; i < getCommandSet().length; i++) {
            if(getCommandSet()[i].accept(commandName)) {
                final Command cmd = getCommandSet()[i];
                new Thread() {
                    public void run() {
                        fireCommand(cmd, arg);
                    }
                }.start();

                if (arg == null)
                    setStatus("cmd "+commandName +" sent.");
                else
                    setStatus("cmd "+commandName +": "+arg+" sent.");                
                return;
            }            
        }
    }
    


    public void setStatus(String message) {
        statusEntry.setText(message);
    }
}