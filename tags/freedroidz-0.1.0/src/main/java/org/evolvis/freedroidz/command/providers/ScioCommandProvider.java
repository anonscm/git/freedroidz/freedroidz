/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 18 January 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.freedroidz.command.providers;

import org.evolvis.freedroidz.command.Command;
import org.evolvis.freedroidz.command.CommandListener;
import org.jivesoftware.smack.XMPPException;



/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class ScioCommandProvider extends AbstractCommandProvider {

public final static int XMPP_DEFAULT_PORT = 5222;
	
//	protected XMPPConnection connection;
	
	public ScioCommandProvider(CommandListener listener, Command[] commandSet, String host, String userName, String password) throws XMPPException {
		
		this(listener, commandSet, host, userName, password, XMPP_DEFAULT_PORT);
	}
	
	public ScioCommandProvider(CommandListener listener, Command[] commandSet, String host, String userName, String password, int port) throws XMPPException {
		addCommandListener(listener);
		setAvailableCommands(commandSet);
		
//		connection = connectToXMPPServer(host, port, userName, password);
	}
//	
//	protected XMPPConnection connectToXMPPServer(String host, int port, String userName, String password) throws XMPPException {
//		XMPPConnection connection = new XMPPConnection(host, host, port);
//		
//		connection.setUsername(userName);
//		connection.setPassword(password);
//		
//		try {
//			connection.connect();
//		} catch (UnknownHostException e) {
//			throw new XMPPException("Could not connect to XMPP-Server", e);
//		} catch (IOException e) {
//			throw new XMPPException("Could not connect to XMPP-Server", e);
//		}
//		
//		return connection;
//	}
}
