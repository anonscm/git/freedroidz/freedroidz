/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 18 January 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.freedroidz.command.providers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.evolvis.freedroidz.command.Command;
import org.evolvis.freedroidz.command.CommandListener;

/**
 * @author fabian
 *
 */
public abstract class AbstractCommandProvider implements CommandProvider {

	protected List<CommandListener> listeners;
	protected Command[] commandSet;
	
	protected List<CommandListener> getListeners() {
		if(listeners == null)
			listeners = new ArrayList<CommandListener>();
		return listeners;
	}
	
	/* (non-Javadoc)
	 * @see org.jalimo.robots.command.CommandProvider#addCommandListener(org.jalimo.robots.command.CommandListener)
	 */
	public void addCommandListener(CommandListener listener) {
		getListeners().add(listener);
	}

	/* (non-Javadoc)
	 * @see org.jalimo.robots.command.CommandProvider#removeCommandListener(org.jalimo.robots.command.CommandListener)
	 */
	public void removeCommandListener(CommandListener listener) {
		getListeners().remove(listener);
	}
	
	public void fireCommand(Command command, Object arg) {
		Iterator<CommandListener> it = getListeners().iterator();
		
		while(it.hasNext())
			it.next().processCommand(command, arg);
	}
	
	
	public void setAvailableCommands(Command[] commands) {
		commandSet = commands;
	}
	
	protected Command[] getCommandSet() {
		if(commandSet == null)
			commandSet = new Command[0];
		return commandSet;
	}
}
