/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 18 January 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.freedroidz.driving;

import icommand.nxt.Motor;
import icommand.nxt.SensorPort;
import icommand.nxt.UltrasonicSensor;
import icommand.nxt.comm.NXTCommand;

import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

public class Drive {
	
	public static void main(String [] args) throws Exception {
		new Drive();

    }

    public static int MAX_SPEED = 100;
    public static int MIN_SPEED = -100;
    int speedLeft = MAX_SPEED;
    int speedRight = MAX_SPEED;
    int stepWidth = 15;

    Motor motorLeft = Motor.B;
    Motor motorRight = Motor.C;

    String CTRL = KeyEvent.getKeyModifiersText(InputEvent.CTRL_MASK);

    Cmd[] cmds = new Cmd[] {
        new CmdAdapter("close", KeyEvent.VK_Q, null) {
            public void execute(Object arg) {
                close();
            }
        },
        new CmdAdapter("fast", KeyEvent.VK_UP, CTRL) {
            public void execute(Object arg) {
                fast();
            }
        },
        new CmdAdapter("faster", KeyEvent.VK_UP, null) {
            public void execute(Object arg) {
                faster();
            }
        },
        new CmdAdapter("slower", KeyEvent.VK_DOWN, null) {
            public void execute(Object arg) {
                slower();
            }
        },
        new CmdAdapter("fastBack", KeyEvent.VK_DOWN, CTRL) {
            public void execute(Object arg) {
                fastBack();
            }
        },
        new CmdAdapter("left", KeyEvent.VK_LEFT, null) {
            public void execute(Object arg) {
                left();
            }
        },
        new CmdAdapter("turnLeft", KeyEvent.VK_LEFT, CTRL) {
            public void execute(Object arg) {
                turnLeft();
            }
        },
        new CmdAdapter("right", KeyEvent.VK_RIGHT, null) {
            public void execute(Object arg) {
                right();
            }
        },
        new CmdAdapter("right", KeyEvent.VK_RIGHT, CTRL) {
            public void execute(Object arg) {
                turnRight();
            }
        },
        new CmdAdapter("flt", KeyEvent.VK_S, null) {
            public void execute(Object arg) {
                flt();
            }
        },
        new CmdAdapter("stop", KeyEvent.VK_SPACE, null) {
            public void execute(Object arg) {
                stop();
            }
        },
        new CmdAdapter("pause", KeyEvent.VK_SPACE, CTRL) {
            public void execute(Object arg) {
                pause();
            }
        },

    };
        

    public Drive() {
        JFrame frame = new JFrame("Drive");
        frame.setSize(100,100);
        frame.addWindowListener(new WindowAdapter() {
                public void windowClosed(WindowEvent e) {
                    close();
                }
            });
        
        frame.setVisible(true);
        frame.addKeyListener(new KeyAdapter() {

                public void keyTyped(KeyEvent e) {
                }

               	public void keyPressed(KeyEvent e) {
                    System.out.print("event-"+KeyEvent.getKeyModifiersText(e.getModifiers())+"-"+KeyEvent.getKeyText(e.getKeyCode()));
                    for (int i = 0; i < cmds.length; i++) {
                        if (cmds[i].accept(e.getKeyCode(), KeyEvent.getKeyModifiersText(e.getModifiers()))) {
                            System.out.println(" > "+cmds[i].getName());
                            try {
                                cmds[i].execute(null);                                
                            } catch (Exception ex) {
                                System.out.println("error on command: "+ex);            
                            }      
                            System.out.println("-> "+speedLeft +"  "+speedRight);                            
                            return;
                        }
                    }
                    System.out.println(" ==> unknown command");                                        
                }
                
                public void keyReleased(KeyEvent e) {
                }
            });
        
        NXTCommand.setVerify(true);
		//Motor.B.setRegulationMode(icommand.nxtcomm.NXTProtocol.REGULATION_MODE_MOTOR_SYNC);
		//Motor.C.setRegulationMode(icommand.nxtcomm.NXTProtocol.REGULATION_MODE_MOTOR_SYNC);
        System.out.println("started");        

        new Thread() {
            UltrasonicSensor us = new UltrasonicSensor(SensorPort.S2);
            public void run() {
                us.setMetric(true);
                while (true) {
                    int dist = us.getDistance();
                    System.out.println("dist: "+dist);                    
                    if (dist < 30) {
                        stop();
                        System.out.println("stop now");
                    }
                    try {
                        Thread.sleep(50);
                    } catch (Exception e) {}
                }
            }
        }.start();
	}	
    
    public interface Cmd {
        public boolean accept(int keyCode, String keyModifier);
        public boolean accept(String cmdName);
        public void execute(Object arg);
        public String getName();
    }

    public abstract class CmdAdapter implements Cmd {
        String cmdName;
        int keyCode;
        String keyModifier;

        public CmdAdapter(String cmdName, int keyCode, String keyModifier) {
            this.cmdName = cmdName;
            this.keyCode = keyCode;
            this.keyModifier = keyModifier;
        }

        public boolean accept(int eventKeyCode, String eventKeyModifier) {
            return this.keyCode == eventKeyCode 
                && ((keyModifier == null && (eventKeyModifier == null || "".equals(eventKeyModifier))) 
                    || (keyModifier != null && keyModifier.equals(eventKeyModifier)));
        }

        public boolean accept(String eventCmdName) {
            return cmdName.equals(eventCmdName);
        }

        public String getName() {
            return cmdName;
        }
    }        
    
    

    public void fast() {
        speedRight = MAX_SPEED;
        speedLeft = MAX_SPEED;
        resend();
    }


    public void fastBack() {
        speedRight = MIN_SPEED;
        speedLeft = MIN_SPEED;
        resend();
    }

    public void faster() {
        speedRight += stepWidth;
        speedLeft += stepWidth;
        ensureValidSpeed();
        resend();
    }


    public void slower() {
        speedRight -= stepWidth;
        speedLeft -= stepWidth;
        ensureValidSpeed();
        resend();
    }


    public void left() {
        speedRight += 5;
        speedLeft -= 5;
        ensureValidSpeed();
        resend();
    }

    public void turnLeft() {
        speedRight = MAX_SPEED;
        speedLeft = MIN_SPEED;
        resend();
    }


    public void turnRight() {
        speedRight = MIN_SPEED;
        speedLeft = MAX_SPEED;
        resend();
    }

    public void right() {
        speedRight -= 5;
        speedLeft += 5;
        ensureValidSpeed();
        resend();
    }


    public void flt() {
        motorRight.flt();
        motorLeft.flt();
        speedLeft = speedRight = 0;
    }


    public void stop() {
        motorRight.stop();
        motorLeft.stop();
        speedLeft = speedRight = 0;
    }


    public void pause() {
        motorRight.stop();
        motorLeft.stop();
    }


    protected void ensureValidSpeed() {
        if (speedRight > MAX_SPEED)
            speedRight = MAX_SPEED;        
        if (speedRight < MIN_SPEED)
            speedRight = MIN_SPEED;

        if (speedLeft > MAX_SPEED)
            speedLeft = MAX_SPEED;        
        if (speedLeft < MIN_SPEED)
            speedLeft = MIN_SPEED;        
    }

    protected void resend() {
        motorLeft.setSpeed(Math.abs(speedLeft));
        motorRight.setSpeed(Math.abs(speedRight));

        if (speedLeft > 0)
            motorLeft.forward();
        else
            motorLeft.backward();

        if (speedRight > 0)
            motorRight.forward();
        else
            motorRight.backward();

        Motor.A.setSpeed(100);
        Motor.A.forward();
    }   
    
    public void close() {
        try {
            NXTCommand.close();
        } catch (Exception e) {
            System.out.println("error on closing: "+e);            
        }
        System.out.println("exit program now");        
        System.exit(0);
    }
}