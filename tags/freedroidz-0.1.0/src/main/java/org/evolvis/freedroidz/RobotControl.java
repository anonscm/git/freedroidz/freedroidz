/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 18 January 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.freedroidz;

import icommand.nxt.Motor;
import icommand.nxt.SensorPort;
import icommand.nxt.UltrasonicSensor;
import icommand.nxt.comm.NXTCommand;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Properties;

import org.evolvis.freedroidz.command.Program;

/**
 * @author fabian
 *
 */
public class RobotControl {
	
    public static int MAX_SPEED = 100;
    public static int MIN_SPEED = -100;
    static int speedLeft = MAX_SPEED;
    static int speedRight = MAX_SPEED;
    static int stepWidth = 15;
    
    static boolean invertLeft;
    static boolean invertRight;

    static boolean drivingDirectionForward = true;

    /** 
     * tacho change for a 360 degree turn of the robot, if both weels turn in different directions.
     */
    static int tachoForTotalTurn;

    static int tachoPerCm;

    static Motor motorLeft = Motor.B;
    static Motor motorRight = Motor.C;
    static Motor gadgetMotor = Motor.A;

    static int POSITIONS_MAX_SIZE = 10;
    static LinkedList positions = new LinkedList();
    public static int[] currentPosition = null;
    public static int[] currentDirection = pos(0,0);

    static Program currentProgram = null;

    static Program haltForWallMonitor = null;
    static boolean haltedForWall = false;
    static int gadgetDistance;
    
    static UltrasonicSensor us;

    static int ulrasonicWallDistance = 20;
    
    static Properties props;
    
    
    public static void init() 
                throws IOException {

    	// TODO we do not want hardcoded sensor-ports
        us = new UltrasonicSensor(SensorPort.S2);

    	props = new Properties();
        try {
            props.load(new FileInputStream(new File("config.properties")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
            throw e;
        }
        
		invertLeft = Boolean.parseBoolean(props.getProperty("INVERT_LEFT"));
		invertRight = Boolean.parseBoolean(props.getProperty("INVERT_RIGHT"));
		tachoForTotalTurn = Integer.parseInt(props.getProperty("TACHO_TOTAL_TURN"));
		stepWidth = Integer.parseInt((String)props.getProperty("STEP_WIDTH"));
		tachoPerCm = Integer.parseInt(props.getProperty("TACHO_PER_CM"));
		gadgetDistance = Integer.parseInt(props.getProperty("GADGET_DISTANCE"));
        
		us.setMetric(true);
    }

	public static void setVerify() {	
		
		NXTCommand.setVerify(true);
	}

	public static void fast() {
        speedRight = MAX_SPEED;
        speedLeft = MAX_SPEED;
        resend();
    }


	public static void driveOffset(int cm) {
        System.out.println("do-driveOffset: "+cm);

        if (cm == 0)
            return;
        
        int targetTacho = (invertRight && invertLeft) ? 
            motorLeft.getTachoCount() - (cm * tachoPerCm)
            : motorLeft.getTachoCount() + (cm * tachoPerCm);

        if (cm > 15)
            fast();
        else {
            speedRight = MAX_SPEED/4;
            speedLeft = MAX_SPEED/4;
            resend();
        }
        
        haltedForWall = false;
        if (invertRight && invertLeft)
            while (motorLeft.getTachoCount() > targetTacho && !haltedForWall) {}        
        else
            while (motorLeft.getTachoCount() < targetTacho && !haltedForWall) {}
        halt();
    }


    public static void fastBack() {
        speedRight = MIN_SPEED;
        speedLeft = MIN_SPEED;
        resend();
    }

    public static void faster() {
        speedRight += stepWidth;
        speedLeft += stepWidth;
        ensureValidSpeed();
        resend();
    }


    public static void slower() {
        speedRight -= stepWidth;
        speedLeft -= stepWidth;
        ensureValidSpeed();
        resend();
    }


    public static void left() {
        speedRight += 5;
        speedLeft -= 5;
        ensureValidSpeed();
        resend();
    }

    public static void turnLeft(int angle) {
        turn(false, angle);
    }
    
    public static void turnLeft() {
        speedRight = MAX_SPEED;
        speedLeft = MIN_SPEED;
        resend();
    }


    public static void turnRight() {
        speedRight = MIN_SPEED;
        speedLeft = MAX_SPEED;
        resend();
    }

    /**
     * turn right the specified angle in degree
     */
    public static void turnRight(int angle) {
        turn(true, angle);
    }

    /**
     * turn right the specified angle in degree
     */
    public static void turn(boolean directionRight, int angle) {
        System.out.println("do-turn: "+angle +" direction right="+directionRight);
        positions.clear();

        if (angle%360 == 0)
            return;
        if (invertLeft && invertRight)
            directionRight = ! directionRight;

        int targetTacho = (directionRight) ? 
            motorLeft.getTachoCount() + (((angle%360)  * tachoForTotalTurn)/360)
            : motorLeft.getTachoCount() - (((angle%360)  * tachoForTotalTurn)/360);

        if (directionRight) {
            speedRight = MIN_SPEED/4;
            speedLeft = MAX_SPEED/4;
        } else {
            speedRight = MAX_SPEED/4;
            speedLeft = MIN_SPEED/4;
        }
        resend();      
        if (angle == Integer.MAX_VALUE)
            return;

        haltedForWall = false;
        if (directionRight)
            while (motorLeft.getTachoCount() < targetTacho && !haltedForWall) {}
        else
            while (motorLeft.getTachoCount() > targetTacho && !haltedForWall) {}        
        halt();
    }

    public static void right() {
        speedRight -= 5;
        speedLeft += 5;
        ensureValidSpeed();
        resend();
    }


    public static void flt() {
        motorRight.flt();
        motorLeft.flt();
        speedLeft = speedRight = 0;
    }


    public static void stop() {
        motorRight.stop();
        motorLeft.stop();
        speedLeft = speedRight = 0;
        if (currentProgram != null) {
            currentProgram.stopProgram();
            currentProgram = null;
        }
    }

    public static void halt() {
        motorRight.stop();
        motorLeft.stop();
        speedLeft = speedRight = 0;
    }


    public static void pause() {
        motorRight.stop();
        motorLeft.stop();
    }
    
	 public static void setDriveSpeed(int driveSpeed) {
		 speedLeft = driveSpeed;
		 speedRight = driveSpeed;
		 resend();
	 }
	 
	 public static void setGadgetEnabled(boolean enabled) {
		 gadgetMotor.resetRotationCounter();
		 if(enabled)
			 gadgetMotor.forward();
		 else
			 gadgetMotor.backward();

		 while(Math.abs(gadgetMotor.getRotationCount()) < gadgetDistance);

		 gadgetMotor.stop();
	 }
	 
	 public static int getBatteryLevel() {	
			return NXTCommand.GET_BATTERY_LEVEL;
		}

    public static void setHaltForWall(boolean monitorWall) {
        if (monitorWall == true && haltForWallMonitor == null) {
            //start thread
            haltForWallMonitor = new Program() {
                    boolean run = true;
                    public void stopProgram() {
                        run = false;
                    }                    
                    
                    public void run() {
                        getAndClearHaltedForWall();
                        while (run) {
                            System.out.println("searching wall");
                            int distance = us.getDistance();
                            System.out.println("distance: "+distance);
                            
                            if (drivingDirectionForward && distance <= ulrasonicWallDistance) {
                                halt();
                                haltedForWall = true;
                                System.out.println("halted because we found a wall");                                
                            }
                            try {
                                Thread.sleep(5);
                            } catch (Exception e) {}
                        }
                    }
                };

            new Thread(haltForWallMonitor).start();
            
        } else if (!monitorWall && haltForWallMonitor != null) {
            // stop thread
            haltForWallMonitor.stopProgram();
            haltForWallMonitor = null;
        }
        
        // else: do nothing
    }

    
    public static boolean getAndClearHaltedForWall() {
        boolean result = haltedForWall;
        haltedForWall = false;
        return result;
    }
    
    public static void turnToBarrier() {
    	boolean searching = true;
    	while (searching) {
            int dist = getUltrasonicValue();
            System.out.println("dist: "+dist);                    
            if (dist < 30) {
                stop();
                searching = false;
                System.out.println("found barrier");
            }
            try {
            	turnRight();
                Thread.sleep(50);
                RobotControl.stop();
            } catch (Exception e) {}
        }
    }

    public static void driveTo(final int[] targetPos) {
        if (currentPosition == null) {
            System.out.println("cant't drive to pos - I don't know my position");
            return;
        }
        int[] moveVector = substractPos(targetPos, currentPosition);
        System.out.println();

        printPos(moveVector, "moveVector");
        printPos(currentDirection, "currentDirection");
        int turningAngle = getAngle(moveVector, currentDirection);
        System.out.println("turningAngle: "+turningAngle);
        
        turn((turningAngle > 0), Math.abs(turningAngle));
        driveOffset(getLengthInt(moveVector));
    }

    public static void startProgramDriveTo(final int[] targetPos) {
        currentProgram = new Program() {
                boolean run = true;
                public void stopProgram() {                    
                    run = false;
                    RobotControl.halt();
                }
                
                public void run() {
                    setHaltForWall(true);
                    while (run && currentPosition != null) {              
                        if (getDistance(targetPos, currentPosition) > 40) {
                            int[] moveVector = substractPos(targetPos, currentPosition);
                            driveTo(addPos(currentPosition, pos(moveVector[0]/2, moveVector[1]/2)));
                        } else {
                            driveTo(targetPos);
                            printPos(currentPosition, "drive from");
                            printPos(targetPos, "to");
                        }
                        if (getDistance(targetPos, currentPosition) < 20)
                            return;
                        try {
                            Thread.sleep(20);
                        } catch (Exception e) {}
                    }
                }
            };
        new Thread(currentProgram).start();        
    }

    public static void startProgramDriveArround() {
        currentProgram = new Program() {
                boolean run = true;
                public void stopProgram() {                    
                    run = false;
                    RobotControl.halt();
                }
                
                public void run() {
                    setHaltForWall(true);
                    while (run) {       
                        stop();
                        startProgramDriveTo(pos(-180, -60));
                        try {                            
                            Thread.sleep(20000);
                        } catch (Exception e) {}

                        stop();
                        startProgramDriveTo(pos(-80, -60));
                        try {
                            Thread.sleep(20000);
                        } catch (Exception e) {}

                        stop();
                        startProgramDriveTo(pos(-80, 45));
                        try {
                            Thread.sleep(20000);
                        } catch (Exception e) {}

                        stop();
                        startProgramDriveTo(pos(-180, 45));
                        try {
                            Thread.sleep(20000);
                        } catch (Exception e) {}

                        try {
                            Thread.sleep(20000);
                        } catch (Exception e) {}
                    }
                }
            };
        new Thread(currentProgram).start();        
    }


    public static void positionIs(int[] pos) {
        boolean use = (currentPosition == null) || getLength(substractPos(pos, currentPosition)) > 2;
        
        if (use) {
            positions.addFirst(pos);
            while (positions.size() > POSITIONS_MAX_SIZE)
                positions.removeLast();
        } else {
            System.out.println("ignoring new position, because significant change");            
        }
                
        currentPosition = pos;
        if (positions.size() >= 2) {

            // compute 
            int cummulativeX = 0;
            int cummulativeY = 0;
            int count = 0;
            
            for (int i=0; i<positions.size(); i++) {
                
                int[] oldPos = (int[])positions.get(i);
                int[] dir = substractPos(pos, oldPos);
                cummulativeX += dir[0] * (positions.size()-i);
                cummulativeY += dir[1] * (positions.size()-i);
                count += (positions.size()-i);
            }
            currentDirection = pos(cummulativeX/count, cummulativeY/count);
            System.out.println("currentDirection: "+ currentDirection[0] +","+currentDirection[1]);
        }        
    }
    
    public static int[] substractPos(int[] posA, int[] posB) {
        return new int[]{posA[0] - posB[0],
                         posA[1] - posB[1]};
    }
    
    public static int[] addPos(int[] posA, int[] posB) {
        return new int[]{posA[0] + posB[0],
                         posA[1] + posB[1]};
    }
    
    public static double getDistance(int[] posA, int[] posB) {
        if (posA == null || posB == null)
            return Integer.MAX_VALUE;
        return getLength(substractPos(posA, posB));
    }


    /**
     * @param posA toVector
     * @param posB fromVector
     */
    public static int getAngle(int[] posA, int[] posB) {
        double lengthProd = getLength(posA)*getLength(posB);
        if (lengthProd == 0)
            return 0;
        
        int crossProductZ = posA[0]*posB[1] - posA[1]*posB[0];
        System.out.println("crossProductZ: "+crossProductZ);       

        if (crossProductZ == 0)
            return 180;

        int mult = posA[0]*posB[0] + posA[1]*posB[1];
        System.out.println("mult: "+mult);
        
        System.out.println("multiplied length: "+lengthProd);
        
        double tmp = Math.toDegrees(Math.acos(
                                              (double)Math.abs(Math.abs(mult))/(lengthProd)
                                              ));
        
        System.out.println("tmp: "+tmp);
        
        
        int sign =  crossProductZ < 0
            ? -1
            : 1;

        
        System.out.println("sign: "+sign);       
        

        return mult < 0
            ? sign * (int) (180 - tmp)
            : sign * (int) tmp;            
        //        return sign * (int) tmp;            
    }

    public static double getLength(int[] posA) {
        return Math.sqrt(Math.abs(posA[0]*posA[0] + posA[1]*posA[1]));
    }    

    public static int getLengthInt(int[] posA) {
        return (int)Math.sqrt(Math.abs(posA[0]*posA[0] + posA[1]*posA[1]));
    }            
    
    public static int[] strToPos(String posString) {
        String[] pos = posString.split(",");
        return new int[]{Integer.parseInt(pos[0]),
                         Integer.parseInt(pos[1])};
    }

    public static int[] normize(int[] vector) {
        double length = getLength(vector);        
        return pos((int)(vector[0]/length), (int)(vector[1]/length));
    }

    public static int[] pos(int x, int y) {
        return new int[]{x,y};
    }

    public static void printPos(int[] pos, String text) {
        System.out.println(text +" "+pos[0] +","+ pos[1]);        
    }



    protected static void ensureValidSpeed() {
        if (speedRight > MAX_SPEED)
            speedRight = MAX_SPEED;        
        if (speedRight < MIN_SPEED)
            speedRight = MIN_SPEED;

        if (speedLeft > MAX_SPEED)
            speedLeft = MAX_SPEED;        
        if (speedLeft < MIN_SPEED)
            speedLeft = MIN_SPEED;        
    }

    protected static void resend() {
        drivingDirectionForward = speedLeft > 0 && speedRight > 0;

        motorLeft.setSpeed(Math.abs(speedLeft));
        motorRight.setSpeed(Math.abs(speedRight));

        if ((speedLeft > 0 && !invertLeft) || (speedLeft < 0 && invertLeft))
            motorLeft.forward();
        else
            motorLeft.backward();

        if ((speedRight > 0 && !invertRight) || (speedRight < 0 && invertRight))
            motorRight.forward();
        else
            motorRight.backward();

//        Motor.A.setSpeed(100);
//        Motor.A.forward();
    }
    
    public static int getUltrasonicValue() {

    	return us.getDistance();
    }
    
    public static void close() {
        try {
            NXTCommand.close();
        } catch (Exception e) {
            System.out.println("error on closing: "+e);            
        }
    }
}
