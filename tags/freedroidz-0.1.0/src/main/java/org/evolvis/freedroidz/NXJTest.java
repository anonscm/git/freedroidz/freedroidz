/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 18 January 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.freedroidz;

import icommand.nxt.Battery;
import icommand.nxt.LightSensor;
import icommand.nxt.Motor;
import icommand.nxt.SensorPort;
import icommand.nxt.Sound;
import icommand.nxt.comm.NXTCommand;

public class NXJTest {

	public static void main(String[] args) throws Exception {
		NXTCommand.open();
		NXTCommand.setVerify(true);
		Motor.B.setSpeed(700);
		System.out.println("Battery = " + Battery.getVoltageMilliVolt());
		System.out.println("Waiting for motor to stop");
		Motor.B.rotateTo(400);
		System.out.println("Motor has stopped.");
		System.out.println("Motor tacho = " + Motor.B.getTachoCount());
		LightSensor ls = new LightSensor(SensorPort.S1);
		System.out.println("Light sensor Port 1: " + ls.getLightValue());
		System.out.println("% Light Port 1: " + ls.getLightPercent());
		System.out.println("Closing shop");
		NXTCommand.close();
	}
}
