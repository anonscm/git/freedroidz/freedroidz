/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.freedroidz.strategy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import org.evolvis.freedroidz.events.SensorListener;
import org.evolvis.taskmanager.TaskManager;
import org.evolvis.taskmanager.TaskManager.Task;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public abstract class AbstractStrategy implements Strategy {
	
	protected final static Logger logger = Logger.getLogger(AbstractStrategy.class.getName());
	protected List<SensorListener> sensorListeners;
	protected List<StrategyListener> strategyListeners;
	
	protected void registerExclusiveTask(Task task, String taskDescription, boolean cancelable) {
		TaskManager.getInstance().registerExclusive(task, taskDescription, cancelable);
	}
	
	protected void registerBlockingTask(Task task, String taskDescription, boolean cancelable) {
		TaskManager.getInstance().registerBlocking(task, taskDescription, cancelable);
	}
	
	protected void registerTask(Task task, String taskDescription, boolean cancelable) {
		TaskManager.getInstance().register(task, taskDescription, cancelable);
	}
	
	protected void log(String text) {
		logger.info(text);
	}
	
	protected void addSensorListener(SensorListener listener) {
		getSensorListeners().add(listener);
	}
	
	protected List<SensorListener> getSensorListeners() {
		if(sensorListeners == null)
			sensorListeners = new ArrayList<SensorListener>();
		
		return sensorListeners;
	}
	
	protected List<StrategyListener> getStrategyListeners() {
		if(strategyListeners == null)
			strategyListeners = new ArrayList<StrategyListener>();
		
		return strategyListeners;
	}
	
	public void fireStrategyStarted() {
		Iterator<StrategyListener> it = getStrategyListeners().iterator();
		
		while(it.hasNext())
			it.next().strategyStarted(new StrategyEvent());
	}
	
	public void fireStrategyFinished() {
	
		Iterator<StrategyListener> it = getStrategyListeners().iterator();
		
		while(it.hasNext())
			it.next().strategyFinished(new StrategyEvent());
	}
	
	public void fireStrategyStopped() {
		Iterator<StrategyListener> it = getStrategyListeners().iterator();
		
		while(it.hasNext())
			it.next().strategyStopped(new StrategyEvent());
	}

	/**
	 * @see org.evolvis.freedroidz.strategy.Strategy#addStrategyListener(org.evolvis.freedroidz.strategy.StrategyListener)
	 */
	public void addStrategyListener(StrategyListener listener) {
		getStrategyListeners().add(listener);
	}
}
