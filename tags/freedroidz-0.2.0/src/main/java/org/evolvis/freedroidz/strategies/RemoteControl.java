/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.freedroidz.strategies;

import java.awt.event.KeyEvent;
import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.evolvis.freedroidz.Services;
import org.evolvis.freedroidz.command.Command;
import org.evolvis.freedroidz.command.CommandListener;
import org.evolvis.freedroidz.control.RobotControl;
import org.evolvis.freedroidz.strategy.AbstractStrategy;

public class RemoteControl extends AbstractStrategy implements CommandListener{
	
	protected static final Logger logger = Logger.getLogger(RemoteControl.class.getName());

	boolean moving = false;

	public void run() {
		logger.info("Strategy Remote Control");
		logger.info("need to register myself to the Command Provider");
		Services.getInstance().getCommandProvider().addCommandListener(this);
		fireStrategyStarted();
	}

	public String getDescription() {
		return "Simple remote control strategy";
	}

	public String getName() {
		return "Remote Control";
	}

	public void commandSent(Command command) {
		logger.info("processing "+command);

		int code = command.getCode();

		if(SWT.ARROW_UP == code || KeyEvent.VK_UP == code) {
			RobotControl.getInstance().driveForward(200);
			moving = true;
		}
		else if(SWT.ARROW_DOWN == code || KeyEvent.VK_DOWN == code) {
			if(moving)  {
				RobotControl.getInstance().brake();
				moving = false;
			}
			else {
				RobotControl.getInstance().driveBackward(100);
				moving = true;
			}
		}
		else if(SWT.ARROW_LEFT == code || KeyEvent.VK_LEFT == code) {
			if(moving)
				RobotControl.getInstance().driveLeftForward(20);
			else
				RobotControl.getInstance().turnLeft(5);
		}
		else if(SWT.ARROW_RIGHT == code || KeyEvent.VK_RIGHT == code) {
			if(moving)
				RobotControl.getInstance().driveRightForward(20);
			else
				RobotControl.getInstance().turnRight(5);
		}
		else
			System.out.println("unknown command: "+command);


	}

	public void commandReleased(Command command){
		logger.info("Command released");;
//		RobotControl.getInstance().rollOut();		
	}

	public void stop() {
		RobotControl.getInstance().brake();
		Services.getInstance().getCommandProvider().removeCommandListener(this);
		fireStrategyStopped();
	}

}
