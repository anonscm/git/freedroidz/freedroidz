/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.freedroidz.ui;

import java.util.Iterator;
import java.util.logging.Logger;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Text;
import org.evolvis.freedroidz.Services;
import org.evolvis.freedroidz.command.Command;
import org.evolvis.freedroidz.config.RobotManager;
import org.evolvis.freedroidz.control.RobotControl;
import org.evolvis.freedroidz.strategy.Strategy;
import org.evolvis.freedroidz.strategy.StrategyEvent;
import org.evolvis.freedroidz.strategy.StrategyListener;
import org.evolvis.freedroidz.strategy.StrategyManager;

/**
 *  
 * @author Thomas Schmitz, tarent GmbH
 */
public class StrategyPanel extends Composite implements KeyListener{

	protected final static Logger logger = Logger.getLogger(StrategyPanel.class.getName());
	protected Combo strategyCombo;
	protected Text descriptionText;
	protected Button runButton;
	protected boolean strategyRunning;
	
	/**
	 * Creates a new instance.
	 * 
	 * @param parent the parent container
	 */
	public StrategyPanel(Composite parent) {
		super(parent, SWT.BORDER);
		createGUI();
		addKeyListener(this);
	}

	/**
	 * Creates the GUI components and lays them down on this Composite, using
	 * the MigLayout.
	 */
	private void createGUI() {
		setLayout(new MigLayout("fillx, wrap 2", "[] [grow, fill]"));

		Label strategyLabel = new Label(this, SWT.READ_ONLY);
		strategyLabel.setText("Strategy");

		strategyCombo = new Combo(this, SWT.READ_ONLY);
		strategyCombo.addKeyListener(this);
		
		Iterator<Strategy> strategies = StrategyManager.getInstance().getAvailableStrategies().iterator();
		
		while(strategies.hasNext())
			strategyCombo.add(strategies.next().getName());
		
		strategyCombo.select(0);

		Label descriptionLabel = new Label(this, SWT.READ_ONLY);
		descriptionLabel.setText("Description");

		descriptionText = new Text(this, SWT.MULTI | SWT.BORDER | SWT.READ_ONLY);
		descriptionText.setEditable(false);
		descriptionText.setLayoutData("height 80");
		descriptionLabel.addKeyListener(this);
		descriptionText.setText(StrategyManager.getInstance().getAvailableStrategies().get(strategyCombo.getSelectionIndex()).getDescription());
		
		strategyCombo.addSelectionListener(new SelectionListener() {

			/**
			 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			/**
			 * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			public void widgetSelected(SelectionEvent arg0) {
				// update the description-field
				descriptionText.setText(StrategyManager.getInstance().getAvailableStrategies().get(strategyCombo.getSelectionIndex()).getDescription());
			}
			
		});
		
		@SuppressWarnings("unused")
		Label sensorLabel = new Label(this, SWT.NONE);
		//sensorLabel.setText("Sensors");
		
		runButton = new Button(this, SWT.None);
		runButton.setText("Start Strategy");
		runButton.addKeyListener(this);
		runButton.addSelectionListener(new SelectionListener() {

			/**
			 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			public void widgetDefaultSelected(SelectionEvent arg0) {

			}

			/**
			 * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			public void widgetSelected(SelectionEvent event) {
				
				final Strategy strategy = StrategyManager.getInstance().getAvailableStrategies().get(strategyCombo.getSelectionIndex());
				
				// Disable UI (Activation of "Start Strategy"-button would lead to an error because of trying to connect twice)
				setControlsEnabled(false);
				
				if(strategyRunning) {
					strategy.stop();
					return;
				}
				
				// register listener to react on changes in strategy-state
				strategy.addStrategyListener(new StrategyListener() {
					
					/**
					 * @see org.evolvis.freedroidz.strategy.StrategyListener#strategyStarted(org.evolvis.freedroidz.strategy.StrategyEvent)
					 */
					public void strategyStarted(StrategyEvent event) {
						strategyRunning = true;
						
						// re-enable run-button (labeled "stop strategy") in order to allow the strategy to be stopped
						enableRunButtonAndSetText("Stop Strategy");
					}

					/**
					 * @see org.evolvis.freedroidz.strategy.StrategyListener#strategyStopped(org.evolvis.freedroidz.strategy.StrategyEvent)
					 */
					public void strategyStopped(StrategyEvent event) {
						strategyRunning = false;
						
						// re-enable the run-button (labeled "start strategy") in order to allow the strategy to be stopped
						RobotControl.getInstance().dispose();
						enableRunButtonAndSetText("Start Strategy");
						setControlsEnabled(true);
					}

					/**
					 * @see org.evolvis.freedroidz.strategy.StrategyListener#strategyFinished(org.evolvis.freedroidz.strategy.StrategyEvent)
					 */
					public void strategyFinished(StrategyEvent event) {
						strategyStopped(event);
					}
				});
				
				try {
					
					new Thread() {
						public void run() {
							// connect to robot
							RobotControl.getInstance().connect(RobotManager.getInstance().getActiveRobot());
							
							// start strategy
							strategy.run();
						}
					}.start();
					
					 
				} catch(Exception excp) {
					MessageBox message = new MessageBox(StrategyPanel.this.getShell(), SWT.ICON_WARNING);
					excp.printStackTrace();
					logger.warning(excp.toString());
					message.setMessage(excp.toString());
					message.setText("Error");
					message.open();
				}
			}
		});
	}
	
	protected void enableRunButtonAndSetText(final String text) {
		getDisplay().asyncExec(new Runnable() {

			/**
			 * @see java.lang.Runnable#run()
			 */
			public void run() {
				runButton.setText(text);
				runButton.setEnabled(true);
			}
			
		});	
	}
	
	protected void setControlsEnabled(final boolean enabled) {
		this.getDisplay().asyncExec(new Runnable() {

			/**
			 * @see java.lang.Runnable#run()
			 */
			public void run() {
				runButton.setEnabled(enabled);
				strategyCombo.setEnabled(enabled);
				descriptionText.setEnabled(enabled);
			}
		
		});
	}

	public void keyPressed(KeyEvent arg0) {
		//System.out.println("key pressed: "+arg0);
		Command command = new Command(arg0);
		Services.getInstance().getCommandProvider().fireCommandSent(command);
		
	}

	public void keyReleased(KeyEvent arg0) {
		//System.out.println("key released: "+arg0);
		Command command = new Command(arg0);
		Services.getInstance().getCommandProvider().fireCommandReleased(command);
		
	}
}
