package org.evolvis.freedroidz.sensor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import org.evolvis.freedroidz.Services;
import org.evolvis.freedroidz.sensor.SensorListener;

/**
 * This class provides a polling mechanism that makes it possible to react on sensor values. 
 * It notifies its listeners after a configured interval of time is expired. 
 * Listeners must implement the method pollingIntervalExpired() and can use this method for example for 
 * sensor checking code or any other code that has to be executed periodicaly. 
 * 
 * This mechanism is most suitable for simple strategies. Be aware of multithreading problems!
 *  
 * @author Steffi Tinder, tarent GmbH
 *
 */
public class SensorProvider extends Thread {
	
	protected final static Logger logger = Logger.getLogger(SensorProvider.class.getName());
	
	public static final long DEFAULT_POLLING_INTERVAL=100;

	private long interval;
	private boolean run=false;

	private List<SensorListener> listeners = new ArrayList<SensorListener>();
	
	public SensorProvider(){
		this(DEFAULT_POLLING_INTERVAL);
	}
		
	public SensorProvider(long interval){
		this.interval=interval;
	}

	public void addSensorListener(SensorListener listener) {
		listeners.add(listener);
		logger.info("listener added: "+listener);
		if (run==false){
			this.start();
		}
	}

	public void removeSensorListener(SensorListener listener) {
		if (listeners.contains(listener)) listeners.remove(listener);
	}

	protected void firePollingIntervalExpiredEvent(){
		Iterator iterator = listeners.iterator();
		SensorListener listener = null;
		while (iterator.hasNext()){
			listener=(SensorListener)iterator.next();
			listener.pollingIntervalExpired();
		}		
	}
	
	public void run(){
		run=true;
		while(run){
			try {
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			firePollingIntervalExpiredEvent();
		}
	}
	
	public void stopPolling(){
		run=false;		
	}

}
