package org.evolvis.freedroidz.strategies;
import wiiusej.WiiUseApiManager;
import wiiusej.wiiuseapievents.StatusEvent;
import wiiusej.Wiimote;
import wiiusej.test.WiiuseJGuiTest;
import wiiusej.wiiuseapievents.ButtonsEvent;
import wiiusej.wiiuseapievents.DisconnectionEvent;
import wiiusej.wiiuseapievents.IREvent;
import wiiusej.wiiuseapievents.MotionSensingEvent;
import wiiusej.wiiuseapievents.WiimoteListener;


import org.evolvis.freedroidz.control.RobotControl;
import org.evolvis.freedroidz.strategy.AbstractStrategy;
import org.evolvis.freedroidz.strategy.Strategy;
import org.evolvis.freedroidz.strategy.StrategyManager;

public class WiiuseJ extends AbstractStrategy{

	protected static StrategyManager instance;
	protected static WiiUseApiManager listeners;
	public int pitchmaximum = 25;
	public int pitchminimum = -25;
	public int rollmaximum = 30;
	public int rollminimum = -30;
	public int speed = 600;
	public int increased_speed = 50;
	public int decreased_speed = -50;
	
	private Wiimote wiimote;
	private WiimoteListener listener;


	public static void main(String[] args) {
		Wiimote[] wiimotes = WiiUseApiManager.getWiimotes(1, true);
		if (wiimotes.length>0){
			WiiuseJGuiTest gui = new WiiuseJGuiTest(wiimotes[0]);

			gui.setVisible(true);
		}
	}

	public String getDescription() {
		return "Control your robot with a Wii Controller\r\n\n" +
		"Accepted commands are:\r\n" +
		"MotionSensing; forward; backward; stop; driveLeft; driveRight; increaseSpeed; decreaseSpeed; terminateSession;";
	}

	public String getName() {
		return "Wii Remote Control";
	}

	public void stop() {
		// halt robot
		RobotControl.getInstance().brake();

		// deregister Wii-listener and free memory
		wiimote.removeWiiMoteEventListeners(listener);
		wiimote.deactivateIRTRacking();
		wiimote.deactivateMotionSensing();
		
		wiimote = null;

		fireStrategyStopped();
	}

	public void run() {
		RobotControl.getInstance().getRobotHardware().getMotorExtra().setSpeed(400);
		fireStrategyStarted();

		// If connection to Wii could not be established, stop strategy



		Wiimote[] wiimotes = WiiUseApiManager.getWiimotes(1, true);
		wiimote = wiimotes[0];
		wiimote.activateIRTRacking();
		wiimote.activateMotionSensing();
		wiimote.addWiiMoteEventListeners(listener = new WiimoteListener(){

			private static final int STATUS_FORWARD = 0;
			private static final int STATUS_BACKWARD = 1;
			private static final int STATUS_LEFT = 2;
			private static final int STATUS_RIGHT = 3;
			private static final int STATUS_BRAKE = 4;


			private int status = STATUS_BRAKE;



			public void onButtonsEvent(ButtonsEvent arg0) {

				// Increase Speed

				if(arg0.isButtonPlusJustPressed()==true){
					if(speed <= 850){
						speed = speed + increased_speed;
						System.out.println("increased speed by: " + increased_speed);
						System.out.println("speed right know: " + speed );
					}
				}

				// Decrease Speed

				if(arg0.isButtonMinusJustPressed()==true){
					if(speed >= 50){
						speed = speed + decreased_speed;
						System.out.println("decreased speed by: " + decreased_speed);
						System.out.println("speed right know: " + speed );
					}
				}

				if(arg0.isButtonBJustPressed()==true){
					RobotControl.getInstance().brake();
//					System.out.println(arg0);
				}

				if (arg0.isButtonHomeJustPressed()==true){
					fireStrategyFinished();
//					System.out.println(arg0);
				}

				if (arg0.isButtonUpHeld()==true && arg0.isButtonLeftHeld()==true){
					RobotControl.getInstance().driveLeftForward(speed - 170);
					RobotControl.getInstance().driveRightForward(speed);
					//			 	System.out.println("Klappt!");
				}
				else{
					if(arg0.isButtonUpPressed()==true){
						RobotControl.getInstance().driveForward(speed);
						//	System.out.println(arg0);
					}	    			 
				}

				if (arg0.isButtonUpHeld()==true && arg0.isButtonRightHeld()==true){
					RobotControl.getInstance().driveLeftForward(speed);
					RobotControl.getInstance().driveRightForward(speed - 170);
					//		 	System.out.println("Klappt!");
				}
				else{
				}

				if (arg0.isButtonDownHeld()==true && arg0.isButtonLeftHeld()==true){
					RobotControl.getInstance().driveLeftBackward(speed - 170);
					RobotControl.getInstance().driveRightBackward(speed);
					//		 	System.out.println("Klappt!");


				}
				else{
					if(arg0.isButtonDownPressed()==true){
						RobotControl.getInstance().driveBackward(speed);
						//	System.out.println(arg0);
					}		
					if(arg0.isButtonLeftPressed()==true){
						RobotControl.getInstance().driveRightForward(speed);
						//	System.out.println(arg0);
					}

				}

				if (arg0.isButtonDownHeld()==true && arg0.isButtonRightHeld()==true){
					RobotControl.getInstance().driveLeftBackward(speed);
					RobotControl.getInstance().driveRightBackward(speed - 170);
					//		 	System.out.println("Klappt!");
				}
				else{
					if(arg0.isButtonRightPressed()==true){
						RobotControl.getInstance().driveLeftForward(speed);
						//	System.out.println(arg0);
					}
				}

				if (arg0.isButtonOneJustPressed() == true){
					RobotControl.getInstance().getRobotHardware().getMotorExtra().forward();
					
				} else if(arg0.isButtonOneJustReleased() || arg0.isButtonTwoJustReleased()) {
					RobotControl.getInstance().getRobotHardware().getMotorExtra().stop();
				} else if(arg0.isButtonTwoJustPressed()) {
					RobotControl.getInstance().getRobotHardware().getMotorExtra().backward();
				}
			}

			public void onIrEvent(IREvent arg0) {
				//   System.out.println(arg0);
			}

			public void onMotionSensingEvent(MotionSensingEvent arg0){

				if(status == STATUS_BRAKE && arg0.getOrientation().getPitch()>= pitchmaximum){
					System.out.println("Pitchmaximum: " + pitchmaximum);
					status = STATUS_BACKWARD;
					RobotControl.getInstance().driveBackward(speed);
					//		System.out.println(status);
				}

				if(status == STATUS_BACKWARD && arg0.getOrientation().getPitch()<= pitchmaximum){
					System.out.println("Pitchminimum: " + pitchminimum);
					status = STATUS_BRAKE;
					RobotControl.getInstance().brake();
					// 		System.out.println(status);
				}

				if(status == STATUS_BRAKE && arg0.getOrientation().getPitch()<= pitchminimum){
					System.out.println("Pitchminimum: " + pitchminimum);
					status = STATUS_FORWARD;
					RobotControl.getInstance().driveForward(speed);
					//   	System.out.println(status);
				}

				if(status == STATUS_FORWARD && arg0.getOrientation().getPitch()>= pitchminimum){
					System.out.println("Pitchmaximum: " + pitchmaximum);
					status = STATUS_BRAKE;
					RobotControl.getInstance().brake();
					//    	System.out.println(status);
				}

				if(status == STATUS_BRAKE && arg0.getOrientation().getRoll()<= rollminimum){
					System.out.println("Rollminimum: " + rollminimum);
					status = STATUS_RIGHT;
					RobotControl.getInstance().driveRightForward(speed);
					//  	System.out.println(status);
				}

				if(status == STATUS_RIGHT && arg0.getOrientation().getRoll()>= rollminimum){
					System.out.println("Rollmaximum: " + rollmaximum);
					status = STATUS_BRAKE;
					RobotControl.getInstance().brake();
					//  	System.out.println(status);
				}

				if(status == STATUS_BRAKE && arg0.getOrientation().getRoll()>= rollmaximum){
					System.out.println("Rollmaximum: " + rollmaximum);
					status = STATUS_LEFT;
					RobotControl.getInstance().driveLeftForward(speed);
					//   	System.out.println(status);
				}

				if(status == STATUS_LEFT && arg0.getOrientation().getRoll()<= rollmaximum){
					System.out.println("Rollminimum: " + rollminimum);
					status = STATUS_BRAKE;
					RobotControl.getInstance().brake();
					//	   System.out.println(status);
				}
			}            

			public void onDisconnectionEvent(DisconnectionEvent e) {
				// TODO Auto-generated method stub

			}

			public void onStatusEvent(StatusEvent e) {
				// TODO Auto-generated method stub

			}

		});


	}
}