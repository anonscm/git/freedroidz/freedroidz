/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.freedroidz.strategy;

/**
 * This interface describes a StrategyListener for freedroidz.
 * 
 * A StrategyListener can be registered on a strategy in order to get notified
 * when some events occur in the strategy (e.g the strategy has finished). 
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public interface StrategyListener {

	/**
	 * Called when the strategy starts to work (after initialisation, configuring etc.)
	 * When this method is called the strategy can be stopped by user in GUI.
	 * So this method should really be called sometime!
	 * 
	 * @param event
	 */
	public void strategyStarted(StrategyEvent event);

	/**
	 * Called when the strategy has finished its work. Only relevant when the strategy is finite.
	 * When this method is called the user can immediately select a new strategy in GUI and start it.
	 *  
	 * @param event
	 */
	public void strategyFinished(StrategyEvent event);
	
	/**
	 * Called when the strategy was stopped by the user and has finished to go into the robots' initial-state (Stop motors etc).
	 * When this method is called the user can immediately select a new strategy in GUI and start it.
	 *  
	 * @param event
	 */
	public void strategyStopped(StrategyEvent event);
}
