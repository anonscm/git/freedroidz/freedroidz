package org.evolvis.freedroidz.strategies.mrsensitive;

public class Stupps extends StateMachineControlState {

	public Stupps(StateMachineData data) {
		super(data);
		
	}
	@Override
	public StateMachineControlState enter() {
		RobotHardware hw = data.getHardware();
		hw.move(RobotHardware.UPPER, 40, 500);
		hw.move(RobotHardware.LOWER, -40, 400);
		return super.enter();
	}
	@Override
	public StateMachineControlState complete(int[] i) {
		RobotHardware hardware = data.getHardware();
		if(hardware.isComplete(RobotHardware.UPPER)&&hardware.isComplete(RobotHardware.LOWER)){
			//return new Neutral(data,new Idle(data));
			return new Moving(data);
		}
		return null;
	}
	
	@Override
	public StateMachineControlState touch(int[] i) {
		RobotHardware hardware = data.getHardware();
		if(hardware.isTouching(RobotHardware.FINGER)){
			System.err.println("Touch!");
			//return //new Neutral(data,new Idle(data));
			return new Moving(data);
		}
		return super.touch(i);
	}
	@Override
	public boolean isMotorInfoRequired(int i) {
		return i==RobotHardware.UPPER||i==RobotHardware.LOWER;
	}
	@Override
	public boolean isSensorInfoRequired(int i) {
		
		return i== RobotHardware.FINGER;
	}
}
