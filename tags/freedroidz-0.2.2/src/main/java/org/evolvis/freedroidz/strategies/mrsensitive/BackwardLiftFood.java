package org.evolvis.freedroidz.strategies.mrsensitive;

public class BackwardLiftFood extends StateMachineControlState {

	public BackwardLiftFood(StateMachineData data) {
		super(data);
		
	}
	@Override
	public StateMachineControlState enter() {
		RobotHardware hardware = data.getHardware();
		hardware.moveTo(RobotHardware.UPPER, 40,100);
		return super.enter();
	}
	@Override
	public StateMachineControlState complete(int[] i) {
		RobotHardware hardware = data.getHardware();
		
		if(hardware.isComplete(RobotHardware.UPPER))
		{
			return new Backward(data);
		}
		return super.complete(i);
	}
	@Override
	public boolean isMotorInfoRequired(int i) {
	return i==RobotHardware.UPPER;
	}
}
