package org.evolvis.freedroidz.strategies.mrsensitive;


public class StateMachineData {

	private final RobotHardware simulatedHardware;

	public StateMachineData(RobotHardware simulatedHardware) {
		this.simulatedHardware = simulatedHardware;
	}

	public RobotHardware getHardware() {
		return simulatedHardware;
	}

}
