package org.evolvis.freedroidz.strategies.mrsensitive;

public class TurnMoveTorso extends StateMachineControlState {
	private int turn;

	public TurnMoveTorso(StateMachineData data, int turn) {
		super(data);
		this.turn = turn;
	}
	
	@Override
	public StateMachineControlState enter() {
		RobotHardware hardware = data.getHardware();
		hardware.move(RobotHardware.BODY, turn, 600);
		return super.enter();
	}
	
	@Override
	public StateMachineControlState complete(int[] i) {
		RobotHardware hardware = data.getHardware();
		if(hardware.isComplete(RobotHardware.BODY)){
			//return new Turn3(data,turn);
			return new Stomp(data,new TurnMoveWheels(data,turn));
			//return new Idle(data);
		}
		return null;
	}
	@Override
	public boolean isMotorInfoRequired(int i) {
		return i==RobotHardware.BODY;
	}
	
}
