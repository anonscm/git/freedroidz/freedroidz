package org.evolvis.freedroidz.strategies.mrsensitive;

import org.eclipse.swt.SWT;

/**
 * a control state. (as in the State Pattern)
 * 
 * Disjunct nested states are possible (see getParentState()).
 * The return value of the enter() method is used to designate nested initial states.
 * Conjunct nesting is not supported. (not directly, anyway).
 * 
 * Event handler methods should return the receiver or null to indicate reflexive
 * transitions.
 * 
 * Long-lived (i.e. context) data is attached to the data field, which is passed on
 * between states.
 */
public abstract class StateMachineControlState {
	protected StateMachineData data;
	StateMachineControlState parent = null;

	/**
	 * create a toplevel state.
	 * @param data
	 */
	public StateMachineControlState(StateMachineData data) {
		this(data, null);
	}

	/**
	 * create a nested state.
	 * @param data
	 * @param parent
	 */
	public StateMachineControlState(StateMachineData data, StateMachineControlState parent) {
		this.data = data;
		this.parent = parent;
	}

	/**
	 * called when control enters this state.
	 * 
	 * Default implementation does nothing.
	 */
	public StateMachineControlState enter() {
		return null;
	}

	/**
	 * called when control exits this state.
	 * Default implementation does nothing. 
	 */
	public void exit() {

	}

	

	
	
	/**
	 * event handler delegatee method.
	 * @see RobotControl#complete()
	 * The default implementation does nothing;
	 * @return next state.
	 */
	public StateMachineControlState complete(int[] i) {
		return this;
	}

	
	/**
	 * event handler delegatee method.
	 * @see RobotControl#touch()
	 * The default implementation does nothing.
	 * @return next state.
	 */
	public StateMachineControlState touch(int[] i) {
		return this;
	}
	/**
	 * event handler delegatee method.
	 * @param i 
	 * @see RobotControl#release()
	 * The default implementation does nothing.
	 * @return next state.
	 */
	public StateMachineControlState release(int[] i) {
		return this;
	}
	/**
	 * event handler delegatee method.
	 * @see RobotControl#keyDown(int)
	 * The default implementation does nothing.
	 * @return next state.
	 */
	public StateMachineControlState keyDown(int code) {
		switch(code){
		case ' ': return new Idle(data);
		case 'p': printPositions();return null;
		case 'w': return new Forward(data);
		case 's': return new Backward(data);
		case 'a': return new Turn(data,-720);
		case 'd': return new Turn(data,720);
		case 'y': return new TurnX(data,-100);
		case 'c': return new TurnX(data,100);
		case '1': return new Stomp(data,new Moving(data));
		case '2': return new Neutral(data,new Moving(data));
		case '3': return new Neutral(data,new Stupps(data));
		case 'h': return new Stomp(data,new StompX(data));
		case 'm':return new Moving(data);
		}
		return this;
	}
	private void printPositions() {
		System.err.println("upper:"+data.getHardware().getUpperArmPosition());
		System.err.println("lower:"+data.getHardware().getLowerArmPosition());
		System.err.println("distance: "+data.getHardware().getDistance());
	}

	/**
	 * event handler delegatee method.
	 * @see RobotControl#keyUp(int)
	 * The default implementation does nothing.
	 * @return next state.
	 */
	public StateMachineControlState keyUp(int code) {
		return this;
	}
	/**
	 * Return containing state.
	 * @return containing state if this is a nested state, null otherwise.
	 */
	public StateMachineControlState getParentState() {
		return parent;
	}

	public boolean isMotorInfoRequired(int i) {
		return false;
	}

	public boolean isSensorInfoRequired(int i){
		return false;
	}
	

}
