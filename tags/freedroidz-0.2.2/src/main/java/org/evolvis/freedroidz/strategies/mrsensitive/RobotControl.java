package org.evolvis.freedroidz.strategies.mrsensitive;

/**
 * Robot control abstraction.
 * NOT thread save. Implementations will most probably expect all events
 * to originate from a single dispatcher thread. 
 */
public interface RobotControl {
	public boolean isSensorInfoRequired(int i);
	public boolean isMotorInfoRequired(int i);
	
	
	/**
	 * called when task completion was detected (see isComplete()) by the event dispatcher.
	 */
	public void complete(int[] is);
	
	
	/**
	 * called by the dispatcher when the touch sensor is pressed.
	 */
	public void touch(int[] is);
	
	/**
	 * called by the dispatcher when the touch sensor is released.
	 */
	public void release(int[] is);
	
	/**
	 * called by the dispatcher when a keyboard key is pressed.
	 */
	public void keyDown(int code);
	
	/**
	 * called by the dispatcher when a keyboard key is released.
	 */
	public void keyUp(int code);

	
}
