package org.evolvis.freedroidz.strategies;

import icommand.nxt.comm.NXTCommand;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Slider;

import org.evolvis.freedroidz.control.RobotControl;
import org.evolvis.freedroidz.strategy.AbstractStrategy;
import org.evolvis.freedroidz.strategy.Strategy;
import org.evolvis.freedroidz.strategy.StrategyManager;
import org.evolvis.freedroidz.ui.SteeringPanel;

public class MotionControl extends AbstractStrategy {

	Shell shell = new Shell();
	Slider sliderV;
	Slider sliderH;
	int x;
	int y;
	int z;
	
	public void run() {	
		new Thread(new Runnable() {
			public void run() {	
				try {
					System.out.println("Motiontracking started");
					FileInputStream inputStream;
					byte[] input = new byte[48];
					inputStream = new FileInputStream("/dev/input/event3");
					
					Display.getDefault().asyncExec(new Runnable() {

						public void run() {
							sliderV = new Slider (shell, SWT.VERTICAL);
							sliderV.setBounds (200, 1, 32, 400);
							sliderH = new Slider (shell, SWT.HORIZONTAL);
							sliderH.setBounds (1, 200, 400, 32);
							sliderV.setMaximum(1500);
							sliderH.setMaximum(1500);
							Button stop = new Button(shell, SWT.PUSH);
							stop.setText("Stop");
							stop.setBounds (1, 420, 100, 50);
							stop.addSelectionListener(new SelectionAdapter() {
								public void widgetSelected(SelectionEvent evt) {
									stop();
								}
							});
							
							fireStrategyStarted();
							
							shell.open();
							
							while (!shell.isDisposed()) {
								if (!shell.getDisplay().readAndDispatch()) {
									shell.getDisplay().sleep();
								}
							}
						}
					});
					
					while(true)
					{
						try {
							Thread.sleep(20);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						try {
							inputStream.read(input);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						
						if(input[10] == 0){
							x = 256 * input[13] + (input[12] & 0xff);}
						if(input[26] == 0){
							x = 256 * input[29] + (input[28] & 0xff);}
						if(input[42] == 0){
							x = 256 * input[45] + (input[44] & 0xff);} 
						
						if(input[10] == 1){
							y = 256 * input[13] + (input[12] & 0xff);}  
						if(input[26] == 1){
							y = 256 * input[29] + (input[28] & 0xff);}
						if(input[42] == 1){
							y = 256 * input[45] + (input[44] & 0xff);}  
						
						if(input[10] == 2){
							z = 256 * input[13] + (input[12] & 0xff);}
						if(input[26] == 2){
							z = 256 * input[29] + (input[28] & 0xff);}
						if(input[42] == 2){
							z = 256 * input[45] + (input[44] & 0xff);}
						
						
						int powerx = x / 100;
						int powery = y / 100;
						
					
						if((x != 0) && (y != 0)){
							System.out.println("X: "+powerx);
							System.out.println("Y: "+powery);
							System.out.println();
							
							if(powery < -2){
								try {
									A(true, true, powery*-50);
									C(true, false, powery*-50);
									System.out.println("Right");
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							else if(powery > 2){
								try {
									A(true, false, powery*50);
									C(true, true, powery*50);
									System.out.println("Left");
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							
							else{
								if(powerx < -2){
									try {
										A(true, false, powerx*-100);
										C(true, false, powerx*-100);
										System.out.println("Back");
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
								else if(powerx > 2){
									try {
										A(true, true, powerx*100);
										C(true, true, powerx*100);
										System.out.println("Go");
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
								
								else{
									System.out.println("Break");
									try {
										A(false, false, powerx*-100);
										C(false, false, powerx*-100);
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} 	
								}
							}
							
							setPositionAsync((-1*y)+750, x+750);
						}
						
						
						
				
					}
				}
			
				 catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Display.getDefault().asyncExec(new Runnable() {
						public void run() {
							MessageBox mb = new MessageBox(shell, SWT.ICON_WARNING);
							mb.setText("Wrong device");
							mb.setMessage("Strategy is only for Neo: FreeRunner");
							mb.open();
							fireStrategyStopped();
						}
					});
				}
			}
		}).start();
	}

	
	
	public boolean powerA = false;
	public boolean directionA = false;
	public int speedA = 200;
	
	public boolean powerC = false;
	public boolean directionC = false;
	public int speedC = 200;
	
	public void A(boolean power, boolean direction, int speed) throws InterruptedException
	{
		if((power != powerA) || (direction != directionA) || (speed != speedA))
		{
			if(power && direction)
			{
				RobotControl.getInstance().getRobotHardware().getMotorLeft().setSpeed(speed);
				RobotControl.getInstance().getRobotHardware().getMotorLeft().forward();
			}
			else if (power && !direction)
			{
				RobotControl.getInstance().getRobotHardware().getMotorLeft().setSpeed(speed);
				RobotControl.getInstance().getRobotHardware().getMotorLeft().backward();
			}
			else if (!power)
			{
				RobotControl.getInstance().getRobotHardware().getMotorLeft().stop();
			}
		powerA = power;
		directionA = direction;
		speedA = speed;
		}
		else 
		{
			Thread.sleep(1);
		}
    	
	}
	
	public void C(boolean power, boolean direction, int speed) throws InterruptedException
	{
		if((power != powerC) || (direction != directionC) || (speed != speedC))
		{
			if(power && direction)
			{
				RobotControl.getInstance().getRobotHardware().getMotorRight().setSpeed(speed);
				RobotControl.getInstance().getRobotHardware().getMotorRight().forward();
			}
			else if (power && !direction)
			{
				RobotControl.getInstance().getRobotHardware().getMotorRight().setSpeed(speed);
				RobotControl.getInstance().getRobotHardware().getMotorRight().backward();
			}
			else if (!power)
			{
				RobotControl.getInstance().getRobotHardware().getMotorRight().stop();
			}
			powerC = power;
			directionC = direction;
			speedC = speed;
			
		}
		else 
		{
			Thread.sleep(1);
		}
    	
	}
	
	
	public void setPositionAsync(final int v, final int h) {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				sliderH.setSelection(h);
				sliderV.setSelection(v);
			}			
		});
	}



	public String getDescription() {
		// TODO Auto-generated method stub
		return "Control your robot via Neo Accellerometer";
	}


	
	public String getName() {
		// TODO Auto-generated method stub
		return "MotionControl";
	}


	public void stop() {
		fireStrategyStopped();
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				shell.setVisible(false);
			}			
		});
	}
}
