/**
 * 
 */
package org.evolvis.freedroidz.strategies;

import org.evolvis.freedroidz.control.RobotControl;
import org.evolvis.freedroidz.strategy.AbstractStrategy;
import org.evolvis.freedroidz.strategy.Strategy;
import org.evolvis.freedroidz.strategy.StrategyManager;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class Splash extends AbstractStrategy {

	/**
	 * @see org.evolvis.freedroidz.strategy.Strategy#getDescription()
	 */
	public String getDescription() {
		return "A splash-player";
	}

	/**
	 * @see org.evolvis.freedroidz.strategy.Strategy#getName()
	 */
	public String getName() {
		return "Splash";
	}

//	/**
//	 * @see org.evolvis.freedroidz.strategy.Strategy#stop()
//	 */
//	public void stop() {
//		StrategyManager.getInstance().fireStrategyStopped();
//	}
//
//	/**
//	 * @see java.lang.Runnable#run()
//	 */
//	public void run() {
//		StrategyManager.getInstance().fireStrategyStarted();
//		
//		RobotControl.getInstance().getRobotHardware().getMotorExtra().setSpeed(900);
//		RobotControl.getInstance().getRobotHardware().getMotorLeft().setSpeed(900);
//		
//		RobotControl.getInstance().getRobotHardware().getMotorExtra().forward();
//		RobotControl.getInstance().getRobotHardware().getMotorLeft().forward();
//	}

	@Override
	public void startStrategy() {
		RobotControl.getInstance().getRobotHardware().getMotorExtra().setSpeed(900);
		RobotControl.getInstance().getRobotHardware().getMotorLeft().setSpeed(900);
		
		RobotControl.getInstance().getRobotHardware().getMotorExtra().forward();
		RobotControl.getInstance().getRobotHardware().getMotorLeft().forward();
		
	}

	@Override
	public void stopStrategy() {
		// Tdo nothing
		
	}

}
