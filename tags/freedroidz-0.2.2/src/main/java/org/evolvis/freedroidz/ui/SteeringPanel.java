/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.freedroidz.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.evolvis.freedroidz.Services;
import org.evolvis.freedroidz.Starter;
import org.evolvis.freedroidz.command.Command;
import org.evolvis.xana.swt.utils.SWTIconFactory;
import org.evolvis.xana.utils.IconFactoryException;

/**
 * 
 * A panel for manually steering the robot 
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class SteeringPanel extends Composite {
	
	protected Button up, left, stop, right, down, extraDown, extraUp;
	
	public static void main(String[] args) {
		Display display = new Display();
		Display.setAppName("freedroidz");
		
		try {
			SWTIconFactory.getInstance().addResourcesFolder(Starter.class, "/org/evolvis/freedroidz/gfx/");
		} catch (IconFactoryException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Shell shell = new Shell(display);
		
		new SteeringPanel(shell);
		
		shell.pack();
		shell.open();
		
		while(!shell.isDisposed())
			if (!display.readAndDispatch()) display.sleep();
		
		display.dispose();
	}

	/**
	 * Creates a new instance.
	 * 
	 * @param parent the parent container
	 */
	public SteeringPanel(Composite parent) {
		super(parent, SWT.BORDER);
		
		createGUI();
	}
	
	/**
	 * Creates the GUI components and lays them down on this Composite, using
	 * the MigLayout.
	 */
	private void createGUI() {
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 3;
		setLayout(gridLayout);
		
		GridData span3Data = new GridData();
		span3Data.horizontalSpan = 3;
		span3Data.horizontalAlignment = GridData.CENTER;
		
		SelectionListener listener = new SteeringSelectionListener();
		
		up = new Button(this, SWT.NONE);
		up.setImage((Image)SWTIconFactory.getInstance().getIcon("go-up.png"));
		up.setLayoutData(span3Data);
		up.addSelectionListener(listener);
		
		left = new Button(this, SWT.NONE);
		left.setImage((Image)SWTIconFactory.getInstance().getIcon("go-previous.png"));
		
		stop = new Button(this, SWT.NONE);
		stop.setImage((Image)SWTIconFactory.getInstance().getIcon("media-record.png"));
		
		right = new Button(this, SWT.NONE);
		right.setImage((Image)SWTIconFactory.getInstance().getIcon("go-next.png"));
		
		down = new Button(this, SWT.NONE);
		down.setImage((Image)SWTIconFactory.getInstance().getIcon("go-down.png"));
		down.setLayoutData(span3Data);
		
		Composite comp = new Composite(this, SWT.NONE);
		comp.setLayoutData(span3Data);
		
		GridLayout gridLayout2 = new GridLayout();
		gridLayout2.numColumns = 2;
		
		comp.setLayout(gridLayout2);
		
		extraDown = new Button(comp, SWT.NONE);
		extraDown.setImage((Image)SWTIconFactory.getInstance().getIcon("media-seek-backward.png"));
		
		extraUp = new Button(comp, SWT.NONE);
		extraUp.setImage((Image)SWTIconFactory.getInstance().getIcon("media-seek-forward.png"));
		
		
		this.pack();
	}
	
	private class SteeringSelectionListener implements SelectionListener {

		/**
		 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
		 */
		public void widgetDefaultSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		/**
		 * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
		 */
		public void widgetSelected(SelectionEvent event) {
			int command = 0;
			
			if(event.getSource().equals(up))
				command = SWT.ARROW_UP;
			else if(event.getSource().equals(down))
				command = SWT.ARROW_DOWN;
			else if(event.getSource().equals(left))
				command = SWT.ARROW_LEFT;
			else if(event.getSource().equals(right))
				command = SWT.ARROW_RIGHT;
			else if(event.getSource().equals(extraDown))
				command = 'y';
			else if(event.getSource().equals(extraUp))
				command = 'c';
			else if(event.getSource().equals(stop))
				command = 'x';
			
			Services.getInstance().getCommandProvider().fireCommandSent(new Command(command));
		}
		
	}
}
