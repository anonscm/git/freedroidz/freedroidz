package org.evolvis.freedroidz.strategies.mrsensitive;

public class BackwardPush extends StateMachineControlState {

	public BackwardPush(StateMachineData data) {
		super(data);
	}

	@Override
	public StateMachineControlState enter() {
		RobotHardware hardware = data.getHardware();
		hardware.move(RobotHardware.LOWER, -50,100);
		hardware.move(RobotHardware.UPPER,20,50);
		return super.enter();
	}
	@Override
	public StateMachineControlState complete(int[] i) {
		RobotHardware hardware = data.getHardware();
		
		if(hardware.isComplete(RobotHardware.LOWER)){
			return new BackwardLiftFood(data);
		}
		return super.complete(i);
	}
	
	@Override
	public boolean isMotorInfoRequired(int i) {
		return i==RobotHardware.LOWER||i==RobotHardware.UPPER;
	}
}
