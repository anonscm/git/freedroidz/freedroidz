package org.evolvis.freedroidz.strategies.mrsensitive;

public class ForwardLiftFood extends StateMachineControlState {

	public ForwardLiftFood(StateMachineData data) {
		super(data);
		
	}
	@Override
	public StateMachineControlState enter() {
		RobotHardware hardware = data.getHardware();
		hardware.moveTo(RobotHardware.UPPER, 55,200);
		hardware.moveTo(RobotHardware.LOWER, 161,100);
		
		return super.enter();
	}
	@Override
	public StateMachineControlState complete(int[] i) {
		RobotHardware hardware = data.getHardware();
		
		if(hardware.isComplete(RobotHardware.UPPER)&&hardware.isComplete(RobotHardware.LOWER))
		{
			return new Moving(data);
		}
		return super.complete(i);
	}
	@Override
	public boolean isMotorInfoRequired(int i) {
		return i==RobotHardware.UPPER||i==RobotHardware.LOWER;
	}
}
