package org.evolvis.freedroidz.strategies;

import java.util.logging.Logger;

import icommand.nxt.SensorPort;
import icommand.nxt.SoundSensor;

import org.evolvis.freedroidz.Services;
import org.evolvis.freedroidz.sensor.SensorListener;
import org.evolvis.freedroidz.strategy.AbstractStrategy;
import org.evolvis.freedroidz.strategy.Strategy;

public class SensorTest extends AbstractStrategy implements SensorListener{
	
	private boolean run = false;
	private boolean init=true;
	
	private SoundSensor sensorFront;
	private SoundSensor sensorRight;
	private SoundSensor sensorRear;
	private SoundSensor sensorLeft;

	protected final static Logger logger = Logger.getLogger(SensorTest.class.getName());
	

	@Override
	public void startStrategy() {
		Services.getInstance().getSensorProvider().addSensorListener(this);
		
	}

	@Override
	public void stopStrategy() {
		Services.getInstance().getSensorProvider().removeSensorListener(this);
		
	}
	
	
	protected void init(){
		System.out.print("initializing the sensors...");
		sensorFront = new SoundSensor(SensorPort.S1);
		sensorRight = new SoundSensor(SensorPort.S2);
		sensorRear = new SoundSensor(SensorPort.S4);
		sensorLeft = new SoundSensor(SensorPort.S3);
		System.out.println("..ready");
	}

	public String getDescription() {		
		return "This strategy simply prints the sensor values to standard out.";
	}

	public String getName() {
		return "Sensor Testing";
	}

//	public void stop() {
//		run=false;		
//	}
//
//	public void run() {
//		if (init){
//			initSensors();
//			logger.fine("SensorTesting: registering myself as SensorListener");
//			Services.getInstance().getSensorProvider().addSensorListener(this);
//		}
//		run=true;
//		while (run){
//
//		}
//	}
	
	public void pollingIntervalExpired() {
		
		int front = sensorFront.getdB();
		int right = sensorRight.getdB();
		int rear = sensorRear.getdB();
		int left = sensorLeft.getdB();
		
		int frontdBA = sensorFront.getdBA();
		int rightdBA = sensorRight.getdBA();
		int reardBA = sensorRear.getdBA();
		int leftdBA = sensorLeft.getdBA();
		
		System.out.println("");
		System.out.println("*****");
		System.out.println("front: "+front+" ,dbA: "+frontdBA);
		System.out.println("right: "+right+" ,dbA: "+rightdBA);
		System.out.println("rear: "+rear+" ,dbA: "+reardBA); 
		System.out.println("left: "+left+" ,dbA: "+leftdBA);
		System.out.println("*****");
		System.out.println("");
		
		
	}
}
