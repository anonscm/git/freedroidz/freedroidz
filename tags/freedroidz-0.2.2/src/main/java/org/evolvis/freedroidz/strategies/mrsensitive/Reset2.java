package org.evolvis.freedroidz.strategies.mrsensitive;

import icommand.nxt.Motor;

public class Reset2 extends StateMachineControlState {
	public Reset2(StateMachineData data) {
		super(data);
	}

	@Override
	public StateMachineControlState enter() {
		RobotHardware hardware = data.getHardware();
		if(hardware.isTouching(RobotHardware.LOWER)){
			return new Moving(data);
		}
		hardware.move(RobotHardware.LOWER, -90,700);
		hardware.move(RobotHardware.UPPER,-90,200);
		return null;
	}
	@Override
	public StateMachineControlState complete(int[] i) {
	RobotHardware hardware = data.getHardware();
		
	if(hardware.isComplete(RobotHardware.LOWER)){
		hardware.move(RobotHardware.LOWER, -60,600);
		hardware.move(RobotHardware.UPPER,-90,200);
	}
		return null;
	}
	@Override
	public StateMachineControlState touch(int[] i) {
		RobotHardware hardware = data.getHardware();
		
		if(hardware.isTouching(RobotHardware.LOWER)){
			
			hardware.resetTachoCount(RobotHardware.LOWER);
			hardware.resetTachoCount(RobotHardware.UPPER);
			
			hardware.stop(RobotHardware.LOWER,100);
			hardware.setSpeed(RobotHardware.LOWER,0);
			return new Idle(data);
		}
		return null;
	}
	@Override
	public boolean isMotorInfoRequired(int i) {
		return i==RobotHardware.LOWER;
	}
	@Override
	public boolean isSensorInfoRequired(int i) {
		return i==RobotHardware.LOWER;
	}

}
