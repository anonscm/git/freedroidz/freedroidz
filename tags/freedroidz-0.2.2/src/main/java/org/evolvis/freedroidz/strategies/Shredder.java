/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.freedroidz.strategies;

import java.awt.event.KeyEvent;
import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.evolvis.freedroidz.Services;
import org.evolvis.freedroidz.command.Command;
import org.evolvis.freedroidz.command.CommandListener;
import org.evolvis.freedroidz.control.RobotControl;
import org.evolvis.freedroidz.strategy.AbstractStrategy;
import org.evolvis.freedroidz.strategy.Strategy;
import org.evolvis.freedroidz.strategy.StrategyManager;

public class Shredder extends AbstractStrategy implements CommandListener{
	
	protected static final Logger logger = Logger.getLogger(Shredder.class.getName());

	boolean moving = false;
	boolean movingExtra = false;

//	public void run() {
//		logger.info("Strategy Remote Control");
//		logger.info("need to register myself to the Command Provider");
//		
//		StrategyManager.getInstance().fireStrategyStarted();
//	}
//
//	public void stop() {
//		RobotControl.getInstance().brake();
//		RobotControl.getInstance().getRobotHardware().getMotorExtra().flt();
//		Services.getInstance().getCommandProvider().removeCommandListener(this);
//		StrategyManager.getInstance().fireStrategyStopped();
//	}

	@Override
	public void startStrategy() {
		Services.getInstance().getCommandProvider().addCommandListener(this);
		RobotControl.getInstance().getRobotHardware().getMotorExtra().setSpeed(300);		
	}

	@Override
	public void stopStrategy() {
		RobotControl.getInstance().getRobotHardware().getMotorExtra().flt();
		Services.getInstance().getCommandProvider().removeCommandListener(this);
		
	}
	public String getDescription() {
		return "Strategy for 'Shredder'";
	}

	public String getName() {
		return "Shredder";
	}

	public void commandSent(Command command) {
		logger.info("processing "+command);

		int code = command.getCode();

		if(SWT.ARROW_UP == code || KeyEvent.VK_UP == code) {
			RobotControl.getInstance().driveForward(400);
			moving = true;
		}
		else if(SWT.ARROW_DOWN == code || KeyEvent.VK_DOWN == code) {
			if(moving)  {
				RobotControl.getInstance().brake();
				moving = false;
			}
			else {
				RobotControl.getInstance().driveBackward(400);
				moving = true;
			}
		}
		else if(SWT.ARROW_LEFT == code || KeyEvent.VK_LEFT == code) {
			if(moving)
				RobotControl.getInstance().driveLeftForward(100);
			else
				RobotControl.getInstance().turnLeft(5);
		}
		else if(SWT.ARROW_RIGHT == code || KeyEvent.VK_RIGHT == code) {
			if(moving)
				RobotControl.getInstance().driveRightForward(100);
			else
				RobotControl.getInstance().turnRight(5);
		} else if(120 == code) {
			if(movingExtra) {
				RobotControl.getInstance().getRobotHardware().getMotorExtra().stop();
				movingExtra = false;
			}
			else {
				RobotControl.getInstance().getRobotHardware().getMotorExtra().forward();
				movingExtra = true;
			}
		} else if(121 == code) {
			if(movingExtra) {
				RobotControl.getInstance().getRobotHardware().getMotorExtra().stop();
				movingExtra = false;
			}
			else {
				RobotControl.getInstance().getRobotHardware().getMotorExtra().backward();
				movingExtra = true;
			}
		}
		else
			logger.info("unknown command: "+command);
	}

	public void commandReleased(Command command){
		//do nothing	
	}

}