package org.evolvis.freedroidz.strategies.mrsensitive;

public class Stomp2 extends StateMachineControlState {

	private StateMachineControlState target;

	public Stomp2(StateMachineData data, StateMachineControlState target) {
		super(data);
		this.target=target;
	}

	@Override
	public StateMachineControlState enter() {
		RobotHardware hardware = data.getHardware();
		hardware.move(RobotHardware.UPPER, 15,100);
		//hardware.move(RobotHardware.LOWER, 15,100);
		return super.enter();
	}
	@Override
	public StateMachineControlState complete(int[] i) {
		RobotHardware hardware = data.getHardware();
		if(hardware.isComplete(RobotHardware.UPPER)&&!hardware.isTouching(RobotHardware.FINGER)){
			hardware.move(RobotHardware.UPPER, 15,200);	
		}
		if(hardware.isTouching(RobotHardware.FINGER)){
			return target;
		}
		return super.complete(i);
	}
	
	@Override
	public StateMachineControlState touch(int[] i) {
		RobotHardware hardware = data.getHardware();
		if(hardware.isTouching(RobotHardware.FINGER)){
			return target;
		}
		return super.touch(i);
	}
	
	@Override
	public boolean isMotorInfoRequired(int i) {
		return i==RobotHardware.LOWER||i==RobotHardware.UPPER;
	}
	@Override
	public boolean isSensorInfoRequired(int i) {
		return i==RobotHardware.FINGER;
	}
}
