/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.freedroidz.xmpp;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.evolvis.scio.Message;
import org.evolvis.scio.xmpp.StreamListener;
import org.evolvis.scio.xmpp.XMPPConnection;
import org.evolvis.scio.xmpp.XMPPFeatures;

/**
 * This class extends XMPPConnection from scio with the possibility
 * to add StreamListeners which listen on incoming messages.
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class ExtendedXMPPConnection extends XMPPConnection {
	
	protected List<StreamListener> listeners;

	public void addStreamListener(StreamListener listener) {
		getStreamListeners().add(listener);
	}
	
	public void removeStreamListener(StreamListener listener) {
		getStreamListeners().remove(listener);
	}
	
	protected List<StreamListener> getStreamListeners() {
		if(listeners == null)
			listeners = new ArrayList<StreamListener>();
		
		return listeners;
	}
	
	

	/**
	 * Calls constructor from XMPPConnection
	 */
	public ExtendedXMPPConnection() {
		super();
	}

	/**
	 * Calls constructor from XMPPConnection
	 */
	public ExtendedXMPPConnection(String arg0, String arg1, int arg2) {
		super(arg0, arg1, arg2);
	}

	/**
	 * @see org.evolvis.scio.xmpp.XMPPConnection#iqMessageRetrieved(org.evolvis.scio.Message)
	 */
	@Override
	public void iqMessageRetrieved(Message message) {
		super.iqMessageRetrieved(message);
		
		Iterator<StreamListener> it = getStreamListeners().iterator();
		
		while(it.hasNext())
			it.next().iqMessageRetrieved(message);
	}

	/**
	 * @see org.evolvis.scio.xmpp.XMPPConnection#messageRetrieved(org.evolvis.scio.Message)
	 */
	@Override
	public void messageRetrieved(Message message) {
		super.messageRetrieved(message);
		
		Iterator<StreamListener> it = getStreamListeners().iterator();
		
		while(it.hasNext())
			it.next().messageRetrieved(message);
	}

	/**
	 * @see org.evolvis.scio.xmpp.XMPPConnection#unknownMessageRetrieved(org.evolvis.scio.Message)
	 */
	@Override
	public void unknownMessageRetrieved(Message message) {
		super.unknownMessageRetrieved(message);
		
		Iterator<StreamListener> it = getStreamListeners().iterator();
		
		while(it.hasNext())
			it.next().unknownMessageRetrieved(message);
	}

	/**
	 * @see org.evolvis.scio.xmpp.XMPPConnection#xmppMessageRetrieved(org.evolvis.scio.Message)
	 */
	@Override
	public void xmppMessageRetrieved(Message message) {
		super.xmppMessageRetrieved(message);
		
		Iterator<StreamListener> it = getStreamListeners().iterator();
		
		while(it.hasNext())
			it.next().xmppMessageRetrieved(message);
	}

	/**
	 * @see org.evolvis.scio.xmpp.XMPPConnection#errorOccured(java.lang.String, java.lang.Exception)
	 */
	@Override
	public void errorOccured(String message, Exception excp) {
		super.errorOccured(message, excp);
		
		Iterator<StreamListener> it = getStreamListeners().iterator();
		
		while(it.hasNext())
			it.next().errorOccured(message, excp);
	}

	/**
	 * @see org.evolvis.scio.xmpp.XMPPConnection#featuresReceived(org.evolvis.scio.xmpp.XMPPFeatures)
	 */
	@Override
	public void featuresReceived(XMPPFeatures feautures) {
		super.featuresReceived(feautures);
		
		Iterator<StreamListener> it = getStreamListeners().iterator();
		
		while(it.hasNext())
			it.next().featuresReceived(feautures);
	}
	
	
}
