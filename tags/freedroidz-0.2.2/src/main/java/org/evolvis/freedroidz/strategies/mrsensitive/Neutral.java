package org.evolvis.freedroidz.strategies.mrsensitive;

public class Neutral extends StateMachineControlState {

	StateMachineControlState target;
	@Override
	public boolean isMotorInfoRequired(int i) {
		return i==RobotHardware.LOWER||i==RobotHardware.UPPER;
	}
	public Neutral(StateMachineData data, StateMachineControlState target) {
		super(data);
		this.target=target;
	}
	
	@Override
	public StateMachineControlState enter() {
		RobotHardware hardware = data.getHardware();
		hardware.moveTo(RobotHardware.UPPER, 46, 600);
		hardware.moveTo(RobotHardware.LOWER, 75, 600);
		return super.enter();
	}
	
	@Override
	public StateMachineControlState complete(int[] i) {
		RobotHardware hardware = data.getHardware();
		if(hardware.isComplete(RobotHardware.UPPER)&&hardware.isComplete(RobotHardware.LOWER)){
			return target;
		}
		return null;
	}
	
}
