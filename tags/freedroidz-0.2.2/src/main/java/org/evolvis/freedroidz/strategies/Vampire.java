package org.evolvis.freedroidz.strategies;

import java.awt.event.KeyEvent;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Logger;

import icommand.nxt.LightSensor;
import icommand.nxt.Motor;
import icommand.nxt.SensorPort;
import icommand.nxt.Sound;

import org.eclipse.swt.SWT;
import org.evolvis.freedroidz.Services;
import org.evolvis.freedroidz.command.Command;
import org.evolvis.freedroidz.command.CommandListener;
import org.evolvis.freedroidz.control.RobotControl;
import org.evolvis.freedroidz.events.CommandEvent;
import org.evolvis.freedroidz.sensor.SensorListener;
import org.evolvis.freedroidz.strategy.AbstractStrategy;
import org.evolvis.freedroidz.strategy.Strategy;
import org.evolvis.freedroidz.strategy.StrategyManager;


public class Vampire extends AbstractStrategy implements SensorListener, CommandListener{

	protected final static Logger logger = Logger.getLogger(Vampire.class.getName());

	private BlockingQueue<CommandEvent>  queue= new LinkedBlockingQueue<CommandEvent>();
	private LightSensor leftEye;
	private LightSensor rightEye;
	private Motor hoodMotor = Motor.A;

//	private boolean run = false;
//	private boolean init=true;	

	
	@Override
	public void startStrategy() {
		logger.fine("Vampire: registering myself as SensorListener");
		Services.getInstance().getSensorProvider().addSensorListener(this);
		Services.getInstance().getCommandProvider().addCommandListener(this);
		
	}

	@Override
	public void stopStrategy() {
		Services.getInstance().getSensorProvider().removeSensorListener(this);
		Services.getInstance().getCommandProvider().removeCommandListener(this);
		RobotControl.getInstance().brake();		
	}


//	public void stop() {
//		Services.getInstance().getSensorProvider().removeSensorListener(this);
//		Services.getInstance().getCommandProvider().removeCommandListener(this);
//		StrategyManager.getInstance().fireStrategyStopped();
//		RobotControl.getInstance().disconnect();
//		run=false;		
//	}

	protected void init(){
		leftEye = new LightSensor(SensorPort.S4);
		rightEye = new LightSensor(SensorPort.S1);
	}

//	public void run() {
//		if (init) initSensors();
//		StrategyManager.getInstance().fireStrategyStarted();
//		logger.fine("ReactOnSound: registering myself as SensorListener");
//		Services.getInstance().getSensorProvider().addSensorListener(this);
//		Services.getInstance().getCommandProvider().addCommandListener(this);
//		run=true;
//		while (run){
//		}
//
//	}
	
	private int queueBuffer = 6;
	private boolean occupied=false;
	int interval=20;
	int counter=0;
	int rotate=60;
	int actionPercentage = 70;

	public void pollingIntervalExpired() {

		//System.out.println("...");

		if (!occupied) {
			//not occupied: check light sensors for bright light
			int left = leftEye.getLightPercent();
			int right = rightEye.getLightPercent();

			if ((left>actionPercentage)||(right>actionPercentage)){
				doHoodAction();
			} else {
				//if no bright light detected: process key command			
				try {
					if (!queue.isEmpty()) {
						CommandEvent event = queue.take();
						processCommand(event);
					}
				} catch (InterruptedException e) {
					logger.warning("interrupted exception detected: "+e.getMessage());
				}
			}

		}
		else if (occupied) {
			System.out.println("counter: "+counter);
			if (counter<interval){
				counter = counter +1;
				moveRandomly();
			}
			else{
				System.out.println("reset counter");
				getBackToNormal(rotate);

				counter = 0;
			}
		} 
	}

	private void getBackToNormal(int rotationLeft){
		moveHoodMotor(rotationLeft);
		activateEyes();
		occupied=false;
	}

	private void doHoodAction(){
		playSound();
		occupied=true;
		moveHoodMotor(-rotate);
		passivateEyes();
		RobotControl.getInstance().turnLeft(180);		
	}

	
	private void moveRandomly(){
		
		playRandomSound();
		Random random = new Random();
		int randomNumber = random.nextInt(3);

		switch (randomNumber){
		case 0:
			RobotControl.getInstance().driveForward(maximalSpeed/2);
			break;
		case 1:
			RobotControl.getInstance().driveLeftForward(30);
			break;
		case 2:	
			RobotControl.getInstance().driveRightForward(30);
			break;
		}


	}


	/**
	 * processes a key command
	 */

	private int maximalSpeed=900;
	private int currentSpeedStep =0;
	private int stepCount = 5;
	private int turningDegrees = 25;
	private int turnWhileMoving = 40;

	private boolean turning=false;

	private int getActualSpeed(){
		return maximalSpeed/stepCount*currentSpeedStep;
	}

	private void driveForOrBackward(int speed){
		if (speed>0) RobotControl.getInstance().driveForward(speed);
		else if (speed<0) RobotControl.getInstance().driveBackward(-speed);
		else RobotControl.getInstance().brake();
	}


	private synchronized void processCommand(CommandEvent event){

		if (event.isReleased())return;

		switch (event.getCommand().getCode()) {
		case SWT.ARROW_UP:
		case KeyEvent.VK_UP:
			if ((currentSpeedStep<stepCount)&& (!turning))currentSpeedStep = currentSpeedStep+1;
			driveForOrBackward(getActualSpeed());
			turning=false;
			break;
		case SWT.ARROW_DOWN:
		case KeyEvent.VK_DOWN:
			if ((currentSpeedStep>-stepCount)&& (!turning))currentSpeedStep = currentSpeedStep-1;
			driveForOrBackward(getActualSpeed());
			turning=false;
			break;
		case SWT.ARROW_LEFT:
		case KeyEvent.VK_LEFT:
			if (currentSpeedStep>0){
				RobotControl.getInstance().driveLeftForward(turnWhileMoving);
				turning=true;
			} else if (currentSpeedStep<0){
				RobotControl.getInstance().driveLeftBackward(turnWhileMoving);
				turning=true;
			} else {
				RobotControl.getInstance().turnLeft(turningDegrees);
			}
			break;
		case SWT.ARROW_RIGHT:
		case KeyEvent.VK_RIGHT:
			if (currentSpeedStep>0){
				RobotControl.getInstance().driveRightForward(turnWhileMoving);
				turning=true;
			} else if (currentSpeedStep<0){
				RobotControl.getInstance().driveRightBackward(turnWhileMoving);
				turning=true;
			} else {
				RobotControl.getInstance().turnRight(turningDegrees);
			}
			break;
		}
	}
	

	public String getDescription() {
		return "the ultimate vampire strategy\n"+
		"LightSensors at port S4 and S3!";
	}

	public String getName() {
		return "Vampire";
	}


	//--------- helper methods ----------

	//he's frightened and you can hear that
	private void playSound(){
		Sound.playTone(700,300);
		Sound.playTone(1400,500 );
		Sound.playTone(1200,300 );

	}
	
	private void playRandomSound(){
		Random random = new Random();
		int randomNumber = random.nextInt(7);
		Sound.playTone(500+randomNumber*100,300);
		//Sound.playTone(,500 );
	}

	private void moveHoodMotor(int rotation){
		//hoodMotor.setSpeed(900);
		hoodMotor.rotate(rotation);
		hoodMotor.stop();

	}

	/**
	 * Turns the LightSensors on 
	 */
	private void activateEyes(){
		leftEye.activate();
		rightEye.activate();
	}

	/**
	 * Turns the LightSensors off
	 */
	private void passivateEyes() {
		leftEye.passivate();
		rightEye.passivate();
	}


	//------------------- command events --------------------------

	public void commandSent(Command command) {
		if (queue.size()<queueBuffer){
			queue.add(new CommandEvent(command,false));
		}
	}	

	public void commandReleased(Command command) {
		queue.add(new CommandEvent(command,true));
	}

}
