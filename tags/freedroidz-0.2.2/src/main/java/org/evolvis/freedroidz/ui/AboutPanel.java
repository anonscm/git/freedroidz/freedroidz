/**
 * 
 */
package org.evolvis.freedroidz.ui;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

/**
 * 
 * This is a simple SWT-Panel which shows some information (like version-number).
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class AboutPanel extends Composite {
	
	protected Shell parent;
	
	/**
	 * Creates a new instance.
	 * 
	 * @param parent the parent container
	 */
	public AboutPanel(Shell parent) {
		super(parent, SWT.BORDER);
		this.parent = parent;
		createGUI();
	}
	
	/**
	 * Creates the GUI components and lays them down on this Composite, using
	 * the MigLayout.
	 */
	private void createGUI() {
		setLayout(new MigLayout("fillx, wrap 1", "[] [grow, fill]"));
		
		Label versionLabel = new Label(this, SWT.NONE);
		
		// FIXME: version-string should be auto-generated using project-version in pom 
		versionLabel.setText("freedroidz 0.3.0");
		
		
		Button closeButton = new Button(this, SWT.NONE);
		closeButton.setText("Close");
		closeButton.addSelectionListener(new SelectionListener() {

			/**
			 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			/**
			 * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			public void widgetSelected(SelectionEvent arg0) {

				parent.close();
				
			}
			
		});
	}	
}
