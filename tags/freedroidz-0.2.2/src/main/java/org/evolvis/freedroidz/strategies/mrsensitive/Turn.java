package org.evolvis.freedroidz.strategies.mrsensitive;

public class Turn extends StateMachineControlState {

	private int turn;

	public Turn(StateMachineData data, int turn) {
		super(data);
		this.turn = turn;
	}
	
	@Override
	public StateMachineControlState enter() {
		
		return new Neutral(data,new TurnMoveTorso(data,turn));
	}
	
	
	
}
