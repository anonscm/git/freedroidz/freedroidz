package org.evolvis.freedroidz.strategies;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Map {

	private List<Point> points = new LinkedList<Point>();
	
	private List<double[]> linePoints = new LinkedList<double[]>();

	private boolean run = true;
	
	private Thread thread;
	
	public Map() {
		startThread();
	}
	
	public void addPoint(double[] p, double[] x) {
		if (p.length != 2)
			throw new RuntimeException("point must be two dimensional");
		points.add(new Point(p, x));
	}
	
	private List<CenterPoint> cluster(int start, int n, int k) {
		Random r = new Random();
		if (k > n)
			throw new RuntimeException("too many clusters");
		List<CenterPoint> c = new ArrayList<CenterPoint>(k); // cluster centers
		int[] a = new int[n-start]; // assignment: point -> cluster
		for (int i = 0; i < n-start; i ++)
			a[i] = -1; // no assignment yet
		// get random cluster centers
		for (int i = 0; i < k; i ++) {
			Point p;
			boolean dup;
			do {
				p = points.get(r.nextInt(n-start)+start);
				dup = false;
				for (int j = 0; j < i; j ++)
					if (p.equals(c.get(j))) {
						dup = true;
						break;
					}
			} while (dup);
			c.add(new CenterPoint(p));
		}
		boolean changed = false;
		while (true) {
			// create cluster assignment
			for (int i = 0; i < n-start; i ++) {
				Point p = points.get(start+i);
				double min = Double.MAX_VALUE;
				int minC = -1;
				for (int j = 0; j < k; j ++) {
					double d = p.getDistance(c.get(j));
					if (d < min) {
						min = d;
						minC = j;
					}
				}
				if (a[i] != minC) {
					a[i] = minC;
					changed = true;
				}
			}
			if (!changed) {
				Collections.sort(c);
				return c;
			}
			changed = false;
			// recalculate cluster centers

			for (int j = 0; j < k; j ++) {
				c.get(j).clear();
				int l = 0;
				for (int i = 0; i < n-start; i ++) {
					if (a[i] == j) {
						Point p = points.get(start+i);
						c.get(j).add(p);
						l++;
					}
				}
				if (l != 0) {
					CenterPoint d = c.get(j);
					d.divide(l);
					for (int m = 0; m < j; m ++)
						if (d.equals(c.get(m))) {
							l=0;
							break;
						}
				} 
				if (l == 0) { // empty or duplicate cluster => similar as above
					Point p;
					boolean dup;
					do {
						p = points.get(start+r.nextInt(n-start));
						dup = false;
						for (int m = 0; m < k; m ++)
							if (j != m && p.equals(c.get(m))) {
								dup = true;
								break;
							}
					} while (dup);
					c.get(j).set(p);
					changed = true;
				}
			}
		}
	}
	
	private static double[] getIntersectionPoint(double[] line1, double[] line2) {
		double[] p = new double[2];
		p[0] = line1[1]*line2[2]-line1[2]*line2[1];
		p[1] = line1[2]*line2[0]-line1[0]*line2[2];
		double f = line1[0]*line2[1]-line1[1]*line2[0];
		if (f == 0) //TODO solve this
			throw new RuntimeException("illegal point");
		p[0] /= f;
		p[1] /= f;
		return p;
	}

	public List<Point> getPoints() {
		return points;
	}

	public List<double[]> getLinePoints() {
		return linePoints;
	}
	
	private static final int EXAMINE_POINT_COUNT = 30;
	
	private double[] buffer = null;
	
	public double getMinimalFrontDistance(double[] x) {
		int end = points.size();
		int i = EXAMINE_POINT_COUNT < end ? end - EXAMINE_POINT_COUNT : 0;
		double min = Double.MAX_VALUE;
		for (; i < end; i ++) {
			Point p = points.get(i);
			buffer = rix(buffer, new double[] {p.getX(), p.getY()}, x);
			if (Math.abs(buffer[1]) < 10) // 20 cm viewfield width
				if (buffer[0] > 0 && buffer[0] < min)
					min = buffer[0];
		}
		return min;
	}
	
	public double getMinimalLeftDistance(double[] x) {
		int end = points.size();
		int i = EXAMINE_POINT_COUNT < end ? end - EXAMINE_POINT_COUNT : 0;
		double min = Double.MAX_VALUE;
		for (; i < end; i ++) {
			Point p = points.get(i);
			buffer = rix(buffer, new double[] {p.getX(), p.getY()}, x);
			if (buffer[0] > 0 && buffer[0] < 20) // viewfield: 0 - 20
				if (buffer[1] > 0 && buffer[1] < min)
					min = buffer[1];
		}
		return min;
	}

	

	private double[] calculateLine(List<Point> points) {
		return calculateLine(points, 0, points.size());
	}

	private double[] calculateLine(List<Point> points, int start, int n) {
		double[] line = new double[3];
		if (n < 2)
			throw new RuntimeException("two points are needed");
		if (n == 2) { //redundand
			Point p1 = points.get(start);
			Point p2 = points.get(start + 1);
			line[0] = p1.getY() - p2.getY();
			line[1] = p2.getX() - p1.getX();
			line[2] = p1.getX()*p2.getY() - p1.getY()*p2.getX();
		} else { //Ausgleichung TODO: kann man vielleicht noch eleganter programmieren
			//Mittelwerte berechnen
			double[] m=new double[] {0,0};
			Point t;
			for (int i=0; i<n; i++) {
				t = points.get(start+i);
				m[0]+=t.getX();
				m[1]+=t.getY();
			}
			m[0]/=n;
			m[1]/=n;
			//
			double t2;
			double[] b=new double[] {0,0};
			for (int i=0; i<n; i++) {
				t = points.get(start+i);
				t2=t.getX()-m[0];
				b[0]+=t2*(t.getY()-m[1]);
				b[1]+=t2*t2;
			}
			if (b[1]==0) { //alle x-Werte gleich ?
				line[0]=-1;
				line[1]=0;
				line[2]=m[0];
			} else {
				line[0]= b[0]/b[1];
				line[1]=-1;
				line[2]=m[1]-line[0]*m[0];
			}
		}
		if (line[0]==0 && line[1]==0)
			throw new RuntimeException("ungültige Linie");
		return line;
	}
	
    // rix = (xra px xx - * xrb py xy - * +;xrb ~ px xx - * xra py xy - * +)
    private static double[] rix(double[] buffer, double[] p, double[] x) {
        if (buffer == null)
            buffer = new double[2];
        buffer[0] = x[2]*(p[0]-x[0])+x[3]*(p[1]-x[1]);
        buffer[1] = -x[3]*(p[0]-x[0])+x[2]*(p[1]-x[1]);
        return buffer;
    }

	public void clear() {
		run = false;
		while (thread != null)
			; // wait till thread is stopped
		points.clear();
		linePoints.clear();
		startThread();
	}

	private void startThread() {
		if (1 == 1)
			return;
		run = true;
		thread = new Thread(new Runnable() {
			public void run() {
				int lastn = 0;
				while (run) {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					int n = points.size();
					if (n -lastn-10 < 30)
						continue;
					
					List<double[]> linePoints2 = new LinkedList<double[]>();
					
					int k = (n-lastn-10)/8;
					List<CenterPoint> cps = cluster(lastn, n-10, k);
					for (int i = 0; i < cps.size(); i ++) {
						List<Point> mmbs = cps.get(i).getMembers();
						if (mmbs.size() >= 2) {

							double[] line = calculateLine(mmbs);
							double maxx = Double.MIN_VALUE;
							double minx = Double.MAX_VALUE;
							
							for (int j = 0; j < mmbs.size(); j ++) {
								Point pnt = mmbs.get(j);
								double x = line[0]*line[1]/(line[0]*line[0]+line[1]*line[1]);
								x *= line[1]/line[0]*pnt.x-pnt.y-line[2]/line[1];
								if (x > maxx)
									maxx = x;
								if (x < minx)
									minx = x;
							}

							double miny = (line[0]*minx + line[2])/-line[1];
							double maxy = (line[0]*maxx + line[2])/-line[1];
							linePoints2.add(new double[] {minx, miny});
							linePoints2.add(new double[] {maxx, maxy});
						}
					}
					lastn += n;
					linePoints.addAll(linePoints2);
				}
				thread = null;
			}
		});
		thread.start();
	}
}

/**
 * immutable 2D point representation.
 */
class Point {

	protected double x;
	protected double y;
	protected double angle;
	protected double time;
	
	private static final long startTime = System.currentTimeMillis();

	protected Point(double x, double y, double angle, double time) {
		this.x = x;
		this.y = y;
		this.angle = angle;
		this.time = time;
	}
	
	
	public Point(double[] p, double[] s) {
		if (p.length != 2)
			throw new RuntimeException("point must be two dimensional");
		x = p[0];
		y = p[1];
		angle = Math.atan2(y-s[1], x-s[0]) * 180/Math.PI;
		time = (System.currentTimeMillis() - startTime) / 1000;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getAngle() {
		return angle;
	}

	public double getTime() {
		return time;
	}

	private static final double WEIGHT_ANGLE = 1;
	private static final double WEIGHT_TIME = 0;
	
	public double getDistance(Point p) {
		double dist = modAngle(p.angle-angle);
		dist *= dist * WEIGHT_ANGLE;
		dist += (p.x-x)*(p.x-x)+(p.y-y)*(p.y-y);
		dist += WEIGHT_TIME*(p.time-time)*(p.time-time);
		return Math.sqrt(dist);
	}
	
	public double getDistance(double[] c) {
		double dist = modAngle(c[2]-angle);
		dist *= dist * WEIGHT_ANGLE;
		dist += (c[0]-x)*(c[0]-x)+(c[1]-y)*(c[1]-y);
		dist += WEIGHT_TIME*(c[3]-time)*(c[3]-time);
		return Math.sqrt(dist);
	}

	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(angle);
		result = PRIME * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(time);
		result = PRIME * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(x);
		result = PRIME * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = PRIME * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (obj.getClass() == double[].class) {
			final double[] c = (double[]) obj;
			return x == c[0] && y == c[1];
		} else if (getClass() == obj.getClass()) {
			final Point other = (Point) obj;
			return x == other.x && y == other.y;
		} else
			return false;
	}


	
	private static double modAngle(double a) {
		while (a > 180)
			a -= 360;
		while (a < -180)
			a += 360;
		return a;
	}
}

class CenterPoint extends Point implements Comparable<CenterPoint> {
	
	private List<Point> members = new LinkedList<Point>();

	public CenterPoint() {
		super(0, 0, 0, 0);
	}

	public CenterPoint(Point p) {
		super(p.getX(), p.getY(), p.getAngle(), p.getTime());
	}
	
	public void set(Point p) {
		x = p.x;
		y = p.y;
		angle = p.angle;
		time = p.time;
	}

	public void clear() {
		x = 0;
		y = 0;
		angle = 0;
		time = 0;
		members.clear();
	}

	public void add(Point p) {
		x += p.x;
		y += p.y;
		angle += p.angle;
		time += p.time;
		members.add(p);
	}
	
	public List<Point> getMembers() {
		return members;
	}

	public void divide(int n) {
		x /= n;
		y /= n;
		angle /= n;
		time /= n;
	}

	public int compareTo(CenterPoint o) {
		return time < o.time ? -1 : time == o.time ? 0 : 1;
	}
}
