
package org.evolvis.freedroidz.strategies;

import org.evolvis.freedroidz.control.RobotControl;
import org.evolvis.freedroidz.strategy.AbstractStrategy;
import org.evolvis.freedroidz.strategy.StrategyManager;

/**
 * 
 * 
 * @author Pascal Puth (p.puth@tarent.de) tarent GmbH Bonn
 *
 */
public class DriveSquare extends AbstractStrategy {

	/**
	 * @see org.evolvis.freedroidz.strategy.Strategy#getDescription()
	 */
	public String getDescription() {
		return "Drives the robot along a square";
	}

	/**
	 * @see org.evolvis.freedroidz.strategy.Strategy#getName()
	 */
	public String getName() {
		return "Drive Square";
	}

	/**
	 * @see org.evolvis.freedroidz.strategy.Strategy#stop()
	 */
	public void stop() {
		// first stop the robot
		RobotControl.getInstance().brake();
		
		// then release the motors
		RobotControl.getInstance().rollOut();
		
		fireStrategyStopped();
	}

	/**
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		fireStrategyStarted();
		
		RobotControl.getInstance().driveForward(300);
		RobotControl.getInstance().turnRight(90);
		RobotControl.getInstance().driveForward(300);
		RobotControl.getInstance().turnRight(90);
		RobotControl.getInstance().driveForward(300);
		RobotControl.getInstance().turnRight(90);
		RobotControl.getInstance().driveForward(300);
		
		fireStrategyFinished();
	}
	

}
