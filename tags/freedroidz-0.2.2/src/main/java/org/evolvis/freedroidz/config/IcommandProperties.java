/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.freedroidz.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * 
 * Class for writing properties to the icommand-config file "icommand.properties"
 * 
 * Contains code from the icommand-library.
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn; Original author: iCommand Developers (http://sourceforge.net/projects/nxtcommand/)
 */
public class IcommandProperties extends Properties {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6948923388940970476L;
	
	protected static final Logger logger = Logger.getLogger(IcommandProperties.class.getName());

	public static final String COM_PORT = "nxtcomm";
	public static final String COM_TYPE = "nxtcomm.type";
	public static final String BT_ADDRESS = "nxt.btaddress";

    private static final String PROPSFNAME = "icommand.properties";
	private static final String UDIR = System.getProperty("user.dir");
	private static final String UHOME = System.getProperty("user.home");
	private static final String FSEP = System.getProperty("file.separator");
	
	public static final String TYPE_RXTX = "rxtx";
	
	public static final String TYPE_BLUEZ = "bluez";
	
	public static final String TYPE_BLUECOVE = "bluecove";
	
	public static final String TYPE_SUN = "sun";
	
	
	/**
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * 
	 */
	public IcommandProperties(String btAddress) throws FileNotFoundException, IOException {
		super();
		
		/* Create properties with defaults for appropriate OS */
		writeDefaults(btAddress);
		
		store();
	}
	
	public void store() {
		File file = new File(UDIR + FSEP + PROPSFNAME);
		if(!file.exists())
			file = new File(UHOME + FSEP + PROPSFNAME);
		
		logger.info("Writing bt-address to file "+file.getAbsolutePath());
		
		try {
			file.createNewFile();
			
			// NOTE: Default settings might not be stored?
			store(new FileOutputStream(file), "Auto-generated iCommand Properties file by freedroidz");
			
		} catch(IOException e) {
			System.out.println(e);
		}
	}
	
	/**
	 * This creates a Properties object with some default settings.
	 * The Properties class will resort to using default settings
	 * if a setting is blank.
	 */
	private void writeDefaults(String btAddress) {
		
		String os = System.getProperty("os.name");
    	boolean windows = false;
    	boolean mac = false;
    	
    	// Search for different OS's
    	if (os.length() >= 7 && os.substring(0,7).equals("Windows"))
    		windows = true;
    	if (os.length() >= 3 && os.substring(0,3).equals("Mac"))
    		mac = true;
    	
    	// Set appropriate default solution for OS
		if (windows) {
			setProperty(COM_TYPE, TYPE_BLUECOVE);
		} else if (mac){
			setProperty(COM_TYPE, TYPE_RXTX);
		} else {
			setProperty(COM_TYPE, TYPE_BLUEZ);
		}
		
		// Write bt-address
		String checkedBtAddress = checkAndFormatBtAddress(btAddress);
		
		if(checkedBtAddress != null)
			setProperty(BT_ADDRESS, checkedBtAddress);
	}
	
	/**
	 * Checks if a Bluetooth-address has a valid form (TODO: Find definition)
	 * and evtl. formats it.
	 * 
	 * @param btAddress The BT-Address to be checked and formatted
	 * @return the formatted BT-Address or null if btAddress is invalid
	 */
	public static String checkAndFormatBtAddress(String btAddress) {
		// TODO implement
		return btAddress;
	}
	
	public static void removeConfiguration() {
		try {
			File file = new File(UDIR + FSEP + PROPSFNAME);
			if(file.exists())
				file.delete();
			
			file = new File(UHOME + FSEP + PROPSFNAME);
			if(file.exists())
				file.delete();
		} catch(Exception excp) {
			logger.warning(excp.getMessage());
		}
	}
}
