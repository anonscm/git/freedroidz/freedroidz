/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.freedroidz.strategies;

import org.evolvis.freedroidz.control.RobotControl;
import org.evolvis.freedroidz.strategy.AbstractStrategy;
import org.evolvis.freedroidz.strategy.Strategy;
import org.evolvis.freedroidz.strategy.StrategyManager;

/**
 * 
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class Turn360Left extends AbstractStrategy {

	public void startStrategy() {
		//do nothing, special case, no infinite loop needed	
	}

	public void stopStrategy() {
		//do nothing, special case, no infinite loop needed	
		
	}
	/**
	 * @see org.evolvis.freedroidz.strategy.Strategy#getDescription()
	 */
	public String getDescription() {
		return "Turns the robot left by 360 degrees";
	}

	/**
	 * @see org.evolvis.freedroidz.strategy.Strategy#getName()
	 */
	public String getName() {
		return "Turn 360";
	}

	/**
	 * @see org.evolvis.freedroidz.strategy.Strategy#stop()
	 */
	public void stop() {
		// first stop the robot
		RobotControl.getInstance().brake();
		
		// then release the motors
		RobotControl.getInstance().rollOut();
		
		fireStrategyStopped();
	}

	/**
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		fireStrategyStarted();
		
		RobotControl.getInstance().setSpeed(100);
		RobotControl.getInstance().turnLeft(360);
		
		fireStrategyFinished();
	}



}
