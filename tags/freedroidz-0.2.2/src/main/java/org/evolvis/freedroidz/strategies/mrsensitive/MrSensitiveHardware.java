package org.evolvis.freedroidz.strategies.mrsensitive;

import icommand.nxt.Motor;
import icommand.nxt.SensorPort;
import icommand.nxt.TouchSensor;
import icommand.nxt.UltrasonicSensor;

import java.util.BitSet;
import java.util.Vector;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.evolvis.freedroidz.command.Command;
import org.evolvis.freedroidz.command.CommandListener;

public class MrSensitiveHardware implements RobotHardware,CommandListener {
	private static class _Event{
		Command cmd;
		boolean released;
	}

	
	
	private BlockingQueue<_Event>  queue= new LinkedBlockingQueue<_Event>();
	private TouchSensor[] sensors = new TouchSensor[3];
	private UltrasonicSensor distanceSensor;
	private Motor[] motors = new Motor[3];
	private boolean[] wasTouching = new boolean[3];
	private boolean[] wasComplete = new boolean[3];
	
	//cache for sensor data. cleared before each callback.
	private Boolean[] moving = new Boolean[3];
	private Integer[] position = new Integer[3];
	private Boolean[] touching = new Boolean[3];
	
	//"write cache" for motor data. flushed at the end of each callback.
	private Integer[] speed = new Integer[3];
	private Integer[] move = new Integer[3];
	private boolean[] reset = new boolean[3];
	private boolean[] stop = new boolean[3];
	public MrSensitiveHardware() {
		initSensors();
		initMotors();
	}
	
	private void initSensors() {
		sensors[FINGER] = new TouchSensor(SensorPort.S1);
		sensors[UPPER] = new TouchSensor(SensorPort.S2);
		sensors[LOWER] = new TouchSensor(SensorPort.S3);
		distanceSensor = new UltrasonicSensor(SensorPort.S4);
	}
	
	private void initMotors() {
		motors[BODY] = Motor.A;
		motors[UPPER] = Motor.B;
		motors[LOWER] = Motor.C;
		
		for(int i=0;i<3;i++){
			motors[i].setSpeed(0);
			motors[i].flt();
			
		}
	}
	
	
	public void cleanup(){
		for (int i = 0; i < motors.length; i++) {
			motors[i].flt();
		}
	}
	
	public void dispatch(RobotControl control, long time) {
		clearSensorCache();
		checkComplete(control);
		checkQueue(control);
		checkTouching(control);
		flushMotorCache();
	}
	
	private void flushMotorCache() {
		for(int i=0;i<3;i++){
			Motor motor = motors[i];
			if(reset[i]){
				motors[i].resetTachoCount();
				reset[i]=false;
			}
			if(speed[i]!=null){
				motors[i].setSpeed(speed[i]);
				speed[i]=null;
			}
			if(stop[i]){
				motors[i].stop();
				stop[i]=false;
			}
			if(move[i]!=null){
				motor.rotate(move[i], true);
				move[i]=null;
			}
			
		}
	}

	private void clearSensorCache() {
		for(int i=0;i<3;i++){
			moving[i]=null;
			position[i]=null;
			touching[i]=null;
		}
	}

	private void checkQueue(RobotControl control) {
		if(!queue.isEmpty()){
			_Event e = queue.remove();
			if(e.released){
				control.keyUp(e.cmd.getCode());
			}else{
				control.keyDown(e.cmd.getCode());
			}
		}
	}
	public void commandSent(Command command) {
		//System.err.println("enque");
		_Event e = new _Event();
		e.cmd=command;
		e.released=false;
		queue.add(e);
		

	}
	
	public void commandReleased(Command command) {
		_Event e = new _Event();
		e.cmd=command;
		e.released=true;
		queue.add(e);
		

	}
	
	

		
	private void checkTouching(RobotControl control) {
		StringBuffer sb = new StringBuffer();
		sb.append("Sensors: ");
		BitSet touched = new BitSet(3);
		BitSet released = new BitSet(3);
		for(int i=0;i<3;i++){
			if(!control.isSensorInfoRequired(i)){
				continue;
			}
			boolean touching = isTouching(i);
			if(i>0){
				sb.append(", ");
			}
			sb.append(touching);
			if(touching && !wasTouching[i]){
				touched.set(i);
			}
			if(!touching && wasTouching[i]){
				released.set(i);
			}
			wasTouching[i]=touching;
		}
		if(released.cardinality()>0){
			int[] r = new int[released.cardinality()];
			int x=0;
			for (int i = 0; i < r.length; i++) {
				x=r[i]=released.nextSetBit(x);
			}
			control.release(r);
		}
		if(touched.cardinality()>0){
			int[] r = new int[touched.cardinality()];
			int x=0;
			for (int i = 0; i < r.length; i++) {
				x=r[i]=touched.nextSetBit(x);
			}
			control.touch(r);
		}
		//System.err.println(sb.toString());
		
	}

	private void checkComplete(RobotControl control) {
		Vector<Integer> justCompleted = new Vector<Integer>();
		for(int i=0;i<3;i++){
			if(!control.isMotorInfoRequired(i)){
				continue;
			}
			//boolean complete = raddist(motors[i].getTachoCount(),desiredPosition[i])<=EPSILON;
			boolean complete = isComplete(i);
			boolean wasComplete = this.wasComplete[i];
			this.wasComplete[i]=complete;
			if(complete && !wasComplete){
				justCompleted.add(i);
			}
		}
		if(justCompleted.isEmpty()){
			return;
		}
		int[] r = new int[justCompleted.size()];
		for (int i = 0; i < r.length; i++) {
			r[i]=justCompleted.get(i);
		}
		control.complete(r);
	}
	public int getPosition(int i){
		if(position[i]==null){
			position[i]=radnorm(motors[i].getTachoCount());
		}
		return position[i];
	}
	
	public int getBodyPosition() {
		return getPosition(BODY);
	}

	

	public int getLowerArmPosition() {
		
		return getPosition(LOWER);
	}

	
	public int getUpperArmPosition() {
		return getPosition(UPPER);
	}

	

	
	static int contain(int a, int b, int v){
		int l = b-a;
		int d = v-a;
		int mod = mymod(d,l);
		int result = a+mod;
		return result;
	}

	static int mymod(int i, int j) {
		if(i* j<0){
			return (j+(i%j))%j;
		}
		return i%j;
	}
	
	static int radnorm(int v){
		return contain(-1800,1800,v);
	}

	static int raddiff(int a, int b){
		return radnorm(a-b);
	}
	
	static int raddist(int a, int b){
		return Math.abs(raddiff(a,b));
	}

	public boolean isTouching(int i){
		if(touching[i]==null){
			touching[i]=sensors[i].isPressed();
		}
		return touching[i];
	}

	public void move(int i, int deg,int speed) {
		wasComplete[i]=false;
		this.speed[i]=speed;
		this.move[i]=deg;
		
	}

	public boolean isComplete(int i) {
		if(moving[i]==null){
			moving[i]=motors[i].isMoving();
		}
		return !moving[i];
	}

	public void moveTo(int i, int deg,int speed) {
		move(i,deg-getPosition(i),speed);
	}

	public void setSpeed(int i,int speed) {
		this.speed[i]=speed;		
	}

	public void stop(int i, int speed) {
		this.speed[i]=speed;
		this.stop[i]=true;
	}

	public void resetTachoCount(int i) {
		reset[i]=true;
		
	}

	public int getDistance() {
		return distanceSensor.getDistance();
	}
}
