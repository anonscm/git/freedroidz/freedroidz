/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.freedroidz.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import org.evolvis.freedroidz.Services;

/**
 * 
 * This class is responsible for managing available robots-configurations
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class RobotManager {

	protected static final Logger logger = Logger.getLogger(RobotManager.class.getName());
	
	protected File configFile = new File(System.getProperty("user.home") + File.separator + ".freedroidz" + File.separator + "robot.properties");
	
	protected static RobotManager instance;
	protected Map<String, RobotProperties> robots;
	protected RobotProperties activeRobot;
	
	protected RobotManager() {
		
	}
	
	public static RobotManager getInstance() {
		if(instance == null)
			instance = new RobotManager();
		
		return instance;
	}
	
	public RobotProperties getActiveRobot() {
		if(activeRobot == null) {

			try {
				logger.info("Loading Robot-configuration from "+configFile.getAbsolutePath());
				activeRobot = new RobotProperties(configFile);
			} catch (IOException e) {
				logger.warning("Could not find robot-configuration in "+configFile.getAbsolutePath()+". Using default values.");
			}
		}
		
		return activeRobot;
	}
	
	public void setActiveRobot(String robotName) {
		if(!getAvailableRobots().containsKey(robotName))
			throw new RuntimeException("Unknown robot \""+robotName+"\"");
		
		activeRobot = getAvailableRobots().get(robotName);
		
		updateIcommandConfiguration();
	}
	
	public void setActiveRobot(RobotProperties robot) {
		if(!getAvailableRobots().values().contains(robot))
			addRobot(robot);
		
		activeRobot = robot;
		updateIcommandConfiguration();
	}
	
	public void addRobot(RobotProperties robot) {
		getAvailableRobots().put(robot.getProperty(RobotProperties.ROBOT_NAME), robot);
	}
	
	public Map<String, RobotProperties> getAvailableRobots() {
		if(robots == null)
			robots = new HashMap<String, RobotProperties>();
		
		return robots;
	}
	
	public void scanConfigurations(File[] searchDirs) {
		if(searchDirs == null) {
			logger.warning("parameter 'searchDirs' is null");
			return;
		}
				
		for(int i=0; i < searchDirs.length; i++) {
			File dir = searchDirs[i];
			
			if(dir.isDirectory()) {
				File[] files = dir.listFiles();
				
				for(int z=0; z < files.length; z++){
					if (files[z].getName().endsWith("properties")) tryToParseAndAdd(files[z]);
				}
				
			} else if(dir.isFile())
				tryToParseAndAdd(dir);
		}
	}
	
	public void tryToParseAndAdd(File configFile) {
		try {
			RobotProperties robot = new RobotProperties(configFile);
			addRobot(robot);
		} catch (IOException e) {
			logger.warning("Could not parse robot-config-file: "+configFile.getAbsolutePath());
		}
	}
	
	public String[] getRobotNames() {
		Set<String> robotNames = getAvailableRobots().keySet();
		String[] array = new String[robotNames.size()];
		return robotNames.toArray(array);
	}
	
	public void updateIcommandConfiguration() {
		// Ensure that the icommand.properties-file contains the correct data
		String btAddress = getActiveRobot().getProperty(RobotProperties.NXT_BTADDRESS);
		
		if(btAddress == null) {
			Services.getInstance().showWarning("Robot is not configured yet.\nPlease select File -> Configure Robot in menu");
			return;
		}
			
			
		try {
			new IcommandProperties(btAddress);
		} catch (Exception e) {
			throw new RuntimeException("Could not store icommand.properties");
		}
	}
	
	public void storeConfiguration() {
		logger.info("Storing Robot-configuration to "+configFile.getAbsolutePath());
		
		if(getActiveRobot() == null) {
			logger.info("Robot is not configured. Not storing configuration.");
			return;
		}
		
		if(!configFile.isFile()) {
			try {
				configFile.getParentFile().mkdirs();
				configFile.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		try {
			getActiveRobot().store(new FileOutputStream(configFile), "Robot-Configuration for freedroidz");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
