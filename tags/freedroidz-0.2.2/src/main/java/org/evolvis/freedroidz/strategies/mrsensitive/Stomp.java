package org.evolvis.freedroidz.strategies.mrsensitive;

public class Stomp extends StateMachineControlState {

	private StateMachineControlState target;

	public Stomp(StateMachineData data,StateMachineControlState target) {
		super(data);
		this.target = target;
	}
	@Override
	public StateMachineControlState enter() {
		RobotHardware hardware = data.getHardware();
		hardware.moveTo(RobotHardware.UPPER, 68,500);
		hardware.moveTo(RobotHardware.LOWER, 123,500);
		return null;
	}
	
	@Override
	public StateMachineControlState complete(int[] i) {
		RobotHardware hardware = data.getHardware();
		if(hardware.isTouching(RobotHardware.FINGER)){
			return target;
		}
		if(hardware.isComplete(RobotHardware.UPPER)&&hardware.isComplete(RobotHardware.LOWER)){
			return new Stomp2(data,target);
		}
		return super.complete(i);
	}
	
	@Override
	public boolean isMotorInfoRequired(int i) {
		return i==RobotHardware.UPPER||i==RobotHardware.LOWER;
	}
	
	@Override
	public boolean isSensorInfoRequired(int i) {
		return i==RobotHardware.FINGER;
	}


}
