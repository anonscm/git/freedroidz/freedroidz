/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.freedroidz;

import icommand.nxt.Motor;

import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.evolvis.freedroidz.command.CommandProvider;

public class Services {
	
	protected static Services instance;
	protected CommandProvider cmdProvider;
	protected Shell shell;
	
	protected final static Logger logger = Logger.getLogger(Services.class.getName());
	
	
	protected Services(){
		cmdProvider = new CommandProvider();
		
	}
	
	public static Services getInstance(){
		if (instance==null) instance = new Services();
		return instance;
	}
	
	public void configure(Motor left, Motor right, Motor third){
		
	}
	
	public CommandProvider getCommandProvider(){
		return cmdProvider;
	}
	
	public void setCommandProvider(CommandProvider provider){
		cmdProvider = provider;
	}
	
	public Shell getShell() {
		return shell;
	}
	
	public void setShell(Shell shell) {
		this.shell = shell;
	}
	
	public void showWarning(final String warningMessage, final Exception excp) {
		logger.warning(warningMessage + "; " + getCauseMessage(excp));
		
		Display
		.getDefault()
		.asyncExec(new Runnable() {

			/**
			 * @see java.lang.Runnable#run()
			 */
			public void run() {
				MessageBox messageBox = new MessageBox(getShell(), SWT.ICON_WARNING);
				
				String message = (warningMessage == null ? "" : warningMessage) +
				(excp != null && warningMessage != null ? ":\r\n\n" : "") +
				(excp == null ? "" : getCauseMessage(excp));
				
				messageBox.setMessage(message);
				messageBox.setText("Error");
				messageBox.open();
			}
			
		});
	}
	
	public void showWarning(String warningMessage) {
		showWarning(warningMessage, null);
	}

	public void showWarning(Exception excp) {
		showWarning(null, excp);
	}
	
	protected String getCauseMessage(Throwable cause) {

		Throwable currentCause = cause;

		while (currentCause.getCause() != null)
			currentCause = currentCause.getCause();

		return currentCause.getLocalizedMessage();
	}
}
