/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.freedroidz.xmpp;

import java.util.logging.Logger;

import org.evolvis.scio.Message;
import org.evolvis.scio.xmpp.StreamListener;
import org.evolvis.scio.xmpp.XMPPFeatures;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public abstract class XMPPMessageListener implements StreamListener {
	
	protected final static Logger logger = Logger.getLogger(XMPPMessageListener.class.getName());

	/**
	 * @see org.evolvis.scio.xmpp.StreamListener#errorOccured(java.lang.String, java.lang.Exception)
	 */
	public void errorOccured(String message, Exception excp) {
		logger.warning(message+"; "+excp.getMessage());
	}

	/**
	 * @see org.evolvis.scio.xmpp.StreamListener#featuresReceived(org.evolvis.scio.xmpp.XMPPFeatures)
	 */
	public void featuresReceived(XMPPFeatures features) {
		logger.info(features.toString());
	}

	/**
	 * @see org.evolvis.scio.xmpp.StreamListener#iqMessageRetrieved(org.evolvis.scio.Message)
	 */
	public void iqMessageRetrieved(Message message) {
		logger.info(message.toString());
	}

	/**
	 * @see org.evolvis.scio.xmpp.StreamListener#messageRetrieved(org.evolvis.scio.Message)
	 */
	public void messageRetrieved(Message message) {
		logger.info(message.toString());
		messageReceived(message.getContent().toString());
	}

	/**
	 * @see org.evolvis.scio.xmpp.StreamListener#unknownMessageRetrieved(org.evolvis.scio.Message)
	 */
	public void unknownMessageRetrieved(Message message) {	
		logger.info(message.toString());
	}

	/**
	 * @see org.evolvis.scio.xmpp.StreamListener#xmppMessageRetrieved(org.evolvis.scio.Message)
	 */
	public void xmppMessageRetrieved(Message message) {
		logger.info(message.toString());
	}

	public abstract void messageReceived(String message);
}
