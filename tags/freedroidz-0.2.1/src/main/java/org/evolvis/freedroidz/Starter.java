/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.freedroidz;

import java.io.File;
import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.evolvis.freedroidz.config.RobotManager;
import org.evolvis.freedroidz.control.RobotControl;
import org.evolvis.freedroidz.ui.RobotConfigPanel;
import org.evolvis.freedroidz.ui.StrategyPanel;
import org.evolvis.freedroidz.ui.XMPPConfigPanel;
import org.evolvis.freedroidz.xmpp.XMPPManager;

/**
 * Class to start the GUI of freedroidz.
 * 
 * @author Thomas Schmitz, tarent GmbH
 */
public class Starter {

	protected final static Logger logger = Logger.getLogger(Starter.class.getName());

	// paths to the freedroidz-icon in different sizes
	protected final static String[] imagePaths = new String[] {
		"/usr/share/icons/hicolor/16x16/apps/freedroidz.png",
		"/usr/share/icons/hicolor/22x22/apps/freedroidz.png",
		"/usr/share/icons/hicolor/26x26/apps/freedroidz.png",
		"/usr/share/icons/hicolor/32x32/apps/freedroidz.png",
		"/usr/share/icons/hicolor/40x40/apps/freedroidz.png",
		"/usr/share/icons/hicolor/48x48/apps/freedroidz.png",
		"/usr/share/icons/hicolor/64x64/apps/freedroidz.png",
		"/usr/share/icons/hicolor/128x128/apps/freedroidz.png",
	}; 

	public static void main(String[] args) {
		Display.setAppName("freedroidz");
		
		final Shell shell = new Shell(new Display());
		shell.setText("freedroidz");
		
		Services.getInstance().setShell(shell);

		Display.getCurrent().addFilter(SWT.KeyDown, new Listener() {

			/**
			 * @see org.eclipse.swt.widgets.Listener#handleEvent(org.eclipse.swt.widgets.Event)
			 */
			public void handleEvent(Event event) {
				if (event.keyCode == SWT.F6)
					shell.setFullScreen(!shell.getFullScreen());
			}
			
		});


		// set window icons
		Image[] images = new Image[imagePaths.length];

		try {
			for(int i=0; i < images.length; i++)
				images[i] = new Image(shell.getDisplay(), imagePaths[i]);

			shell.setImages(images);
		} catch(SWTException excp) {
			logger.warning("could not load window-icons: "+excp);
		}


		shell.setLayout(new FillLayout());
		final StrategyPanel gui = new StrategyPanel(shell);

		Menu menuBar = new Menu(shell, SWT.BAR);
		shell.setMenuBar(menuBar);

		MenuItem fileItem =  new MenuItem(menuBar, SWT.CASCADE);
		fileItem.setText("&File");

		Menu submenu = new Menu (shell, SWT.DROP_DOWN);
		fileItem.setMenu (submenu);

		MenuItem configureRobotItem = new MenuItem (submenu, SWT.PUSH);
		configureRobotItem.setText("Configure Robot");
		configureRobotItem.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				Shell dialog = new Shell (shell);
				dialog.setText ("Configure Robot");
				dialog.setLayout(new FillLayout());
				@SuppressWarnings("unused")
				RobotConfigPanel robotConfig = new RobotConfigPanel(dialog);

				dialog.pack();
				dialog.open ();
				while (!shell.isDisposed ()) {
					if (!gui.getDisplay().readAndDispatch ()) gui.getDisplay().sleep ();
				}
			}
		});

		MenuItem configureXMPPItem = new MenuItem (submenu, SWT.PUSH);
		configureXMPPItem.setText("Configure XMPP");
		configureXMPPItem.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				Shell dialog = new Shell (shell);
				dialog.setText ("Configure XMPP");
				dialog.setLayout(new FillLayout());
				@SuppressWarnings("unused")
				XMPPConfigPanel robotConfig = new XMPPConfigPanel(dialog);

				dialog.pack();
				dialog.open ();
				while (!shell.isDisposed ()) {
					if (!gui.getDisplay().readAndDispatch ()) gui.getDisplay().sleep ();
				}
			}
		});

		MenuItem connectXMPPItem = new MenuItem (submenu, SWT.PUSH);
		connectXMPPItem.setText("Connect XMPP");
		connectXMPPItem.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {

				new Thread() {

					/**
					 * @see java.lang.Thread#run()
					 */
					@Override
					public void run() {
						try {
							XMPPManager.getInstance().connect();
						} catch(Exception excp) {
							Services.getInstance().showWarning("Could not connect to XMPP-Server", excp);
						}
					}
					
				}.start();
			}
		});

		MenuItem quitItem = new MenuItem (submenu, SWT.PUSH);
		quitItem.setText("Quit");
		quitItem.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				quitApplication();
			}
		});

		RobotManager.getInstance().scanConfigurations(new File[] { 
				new File("src/main/config/"),
				new File("/usr/share/freedroidz/robots") });

		// Allow selecting a robot via command-line parameters
		if(args.length > 0)
			RobotManager.getInstance().setActiveRobot(args[0]);

		shell.open();


		while (!gui.isDisposed()) {
			if (!gui.getDisplay().readAndDispatch()) {
				gui.getDisplay().sleep();
			}
		}

		quitApplication();
	}

	protected static void quitApplication() {

		// Disconnect from robot
		if(RobotControl.isInitialized())
			RobotControl.getInstance().dispose();

		// Store robot-configuration
		RobotManager.getInstance().storeConfiguration();


		// Disconnect XMPP and store configuration
		if(XMPPManager.isInitialized()) {
			XMPPManager.getInstance().disconnect();
			XMPPManager.getInstance().storeConfiguration();
		}

		System.exit(0);
	}
}
