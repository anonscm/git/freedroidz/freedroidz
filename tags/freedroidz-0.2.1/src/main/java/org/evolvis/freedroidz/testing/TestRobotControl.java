/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.freedroidz.testing;

import org.evolvis.freedroidz.config.RobotProperties;
import org.evolvis.freedroidz.control.RobotControl;

/**
 * This is a simple test class that makes it possible to test strategies "offline" (e.g. without a robot connected). 
 * The class just prints out the names and parameters of the called methods to standard out. 
 * If you are about to write a complicated strategy, it might be helpful to test and bugfix without the need to deploy the package every time.
 * 
 * To use the test class, simply change the constructor of RobotControl and create an instance of this class instead.  
 * @author Steffi Tinder, tarent GmbH
 *
 */
public class TestRobotControl extends RobotControl {
	
	protected void init(){
		//do nothing
	}
	
	public void connect(RobotProperties robotProperties)  {
		System.out.println("Connecting to robot...." + getRobotHardware().getRobotProperties().getProperty(RobotProperties.ROBOT_NAME)+"\"");
		//nothing else, no real connection
	}
	
	@Override
	public void brake() {
		System.out.println("*** brake ***");
	}

	@Override
	public void dispose() {
		System.out.println("*** dispose ***");
	}

	@Override
	public void driveForward(int speed) {
		System.out.println("driveFroward("+speed+")");
	}

	@Override
	public void driveLeft(int speed, boolean backward) {
		System.out.println("driveLeft("+speed+", "+backward+")");
	}

	@Override
	public void driveLeftBackward(int speed) {
		System.out.println("driveLeftBackward("+speed+")");
	}

	@Override
	public void driveLeftForward(int speed) {
		System.out.println("driveLeftForward("+speed+")");
	}

	@Override
	public void driveRight(int speed, boolean backward) {
		System.out.println("driveRight("+speed+","+backward+")");
	}

	@Override
	public void driveRightBackward(int speed) {
		System.out.println("driveRightBackward("+speed+")");
	}

	@Override
	public void driveRightForward(int speed) {
		System.out.println("driveRightForward("+speed+")");
	}

	@Override
	public void rollOut() {
		System.out.println("rollOut");
	}

	@Override
	public void setSpeed(int speed) {
		System.out.println("setSpeed("+speed+")");
	}

	@Override
	public void turn(int degrees, boolean left) {
		System.out.println("turn("+degrees+", "+left+")");
	}

	@Override
	public void turnLeft(int degrees) {
		System.out.println("turnLeft("+degrees+")");
	}

	@Override
	public void turnRight(int degrees) {
		System.out.println("turnRight("+degrees+")");
	}
	
	

}
