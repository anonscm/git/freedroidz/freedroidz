/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.freedroidz.strategy;

import java.util.ArrayList;
import java.util.List;

import org.evolvis.freedroidz.strategies.RemoteControl;
import org.evolvis.freedroidz.strategies.Turn360Left;
import org.evolvis.freedroidz.strategies.XMPPRemoteControl;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class StrategyManager {

	protected static StrategyManager instance;
	protected List<Strategy> strategies;
	
	protected StrategyManager() {
		
	}
	
	public static StrategyManager getInstance() {
		if(instance == null)
			instance = new StrategyManager();
		
		return instance;
	}
	
	public List<Strategy> getAvailableStrategies() {
		if(strategies == null) {
			strategies = new ArrayList<Strategy>();
			
			strategies.add(new Turn360Left());
			strategies.add(new RemoteControl());
			strategies.add(new XMPPRemoteControl());
		}
		
		return strategies;
	}
}
