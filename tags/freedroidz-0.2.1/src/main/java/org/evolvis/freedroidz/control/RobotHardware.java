/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.freedroidz.control;

import icommand.nxt.Motor;
import icommand.nxt.SensorPort;
import icommand.nxt.UltrasonicSensor;

import java.util.logging.Logger;

import org.evolvis.freedroidz.config.RobotProperties;

/**
 * 
 * This class abstracts the hardware of a LEGO Mindstorms robot.
 * 
 * Classes can use the API of this class to get the desired component
 * relative to the vehicle without knowing the concrete assembly of the robot.
 * 
 * For example the method getMotorLeft() returns the vehicles left-motor
 * (in driving-direction).
 * 
 * The concrete assembly for each robot is defined through an instance of
 * RobotProperties. 
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class RobotHardware {

	protected final static Logger logger = Logger.getLogger(RobotHardware.class.getName());
	protected RobotProperties props;

	protected Motor motorLeft;
	protected Motor motorRight;
	
	protected UltrasonicSensor ultrasonic1;
	
	protected Boolean invertLeft;
	protected Boolean invertRight;

	public RobotHardware(RobotProperties props) {
		this.props = props;
	}

	public Motor getMotorLeft() {
		if(motorLeft == null)
			motorLeft = getMotor(props.getProperty(RobotProperties.MOTOR_LEFT));

		return motorLeft;
	}

	public Motor getMotorRight() {
		if(motorRight == null)
			motorRight = getMotor(props.getProperty(RobotProperties.MOTOR_RIGHT));

		return motorRight;
	}
	
	public boolean isLeftMotorInverted() {
		if(invertLeft == null)
			invertLeft = Boolean.parseBoolean(props.getProperty(RobotProperties.INVERT_LEFT));
		
		return invertLeft;
	}
	
	public boolean isRightMotorInverted() {
		if(invertRight == null)
			invertRight = Boolean.parseBoolean(props.getProperty(RobotProperties.INVERT_RIGHT));
		
		return invertRight;
	}

	protected Motor getMotor(String port) {

		if(port.toLowerCase().equals("a"))
			return Motor.A;
		else if(port.toLowerCase().equals("b"))
			return Motor.B;
		else if(port.toLowerCase().equals("c"))
			return Motor.C;

		logger.warning("unexpected motor-port configuration-value: \""+port+"\".\r\n Allowed values are: \"A\", \"B\" or \"C\"");
		return null;
	}
	
	protected SensorPort getSensorPort(String port) {
		if(port.equals("1"))
			return SensorPort.S1;
		else if(port.equals("2"))
			return SensorPort.S2;
		else if(port.equals("3"))
			return SensorPort.S3;
		else if(port.equals("4"))
			return SensorPort.S4;
		
		logger.warning("unexpected sensor-port configuration-value: \""+port+"\".\r\n Allowed values are: \"1\", \"2\", \"3\" or \"4\"");
		return null;
	}
	
	public UltrasonicSensor getUltrasonic1Sensor() {
		if(ultrasonic1 == null) {
			ultrasonic1 = new UltrasonicSensor(getSensorPort(props.getProperty(RobotProperties.ULTRASONIC_1_PORT)));
			ultrasonic1.setMetric(true);
		}
		
		return ultrasonic1;
	}
	
	public RobotProperties getRobotProperties() {
		return props;
	}
}
