/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.freedroidz.xmpp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.logging.Logger;

import org.evolvis.scio.xmpp.XMPPException;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class XMPPManager {

	protected final static Logger logger = Logger.getLogger(XMPPManager.class.getName());

	protected static XMPPManager instance;

	public final static String USERNAME = "USERNAME";
	public final static String PASSWORD = "PASSWORD";
	public final static String HOST = "HOST";
	public final static String PORT = "PORT";

	protected ExtendedXMPPConnection connection;

	protected boolean isConnected;

	protected Properties properties;

	protected File configFile = new File(System.getProperty("user.home") + File.separator + ".freedroidz" + File.separator + "xmpp.properties");

	protected XMPPManager() {
		properties = new Properties();
		loadConfiguration();
	}

	public static XMPPManager getInstance() {
		if(instance == null)
			instance = new XMPPManager();

		return instance;
	}

	public static boolean isInitialized() {
		return instance != null;
	}

	public void setUsername(String username) {
		setProperty(USERNAME, username);
	}

	public void setPassword(String password) {
		setProperty(PASSWORD, password);
	}

	public void setHost(String host) {
		setProperty(HOST, host);
	}

	public void setPort(int port) {
		setProperty(PORT, String.valueOf(port));
	}

	public String getHost() {
		ensureNotNull(HOST, "localhost");

		return getProperty(HOST);
	}

	public int getPort() {
		ensureNotNull(PORT, "5222");

		return Integer.parseInt(getProperty(PORT));
	}

	public String getUsername() {
		ensureNotNull(USERNAME, "");

		return getProperty(USERNAME);
	}

	public String getPassword() {
		ensureNotNull(PASSWORD, "");

		return getProperty(PASSWORD);
	}

	public void connect() throws UnknownHostException, IOException, XMPPException {
		if(isConnected())
			disconnect();

		connection = new ExtendedXMPPConnection("fabis-thinkpad", getHost(), getPort());
		connection.setUsername(getUsername());
		connection.setPassword(getPassword());

		connection.connect();

		isConnected = true;
	}

	public void disconnect() {
		if(!isConnected())
			return;

		logger.info("Disconnecting XMPP");

		try {
			connection.close();
			isConnected = false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean isConnected() {
		return isConnected;
	}

	public void loadConfiguration() {
		logger.info("Loading XMPP-configuration from "+configFile.getAbsolutePath());

		if(!configFile.isFile()) {
			logger.info("No XMPP-configuration found in "+configFile.getAbsolutePath());
			return;
		}

		try {
			properties.load(new FileInputStream(configFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void storeConfiguration() {
		logger.info("Storing XMPP-configuration to "+configFile.getAbsolutePath());

		if(!configFile.isFile()) {
			try {
				configFile.getParentFile().mkdirs();
				configFile.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		try {
			properties.store(new FileOutputStream(configFile), "XMPP-Configuration for freedroidz");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void addMessageListener(XMPPMessageListener listener) {
		getConnection().addStreamListener(listener);
	}

	public void removeMessageListener(XMPPMessageListener listener) {
		getConnection().removeStreamListener(listener);
	}

	protected void setProperty(String propName, String value) {
		properties.setProperty(propName, value);
	}

	protected String getProperty(String propName) {
		Object value = properties.get(propName);

		if(value != null)
			return value.toString();

		return null;
	}

	protected void ensureNotNull(String propName, String defaultValue) {
		if(getProperty(propName) == null)
			setProperty(propName, defaultValue);
	}

	protected ExtendedXMPPConnection getConnection() {
		if(connection == null || !isConnected())
			throw new RuntimeException("Not connected to XMPP service");

		return connection;
	}
}
