/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.freedroidz.strategies;

import org.evolvis.freedroidz.Services;
import org.evolvis.freedroidz.control.RobotControl;
import org.evolvis.freedroidz.strategy.AbstractStrategy;
import org.evolvis.freedroidz.xmpp.XMPPManager;
import org.evolvis.freedroidz.xmpp.XMPPMessageListener;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class XMPPRemoteControl extends AbstractStrategy {
	
	protected XMPPMessageListener listener;

	/**
	 * @see org.evolvis.freedroidz.strategy.Strategy#getDescription()
	 */
	public String getDescription() {
		return "Control your robot over XMPP!\r\n\n" +
				"Accepted commands are:\r\n" +
				"forward; stop; driveLeft; driveRight";
	}

	/**
	 * @see org.evolvis.freedroidz.strategy.Strategy#getName()
	 */
	public String getName() {
		return "XMPP Remote Control";
	}

	/**
	 * @see org.evolvis.freedroidz.strategy.Strategy#stop()
	 */
	public void stop() {
		// halt robot
		RobotControl.getInstance().brake();
		
		// deregister XMPP-listener and free memory
		XMPPManager.getInstance().removeMessageListener(listener);
		listener = null;
		
		fireStrategyStopped();
	}

	/**
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		fireStrategyStarted();
		
		if(!XMPPManager.getInstance().isConnected()) {
			try {
				XMPPManager.getInstance().connect();
			} catch(Exception excp) {
				
				Services.getInstance().showWarning("Could not connect to XMPP-Server.", excp);
			}
		}
		
		// If connection to XMPP-server could not be established, stop strategy
		
		if(!XMPPManager.getInstance().isConnected()) {
			fireStrategyStopped();
			return;
		}
		
		XMPPManager.getInstance().addMessageListener(listener = new XMPPMessageListener() {

			/**
			 * @see org.evolvis.freedroidz.xmpp.XMPPMessageListener#messageReceived(java.lang.String)
			 */
			@Override
			public void messageReceived(String message) {
				if(message == null)
					return;
				
				// remove whitespaces
				message = message.trim();
				
				if(message.equals("stop"))
					RobotControl.getInstance().brake();
				else if(message.equals("forward"))
					RobotControl.getInstance().driveForward(100);
				else if(message.equals("driveLeft"))
					RobotControl.getInstance().driveLeftForward(20);
				else if(message.equals("driveRight"))
					RobotControl.getInstance().driveRightForward(20);
			}
			
		});

	}
}
