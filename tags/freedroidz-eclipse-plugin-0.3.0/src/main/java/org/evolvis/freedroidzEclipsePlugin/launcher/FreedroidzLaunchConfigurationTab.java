package org.evolvis.freedroidzEclipsePlugin.launcher;

import lejos.pc.comm.NXTInfo;
import net.miginfocom.swt.MigLayout;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Layout;
import org.evolvis.freedroidz.ui.LejosScanPanel;
import org.evolvis.freedroidz.ui.LejosUploadFilePanel;

public class FreedroidzLaunchConfigurationTab extends AbstractLaunchConfigurationTab {
	
	private FreedroidzLaunchConfigurationPanel panel;

	public FreedroidzLaunchConfigurationTab(){
		
		
	}


	public void createControl(Composite arg0) {

//		Layout layout = new MigLayout("", "[100%, fill]", ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
//		arg0.setLayout(layout);
		
		FillLayout fillLayout = new FillLayout(SWT.HORIZONTAL);
		arg0.setLayout(fillLayout);

		panel = new FreedroidzLaunchConfigurationPanel(arg0);
		//panel.setLayoutData("span, wrap");
		panel.setLaunchConfigurationTab(this);
		panel.setSize(arg0.getBounds().width, arg0.getBounds().height);
		
		((CTabFolder)arg0).getItems()[0].setControl(panel);
		((CTabFolder)arg0).getItems()[0].setImage(new Image(Display.getDefault(), this.getClass().getResourceAsStream("/org/evolvis/freedroidzEclipsePlugin/gfx/freedroidz-favicon.png")));
	
		
	}


	public String getName() {
		
		return "freedroidz";
	}

	public void initializeFrom(ILaunchConfiguration arg0) {

		
			try {				
				String alternativeName = arg0.getAttribute(IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME, "freedroidz");			
				String name = arg0.getAttribute("FREEDROIDZ_PROGRAMM_NAME", alternativeName);
				
				if(name.equals(""))
					name=alternativeName;
				
				panel.setProjectName(arg0.getAttribute(IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME, ""));
				panel.setMainClass(arg0.getAttribute(IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME, ""));
				panel.setProgrammName(name);
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}

	public void performApply(ILaunchConfigurationWorkingCopy arg0) {
		// TODO Auto-generated method stub
		
		arg0.setAttribute("FREEDROIDZ_PROGRAMM_NAME", panel.getProgrammName());
		arg0.setAttribute(IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME, panel.getProjectName());
		arg0.setAttribute(IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME, panel.getMainClass());
		
	}

	public void setDefaults(ILaunchConfigurationWorkingCopy arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public void setDirty(){
		this.setDirty(true);
		this.updateLaunchConfigurationDialog();
	}
	
}
