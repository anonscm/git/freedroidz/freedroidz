package org.evolvis.freedroidzEclipsePlugin.handlers;

import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.debug.internal.ui.DebugUIPlugin;
import org.eclipse.debug.internal.ui.launchConfigurations.LaunchConfigurationPropertiesDialog;
import org.eclipse.debug.internal.ui.launchConfigurations.LaunchGroupExtension;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.evolvis.freedroidz.ui.AboutPanel;

public class AboutHandler extends AbstractHandler {
	
	private static final String LAUNCH_GROUP = "freedroidz-eclipse-plugin.launchGroup";
	
	/**
	 * The constructor.
	 */
	public AboutHandler() {
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {

		
		DebugUITools.openLaunchConfigurationPropertiesDialog(Display.getDefault().getActiveShell(), DebugUITools.getLastLaunch(LAUNCH_GROUP), LAUNCH_GROUP);
		
		
		return null;
	}

}