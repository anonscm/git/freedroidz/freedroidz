package org.evolvis.freedroidzEclipsePlugin.launcher;

import org.eclipse.debug.ui.AbstractLaunchConfigurationTabGroup;
import org.eclipse.debug.ui.CommonTab;
import org.eclipse.debug.ui.EnvironmentTab;
import org.eclipse.debug.ui.ILaunchConfigurationDialog;
import org.eclipse.debug.ui.ILaunchConfigurationTab;
import org.eclipse.debug.ui.WorkingDirectoryBlock;
import org.eclipse.debug.ui.sourcelookup.SourceLookupTab;



public class FreedroidzLaunchConfigurationTabGroup extends AbstractLaunchConfigurationTabGroup {

	public void createTabs(ILaunchConfigurationDialog arg0, String arg1) {
		
		  ILaunchConfigurationTab[] tabs = new ILaunchConfigurationTab[] {
				 new FreedroidzLaunchConfigurationTab(),

				 new EnvironmentTab(),
				 new CommonTab()
				 };
				 setTabs(tabs); 
		
	}
	
}