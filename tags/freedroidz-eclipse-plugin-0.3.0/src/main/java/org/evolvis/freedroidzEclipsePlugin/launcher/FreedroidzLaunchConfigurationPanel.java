package org.evolvis.freedroidzEclipsePlugin.launcher;

import java.io.File;
import java.util.ResourceBundle;

import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTConnector;
import lejos.pc.comm.NXTInfo;
import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.evolvis.freedroidz.lejos.Configuration;
import org.evolvis.freedroidz.lejos.NXTMock.NXTCommandMock;
import org.evolvis.freedroidz.ui.LejosScanPanel;
import org.evolvis.freedroidz.ui.TableControl;

public class FreedroidzLaunchConfigurationPanel extends Composite {
	private static final String FREEDROIDZ_UPLOAD_NXT_NAME = "freedroidz.upload.nxtName";

	private static final String FREEDROIDZ_UPLOAD_ADDRESS = "freedroidz.upload.address";

	private static final String FREEDROIDZ_UPLOAD_CONNECTION_TYPE = "freedroidz.upload.connectionType";
	private static final String FREEDROIDZ_UPLOAD_FILENAME = "freedroidz.upload.filename";
	
	String baseName = "org.evolvis.freedroidzEclipsePlugin.FreedroidzLaunchConfigurationPanel"; //$NON-NLS-1$
	final ResourceBundle bundle = ResourceBundle.getBundle(baseName);

	Composite parent = null;

	private static Configuration config = Configuration.getInstance();

	private boolean scanning = false;
	
	private Label connectionTypeValueLabel;
	private Image bluetoothImage;
	private Image usbImage;
	
	private static NXTInfo nxtInfo;

	private Button selectButton;

	private Button rescanButton;
	
	private int protocols = NXTCommFactory.BLUETOOTH | NXTCommFactory.USB;
	
	private Table table;
	private TableControl<NXTInfo> tableControl;

	private Composite searchComposite;

	private Text mainClassText;

	private Text projectNameText;
	
	private Text programmNameText;
	
	private Text adressText;
	private static String addressString;

	private Button runAfterDownloadButton;

	private String programmName;

	private FreedroidzLaunchConfigurationTab launchConfigurationTab;

	
	public static void main(String[] args){
		Shell shell = new Shell(Display.getDefault());
		
		shell.setBounds(200, 200, 830, 370);
		
//		shell.setLayout( new MigLayout("", "[100%, fill]", "[50%, fill][50%, fill]"));
		
		FillLayout fillLayout = new FillLayout(SWT.HORIZONTAL);
		fillLayout.spacing = 1;
		
		shell.setLayout(fillLayout);
		
		FreedroidzLaunchConfigurationPanel panel = new FreedroidzLaunchConfigurationPanel(shell);
		
		shell.open();
		
		while(!shell.isDisposed())
			Display.getDefault().readAndDispatch();
		
		Display.getDefault().dispose();
		
	}

	public FreedroidzLaunchConfigurationPanel(Composite parent) {
		super(parent, 0);
		this.parent = parent;
		initGUI();
	}

	private static void loadSavedInfo() {
		String name = config.getString(FREEDROIDZ_UPLOAD_NXT_NAME, null);
		String address = config.getString(FREEDROIDZ_UPLOAD_ADDRESS, null);
		int protocol = config.getInt(FREEDROIDZ_UPLOAD_CONNECTION_TYPE,
				NXTCommFactory.USB);
		if (address != null) {
			nxtInfo = new NXTInfo(protocol, name, address);
			nxtInfo.protocol = protocol;
		}else{
			nxtInfo=null;
		}
	}
	
	public static void setNxtInfoS(NXTInfo info){
		nxtInfo=info;		
		config.setValue(FREEDROIDZ_UPLOAD_ADDRESS, info.deviceAddress);
		config.setValue(FREEDROIDZ_UPLOAD_CONNECTION_TYPE, info.protocol);
		config.setValue(FREEDROIDZ_UPLOAD_NXT_NAME, ""+info.name);
	}
	
	public void setNxtInfo(NXTInfo info){
		nxtInfo=info;		
		config.setValue(FREEDROIDZ_UPLOAD_ADDRESS, info.deviceAddress);
		config.setValue(FREEDROIDZ_UPLOAD_CONNECTION_TYPE, info.protocol);
		config.setValue(FREEDROIDZ_UPLOAD_NXT_NAME, info.name);
		updateAddressAndConnectionTypGui();
	}
	

	private void initGUI() {
		loadSavedInfo();
		bluetoothImage = new Image(parent.getDisplay(), this.getClass()
				.getResourceAsStream(
						"/org/evolvis/freedroidz/gfx/bluetooth.png"));
		usbImage = new Image(parent.getDisplay(), this.getClass()
				.getResourceAsStream("/org/evolvis/freedroidz/gfx/usb.png"));

//		Layout layout = new MigLayout("", "[50%, fill][40%, fill][10%]", "[70%, fill][30%, fill]"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
//		setLayout(layout);
		
		FillLayout fillLayout = new FillLayout();
		fillLayout.spacing = 1;
		setLayout(fillLayout);
		
		/*
		 * Standart Comp
		 */

		Composite standartComp = new Composite(this, SWT.NONE);
		standartComp.setLayout(new MigLayout(
				"", "[15%!,fill][15%!,fill][15%!,fill][30%!,fill][15%!,fill]", "")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		
		Label headlineLabel = new Label(standartComp, 0);
		headlineLabel.setFont(new Font(Display.getDefault(), new FontData(
				"Sans", 10, SWT.BOLD))); //$NON-NLS-1$
		headlineLabel.setText("General"); //$NON-NLS-1$
		headlineLabel.setLayoutData("wrap"); //$NON-NLS-1$
		headlineLabel.setLayoutData("span, center"); //$NON-NLS-1$

		Label firstSeperator = new Label(standartComp, SWT.SEPARATOR
				| SWT.HORIZONTAL);
		firstSeperator.setLayoutData("span, wrap 15"); //$NON-NLS-1$

		Label mainClassLabel = new Label(standartComp, SWT.NONE);
		mainClassLabel.setText(bundle.getString("FreedroidzLaunchConfigurationPanel.main_class"));
		mainClassLabel.setLayoutData("span 2" );
		
		mainClassText = new Text(standartComp, SWT.BORDER);
		mainClassText.setText("");
		mainClassText.setLayoutData("span 2, wrap");
		
		Label projectNameLabel = new Label(standartComp, SWT.NONE);
		projectNameLabel.setText(bundle.getString("FreedroidzLaunchConfigurationPanel.project_name"));
		projectNameLabel.setLayoutData("span 2" );
		
		projectNameText = new Text(standartComp, SWT.BORDER);
		projectNameText.setText("");
		projectNameText.setLayoutData("span 2, wrap");

		Label programmNameLabel = new Label(standartComp, SWT.NONE);
		programmNameLabel.setText(bundle.getString("FreedroidzLaunchConfigurationPanel.program_name"));
		programmNameLabel.setLayoutData("span 2" );
		
		programmNameText = new Text(standartComp, SWT.BORDER);
		programmNameText.setText("");
		programmNameText.setLayoutData("span 2, wrap");
		
		
		runAfterDownloadButton = new Button(standartComp, SWT.CHECK);
		runAfterDownloadButton.setText(bundle.getString("FreedroidzLaunchConfigurationPanel.run_after_upload"));
		runAfterDownloadButton.setLayoutData("span 2, wrap 15");
		
		Label headline2Label = new Label(standartComp, 0);
		headline2Label.setFont(new Font(Display.getDefault(), new FontData(
				"Sans", 10, SWT.BOLD))); //$NON-NLS-1$
		headline2Label.setText(bundle.getString("FreedroidzLaunchConfigurationPanel.1")); //$NON-NLS-1$
		headline2Label.setLayoutData("wrap"); //$NON-NLS-1$
		headline2Label.setLayoutData("span, center"); //$NON-NLS-1$

		Label secondSeperator = new Label(standartComp, SWT.SEPARATOR
				| SWT.HORIZONTAL);
		secondSeperator.setLayoutData("span, wrap 15"); //$NON-NLS-1$

		Label connectionTypeLabel = new Label(standartComp, SWT.NONE);
		connectionTypeLabel
				.setText(bundle.getString("FreedroidzLaunchConfigurationPanel.2")); //$NON-NLS-1$
		connectionTypeLabel.setLayoutData("span 2");
		connectionTypeValueLabel = new Label(standartComp, SWT.NONE);		
		connectionTypeValueLabel.setLayoutData("span 1, wrap"); //$NON-NLS-1$

		Label adressLabel = new Label(standartComp, 0);
		adressLabel.setText(bundle.getString("FreedroidzLaunchConfigurationPanel.3")); //$NON-NLS-1$
		adressLabel.setLayoutData("span 2");

		adressText = new Text(standartComp, SWT.BORDER);		
//		adressText.setEditable(true);
		adressText.setLayoutData("span 2");
		
		final Button adressButton = new Button(standartComp, SWT.PUSH);
		adressButton.setText(bundle.getString("FreedroidzLaunchConfigurationPanel.4")); //$NON-NLS-1$
		adressButton.setLayoutData("span 1, wrap"); //$NON-NLS-1$
		
//		standartComp.setLayoutData("span 1, growy"); //$NON-NLS-1$
		updateAddressAndConnectionTypGui();
		
		
		
		/*
		 * Listener
		 */
		
		programmNameText.addModifyListener(new ModifyListener(){
			public void modifyText(ModifyEvent arg0) {
				setDirty();
			}
			
		});
		
		mainClassText.addModifyListener(new ModifyListener(){
			public void modifyText(ModifyEvent arg0) {
				setDirty();
			}
			
		});
		
		projectNameText.addModifyListener(new ModifyListener(){
			public void modifyText(ModifyEvent arg0) {
				setDirty();
			}
			
		});
		
		adressText.addModifyListener(new ModifyListener(){
			public void modifyText(ModifyEvent arg0){
				addressString = adressText.getText();
			}
		});
		
		adressButton.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			public void widgetSelected(SelectionEvent arg0) {
//				scan();
//				enableSearchComp(true);
				nxtInfo = LejosScanPanel.runGUI(NXTCommFactory.ALL_PROTOCOLS);
				setNxtInfo(nxtInfo);
			}

		});

	}
	
	private void enableSearchComp(boolean status){
		for(Control control : searchComposite.getChildren()){
			control.setEnabled(status);
		}
		searchComposite.setEnabled(status);
	}

	private void updateAddressAndConnectionTypGui() {
		if(nxtInfo==null){
			adressText.setText(bundle.getString("FreedroidzLaunchConfigurationPanel.5"));//$NON-NLS-1$
			connectionTypeValueLabel.setText(bundle.getString("FreedroidzLaunchConfigurationPanel.5"));//$NON-NLS-1$
			connectionTypeValueLabel.setImage(null);
			return;
		}
		
		adressText.setText(nxtInfo.deviceAddress+" ("+nxtInfo.name+")");
		if (nxtInfo.protocol == NXTCommFactory.BLUETOOTH) {
			connectionTypeValueLabel.setText(bundle
					.getString("FreedroidzLaunchConfigurationPanel.6"));
			connectionTypeValueLabel.setImage(bluetoothImage);
		} else {
			connectionTypeValueLabel.setText(bundle
					.getString("FreedroidzLaunchConfigurationPanel.7"));
			connectionTypeValueLabel.setImage(usbImage);
		}
		
		addressString = adressText.getText();
	}
	
	private void scan() {
//		System.out.println("Scanning...");
		if (scanning) {
			return;
		}
		removeCacheFile();
		scanning = true;
		rescanButton.setEnabled(false);
		selectButton.setEnabled(false);
		Thread t = new Thread() {
			@Override
			public void run() {
				
				Display.getDefault().asyncExec(new Runnable() {
					public void run() {	
						removeAllFromTable();
						addTableEntry("searching...", null, null); 
					}
				});

				try {
					final NXTInfo[] infos = scan0();
					Display.getDefault().asyncExec(new Runnable() {

						public void run() {

							populateList(infos);
							rescanButton.setEnabled(true);
							scanning = false;
						}
					});
				} catch (final NXTCommException e) {

					Display.getDefault().asyncExec(new Runnable(){
						public void run(){
							Shell shell = new Shell(Display.getDefault());
							MessageBox messageBox = new MessageBox(shell, SWT.OK| SWT.ICON_WARNING); 
							messageBox.setText(bundle.getString("LejosScanPanel.1"));
							messageBox.setMessage(bundle.getString("LejosScanPanel.2"));
							messageBox.open();
							
							e.printStackTrace();
							shell.dispose();
						}
					});

				}
			}

		};
		t.start();
	}

	private NXTInfo[] scan0() throws NXTCommException {
		/* the lejos search method skips any bluetooth devices if a device was
		 * found on the usb bus. We do two separate scans to see ALL devices
		 */
		NXTInfo[] btDevices=new NXTInfo[0];
		NXTInfo[] usbDevices=new NXTInfo[0];
		NXTConnector conn = new NXTConnector();

		if((protocols&NXTCommFactory.USB)!=0){
			usbDevices=conn.search(null,
					null,NXTCommFactory.USB);
		}
		
		
		if((protocols&NXTCommFactory.BLUETOOTH)!=0){
			btDevices=conn.search(null,
					null,NXTCommFactory.BLUETOOTH);
		}
		
/*		if((protocols&NXTCommFactory.USB)!=0){
			usbDevices=NXTCommand.getSingleton().search(null,
					NXTCommFactory.USB);
		}
		
		
		if((protocols&NXTCommFactory.BLUETOOTH)!=0){
			btDevices=NXTCommand.getSingleton().search(null,
					NXTCommFactory.BLUETOOTH);
		}*/
		NXTInfo[] infos = new NXTInfo[btDevices.length+usbDevices.length];
		System.arraycopy(usbDevices, 0, infos, 0, usbDevices.length);
		System.arraycopy(btDevices, 0, infos, usbDevices.length, btDevices.length);
		return infos;
	}

	private void populateList(NXTInfo[] infos) {
		removeAllFromTable();
		for (NXTInfo info : infos) {

			String label = info.name + " (" + info.deviceAddress + ")";

			Image image = info.protocol == NXTCommFactory.BLUETOOTH ? bluetoothImage
					: usbImage;

			addTableEntry(label, image, info);
		}
		
		if(infos.length == 0)
			addTableEntry(bundle.getString("LejosScanPanel.7"), null, null);
	}
	
	private void removeAllFromTable(){
		if(!table.isDisposed())
			tableControl.removeAll();
	}
	
	private void addTableEntry(String label, Image image, NXTInfo info){
		if(!table.isDisposed())
			tableControl.addEntry(label, image, info);
	}
	
	public static NXTInfo getNXTInfo(){
		loadSavedInfo();
		return nxtInfo;
	}
	
	public static String getMacAdress(){
		return addressString;
	}
	
	public void setProgrammName(String name){
		this.programmName = name;
		programmNameText.setText(name);
	}
	
	public String getProgrammName(){
		return  programmNameText.getText();
	}
	
	public void setRunAfter(boolean status){
		runAfterDownloadButton.setEnabled(status);
	}
	
	public void setMainClass(String className){
		mainClassText.setText(className);
	}
	
	public String getMainClass(){
		return mainClassText.getText();
	}
	
	public String getProjectName(){
		return projectNameText.getText();
	}
	
	public void setProjectName(String project){
		projectNameText.setText(project);
	}
	
	private void setDirty(){
		this.launchConfigurationTab.setDirty();
	}
	
	public void setLaunchConfigurationTab(FreedroidzLaunchConfigurationTab tab){
		this.launchConfigurationTab = tab;
	}
	
	private void removeCacheFile(){
		new File(System.getProperty("user.home")+ System.getProperty("file.separator")+"nxj.cache").delete();
	}

}