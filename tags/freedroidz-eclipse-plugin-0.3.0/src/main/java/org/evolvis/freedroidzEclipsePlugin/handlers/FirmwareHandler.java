package org.evolvis.freedroidzEclipsePlugin.handlers;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.evolvis.freedroidz.lejos.UploadFirmware;
import org.evolvis.freedroidz.ui.LejosUploadFirmwarePanel;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class FirmwareHandler extends AbstractHandler {
	
	Composite parent = null;

	/**
	 * The constructor.
	 */
	public FirmwareHandler() {
	}


	public Object execute(ExecutionEvent event) throws ExecutionException {

		File file = null;
		try {
			file = makeJNIFilesAvailable();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			UploadFirmware.addToJavaLibraryPath(file.getPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//Open the flashfirmware-dialog
		Shell dialog = new Shell (Display.getDefault());
		dialog.setText ("Lejos Firmware flashen"); //$NON-NLS-1$
		dialog.setBounds(Display.getDefault().getBounds().width/2-100,Display.getDefault().getBounds().height/2-100,200,200);
		dialog.setLayout(new FillLayout());
		LejosUploadFirmwarePanel lejosUploadFirmwarePanel = new LejosUploadFirmwarePanel(dialog);

		dialog.pack();
		dialog.open ();

	        
		return null;
	}


	private File makeJNIFilesAvailable() throws IOException {
		
		File tmpFolder = new File(System.getProperty("java.io.tmpdir"),"JNIFiles");
		if(!tmpFolder.exists())
			tmpFolder.mkdir();	
		
		InputStream is = this.getClass().getResourceAsStream("/lib/libjlibnxt.so");
		
		File jniFile = new File(tmpFolder,"libjlibnxt.so");
		if(jniFile.exists())
			return jniFile;
			
		jniFile.createNewFile();
		
		FileOutputStream fos = null; 
		InputStream inputStream = null;
		try {
			fos = new FileOutputStream(jniFile);
			inputStream = this.getClass().getResourceAsStream("/lib/libjlibnxt.so");
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		int ARRAY_SIZE = 2048;		
		byte[] buf = new byte[ARRAY_SIZE];
			
		int Len = 0;
        int NumRead = 0;
        try {
			while ((NumRead=inputStream.read(buf, 0, ARRAY_SIZE)) > 0) {
				Len += NumRead;
				fos.write(buf, 0, NumRead);
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		return tmpFolder;
	}
	
}
