package org.evolvis.freedroidz.lejos;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;

import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import lejos.nxt.remote.NXTCommand;
import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTCommLogListener;
import lejos.pc.comm.NXTInfo;

import org.apache.commons.io.IOUtils;

public class LejosTestUtils {

	/**
	 * Convenince method for unpacking ZIP archives. 
	 * 
	 * I found the code <a href=
	 * "http://piotrga.wordpress.com/2008/05/07/how-to-unzip-archive-in-java/"
	 * >here</a>, but adopted it to fit my needs. 
	 * Comment by the original author: <cite>It is too small to
	 * release it as an open source library, and Apache Commons does not contain
	 * such a function, when I am writing it this post. Please feel free to
	 * copy/use/modify this code</cite>
	 * 
	 * @param archive
	 * @param outputDir
	 * @throws IOException
	 */
	public static void unzipArchive(File archive, File outputDir)
			throws IOException {

		ZipFile zipfile = new ZipFile(archive);
		for (Enumeration e = zipfile.entries(); e.hasMoreElements();) {
			ZipEntry entry = (ZipEntry) e.nextElement();
			unzipEntry(zipfile, entry, outputDir);
		}

		//      
	}

	private static void unzipEntry(ZipFile zipfile, ZipEntry entry,
			File outputDir) throws IOException {

		if (entry.isDirectory()) {
			createDir(new File(outputDir, entry.getName()));
			return;
		}

		File outputFile = new File(outputDir, entry.getName());
		if (!outputFile.getParentFile().exists()) {
			createDir(outputFile.getParentFile());
		}

		BufferedInputStream inputStream = new BufferedInputStream(zipfile
				.getInputStream(entry));
		BufferedOutputStream outputStream = new BufferedOutputStream(
				new FileOutputStream(outputFile));

		try {
			IOUtils.copy(inputStream, outputStream);
		} finally {
			outputStream.close();
			inputStream.close();
		}
	}

	private static void createDir(File dir) {
		if (!dir.mkdirs())
			throw new RuntimeException("Can not create dir " + dir);
	}

	static{
		NXTCommLogListener logListener = new NXTCommLogListener(){

			@Override
			public void logEvent(String arg0) {
				System.err.println(arg0);
				
			}

			@Override
			public void logEvent(Throwable arg0) {
				arg0.printStackTrace();
				
			}};
//		NXTCommand.getSingleton().addLogListener(logListener);
	}
	/**
	 * Convenience method for connecting to an NXT. Looks for a nxt device and
	 * opens a connection according to the given arguments.
	 * 
	 * @param address
	 *            the device string of the nxt to connect to. Can be null; in
	 *            this case the first device with name NXT that is found is
	 *            used.
	 * @param protocol
	 *            any combination of NXTCommandFactory.USB and/or
	 *            NXTCommandFactory.BLUETOOTH
	 * @return the NXTCommand instance. (which is a singleton, but anyway).
	 * @throws NXTCommException
	 */
/*	public static NXTCommand openConnection(String address, int protocol)
			throws NXTCommException {
		NXTInfo nxt = null;
		NXTCommand nxtCommand = NXTCommand.getSingleton();
		
		if (nxtCommand == null) {
			throw new RuntimeException(
					"NXTCommand.getSingleton() returned null.");
		}
		if (address == null) {
			NXTInfo[] foundNXTs = nxtCommand.getSsearch(null, protocol);
			if (foundNXTs == null || foundNXTs.length == 0) {
				throw new RuntimeException("No NXTs found.");
			}
			nxt = foundNXTs[0];
		} else {
			nxt = new NXTInfo("NXT", address);
		}
		nxtCommand.getS
		boolean result = nxtCommand.open(nxt);
		if(!result){
			throw new RuntimeException("Failed to open connection");
		}
		return nxtCommand;
	}*/
}
