package org.evolvis.freedroidz.lejos;

import java.io.File;
import java.io.IOException;

import org.evolvis.freedroidz.lejos.NXTMock.NXTCommMock;
import org.evolvis.freedroidz.lejos.NXTMock.NXTCommandMock;

import lejos.nxt.remote.FileInfo;
import lejos.pc.comm.NXTCommFactory;
import junit.framework.TestCase;

import lejos.pc.comm.NXTComm;
import lejos.pc.comm.NXTInfo;
import lejos.nxt.remote.NXTCommand;


public class USBUploadTest extends TestCase {
	private UploadFile uploadFile;
	private File testdir;
	private File nxjFile;
	private NXTCommand nxtCommand;

	protected void setUp() throws Exception {
		super.setUp();
		this.uploadFile = new UploadFile();

		/* use classloader to locate our testdata */
		this.testdir = new File(this.getClass().getResource(
				"nxtfilemanagementtest").toURI());
		assertTrue(testdir.exists());
		this.nxjFile = new File(testdir, "test.nxj");
		assertTrue(nxjFile.canRead());
		
		
		NXTInfo nxts = new NXTInfo(NXTCommFactory.USB, null, "00:16:53:02:30:C0" );
		NXTComm nxtComm = new NXTCommMock();
		nxtCommand = new NXTCommandMock();
		boolean open = nxtComm.open(nxts, NXTComm.LCP);
		nxtCommand.setNXTComm(nxtComm);
		
		
		
		
		nxtCommand.delete("test.nxj");
		FileInfo fileInfo = nxtCommand.findFirst("test.nxj");
		assertNull(fileInfo);

	}

	protected void tearDown() throws Exception {
		super.tearDown();

		nxtCommand.delete("test.nxj");
		FileInfo fileInfo = nxtCommand.findFirst("test.nxj");
		nxtCommand.close();
		assertNull(fileInfo);

	}

	public void testUploadViaBlueTooth() throws Throwable {

		String message = nxtCommand.uploadFile(nxjFile);
		assertTrue(message.startsWith("Upload successful"));

		FileInfo fileInfo = nxtCommand.findFirst("test.nxj");
		assertNotNull(fileInfo);

	}

}
