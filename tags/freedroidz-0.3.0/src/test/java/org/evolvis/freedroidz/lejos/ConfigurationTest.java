package org.evolvis.freedroidz.lejos;

import junit.framework.TestCase;

public class ConfigurationTest extends TestCase{

	Configuration configuration;
	
	protected void setUp() throws Exception {
		configuration = Configuration.getInstance();
		assertNotNull(configuration);
		
	}
	
	public void testFunctions() throws Exception{
		
		configuration.setValue("testtest", "true");
		
		assertTrue(configuration.getString("testtest", "false").equals("true"));
		
		assertTrue(configuration.getBoolean("testtest", false));
		
		configuration.removeValue("testtest");
		
		assertFalse(configuration.getBoolean("testtest", false));
		
	}
}
