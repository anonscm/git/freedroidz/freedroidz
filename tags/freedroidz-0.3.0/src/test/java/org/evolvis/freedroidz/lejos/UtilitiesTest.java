package org.evolvis.freedroidz.lejos;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.apache.commons.io.FileUtils;

import junit.framework.TestCase;

public class UtilitiesTest extends TestCase {

	private File testdir;
	private File testSourceArchive;
	private File javacInputDir;
	private File javacOutputDir;
	private CompileLinkFile compileLinkFile;
	

	protected void setUp() throws Exception {
		super.setUp();
		
		this.compileLinkFile = new CompileLinkFile();
		
		/* use classloader to locate our testdata */
		this.testdir = new File(this.getClass().getResource("utilitiestest").toURI());		
		assertTrue(testdir.exists());
		this.testSourceArchive = new File(testdir,"testsource.zip");
		assertTrue(testSourceArchive.canRead());
		
		/* generate a temporary dir for keeping the source files */
		this.javacInputDir = CompileLinkFile.createTempDir(new File(System.getProperty("java.io.tmpdir")),this.getName()+"_input");		
	
		/* unzip the test source */		
		LejosTestUtils.unzipArchive(testSourceArchive, javacInputDir);
		
	}
	
	
	public void test_getPackageNameFromSourceFile() throws Throwable{
		File sourceFile = new File(new File(javacInputDir,"ludeg"),"Testor.java");
		assertTrue(sourceFile.canRead());
		String packageName = Utilities.getPackageNameFromSourceFile(sourceFile);
		assertEquals("ludeg", packageName); 
	}
	
	public void test_getPackageNameFromSourceFile_defaultPackage() throws Throwable{
		File sourceFile = new File(javacInputDir,"Empty.java");
		assertTrue(sourceFile.canRead());
		String packageName = Utilities.getPackageNameFromSourceFile(sourceFile);
		assertEquals("", packageName);
	}
	public void test_getFullClassNameFromSourceFile() throws Throwable{
		File sourceFile = new File(new File(javacInputDir,"ludeg"),"Testor.java");
		assertTrue(sourceFile.canRead());
		String packageName = Utilities.getFullClassNameFromSourceFile(sourceFile);
		assertEquals("ludeg.Testor", packageName);
	}
	
	public void test_getFullClassFromSourceFile_defaultPackage() throws Throwable{
		File sourceFile = new File(javacInputDir,"Empty.java");
		assertTrue(sourceFile.canRead());
		String packageName = Utilities.getFullClassNameFromSourceFile(sourceFile);
		assertEquals("Empty", packageName);
	}

	public void test_getContainingSourcePathEntry() throws Throwable{
		File sourceFile = new File(new File(javacInputDir,"ludeg"),"Testor.java");
		assertTrue(sourceFile.canRead());
		File spe = Utilities.getContainingSourcePathElement(sourceFile);
		assertEquals(javacInputDir, spe);
	}
	
	public void test_getContainingSourcePathEntry_defaultPackage() throws Throwable{
		File sourceFile = new File(javacInputDir,"Empty.java");
		assertTrue(sourceFile.canRead());
		File spe = Utilities.getContainingSourcePathElement(sourceFile);
		assertEquals(javacInputDir, spe);
	}
	
	protected void tearDown() throws Exception {
		FileUtils.deleteDirectory(this.javacInputDir);
		super.tearDown();
	}

}
