package org.evolvis.freedroidz.lejos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import org.apache.bcel.classfile.ClassParser;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import org.apache.commons.io.FileUtils;

import com.sun.source.tree.AnnotationTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.util.JavacTask;

public class Utilities {

	public static String getFullClassName(String input) {

		Class mainClassfile = null;

		int dot = input.lastIndexOf(".");

		String mainFile = input.substring(0, dot) + ".class";

//		System.out.pri ntln(mainFile);

		if (!mainFile.endsWith(".class"))
			mainFile = mainFile + ".class";

		try {
			mainClassfile = new OwnClassLoader().findClass(mainFile);
		} catch (ClassNotFoundException e1) {

			e1.printStackTrace();
		}

		return mainClassfile.getName();

	}

	public static String getClasspath(String input) {

		Class mainClassfile = null;

		int dot = input.lastIndexOf(".");

		String mainFile = input.substring(0, dot) + ".class";

		if (!mainFile.endsWith(".class"))
			mainFile = mainFile + ".class";

		try {
			mainClassfile = new OwnClassLoader().findClass(mainFile);
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		String mainClassFilename = mainClassfile.getName();

		String[] mainFileString = mainFile.split(File.separator);
		int mainFileLen = mainFileString.length;

		StringTokenizer st = new StringTokenizer(mainClassFilename, ".");
		int classNameLen = st.countTokens();

		String classpath = "";

		for (int i = 0; i < (mainFileLen - classNameLen); i++) {
			classpath = classpath + mainFileString[i] + File.separator;
		}

		return classpath;
	}

	static class OwnClassLoader extends ClassLoader {
		public Class findClass(String name) throws ClassNotFoundException {
			byte[] b = null;
			try {
				b = loadClassData(name);
			} catch (FileNotFoundException fnfe) {
				throw new ClassNotFoundException("Could not find " + name, fnfe);
			} catch (IOException e) {

				e.printStackTrace();
			}
			return defineClass(null, b, 0, b.length);
		}

		private byte[] loadClassData(String name) throws IOException {
			File file = new File(name);
			InputStream is = new FileInputStream(file);
			long length = file.length();
			if (length > Integer.MAX_VALUE) {
			}
			byte[] bytes = new byte[(int) length];
			int offset = 0;
			int numRead = 0;
			while (offset < bytes.length
					&& (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
				offset += numRead;
			}
			if (offset < bytes.length) {
				throw new IOException("Could not completely read file "
						+ file.getName());
			}
			is.close();
			return bytes;
		}
	}

	/**
	 * Find the source path entry (i.e. source folder) containing a given source
	 * file. FIXME: maybe this could be generalized so that it works with
	 * archive entries.
	 * 
	 * @param sourceFile
	 *            a File containing a valid Java compilation unit (i.e. any
	 *            well-formed *.java file).
	 * @return The containing source path entry (i.e. source folder).
	 * @throws IOException
	 */
	public static File getContainingSourcePathElement(File sourceFile)
			throws IOException {
		String packageName;
		File spe = sourceFile.getParentFile();
		packageName = getPackageNameFromSourceFile0(sourceFile);
		if (packageName != null) {
			String[] fragments = packageName.split("\\.");
			for (String fragment : fragments) {
				assert (spe.getName().equals(fragment));
				spe = spe.getParentFile();
			}
		}
		return spe;
	}

	private static String getPackageNameFromSourceFile0(File sourceFile)
			throws IOException {
		String packageName;
		CompilationUnitTree ast = parseToASTs(sourceFile).iterator().next();

		ExpressionTree packageNameExpression = ast.getPackageName();
		if (packageNameExpression == null) {
			return null;
		}

		packageName = packageNameExpression.toString();
		return packageName;
	}

	private static Iterable<? extends CompilationUnitTree> parseToASTs(
			File sourceFile) throws IOException {
		Iterable<? extends CompilationUnitTree> ts;
		final JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		final StandardJavaFileManager fileManager = compiler
				.getStandardFileManager(null, null, null);
		Iterable<? extends JavaFileObject> compilationUnits = fileManager
				.getJavaFileObjects(sourceFile);
		com.sun.source.util.JavacTask jt = (com.sun.source.util.JavacTask) compiler
				.getTask(null, fileManager, null, null, null, compilationUnits);

		ts = jt.parse();
		return ts;
	}

	/**
	 * Search a given directory for class files containing main types. FIXME: we
	 * should generalize this so that a given classpath, can be searched.
	 * Shouldn't be too difficult.
	 * 
	 * @param directory
	 * @return a list of fully qualified names of classes that are main types
	 * @throws IOException
	 */
	public static List<String> findMainTypesInDirectory(File directory)
			throws IOException {
		ArrayList<String> fqns = new ArrayList<String>();
		for (Iterator<File> i = FileUtils.iterateFiles(directory,
				new String[] { "class" }, true); i.hasNext();) {
			File classFile = i.next();
			ClassParser parser = new ClassParser(classFile.getAbsolutePath());
			JavaClass javaClass = parser.parse();
			if (isMainType(javaClass)) {
				fqns.add(javaClass.getClassName());
			}
		}
		return fqns;
	}

	private static boolean isMainType(JavaClass javaClass) {
		for (Method method : javaClass.getMethods()) {
			if (isMainMethod(method)) {
				return true;
			}
		}
		return false;
	}

	private static boolean isMainMethod(Method method) {
		if (!method.isStatic()) {
			return false;
		}
		if (!"main".equals(method.getName())) {
			return false;
		}
		if (!"([Ljava/lang/String;)V".equals(method.getSignature())) {
			return false;
		}
		return true;
	}

	public static String getPackageNameFromSourceFile(File sourceFile) throws IOException {
		String name = getPackageNameFromSourceFile0(sourceFile);
		return name==null?"":name;
	}

	public static String getFullClassNameFromSourceFile(File sourceFile) throws IOException {
		String packageName = getPackageNameFromSourceFile0(sourceFile);
		String fileName=sourceFile.getName();
		String simpleClassName=fileName.split("\\.")[0];
		return packageName==null?simpleClassName:packageName+"."+simpleClassName;
	}

}
