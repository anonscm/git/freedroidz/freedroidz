package org.evolvis.freedroidz.ui;

import java.io.File;
import java.io.IOException;
import java.util.ResourceBundle;

import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTConnector;
import lejos.pc.comm.NXTInfo;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

public class LejosScanPanel {

	private Shell shell;

	private Table table;
	private TableControl<NXTInfo> tableControl;

	private Button scanButton;
	
	private Button applyButton;
	
	private Button cancelButton;

	private int protocols;

	private LejosScanPanel(int protocols) {
		
		this.protocols = protocols;
		initGUI();
		scan();
	}
	
	public LejosScanPanel() {
		
		this.protocols = NXTCommFactory.ALL_PROTOCOLS;
		scan();
	}

	private boolean scanning = false;

	private Image bluetoothImage;

	private Image usbImage;

	private NXTInfo selectedInfo;

	String baseName = "org.evolvis.freedroidz.LejosScanPanel"; 
    final ResourceBundle bundle = ResourceBundle.getBundle( baseName ); 
	
	private void scan() {
//		System.out.println("Scanning...");
		if (scanning) {
			return;
		}
		removeCacheFile();
		scanning = true;
		scanButton.setEnabled(false);
		applyButton.setEnabled(false);
		Thread t = new Thread() {
			@Override
			public void run() {
				
				shell.getDisplay().asyncExec(new Runnable() {
					@Override
					public void run() {	
						removeAllFromTable();
						addTableEntry("searching...", null, null); 
					}
				});

				try {
					final NXTInfo[] infos = scan0();
					shell.getDisplay().asyncExec(new Runnable() {
						@Override
						public void run() {

							populateList(infos);
							scanButton.setEnabled(true);
							scanning = false;
						}
					});
				} catch (final NXTCommException e) {

					Display.getDefault().asyncExec(new Runnable(){
						public void run(){
							MessageBox messageBox = new MessageBox(shell, SWT.OK| SWT.ICON_WARNING); 
							messageBox.setText(bundle.getString("LejosScanPanel.1"));
							messageBox.setMessage(bundle.getString("LejosScanPanel.2"));
							messageBox.open();
							
							e.printStackTrace();
							shell.dispose();
						}
					});

				}
			}

		};
		t.start();
	}

	private NXTInfo[] scan0() throws NXTCommException {
		/* the lejos search method skips any bluetooth devices if a device was
		 * found on the usb bus. We do two separate scans to see ALL devices
		 */
		NXTInfo[] btDevices=new NXTInfo[0];
		NXTInfo[] usbDevices=new NXTInfo[0];
		NXTConnector conn = new NXTConnector();

		if((protocols&NXTCommFactory.USB)!=0){
			usbDevices=conn.search(null,
					null,NXTCommFactory.USB);
		}
		
		
		if((protocols&NXTCommFactory.BLUETOOTH)!=0){
			btDevices=conn.search(null,
					null,NXTCommFactory.BLUETOOTH);
		}
		
/*		if((protocols&NXTCommFactory.USB)!=0){
			usbDevices=NXTCommand.getSingleton().search(null,
					NXTCommFactory.USB);
		}
		
		
		if((protocols&NXTCommFactory.BLUETOOTH)!=0){
			btDevices=NXTCommand.getSingleton().search(null,
					NXTCommFactory.BLUETOOTH);
		}*/
		NXTInfo[] infos = new NXTInfo[btDevices.length+usbDevices.length];
		System.arraycopy(usbDevices, 0, infos, 0, usbDevices.length);
		System.arraycopy(btDevices, 0, infos, usbDevices.length, btDevices.length);
		return infos;
	}

	private void populateList(NXTInfo[] infos) {
		removeAllFromTable();
		for (NXTInfo info : infos) {

			String label = info.name + " (" + info.deviceAddress + ")";

			Image image = info.protocol == NXTCommFactory.BLUETOOTH ? bluetoothImage
					: usbImage;

			addTableEntry(label, image, info);
		}
		
		if(infos.length == 0)
			addTableEntry(bundle.getString("LejosScanPanel.7"), null, null);
	}
	
	private void removeAllFromTable(){
		if(!table.isDisposed())
			tableControl.removeAll();
	}
	
	private void addTableEntry(String label, Image image, NXTInfo info){
		if(!table.isDisposed())
			tableControl.addEntry(label, image, info);
	}

	private void initGUI() {

		shell = new Shell(Display.getDefault().getActiveShell());
		shell.setBounds(Display.getDefault().getBounds().width / 2 - 150,
				Display.getDefault().getBounds().height / 2 - 160, 300, 320);
		
		shell.setActive();
		
		shell.setText("Lejos scan panel");

		bluetoothImage = new Image(shell.getDisplay(), this.getClass()
				.getResourceAsStream(
						"/org/evolvis/freedroidz/gfx/bluetooth.jpg"));
		usbImage = new Image(shell.getDisplay(), this.getClass()
				.getResourceAsStream("/org/evolvis/freedroidz/gfx/usb.jpg"));

		shell.setLayout(new MigLayout("", "[47%!,fill][47%!,fill]", "[65%!,fill][10%!,fill][10%!,fill]"));
		
		table = new Table(shell, SWT.BORDER | SWT.MULTI);
		table.setBounds(0, 0, 295, 260);
		table.setLayoutData("span 2, growx, growy, wrap");
		
		tableControl = new TableControl<NXTInfo>(table);
		scanButton = new Button(shell, 0);
		scanButton.setText(bundle.getString("LejosScanPanel.3"));
		scanButton.setLayoutData("growx");
		
		applyButton = new Button(shell, 0);
		applyButton.setText(bundle.getString("LejosScanPanel.4"));
		applyButton.setLayoutData("growx, wrap");
		applyButton.setEnabled(false);
		
		cancelButton = new Button(shell, 0);
		cancelButton.setText(bundle.getString("LejosScanPanel.5"));
		cancelButton.setLayoutData("span 2");

		scanButton.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			public void widgetSelected(SelectionEvent arg0) {
				scan();
			}

		});
		
		applyButton.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			public void widgetSelected(SelectionEvent arg0) {
				selectedInfo = tableControl.getSelectedValue();
				shell.dispose();
			}

		});
		
		cancelButton.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			public void widgetSelected(SelectionEvent arg0) {
				shell.dispose();
			}

		});

		table.addMouseListener(new MouseListener() {

			public void mouseDoubleClick(MouseEvent arg0) {
				selectedInfo = tableControl.getSelectedValue();
				shell.dispose();

			}

			public void mouseDown(MouseEvent arg0) {
			}

			public void mouseUp(MouseEvent arg0) {
			}
		});
		
		table.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				if(tableControl.getSelectedValue() != null)
					applyButton.setEnabled(true);
				
			}
		});
	
		shell.open();

	}

	public static NXTInfo runGUI(int protocols) {
		final LejosScanPanel panel = new LejosScanPanel(protocols);
		
		while (!panel.shell.isDisposed()) {
			Display.getDefault().readAndDispatch();
		}
		return panel.selectedInfo;
	}
	
	private void removeCacheFile(){
		new File(System.getProperty("user.home")+ System.getProperty("file.separator")+"nxj.cache").delete();
	}

	
	public static void main(String[] args){
		runGUI(NXTCommFactory.BLUETOOTH|NXTCommFactory.USB);
		
		
	}

}
