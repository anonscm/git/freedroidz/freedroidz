package org.evolvis.freedroidz.lejos;

import java.io.File;
import java.io.IOException;

import lejos.nxt.remote.NXTCommand;
import lejos.pc.comm.NXTComm;
import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;


public class UploadFile {

	String message = "";
	NXTComm nxtComm = null;
	NXTCommand nxtCommand = null;
	
	static UploadFile instance = null;
	
	public UploadFile(){
		
	}
	
	public UploadFile(NXTComm comm, NXTCommand command){
		this.nxtComm = comm;
		this.nxtCommand = command;
	}
	
	public boolean uploadFile(String filename, String mac, int connection) {
		
		try {
			nxtComm = Connection.getInstance(connection).getNXTComm();
			nxtCommand = Connection.getInstance(connection).getNXTCommand();
			
			NXTInfo nxts = new NXTInfo(connection, null, mac);

			File nxjFile = new File(filename);

			if(!nxtComm.open(nxts, NXTComm.LCP))
				return false;
			
			nxtCommand.setNXTComm(nxtComm);

			message = nxtCommand.uploadFile(nxjFile);
			nxtCommand.close();
		} catch (NXTCommException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (message.startsWith("Upload successful"))
			return true;
		return false;

		// Upload fUpload = new Upload();
		//		
		// try {
		// fUpload.upload(null, mac, connection, filename, false);
		// return true;
		// } catch (NXJUploadException e) {
		// e.printStackTrace();
		// return false;
		// }


	}
	
	public static UploadFile getInstance(){
		if(instance == null)
			instance = new UploadFile();
		return instance;
	}
	


}
