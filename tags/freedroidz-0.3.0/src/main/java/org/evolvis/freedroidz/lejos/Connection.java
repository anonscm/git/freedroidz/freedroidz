package org.evolvis.freedroidz.lejos;

import lejos.nxt.remote.NXTCommand;
import lejos.pc.comm.NXTComm;
import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTCommFactory;

public class Connection {

	static Connection instance = null;
	
	static int connection;
	NXTComm nxtComm = null;
	NXTCommand nxtCommand = null;
	
	public NXTComm getNXTComm(){
		if(nxtComm==null){
			try {
				nxtComm=NXTCommFactory.createNXTComm(connection);
			} catch (NXTCommException e) {
				e.printStackTrace();
			}
		}
		return nxtComm;
	}
	
	public NXTCommand getNXTCommand(){
		if(nxtCommand==null)
			nxtCommand=new NXTCommand();
		return nxtCommand;
	}
	
	public void setNXTComm(NXTComm nxtComm){
		this.nxtComm = nxtComm;
	}
	
	public void setNXTCommand(NXTCommand nxtCommand){
		this.nxtCommand = nxtCommand;
	}
	
	public static Connection getInstance(int conn){
		connection = conn;
		if(instance==null)
			instance=new Connection();
		return instance;
	}
}
