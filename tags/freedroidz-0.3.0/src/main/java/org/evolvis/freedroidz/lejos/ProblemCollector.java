package org.evolvis.freedroidz.lejos;

import java.io.File;

public interface ProblemCollector {
	public void addProblem(String message, File sourceFile, long lineNumber);
}
