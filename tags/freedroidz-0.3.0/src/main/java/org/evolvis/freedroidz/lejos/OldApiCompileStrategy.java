
/**
 * 
 */
package org.evolvis.freedroidz.lejos;

import java.io.File;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Collection;

public final class OldApiCompileStrategy implements CompileStrategy {
	/**
	 * 
	 */
	private final CompileLinkFile compileLinkFile;

	/**
	 * @param compileLinkFile
	 */
	OldApiCompileStrategy(CompileLinkFile compileLinkFile) {
		this.compileLinkFile = compileLinkFile;
	}

	
	public boolean compile(Collection<File> sourceFiles, File destinationDirectory,OutputStream output,ProblemCollector collector) {
		String[] args = this.compileLinkFile.buildJavacCommandLineArray(sourceFiles,destinationDirectory,false);
		OutputStreamWriter diagnosticsWriter = new OutputStreamWriter(output);
		int javacExitCode = com.sun.tools.javac.Main.compile(args,
				new PrintWriter(diagnosticsWriter));
		
		return (javacExitCode == 0);
	}

	
}