package org.evolvis.freedroidz.ui;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.widgets.Combo;

/**
 * Helper class for controlling swt combo widgets.
 * @author ldegen
 *
 */
public class  ComboControl<T> {
	private Combo combo;
	private Map<Integer,T> values = new HashMap<Integer, T>();
	private Map<T,Integer> positions= new HashMap<T, Integer>();
	public ComboControl(Combo combo){
		this.combo=combo;
	}
	
	public void addEntry(String label,T value){
		int position = combo.getItemCount();
		combo.add(label);
		values.put(position, value);
		positions.put(value,position);
	}
	public void select(T value){
		int position=positions.get(value);
		combo.select(position);		
	}
	public T getSelectedValue(){
		int position = combo.getSelectionIndex();
		T value = values.get(position);
		return value;
	}
}
