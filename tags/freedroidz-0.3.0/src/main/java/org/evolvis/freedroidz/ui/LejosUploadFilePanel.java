package org.evolvis.freedroidz.ui;



import java.io.File;
import java.util.ResourceBundle;

import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;
import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.evolvis.freedroidz.lejos.Configuration;
import org.evolvis.freedroidz.lejos.UploadFile;

public class LejosUploadFilePanel extends Composite {
	private static final String FREEDROIDZ_UPLOAD_NXT_NAME = "freedroidz.upload.nxtName";

	private static final String FREEDROIDZ_UPLOAD_ADDRESS = "freedroidz.upload.address";

	private static final String FREEDROIDZ_UPLOAD_CONNECTION_TYPE = "freedroidz.upload.connectionType";
	private static final String FREEDROIDZ_UPLOAD_FILENAME = "freedroidz.upload.filename";
	String baseName = "org.evolvis.freedroidz.LejosUploadFilePanel"; //$NON-NLS-1$
	final ResourceBundle bundle = ResourceBundle.getBundle(baseName);

	Composite parent = null;
//	Shell parent = null;

	private Text filenameText;

	private Text adressText;

	private Configuration config = Configuration.getInstance();

	private Label connectionTypeValueLabel;
	private Image bluetoothImage;
	private Image usbImage;
	
	private NXTInfo nxtInfo;

//	public LejosUploadFilePanel(Shell parent) {
//		super(parent, 0);
//		this.parent = parent;
//		initGUI();
//	}
	
	public LejosUploadFilePanel(Composite parent) {
		super(parent, 0);
		this.parent = parent;
		initGUI();
	}

	public LejosUploadFilePanel(Composite parent, String fileToUpload) {
		super(parent, 0);
		this.parent = parent;
		
		initGUI();
		filenameText.setText(fileToUpload);
		filenameText.setEnabled(false);
	}

	private void loadSavedInfo() {
		String name = config.getString(FREEDROIDZ_UPLOAD_NXT_NAME, null);
		String address = config.getString(FREEDROIDZ_UPLOAD_ADDRESS, null);
		int protocol = config.getInt(FREEDROIDZ_UPLOAD_CONNECTION_TYPE,
				NXTCommFactory.USB);
		if (address != null) {
			nxtInfo = new NXTInfo(protocol, name, address);
			nxtInfo.protocol = protocol;
		}else{
			nxtInfo=null;
		}
	}
	private void setNxtInfo(NXTInfo info){
		nxtInfo=info;		
		config.setValue(FREEDROIDZ_UPLOAD_ADDRESS, info.deviceAddress);
		config.setValue(FREEDROIDZ_UPLOAD_CONNECTION_TYPE, info.protocol);
		config.setValue(FREEDROIDZ_UPLOAD_NXT_NAME, info.name);
		updateAddressAndConnectionTypGui();
	}

	private void initGUI() {
		loadSavedInfo();
		bluetoothImage = new Image(parent.getDisplay(), this.getClass()
				.getResourceAsStream(
						"/org/evolvis/freedroidz/gfx/bluetooth.png"));
		usbImage = new Image(parent.getDisplay(), this.getClass()
				.getResourceAsStream("/org/evolvis/freedroidz/gfx/usb.png"));
		setSize(720, 250);

		Layout layout = new MigLayout("", "[100%, fill]", ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		setLayout(layout);

		Composite standartComp = new Composite(this, SWT.BORDER);
		standartComp.setLayout(new MigLayout(
				"", "[15%!,fill][65%!,fill][15%!,fill]", "")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		Label headlineLabel = new Label(standartComp, 0);
		headlineLabel.setFont(new Font(Display.getDefault(), new FontData(
				"Sans", 10, SWT.BOLD))); //$NON-NLS-1$
		headlineLabel.setText(bundle.getString("LejosUploadFilePanel.7")); //$NON-NLS-1$
		headlineLabel.setLayoutData("wrap"); //$NON-NLS-1$
		headlineLabel.setLayoutData("span, center"); //$NON-NLS-1$

		Label firstSeperator = new Label(standartComp, SWT.SEPARATOR
				| SWT.HORIZONTAL);
		firstSeperator.setLayoutData("span, growx, wrap 20"); //$NON-NLS-1$

		Label filenameLabel = new Label(standartComp, 0);
		filenameLabel.setText(bundle.getString("LejosUploadFilePanel.11")); //$NON-NLS-1$

		filenameText = new Text(standartComp, SWT.SINGLE | SWT.BORDER);
		filenameText.setText(config.getString(FREEDROIDZ_UPLOAD_FILENAME, "")); //$NON-NLS-1$
		Button browseButton = new Button(standartComp, SWT.PUSH);
		browseButton.setText(bundle.getString("LejosUploadFilePanel.12")); //$NON-NLS-1$
		browseButton.setLayoutData("growx, wrap"); //$NON-NLS-1$

		Label connectionTypeLabel = new Label(standartComp, SWT.NONE);
		connectionTypeLabel
				.setText(bundle.getString("LejosUploadFilePanel.14")); //$NON-NLS-1$
		connectionTypeValueLabel = new Label(standartComp, SWT.NONE);		
		connectionTypeValueLabel.setLayoutData("span 1, wrap"); //$NON-NLS-1$

		Label adressLabel = new Label(standartComp, 0);
		adressLabel.setText(bundle.getString("LejosUploadFilePanel.18")); //$NON-NLS-1$

		adressText = new Text(standartComp, SWT.SINGLE | SWT.BORDER);		
		adressText.setEditable(false);
		final Button adressButton = new Button(standartComp, SWT.PUSH);
		adressButton.setText(bundle.getString("LejosUploadFilePanel.20")); //$NON-NLS-1$
		adressButton.setLayoutData("growx, wrap"); //$NON-NLS-1$

		final Button uploadButton = new Button(standartComp, SWT.PUSH);
		uploadButton.setLayoutData("span 2"); //$NON-NLS-1$
		uploadButton.setText(bundle.getString("LejosUploadFilePanel.23")); //$NON-NLS-1$

		Button cancelButton = new Button(standartComp, SWT.PUSH);
		cancelButton.setText(bundle.getString("LejosUploadFilePanel.36"));
		cancelButton.setLayoutData("growx, wrap");
		
		standartComp.setLayoutData("span, growx"); //$NON-NLS-1$
		updateAddressAndConnectionTypGui();
		
		cancelButton.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {

			}

			@Override
			public void widgetSelected(SelectionEvent arg0) {
					parent.dispose();
			}
			
		});
		
		uploadButton.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			public void widgetSelected(SelectionEvent arg0) {
				uploadButtonPressed();
			}

		});

		adressButton.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			public void widgetSelected(SelectionEvent arg0) {
				NXTInfo info = LejosScanPanel
						.runGUI(NXTCommFactory.BLUETOOTH|NXTCommFactory.USB);
				
				if(info != null)
					setNxtInfo(info);
				
			}


		});

		browseButton.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			public void widgetSelected(SelectionEvent arg0) {
				FileDialog openFile = new FileDialog(parent.getShell(), SWT.OPEN);
				String path = openFile.open();
				if(path == null)
					path = "";
				filenameText.setText(path);
				config.setValue(FREEDROIDZ_UPLOAD_FILENAME, path);
			}

		});

	}

	private void updateAddressAndConnectionTypGui() {
		if(nxtInfo==null){
			adressText.setText(bundle.getString("LejosUploadFilePanel.25"));//$NON-NLS-1$
			connectionTypeValueLabel.setText(bundle.getString("LejosUploadFilePanel.25"));//$NON-NLS-1$
			connectionTypeValueLabel.setImage(null);
			return;
		}
		
		adressText.setText(nxtInfo.name+" ("+nxtInfo.deviceAddress+")");
		if (nxtInfo.protocol == NXTCommFactory.BLUETOOTH) {
			connectionTypeValueLabel.setText(bundle
					.getString("LejosUploadFilePanel.16"));
			connectionTypeValueLabel.setImage(bluetoothImage);
		} else {
			connectionTypeValueLabel.setText(bundle
					.getString("LejosUploadFilePanel.15"));
			connectionTypeValueLabel.setImage(usbImage);
		}
	}

	

	public void uploadButtonPressed() {
		String msg = null;

		if (UploadFile.getInstance().uploadFile(filenameText.getText(), nxtInfo.deviceAddress, nxtInfo.protocol)) {
			String text = bundle.getString("LejosUploadFilePanel.31"); //$NON-NLS-1$
			String message = bundle.getString("LejosUploadFilePanel.32"); //$NON-NLS-1$
			boolean yes = showYesNoQuestion(text, message);
			if (yes) {
				filenameText.setText(""); //$NON-NLS-1$
			} else {
				parent.dispose();
			}
		} else {

			boolean shouldRetry = showErrorRetryDialog(msg);

			if (shouldRetry) {
				uploadButtonPressed();
			}

		}
	}

	private boolean showErrorRetryDialog(String errorMsg) {
		MessageBox messageBox = new MessageBox(parent.getShell(), SWT.ICON_WORKING
				| SWT.RETRY | SWT.CANCEL);
		messageBox.setText(bundle.getString("LejosUploadFilePanel.34")); //$NON-NLS-1$

		messageBox.setMessage(bundle.getString("LejosUploadFilePanel.35") + //$NON-NLS-1$
				errorMsg);
		boolean yes = SWT.RETRY == messageBox.open();
		return yes;
	}

	private boolean showYesNoQuestion(String text, String message) {
		MessageBox messageBox = new MessageBox(parent.getShell(), SWT.ICON_WORKING
				| SWT.YES | SWT.NO);
		messageBox.setText(text);
		messageBox.setMessage(message);
		int answer = messageBox.open();
		boolean yes = answer == SWT.YES;
		return yes;
	}
}
