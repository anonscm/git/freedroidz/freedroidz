/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.freedroidz;

import java.io.File;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.evolvis.freedroidz.icommand.Services;
import org.evolvis.freedroidz.icommand.config.IcommandProperties;
import org.evolvis.freedroidz.icommand.config.RobotManager;
import org.evolvis.freedroidz.ui.AboutPanel;
import org.evolvis.freedroidz.ui.LejosUtilsPanel;
import org.evolvis.freedroidz.ui.RobotConfigPanel;
import org.evolvis.freedroidz.ui.StrategyPanel;
import org.evolvis.freedroidz.ui.XMPPConfigPanel;
import org.evolvis.freedroidz.xmpp.XMPPManager;
import org.evolvis.freedroidzI.icommand.control.RobotControl;
import org.evolvis.xana.swt.utils.SWTIconFactory;
import org.evolvis.xana.utils.IconFactoryException;

/**  
 * Class to start the freedroidz-application
 * 
 * @author Thomas Schmitz, tarent GmbH
 */
public class Starter {

	protected final static Logger logger = Logger.getLogger(Starter.class.getName());

	// paths to the freedroidz-icon in different sizes
	protected final static String[] imagePaths = new String[] {
		"/usr/share/icons/hicolor/16x16/apps/freedroidz.png", //$NON-NLS-1$
		"/usr/share/icons/hicolor/22x22/apps/freedroidz.png", //$NON-NLS-1$
		"/usr/share/icons/hicolor/26x26/apps/freedroidz.png", //$NON-NLS-1$
		"/usr/share/icons/hicolor/32x32/apps/freedroidz.png", //$NON-NLS-1$
		"/usr/share/icons/hicolor/40x40/apps/freedroidz.png", //$NON-NLS-1$
		"/usr/share/icons/hicolor/48x48/apps/freedroidz.png", //$NON-NLS-1$
		"/usr/share/icons/hicolor/64x64/apps/freedroidz.png", //$NON-NLS-1$
		"/usr/share/icons/hicolor/128x128/apps/freedroidz.png", //$NON-NLS-1$
	}; 

	public static void main(String[] args) {
		
	    String baseName = "org.evolvis.freedroidz.starter";
	    final ResourceBundle bundle = ResourceBundle.getBundle( baseName ); 

		Display.setAppName("freedroidz"); //$NON-NLS-1$

		final Shell shell = new Shell(new Display());
		shell.setText("freedroidz"); //$NON-NLS-1$

		Services.getInstance().setShell(shell);

		// Init Icon-dirs
		try {
			SWTIconFactory.getInstance().addResourcesFolder(Starter.class, "/org/evolvis/freedroidz/gfx/"); //$NON-NLS-1$
		} catch (IconFactoryException e1) {
			// TODO Auto-generated catch block
			e1.getMessage();
		}

		Display.getCurrent().addFilter(SWT.KeyDown, new Listener() {

			/**
			 * @see org.eclipse.swt.widgets.Listener#handleEvent(org.eclipse.swt.widgets.Event)
			 */
			public void handleEvent(Event event) {
				if (event.keyCode == SWT.F6)
					shell.setFullScreen(!shell.getFullScreen());
			}

		});


		// set window icons
		Image[] images = new Image[imagePaths.length];

		try {
			for(int i=0; i < images.length; i++)
				images[i] = new Image(shell.getDisplay(), imagePaths[i]);

			shell.setImages(images);
		} catch(SWTException excp) {
			logger.warning(bundle.getString("Starter.11")+excp); //$NON-NLS-1$
		}

		final Display display = Display.getDefault();

		shell.setLayout(new FormLayout());
		

				
		ToolBar toolbar = new ToolBar(shell, 0);
		toolbar.setLayout(new FormLayout());
		FormData toolbarData = new FormData();
		toolbarData.top = new FormAttachment(0,0);
		toolbarData.bottom = new FormAttachment(10,0);
		toolbarData.left = new FormAttachment(0,0);
		toolbarData.right = new FormAttachment(100,0);
		toolbar.setLayoutData(toolbarData);


		Image configureRobot = new Image(display, Starter.class.getResourceAsStream("/org/evolvis/freedroidz/gfx/configure-robot.png")); //$NON-NLS-1$
		Image configureXMPP = new Image(display, Starter.class.getResourceAsStream("/org/evolvis/freedroidz/gfx/configure-server.png")); //$NON-NLS-1$
		Image connectXMPP = new Image(display, Starter.class.getResourceAsStream("/org/evolvis/freedroidz/gfx/network-server.png")); //$NON-NLS-1$
		Image Info = new Image(display, Starter.class.getResourceAsStream("/org/evolvis/freedroidz/gfx/about.png")); //$NON-NLS-1$
		Image imageQuit = new Image(display, Starter.class.getResourceAsStream("/org/evolvis/freedroidz/gfx/system-log-out.png")); //$NON-NLS-1$

	    
	    ToolItem toolItem1 = new ToolItem(toolbar, SWT.PUSH);
	    toolItem1.setImage(configureRobot);
	    toolItem1.setToolTipText(bundle.getString("Starter.17")); //$NON-NLS-1$
	    ToolItem toolItem2 = new ToolItem(toolbar, SWT.PUSH);
	    toolItem2.setImage(configureXMPP);
	    toolItem2.setToolTipText(bundle.getString("Starter.18")); //$NON-NLS-1$
	    ToolItem toolItem3 = new ToolItem(toolbar, SWT.PUSH);
	    toolItem3.setImage(connectXMPP);
	    toolItem3.setToolTipText(bundle.getString("Starter.19")); //$NON-NLS-1$
	    ToolItem toolItem4 = new ToolItem(toolbar, SWT.PUSH);
	    toolItem4.setImage(Info);
	    toolItem4.setToolTipText(bundle.getString("Starter.20")); //$NON-NLS-1$
	    ToolItem toolItem5 = new ToolItem(toolbar, SWT.PUSH);
	    toolItem5.setToolTipText(bundle.getString("Starter.21")); //$NON-NLS-1$
	    toolItem5.setImage(imageQuit);

	    //Listener for configure robot
	    toolItem1.addListener(SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				Shell dialog = new Shell (shell);
				dialog.setText (bundle.getString("Starter.22")); //$NON-NLS-1$
				dialog.setLayout(new FillLayout());
				@SuppressWarnings("unused")
				RobotConfigPanel robotConfig = new RobotConfigPanel(dialog);

				dialog.pack();
				dialog.open ();
				while (!shell.isDisposed ()) {
					if (!display.readAndDispatch ()) display.sleep ();
				}
			}
		});
	    
	    //Listener for configure XMPP
	    toolItem2.addListener(SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				Shell dialog = new Shell (shell);
				dialog.setText (bundle.getString("Starter.23")); //$NON-NLS-1$
				dialog.setLayout(new FillLayout());
				@SuppressWarnings("unused")
				XMPPConfigPanel robotConfig = new XMPPConfigPanel(dialog);

				dialog.pack();
				dialog.open ();
				while (!shell.isDisposed ()) {
					if (!display.readAndDispatch ()) display.sleep ();
				}
			}
		});
	    
	    //connect XMPP listener
	    toolItem3.addListener(SWT.Selection, new Listener () {
			public void handleEvent (Event e) {

				new Thread() {

					/**
					 * @see java.lang.Thread#run()
					 */
					@Override
					public void run() {
						try {
							XMPPManager.getInstance().connect();
						} catch(Exception excp) {
							Services.getInstance().showWarning(bundle.getString("Starter.24"), excp); //$NON-NLS-1$
						}
					}

				}.start();
			}
		});
	    
	    //about listener
	    toolItem4.addListener(SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				Shell dialog = new Shell (shell);
				dialog.setText (bundle.getString("Starter.25")); //$NON-NLS-1$
				dialog.setLayout(new FillLayout());
				@SuppressWarnings("unused")
				AboutPanel aboutPanel = new AboutPanel(dialog);

				dialog.pack();
				dialog.open ();
				while (!shell.isDisposed ()) {
					if (!display.readAndDispatch ()) display.sleep ();
				}
			}
		});
	    
	    //quit listener
	    toolItem5.addListener(SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				quitApplication();
			}
		});
		
		
		
		
	    
	    
		TabFolder tabFolder = new TabFolder(shell, 0);
		tabFolder.setLayout(new FillLayout());
		FormData tabLayoutData = new FormData();
		tabLayoutData.top = new FormAttachment(toolbar, 0);
		tabLayoutData.bottom = new FormAttachment(100,-5);
		tabLayoutData.left = new FormAttachment(0, 0);
		tabLayoutData.right = new FormAttachment(100, 0);
		tabFolder.setLayoutData(tabLayoutData);
		
		
		
	    
		
		TabItem lejosUtilsPanelTabItem = new TabItem(tabFolder, 0);
		final LejosUtilsPanel lejosUtilsPanel = new LejosUtilsPanel(tabFolder);
		lejosUtilsPanelTabItem.setControl(lejosUtilsPanel);
		lejosUtilsPanelTabItem.setText(bundle.getString("Starter.26")); //$NON-NLS-1$
		
		TabItem strategyPanelTabItem = new TabItem(tabFolder, 0);
		final StrategyPanel strategyPanel = new StrategyPanel(tabFolder);
		strategyPanelTabItem.setControl(strategyPanel);
		strategyPanelTabItem.setText(bundle.getString("Starter.27")); //$NON-NLS-1$

		Menu menuBar = new Menu(shell, SWT.BAR);
		shell.setMenuBar(menuBar);

		MenuItem fileItem =  new MenuItem(menuBar, SWT.CASCADE);
		fileItem.setText(bundle.getString("Starter.28")); //$NON-NLS-1$

		Menu submenu = new Menu (shell, SWT.DROP_DOWN);
		fileItem.setMenu (submenu);

		MenuItem configureRobotItem = new MenuItem (submenu, SWT.PUSH);
		configureRobotItem.setText(bundle.getString("Starter.29")); //$NON-NLS-1$
		configureRobotItem.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				Shell dialog = new Shell (shell);
				dialog.setText (bundle.getString("Starter.30")); //$NON-NLS-1$
				dialog.setLayout(new FillLayout());
				@SuppressWarnings("unused")
				RobotConfigPanel robotConfig = new RobotConfigPanel(dialog);

				dialog.pack();
				dialog.open ();
				while (!shell.isDisposed ()) {
					if (!display.readAndDispatch ()) display.sleep ();
				}
			}
		});

		MenuItem configureXMPPItem = new MenuItem (submenu, SWT.PUSH);
		configureXMPPItem.setText(bundle.getString("Starter.31")); //$NON-NLS-1$
		configureXMPPItem.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				Shell dialog = new Shell (shell);
				dialog.setText (bundle.getString("Starter.32")); //$NON-NLS-1$
				dialog.setLayout(new FillLayout());
				@SuppressWarnings("unused")
				XMPPConfigPanel robotConfig = new XMPPConfigPanel(dialog);

				dialog.pack();
				dialog.open ();
				while (!shell.isDisposed ()) {
					if (!display.readAndDispatch ()) display.sleep ();
				}
			}
		});

		MenuItem connectXMPPItem = new MenuItem (submenu, SWT.PUSH);
		connectXMPPItem.setText(bundle.getString("Starter.33")); //$NON-NLS-1$
		connectXMPPItem.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {

				new Thread() {

					/**
					 * @see java.lang.Thread#run()
					 */
					@Override
					public void run() {
						try {
							XMPPManager.getInstance().connect();
						} catch(Exception excp) {
							Services.getInstance().showWarning(bundle.getString("Starter.34"), excp); //$NON-NLS-1$
						}
					}

				}.start();
			}
		});

		//		MenuItem infoItem = new MenuItem (submenu, SWT.PUSH);
		//		infoItem.setText("Info");
		//		infoItem.addListener (SWT.Selection, new Listener () {
		//			public void handleEvent (Event e) {
		//
		//				// Show version-information			
		//				Services.getInstance().showInfo();
		//			}
		//		});

		MenuItem aboutItem = new MenuItem (submenu, SWT.PUSH);
		aboutItem.setText(bundle.getString("Starter.35")); //$NON-NLS-1$
		aboutItem.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				Shell dialog = new Shell (shell);
				dialog.setText (bundle.getString("Starter.36")); //$NON-NLS-1$
				dialog.setLayout(new FillLayout());
				@SuppressWarnings("unused")
				AboutPanel aboutPanel = new AboutPanel(dialog);

				dialog.pack();
				dialog.open ();
				while (!shell.isDisposed ()) {
					if (!display.readAndDispatch ()) display.sleep ();
				}
			}
		});

		MenuItem quitItem = new MenuItem (submenu, SWT.PUSH);
		quitItem.setText(bundle.getString("Starter.37")); //$NON-NLS-1$
		quitItem.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				quitApplication();
			}
		});

		RobotManager.getInstance().scanConfigurations(new File[] { 
				//the latest entry has the highest priority
				new File("/usr/share/freedroidz/robots"), //$NON-NLS-1$
				new File("src/main/config/")}); //$NON-NLS-1$

		// Allow selecting a robot via command-line parameters
		if(args.length > 0)
			RobotManager.getInstance().setActiveRobot(args[0]);

		shell.open();
		
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		quitApplication();
	}

	protected static void quitApplication() {

		// Disconnect from robot
		if(RobotControl.isInitialized())
			RobotControl.getInstance().dispose();

		// Store robot-configuration
		RobotManager.getInstance().storeConfiguration();

		// Remove icommand.properties file
		IcommandProperties.removeConfiguration();


		// Disconnect XMPP and store configuration
		if(XMPPManager.isInitialized()) {
			XMPPManager.getInstance().disconnect();
			XMPPManager.getInstance().storeConfiguration();
		}

		System.exit(0);
	}
}
