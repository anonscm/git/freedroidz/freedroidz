package org.evolvis.freedroidz.lejos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;

/**
 * Simple singleton class for storing persistent settings.
 * 
 * @author ldegen
 * 
 */
public class Configuration {
	private static Configuration instance;

	public static Configuration getInstance() {
		if (instance == null) {
			instance = new Configuration();
		}
		return instance;
	} 

	private Properties properties = new Properties();

	private Configuration() {
		try {
			load();
		} catch (IOException e) {
			// not critical. It just means we couldn't load default values.
			e.printStackTrace();
		}
		Runtime.getRuntime().addShutdownHook(new Thread(){
			@Override
			public void run() {
				try {
					store();
				} catch (IOException e) {
					// not critical. It just means we couldn't save default values.
					e.printStackTrace();
				}
			}
		});
	}

	private void load() throws IOException {
		File propertiesFile = getPropertiesFile();
		if (propertiesFile.canRead()) {
			FileInputStream inStream = new FileInputStream(propertiesFile);
			properties.load(inStream);
			inStream.close();
		}
	}

	private File getPropertiesFile() throws IOException {
		File homeDir = new File(System.getProperty("user.home"));
		File freedroidzConfigDir = new File(homeDir, ".freedroidz");
		FileUtils.forceMkdir(freedroidzConfigDir);

		File propertiesFile = new File(freedroidzConfigDir,
				"configuration.properties");
		return propertiesFile;
	}

	public void store() throws IOException {
		File propertiesFile = getPropertiesFile();
		FileOutputStream out = new FileOutputStream(propertiesFile);
		properties.store(out, "see org.evolvis.freedroidz.lejos.Configuration");
	}

	public void setValue(String name, String value) {
		properties.setProperty(name, value);
	}

	public int getInt(String name, int defaultValue) {
		String value = properties.getProperty(name, "" + defaultValue);
		return Integer.parseInt(value);
	}

	public String getString(String name, String defaultValue) {
		return this.properties.getProperty(name, defaultValue);
	}

	public boolean getBoolean(String name, boolean defaultValue) {
		String value = properties.getProperty(name, "" + defaultValue);
		return Boolean.parseBoolean(value);
	}

	public void setValue(String name, int value) {
		setValue(name,""+value);
		
	}
	
	public void removeValue(String name){
		properties.remove(name);
	}

}
