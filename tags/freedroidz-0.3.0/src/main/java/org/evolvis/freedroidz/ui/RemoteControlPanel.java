package org.evolvis.freedroidz.ui;

import java.io.IOException;
import java.util.ResourceBundle;

import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Widget;
import org.evolvis.freedroidz.utilities.RemoteControl;

public class RemoteControlPanel extends Composite{
    String baseName = "org.evolvis.freedroidz.RemoteControlPanel"; 
    final ResourceBundle bundle = ResourceBundle.getBundle( baseName ); 
	
	Shell parent;
	RemoteControl remoteControl;
	
	public RemoteControlPanel(Shell parent){
		super(parent, 0);
		this.parent = parent;
		initGui();
	}
	
	public void initGui(){
		
		MessageBox messageBox = new MessageBox(parent, SWT.ICON_INFORMATION | SWT.OK);
		messageBox.setText(bundle.getString("RemoteControlPanel.0")); //$NON-NLS-1$
		messageBox.setMessage(bundle.getString("RemoteControlPanel.1")); //$NON-NLS-1$
		messageBox.open();
		
/*		BluetoothScanPanel bluetoothScanPanel = new BluetoothScanPanel();
		String adress = bluetoothScanPanel.runGUI();*/
		
		NXTInfo nxtInfo = LejosScanPanel.runGUI(NXTCommFactory.BLUETOOTH|NXTCommFactory.USB);
		
		if(nxtInfo == null){
			this.parent.dispose();
			return;
		}
			
		remoteControl = new RemoteControl(nxtInfo);
		try {
			remoteControl.initConnection();
		} catch (NXTCommException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
/*		if(adress == null)
			return;*/
		
		
//		setBounds(Display.getDefault().getBounds().width/2-160,Display.getDefault().getBounds().height/2-125,320,250);
/*		setSize(320,250);
		
		setLayout(new GridLayout(1, true));
		
		Composite remoteComp = new Composite(this, SWT.BORDER);
		GridData remoteCompGridData = new GridData(SWT.FILL, SWT.FILL, true,
				true, 1, 1);
		GridData remoteElementsGridData = new GridData(SWT.FILL, SWT.FILL,
				true, true, 2, 1);
		remoteComp.setLayoutData(remoteElementsGridData);
		


		remoteComp.setLayout(new GridLayout(3, false));
		Label blancUpLeft = new Label(remoteComp, SWT.NONE);
		blancUpLeft.setLayoutData(remoteCompGridData);
		Button upButton = new Button(remoteComp, SWT.NONE);
		upButton.setLayoutData(remoteCompGridData);
		upButton.setText(bundle.getString("RemoteControlPanel.2")); //$NON-NLS-1$
		Label blancUpRight = new Label(remoteComp, SWT.NONE);
		blancUpRight.setLayoutData(remoteCompGridData);
		Button leftButton = new Button(remoteComp, SWT.NONE);
		leftButton.setLayoutData(remoteCompGridData);
		leftButton.setText(bundle.getString("RemoteControlPanel.3")); //$NON-NLS-1$
		final Button stopButton = new Button(remoteComp, SWT.NONE);
		stopButton.setLayoutData(remoteCompGridData);
		stopButton.setText(bundle.getString("RemoteControlPanel.4")); //$NON-NLS-1$
		Button rightButton = new Button(remoteComp, SWT.NONE);
		rightButton.setLayoutData(remoteCompGridData);
		rightButton.setText(bundle.getString("RemoteControlPanel.5")); //$NON-NLS-1$
		Label blancDownLeft = new Label(remoteComp, SWT.NONE);
		blancDownLeft.setLayoutData(remoteCompGridData);
		Button downButton = new Button(remoteComp, SWT.NONE);
		downButton.setLayoutData(remoteCompGridData);
		downButton.setText(bundle.getString("RemoteControlPanel.6")); //$NON-NLS-1$
		Label blancDownRight = new Label(remoteComp, SWT.NONE);
		blancDownRight.setLayoutData(remoteCompGridData);

		remoteControl = new RemoteControl(adress);
		try {
			remoteControl.initConnection();
		} catch (NXTCommException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		stopButton.setFocus();
		
		for(int i=0;i<remoteComp.getChildren().length;i++){
			remoteComp.getChildren()[i].addKeyListener(new KeyListener(){
				public void keyPressed(KeyEvent arg0) {
					if(arg0.keyCode == SWT.ARROW_UP){
						remoteControl.foreward();
					}
					if(arg0.keyCode == SWT.ARROW_DOWN){
						remoteControl.backward();
					}
					if(arg0.keyCode == SWT.ARROW_LEFT){
						remoteControl.turnLeft();
					}
					if(arg0.keyCode == SWT.ARROW_RIGHT){
						remoteControl.turnRight();
					}
				
				}

				public void keyReleased(KeyEvent arg0) {
					remoteControl.stop();
				}
			});
			
			remoteComp.getChildren()[i].addFocusListener(new FocusListener(){

				public void focusGained(FocusEvent arg0) {
					handle(arg0.widget);
				}

				public void focusLost(FocusEvent arg0) {
				}
			});
			
			remoteComp.getChildren()[i].addListener(SWT.Selection, new Listener(){
				public void handleEvent(Event arg0) {
					remoteControl.stop();
					stopButton.setFocus();
				}
			});
			
		}*/
		
		setLayout(new MigLayout("", "[95%!,fill]", ""));
		
		Label headlineLabel = new Label(this, 0);
		headlineLabel.setLayoutData("span, growx, growy, wrap");
		
		Font font1 = new Font(Display.getDefault(),"Arial", 12, SWT.BOLD);
		headlineLabel.setFont(font1);
		
		headlineLabel.setText(bundle.getString("RemoteControlPanel.7"));
		
		Label firstSeperator = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		firstSeperator.setLayoutData("span, wrap");
		
		Font font2 = new Font(Display.getDefault(),"Arial", 10, SWT.NORMAL);
		
		Label mainLabel = new Label(this, 0);
		mainLabel.setLayoutData("span, wrap 20");
		mainLabel.setFont(font2);
		
		final Button reverseButton = new Button(this, SWT.CHECK);
		reverseButton.setText(bundle.getString("RemoteControlPanel.9"));
		
		mainLabel.setText(bundle.getString("RemoteControlPanel.8"));
		
		reverseButton.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent arg0) {
				if(arg0.keyCode == SWT.ARROW_UP){
					remoteControl.foreward();
				}
				if(arg0.keyCode == SWT.ARROW_DOWN){
					remoteControl.backward();
				}
				if(arg0.keyCode == SWT.ARROW_LEFT){
					remoteControl.turnLeft();
				}
				if(arg0.keyCode == SWT.ARROW_RIGHT){
					remoteControl.turnRight();
				}
				if(arg0.keyCode == 65536){
					int[] tmp = new int[3];
					remoteControl.centerMotor(tmp, 300);
					remoteControl.flushCommand(tmp);
				}
				if(arg0.keyCode == 262144){
					int[] tmp = new int[3];
					remoteControl.centerMotor(tmp, -300);
					remoteControl.flushCommand(tmp);
				}
				
			}

			public void keyReleased(KeyEvent arg0) {
				remoteControl.stop();
			}
			
		});
		
		reverseButton.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {}

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				remoteControl.reverseDirection(reverseButton.getSelection());
			}
			
		});
		
		parent.addDisposeListener(new DisposeListener(){
			public void widgetDisposed(DisposeEvent arg0) {
				try {
					remoteControl.closeConnection();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
		});
	}
	
	public void handle(Widget widget){
		if(widget.toString().equals("Button {right}")){ //$NON-NLS-1$
			remoteControl.turnRight();
		}
		if(widget.toString().equals("Button {left}")){ //$NON-NLS-1$
			remoteControl.turnLeft();
		}
		if(widget.toString().equals("Button {stop}")){ //$NON-NLS-1$
			remoteControl.stop();
		}
		if(widget.toString().equals("Button {go}")){ //$NON-NLS-1$
			remoteControl.foreward();
		}
		if(widget.toString().equals("Button {back}")){ //$NON-NLS-1$
			remoteControl.backward();
		}
	}
}
