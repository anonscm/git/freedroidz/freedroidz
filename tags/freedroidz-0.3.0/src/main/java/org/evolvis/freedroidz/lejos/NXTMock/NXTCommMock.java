package org.evolvis.freedroidz.lejos.NXTMock;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import lejos.pc.comm.NXTComm;
import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTInfo;

public class NXTCommMock implements NXTComm {


	@Override
	public int available() throws IOException {
		return 1;
	}

	@Override
	public InputStream getInputStream() {
		return System.in;
	}

	@Override
	public OutputStream getOutputStream() {
		return System.out;
	}

	@Override
	public boolean open(NXTInfo nxt) throws NXTCommException {
		return true;
	}

	@Override
	public byte[] read() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NXTInfo[] search(String name, int protocol) throws NXTCommException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void write(byte[] data) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void close() throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public byte[] sendRequest(byte[] message, int replyLen) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean open(NXTInfo nxt, int mode) throws NXTCommException {
		return true;
	}
}
