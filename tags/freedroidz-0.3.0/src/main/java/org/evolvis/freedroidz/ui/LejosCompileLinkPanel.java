package org.evolvis.freedroidz.ui;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.ResourceBundle;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.evolvis.freedroidz.lejos.CompileLinkFile;
import org.evolvis.freedroidz.lejos.Configuration;
import org.evolvis.freedroidz.lejos.Utilities;

public class LejosCompileLinkPanel extends Composite {
	
    private static final String FREEDROIDZ_LINK_DESTFILE = "freedroidz.link.destfile";
	private static final String FREEDROIDZ_COMPILE_SOURCEFILE = "freedroidz.compile.sourcefile";
	String baseName = "org.evolvis.freedroidz.LejosCompileLink"; 
    final ResourceBundle bundle = ResourceBundle.getBundle( baseName ); 
	
    public Text sourceFilenameText; 
	
	public Text destFilenameText;
	
	private Composite parent;
	private Configuration config;
	
	public LejosCompileLinkPanel(Composite parent){
		super(parent, 0);
		this.parent = parent;
		this.config = Configuration.getInstance();
		initGui();
	}
	
	public void initGui(){
		setSize(720,250);
		
		Layout layout = new MigLayout("","[100%, fill]",""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		setLayout(layout);
		
		Composite standartComp = new Composite(this, SWT.BORDER);
		standartComp.setLayout(new MigLayout("","[20%!,fill][60%!,fill][15%!,fill]","")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		
		Label headlineLabel = new Label(standartComp, 0);
		headlineLabel.setFont(new Font(Display.getDefault(), new FontData("Sans", 10, SWT.BOLD))); //$NON-NLS-1$
		headlineLabel.setText(bundle.getString("LejosCompileLinkPanel.7")); //$NON-NLS-1$
		headlineLabel.setLayoutData("wrap"); //$NON-NLS-1$
		headlineLabel.setLayoutData("span, center"); //$NON-NLS-1$
		
		Label firstSeperator = new Label(standartComp,SWT.SEPARATOR | SWT.HORIZONTAL);
		firstSeperator.setLayoutData("span, growx, wrap 20"); //$NON-NLS-1$
		
		Label sourceFilenameLabel = new Label(standartComp, 0);
		sourceFilenameLabel.setText(bundle.getString("LejosCompileLinkPanel.11")); //$NON-NLS-1$
		
		sourceFilenameText= new Text(standartComp, SWT.SINGLE | SWT.BORDER);		
		sourceFilenameText.setText(config.getString(FREEDROIDZ_COMPILE_SOURCEFILE, ""));//$NON-NLS-1$ //$NON-NLS-2$
		sourceFilenameText.setLayoutData("width 60%!");
		
		Button browseSourceButton = new Button(standartComp, SWT.PUSH);
		browseSourceButton.setText(bundle.getString("LejosCompileLinkPanel.12")); //$NON-NLS-1$
		browseSourceButton.setLayoutData("growx, wrap"); //$NON-NLS-1$
		
		Label filenameLabel = new Label(standartComp, 0);
		filenameLabel.setText(bundle.getString("LejosCompileLinkPanel.14")); //$NON-NLS-1$
		
		destFilenameText= new Text(standartComp, SWT.SINGLE | SWT.BORDER);
		destFilenameText.setText(config.getString(FREEDROIDZ_LINK_DESTFILE, ""));//$NON-NLS-1$ //$NON-NLS-2$
		destFilenameText.setLayoutData("width 60%!");
		
		Button browseDestButton = new Button(standartComp, SWT.PUSH);
		browseDestButton.setText(bundle.getString("LejosCompileLinkPanel.15")); //$NON-NLS-1$
		browseDestButton.setLayoutData("growx, wrap"); //$NON-NLS-1$
		
		
		Button compileButton = new Button(standartComp, 0);
		compileButton.setText(bundle.getString("LejosCompileLinkPanel.17")); //$NON-NLS-1$
		compileButton.setLayoutData("growx, span 2"); //$NON-NLS-1$
		
		Button cancelButton = new Button(standartComp, 0);
		cancelButton.setText(bundle.getString("LejosCompileLinkPanel.29"));
		cancelButton.setLayoutData("growx, wrap");
		
		cancelButton.addSelectionListener(new SelectionListener(){
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {}
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				parent.dispose();
			}
		});
	
		compileButton.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			public void widgetSelected(SelectionEvent arg0) {
				try {
					compileAndLink();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		});
		
		browseSourceButton.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			public void widgetSelected(SelectionEvent arg0) {
				FileDialog openFile = new FileDialog(parent.getShell(), SWT.OPEN);							
				//set path of open dialog to previously used path
				File file = new File(config.getString(FREEDROIDZ_COMPILE_SOURCEFILE, ""));
				if (file!=null) openFile.setFilterPath(file.getParent());
				
				String path = openFile.open();
				if(path != null){
					sourceFilenameText.setText(path);
					config.setValue(FREEDROIDZ_COMPILE_SOURCEFILE, path);
				}
			}
			
		});
		
		browseDestButton.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			public void widgetSelected(SelectionEvent arg0) {
				FileDialog saveFile = new FileDialog(parent.getShell(), SWT.SAVE);

				//set path of open dialog to previously used path
				File file = new File(config.getString(FREEDROIDZ_LINK_DESTFILE, ""));
//				System.out.println(file);
				if (file!=null) saveFile.setFilterPath(file.getParent());

				//doesn't seem to work properly
				//saveFile.setFilterExtensions(new String[]{bundle.getString("LejosCompileLinkPanel.19")}); //$NON-NLS-1$
				String path = saveFile.open();

				if(path != null){
					if(!path.endsWith(".nxj"))
						path=path+".nxj";
					destFilenameText.setText(path);	
					config.setValue(FREEDROIDZ_LINK_DESTFILE, path);
				}
				
			}

		});
	}
	
	public void compileAndLink() throws IOException{
		if(sourceFilenameText.getText().equals("") || destFilenameText.getText().equals("")){ //$NON-NLS-1$ //$NON-NLS-2$
			showErrorMessage(bundle.getString("LejosCompileLinkPanel.23"));//$NON-NLS-1$
		}
		
		CompileLinkFile compileLinkFile = new CompileLinkFile();
		File outputFile = new File(destFilenameText.getText());
		File inputFile = new File(sourceFilenameText.getText());
		File javacOuputDir=CompileLinkFile.createTempDir(new File(System.getProperty("java.io.tmpdir")), "freedroidz-javac-output-");
		File sourceFolder = Utilities.getContainingSourcePathElement(inputFile);
		Collection<File> sourceFiles = compileLinkFile.searchSourceFiles(sourceFolder);
		
		if(!compileLinkFile.compile(sourceFiles, javacOuputDir)){
			showCompileErrors(compileLinkFile);
		}else{
			
			String classFileName = Utilities.getFullClassNameFromSourceFile(inputFile);
			String classpath = javacOuputDir.getAbsolutePath();	
			
			compileLinkFile.startLink(classFileName, classpath, outputFile);
			
			String text = bundle.getString("LejosCompileLinkPanel.26"); //$NON-NLS-1$
			String message = bundle.getString("LejosCompileLinkPanel.27"); //$NON-NLS-1$			
			showUploadDialog(outputFile.getAbsolutePath());			
			
		}
			
		
	}

	private void showUploadDialog(String outputFileName) {
		Shell dialog = new Shell (Display.getDefault());
		dialog.setText (bundle.getString("LejosCompileLinkPanel.28")); //$NON-NLS-1$
		dialog.setBounds(Display.getDefault().getBounds().width/2-360,Display.getDefault().getBounds().height/2-125,720,250);
		dialog.setLayout(new FillLayout());

		
		new LejosUploadFilePanel(dialog, outputFileName);

		dialog.pack();
		dialog.open ();
	}

	private boolean showYesNoQuestion(String text, String message) {
		MessageBox messageBox = new MessageBox(parent.getShell(), SWT.ICON_WORKING | SWT.YES | SWT.NO);
		messageBox.setText(text); 			
		messageBox.setMessage(message); 
		int answer = messageBox.open();
		boolean yes = answer==SWT.YES;
		return yes;
	}

	private void showErrorMessage(String message) {
		MessageBox messageBox = new MessageBox(parent.getShell(), SWT.ICON_ERROR | SWT.OK);
		messageBox.setText(bundle.getString("LejosCompileLinkPanel.22")); //$NON-NLS-1$			
		messageBox.setMessage(message); 
		messageBox.open();
	}

	private void showCompileErrors(CompileLinkFile compileLinkFile) {
		MessageBox messageBox = new MessageBox(parent.getShell(), SWT.ICON_ERROR | SWT.OK);
		messageBox.setText(bundle.getString("LejosCompileLinkPanel.24")); //$NON-NLS-1$
		messageBox.setMessage(bundle.getString("LejosCompileLinkPanel.25") //$NON-NLS-1$
				+ compileLinkFile.getErrors());
		messageBox.open();
	}
}
