/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.freedroidz.ui;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Text;
import org.evolvis.freedroidz.icommand.Services;
import org.evolvis.freedroidz.icommand.command.Command;
import org.evolvis.freedroidz.icommand.config.RobotManager;
import org.evolvis.freedroidz.icommand.strategy.AbstractStrategy;
import org.evolvis.freedroidz.icommand.strategy.Strategy;
import org.evolvis.freedroidz.icommand.strategy.StrategyEvent;
import org.evolvis.freedroidz.icommand.strategy.StrategyListener;
import org.evolvis.freedroidz.icommand.strategy.StrategyManager;
import org.evolvis.freedroidzI.icommand.control.RobotControl;

/**
 *  
 *  This is a SWT-Panel for displaying the available strategies (in a ComboBox),
 *  show the description of the selected strategy and for starting and stopping the selected
 *  strategy.
 *  
 * @author Thomas Schmitz, tarent GmbH; Fabian Köster, tarent GmbH Bonn
 */
public class StrategyPanel extends Composite implements KeyListener{
	
    String baseName = "org.evolvis.freedroidz.StrategyPanel";
    final ResourceBundle bundle = ResourceBundle.getBundle( baseName ); 

	protected final static Logger logger = Logger.getLogger(StrategyPanel.class.getName());
	protected Combo strategyCombo;
	protected Text descriptionText;
	protected Button runButton;
	protected boolean strategyRunning;
	
	/**
	 * Creates a new instance.
	 * 
	 * @param parent the parent container
	 */
	public StrategyPanel(Composite parent) {
		super(parent, SWT.BORDER);
		createGUI();
		pack();
		addKeyListener(this);
	}

	/**
	 * Creates the GUI components and lays them down on this Composite, using
	 * the MigLayout.
	 */
	private void createGUI() {
		setLayout(new MigLayout("fillx, wrap 2", "[] [grow, fill]")); //$NON-NLS-1$ //$NON-NLS-2$

		Label strategyLabel = new Label(this, SWT.READ_ONLY);
		strategyLabel.setText(bundle.getString("StrategyPanel.2")); //$NON-NLS-1$

		strategyCombo = new Combo(this, SWT.READ_ONLY);
		strategyCombo.addKeyListener(this);
		
		Iterator<Strategy> strategies = StrategyManager.getInstance().getAvailableStrategies().iterator();
			
		while(strategies.hasNext())
			strategyCombo.add(strategies.next().getName());
		
		String lastStrategy = null;
		
		// select last used strategy
		
		/* We currently (2008-05-23) have a bug in jalimo for maemo which prevents the installation of the classpath-gconf package which contains
		 * the gconf-peer-library which is needed for using Preferences with classpath. Therefore we need to catch the UnsatisfiedLinkError.
		 * See bug #181 in jalimo-tracker (http://evolvis.org/tracker/index.php?func=detail&aid=181&group_id=11&atid=105)
		 */
		try {
			lastStrategy = Preferences.userRoot().get("org.freedroidz.last_strategy", null); //$NON-NLS-1$
		} catch(UnsatisfiedLinkError err) {
			logger.warning(bundle.getString("StrategyPanel.4")+err.getLocalizedMessage()); //$NON-NLS-1$
		} catch(NoClassDefFoundError err) {
			logger.warning(bundle.getString("StrategyPanel.5")+err.getLocalizedMessage()); //$NON-NLS-1$
		}
		
		if(lastStrategy == null)
			strategyCombo.select(0);		
		else {
			
			for(int i=0; i < strategyCombo.getItemCount(); i++) {
				
				if(lastStrategy.equals(strategyCombo.getItem(i))) {
					
					strategyCombo.select(i);
					break;
				}
			}
			
		}

		Label descriptionLabel = new Label(this, SWT.READ_ONLY);
		descriptionLabel.setText(bundle.getString("StrategyPanel.6")); //$NON-NLS-1$

		descriptionText = new Text(this, SWT.MULTI | SWT.BORDER | SWT.READ_ONLY |SWT.WRAP );
		descriptionText.setEditable(false);
		descriptionText.setLayoutData("height 80"); //$NON-NLS-1$
		descriptionLabel.addKeyListener(this);
		List<Strategy> sList = StrategyManager.getInstance().getAvailableStrategies();
		int idx = strategyCombo.getSelectionIndex();
		if (idx >= 0 && idx < sList.size())
			descriptionText.setText(sList.get(idx).getDescription());
		
		strategyCombo.addSelectionListener(new SelectionListener() {

			/**
			 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			/**
			 * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			public void widgetSelected(SelectionEvent arg0) {
				// update the description-field
				descriptionText.setText(StrategyManager.getInstance().getAvailableStrategies().get(strategyCombo.getSelectionIndex()).getDescription());
				
				/* We currently (2008-05-23) have a bug in jalimo for maemo which prevents the installation of the classpath-gconf package which contains
				 * the gconf-peer-library which is needed for using Preferences with classpath. Therefore we need to catch the UnsatisfiedLinkError.
				 * See bug #181 in jalimo-tracker (http://evolvis.org/tracker/index.php?func=detail&aid=181&group_id=11&atid=105)
				 */
				try {
					Preferences.userRoot().put("org.freedroidz.last_strategy", strategyCombo.getItem(strategyCombo.getSelectionIndex())); //$NON-NLS-1$
				} catch(UnsatisfiedLinkError err) {
					logger.warning(bundle.getString("StrategyPanel.9")+err.getLocalizedMessage()); //$NON-NLS-1$
				} catch(NoClassDefFoundError err) {
					logger.warning(bundle.getString("StrategyPanel.10")+err.getLocalizedMessage()); //$NON-NLS-1$
				}
			}
			
		});
		
		@SuppressWarnings("unused")
		Label sensorLabel = new Label(this, SWT.NONE);
		//sensorLabel.setText("Sensors");
		
		final Image StrategyImage = new Image(getDisplay(), this.getClass().getResourceAsStream("/org/evolvis/freedroidz/gfx/start.png")); //$NON-NLS-1$
		runButton = new Button(this, SWT.None);
		runButton.setText(bundle.getString("StrategyPanel.12")); //$NON-NLS-1$
		runButton.setImage(StrategyImage);
		runButton.addKeyListener(this);
		runButton.addSelectionListener(new SelectionListener() {

			/**
			 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			public void widgetDefaultSelected(SelectionEvent arg0) {

			}

			/**
			 * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			public void widgetSelected(SelectionEvent event) {
				
				final Strategy strategy = StrategyManager.getInstance().getAvailableStrategies().get(strategyCombo.getSelectionIndex());
				
				// Disable UI (Activation of "Start Strategy"-button would lead to an error because of trying to connect twice)
				setControlsEnabled(false);
				
				if(strategyRunning) {
					strategy.stop();
					return;
				}
				
				// register listener to react on changes in strategy-state
				//StrategyManager.getInstance().addStrategyListener(new StrategyListener() {
				//TODO: when all strategies are migrated, add the methods to the interface and remove the cast
				((AbstractStrategy)strategy).addStrategyListener(new StrategyListener() {
					
					/**
					 * @see org.evolvis.freedroidz.icommand.strategy.StrategyListener#strategyStarted(org.evolvis.freedroidz.icommand.strategy.StrategyEvent)
					 */
					public void strategyStarted(StrategyEvent event) {
						strategyRunning = true;
						logger.info(bundle.getString("StrategyPanel.13")); //$NON-NLS-1$
						// re-enable run-button (labeled "stop strategy") in order to allow the strategy to be stopped
						enableRunButtonAndSetText(bundle.getString("StrategyPanel.14"), StrategyImage); //$NON-NLS-1$
					}

					/**
					 * @see org.evolvis.freedroidz.icommand.strategy.StrategyListener#strategyStopped(org.evolvis.freedroidz.icommand.strategy.StrategyEvent)
					 */
					public void strategyStopped(StrategyEvent event) {
						strategyRunning = false;
						
						// re-enable the run-button (labeled "start strategy") in order to allow the strategy to be stopped
						RobotControl.getInstance().dispose();
						enableRunButtonAndSetText(bundle.getString("StrategyPanel.15"), StrategyImage); //$NON-NLS-1$
						setControlsEnabled(true);
					}

					/**
					 * @see org.evolvis.freedroidz.icommand.strategy.StrategyListener#strategyFinished(org.evolvis.freedroidz.icommand.strategy.StrategyEvent)
					 */
					public void strategyFinished(StrategyEvent event) {
						strategyStopped(event);
					}
				});
				
				//start strategy in new thread				
				try {
					
					new Thread() {
						public void run() {
							// Create icommand.properties file
							RobotManager.getInstance().updateIcommandConfiguration();
							
							// connect to robot
							RobotControl.getInstance().connect(RobotManager.getInstance().getActiveRobot());
							
							// check if connection could be established (maybe robot was not configured properly...)
							if(!RobotControl.getInstance().isConnected()) {
								setControlsEnabled(true);
								return;
							}
							
							// start strategy
							strategy.run();
						}
					}.start();
					
					 
				} catch(Exception excp) {
					MessageBox message = new MessageBox(StrategyPanel.this.getShell(), SWT.ICON_WARNING);
					excp.printStackTrace();
					logger.warning(excp.toString());
					message.setMessage(excp.toString());
					message.setText(bundle.getString("StrategyPanel.16")); //$NON-NLS-1$
					message.open();
				}
			}
		});
		
//		// register listener to react on changes in strategy-state
//		//StrategyManager.getInstance().addStrategyListener(new StrategyListener() {
//		strategy.addStrategyListener(new StrategyListener() {
//			
//			/**
//			 * @see org.evolvis.freedroidz.strategy.StrategyListener#strategyStarted(org.evolvis.freedroidz.strategy.StrategyEvent)
//			 */
//			public void strategyStarted(StrategyEvent event) {
//				strategyRunning = true;
//				logger.info("Strategy started!");
//				// re-enable run-button (labeled "stop strategy") in order to allow the strategy to be stopped
//				enableRunButtonAndSetText("Stop Strategy");
//			}
//
//			/**
//			 * @see org.evolvis.freedroidz.strategy.StrategyListener#strategyStopped(org.evolvis.freedroidz.strategy.StrategyEvent)
//			 */
//			public void strategyStopped(StrategyEvent event) {
//				strategyRunning = false;
//				
//				// re-enable the run-button (labeled "start strategy") in order to allow the strategy to be stopped
//				RobotControl.getInstance().dispose();
//				enableRunButtonAndSetText("Start Strategy");
//				setControlsEnabled(true);
//			}
//
//			/**
//			 * @see org.evolvis.freedroidz.strategy.StrategyListener#strategyFinished(org.evolvis.freedroidz.strategy.StrategyEvent)
//			 */
//			public void strategyFinished(StrategyEvent event) {
//				strategyStopped(event);
//			}
//		});
	}
	
	protected void enableRunButtonAndSetText(final String text, final Image strategyImage) {
		getDisplay().asyncExec(new Runnable() {

			/**
			 * @see java.lang.Runnable#run()
			 */
			public void run() {
				runButton.setText(text);
				runButton.setEnabled(true);
				runButton.setImage(strategyImage);
			}
			
		});	
	}
	
	protected void setControlsEnabled(final boolean enabled) {
		this.getDisplay().asyncExec(new Runnable() {

			/**
			 * @see java.lang.Runnable#run()
			 */
			public void run() {
				runButton.setEnabled(enabled);
				strategyCombo.setEnabled(enabled);
				descriptionText.setEnabled(enabled);
			}
		
		});
	}

	public void keyPressed(KeyEvent arg0) {
		Command command = new Command(arg0);
		Services.getInstance().getCommandProvider().fireCommandSent(command);
		
	}

	public void keyReleased(KeyEvent arg0) {
		Command command = new Command(arg0);
		Services.getInstance().getCommandProvider().fireCommandReleased(command);
		
	}
}
