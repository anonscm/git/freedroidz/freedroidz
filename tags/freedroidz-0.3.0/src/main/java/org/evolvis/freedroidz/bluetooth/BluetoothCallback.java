package org.evolvis.freedroidz.bluetooth;

public interface BluetoothCallback {
	public void newDeviceFound(final String Device);
	public void searchFinished();
}
