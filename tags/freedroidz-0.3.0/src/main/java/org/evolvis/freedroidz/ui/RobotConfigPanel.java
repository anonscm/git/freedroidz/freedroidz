/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.freedroidz.ui;

import java.util.ResourceBundle;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.evolvis.freedroidz.icommand.config.RobotManager;
import org.evolvis.freedroidz.icommand.config.RobotProperties;

/**
 * 
 * This SWT-Panel allows the user to select the robot which will be used.
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class RobotConfigPanel extends Composite {

	protected Shell parent;
	
	/**
	 * Creates a new instance.
	 * 
	 * @param parent the parent container
	 */
	public RobotConfigPanel(Shell parent) {
		super(parent, SWT.BORDER);
		this.parent = parent;
		createGUI();
	}
	
	/**
	 * Creates the GUI components and lays them down on this Composite, using
	 * the MigLayout.
	 */
	private void createGUI() {
		setLayout(new MigLayout("fillx, wrap 2", "[] [grow, fill]"));
		
		Label robotLabel = new Label(this, SWT.NONE);
		robotLabel.setText("Robot");
		
		/*final Combo robotCombo = new Combo(this, SWT.READ_ONLY);
		robotCombo.setItems(RobotManager.getInstance().getRobotNames());
		
		
		// Select active robot
		Object activeRobotName = null;
		
		RobotProperties activeRobot = RobotManager.getInstance().getActiveRobot();
		if(activeRobot != null)
			activeRobotName = activeRobot.get(RobotProperties.ROBOT_NAME);
		
		if(activeRobotName == null)
			activeRobotName = "";
	
		for(int i=0; i < robotCombo.getItemCount(); i++)
			if(activeRobotName.equals(robotCombo.getItem(i)))
				robotCombo.select(i);
		
		Button saveButton = new Button(this, SWT.NONE);
		saveButton.setText("Save");
		saveButton.addSelectionListener(new SelectionListener() {


			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}


			public void widgetSelected(SelectionEvent arg0) {
				
				try {
					RobotManager.getInstance().setActiveRobot(robotCombo.getItems()[robotCombo.getSelectionIndex()]);
					parent.close();
				} catch(Exception excp) {
					MessageBox message = new MessageBox(RobotConfigPanel.this.getShell(), SWT.ICON_WARNING);
					message.setMessage(excp.getMessage());
					message.setText("Error");
					message.open();
				}
			}
			
		});
		
		Button cancelButton = new Button(this, SWT.NONE);
		cancelButton.setText("Cancel");
		cancelButton.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}


			public void widgetSelected(SelectionEvent arg0) {
				
				try {
					RobotManager.getInstance().setActiveRobot(robotCombo.getItems()[robotCombo.getSelectionIndex()]);
					parent.close();
				} catch(Exception excp) {
					MessageBox message = new MessageBox(RobotConfigPanel.this.getShell(), SWT.ICON_WARNING);
					message.setMessage(excp.getMessage());
					message.setText("Error");
					message.open();
				}
			}
			
		});*/
		
		
		
		
		
		
		
		
		
		
		
		
		/*************************************************/
		
		
		
		final List roboList = new List(this, SWT.BORDER | SWT.V_SCROLL);
		FormData roboListData = new FormData();
		roboListData.top = new FormAttachment(0,0);
		roboListData.bottom = new FormAttachment(10,0);
		roboListData.left = new FormAttachment(0,0);
		roboListData.right = new FormAttachment(100,0);
		//roboList.setLayoutData(roboListData);
		
	    roboList.setBounds(50, 50, 50, 50);
	    roboList.setItems(RobotManager.getInstance().getRobotNames());
	    final Button b1 = new Button(this, SWT.PUSH | SWT.BORDER);
	    b1.setBounds(150, 150, 50, 25);
	    b1.setText("save");
	    b1.addSelectionListener(new SelectionAdapter() {

	    	

			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}


			public void widgetSelected(SelectionEvent arg0) {
				
				try {
					RobotManager.getInstance().setActiveRobot(roboList.getItems()[roboList.getSelectionIndex()]);
					parent.close();
				} catch(Exception excp) {
					MessageBox message = new MessageBox(RobotConfigPanel.this.getShell(), SWT.ICON_WARNING);
					message.setMessage(excp.getMessage());
					message.setText("Error");
					message.open();
				}
			}
	    	
	    	
	    	
	    });
		
		
		/****************************************************/
		
		
		
		
		
		
		
		
		
	}
}
