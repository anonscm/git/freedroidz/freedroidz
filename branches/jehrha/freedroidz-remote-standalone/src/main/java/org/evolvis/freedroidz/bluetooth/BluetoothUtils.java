package org.evolvis.freedroidz.bluetooth;

import java.io.IOException;
import java.util.ArrayList;


import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import javax.bluetooth.UUID;

public class BluetoothUtils implements DiscoveryListener {
	
	public ArrayList <RemoteDevice> deviceList = new ArrayList<RemoteDevice>();
	
	boolean statusOk = false;
	
	boolean serviceDiscovered = false;

	protected UUID uuid = new UUID(0x1101); // serial port profile

	protected int inquiryMode = DiscoveryAgent.GIAC;

	protected int connectionOptions = ServiceRecord.NOAUTHENTICATE_NOENCRYPT;
	
	public BluetoothCallback callback;
	
	public BluetoothUtils(BluetoothCallback callback){
		this.callback = callback;
	}
	
	
	public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
		System.out.println("A device discovered (" + getDeviceStr(btDevice)
				+ ")");
		deviceList.add(btDevice);
		callback.newDeviceFound(getDeviceStr(btDevice));
	}

	public void servicesDiscovered(int transId, ServiceRecord[] records) {
		System.out.println("Service discovered.");
		for (int i = 0; i < records.length; i++) {
			System.out.println("records: " + records.length);
			ServiceRecord rec = records[i];
			String url = rec.getConnectionURL(connectionOptions, false);
			serviceDiscovered = true;
			statusOk = true;
		}
	}


	private void startServiceSearch(RemoteDevice device) {
		try {
			System.out.println("Start search for Serial Port Profile service from "+ getDeviceStr(device));
			UUID uuids[] = new UUID[] { uuid };
			serviceDiscovered = false;
			getAgent().searchServices(null, uuids, device, this);
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	private String getDeviceStr(RemoteDevice btDevice) {
		return getFriendlyName(btDevice) + " - 0x"
				+ btDevice.getBluetoothAddress();
	}
	
	public String getDeviceMAC(RemoteDevice btDevice){
		return btDevice.getBluetoothAddress();
	}

	private String getFriendlyName(RemoteDevice btDevice) {
		try {
			return btDevice.getFriendlyName(false);
		} catch (IOException e) {
			return "no name available";
		}
	}

	private DiscoveryAgent getAgent() {
		try {
			return LocalDevice.getLocalDevice().getDiscoveryAgent();
		} catch (BluetoothStateException e) {
			System.err.println(e);
			System.err.println("ERROR detected and all operations stopped.");
			throw new Error("No discovery agent available.");
		}
	}

	public void startDeviceInquiry() {
		try {
			System.out
					.println("Scanning for Devices...");
			deviceList.clear();
			getAgent().startInquiry(inquiryMode, this);
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	public void serviceSearchCompleted(int transID, int respCode) {
		String msg = null;
		switch (respCode) {
		case SERVICE_SEARCH_COMPLETED:
			msg = "the service search completed normally";
			break;
		case SERVICE_SEARCH_TERMINATED:
			msg = "the service search request was cancelled by a call to DiscoveryAgent.cancelServiceSearch()";
			break;
		case SERVICE_SEARCH_ERROR:
			msg = "an error occurred while processing the request";
			break;
		case SERVICE_SEARCH_NO_RECORDS:
			msg = "no records were found during the service search";
			break;
		case SERVICE_SEARCH_DEVICE_NOT_REACHABLE:
			msg = "the device specified in the search request could not be reached or the local device could not establish a connection to the remote device";
			break;
		}
		System.out.println("Service search completed - " + msg);
		if(!serviceDiscovered) {
			System.out.println("no service found!");
//			ControlClient.getInstance().publishErrorAsync("Error", "Opreco Server not running!");
		}
		
	
	}
	
	

	public void inquiryCompleted(int discType) {
		System.out.println("Search finished");

	}
	
}
