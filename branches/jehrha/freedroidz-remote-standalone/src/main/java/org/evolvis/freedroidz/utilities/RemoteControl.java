package org.evolvis.freedroidz.utilities;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import lejos.pc.comm.FileInfo;
import lejos.pc.comm.NXTComm;
import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTCommand;
import lejos.pc.comm.NXTInfo;
import lejos.pc.tools.SendFile;

public class RemoteControl {

	InputStream in;
	DataInputStream din;
	OutputStream out;
	DataOutputStream dout;

	static NXTInfo nxtInfo;
	NXTComm comm;

	String nxtAdress = null; // "00:16:53:03:66:B3";
	String nxtName = null;

	public int PortA = 0;
	public int PortB = 1;
	public int PortC = 2;

	int leftMotor = PortA;
	int centerMotor = PortB;
	int rightMotor = PortC;

	int leftSpeed = 500;
	int centerSpeed = 500;
	int rightSpeed = 500;

	boolean reverseDirection = false;

	public RemoteControl(String nxtName, String nxtAdress) {
		this.nxtAdress = nxtAdress;
		this.nxtName = nxtName;
	}

	public RemoteControl(String nxtAdress) {
		this.nxtAdress = nxtAdress;
		
		try {
			comm = NXTCommFactory.createNXTComm(NXTCommFactory.BLUETOOTH);
		} catch (NXTCommException e) {
			e.printStackTrace();
		}
		nxtInfo = new NXTInfo("NXT", nxtAdress);
	}

	public RemoteControl(NXTInfo nxtInfo) {
		this.nxtInfo = nxtInfo;
	}

	public void initConnection() throws NXTCommException, IOException {
		
//		checkRemoteControl(nxtInfo);
		comm = NXTCommFactory.createNXTComm(NXTCommFactory.BLUETOOTH);
		comm.open(nxtInfo);

		in = comm.getInputStream();
		din = new DataInputStream(in);
		out = comm.getOutputStream();
		dout = new DataOutputStream(out);

	}

	public void closeConnection() throws IOException {
		
		int[] close = new int[3];
		close[0] = 666;
		close[1] = 666;
		close[2] = 666;
		
		sendCommand(close);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		din.close();
		dout.close();
	}

	public void sendCommand(int[] motor) {

		if (reverseDirection) {
			motor[0] = motor[0] * -1;
			motor[1] = motor[1] * -1;
			motor[2] = motor[2] * -1;
		}

		byte[] b = new byte[12];

		intToByteArray(b, 0, motor[0]);
		intToByteArray(b, 4, motor[1]);
		intToByteArray(b, 8, motor[2]);

		try {
			dout.write(b);
			dout.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void foreward() {
		int[] dire = new int[3];

		dire[leftMotor] = leftSpeed;
		dire[rightMotor] = rightSpeed;

		sendCommand(dire);
	}

	public void backward() {
		int[] dire = new int[3];

		dire[leftMotor] = -1 * leftSpeed;
		dire[rightMotor] = -1 * rightSpeed;

		sendCommand(dire);
	}

	public void turnLeft() {
		int[] dire = new int[3];

		dire[leftMotor] = -1 * leftSpeed;
		dire[rightMotor] = rightSpeed;

		sendCommand(dire);
	}

	public void turnRight() {
		int[] dire = new int[3];

		dire[leftMotor] = leftSpeed;
		dire[rightMotor] = -1 * rightSpeed;

		sendCommand(dire);
	}

	public void rightMotor(int[] motor, int speed) {
		motor[rightMotor] = speed;
	}

	public void leftMotor(int[] motor, int speed) {
		motor[leftMotor] = speed;
	}

	public void centerMotor(int[] motor, int speed) {
		motor[centerMotor] = speed;
	}

	public void flushCommand(int[] command) {
		sendCommand(command);
	}

	public void stop() {
		int[] dire = new int[3];

		dire[leftMotor] = 0;
		dire[rightMotor] = 0;
		dire[centerMotor] = 0;

		sendCommand(dire);
	}

	public void setRightMotor(int port) {
		rightMotor = port;
	}

	public void setCenterMotor(int port) {
		centerMotor = port;
	}

	public void setLeftMotor(int port) {
		leftMotor = port;
	}

	public void setLeftSpeed(int speed) {
		leftSpeed = speed;
	}

	public void setCenterSpeed(int speed) {
		centerSpeed = speed;
	}

	public void setRightSpeed(int speed) {
		rightSpeed = speed;
	}

	public void reverseDirection(boolean reverse) {
		this.reverseDirection = reverse;
	}

	private void intToByteArray(byte[] input, int offset, int value) {
		input[offset] = (byte) (value >>> 24);
		input[offset + 1] = (byte) (value >>> 16);
		input[offset + 2] = (byte) (value >>> 8);
		input[offset + 3] = (byte) value;
	}

	private boolean remoteControlExists(NXTCommand nxtCommand)
			throws NXTCommException {


		int numFiles = 0;
		FileInfo files[] = new FileInfo[30];
		try {
			files[0] = nxtCommand.findFirst("*.*");
			if(files[0].fileName.equals("RemoteControl.nxj"))
				return true;
			
			if (files[0] != null) {
				numFiles = 1;

				for (int i = 1; i < 30; i++) {
					files[i] = nxtCommand.findNext(files[i - 1].fileHandle);
					if (files[i] == null)
						break;
					else {
						numFiles++;
						if(files[i].fileName.equals("RemoteControl.nxj"))
							return true;
					}
				}
			}
		} catch (IOException ioe) {
		}
		return false;
	}
	
	private void checkRemoteControl(NXTInfo nxtInfo){
		NXTCommand nxtCommand = new NXTCommand();
		NXTCommand nxtCommand2 = new NXTCommand();
		try {
			nxtCommand.open(nxtInfo);
			if(!remoteControlExists(nxtCommand)){
				File f = new File("/tmp/RemoteControl.nxj");
				writeToFile(this.getClass()
				.getResourceAsStream(
						"/org/evolvis/freedroidz/nxtUtilities/RemoteControl.nxj"), f);
				
				SendFile.sendFile(nxtCommand, f);
			}		
			
			
			Thread.sleep(1000);
			nxtCommand.close();
			Thread.sleep(5000);
			nxtCommand2.open(nxtInfo);
			
			nxtCommand2.startProgram("RemoteControl.nxj");

			nxtCommand2.close();
			Thread.sleep(5000);
			
		} catch (NXTCommException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	public void writeToFile(InputStream is, File file) {
		try {
			DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
			int c;
			while((c = is.read()) != -1) {
				out.writeByte(c);
			}
			is.close();
			out.close();

		}
		catch(IOException e) {
			System.err.println("Error Writing/Reading Streams.");
		}
	}

}
