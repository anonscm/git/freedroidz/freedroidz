/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.freedroidz;

import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;

import org.evolvis.freedroidz.icommand.Services;
import org.evolvis.freedroidz.icommand.config.IcommandProperties;
import org.evolvis.freedroidz.ui.AboutPanel;
import org.evolvis.freedroidz.ui.OpenMokoMotioncontrolPanel;
import org.evolvis.freedroidz.ui.RemoteControlPanel;
import org.evolvis.xana.swt.utils.SWTIconFactory;
import org.evolvis.xana.utils.IconFactoryException;

/**  
 * Class to start the freedroidz-remot-standalone-application
 * 
 * @author Joscha Haering, tarent GmbH
 */
public class Starter {

	protected final static Logger logger = Logger.getLogger(Starter.class.getName());

	// paths to the freedroidz-icon in different sizes
	protected final static String[] imagePaths = new String[] {
		"/usr/share/icons/hicolor/16x16/apps/freedroidz.png", //$NON-NLS-1$
		"/usr/share/icons/hicolor/22x22/apps/freedroidz.png", //$NON-NLS-1$
		"/usr/share/icons/hicolor/26x26/apps/freedroidz.png", //$NON-NLS-1$
		"/usr/share/icons/hicolor/32x32/apps/freedroidz.png", //$NON-NLS-1$
		"/usr/share/icons/hicolor/40x40/apps/freedroidz.png", //$NON-NLS-1$
		"/usr/share/icons/hicolor/48x48/apps/freedroidz.png", //$NON-NLS-1$
		"/usr/share/icons/hicolor/64x64/apps/freedroidz.png", //$NON-NLS-1$
		"/usr/share/icons/hicolor/128x128/apps/freedroidz.png", //$NON-NLS-1$
	}; 

	public static void main(String[] args) {
		
		Display.setAppName("freedroidz"); //$NON-NLS-1$

		final Shell shell = new Shell(new Display());
		shell.setText("freedroidz"); //$NON-NLS-1$

		Services.getInstance().setShell(shell);

		// Init Icon-dirs
		try {
			SWTIconFactory.getInstance().addResourcesFolder(Starter.class, "/org/evolvis/freedroidz/gfx/"); //$NON-NLS-1$
		} catch (IconFactoryException e1) {
			// TODO Auto-generated catch block
			e1.getMessage();
		}

		Display.getCurrent().addFilter(SWT.KeyDown, new Listener() {

			/**
			 * @see org.eclipse.swt.widgets.Listener#handleEvent(org.eclipse.swt.widgets.Event)
			 */
			public void handleEvent(Event event) {
				if (event.keyCode == SWT.F6)
					shell.setFullScreen(!shell.getFullScreen());
			}

		});


		// set window icons
		Image[] images = new Image[imagePaths.length];

		try {
			for(int i=0; i < images.length; i++)
				images[i] = new Image(shell.getDisplay(), imagePaths[i]);

			shell.setImages(images);
		} catch(SWTException excp) {
			logger.warning("could not load window-icons: "+excp); //$NON-NLS-1$
		}

		final Display display = Display.getCurrent();

		shell.setLayout(new FormLayout());
		
		

		Menu menuBar = new Menu(shell, SWT.BAR);
		shell.setMenuBar(menuBar);

		MenuItem fileItem =  new MenuItem(menuBar, SWT.CASCADE);
		fileItem.setText("File"); //$NON-NLS-1$

		Menu submenu = new Menu (shell, SWT.DROP_DOWN);
		fileItem.setMenu (submenu);

	
		MenuItem aboutItem = new MenuItem (submenu, SWT.PUSH);
		aboutItem.setText("About"); //$NON-NLS-1$
		aboutItem.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				Shell dialog = new Shell (shell);
				dialog.setText ("Info"); //$NON-NLS-1$
				dialog.setLayout(new FillLayout());
				@SuppressWarnings("unused")
				AboutPanel aboutPanel = new AboutPanel(dialog);

				dialog.pack();
				dialog.open ();
				while (!shell.isDisposed ()) {
					if (!display.readAndDispatch ()) display.sleep ();
				}
			}
		});

		MenuItem quitItem = new MenuItem (submenu, SWT.PUSH);
		quitItem.setText("Quit"); //$NON-NLS-1$
		quitItem.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				quitApplication();
			}
		});

	
		
		
		
		Image remoteIcon = new Image(display, Starter.class.getResourceAsStream("/org/evolvis/freedroidz/gfx/input-gaming.png"));
		Button remoteButton = new Button(shell, 0);
		remoteButton.setText("Start Remote-Control"); //$NON-NLS-1$
		remoteButton.setBounds(200, 200, 200, 80);
		remoteButton.setImage(remoteIcon);
		
		remoteButton.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent arg0) {		
			}

			public void widgetSelected(SelectionEvent arg0) {
				
				Shell dialog = new Shell (Display.getDefault());
				dialog.setText ("Lejos - Remote control");
				dialog.setBounds(Display.getDefault().getBounds().width/2-180,Display.getDefault().getBounds().height/2-125,360,250);
				dialog.setLayout(new FillLayout());
		
				if(!System.getProperty("os.arch").equals("arm")){
					RemoteControlPanel remoteControlPanel = new RemoteControlPanel(dialog);
				}else{
					OpenMokoMotioncontrolPanel openMokoMotionControlPanel = new OpenMokoMotioncontrolPanel(dialog);
				}
				if(!dialog.isDisposed())
					dialog.open ();
			}
		});
		
		
		
		
		
		
		
		
		
		
		

		shell.open();
		
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		quitApplication();
	}

	protected static void quitApplication() {

		// Remove icommand.properties file
		IcommandProperties.removeConfiguration();

		System.exit(0);
	}
}
