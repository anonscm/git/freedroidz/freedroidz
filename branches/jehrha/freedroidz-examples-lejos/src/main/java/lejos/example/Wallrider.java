/**
 * Der Wallrider erkennt mit dem Ultraschallsensor wo eine Wand ist und fährt so an ihr vorbei
 * 
 */

package lejos.example;


import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.Motor;
import lejos.robotics.navigation.Pilot;
import lejos.robotics.navigation.TachoPilot;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;

public class Wallrider {
	Pilot pilot;

	/**
	 * @param args
	 */

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Wallrider rider = new Wallrider();
		// die Pilotklasse dient zum steuern des Roboters
		// sie benoetigt die Reifengroesse, Den Radabstand und welche Motoren
		rider.pilot = new TachoPilot(5.5f, 9f, Motor.B, Motor.C);
		rider.go();

	}

	public void go() {
		// Das Programm laeuft so lange bis der Escapebutton gedrueckt wurde
		while (!Button.ESCAPE.isPressed()) {

			LCD.drawString("=Wallrider=", 2, 0);
			// Neuer Ultraschallsensor an SensorPort 2

			UltrasonicSensor sonic = new UltrasonicSensor(SensorPort.S2);
			LCD.drawString(sonic.getSensorType(), 13, 7);
			LCD.drawInt(sonic.getDistance(), 0, 7);

			while (sonic.getDistance() > 20 && !Button.ESCAPE.isPressed()) {
				LCD.drawString("Alles klar.", 0, 2);
				LCD.drawString("Fahr ruhig!", 0, 3);
				pilot.forward();
			}
			if (sonic.getDistance() < 20) {
				LCD.drawString("Aaaahhh!!", 0, 2);
				LCD.drawString("Eine Wand!!", 0, 3);
				pilot.travel(-8);
				pilot.rotate(130);
			}

		}

	}

}
