package lejos.example.square;

import lejos.nxt.Motor;
import lejos.robotics.navigation.Pilot;
import lejos.robotics.navigation.TachoPilot;

public class Quadrat {
	Pilot pilot;
	
	public static void main(String[] args) {
		Quadrat quadrat = new Quadrat();
		quadrat.pilot = new TachoPilot(56, 173, Motor.A, Motor.C);
		quadrat.run();
	}
	
	private void run() {
		pilot.travel(1000);
		pilot.rotate(900);
		pilot.travel(1000);
		pilot.rotate(900);
		pilot.travel(1000);
		pilot.rotate(900);
		pilot.travel(1000);
		pilot.rotate(900);
	}
}
