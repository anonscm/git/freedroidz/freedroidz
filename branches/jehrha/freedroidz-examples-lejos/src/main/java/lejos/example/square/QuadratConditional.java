package lejos.example.square;

import lejos.nxt.Button;
import lejos.nxt.Motor;
import lejos.robotics.navigation.Pilot;
import lejos.robotics.navigation.TachoPilot;

public class QuadratConditional {
	Pilot pilot;

	public static void main(String[] args) {
		QuadratConditional quadrat = new QuadratConditional();
		quadrat.pilot = new TachoPilot(56, 173, Motor.A, Motor.C);
		quadrat.run();
	}

	private void run() {
		int entfernung = 800;
		int drehen = 180;

		for (int i = 0; i < 100; i++) {

			if (i < 5) {
				pilot.travel(entfernung);
				pilot.rotate(drehen);
			} else {
				System.out.println("Die Durchlaufvariable ist bei " + i);
			}
		}
		Button.waitForPress();
	}

}
