package lejos.example.legolem;

import lejos.robotics.navigation.TachoPilot;

import lejos.nxt.Button;
import lejos.nxt.ButtonListener;
import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;

public class LeGolem {
	private int maxLightValue = 0;
	private int minLightValue = Integer.MAX_VALUE;
	private int minLightDirectionEnter = 0;
	private int maxLightDirectionEnter = 0;
	private int minLightDirectionExit = 0;
	private int maxLightDirectionExit = 0;
	private TachoPilot pilot;
	private LightSensor lightSensor;
	private UltrasonicSensor usSensor;
	private boolean calibrating;
	protected boolean shouldBeRunning = true;
	private boolean walking;
	private int currentLightValue;
	private int currentLightDelta;
	private int currentHeading;
	private int lastHeadingDelta;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		final LeGolem leGolem = new LeGolem();

		new Thread() {
			public void run() {
				while (leGolem.shouldBeRunning) {
					int value = leGolem.lightSensor.readNormalizedValue();
					leGolem.lightSensorStateChanged(value);

				}
			}
		}.start();
		new Thread() {
			public void run() {
				while (leGolem.shouldBeRunning) {
					// leGolem.usSensor.getSensorType();
					// leGolem.usSensor.getVersion();
					int value = leGolem.usSensor.getDistance();
					leGolem.usSensorStateChanged(value);
				}
			}
		}.start();

		Button.LEFT.addButtonListener(new ButtonListener() {

			public void buttonPressed(Button arg0) {
				leGolem.lightSensor.calibrateLow();
			}

			public void buttonReleased(Button arg0) {
			}
		});
		Button.RIGHT.addButtonListener(new ButtonListener() {

			public void buttonPressed(Button arg0) {
				leGolem.lightSensor.calibrateHigh();
			}

			public void buttonReleased(Button arg0) {
			}
		});
		Button.ENTER.addButtonListener(new ButtonListener() {

			public void buttonPressed(Button arg0) {
				//Sound.beepSequence();
				leGolem.lookIntoTheSun();
				leGolem.walkToTheWall();
				leGolem.correctHeading();
				leGolem.turnAbsolute(-90);
				leGolem.walkToTheWall();
				leGolem.turnAbsolute(0);
				leGolem.walkToTheWall();
				leGolem.turnAbsolute(180);
				
			}

			public void buttonReleased(Button arg0) {
			}
		});
		Button.ESCAPE.addButtonListener(new ButtonListener() {

			public void buttonPressed(Button arg0) {
				leGolem.shouldBeRunning = false;
			}

			public void buttonReleased(Button arg0) {
			}
		});
		while (leGolem.shouldBeRunning) {
			Thread.yield();
		}

	}

	protected void usSensorStateChanged(int value) {
		if (!walking) {
			return;
		}
		if (value < 15) {
			walking = false;
		}
	}

	protected void walkToTheWall() {
		walking = true;
		pilot.forward();
		while (walking) {
			Thread.yield();
		}
		pilot.stop();
	}

	
	private void lookIntoTheSun() {

		try {// give some time for the human to stand clear from the robot.
			Thread.sleep(1500);
		} catch (InterruptedException e) {

		}

		resetExtrema();
		pilot.reset();
		calibrating = true;
		pilot.rotate(-360);
		
		if(currentLightValue==maxLightValue){
			throw new RuntimeException("bla");
			//TODO: if we are on a maximum platau, we have to keep turning, until we have found the end.
		}
		
		calibrating = false;

		pilot.reset();
		if(Math.abs(maxLightDirectionEnter-maxLightDirectionExit)>1){
			throw new RuntimeException("bla");
		}
		
		correctHeading();
		//pilot.resetTachoCount();
		/*
		 * we should be looking roughly into the right direction, correct
		 * direction, so it is unlikely that we get stuck in some local
		 * extremum. We can now try some fine tuning.
		 */
	}

	private void correctHeading() {
		while(currentLightDelta!=0){
			turnAbsolute(0);
		}
	}

	private void turnAbsolute(int absoluteHeading) {
		pilot.rotate((int) (0.5f*(maxLightDirectionEnter+maxLightDirectionExit)+absoluteHeading-pilot.getAngle()));
		pilot.stop();
	}

	private void resetExtrema() {
		maxLightValue = 0;
		minLightValue = Integer.MAX_VALUE;
		minLightDirectionEnter = 0;
		maxLightDirectionEnter = 0;
		minLightDirectionExit = 0;
		maxLightDirectionExit = 0;
		currentLightDelta=0;		
		currentLightValue=0;
	}

	public LeGolem() {
		pilot = new TachoPilot(56, 105, Motor.B, Motor.C);

		lightSensor = new LightSensor(SensorPort.S2);
		lightSensor.setFloodlight(false);
		usSensor = new UltrasonicSensor(SensorPort.S1);

	}

	protected void lightSensorStateChanged(int newVal) {
//		if (!calibrating) {
//			return;
//		}
		float newHeading = pilot.getAngle();
		if(newHeading!=currentHeading){
			lastHeadingDelta=(int) (newHeading-currentHeading);
		}
		currentLightDelta=newVal-currentLightValue;
		currentLightDelta=newVal-currentLightValue;
		
		if (newVal > maxLightValue) {
			maxLightValue = newVal;
			maxLightDirectionEnter = maxLightDirectionExit = (int) newHeading;
		}
		else if (newVal == maxLightValue) {
			// is this a plateau? Or a second maximum?
			if (currentLightDelta==0) { // plateau
				maxLightValue = newVal;
				maxLightDirectionExit = (int) newHeading;
			}else{ //another maximum
				/* in theory there could be more than one maximum plateau. 
				 * Not much we can do about it. We simply pick the last one we encounter. 
				 */
				maxLightDirectionEnter = maxLightDirectionExit = (int) pilot.getAngle();
			}
		}
		currentLightValue = newVal;
		currentHeading=(int) pilot.getAngle();
	}

}
