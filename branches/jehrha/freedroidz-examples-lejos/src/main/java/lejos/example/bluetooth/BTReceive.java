package lejos.example.bluetooth;

import lejos.nxt.*;
import lejos.nxt.comm.*;
import java.io.*;

/**
 * Receive data from another NXT, a PC, a phone, or another bluetooth device.
 * 
 * Waits for a connection, receives an int and returns its negative as a reply,
 * 100 times, and then closes the connection, and waits for a new one.
 * 
 * @author Lawrie Griffiths
 * 
 */
public class BTReceive {
	public static void main(String[] args) throws Exception {
		String connected = "Connected";
		String waiting = "Waiting...";
		String closing = "Closing...";
		while (true) {
			LCD.drawString(waiting, 0, 0);
			LCD.refresh();
			BTConnection btc = Bluetooth.waitForConnection();
			LCD.clear();
			LCD.drawString(connected, 0, 0);
			LCD.refresh();
			DataInputStream dis = btc.openDataInputStream();
			DataOutputStream dos = btc.openDataOutputStream();

			if (dis.readInt()<5){
				LCD.drawInt(dis.readInt(),0, 1);
			}
			else if (dis.readInt()==5){
				LCD.refresh();
				LCD.drawString("dance",0, 1);

			}
			
			/*for (int i = 0; i < 100; i++) {
				int n = dis.readInt();
				LCD.drawInt(n, 7, 0, 1);
				LCD.refresh();
				dos.writeInt(-n);
				dos.flush();
			}*/
			dis.close();
			dos.close();
			Thread.sleep(100); // wait for data to drain
			LCD.clear();
			LCD.drawString(closing, 0, 0);
			LCD.refresh();
			btc.close();
			LCD.clear();
		}
	}
}
