package lejos.example.walker1;

import lejos.robotics.navigation.Pilot;
import lejos.robotics.navigation.TachoPilot;

import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;

public class Walker1 {

	public static int PREFERED_DISTANCE = 15;

	static float diameter = 56; // Reifendurchmesser in mm

	static float trackWidth = 105; // Abstand der Reifen in mm

	public static boolean shouldBeRunning;

	public static void main(String args[]) {

		Pilot pilot = new TachoPilot(diameter, trackWidth, Motor.A, Motor.C);
		LightSensor lightSensor = new LightSensor(SensorPort.S2);
		UltrasonicSensor usSensor = new UltrasonicSensor(SensorPort.S1);
		lightSensor.setFloodlight(false);
		usSensor.getVersion();

		pilot.reset(); // Tacho beider Motoren auf 0 setzen
		pilot.setSpeed(200);

		/*
		 * int brightestLight = 0; int brightestLightAngle = 0;
		 * pilot.rotate(400, true); while(pilot.getAngle() < 360){ int tmp =
		 * lightSensor.readNormalizedValue(); if(tmp > brightestLight){
		 * brightestLight = tmp; brightestLightAngle = pilot.getAngle(); } }
		 * pilot.rotate(brightestLightAngle-pilot.getAngle());
		 */

		alignWithNearestWall(pilot, usSensor);
		
		/*Button.ESCAPE.addButtonListener(new ButtonListener() {

			public void buttonPressed(Button arg0) {
				// TODO Auto-generated method stub

			}

			public void buttonReleased(Button arg0) {
				shouldBeRunning = false;

			}
		});*/
		shouldBeRunning = true;
		int turns=0;
		while (shouldBeRunning) {
			walkToWall(pilot, usSensor);
			
			switch(turns){}
			if(turns<4){
				pilot.rotate(90);
			}else{
				
				alignWithNearestWall(pilot, usSensor);
				turns=0;
			}
			turns++;
		}
	}

	private static void alignWithNearestWall(Pilot pilot,
			UltrasonicSensor usSensor) {
		turnTowardsNearestWall(pilot, usSensor);
		walkToWall(pilot, usSensor);
		pilot.rotate(90);
	}

	private static void walkToWall(Pilot pilot, UltrasonicSensor usSensor) {
		pilot.forward();
		while (usSensor.getDistance() > PREFERED_DISTANCE) {
			// einfach nur weiterfahren.
		}
		pilot.backward();
		while (usSensor.getDistance() < PREFERED_DISTANCE) {
			// einfach nur weiterfahren.
		}
		pilot.stop();
	}

	private static void turnTowardsNearestWall(Pilot pilot,
			UltrasonicSensor usSensor) {
		int shortestDistance = 10000; // irgendwas grosses
		int shortestDistanceAngle = 0;

		pilot.rotate(-90);
		pilot.reset();
		pilot.rotate(180, true);
		while (pilot.getAngle() < 180) {
			int tmp = usSensor.getDistance();
			if (tmp < shortestDistance) {
				shortestDistance = tmp;
				shortestDistanceAngle = (int) pilot.getAngle();
			}
		}
		pilot.rotate(shortestDistanceAngle - pilot.getAngle());
	}
	
	private static void turnToMaxDistance(Pilot pilot,
			UltrasonicSensor usSensor) {
		int greatestDistance = 0; // irgendwas grosses
		int greatestDistanceAngle = 0;

		
		pilot.reset();
		pilot.rotate(400, true);
		while (pilot.getAngle() < 360) {
			int tmp = usSensor.getDistance();
			if (tmp > greatestDistance) {
				greatestDistance = tmp;
				greatestDistanceAngle = (int) pilot.getAngle();
			}
		}
		pilot.stop();
		pilot.rotate(greatestDistanceAngle - pilot.getAngle());
	}
}
