package lejos.example.lightdetector;

import lejos.robotics.navigation.TachoPilot;
import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;

public class LightDetector {
	
	static float diameter = 5.7f;	//Reifendurchmesser in cm
	static float trackWidth = 10.6f;	//Abstand der Reifen in cm
	
	public static void main(String args[]){
		
		TachoPilot pilot = new TachoPilot(diameter, trackWidth, Motor.A, Motor.B);
		LightSensor lightSensor = new LightSensor(SensorPort.S2);
		UltrasonicSensor usSensor = new UltrasonicSensor(SensorPort.S1);
		lightSensor.setFloodlight(false);
		
		pilot.reset();	//Tacho beider Motoren auf 0 setzen
		pilot.setSpeed(200);
		
			
		int brightestLight = 0;
		int brightestLightAngle = 0;
		pilot.rotate(400, true);
		while(pilot.getAngle() < 360){
			int tmp = lightSensor.readNormalizedValue();
			if(tmp > brightestLight){
				brightestLight = tmp;
				brightestLightAngle = (int) pilot.getAngle();
			}
		}	
		pilot.rotate(brightestLightAngle-pilot.getAngle());
		
		
		int shortestDistance = 0;
		int shortestDistanceAngle = 0;

		pilot.reset();
		pilot.rotate(-45);
		pilot.rotate(100, true);
		while(pilot.getAngle() < 45){
			int tmp = usSensor.getDistance();
			if(tmp < shortestDistance){
				shortestDistance = tmp;
				shortestDistanceAngle = (int) pilot.getAngle();
			}
		}	
		pilot.rotate(shortestDistanceAngle-pilot.getAngle());
	}
}
