package lejos.examples.RubiksCubeSolver;

import lejos.nxt.Motor;

public class MotorWrapper implements IMotor {

	Motor motor;
	
	public MotorWrapper(Motor motor) {
		super();
		this.motor = motor;
	}

	/* (non-Javadoc)
	 * @see lejos.examples.RubiksCubeSolver.IMotor#setPower(int)
	 */
	@Override
	public void setPower(int i) {
		motor.setPower(i);
	}

	/* (non-Javadoc)
	 * @see lejos.examples.RubiksCubeSolver.IMotor#rotate(int)
	 */
	@Override
	public void rotate(int i) {
		motor.rotate(i);
		
	}

}
