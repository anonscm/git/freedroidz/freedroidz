package lejos.examples.RubiksCubeSolver;


/*
 * The Scanner-class scans the cube and saves the color of the cubies
 * 
 */

public class Scanner {
	
	

	private Robot robot;

	private final Hardware hardware;
	
	
	public Scanner(Robot robot, Hardware hw) {
		super();
		this.robot = robot;
		this.hardware = hw;
	}


	private void sleep(int i) {
		
		try
		{
			Thread.sleep(i);
		}catch(InterruptedException e)
		{}
	}


	//starts to scan
	public void scanCube()
	{
		sideOne();
		armtwist();
		sideTwo();
		cubetwist();
		armtwist();
		cubetwistInvert();
		sideThree();
		armtwist();
		cubetwist();
		sideFour();
		armtwist();
		sideFive();
		cubetwistInvert();
		armtwist();
		cubetwist();
		sideSix();
		armtwist();
		cubetwistInvert();
	}
	
	private void armtwist()
	{
		hardware.getMotorC().setPower(100);

		hardware.getMotorC().rotate(82);
		sleep(500);
		hardware.getMotorC().rotate(-82);
		sleep(500);
	}
	
	private void cubetwist()
	{
		hardware.getMotorB().rotate(-315);
	}
	
	private void cubetwistInvert()
	{
		hardware.getMotorB().rotate(315);
	}
	
	//saves to color from the first side
	private void sideOne()
	{
		//EdgeCubie color check
		hardware.getMotorA().rotate(100);
		hardware.getMotorB().rotate(46);
		
		sleep(500);
		getRobot().edgeCubie[0].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().edgeCubie[1].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().edgeCubie[2].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().edgeCubie[3].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
			
		hardware.getMotorB().rotate(-45);
		
		
		//MidCubie color check
		hardware.getMotorA().rotate(20);
		sleep(500);
		getRobot().midCubie[0].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		
		
		//CornerCubie color check
		hardware.getMotorA().rotate(25);
		
		sleep(500);
		getRobot().cornerCubie[0].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().cornerCubie[1].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().cornerCubie[2].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().cornerCubie[3].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
				
		hardware.getMotorA().rotate(-146);
	}


	
	public Robot getRobot() {
		return robot;
	}
	
	public void setRobot(Robot robot) {
		this.robot = robot;
	}


	private void sideTwo()
	{
		//EdgeCubie color check
		hardware.getMotorA().rotate(100);
		hardware.getMotorB().rotate(46);
		
		sleep(500);
		getRobot().edgeCubie[2].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().edgeCubie[4].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().edgeCubie[5].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().edgeCubie[6].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
			
		hardware.getMotorB().rotate(-45);
		
		
		//MidCubie color check
		hardware.getMotorA().rotate(20);
		sleep(500);
		getRobot().midCubie[1].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		
		
		//CornerCubie color check
		hardware.getMotorA().rotate(25);
		
		sleep(500);
		getRobot().cornerCubie[4].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().cornerCubie[5].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().cornerCubie[1].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().cornerCubie[0].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
				
		hardware.getMotorA().rotate(-146);
	}
	
	private void sideThree()
	{
		//EdgeCubie color check
		hardware.getMotorA().rotate(100);
		hardware.getMotorB().rotate(46);
		
		sleep(500);
		getRobot().edgeCubie[1].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().edgeCubie[10].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().edgeCubie[7].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().edgeCubie[4].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
			
		hardware.getMotorB().rotate(-45);
		
		
		//MidCubie color check
		hardware.getMotorA().rotate(20);
		sleep(500);
		getRobot().midCubie[5].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		
		
		//CornerCubie color check
		hardware.getMotorA().rotate(25);
		
		sleep(500);
		getRobot().cornerCubie[6].Color3 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().cornerCubie[4].Color3 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().cornerCubie[0].Color3 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().cornerCubie[3].Color3 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
				
		hardware.getMotorA().rotate(-146);
	}
	
	private void sideFour()
	{
		//EdgeCubie color check
		hardware.getMotorA().rotate(100);
		hardware.getMotorB().rotate(46);
		
		sleep(500);
		getRobot().edgeCubie[5].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().edgeCubie[7].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().edgeCubie[8].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().edgeCubie[9].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
			
		hardware.getMotorB().rotate(-45);
		
		
		//MidCubie color check
		hardware.getMotorA().rotate(20);
		sleep(500);
		getRobot().midCubie[2].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		
		
		//CornerCubie color check
		hardware.getMotorA().rotate(25);
		
		sleep(500);
		getRobot().cornerCubie[6].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().cornerCubie[7].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().cornerCubie[5].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().cornerCubie[4].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
				
		hardware.getMotorA().rotate(-146);
	}
	
	private void sideFive()
	{
		//EdgeCubie color check
		hardware.getMotorA().rotate(100);
		hardware.getMotorB().rotate(46);
		
		sleep(500);
		getRobot().edgeCubie[8].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().edgeCubie[10].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().edgeCubie[0].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().edgeCubie[11].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
			
		hardware.getMotorB().rotate(-45);
		
		
		//MidCubie color check
		hardware.getMotorA().rotate(20);
		sleep(500);
		getRobot().midCubie[3].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		
		
		//CornerCubie color check
		hardware.getMotorA().rotate(25);
		
		sleep(500);
		getRobot().cornerCubie[3].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().cornerCubie[2].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().cornerCubie[7].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().cornerCubie[6].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
				
		hardware.getMotorA().rotate(-146);
	}
	
	private void sideSix()
	{
		//EdgeCubie color check
		hardware.getMotorA().rotate(100);
		hardware.getMotorB().rotate(46);
		
		sleep(500);
		getRobot().edgeCubie[9].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().edgeCubie[11].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().edgeCubie[3].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().edgeCubie[6].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
			
		hardware.getMotorB().rotate(-45);
		
		
		//MidCubie color check
		hardware.getMotorA().rotate(20);
		sleep(500);
		getRobot().midCubie[4].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		
		
		//CornerCubie color check
		hardware.getMotorA().rotate(25);
		
		sleep(500);
		getRobot().cornerCubie[2].Color3 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().cornerCubie[1].Color3 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().cornerCubie[5].Color3 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
		sleep(500);
		getRobot().cornerCubie[7].Color3 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(314);
				
		hardware.getMotorA().rotate(-146);
	}
	
	private String colorChecker(int colorValue)
	{
		String colorBuffer = "null";
		
		switch(colorValue)
		{
		case 2:
			colorBuffer = Color.BLUE.toString();
			break;
			
		case 3:
			colorBuffer = Color.WHITE.toString();
			break;
			
		case 4:
			colorBuffer = Color.GREEN.toString();
			break;
			
		case 6:
			colorBuffer = Color.YELLOW.toString();
			break;
			
		case 7:
			colorBuffer = Color.ORANGE.toString();
			break;
			
		case 8:
			colorBuffer = Color.ORANGE.toString();
			break;
			
		case 9:
			colorBuffer = Color.RED.toString();
			break;
			
		case 12:
			colorBuffer = Color.WHITE.toString();
			break;
			
		default:
			//Sound.playTone(1760, 2000, 100);
			System.out.println("Fehler");
			System.out.println("Roboter wird ausgeschaltet.");

			sleep(10000);
			System.exit(0);
		}
		
		return colorBuffer;
	}
	
}
