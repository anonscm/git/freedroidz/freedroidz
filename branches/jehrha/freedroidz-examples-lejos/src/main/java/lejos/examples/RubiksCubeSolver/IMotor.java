package lejos.examples.RubiksCubeSolver;

public interface IMotor {

	public abstract void setPower(int i);

	public abstract void rotate(int i);

}