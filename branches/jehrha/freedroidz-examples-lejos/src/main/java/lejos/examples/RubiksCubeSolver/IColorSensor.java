package lejos.examples.RubiksCubeSolver;

public interface IColorSensor {

	int getColorNumber();

}
