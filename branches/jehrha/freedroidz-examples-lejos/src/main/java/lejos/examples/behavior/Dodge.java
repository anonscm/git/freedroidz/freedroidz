package lejos.examples.behavior;

import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;
import lejos.robotics.navigation.Pilot;
import lejos.robotics.navigation.TachoPilot;
import lejos.robotics.subsumption.Behavior;

public class Dodge implements Behavior{

	TouchSensor touchSensor;
	TouchSensor touch2;
	Pilot pilot;
	
	public Dodge(){
		
		touchSensor = new TouchSensor(SensorPort.S1);
		touch2 = new TouchSensor(SensorPort.S3);
		pilot = new TachoPilot(56, 173, Motor.A, Motor.C);
	}
	
	@Override
	public boolean takeControl(){
		
		return touchSensor.isPressed() | touch2.isPressed();
	}
	
	@Override
	public void action(){
		
		pilot.travel(-100);
		pilot.rotate(90);
	}
	
	@Override
	public void suppress(){
		
		pilot.stop();
	}
}
