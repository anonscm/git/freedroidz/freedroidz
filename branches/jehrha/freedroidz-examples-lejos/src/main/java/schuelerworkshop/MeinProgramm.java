package schuelerworkshop;

import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;
import lejos.robotics.navigation.Pilot;
import lejos.robotics.navigation.TachoPilot;

public class MeinProgramm {
	Pilot pilot;

	public static void main(String[] args) {
		MeinProgramm meinProgramm = new MeinProgramm();
		meinProgramm.pilot = new TachoPilot(56, 173, Motor.A, Motor.C);
		meinProgramm.run();
	}

	private void run() {
		// erstellt einen Berührungssensor mit dem Namen 'touchMe'
		TouchSensor touchMe = new TouchSensor(SensorPort.S1);

		// das ist eine Endlosschleife die den Code immer weiter wiederholt
		while (true) {
			// hier kommt das hin was wiederholt werden soll
		}
	}

}
