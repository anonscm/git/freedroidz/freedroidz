package org.evolvis.freedroidzEclipsePlugin.freedroidzPopUpMenu;

import java.io.File;

import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;

import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.internal.core.JavaElement;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.evolvis.freedroidz.config.Config;
import org.evolvis.freedroidz.lejos.CompileLinkFile;
import org.evolvis.freedroidz.lejos.LejosUICallback;
import org.evolvis.freedroidz.lejos.UploadFile;
import org.evolvis.freedroidz.lejos.Utilities;
import org.evolvis.freedroidz.ui.LejosScanPanel;

/**
 * Let you upload a file or compile program an upload it to the NXT that is set as default.
 * @author "T.Wylegala"
 *
 */
public class Upload  implements IObjectActionDelegate, LejosUICallback{

	private ISelection _selection;

	private NXTInfo nxt;

	@Override
	public void setActivePart(IAction arg0, IWorkbenchPart arg1) {
		//Nothing to be done yet
	}

	/**
	 * Gets called by "Upload". 
	 * 
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	@Override
	public void run(IAction arg0) {
		
		

		IStructuredSelection iselection  = (IStructuredSelection)_selection;	
		JavaElement je = (JavaElement)iselection.getFirstElement();

		
		String binName = LeJOSNXJUtil.getBinaryName(je);  	//Binaryname
		String classpath = null;								//Output dir
		try {
			classpath = LeJOSNXJUtil.getAbsoluteProjectTargetDir(je.getJavaProject()).getAbsolutePath();
		} catch (JavaModelException e) {
			e.printStackTrace();
		}
		String className = LeJOSNXJUtil.getFullQualifiedClassName(je);	//Classpath
		
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = workspace.getRoot();
		IPath rootPath = root.getLocation();
		
		DebugUITools.saveAndBuildBeforeLaunch();
		
		File fileToUpload;	
		String path = className.substring(0, className.length()).replace(".", File.separator)+".class";
		File inputFile = new File(rootPath.toFile().getAbsoluteFile(),je.getPath().toFile().getPath());
		File inputClassFile = new File(classpath, path);
		
		
		if(inputFile.getName().endsWith(".java") && Utilities.fileHasMainMethod(inputClassFile)){
			CompileLinkFile clp = new CompileLinkFile();
			File finalOutput = new File(System.getProperty("java.io.tmpdir"), binName).getAbsoluteFile();
			clp.link(className, classpath, finalOutput);
			fileToUpload = finalOutput;
		}else{
			fileToUpload = inputFile;
		}
		
		nxt = Config.getInstance().getNXTInfo();
		
		if(nxt == null){
			nxt = LejosScanPanel.runGUI(NXTCommFactory.ALL_PROTOCOLS);
		}
		
		if(nxt != null){
			UploadFile uf = new UploadFile();
			uf.registerCallback(this);
			uf.uploadFile(fileToUpload.getAbsolutePath(), nxt.deviceAddress, nxt.protocol, false);
			
		}
	}
	
	/*
	 * Needed to get the current *.java element 
	 * 
	 * @see org.eclipse.ui.IActionDelegate#selectionChanged(org.eclipse.jface.action.IAction, org.eclipse.jface.viewers.ISelection)
	 */
	@Override
	public void selectionChanged(IAction arg0, ISelection arg1) {
		_selection = arg1;	
	}
		
	private void showErrorMessageDialog() {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				MessageBox message = new MessageBox(new Shell(Display
						.getDefault()), SWT.ICON_ERROR | SWT.CANCEL);
				message.setText("Fehler");
				message.setMessage("NXT nicht gefunden");		
				message.open();
			}
		});
	}

	@Override
	public void processDone(int status) {
		if(status == LejosUICallback.OPERATION_SUCCESS){
			Config.getInstance().setNxtInfo(nxt);
			Config.getInstance().storeConfig();
		}
		if(status == LejosUICallback.OPERATION_FAIL){
			showErrorMessageDialog();
		}
	}

}
