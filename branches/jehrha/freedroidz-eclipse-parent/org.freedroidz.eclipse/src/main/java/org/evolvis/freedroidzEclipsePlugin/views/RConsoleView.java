package org.evolvis.freedroidzEclipsePlugin.views;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;
import org.evolvis.freedroidz.ui.RConsolePanel;

public class RConsoleView extends ViewPart{

	@Override
	public void createPartControl(Composite arg0) {
		new RConsolePanel(arg0);
		
	}

	@Override
	public void setFocus() {

	}

}
