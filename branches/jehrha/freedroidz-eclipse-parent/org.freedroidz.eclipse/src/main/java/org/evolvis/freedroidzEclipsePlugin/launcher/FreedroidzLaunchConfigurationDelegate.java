package org.evolvis.freedroidzEclipsePlugin.launcher;

import java.io.File;
import java.text.MessageFormat;
import java.util.ResourceBundle;

import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;

import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.jdt.internal.launching.LaunchingMessages;
import org.eclipse.jdt.launching.AbstractJavaLaunchConfigurationDelegate;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.evolvis.freedroidz.lejos.CompileLinkFile;
import org.evolvis.freedroidz.lejos.LejosUICallback;
import org.evolvis.freedroidz.lejos.UploadFile;

public class FreedroidzLaunchConfigurationDelegate extends
		AbstractJavaLaunchConfigurationDelegate implements LejosUICallback{

	String baseName = "org.evolvis.freedroidzEclipsePlugin.FreedroidzLaunchConfigurationDelegate"; //$NON-NLS-1$
	final ResourceBundle bundle = ResourceBundle.getBundle(baseName);

	
	public void launch(ILaunchConfiguration configuration, String mode,
			ILaunch launch, IProgressMonitor monitor) throws CoreException {

		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}

		monitor.beginTask(MessageFormat.format(
				"{0}...", new String[] { configuration.getName() }), 3); //$NON-NLS-1$
		// check for cancellation
		if (monitor.isCanceled()) {
			return;
		}
		try {
			String filename = configuration.getAttribute(
					"FREEDROIDZ_PROGRAMM_NAME", "freedroidz")
					+ ".nxj";
			monitor
					.subTask(LaunchingMessages.JavaLocalApplicationLaunchConfigurationDelegate_Verifying_launch_attributes____1);

			String mainTypeName = verifyMainTypeName(configuration);

			File workingDir = verifyWorkingDirectory(configuration);

			String workspacePath = getWorkspacePath();

			String binPath = getJavaProject(configuration).getOutputLocation()
					.toOSString();
			
			//Needs improvement: Way to recognize if user modified devices address by hand
			checkDeviceAddress();

			upload2(mainTypeName, workspacePath + binPath, filename);

			if (monitor.isCanceled()) {
				return;
			}
		} finally {
			monitor.done();
		}
	}

	private void checkDeviceAddress() {
		
		NXTInfo tmp = FreedroidzLaunchConfigurationPanel.getNXTInfo();
		
		if(tmp==null || (FreedroidzLaunchConfigurationPanel.getMacAdress() != null && !FreedroidzLaunchConfigurationPanel.getMacAdress().endsWith("("+tmp.name+")")))
			FreedroidzLaunchConfigurationPanel.setNxtInfoS(new NXTInfo(NXTCommFactory.BLUETOOTH, null, FreedroidzLaunchConfigurationPanel.getMacAdress()));
	}

	public void upload2(String mainTypeName, String classPath, String filename) {
		String path = File.separator
				+ mainTypeName.replace(".", File.separator);

		NXTInfo nxtInfo = FreedroidzLaunchConfigurationPanel.getNXTInfo();

		link(path, filename, classPath);

		upload(filename, nxtInfo);
	}

	private String getWorkspacePath() {
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = workspace.getRoot();
		IPath location = root.getLocation();
		String workspacePath = location.toPortableString();
		return workspacePath;
	}

	public void upload(String outputFileName, NXTInfo nxtInfo) {

		UploadFile instance = UploadFile.getInstance();
		instance.registerCallback(this);
		instance.uploadFile(new File(System.getProperty("java.io.tmpdir"), outputFileName).getAbsolutePath(),
				nxtInfo.deviceAddress, nxtInfo.protocol, true);
		
	}

	private boolean dialogReturnValue;
	
	private boolean showErrorMessageDialog() {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				MessageBox message = new MessageBox(new Shell(Display
						.getDefault()), SWT.ICON_ERROR | SWT.CANCEL);
				message.setText("Fehler");
				message.setMessage("NXT nicht gefunden");

				
				dialogReturnValue = (message.open() == 0);


			}
		});
		return dialogReturnValue;
	}

	/**
	 * TODO
	 * 
	 * @param file
	 *            class file containing the main type.
	 * @param filename
	 *            name of the nxj file (to use on the brick)
	 * @param classpath
	 *            class path to use during linking.
	 * @return
	 */
	public boolean link(String file, String filename, String classpath) {

		CompileLinkFile compileLinkFile = new CompileLinkFile();

		String tmpPath = System.getProperty("java.io.tmpdir");
		File tmpDir = new File(tmpPath);
		File outputFile = new File(tmpDir, filename);

		return compileLinkFile.startLink(file, classpath, outputFile);

	}

	@Override
	public void processDone(int status) {
		if (status == LejosUICallback.OPERATION_FAIL) {
			showErrorMessageDialog();
		}		
	}

}
