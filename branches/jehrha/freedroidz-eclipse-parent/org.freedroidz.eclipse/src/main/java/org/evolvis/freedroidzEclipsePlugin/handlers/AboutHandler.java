package org.evolvis.freedroidzEclipsePlugin.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.evolvis.freedroidz.ui.AboutPanel;

public class AboutHandler extends AbstractHandler {
	
	private static final String LAUNCH_GROUP = "freedroidz-eclipse-plugin.launchGroup";
	
	/**
	 * The constructor.
	 */
	public AboutHandler() {
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {

		
//		DebugUITools.openLaunchConfigurationPropertiesDialog(Display.getDefault().getActiveShell(), DebugUITools.getLastLaunch(LAUNCH_GROUP), LAUNCH_GROUP);

		Shell dialog = new Shell (Display.getDefault().getActiveShell());
		dialog.setText ("About"); 
		dialog.setLayout(new FillLayout());
		@SuppressWarnings("unused")
		AboutPanel aboutPanel = new AboutPanel(dialog);

		dialog.pack();
		dialog.open ();
		
		
		return null;
	}

}