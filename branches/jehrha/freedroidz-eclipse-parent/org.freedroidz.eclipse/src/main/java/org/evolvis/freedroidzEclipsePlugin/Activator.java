package org.evolvis.freedroidzEclipsePlugin;

import java.io.File;
import java.io.FileNotFoundException;

import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.evolvis.freedroidz.Log;
import org.evolvis.freedroidz.lejos.CompileLinkFile;
import org.evolvis.freedroidz.lejos.NativeLibraryManager;
import org.evolvis.freedroidz.ui.SetupPanel;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "freedroidz_eclipse_plugin";

	// The shared instance
	private static Activator plugin;
	
	/**
	 * The constructor
	 */
	public Activator() {
		
		try {
			Log.initLogging();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		Log.registerOutputStream(findConsole("freedroidz").newOutputStream());
		Log.log("freedroidz Plugin started");
		
		new CompileLinkFile();
		
//		try {
//			UploadFirmware.addToJavaLibraryPath(new UploadFirmware().createNativeLibs().getParent());
//		} catch (IOException e) {
//			e.printStackTrace();
//		}

		NativeLibraryManager.getInstance().checkAndAdd();

		if(!new File("/etc/udev/rules.d/01-lego-nxj.rules").exists()){
			new SetupPanel();
		}
		
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}
	
	public static MessageConsole findConsole(String name) {
	      ConsolePlugin plugin = ConsolePlugin.getDefault();
	      IConsoleManager conMan = plugin.getConsoleManager();
	      IConsole[] existing = conMan.getConsoles();
	      for (int i = 0; i < existing.length; i++)
	         if (name.equals(existing[i].getName()))
	            return (MessageConsole) existing[i];
	      //no console found, so create a new one
	      MessageConsole myConsole = new MessageConsole(name, null);
	      conMan.addConsoles(new IConsole[]{myConsole});
	      return myConsole;
	}




}
