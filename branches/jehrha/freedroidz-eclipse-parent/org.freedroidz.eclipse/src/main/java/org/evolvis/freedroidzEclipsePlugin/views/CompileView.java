package org.evolvis.freedroidzEclipsePlugin.views;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;
import org.evolvis.freedroidz.ui.LejosCompileLinkPanel;

public class CompileView extends ViewPart {
	public CompileView() {
		 super();
	}


	public void createPartControl(Composite parent) {
	
		new LejosCompileLinkPanel(parent);

	}


	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		
	}
}
