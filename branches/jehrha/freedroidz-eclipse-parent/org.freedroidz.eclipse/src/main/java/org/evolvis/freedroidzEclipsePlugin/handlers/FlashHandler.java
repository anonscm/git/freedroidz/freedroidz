package org.evolvis.freedroidzEclipsePlugin.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.evolvis.freedroidz.ui.LejosUploadFirmwarePanel;

public class FlashHandler extends AbstractHandler {

	public Object execute(ExecutionEvent arg0) throws ExecutionException {
		
		Shell dialog = new Shell (Display.getDefault());
//		dialog.setText (bundle.getString("LejosUtilsPanel.8")); //$NON-NLS-1$
		dialog.setBounds(Display.getDefault().getBounds().width/2-100,Display.getDefault().getBounds().height/2-100,200,200);
		dialog.setLayout(new FillLayout());

		LejosUploadFirmwarePanel lejosUploadFirmwarePanel = new LejosUploadFirmwarePanel(dialog);

		dialog.pack();
		dialog.open ();
		
		return null;
	}

}
