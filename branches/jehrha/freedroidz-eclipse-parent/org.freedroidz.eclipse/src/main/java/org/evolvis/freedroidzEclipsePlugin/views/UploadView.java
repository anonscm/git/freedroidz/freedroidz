package org.evolvis.freedroidzEclipsePlugin.views;


import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;
import org.evolvis.freedroidz.ui.LejosUploadFilePanel;

public class UploadView extends ViewPart {

	public UploadView() {
		 super();
	}

	public void setFocus() {
	}

	public void createPartControl(Composite parent) {
	
		new LejosUploadFilePanel(parent);

	}

}