package org.evolvis.freedroidzEclipsePlugin.freedroidzPopUpMenu;

import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.evolvis.freedroidz.config.Config;
import org.evolvis.freedroidz.ui.LejosScanPanel;

public class ChangeNXT implements IObjectActionDelegate{

	@Override
	public void setActivePart(IAction arg0, IWorkbenchPart arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void run(IAction arg0) {
		NXTInfo nxt = Config.getInstance().getNXTInfo();	
		nxt = LejosScanPanel.runGUI(NXTCommFactory.ALL_PROTOCOLS);		
		Config.getInstance().setNxtInfo(nxt);
		Config.getInstance().storeConfig();

		
	}

	@Override
	public void selectionChanged(IAction arg0, ISelection arg1) {
		// TODO Auto-generated method stub
		
	}

}
