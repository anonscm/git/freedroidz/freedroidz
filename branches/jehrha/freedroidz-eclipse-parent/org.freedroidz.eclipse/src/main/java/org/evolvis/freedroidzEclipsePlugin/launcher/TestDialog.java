package org.evolvis.freedroidzEclipsePlugin.launcher;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class TestDialog {
	public TestDialog(){
		initGui();
	}
	
	public void initGui(){
		Shell shell = new Shell(Display.getDefault().getActiveShell(), SWT.MODELESS);
		
		shell.setBounds(200,200,200,200);
		Button button = new Button(shell, SWT.NONE);
		button.setText("test");
		button.setBounds(1,1,100,100);
		
		shell.open();
	
	}
}
