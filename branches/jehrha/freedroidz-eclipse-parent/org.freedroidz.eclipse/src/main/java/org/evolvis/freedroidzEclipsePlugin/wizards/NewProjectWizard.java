package org.evolvis.freedroidzEclipsePlugin.wizards;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import lejos.pc.comm.NXTSamba;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWizard;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.evolvis.freedroidz.lejos.CompileLinkFile;
import org.evolvis.freedroidz.lejos.Utilities;

/**
 *	This Dialog is used to create a new freedroidz Project.
 *	It automatically adds lejos classes.jar to offer full functionality.
 */



public class NewProjectWizard extends Wizard implements INewWizard {
	private NewProjectWizardPage page;
	private ISelection selection;

	/**
	 * Constructor for SampleNewWizard.
	 */
	public NewProjectWizard() {
		super();
		setNeedsProgressMonitor(true);
	}
	
    private static class ProjectInfo {
        private final IProject mProject;
        private final IProjectDescription mDescription;
        private final Map<String, Object> mParameters;
        private final HashMap<String, String> mDictionary;

        public ProjectInfo(IProject project,
                IProjectDescription description,
                Map<String, Object> parameters,
                HashMap<String, String> dictionary) {
                    mProject = project;
                    mDescription = description;
                    mParameters = parameters;
                    mDictionary = dictionary;
        }

        public IProject getProject() {
            return mProject;
        }

        public IProjectDescription getDescription() {
            return mDescription;
        }

        public Map<String, Object> getParameters() {
            return mParameters;
        }

        public HashMap<String, String> getDictionary() {
            return mDictionary;
        }
    }
	
	/**
	 * Adding the page to the wizard.
	 */

	public void addPages() {
		page = new NewProjectWizardPage(selection);
		addPage(page);
	}

	/**
	 * This method is called when 'Finish' button is pressed in
	 * the wizard. We will create an operation and run it
	 * using wizard as execution context.
	 */
	public boolean performFinish() {
		final String projectName = page.getProjectName();
		final boolean checkBox = page.getCheckBox();
		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor) throws InvocationTargetException {
				try {
					doFinish(projectName, checkBox, monitor);
				} catch (CoreException e) {
					throw new InvocationTargetException(e);
				} finally {
					monitor.done();
				}
			}
		};
		try {
			getContainer().run(true, false, op);
		} catch (InterruptedException e) {
			return false;
		} catch (InvocationTargetException e) {
			Throwable realException = e.getTargetException();
			MessageDialog.openError(getShell(), "Error", realException.getMessage());
			return false;
		}
		
		IWorkbench workbench = PlatformUI.getWorkbench();
		try {
			workbench.showPerspective("org.evolvis.freedroidzEclipsePlugin.freedroidzPerspective", workbench.getActiveWorkbenchWindow());
		} catch (WorkbenchException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	/**
	 * The worker method. It will find the container, create the
	 * file if missing or just replace its contents, and open
	 * the editor on the newly created file.
	 */

	private void doFinish(String projectName, boolean samples, IProgressMonitor monitor)
		throws CoreException {
		// create a sample file
		monitor.beginTask("Creating " + projectName, 2);
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		ProjectInfo pi = new ProjectInfo(workspace.getRoot().getProject(projectName), workspace.newProjectDescription(projectName), null, null);
		
		IProject project = pi.getProject();
		
		project.create(pi.getDescription(), new SubProgressMonitor(monitor, 10));
		
		if (monitor.isCanceled()) throw new OperationCanceledException();
		
		project.open(IResource.BACKGROUND_REFRESH, new SubProgressMonitor(monitor, 10));
		
		IFolder srcFolder = project.getFolder("src");
		srcFolder.create(true, true, monitor);
		
		IFolder settingsFolder = project.getFolder(".settings");
		settingsFolder.create(true, true, monitor);
		
		writeInfoFiles(project.getLocation().toOSString(), projectName);
		if(samples){
			Utilities.addExample(project.getLocation().toOSString());
		}
		
		//Really dirty hack to make eclipse notice that something has changed
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		File file = new File(new File(project.getLocation().toOSString()), ".project");
		file.setLastModified(System.currentTimeMillis());
		
		
		project.refreshLocal(IResource.PROJECT, null);

	}
	
	public void writeSampleFiles(String location, boolean checked){
	
			
	}
	
	public void writeInfoFiles(String location, String name){
		String classpath = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
				"<classpath>\n" +
				"	<classpathentry kind=\"src\" path=\"src\"/>\n" +
				"	<classpathentry kind=\"lib\" path=\""+ new CompileLinkFile().getClassesJar().getAbsolutePath() +"\"/>\n" +
				"	<classpathentry kind=\"output\" path=\"bin\"/>\n" +
				"</classpath>\n";
		
		String project = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
				"<projectDescription>\n" +
				"	<name>"+name+"</name>\n" +
				"	<comment></comment>\n" +
				"	<projects></projects>\n" +
				"	<buildSpec>\n" +
				"		<buildCommand>\n" +
				"			<name>org.eclipse.jdt.core.javabuilder</name>\n" +
				"			<arguments></arguments>\n" +
				"		</buildCommand>\n" +
				"	</buildSpec>\n" +
				"	<natures>\n" +
				"		<nature>org.eclipse.jdt.core.javanature</nature>\n" +
				"	</natures>\n" +
				"</projectDescription>";
		
		String prefs = "#Thu May 27 14:35:21 CEST 2010\n" +
			"eclipse.preferences.version=1\n" +
			"org.eclipse.jdt.core.compiler.codegen.inlineJsrBytecode=enabled\n"+
			"org.eclipse.jdt.core.compiler.codegen.targetPlatform=1.6\n"+
			"org.eclipse.jdt.core.compiler.codegen.unusedLocal=preserve\n"+
			"org.eclipse.jdt.core.compiler.compliance=1.6\n"+
			"org.eclipse.jdt.core.compiler.debug.lineNumber=generate\n"+
			"org.eclipse.jdt.core.compiler.debug.localVariable=generate\n"+
			"org.eclipse.jdt.core.compiler.debug.sourceFile=generate\n"+
			"org.eclipse.jdt.core.compiler.problem.assertIdentifier=error\n"+
			"org.eclipse.jdt.core.compiler.problem.enumIdentifier=error\n"+
			"org.eclipse.jdt.core.compiler.source=1.6\n";
		
		try {
			FileOutputStream classpathFos = new FileOutputStream(new File(location, ".classpath"));
			FileOutputStream projectFos = new FileOutputStream(new File(location, ".project"));
			FileOutputStream prefsFos = new FileOutputStream(new File(new File(location, ".settings"), "org.eclipse.jdt.core.prefs"));
			
			classpathFos.write(classpath.getBytes());
			projectFos.write(project.getBytes());
			prefsFos.write(prefs.getBytes());
			
			classpathFos.flush();
			projectFos.flush();
			prefsFos.flush();
			
			classpathFos.close();
			projectFos.close();
			prefsFos.close();	
	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * We will initialize file contents with a sample text.
	 */

	private InputStream openContentStream() {
		String contents =
			"This is the initial file contents for *.mpe file that should be word-sorted in the Preview page of the multi-page editor";
		return new ByteArrayInputStream(contents.getBytes());
	}

	private void throwCoreException(String message) throws CoreException {
		IStatus status =
			new Status(IStatus.ERROR, "Plugin_Test_Numbertwo", IStatus.OK, message, null);
		throw new CoreException(status);
	}

	/**
	 * We will accept the selection in the workbench to see if
	 * we can initialize from it.
	 * @see IWorkbenchWizard#init(IWorkbench, IStructuredSelection)
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.selection = selection;
	}
}