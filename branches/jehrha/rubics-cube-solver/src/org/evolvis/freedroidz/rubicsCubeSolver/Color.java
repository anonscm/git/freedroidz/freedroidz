package org.evolvis.freedroidz.rubicsCubeSolver;

public enum Color {
	RED, GREEN, YELLOW, BLUE, ORANGE, WHITE
}
