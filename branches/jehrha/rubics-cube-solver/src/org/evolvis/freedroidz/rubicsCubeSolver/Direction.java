package org.evolvis.freedroidz.rubicsCubeSolver;

public enum Direction {
	UP, DOWN, LEFT, RIGHT, FRONT, BACK
}
