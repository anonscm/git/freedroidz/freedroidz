package org.evolvis.freedroidz.rubicsCubeSolver;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


public class ScannerTest {
	Scanner scanner;
	@Mock
	Hardware hardware;
	
	Robot robot;
	
	@Mock
	private IColorSensor colorSensor;
	@Mock
	private IMotor motorB;
	@Mock
	private IMotor motorA;
	@Mock
	private IMotor motorC;
	
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		robot = new Robot(new CubeFactory(), motorB, motorC);
		scanner = new Scanner(robot, hardware);
		when(hardware.getColorSensor()).thenReturn(colorSensor);
		when(hardware.getMotorB()).thenReturn(motorB);
		when(hardware.getMotorA()).thenReturn(motorA);
		when(hardware.getMotorC()).thenReturn(motorC);
	}
	
	//@Test
	public void test() throws Throwable{
		
		when(hardware.getColorSensor().getColorNumber()).thenReturn(2,6,2,8,4,12);
		scanner.scanCube();
		assertEquals("WHITE",robot.cornerCubie[0].Color1);
		assertEquals("BLUE",robot.edgeCubie[0].Color1);
		assertEquals("YELLOW",robot.edgeCubie[1].Color1);
		assertEquals("BLUE",robot.edgeCubie[2].Color1);
		
		System.out.println(robot.cornerCubie[0].Color1 + " " + robot.cornerCubie[0].Color1Position);
		System.out.println(robot.edgeCubie[0].Color1 + " " + robot.edgeCubie[0].Color1Position);
		System.out.println(robot.edgeCubie[1].Color1 + " " + robot.edgeCubie[1].Color1Position);
		System.out.println(robot.edgeCubie[2].Color1 + " " + robot.edgeCubie[2].Color1Position);

	}
	
	@Test
	public void first_edge_cubie() throws Throwable{
		// simulate sensor input such that
		// the first edge cubie is (green,red) with orientation (front,up)
		when(hardware.getColorSensor().getColorNumber()).thenReturn(9,9,9,9,9,9,9,9,9,
				2,2,2,2,2,2,2,2,2,
				12,12,12,12,12,12,12,12,12,
				8,8,8,8,8,8,8,8,8,
				4,4,4,4,4,4,4,4,4,
				6,6,6,6,6,6,6,6,6);
		scanner.scanCube();
		assertEquals("ORANGE",robot.edgeCubie[9].Color1);
		assertEquals("YELLOW",robot.edgeCubie[9].Color2);
		assertEquals("DOWN",robot.edgeCubie[9].Color1Position);
		assertEquals("RIGHT",robot.edgeCubie[9].Color2Position);
		assertEquals("GREEN",robot.cornerCubie[7].Color1);
		assertEquals("FRONT",robot.cornerCubie[7].Color1Position);
		assertEquals("ORANGE",robot.cornerCubie[7].Color2);
		assertEquals("DOWN",robot.cornerCubie[7].Color2Position);
		assertEquals("YELLOW",robot.cornerCubie[7].Color3);
		assertEquals("RIGHT",robot.cornerCubie[7].Color3Position);
		
	}
}
