/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.freedroidz.icommand.strategies;

import icommand.nxt.Motor;
import icommand.nxt.UltrasonicSensor;

import java.util.logging.Logger;

import org.evolvis.freedroidz.icommand.Services;
import org.evolvis.freedroidz.icommand.command.Command;
import org.evolvis.freedroidz.icommand.command.CommandListener;
import org.evolvis.freedroidz.icommand.strategy.AbstractStrategy;
import org.evolvis.freedroidz.icommand.strategy.Strategy;
import org.evolvis.freedroidzI.icommand.control.RobotControl;

/**
 * @author Hendrik Helwich
 */
@SuppressWarnings("unused")
public class HoldDistance extends AbstractStrategy implements CommandListener {
	
	/*^
	 * # constants
	 * PI = 3.1415926535897932384626433832795028841971693993751058209749445923078164
	 * WHEEL_DIAMETER = 5.5
	 * DISTANCE_REACT = 100
	 * DISTANCE_HOLD  = 20
	 * SPEED_MINIMUM = 20
	 * C = PI WHEEL_DIAMETER * 360 /
	 * MEASURING_ERROR = 1
	 * 
	 * VAR_DIST = 1 1 *
	 * VAR_SPEED = 0.1 0.1 *
	 * COVAR_SPEED_DIST = 0.01 0.01 *
	 * 
	 * # system model
	 * F = (0, 0; 0, 1)
	 * x = (xv; xd)
	 * P = (Pvv, Pvd; Pvd, Pdd)
	 * # u = (p)
	 * B = (C; C t *)
	 * Q = (VAR_SPEED, COVAR_SPEED_DIST; COVAR_SPEED_DIST, VAR_DIST)
	 * 
	 * 
	 * # measurement model
	 * H = (t,1)
	 * # R = (r)
	 *
	 * 
	 * # prediction
	 * x_p = F x * B u * +                 # predicted state
	 * P_p = F P * F ^t * Q +              # predicted state covariance
	 * 
	 * # update
	 * y = z H x * -                       # innovation
	 * S = H P * H ^t * R +                # innovation covariance
	 * K = S H P * slvspd ^t               # kalman gain (K = P H ^t * S ^-1 *)
	 * x_u = x K y * +                     # update state
	 * P_u = H width ones K H * - P *      # update state covariance
	 * #T = 1/2 K * S * P H ^t * - K ^t *
	 * #P_u = P T + T +
	 * 
	 * # optimized prediction
	 * x_p_opt = (u 86393797973719314057722693040186329315422158482815410076810976288464951/1800000000000000000000000000000000000000000000000000000000000000000000000 *;xd u 86393797973719314057722693040186329315422158482815410076810976288464951/1800000000000000000000000000000000000000000000000000000000000000000000000 t * * +)
	 * P_p_opt = (0.01,0.0001;0.0001,Pdd 1 +)
	 * 
	 * # optimized update
	 * y_opt = xd z - xv t * + ~
	 * S_opt = t t Pvv * Pvd 2 * + * R Pdd + +
	 * K_opt = (Pvd t Pvv * + Pdd R + t Pvv t * 2 Pvd * + * + /;t Pvd * Pdd + t t Pvv * 2 Pvd * + * Pdd R + + /)
	 * x_u_opt = (Pvd t Pvv * + z t xv * xd + - * t Pvv * 2 Pvd * + t * R Pdd + + / xv +;xd Pvd t * Pdd + z xd t xv * + - * R Pdd + t Pvv t * 2 Pvd * + * + / +)
	 * P_u_opt = (Pvv Pvd Pvd Pvv t * + * R Pdd + t 2 Pvd * t Pvv * + * + / ~ + Pvv t * Pvd t * R Pdd + + Pvd t Pvv * + / t + / -,Pvd Pvv t * Pvd + * R t / Pdd t / + Pvv t * 2 Pvd * + + / ~ Pvd + Pdd Pvd t Pvv * + * Pdd R + t Pvv t * 2 Pvd * + * + / -;Pvd Pvd t Pvd 2 * t Pvv * + * R Pdd + + / Pdd Pvd t * + * - Pvv Pdd t Pvd * + 2 Pvd * t Pvv * + Pdd R + t / + / * -,Pdd Pdd Pvd t * + Pdd * Pdd R + t Pvv t * Pvd 2 * + * + / - Pvd t * Pdd + Pdd R + t / Pvv t * Pvd 2 * + + / Pvd * -)
	 */
	
	protected static final Logger logger = Logger.getLogger(HoldDistance.class.getName());

    private boolean stopped;
	private Thread robotThread = null;

	private RobotControl rc;
	private Motor mleft;
	private Motor mright;
	private UltrasonicSensor usLeft;
	//private UltrasonicSensor usRight;
	
	public void run() {
		rc = RobotControl.getInstance();
		setSpeed(0);
		mleft = rc.getRobotHardware().getMotorLeft();
		mright = rc.getRobotHardware().getMotorRight();
		usLeft = rc.getRobotHardware().getUltrasonicSensor(1);
		//usRight = rc.getRobotHardware().getUltrasonicSensor(2);
		
		logger.info("Strategy "+getName());
		logger.info("need to register myself to the Command Provider");
		Services.getInstance().getCommandProvider().addCommandListener(this);
		fireStrategyStarted();
		
		start();
	}

	public String getDescription() {
		return "The robot tries to hold a given distance by checking the ultrasonic sensor 1 and driving forward and backwards";
	} 

	public String getName() {
		return "Hold Distance";
	}
	
	public void start() {
		stopped = false;
		double p = 0; // current speed

		double[] x = x(null, usLeft.getDistance(), 0);
		double[][] P = P(null, 1, 0, 0);
		double[] xp = null;
		double[][] Pp = null;
		double z;
		double S;
		double[] K = null;
		double r = MEASURING_ERROR*MEASURING_ERROR;
		double t = 0, nt = 0;
		long lastTime = System.currentTimeMillis();
		for (int j = 0; ! stopped; j++) {
			z = getDistance();
			t = nt;
			long now = System.currentTimeMillis();
			nt = (now - lastTime) / 1000.0;
			lastTime = now;
			
			xp = x_p_opt(xp, nt, p, x);
			Pp = P_p_opt(Pp, P);
			
			double y = y_opt(nt, xp, z);
			if (Math.abs(y) > 5) {
				//TODO handle outlier
			}
			S = S_opt(Pp, r, nt);
			K = K_opt(K, Pp, r, nt);
			x = x_u_opt(x, Pp, r, nt, xp, z);
			P = P_u_opt(P, Pp, r, nt);
			
			if (get_x_d(x) < DISTANCE_REACT) {
				double speed = (get_x_d(x) - DISTANCE_HOLD) / C;
				if (Math.abs(speed) < 20)
					speed = 0;
				p = setSpeed(speed);
			} else {
				p = setSpeed(0);
			}
		}
		
		setSpeed(0);
		Services.getInstance().getCommandProvider().removeCommandListener(this);
		fireStrategyStopped();
	}
	
 	/* ultrasonic device modes (they are not public) */
	private static final byte MODE_OFF = 0x0;
	private static final byte MODE_SINGLE = 0x1;
	private static final byte MODE_CONTINUOUS = 0x2;
	private static final byte MODE_CAPTURE = 0x3;
	private static final byte MODE_RESET = 0x4;
	
	private int setSpeed(double p) {
		return rc.driveAhead((int) Math.round(p));
	}
	
	private double getDistance() {
		double l = usLeft.getDistance();
		if (l == 255)
			l = usLeft.getDistance();
		return l;
	}

	public void stop() {
		logger.info("stopping strategy");
		stopped = true;
	}

	public void commandReleased(Command command) {}

	public void commandSent(Command command) {}

	/*^ BEGIN GENERATED CODE */

    // PI = 3.1415926535897932384626433832795028841971693993751058209749445923078164
    private static final double PI = 3.141592653589793;

    // WHEEL_DIAMETER = 5.5
    private static final double WHEEL_DIAMETER = 5.5;

    // DISTANCE_REACT = 100
    private static final double DISTANCE_REACT = 100.0;

    // DISTANCE_HOLD = 20
    private static final double DISTANCE_HOLD = 20.0;

    // SPEED_MINIMUM = 20
    private static final double SPEED_MINIMUM = 20.0;

    // C = 86393797973719314057722693040186329315422158482815410076810976288464951/1800000000000000000000000000000000000000000000000000000000000000000000000
    private static final double C = 0.04799655442984406;

    // MEASURING_ERROR = 1
    private static final double MEASURING_ERROR = 1.0;

    // VAR_DIST = 1
    private static final double VAR_DIST = 1.0;

    // VAR_SPEED = 0.01
    private static final double VAR_SPEED = 0.01;

    // COVAR_SPEED_DIST = 0.0001
    private static final double COVAR_SPEED_DIST = 1.0E-4;

    // F = (0,0;0,1)
    private static final double[][] F = {
        {0.0, 0.0},
        {0.0, 1.0}
    };

    // x = (xv;xd)
    private static double[] x(double[] buffer, double xd, double xv) {
        if (buffer == null)
            buffer = new double[2];
        buffer[0] = xv;
        buffer[1] = xd;
        return buffer;
    }

    // P = (Pvv,Pvd;Pvd,Pdd)
    private static double[][] P(double[][] buffer, double Pdd, double Pvd, 
            double Pvv) {
        if (buffer == null)
            buffer = new double[2][2];
        buffer[0][0] = Pvv;
        buffer[0][1] = Pvd;
        buffer[1][0] = Pvd;
        buffer[1][1] = Pdd;
        return buffer;
    }

    // B = (86393797973719314057722693040186329315422158482815410076810976288464951/1800000000000000000000000000000000000000000000000000000000000000000000000;86393797973719314057722693040186329315422158482815410076810976288464951/1800000000000000000000000000000000000000000000000000000000000000000000000 t *)
    private static double[] B(double[] buffer, double t) {
        if (buffer == null)
            buffer = new double[2];
        buffer[0] = 0.04799655442984406;
        buffer[1] = (0.04799655442984406*t);
        return buffer;
    }

    // Q = (0.01,0.0001;0.0001,1)
    private static final double[][] Q = {
        {0.01, 1.0E-4},
        {1.0E-4, 1.0}
    };

    // H = (t,1)
    private static double[] H(double[] buffer, double t) {
        if (buffer == null)
            buffer = new double[2];
        buffer[0] = t;
        buffer[1] = 1.0;
        return buffer;
    }

    // x_p = (u 86393797973719314057722693040186329315422158482815410076810976288464951/1800000000000000000000000000000000000000000000000000000000000000000000000 *;xd u 86393797973719314057722693040186329315422158482815410076810976288464951/1800000000000000000000000000000000000000000000000000000000000000000000000 t * * +)
    private static double[] x_p(double[] buffer, double t, double u, double[] x
            ) {
        if (buffer == null)
            buffer = new double[2];
        buffer[0] = (u*0.04799655442984406);
        buffer[1] = (x[1]+(u*(0.04799655442984406*t)));
        return buffer;
    }

    // P_p = (0.01,0.0001;0.0001,Pdd 1 +)
    private static double[][] P_p(double[][] buffer, double[][] P) {
        if (buffer == null)
            buffer = new double[2][2];
        buffer[0][0] = 0.01;
        buffer[0][1] = 1.0E-4;
        buffer[1][0] = 1.0E-4;
        buffer[1][1] = (P[1][1]+1.0);
        return buffer;
    }

    // y = z t xv * xd + -
    private static double y(double t, double[] x, double z) {
        return (z-((t*x[0])+x[1]));
    }

    // S = t Pvv * Pvd + t * t Pvd * Pdd + + R +
    private static double S(double[][] P, double R, double t) {
        return (((((t*P[0][0])+P[0][1])*t)+((t*P[0][1])+P[1][1]))+R);
    }

    // K = (t Pvv * Pvd + t Pvv * Pvd + t * t Pvd * Pdd + + R + /;t Pvd * Pdd + t Pvv * Pvd + t * t Pvd * Pdd + + R + /)
    private static double[] K(double[] buffer, double[][] P, double R, double t
            ) {
        if (buffer == null)
            buffer = new double[2];
        buffer[0] = (((t*P[0][0])+P[0][1])/(((((t*P[0][0])+P[0][1])*t)+((t*P[0][
                1])+P[1][1]))+R));
        buffer[1] = (((t*P[0][1])+P[1][1])/(((((t*P[0][0])+P[0][1])*t)+((t*P[0][
                1])+P[1][1]))+R));
        return buffer;
    }

    // x_u = (xv z t xv * xd + - t Pvv * Pvd + t Pvv * Pvd + t * t Pvd * Pdd + + R + / * +;xd z t xv * xd + - t Pvd * Pdd + t Pvv * Pvd + t * t Pvd * Pdd + + R + / * +)
    private static double[] x_u(double[] buffer, double[][] P, double R, 
            double t, double[] x, double z) {
        if (buffer == null)
            buffer = new double[2];
        buffer[0] = (x[0]+((z-((t*x[0])+x[1]))*(((t*P[0][0])+P[0][1])/(((((t*P[0
                ][0])+P[0][1])*t)+((t*P[0][1])+P[1][1]))+R))));
        buffer[1] = (x[1]+((z-((t*x[0])+x[1]))*(((t*P[0][1])+P[1][1])/(((((t*P[0
                ][0])+P[0][1])*t)+((t*P[0][1])+P[1][1]))+R))));
        return buffer;
    }

    // P_u = (1 t Pvv * Pvd + t Pvv * Pvd + t * t Pvd * Pdd + + R + / t * - Pvv * t Pvv * Pvd + t Pvv * Pvd + t * t Pvd * Pdd + + R + / ~ Pvd * +,1 t Pvv * Pvd + t Pvv * Pvd + t * t Pvd * Pdd + + R + / t * - Pvd * t Pvv * Pvd + t Pvv * Pvd + t * t Pvd * Pdd + + R + / ~ Pdd * +;t Pvd * Pdd + t Pvv * Pvd + t * t Pvd * Pdd + + R + / t * ~ Pvv * 1 t Pvd * Pdd + t Pvv * Pvd + t * t Pvd * Pdd + + R + / - Pvd * +,t Pvd * Pdd + t Pvv * Pvd + t * t Pvd * Pdd + + R + / t * ~ Pvd * 1 t Pvd * Pdd + t Pvv * Pvd + t * t Pvd * Pdd + + R + / - Pdd * +)
    private static double[][] P_u(double[][] buffer, double[][] P, double R, 
            double t) {
        if (buffer == null)
            buffer = new double[2][2];
        buffer[0][0] = (((1.0-((((t*P[0][0])+P[0][1])/(((((t*P[0][0])+P[0][1])*t
                )+((t*P[0][1])+P[1][1]))+R))*t))*P[0][0])+((-(((t*P[0][0])+P[0][
                1])/(((((t*P[0][0])+P[0][1])*t)+((t*P[0][1])+P[1][1]))+R)))*P[0]
                [1]));
        buffer[0][1] = (((1.0-((((t*P[0][0])+P[0][1])/(((((t*P[0][0])+P[0][1])*t
                )+((t*P[0][1])+P[1][1]))+R))*t))*P[0][1])+((-(((t*P[0][0])+P[0][
                1])/(((((t*P[0][0])+P[0][1])*t)+((t*P[0][1])+P[1][1]))+R)))*P[1]
                [1]));
        buffer[1][0] = (((-((((t*P[0][1])+P[1][1])/(((((t*P[0][0])+P[0][1])*t)+(
                (t*P[0][1])+P[1][1]))+R))*t))*P[0][0])+((1.0-(((t*P[0][1])+P[1][
                1])/(((((t*P[0][0])+P[0][1])*t)+((t*P[0][1])+P[1][1]))+R)))*P[0]
                [1]));
        buffer[1][1] = (((-((((t*P[0][1])+P[1][1])/(((((t*P[0][0])+P[0][1])*t)+(
                (t*P[0][1])+P[1][1]))+R))*t))*P[0][1])+((1.0-(((t*P[0][1])+P[1][
                1])/(((((t*P[0][0])+P[0][1])*t)+((t*P[0][1])+P[1][1]))+R)))*P[1]
                [1]));
        return buffer;
    }

    // x_p_opt = (u 86393797973719314057722693040186329315422158482815410076810976288464951/1800000000000000000000000000000000000000000000000000000000000000000000000 *;xd u 86393797973719314057722693040186329315422158482815410076810976288464951/1800000000000000000000000000000000000000000000000000000000000000000000000 t * * +)
    private static double[] x_p_opt(double[] buffer, double t, double u, double
            [] x) {
        if (buffer == null)
            buffer = new double[2];
        buffer[0] = (u*0.04799655442984406);
        buffer[1] = (x[1]+(u*(0.04799655442984406*t)));
        return buffer;
    }

    // P_p_opt = (0.01,0.0001;0.0001,Pdd 1 +)
    private static double[][] P_p_opt(double[][] buffer, double[][] P) {
        if (buffer == null)
            buffer = new double[2][2];
        buffer[0][0] = 0.01;
        buffer[0][1] = 1.0E-4;
        buffer[1][0] = 1.0E-4;
        buffer[1][1] = (P[1][1]+1.0);
        return buffer;
    }

    // y_opt = xd z - xv t * + ~
    private static double y_opt(double t, double[] x, double z) {
        return (-((x[1]-z)+(x[0]*t)));
    }

    // S_opt = t t Pvv * Pvd 2 * + * R Pdd + +
    private static double S_opt(double[][] P, double R, double t) {
        return ((t*((t*P[0][0])+(P[0][1]*2.0)))+(R+P[1][1]));
    }

    // K_opt = (Pvd t Pvv * + Pdd R + t Pvv t * 2 Pvd * + * + /;t Pvd * Pdd + t t Pvv * 2 Pvd * + * Pdd R + + /)
    private static double[] K_opt(double[] buffer, double[][] P, double R, 
            double t) {
        if (buffer == null)
            buffer = new double[2];
        buffer[0] = ((P[0][1]+(t*P[0][0]))/((P[1][1]+R)+(t*((P[0][0]*t)+(2.0*P[0
                ][1])))));
        buffer[1] = (((t*P[0][1])+P[1][1])/((t*((t*P[0][0])+(2.0*P[0][1])))+(P[1
                ][1]+R)));
        return buffer;
    }

    // x_u_opt = (Pvd t Pvv * + z t xv * xd + - * t Pvv * 2 Pvd * + t * R Pdd + + / xv +;xd Pvd t * Pdd + z xd t xv * + - * R Pdd + t Pvv t * 2 Pvd * + * + / +)
    private static double[] x_u_opt(double[] buffer, double[][] P, double R, 
            double t, double[] x, double z) {
        if (buffer == null)
            buffer = new double[2];
        buffer[0] = ((((P[0][1]+(t*P[0][0]))*(z-((t*x[0])+x[1])))/((((t*P[0][0])
                +(2.0*P[0][1]))*t)+(R+P[1][1])))+x[0]);
        buffer[1] = (x[1]+((((P[0][1]*t)+P[1][1])*(z-(x[1]+(t*x[0]))))/((R+P[1][
                1])+(t*((P[0][0]*t)+(2.0*P[0][1]))))));
        return buffer;
    }

    // P_u_opt = (Pvv Pvd Pvd Pvv t * + * R Pdd + t 2 Pvd * t Pvv * + * + / ~ + Pvv t * Pvd t * R Pdd + + Pvd t Pvv * + / t + / -,Pvd Pvv t * Pvd + * R t / Pdd t / + Pvv t * 2 Pvd * + + / ~ Pvd + Pdd Pvd t Pvv * + * Pdd R + t Pvv t * 2 Pvd * + * + / -;Pvd Pvd t Pvd 2 * t Pvv * + * R Pdd + + / Pdd Pvd t * + * - Pvv Pdd t Pvd * + 2 Pvd * t Pvv * + Pdd R + t / + / * -,Pdd Pdd Pvd t * + Pdd * Pdd R + t Pvv t * Pvd 2 * + * + / - Pvd t * Pdd + Pdd R + t / Pvv t * Pvd 2 * + + / Pvd * -)
    private static double[][] P_u_opt(double[][] buffer, double[][] P, double R
            , double t) {
        if (buffer == null)
            buffer = new double[2][2];
        buffer[0][0] = ((P[0][0]+(-((P[0][1]*(P[0][1]+(P[0][0]*t)))/((R+P[1][1])
                +(t*((2.0*P[0][1])+(t*P[0][0])))))))-((P[0][0]*t)/((((P[0][1]*t)
                +(R+P[1][1]))/(P[0][1]+(t*P[0][0])))+t)));
        buffer[0][1] = (((-((P[0][1]*((P[0][0]*t)+P[0][1]))/(((R/t)+(P[1][1]/t))
                +((P[0][0]*t)+(2.0*P[0][1])))))+P[0][1])-((P[1][1]*(P[0][1]+(t*P
                [0][0])))/((P[1][1]+R)+(t*((P[0][0]*t)+(2.0*P[0][1]))))));
        buffer[1][0] = ((P[0][1]-((P[0][1]/((t*((P[0][1]*2.0)+(t*P[0][0])))+(R+P
                [1][1])))*(P[1][1]+(P[0][1]*t))))-(P[0][0]*((P[1][1]+(t*P[0][1])
                )/(((2.0*P[0][1])+(t*P[0][0]))+((P[1][1]+R)/t)))));
        buffer[1][1] = ((P[1][1]-(((P[1][1]+(P[0][1]*t))*P[1][1])/((P[1][1]+R)+(
                t*((P[0][0]*t)+(P[0][1]*2.0))))))-((((P[0][1]*t)+P[1][1])/(((P[1
                ][1]+R)/t)+((P[0][0]*t)+(P[0][1]*2.0))))*P[0][1]));
        return buffer;
    }

    private static double get_P_vv(double[][] P) {
        return P[0][0];
    }
    
    private static void set_P_vv(double[][] P, double Pvv) {
        P[0][0] = Pvv;
    }
    
    private static double get_P_vd(double[][] P) {
        return P[0][1];
    }
    
    private static void set_P_vd(double[][] P, double Pvd) {
        P[0][1] = Pvd;
    }
    
    private static double get_x_v(double[] x) {
        return x[0];
    }
    
    private static void set_x_v(double[] x, double xv) {
        x[0] = xv;
    }
    
    private static double get_P_dd(double[][] P) {
        return P[1][1];
    }
    
    private static void set_P_dd(double[][] P, double Pdd) {
        P[1][1] = Pdd;
    }
    
    private static double get_x_d(double[] x) {
        return x[1];
    }
    
    private static void set_x_d(double[] x, double xd) {
        x[1] = xd;
    }
    
	/*^ END GENERATED CODE */
}
