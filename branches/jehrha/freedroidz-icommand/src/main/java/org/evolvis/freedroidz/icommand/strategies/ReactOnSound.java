package org.evolvis.freedroidz.icommand.strategies;

import icommand.nxt.SensorPort;
import icommand.nxt.SoundSensor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import org.evolvis.freedroidz.icommand.Services;
import org.evolvis.freedroidz.icommand.sensor.SensorListener;
import org.evolvis.freedroidz.icommand.strategy.AbstractStrategy;
import org.evolvis.freedroidz.icommand.strategy.Strategy;
import org.evolvis.freedroidz.icommand.strategy.StrategyManager;
import org.evolvis.freedroidzI.icommand.control.RobotControl;

public class ReactOnSound extends AbstractStrategy implements SensorListener {

	private SoundSensor sensorFront;
	private SoundSensor sensorRight;
	private SoundSensor sensorRear;
	private SoundSensor sensorLeft;

//	private boolean run = false;
//	private boolean init=true;
	
	protected final static Logger logger = Logger.getLogger(ReactOnSound.class.getName());

	protected void init(){
		logger.info("initializing the sound sensors...");
		sensorFront = new SoundSensor(SensorPort.S1);
		sensorRight = new SoundSensor(SensorPort.S2);
		sensorRear = new SoundSensor(SensorPort.S4);
		sensorLeft = new SoundSensor(SensorPort.S3);
		logger.info("..ready");
	}
	

	@Override
	public void startStrategy() {
		Services.getInstance().getSensorProvider().addSensorListener(this);
		
	}

	@Override
	public void stopStrategy() {
		Services.getInstance().getSensorProvider().removeSensorListener(this);
		
	}

	public ReactOnSound(){
	}

	public String getDescription() {
		return "A robot that runs away from sound";
	}

	public String getName() {
		return "React On Sound Events";
	}

//	public void stop() {
//		Services.getInstance().getSensorProvider().removeSensorListener(this);
//		StrategyManager.getInstance().fireStrategyStopped();
//		run=false;
//	}
//
//	public void run() {
//		if (init) initSensors();
//		StrategyManager.getInstance().fireStrategyStarted();
//		logger.fine("ReactOnSound: registering myself as SensorListener");
//		Services.getInstance().getSensorProvider().addSensorListener(this);
//		run=true;
//		while (run){
//		}
//	}

	public void pollingIntervalExpired() {
		reactOnSound();
	}

	private int threshold=25;
	private int difference =15;
	private int speed = 150;
	private boolean forward=true;
	private int degrees = 20;

	private void reactOnSound(){
		int front = sensorFront.getdB();
		int right = sensorRight.getdB();
		int rear = sensorRear.getdB();
		int left = sensorLeft.getdB();

		if ((forward)&&(rear>front+difference)){
			forward=false;
		} else if ((forward==false)&&(front>rear+difference)){
			forward=true;			
		}

		List<Integer> list = new ArrayList<Integer>(); 
		list.add(front);
		list.add(right);
		list.add(rear);
		list.add(left);

		Collections.sort(list);

		Integer loudest = list.get(3);
		
		if (loudest>threshold){
			if (left>right+difference){
				if (forward)RobotControl.getInstance().turnLeft(degrees);
				else RobotControl.getInstance().turnRight(degrees);
			}
			else if (right>left+difference){
				if (forward) RobotControl.getInstance().turnRight(degrees);
				else RobotControl.getInstance().turnLeft(degrees);
			}
		}
		
//		if (loudest>threshold){
//			if (loudest==left)	RobotControl.getInstance().turnLeft(degrees);
//			else if (loudest==right) 	RobotControl.getInstance().turnRight(degrees);
//		}

		if (forward) RobotControl.getInstance().driveForward(speed);
		else RobotControl.getInstance().driveBackward(speed);

	}

	private void checkSensors(){

		int front = sensorFront.getdB();
		int right = sensorRight.getdB();
		int rear = sensorRear.getdB();
		int left = sensorLeft.getdB();

		List<Integer> list = new ArrayList<Integer>(); 
		list.add(front);
		list.add(right);
		list.add(rear);
		list.add(left);

		Collections.sort(list);

		Integer loudest = list.get(3);

		if (loudest>threshold){

			if (loudest==front){

				RobotControl.getInstance().driveForward(speed);
				forward=true;

			} else if (loudest==right){

				RobotControl.getInstance().turnRight(degrees);
				if (forward){
					RobotControl.getInstance().driveForward(speed);
				} else RobotControl.getInstance().driveBackward(speed);

			} else if (loudest==rear){
				forward=false;
				RobotControl.getInstance().driveBackward(speed);
			} else if (loudest==left){

				RobotControl.getInstance().turnLeft(degrees);
				if (forward){
					RobotControl.getInstance().driveForward(speed);
				} else RobotControl.getInstance().driveBackward(speed);

			}
		}
	}

}
