/**
 * 
 */
package org.evolvis.freedroidz.icommand.strategies;

import icommand.nxt.Motor;
import icommand.nxt.TouchSensor;

import java.io.File;
import java.util.Iterator;
import java.util.logging.Logger;

import org.evolvis.freedroidz.icommand.midi.MidiHelper;
import org.evolvis.freedroidz.icommand.midi.Pitch;
import org.evolvis.freedroidz.icommand.strategy.AbstractStrategy;
import org.evolvis.freedroidz.icommand.strategy.Strategy;
import org.evolvis.freedroidz.icommand.strategy.StrategyManager;
import org.evolvis.freedroidzI.icommand.control.RobotControl;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class Piano extends AbstractStrategy {
	
	protected static final Logger logger = Logger.getLogger(RemoteControl.class.getName());
	
	protected static final int keyHitRadius = 19;
	protected static int keyDistance;
	
	protected static final int keys = 14;
	protected static final int octaveShift = 0;
	protected int startTacho;
	protected int endTacho;
	
	protected Motor hitMotor;
	protected Motor driveMotor;
	
	protected TouchSensor leftTouchSensor;
	protected TouchSensor rightTouchSensor;
	protected int previousNote = -1;

	public void run() {
		fireStrategyStarted();
		
		hitMotor = RobotControl.getInstance().getRobotHardware().getMotorExtra();
		driveMotor = RobotControl.getInstance().getRobotHardware().getMotorLeft();
		
		leftTouchSensor = RobotControl.getInstance().getRobotHardware().getTouchSensor(2);
		rightTouchSensor = RobotControl.getInstance().getRobotHardware().getTouchSensor(1);
		
		hitMotor.setSpeed(900);
		driveMotor.setSpeed(400);
//		hitMotor.rotate(-keyHitRadius);
//		hitMotor.backward();
		
//		playScale(false);
		
		calibrate();
		
		driveMotor.setSpeed(900);
		
		File midiFile = new File(getClass().getResource("/org/evolvis/freedroidz/midi/Alle_Meine_Entchen.mid").getFile()); 
		Iterator<Pitch> tones = MidiHelper.getTonesFromFile(midiFile).iterator();
		
		while(tones.hasNext()) {
			Pitch tone = tones.next();
			hitNote(tone.getNote() + (12*octaveShift));
			previousNote = tone.getNote();
		}
		
//		hitKey();
//		
//		while(!rightTouchSensor.isPressed()) {
//			
//			//driveMotor.rotate(-keyDistance);
//			driveMotor.backward();
//			try {
//				Thread.sleep(100);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			driveMotor.flt();
//			hitKey();
//		}
//		
		stop();
	}

	public String getDescription() {
		return "A piano robot";
	}

	public String getName() {
		return "Piano";
	}
	
	/**
	 * 
	 * Measures the distance of the piano and start-/end-points
	 *
	 */
	protected void calibrate() {
		driveToRightBarrier();
		
		System.out.println("rightBarrier: "+
				(endTacho = driveMotor.getTachoCount()));
		
		driveToLeftBarrier();
		
		System.out.println("leftBarrier: "+
				(startTacho = driveMotor.getTachoCount()));
				
		System.out.println("keyDistance: "+(keyDistance = (startTacho-endTacho+40) / 14));
	}
	
	protected void driveToLeftBarrier() {
		driveToBarrier(true);
	}
	
	protected void driveToRightBarrier() {
		driveToBarrier(false);
	}
	
	protected void hitNote(int note) {
		System.out.println("hitNote "+note);
		if(previousNote != note)
			driveToNote(note);
//		else {
//			try {
//				Thread.sleep(5);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
		
		hitKey();
	}
	
	protected void driveToNote(int note) {
		System.out.println("driveToNote "+note);
		driveToPosition(noteToPosition(note));
	}
	
	protected void driveToPosition(long position) {
		System.out.println("driveToPosition "+position+". Current position: "+driveMotor.getTachoCount());
		driveMotor.rotateTo(position);
	}
	
	protected long noteToPosition(int note) {
		long pos = startTacho + getKeyIndex(note) * -keyDistance;
		System.out.println("noteToPosition "+note+" return "+pos);
		return pos;
	}
	
	protected int getKeyIndex(int note) {
		if(note <= 41)
			return 0;
		else if(note == 43)
			return 1;
		else if(note == 45)
			return 2;
		else if(note == 47)
			return 3;
		else if(note == 48)
			return 4;
		else if(note == 50)
			return 5;
		else if(note == 52)
			return 6;
		else if(note == 53)
			return 7;
		else if(note == 55)
			return 8;
		else if(note == 57)
			return 9;
		else if(note == 59)
			return 10;
		else if(note == 60)
			return 11;
		else if(note == 62)
			return 12;
		else
			return 13;			
	}
	
	protected void driveToBarrier(boolean left) {
		if(left)
			driveMotor.forward();
		else
			driveMotor.backward();
		
		while((left && !leftTouchSensor.isPressed()) || (!left && !rightTouchSensor.isPressed())) {
					
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		driveMotor.flt();
	}
	
	protected int noteToLocation(int note) {
		return note;
	}
	
	protected void playScale(boolean up) {
		if(up)
			driveToLeftBarrier();
		else
			driveToRightBarrier();
		
		RobotControl.getInstance().getRobotHardware().getMotorExtra().rotate(keyHitRadius-4);
		
		if(up)
			driveToRightBarrier();
		else
			driveToLeftBarrier();
		
		RobotControl.getInstance().getRobotHardware().getMotorExtra().rotate(-(keyHitRadius-4));
	}
	
	/**
	 * Activates the trigger for the current key
	 * 
	 */
	protected void hitKey() {
		hitMotor.rotate(keyHitRadius);
//		hitMotor.forward();
//		try {
//			Thread.sleep(4);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		hitMotor.backward();
		hitMotor.rotate(-keyHitRadius);	
	}
//
//	public void commandSent(Command command) {
//		logger.info("processing "+command);
//
//		int code = command.getCode();
//
//		if(SWT.ARROW_UP == code || KeyEvent.VK_UP == code) {
//
//			
//			hitKey();
//		}
//		else if(SWT.ARROW_DOWN == code || KeyEvent.VK_DOWN == code) {
//
//		}
//		else if(SWT.ARROW_LEFT == code || KeyEvent.VK_LEFT == code) {
//			RobotControl.getInstance().getRobotHardware().getMotorLeft().rotate(keyDistance);
//		}
//		else if(SWT.ARROW_RIGHT == code || KeyEvent.VK_RIGHT == code) {
//			RobotControl.getInstance().getRobotHardware().getMotorLeft().rotate(-keyDistance);
//		}
//		else
//			logger.info("unknown command: "+command);
//	}

//	public void commandReleased(Command command){
//		//do nothin
//	}

	public void stop() {
//		Services.getInstance().getCommandProvider().removeCommandListener(this);
		fireStrategyStopped();
	}

}
