/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.freedroidz.icommand.strategies;

import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.evolvis.freedroidz.icommand.Services;
import org.evolvis.freedroidz.icommand.command.Command;
import org.evolvis.freedroidz.icommand.command.CommandListener;
import org.evolvis.freedroidz.icommand.strategy.AbstractStrategy;
import org.evolvis.freedroidz.icommand.strategy.Strategy;
import org.evolvis.freedroidz.icommand.strategy.StrategyManager;
import org.evolvis.freedroidzI.icommand.control.RobotControl;

public class RemoteControl extends AbstractStrategy implements CommandListener{

	protected static final Logger logger = Logger.getLogger(RemoteControl.class.getName());

	boolean moving = false;
	

	@Override
	public void startStrategy() {
		// TODO change implementation to new template method
		
	}

	@Override
	public void stopStrategy() {
		// TODO change implementation to new template method
		
	}

	public void run() {
		logger.info("Strategy Remote Control");
		logger.info("need to register myself to the Command Provider");
		Services.getInstance().getCommandProvider().addCommandListener(this);
//		RobotControl.getInstance().getRobotHardware().getMotorExtra().setSpeed(100);


		Display.getDefault().asyncExec(new Runnable() {

			public void run() {

				Shell shell = new Shell();
				//SteeringPanel steering = new SteeringPanel(shell);

				shell.open();

				fireStrategyStarted();

				while (!shell.isDisposed()) {
					if (!shell.getDisplay().readAndDispatch()) {
						shell.getDisplay().sleep();
					}
				}
			}
		});

	}

	public String getDescription() {
		return "Simple remote control strategy\n";
	}

	public String getName() {
		return "Remote Control with additional UI";
	}

	public void commandSent(Command command) {
		logger.info("processing "+command);
		processCommand(command);
	}

	public void commandReleased(Command command){
		//do nothing	
	}

	/**
	 * The current speed of the robot. If negative robot is driving backwards
	 */
	private int currentSpeed = 0;
	/**
	 * The current turn of the robot in degrees. If negative robot is turning left, otherwise right. 
	 */
	private int currentTurnRatio = 0;

	/**
	 * The speed is in-/decreased by this constant per step.
	 */
	private int speedPerStep = 100;
	/**
	 * The turning is in-/decreased by this constant per step.
	 */
	private int turnWhileMovingStep = 20;

	/**
	 * The robot turns by this degrees if it is not moving.
	 */
	private int turnWhileStandingInDegrees = 25;

	/**
	 * The maximum speed of the robot.
	 */
	private int maximumSpeed = 900;


	private void faster() {
		if(currentSpeed < maximumSpeed)
			currentSpeed += speedPerStep;
	}

	private void slower() {
		if(currentSpeed > -maximumSpeed)
			currentSpeed -= speedPerStep;
	}

	private void turnLeft() {
		if(currentTurnRatio > -180)
			currentTurnRatio -= turnWhileMovingStep;
	}

	private void turnRight() {
		if(currentTurnRatio < 180)
			currentTurnRatio += turnWhileMovingStep;
	}

	private void driveForOrBackward(int speed){
		if (speed > 0)
			RobotControl.getInstance().driveForward(speed);

		else if (speed < 0)
			RobotControl.getInstance().driveBackward(-speed);

		else
			RobotControl.getInstance().brake();
	}

	private void processCommand(Command command){

		/*
		 * Available commands on a Nokia Internet Tablet:
		 * 
		 * Key			-> Keycode
		 * 
		 * Home  		-> 16777230
		 * Fullscreen	-> 16777231
		 * Plus			-> 16777232
		 * Minus		-> 16777233
		 * Back			-> 27 (Escape) 
		 * 
		 */

		switch (command.getCode()) {

		case SWT.ARROW_UP:
			if (currentTurnRatio == 0)
				faster();

			currentTurnRatio = 0;
			driveForOrBackward(currentSpeed);

			break;

		case SWT.ARROW_DOWN:
			if (currentTurnRatio == 0)
				slower();

			currentTurnRatio = 0;
			driveForOrBackward(currentSpeed);

			break;

		case SWT.ARROW_LEFT:
			turnLeft();

			if(currentSpeed == 0)
				RobotControl.getInstance().turnLeft(turnWhileStandingInDegrees);

			else
				RobotControl.getInstance().turnWhileDriving(Math.abs(currentSpeed), currentSpeed < 0, currentTurnRatio);

			break;

		case SWT.ARROW_RIGHT:
			turnRight();

			if(currentSpeed == 0)
				RobotControl.getInstance().turnRight(turnWhileStandingInDegrees);

			else
				RobotControl.getInstance().turnWhileDriving(Math.abs(currentSpeed), currentSpeed < 0, currentTurnRatio);

			break;

		case SWT.ESC:
		case ' ': // Space-bar
			currentTurnRatio = 0;
			driveForOrBackward(currentSpeed = 0);
			break;

		case 'y':
		case 16777233:

			RobotControl.getInstance().getRobotHardware().getMotorExtra().backward();
			break;

		case 'c':
		case 16777232:

			RobotControl.getInstance().getRobotHardware().getMotorExtra().forward();
			break;

		case 'x':

			RobotControl.getInstance().getRobotHardware().getMotorExtra().stop();
			break;

		default:
			System.out.println(command.getCode());
		}
	}

	public void stop() {
		RobotControl.getInstance().brake();
		RobotControl.getInstance().getRobotHardware().getMotorExtra().stop();
		Services.getInstance().getCommandProvider().removeCommandListener(this);
		fireStrategyStopped();
	}


}