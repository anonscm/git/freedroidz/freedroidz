package org.evolvis.freedroidz.icommand.strategies.icommand.mrsensitive;

public class ForwardPull extends StateMachineControlState {

	public ForwardPull(StateMachineData data) {
		super(data);
	}
	@Override
	public boolean isMotorInfoRequired(int i) {
		return i==RobotHardware.LOWER;
	}
	@Override
	public StateMachineControlState enter() {
		RobotHardware hardware = data.getHardware();
		hardware.move(RobotHardware.LOWER, 75,600);
		//hardware.move(RobotHardware.UPPER,20,100);
		return super.enter();
	}
	@Override
	public StateMachineControlState complete(int[] i) {
		RobotHardware hardware = data.getHardware();
		
		if(hardware.isComplete(RobotHardware.LOWER)){
			return new ForwardLiftFood(data);
		}
		return super.complete(i);
	}
}
