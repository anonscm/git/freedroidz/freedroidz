
package org.evolvis.freedroidz.icommand.strategies.icommand.mrsensitive;

import icommand.nxt.Motor;

import org.eclipse.swt.SWT;

public class Idle extends StateMachineControlState{

	public Idle(StateMachineData data) {
		super(data);
	}

	@Override
	public StateMachineControlState enter() {
		RobotHardware hardware = data.getHardware();
		
		for(int i=0;i<3;i++){
			 hardware.stop(i,900);
			 //data.getHardware().setSpeed(i,0);
			 
			 
		}
		return null;
		
	}
	
	@Override
	public boolean isSensorInfoRequired(int i) {
		// TODO Auto-generated method stub
		return true;
	}

	
}
