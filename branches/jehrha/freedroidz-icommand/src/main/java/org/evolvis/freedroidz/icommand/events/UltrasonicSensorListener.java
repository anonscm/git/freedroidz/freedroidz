/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.freedroidz.icommand.events;

import org.evolvis.freedroidzI.icommand.control.RobotControl;

/**
 * A listener for the Ultrasonic Sensor
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public abstract class UltrasonicSensorListener extends AbstractSensorListener {

	protected int rangeBegin;
	protected int rangeEnd;
	
	/**
	 * Creates a SensorListener which calls the method eventOccured every time
	 * the metric value of the UltrasonicSensor is in given range.
	 * 
	 * @param rangeBegin the begin of the range (in cm)
	 * @param rangeEnd the end of the range (in cm)
	 */
	public UltrasonicSensorListener(int rangeBegin, int rangeEnd) {
		this.rangeBegin = rangeBegin;
		this.rangeEnd = rangeEnd;
		
		// Starts checking the sensors 
		run();
	}
	
	/**
	 * @see org.evolvis.freedroidz.icommand.events.SensorListener#eventOccured(org.evolvis.freedroidz.icommand.events.SensorEvent)
	 */
	public abstract void eventOccured(SensorEvent event);
	
	/**
	 * @see org.evolvis.freedroidz.icommand.events.AbstractSensorListener#checkSensorValues()
	 */
	@Override
	protected SensorEvent checkSensorValues() {
		int distance = RobotControl.getInstance().getRobotHardware().getUltrasonicSensor(1).getDistance();
		
		if(distance <= rangeEnd && distance >= rangeBegin)
			return new SensorEvent(Integer.valueOf(distance));
		
		return null;
	}
}
