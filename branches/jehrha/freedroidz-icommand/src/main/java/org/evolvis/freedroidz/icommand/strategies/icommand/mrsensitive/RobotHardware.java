package org.evolvis.freedroidz.icommand.strategies.icommand.mrsensitive;

import icommand.nxt.Motor;

/**
 * Boundary abstraction.
 * 
 * Should be used to access sensors/motors.
 * The dispatch method should be used to call dispatch events to the control. 
 * 
 * I hope to be able to use the control subsystem for both jmephysics and freedroidz.
 * @author lukas
 *
 */
public interface RobotHardware {
	
	public static final int BODY = 0;
	public static final int FINGER = 0;
	public static final int LOWER = 2;
	public static final int UPPER = 1;
	
	public int getUpperArmPosition();
	public int getLowerArmPosition();
	public int getBodyPosition();
	public int getDistance();
	public boolean isTouching(int i);
	public boolean isComplete(int i);
	
	
	public void move(int axis, int deg,int speed);
	public void moveTo(int i, int deg,int speed);
	public void stop(int i, int j);
	public void setSpeed(int i,int speed);
	public void resetTachoCount(int i);
	
	
	/**
	 * dispatch events.
	 * This method is called by the "world" i.e. a physics simulation callback or
	 * the real sensor polling loop. It should use the the control to identify and dispatch 
	 * events.
	 */
	public void dispatch(RobotControl control,long time);
	
	
	
}
