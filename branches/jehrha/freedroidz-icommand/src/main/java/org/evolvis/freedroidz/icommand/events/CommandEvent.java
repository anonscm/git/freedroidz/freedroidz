package org.evolvis.freedroidz.icommand.events;

import org.evolvis.freedroidz.icommand.command.Command;

public class CommandEvent {
	
	private Command command;
	private boolean released;
	
	public CommandEvent(Command command, boolean released) {
		super();
		this.command = command;
		this.released = released;
	}
	public Command getCommand() {
		return command;
	}
	public void setCommand(Command command) {
		this.command = command;
	}
	public boolean isReleased() {
		return released;
	}
	public void setReleased(boolean released) {
		this.released = released;
	}

}
