/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */
//TODO: beim nach vorne fahren den minimalen Abstand merken statt durchschnitt.
//TODO: beim start zum nahesten Punkt fahren
//TODO: statt 90° drehen bis Sicht vorne wieder frei ist
package org.evolvis.freedroidz.icommand.strategies;

import icommand.nxt.Motor;
import icommand.nxt.UltrasonicSensor;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
//import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.evolvis.freedroidz.icommand.Services;
import org.evolvis.freedroidz.icommand.command.Command;
import org.evolvis.freedroidz.icommand.command.CommandListener;
import org.evolvis.freedroidz.icommand.strategy.AbstractStrategy;
import org.evolvis.freedroidz.icommand.strategy.Strategy;
import org.evolvis.freedroidzI.icommand.control.RobotControl;
import org.evolvis.freedroidzI.icommand.control.RobotHardware;


/**
 * @author Hendrik Helwich
 */
@SuppressWarnings("unused")
public class BuildMap extends AbstractStrategy implements CommandListener {
	
	/*^
	 * # constants
	 * PI = 3.1415926535897932384626433832795028841971693993751058209749445923078164
	 * DISTANCE_REACT = 100
	 * MIN_DISTANCE  = 20
	 * DEFAULT_SPEED = 5 # cm per second
	 * SPEED_MINIMUM = 20
	 * C = PI wd * 360 /
	 * 
	 * 
	 * MEASURING_ERROR = 2
	 * 
	 * VAR_DIST = 1 1 *
	 * VAR_SPEED = 0.1 0.1 *
	 * COVAR_SPEED_DIST = 0.01 0.01 *
	 * 
	 * # system model
	 * F = (1, 0, ux, uy ~, 0, 0; 0, 1, uy, ux, 0, 0; 0, 0, ura, urb ~, 0, 0; 0, 0, urb, ura, 0, 0; 0, 0, 0, 0, 1, 0; 0, 0, 0, 0, 0, 1)
	 * x = (xx; xy; xra; xrb; xox; xoy)
	 * P = (Px, 0, 0, 0, 0, 0; 0, Py, 0, 0, 0, 0; 0, 0, Pra, 0, 0, 0; 0, 0, 0, Prb, 0, 0; 0, 0, 0, 0, Pox, 0; 0, 0, 0, 0, 0, Poy)
	 * u = (ux; uy; ura; urb)
	 * B = (0, 0, 0, 0; 0, 0, 0, 0; 0, 0, 0, 0; 0, 0, 0, 0; 0, 0, 0, 0; 0, 0, 0, 0)
	 * Q = (Qx, Qxy, 0, 0, 0, 0; Qxy, Qy, 0, 0, 0, 0; 0, 0, Qra, Qrab, 0, 0; 0, 0, Qrab, Qrb, 0, 0; 0, 0, 0, 0, Qox, Qoxy; 0, 0, 0, 0, Qoxy, Qoy)
	 * 
	 * # measurement model
	 * H = (xra, xrb; xrb ~, xra) (-1, 0, 0, 0, 1, 0; 0, -1, 0, 0, 0, 1) *
	 * R = (Rx, Rxy; Rxy, Ry)
	 *
	 * # prediction
	 * x_p = F x * B u * +                 # predicted state
	 * P_p = F P * F ^t * Q +              # predicted state covariance
	 * 
	 * # update
	 * z = (zx; zy)
	 * y = z H x * -                       # innovation
	 * y2_ = H x *
	 * S = H P * H ^t * R +                # innovation covariance
	 * K = S H P * slvspd ^t               # kalman gain (K = P H ^t * S ^-1 *)
	 * x_u = x K y * +                     # update state
	 * P_u = H width ones K H * - P *      # update state covariance
	 * #T = 1/2 K * S * P H ^t * - K ^t *
	 * #P_u = P T + T +
	 * 
	 * # optimized prediction
	 * x_p_opt = x_p # (ux xra * xx + uy xrb * -;xy uy xra * + ux xrb * +;ura xra * urb xrb * -;urb xra * ura xrb * +;xox;xoy) # opt
	 * P_p_opt = P_p #(Px ux Pra * ux * + uy ~ Prb * uy ~ * + Qx +,ux Pra * uy * uy ~ Prb * ux * + Qxy +,ux Pra * ura * uy ~ Prb * urb ~ * +,ux Pra * urb * uy ~ Prb * ura * +,0,0;uy Pra * ux * ux Prb * uy ~ * + Qxy +,Py uy Pra * uy * + ux Prb * ux * + Qy +,uy Pra * ura * ux Prb * urb ~ * +,uy Pra * urb * ux Prb * ura * +,0,0;ura Pra * ux * uy urb ~ Prb * * ~ +,ura Pra * uy * urb Prb * ~ ux * +,ura Pra * ura * urb Prb * ~ urb ~ * + Qra +,ura Pra urb * * urb Prb * ~ ura * + Qrab +,0,0;urb Pra * ux * uy Prb ura * * ~ +,urb Pra * uy * ura Prb * ux * +,urb Pra * ura * urb Prb ura * * ~ + Qrab +,urb Pra * urb * Qrb ura Prb * ura * + +,0,0;0,0,0,0,Pox Qox +,Qoxy;0,0,0,0,Qoxy,Poy Qoy +) # opt
	 * 
	 * # optimized update
	 * y_opt = y # (xx xra * zx + xox xra * - xoy xy - xrb * -;xy xoy - xra * zy xx xox - xrb * - +) # opt
     * S_opt = S # (Pox Px + xra xra * * Rx xrb xrb * Py Poy + * + +,xrb xra * Py Poy Pox - + * Rxy xra xrb * Px * - +;Rxy xrb xra * Pox * - Py Poy + xra xrb * * xrb xra Px * * - +,xrb xrb * Px Pox + * Ry Py Poy + xra xra * * + +) # opt
     * K_opt = K # (xra ~ Px * Rxy xra Poy * xrb * + xrb ~ xra Pox * * xra xrb Px * * - xrb ~ xra ~ * Py * + + xrb Px xrb * * xra xra ~ Py * * - xrb xrb ~ Pox * * - Ry Poy xra * xra * + + / Px xrb * * - Rx xra Px * ~ xra ~ * + xra xra Pox * * + xrb Poy * xrb * xrb ~ xrb * Py * - + xra Poy xrb * * Rxy xrb Pox xra * * - + xrb xra ~ Px * * xrb xra ~ Py * * - + xrb xra ~ Py * * ~ xra Px * ~ xrb * + xra xrb * Poy * Pox xra * xrb ~ * + + Rxy + * Ry xra Poy xra * * + xrb xrb * Px * xra ~ xra ~ Py * * + Pox xrb ~ xrb * * - + / - /,xrb Px * xrb Px * xrb * xra ~ xra ~ * Py * + xrb ~ xrb ~ Pox * * + xra Poy * xra * + Ry + / Rxy xra xrb Poy * * + xrb ~ Pox * xra * + xra ~ Px * xrb * xrb ~ Py * xra ~ * + + Ry xrb Px * xrb * Py xra ~ * xra ~ * + xrb ~ Pox xrb ~ * * + xra Poy * xra * + + / xra ~ Px * Rxy xrb Poy * xra * + xra ~ Px * xrb * xrb ~ Py * xra ~ * xrb ~ Pox xra * * + + + Pox xrb ~ xrb ~ * * xra ~ Py * xra ~ * + Ry xra Poy xra * * + xrb Px * xrb * + + / xrb Px * * - * xrb Poy * xrb * xra Pox * xra * + Rx xra ~ Px * xra ~ * + xrb ~ Py * xrb ~ * + + xra ~ Px * xra xrb * Poy * xra Pox * xrb ~ * + xrb ~ Py * xra ~ * + xrb / + xrb * Rxy + Poy xra xra * * xrb Pox * ~ xrb ~ * + xrb Px xra ~ Py * xra ~ * xrb xrb * / + xrb * * + Ry + / Rxy xrb Poy * xra * + xra Pox * xrb ~ * + Px xra ~ * xrb * xrb ~ Py * xra ~ * + + * - / -;xrb ~ Py * xra ~ Py * Ry Pox xrb xrb ~ * * - xra Poy * xra * + xrb Px * xrb * xra xra ~ Py * * - + / xrb Px xra * * ~ xra ~ xrb ~ Py * * + Rxy + xrb xra Pox * * ~ Poy xrb xra * * + + * - xrb Py xrb * * xra xra ~ Px * * - xrb Poy xrb * * xra xra * Pox * Rx + + + Py xrb * xra * Px xra xrb * * - Rxy Pox xrb xra * * - xrb Poy * xra * + + xrb Poy * xra * Rxy + xra Py xrb * * xra ~ Px * xrb * Pox xra * xrb * - + + * xrb xrb Pox * * xra Poy * xra * + Px xrb * xrb * xra ~ Py xra ~ * * + + Ry + / - /,Rxy xra xrb ~ Px * ~ * - Poy xrb * xra * + xrb ~ Py * xra ~ * Pox xra * xrb ~ * + + xrb Px * xrb * xra ~ Py * xra ~ * + xra Poy * xra * xrb ~ Pox * xrb ~ * + + Ry + / xrb ~ Py * xra Py * Px xrb xra ~ * * Py xrb ~ xra * * - Rxy Poy xrb * xra * xrb Pox xra * * ~ + + + * Ry xrb Px * xrb * + Py xra ~ xra ~ * * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + / + * Rx Poy xrb * xrb * + xra Pox * xra * + xrb ~ Py * xrb ~ * + xra ~ Px * xra ~ * + Rxy xrb xra Poy * * + xra ~ xrb * Px Py xrb / xrb ~ * + * xra Pox * xrb ~ * + + Rxy xrb Poy * xra * + Px xra * xrb * ~ xrb Py * ~ xra ~ * xra Pox * xrb ~ * + + + xrb Px * xrb * xra Poy * xra * xrb ~ Pox * xrb ~ * + xra xra ~ Py * * - + Ry + / * - / xra ~ Py * xrb Px * xrb * xra ~ Py * xra ~ * + Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + + / - ~;0,0;0,0;xra Pox * Rxy xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xrb ~ Pox xra * * + xrb Poy * xra * + + xra xra ~ Py * * ~ xrb ~ Px * ~ xrb * + Ry xra Poy * xra * + xrb ~ xrb ~ Pox * * + + / xrb ~ Pox * * - xra ~ Px * xra ~ * Rx xrb Poy * xrb * + xra Pox * xra * + xrb xrb ~ Py * * ~ + + xrb Poy * xra * Rxy + xra ~ Px * xrb * xrb ~ Py * xra ~ * + Pox xrb ~ * xra * + + xrb ~ ~ Px * xrb * xra ~ Py * xra ~ * + xra xra * Poy * Ry + xrb ~ Pox * xrb ~ * + + / xrb ~ ~ Px * xrb ~ ~ * Py xra ~ * xra ~ * + Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + + * Rxy xrb Poy * xra * + xra Pox * xrb ~ * + xra Px * ~ xrb ~ ~ * Py xra ~ xrb ~ * * + + xra ~ Py * xra ~ * xrb Px * xrb * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - /,xrb ~ Pox * xra Poy * xra * xrb ~ Pox * xrb ~ * + xrb Px * xrb * xra ~ Py * xra ~ * + + Ry + / Py xrb ~ * xra ~ * xra ~ Px * xrb * xrb xra Pox * * - + xrb Poy * xra * + Rxy + Ry xra Poy * xra * + xrb Px * xrb * xra ~ Py * xra ~ * + xrb Pox * ~ xrb ~ * + + / xra Pox * xrb ~ Pox xra ~ Px xrb * * xra ~ xrb ~ * Py * + xra xrb Poy * * xra Pox * xrb ~ * + + Rxy + Ry xra Poy * xra * + xrb Px * xrb * Pox xrb ~ * xrb ~ * Py xra ~ * xra ~ * + + + / * * - * xra ~ Px * xra ~ * xrb ~ xrb ~ Py * * + xra Pox * xra * + xrb Poy * xrb * + Rx + Ry xra Poy * xra * + xrb ~ xrb ~ Pox * * + xrb Px * xrb * xra ~ Py * xra ~ * + + xra Pox * xrb ~ * xra ~ Px * xrb * Py xra ~ xrb ~ * * + + xrb Poy * xra * + Rxy + xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * xra ~ Px * xrb * xrb xra Pox * * - xrb ~ Py * xra ~ * + Rxy xrb Poy * xra * + + Py xra ~ * xra ~ * xrb Pox * ~ xrb ~ * + xra xra * Poy * + xrb Px * xrb * + Ry + / * - / -;Poy xrb * Poy xra * xra Py xrb * * xra ~ Px * xrb * xrb Poy * xra * xrb xra Pox * * ~ + + + Rxy + Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + xrb xrb Px * * xra ~ xra ~ * Py * + + / * - Rx xrb Poy * xrb * + xra ~ Px * xra ~ * xrb Py * ~ xrb ~ * + xra Pox xra * * + + xrb Px * xra ~ * Rxy xrb Poy * xra * + Pox xra * xrb ~ * + xrb Py xra ~ * * ~ + + Px xrb * xrb * xra ~ xra ~ Py * * + xra Poy * xra * xrb ~ Pox * xrb ~ * + + Ry + / Px xrb * xrb * Ry xra Poy * xra * + xra Py xra ~ * * ~ xrb ~ Pox xrb ~ * * + + + * Rxy xrb Poy * xra * + xrb xra Pox * * xra Px * ~ xrb * xrb ~ xra Py * * - - - xrb ~ xrb ~ Pox * * xrb Px xrb * * xra ~ Py * xra ~ * + + Ry xra xra Poy * * + + / * - /,xra Poy * Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + xra ~ Py * xra ~ * + xrb Px * xrb * + / xrb Poy * xra ~ Px * xrb * Pox xra * xrb ~ * + xra xrb ~ Py * * - Rxy Poy xra * xrb * + + xra Poy * xra * xrb ~ xrb ~ Pox * * + Ry xrb Px xrb * * + xra ~ Py * xra ~ * + + / xra Poy * * - Rxy xrb xra * Poy * + Pox xra xrb ~ * * + xrb Py xra ~ * * ~ xra xrb Px * * - + * Rx xrb Poy * xrb * + xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + + Poy xra * xrb * xra xrb ~ * Pox * + xra ~ xrb Px * * xrb ~ Py * xra ~ * + + Rxy + xrb ~ Px * ~ xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb xrb Px * * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + Poy xra * xra * + Ry + * Rxy xrb xra * Poy * + xra Pox * xrb ~ * + xra ~ xrb * Px * xra xrb ~ Py * * - + xra Poy * xra * Ry + xrb ~ Pox * xrb ~ * + xrb Px * xrb * xra ~ Py * xra ~ * + + / * - xra ~ Py * xra ~ * xrb ~ Pox * xrb ~ * + Px xrb xrb * * + xra Poy * xra * + Ry + * / -) # opt
	 * x_u_opt = x_u # (xx xrb Px * xrb Px * xrb * xra xra ~ Py * * - xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb * xrb ~ Py xra ~ * * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb xrb Px * * xra Py * ~ xra ~ * + Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + + / xra ~ Px * Px xra ~ * xrb * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy xra * * + Rxy + xra Poy * xra * xrb ~ Pox xrb ~ * * xra ~ Py * xra ~ * + xrb Px * xrb * + + Ry + / xrb Px * * - * xrb ~ Py * xrb ~ * xra Px * ~ xra ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + Rxy xra Poy * xrb * xra Pox * xrb ~ * + xra xrb Px * * - + xrb ~ Py * xra ~ * + xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + Ry xra Poy * xra * + + / xrb Px * xrb * Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + xra ~ Py * xra ~ * + + * xra Px * ~ xrb * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xra Poy * xra * xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + + Ry + / * - / - zy xra xoy * xrb ~ xox * + xrb xx * xra xy * - + - * + xra ~ Px * xra ~ xrb ~ ~ * Px * xra Py xrb ~ * * ~ + xra xrb ~ Pox * * + xrb Poy * xra * + Rxy + xrb Px * xrb ~ ~ * xra Py xra ~ * * - xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb Px * * - xrb Poy * xrb * Pox xra * xra * + xrb ~ Py * xrb ~ * + xra ~ Px * xra ~ * + Rx + xra ~ Px * xrb * xrb xra Pox * * - xrb ~ Py * xra ~ * + xrb Poy * xra * + Rxy + Ry xra Poy * xra * + xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + + * xra Poy * xra * xrb ~ Pox * xrb ~ * + xra ~ Py * xra ~ * + Px xrb ~ * xrb * - Ry + / xra ~ Px * xrb * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xra xrb Poy * * + Rxy + Ry xrb Px * xrb * xra ~ xra ~ * Py * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + + / * - / zx xra ~ xx * xrb ~ xy * + xra xox * + xrb xoy * + - * +;xrb ~ Py * Py xra ~ * xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / Rxy xrb Poy * xra * + xra Pox * xrb * - xra ~ Px * xrb * xra Py * ~ xrb ~ * + + * - Px xra ~ * xra ~ * xrb Py * ~ xrb ~ * + xra Pox * xra * + Rx xrb Poy * xrb * + + xra ~ Px * xrb * xra ~ xrb ~ Py * * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xra Poy * xra * xrb ~ Pox * xrb ~ * + Px xrb * xrb * xra Py * ~ xra ~ * + + Ry + / Px xrb xra ~ * * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb xra * Poy * + Rxy + xrb Px xrb * * xra ~ Py * xra ~ * + Ry xra xra * Poy * + xrb ~ Pox * xrb ~ * + + / xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy xra * * + Ry + * * - / xra ~ xx * xrb ~ xy * + xra xox * + xrb xoy * + ~ zx + * xy xra Py Px xrb * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + Ry xra Poy * xra * + + / * ~ xrb ~ Py * xrb Py * ~ xra ~ * xra Px xrb * * - xra Pox * xrb ~ * + xra Poy * xrb * + Rxy + Py xra ~ * * Px xrb xrb * * Pox xrb ~ * xrb ~ * Py xra xra ~ * * - + xra Poy * xra * + Ry + / - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + Poy xrb * xrb * xra Pox * xra * + + Rx + Rxy xra ~ Px * xrb * xrb xra Pox * * - xrb ~ Py * xra ~ * + xrb Poy * xra * + + Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + xra ~ Py * xra ~ * + xrb Px * xrb * + / xra ~ Px * xrb * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + Poy xra xrb * * + Rxy + * - / xrb Poy * xra * xra Pox * xrb ~ * + xra ~ Px * xrb * xrb ~ Py * xra ~ * + + Rxy + Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + xrb Px xrb * * + xra Py xra ~ * * - / * - zy xrb xx * xra ~ xy * + xrb ~ xox * xra xoy * + + - * + +;xra;xrb;xox xrb ~ Pox * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / Rxy xrb Poy * xra * + xra Pox * xrb ~ * + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + Px xra ~ * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xra Poy * xra * xrb ~ Pox * xrb ~ * + xrb ~ ~ Px * xrb * xra ~ Py * xra ~ * + + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra Py * ~ xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xrb Poy * xra * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + + Rxy + xrb ~ ~ Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - zy xrb ~ ~ xx * xra ~ xy * + xrb ~ xox * + xra xoy * + - * + xra Pox * xrb Pox xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + Poy xra xra * * xrb ~ Pox * xrb ~ * + xrb ~ xrb Px * * ~ Py xra ~ xra ~ * * + + Ry + / * * ~ - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + Ry Poy xra xra * * + + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ xrb ~ ~ Px * * ~ xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / zx xra ~ xx * xrb ~ xy * + xra xox * + xrb xoy * + - * +;xrb Poy * Rxy Poy xrb * xra * + Pox xra xrb ~ * * xrb ~ Py * xra ~ * + xrb xra ~ * Px * + + Px xrb * xrb * Py xra ~ * xra ~ * + xrb ~ Pox * xrb ~ * + Ry xra Poy * xra * + + / xra Poy * * - xra ~ Px xra ~ * * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb xrb Poy * * + Rx + xra ~ Px * xrb * xrb ~ Py * xra ~ * + Pox xra * xrb ~ * + xrb Poy * xra * + Rxy + xrb Px * xrb * xra ~ Py * xra ~ * + xrb Pox * ~ xrb ~ * + xra Poy * xra * + Ry + / xrb Px * xrb * xra ~ Py * xra ~ * + xra Poy * xra * Pox xrb ~ * xrb ~ * + + Ry + * Py xra ~ * xrb ~ * xrb Poy * xra * xrb ~ Px xra ~ * * - Pox xra xrb ~ * * + + Rxy + xrb Px * xrb * xra ~ Py * xra ~ * + xrb Pox xrb ~ * * ~ + Ry xra Poy * xra * + + / * - / zx xra ~ xx * xrb ~ xy * + xra xox * + xrb xoy * + - * xoy xra Poy * xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + Poy xra * xra * + Ry + / xra ~ Px * xrb * Rxy xrb Poy * xra * + xrb xra Pox * * - xrb ~ Py * xra ~ * + + Ry xrb xrb * Px * xra xra ~ Py * * ~ + xrb ~ xrb ~ * Pox * + xra Poy * xra * + + xrb Poy * xrb * xra Pox * xra * + xra ~ Px * xra ~ * xrb ~ xrb Py * ~ * + + Rx + xrb ~ Py * xra ~ * xra Px * ~ xrb * Pox xra xrb * * - + xrb Poy * xra * + Rxy + xrb Px xrb * * xra ~ Py * xra ~ * + xrb ~ xrb ~ * Pox * + xra Poy * xra * + Ry + / xra ~ Py * xra ~ * xrb Px * xrb * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra Pox * xrb ~ * xrb ~ Py * xra ~ * + xra Px xrb * * - xrb Poy * xra * + Rxy + xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - * / xrb Poy * Poy xra * xrb xra ~ * Px * xrb Py * ~ xra ~ * + xra Pox * xrb ~ * + Rxy xrb Poy * xra * + + xrb xrb Px * * xra Poy * xra * Ry + xrb ~ Pox * xrb ~ * + xra ~ Py * xra ~ * + + / * - * - zy xrb xx * xra ~ xy * + xrb ~ xox * + xra xoy * + - * + +) # opt
	 * P_u_opt = P_u # (1 Px xra ~ * Rxy xrb Poy * xra * + xrb xra Pox * * - xra ~ Px * xrb * xrb ~ Py * xra ~ * + + xrb xrb Px * * xra ~ Py * xra ~ * + xrb ~ xrb ~ * Pox * + Poy xra xra * * + Ry + / xrb Px * * - xra ~ Px xra ~ * * xrb ~ Py * xrb ~ * + xrb Poy * xrb * xra Pox * xra * + + Rx + xra ~ Px * xrb * Py xrb ~ * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb Px * xrb * xra ~ xra ~ Py * * + xrb ~ Pox * xrb ~ * + xra xra * Poy * + Ry + / Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + xrb ~ ~ Px * xrb * xra ~ Py * xra ~ * + + * xra ~ Px * xrb * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + Pox xrb ~ xrb ~ * * xra ~ Py * xra ~ * + xrb Px * xrb ~ ~ * + xra Poy * xra * + Ry + / * - / xra ~ * xrb Px * xrb Px xrb ~ ~ * * xra ~ Py * xra ~ * + xrb Pox * ~ xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + Px xrb xrb * * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + Poy xra * xra * + Ry + / xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb Px * * xrb ~ Px * ~ xrb * xra ~ Py * xra ~ * + xrb ~ Pox xrb ~ * * + xra Poy * xra * + Ry + / - xra Pox * xra * Rx xrb Poy * xrb * + xrb Py xrb ~ * * - + Px xra ~ * xra ~ * + xrb Px xrb * * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + xra xrb ~ Px * ~ * ~ xrb Py * ~ xra ~ * + Rxy xrb Poy * xra * + xrb ~ Pox * xra * + + Ry xra Poy * xra * + Pox xrb ~ * xrb ~ * + xrb Px * xrb * xra ~ xra ~ Py * * + + / * xra ~ Px * xrb * xrb ~ Py * xra ~ * + Rxy xrb Poy * xra * + xra Pox * xrb ~ * + + xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xrb * + ~ + Px *,xra ~ Px * xra ~ Px xrb ~ ~ * * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + Px xrb ~ ~ xrb * * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + Rxy xrb Poy * xra * + xra Pox * xrb ~ * + xrb ~ Py * xra ~ * + xra ~ Px * xrb * + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + Rxy xrb Poy * xra * + + xrb ~ ~ Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xrb ~ * xrb ~ ~ Px * xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ Px xrb ~ ~ * * ~ xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb Px * * - xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + xra ~ xra ~ * Px * xra Pox * xra * xrb ~ Py * xrb ~ * + + xrb Poy * xrb * + Rx + Px xra ~ xrb * * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + xrb Px * xrb ~ ~ * xra ~ Py * xra ~ * + + / xrb ~ ~ Px * xrb ~ ~ * xrb ~ Pox * xrb ~ * xra ~ Py * xra ~ * + + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + Rxy xrb Poy * xra * + xra Pox * xrb ~ * + + Ry xra Poy * xra * + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + + / * - * / * - xra ~ * + ~ Py *,0,0,xra xra ~ Px * xra ~ Px xrb * * xrb Py xra * * + xra Pox * xrb ~ * + xra xrb Poy * * + Rxy + xrb Px * xrb * xra ~ xra ~ * Py * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / Px xrb * * - * Px xra ~ * xra ~ * Py xrb ~ * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xrb Px * xra ~ * Poy xrb * xra * xra Pox * xrb ~ * + xrb Py * ~ xra ~ * + + Rxy + xrb Px * xrb * xra ~ Py * xra ~ * + Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + + / xrb Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xrb Poy * xra * xra Pox * xrb ~ * + xrb Py * ~ xra ~ * + xra ~ Px * xrb * + Rxy + xrb Px * xrb * xra ~ Py * xra ~ * + Pox xrb ~ * xrb ~ * + xra xra * Poy * + Ry + / * - / xrb ~ ~ Px * Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + xra ~ Py xra ~ * * + xrb xrb Px * * + / Rxy xra Poy * xrb * + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + + xra Poy * xra * xrb ~ Pox * xrb ~ * + xra ~ Py * xra ~ * + xrb Px * xrb * + Ry + / xra ~ Px * xrb Px * xrb Px * xrb * xra ~ Py * xra ~ * + Ry xra Poy * xra * + xrb xrb ~ Pox * * - + / Py xrb ~ * xra ~ * xrb ~ xra ~ Px * * - xrb xra Pox * * ~ + xrb Poy * xra * + Rxy + * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xrb Px * xrb ~ ~ * xra ~ xra ~ * Py * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + xrb Poy * xra * xra Pox * xrb ~ * + Py xrb ~ xra ~ * * + xra ~ Px * xrb * + Rxy + xra ~ Py * xra ~ * xrb Px * xrb * + Pox xrb * ~ xrb ~ * + xra Poy * xra * + Ry + / * xrb Px * xrb * xra Py xra ~ * * - xra Poy * xra * xrb ~ Pox * xrb ~ * + + Ry + / xra ~ Px * xrb * xra ~ Py xrb ~ * * + Pox xra xrb ~ * * + xrb Poy * xra * + Rxy + * - / * - xrb ~ * + ~ Pox *,xra ~ Px * xra ~ Px * xrb * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb Px * xrb * xrb ~ Pox * xrb ~ * xra ~ Py * xra ~ * + + xra Poy * xra * + Ry + / xrb Px * * - xrb xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + Rxy Poy xra * xrb * + + Poy xrb xra * * xrb ~ Py * xra ~ * xra Pox * xrb ~ * + + xra ~ Px * xrb ~ ~ * + Rxy + xra Poy * xra * xrb xrb ~ Pox * * ~ + xra ~ Py * xra ~ * + xrb Px * xrb * + Ry + / * - / * xrb Px Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + xrb Px * xrb ~ ~ * xra ~ Py * xra ~ * + + / * xra Px xrb * * ~ xra ~ xrb ~ Py * * + Pox xra * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb * xra Py xra ~ * * ~ + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xra ~ Px * xrb * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + Ry xra Poy * xra * + + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb * xrb ~ Py * xra ~ * + Rxy xrb Poy * xra * + xra Pox * xrb ~ * + + xrb Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy xra * * + Ry + * xra ~ Px * xrb * xrb xra ~ Py * * ~ + Pox xrb ~ * xra * + xrb Poy * xra * + Rxy + xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ xrb ~ Pox * * + xra Poy * xra * + Ry + / * - / * - xra * + ~ Poy *;xra xrb xra ~ / xra Py * ~ xrb Px * xrb * xra ~ Py * xra ~ * + Ry Poy xra * xra * + xrb ~ Pox * xrb ~ * + + / xrb ~ Py * xrb Poy * xra * xra Pox * xrb ~ * + xra ~ Px * xrb * xrb ~ Py * xra ~ * + + Rxy + Ry xra Poy * xra * + xrb Px xrb * * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + + / xra ~ Py * * - Px xrb xra ~ * * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + Px xra ~ xra ~ * * xrb ~ Py * xrb ~ * + Rx Poy xrb * xrb * + xra Pox * xra * + + Rxy xra Poy xrb * * + xrb ~ xra * Pox * + xrb ~ Py * xra ~ * xra ~ Px * xrb * + + Rxy xrb Poy * xra * + xra xrb ~ Pox * * + xrb ~ Py * xra ~ * Px xrb xra ~ * * + + xrb Px * xrb ~ ~ * xra ~ xra ~ Py * * + xrb ~ Pox * xrb ~ * + Ry xra Poy * xra * + + / * - Ry xrb Px * xrb * xra Poy * xra * xrb ~ xrb ~ * Pox * + Py xra ~ * xra ~ * + + + * / * - * xrb ~ Py * xra ~ Py * xrb ~ Py * xra ~ * Rxy xrb Poy * xra * + xra Pox * xrb ~ * + xra xrb Px * * - + Ry xra Poy xra * * xrb ~ Pox * xrb ~ * + xrb Px * xrb * Py xra ~ xra ~ * * + + + / * - xra Pox * xra * Rx xrb Poy * xrb * + xrb xrb ~ Py * * - + xra ~ Px * xra ~ * + Rxy xrb Poy * xra * + xrb xra ~ Px * * xrb ~ Py * xra ~ * + Pox xra xrb ~ * * + + Px xrb * xrb * Py xra ~ * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb Px * xrb * Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + + xra Py xra * * + * xra ~ Px * xrb * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xra xrb * Poy * + Rxy + xra ~ Py * xra ~ * xrb xrb ~ * Px * - xrb ~ Pox * xrb ~ * + xra xra * Poy * + Ry + / * - / + * ~ ~ Px *,1 xrb ~ Py * xrb Poy xra * * xra ~ Px * xrb * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + + Rxy + xrb ~ ~ Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra xra Poy * * + Ry + / xra Py * ~ * - xra Pox * xra * xrb ~ Py * xrb ~ * + xra Px xra ~ * * - xrb Poy * xrb * + Rx + xra ~ Px * xrb * Py xrb ~ * xra ~ * + xra Pox * xrb ~ * + Poy xrb * xra * + Rxy + xrb Px * xrb * xra Poy * xra * xrb ~ Pox * xrb ~ * + xra Py * ~ xra ~ * + + Ry + / Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + xrb Px * xrb * xra ~ Py * xra ~ * + + * xra ~ Px * xrb * Py xrb ~ xra ~ * * + xrb Poy * xra * xra Pox * xrb ~ * + + Rxy + xrb ~ Px * ~ xrb * xra ~ Py * xra ~ * + Pox xrb ~ * xrb ~ * + xra Poy * xra * + Ry + / * - / xrb ~ * xra ~ Py * xrb Px * xrb * xrb ~ Pox * xrb ~ * xra ~ Py * xra ~ * + + xra Poy * xra * + Ry + / xrb ~ Py * xra Pox * xrb ~ * xrb ~ Py * xra ~ * + Rxy xra xrb * Poy * + xra ~ Px * xrb * + + xrb Px * xrb * xra ~ xra ~ * Py * + xrb ~ Pox xrb ~ * * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * xra ~ Px * xra ~ * Py xrb ~ * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + Rxy xrb Poy * xra * + xra Pox * xrb ~ * + xra ~ Px xrb * * xrb ~ Py * xra ~ * + + Pox xrb ~ * xrb ~ * xrb Px * xrb * xra ~ Py * xra ~ * + + Ry xra Poy * xra * + + * xrb Px * xrb * xra Py xra ~ * * - xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb * xrb ~ Py * xra ~ * + Rxy xrb Poy * xra * + xra Pox * xrb ~ * + + xra ~ Py * xra ~ * xrb Px * xrb * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / - xra ~ * + - Py *,0,0,xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ xra ~ * Py * + xrb ~ xra Pox * * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb * xra ~ xra ~ Py * * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xra Poy * xra * xrb Pox * ~ xrb ~ * + xrb Px xrb * * xra ~ Py * xra ~ * + + Ry + / xrb Px * xrb * xra Py * ~ xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb * Py xra ~ xrb ~ * * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + Px xrb * xrb ~ ~ * xra Py xra ~ * * ~ + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xra * xra ~ Py * xrb ~ ~ Px * xrb ~ ~ * xrb ~ Pox * xrb ~ * xra ~ Py * xra ~ * + + xra Poy * xra * + Ry + / xrb ~ Py * xrb ~ ~ Px * xra ~ * Rxy xrb Poy * xra * + xra Pox * xrb ~ * + Py xra ~ * xrb ~ * + + Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + xrb Px * xrb * xra ~ Py * xra ~ * + + / xra ~ Py * * - xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + * xra ~ Py * xra ~ * xrb ~ xrb Px * * - xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra xra * Pox * + xrb Poy * xrb * + Rx + Rxy xrb Poy * xra * + xra Pox * xrb ~ * + xrb ~ Py * xra ~ * + xra ~ Px * xrb ~ ~ * + xra ~ Px * xrb * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + xra ~ Py * xra ~ * + xrb Px * xrb * + / * - / - xrb ~ * + ~ Pox *,xrb xrb ~ Py * Py xra Py * ~ xra ~ * xrb ~ xrb Px * * - xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb xra * Poy * xra Pox xrb ~ * * + xra Px xrb * * - xrb ~ Py * xra ~ * + Rxy + * xra ~ * - * xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + Poy xrb xrb * * + Rx + xrb Px xra ~ * * Py xrb ~ xra ~ * * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb Px * xrb * Py xra ~ * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb Px * xrb * Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + xra Py * ~ xra ~ * + + * Rxy xrb ~ Py * xra ~ * xra ~ Px * xrb * + xra Pox * xrb ~ * + xrb Poy * xra * + + Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + xrb Px * xrb * xra ~ Py * xra ~ * + + / * - / xra xra Py xra Poy * xra * Pox xrb ~ * xrb ~ * + xrb ~ Px xrb * * - xra xra ~ Py * * - Ry + / * ~ xrb ~ Py * xrb Poy * xra * Pox xra * xrb ~ * + xra ~ Px * xrb * xrb Py * ~ xra ~ * + + Rxy + xra ~ Py * xra ~ * xrb ~ xrb Px * * - Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + + / xra ~ Py * * - xra ~ Px * xrb * xrb ~ Py * xra ~ * + xrb Poy * xra * xra Pox * xrb ~ * + + Rxy + xrb Px * xrb * xrb ~ Pox * xrb ~ * xra ~ Py * xra ~ * + + xra Poy * xra * + Ry + / * xra ~ Px * xra ~ * Rx xrb Poy * xrb * + xra Pox * xra * + xrb ~ Py * xrb ~ * + + xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + Px xrb xra ~ * * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ Pox * xrb ~ * xra ~ Py * xra ~ * + xrb Px * xrb * + xra Poy * xra * + Ry + / xrb Px * xra ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xra Poy xrb * * + Rxy + Ry xra Poy * xra * + Pox xrb ~ * xrb ~ * + xrb Px * xrb * xra ~ Py * xra ~ * + + / * * - / - * + ~ Poy *;0,0,Pra,0,0,0;0,0,0,Prb,0,0;xra Pox * xra ~ Px * xrb * xrb ~ xra ~ Py * * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / Pox xrb ~ * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb Px * xrb * xra ~ Py * xra ~ * + Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * xrb ~ ~ Px * xrb * xra ~ Py * xra ~ * + Pox xrb ~ xrb ~ * * + xra xra Poy * * + Ry + * - / xra ~ * xrb ~ Pox * xra ~ Py * xra ~ * xrb ~ ~ Px * xrb ~ ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / Rxy xrb Poy * xra * + xra ~ Px * xrb * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + Poy xra * xra * + Ry + / xra Pox * xra Pox * xrb ~ * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb * xra ~ xra ~ Py * * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + Rxy xrb Poy * xra * + xra Pox * xrb ~ * + xrb ~ Py * xra ~ * + xra ~ Px * xrb ~ ~ * + Px xra xrb ~ ~ * ~ * xrb ~ Py * xra ~ * + Rxy xra xrb * Poy * + xra Pox * xrb ~ * + + xrb ~ Px xrb ~ ~ * * ~ xra ~ Py * xra ~ * + xrb ~ xrb ~ Pox * * + xra Poy * xra * + Ry + / * - / * - xrb ~ ~ * + ~ Px *,Py xrb ~ * xra ~ * Rxy xrb Poy * xra * + xra Pox * xrb ~ * + xrb ~ Px xra ~ * * - + xrb Px * xrb * xra ~ Py xra ~ * * + xrb ~ Pox * xrb ~ * + Poy xra * xra * + Ry + / xrb ~ Pox * * xra Pox * - ~ Rx xrb Poy * xrb * + xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + Pox xra xra * * + + xra ~ Px * xrb * xrb ~ Py * xra ~ * + xrb Poy * xra * Rxy Pox xrb * xra * - + + xrb Poy * xra * xra Pox * xrb ~ * + xra Px xrb * * - xra ~ Py xrb ~ * * + Rxy + xrb Px xrb * * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xrb ~ * xrb ~ Pox * xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + Poy xrb * xra * + Rxy + xrb Px xrb * * xra ~ Py * xra ~ * + xrb ~ xrb ~ * Pox * + xra Poy * xra * + Ry + / xra Pox * xra Px * ~ xrb * Py xrb ~ * xra ~ * + Rxy xrb Poy * xra * + xra Pox * xrb ~ * + + xra Poy xra * * xrb ~ Pox * xrb ~ * xrb ~ xrb Px * * - + xra ~ Py * xra ~ * + Ry + / xrb ~ Pox * * - xra Px * ~ xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + Px xra ~ * xrb * xrb ~ Py * xra ~ * + Rxy xrb Poy * xra * + xra Pox * xrb ~ * + + Rxy xra Pox * xrb ~ * xrb xra ~ Px * * xrb ~ Py * xra ~ * + + xrb Poy * xra * + + Ry xra Poy * xra * + Pox xrb ~ * xrb ~ * + xrb xrb * Px * xra Py xra ~ * * - + / * - / * - xra ~ * + ~ Py *,0,0,1 xra Pox * Px xra ~ xrb * * xrb ~ Py * xra ~ * + xra xrb ~ * Pox * + xrb Poy * xra * + Rxy + xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb Pox * ~ * - xrb xrb * Poy * xra Pox * xra * + xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + + Rx + xra Poy * xra * xrb ~ Pox * xrb ~ * + xra ~ Py * xra ~ * + xrb Px * xrb * + Ry + xra ~ Px * xrb * xra ~ xrb ~ * Py * + xra xrb ~ Pox * * + xrb Poy * xra * + Rxy + Ry xrb Px * xrb ~ ~ * xra ~ xra ~ Py * * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + + / * Rxy xrb Poy * xra * + Pox xra * xrb ~ * + Px xra ~ * xrb * xrb ~ Py * xra ~ * + + xrb ~ ~ Px * xrb * xra ~ Py * xra ~ * + xra Poy * xra * xrb ~ Pox * xrb ~ * + + Ry + / * - / xra * Pox xrb ~ * Ry xra Poy * xra * + xrb Px * xrb * Py xra ~ * xra ~ * + xrb ~ xrb ~ * Pox * + + / xra Pox * xra ~ Px * xrb * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb xrb Px * * xra ~ Py xra ~ * * + xra Poy * xra * Ry xrb Pox xrb ~ * * - + + / xrb ~ Pox * * - xra ~ Px * xrb * xrb ~ Py * xra ~ * + xra xrb ~ Pox * * + Poy xrb * xra * + Rxy + xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * Rxy xrb Poy * xra * + xra Pox * xrb ~ * + xra ~ Px * xrb * xrb ~ Py xra ~ * * + + xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / Ry xra xra * Poy * + xrb ~ Pox * xrb ~ * + xrb Px * xrb * xra ~ Py * xra ~ * + + * Rxy xra xrb Poy * * + xra Pox * xrb ~ * + xra ~ Px * xrb * xra ~ Py * xrb ~ * + + Ry xrb Px * xrb * Py xra ~ xra ~ * * + xrb ~ Pox * xrb ~ * + xra xra * Poy * + + / * ~ xra ~ Px * xra ~ * Py xrb ~ * xrb ~ * + xrb Poy xrb * * xra Pox * xra * + + Rx + + / - xrb ~ * + - Pox *,xra Pox * xra ~ Px * xrb * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb * xrb Poy * xra * xra Pox * xrb ~ * + xrb ~ Py * xra ~ * + + Rxy + xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + Ry xra Poy * xra * + + / Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + xrb ~ xrb Px * * ~ Py xra ~ * xra ~ * + + * xrb Poy * xra * Pox xra * xrb ~ * + xrb ~ Py * xra ~ * + xra Px xrb * * - Rxy + xrb Px * xrb * xra ~ Py * xra ~ * + Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + + / * - / xrb * xrb ~ Pox * xrb ~ ~ Px * xrb * xra ~ Py * xra ~ * + xrb Pox * ~ xrb ~ * + Ry Poy xra xra * * + + / xrb Poy * xra * xra Pox * xrb ~ * + xrb ~ Py * xra ~ * + xra Px xrb * * ~ + Rxy + xra Poy * xra * xrb ~ Pox * xrb ~ * + xra ~ Py * xra ~ * + xrb xrb ~ ~ Px * * + Ry + / xra Pox * Px xra ~ xrb ~ ~ * * xrb Poy xra * * Pox xra xrb ~ * * + xrb Py xra ~ * * ~ + + Rxy + xrb Px * xrb * xra ~ Py * xra ~ * + Ry xra Poy * xra * + xrb Pox xrb ~ * * ~ + + / xrb ~ Pox * * - Px xra ~ xra ~ * * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy xrb * * + Rx + xrb Px xra ~ * * xrb ~ Py * xra ~ * + Rxy xrb Poy * xra * + xra Pox * xrb ~ * + + Ry xra Poy * xra * + xrb Px * xrb * xra ~ Py * xra ~ * + Pox xrb ~ * xrb ~ * + + / xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * Px xra ~ * xrb * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb Px * xrb * xra xra ~ Py * * - Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + + / * - / * - xra * + ~ Poy *;xrb Poy * xra ~ Px * xrb * xrb ~ Py * xra ~ * + Rxy xrb Poy * xra * + xra Pox * xrb ~ * + + Poy xra * * xrb xrb Px * * Ry xra Poy * xra * + Pox xrb ~ * xrb ~ * + xra ~ Py * xra ~ * + + / - Px xra ~ * xra ~ * Rx xrb Poy * xrb * + xra Pox * xra * + xrb ~ Py * xrb ~ * + + xra ~ Px * xrb * Rxy Poy xra xrb * * + Pox xra xrb ~ * * + Py xrb ~ * xra ~ * + + xrb xrb Px * * xra ~ Py * xra ~ * + xrb ~ xrb * Pox * ~ + xra Poy * xra * + Ry + / Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + xrb ~ Px * ~ xrb * Py xra ~ * xra ~ * + + * xra ~ Px * xrb * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + Poy xrb xra * * + Rxy + xrb xrb Px * * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xra ~ * xra Poy * xrb Px * xrb * xra ~ Py * xra ~ * + xrb xrb ~ Pox * * ~ + xra Poy * xra * + Ry + / xra Pox xrb ~ * * Py xrb ~ xra ~ * * + xra ~ Px * xrb * + xrb Poy * xra * + Rxy + Px xrb ~ xrb * ~ * Py xra ~ xra ~ * * + xra Poy * xra * xrb Pox * ~ xrb ~ * + + Ry + / xrb Poy * xra ~ Px * xrb * xrb ~ Py * xra ~ * + Rxy xra Poy * xrb * + xra Pox * xrb ~ * + + Poy xra * Ry xrb Px * xrb * xra xra * Poy * xrb ~ Pox * xrb ~ * + xra Py * ~ xra ~ * + + + / * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ xrb * Px * xra Pox * xrb ~ * Py xrb ~ * xra ~ * + + xrb Poy * xra * + Rxy + Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + xrb Px * xrb * xra xra ~ Py * * - + / xra Poy * xra * xrb Px * xrb * xra ~ Py * xra ~ * + xrb Pox * ~ xrb ~ * + + Ry + * xra ~ Px * xrb * xrb ~ Py * xra ~ * + xra xrb ~ Pox * * + xra xrb Poy * * + Rxy + Px xrb xrb * * xra ~ xra ~ Py * * + Pox xrb ~ * xrb ~ * + xra Poy xra * * + Ry + / * - / * - xrb * + ~ Px *,xra Poy * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ * xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + Rxy xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ Px * ~ xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xrb ~ * + xrb Poy * xra ~ Px * xrb ~ ~ * xra ~ xrb ~ * Py * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + Pox xrb ~ xrb ~ * * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xrb * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xrb Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + Rxy xrb Poy * xra * + xra Pox * xrb ~ * + xrb ~ Py * xra ~ * + xra ~ Px * xrb ~ ~ * + xrb Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb Px * xrb * Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + xra ~ Py * xra ~ * + + / * - / xra ~ * - ~ Py *,0,0,xrb Poy * xra ~ xrb * Px * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb xrb * Px * Py xra ~ * xra ~ * + Pox xrb ~ * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + Pox xra * xra * + xrb Poy * xrb * + Rx + xrb Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + xrb ~ Pox * xrb ~ * Px xrb ~ ~ * xrb * xra ~ Py * xra ~ * + + xra Poy * xra * + Ry + Px xrb xrb * * Py xra ~ * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * / xra ~ Px * xrb * xrb ~ xra ~ * Py * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb Px * xra ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + * * - / * xra Poy * xra ~ Py * xra ~ * xrb Px * xrb * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb Poy * xra * xra ~ Px * xrb * xrb ~ Py xra ~ * * + xra Pox * xrb ~ * + + Rxy + xrb xrb ~ ~ Px * xra ~ Py * xra ~ * xrb / + * xra Poy * xra * xrb ~ Pox * xrb ~ * + + Ry + / Rxy xrb Poy * xra * + Pox xra * xrb ~ * + xra ~ Px * xrb * xrb ~ Py * xra ~ * + + Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + xrb ~ ~ Px * xrb * Py xra ~ xra ~ * * + + / xra Poy * * xrb Poy * - ~ xra ~ Px * xra ~ * xra Pox * xra * xrb ~ Py * xrb ~ * + + xrb Poy * xrb * + Rx + xra ~ Px * xrb * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xra Poy * xra * xrb ~ Pox * xrb ~ * + xrb Px * xrb * xra ~ Py xra ~ * * + + Ry + / xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + Ry xra Poy * xra * + + * xra ~ Px * xrb ~ ~ * Py xrb ~ * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb xrb * Px * xra ~ Py * xra ~ * + Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + + / * - / * - xrb ~ * + ~ Pox *,1 xrb Poy * xra Poy * xra Poy * xra * xrb ~ Pox * xrb ~ * + xra ~ Py * xra ~ * xrb ~ Px xrb * * - + Ry + / xra ~ Px * xrb * Rxy xrb Poy * xra * + xra Pox * xrb ~ * + xrb ~ Py * xra ~ * + + * - xrb * Rx xrb Poy * xrb * + xra Pox * xra * + xrb ~ Py * xrb ~ * + xra ~ Px * xra ~ * + xrb Px * xrb * Ry Poy xra xra * * + xrb ~ Pox * xrb ~ * + Py xra ~ * xra ~ * + + xrb Poy * xra * xra Pox * xrb ~ * + xrb ~ Py * xra ~ * + Px xrb xra ~ * * + Rxy + xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * Rxy xrb ~ Py * xra ~ * xra ~ Px * xrb * + xra Pox * xrb ~ * + Poy xra xrb * * + + xrb ~ ~ Px * xrb * xrb ~ Pox * xrb ~ * Py xra ~ * xra ~ * + + xra Poy * xra * + Ry + / * - / xra Poy * xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Py * xra ~ * xrb Poy * xra * Pox xra xrb ~ * * + xrb ~ xra ~ Px * * - + Rxy + xrb Px * xrb * xra ~ Py * xra ~ * + xrb ~ Pox xrb ~ * * + Poy xra * xra * + Ry + / xrb Poy * Rxy xra Poy * xrb * + xra ~ Px * xrb * xrb ~ xra ~ Py * * + Pox xrb ~ xra * * + + Ry xra Poy * xra * + xrb ~ Pox * xrb ~ * + xrb Px * xrb * xra ~ Py * xra ~ * + + / xra Poy * * - xrb Poy * xrb * xra Pox * xra * xrb ~ Py * xrb ~ * + xra ~ Px * xra ~ * + + Rx + Px xra ~ * xrb * Rxy xrb Poy * xra * + Pox xra * xrb ~ * + xrb ~ Py * xra ~ * + + xra ~ Px * xrb * Py xrb ~ * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb Px * xrb * Py xra ~ * xra ~ * + xrb ~ xrb ~ * Pox * + xra Poy * xra * + Ry + / * - / * - xra * + - Poy *) # opt
	 * 
	 * 
	 * # helpers
	 * p = (px; py)
	 * rx = (xra, xrb ~; xrb, xra) p * (xx; xy) +
	 * rix = (xra, xrb; xrb ~, xra) p (xx; xy) - *
	 * 
	 */
	
	protected static final Logger logger = Logger.getLogger(BuildMap.class.getName());

    private boolean stopped;

	private RobotControl rc;
	private Motor mleft;
	private Motor mright;
	private Motor head;
	private boolean mlinv, mrinv;
	private UltrasonicSensor uss;
	private double wheelDistance;
	private double C;

	private enum Mode {DRIVE_TO_WALL, TURN_30_RIGHT, FOLLOW_WALL, PAUSE, TURN_TO_POINT, DRIVE_TO_POINT}
	private Mode mode;
	private double[] driveToPoint;
	
	public void run() {
		rc = RobotControl.getInstance();
		RobotHardware rh = rc.getRobotHardware();
		setSpeed(0);
		mleft = rh.getMotorLeft();
		mright = rh.getMotorRight();
		mlinv = rh.isLeftMotorInverted();
		mrinv = rh.isRightMotorInverted();
		head = rh.getMotorExtra();
		uss = rh.getUltrasonicSensor(1);
		wheelDistance = rh.getWheelDistance();
		C = C(rh.getWheelDiameter());
		mode = Mode.DRIVE_TO_WALL;
		driveToPoint = null;
		position = new LinkedList<double[]>();
		
		logger.info("Strategy "+getName());
		logger.info("need to register myself to the Command Provider");
		Services.getInstance().getCommandProvider().addCommandListener(this);
		fireStrategyStarted();
		
		start();
	}

	public String getDescription() {
		return "a robot with a pivoting ultrasonic sensor, which stores each measured point coordinate in a map."
				+ "It opens a map window which shows the path of the robot and the map data.";
	}

	public String getName() {
		return "Build A Map";
	}

	public static final int HEAD_LEFT_APEX_ANGLE = 90;
	public static final int HEAD_RIGHT_APEX_ANGLE = 45;
	
	/**
	 * At which angle should be switched between in front and left
	 */
	public static final int HEAD_LEFT_SWITCH_ANGLE = 45;
	/**
	 * Distance from the center between the wheels to the center of the
	 * ultrasonic sensor in driving direction.
	 */
	public static final double HEAD_CENTER_DISTANCE = 10; //TODO not exact
	public static final int HEAD_SPEED = 80; // degrees per second
	
	
	/**
	 * @param  oldMean
	 * @param  n >= 0
	 * @return
	 */
	private static double getPseudoMean(int n, double oldMean, double newValue) {
		return oldMean*(n/(n+1.0))+newValue/(n+1.0);
	}
	
	private final void calculateMovement(double[] buffer, int speedLeft, int speedRight, int time) {
		double t = time / 1000.0 * C;
		double distLeft = t * speedLeft;
		if (speedLeft == speedRight) { // straight driving
			buffer[0] = distLeft;
			buffer[1] = 0;
			buffer[2] = 1;
			buffer[3] = 0;
		} else {
			double distRight = t * speedRight;
			// calculate the angle which is driven on the circle on which the
			// robot is driving
			double angle = (distRight - distLeft) / wheelDistance;
			// calculate about which angle the robot is rotated
			buffer[2] = Math.cos(angle);
			buffer[3] = Math.sin(angle);
			double radius = distLeft / angle;//WHEEL_DISTANCE * distLeft / (distRight - distLeft);
			buffer[0] = (radius + wheelDistance / 2) * (Math.sin(angle));
			buffer[1] = (radius + wheelDistance / 2) * (1-Math.cos(angle));
		}
	}
	
	private static double[] polar2cart(double[] buffer, double distance, double angle, double[] x) {
		return z(buffer,
				distance*Math.cos((angle - getAngle(x))*PI/180)+HEAD_CENTER_DISTANCE,
				distance*Math.sin((angle - getAngle(x))*PI/180));
	}
	
	private final int getDistance() {
		int distance = uss.getDistance();
		/*
		do
			distance = uss.getDistance();
		while (distance <= 0 && distance >= 255);
		*/
		return distance;
	}
	private Map map;
	
	public void start() {
		double defaultSpeed = DEFAULT_SPEED / C;
		MAP_ZOOM = 3;
		
		map = new Map();
		openRobotMap(map);
		
		head.resetTachoCount();
		boolean clockwise = true;
		
		head.setSpeed(HEAD_SPEED);
		
		stopped = false;

		double[] x = x(null, getDistance(), 0, 1, 0, 0, 0);
		
		
		double[][] P = P(null, 1, 1, 0, 0, 0, 0);
		double[] u = u(null, 1, 0, 0, 0);
		double[] xp = null;
		double[][] Pp = null;
		double[][] Q = Q(null, 0.1, 0, 0.1, 0, 0, 0, 0, 0, 0);
		double[] z = new double[] {0, 0};
		double[] y = null;
		double[][] S = null;
		double[][] K = null;
		double[][] R = R(null, MEASURING_ERROR*MEASURING_ERROR, 0, MEASURING_ERROR*MEASURING_ERROR);
		int pl = 0, pr = 0;
		long lastTime = System.currentTimeMillis();

		head.rotateTo(-HEAD_RIGHT_APEX_ANGLE, true); //	start clockwise rotation

		double startangle = 0;
		double followWallLeftStartAngel = 0;
		
		for (int j = 0; ! stopped; j++) {
			
			int tacho = head.getTachoCount(); // ~ 20-60 ms //TODO test: maybe better after getDistance() ?
			int distance = getDistance();

			long now = System.currentTimeMillis();
			int ms = (int) (now - lastTime);
			lastTime = now;
			
			if (distance < 255 && distance > 0) {
				double angle = getAngle(x, tacho); // absolute angle of the tacho
				
	
				/*
				calculateMovement(u, pl, pr, (int) (System.currentTimeMillis() - lastTime));
				xp = x_p_opt(xp, u, x);
				z = polar2cart(z, distance, getAngle(xp, tacho), xp);
				
				double[] p2 = rx(null, z, xp);
				map.addPoint(p2);
				
				if (distance < minRoundDistance) {
					minRoundDistance = distance;
					minRoundAngle = getAngle(x, tacho);
					if (distance < minDistance) {
						minDistance = minRoundDistance;
						minAngle = minRoundAngle;
					}
				}*/
				
				z = polar2cart(z, distance, angle, x);
				double[] p2 = rx(null, z, x);
				map.addPoint(p2, x);
			}

			calculateMovement(u, pl, pr, ms);
			
			xp = x_p_opt(xp, u, x);
			//Pp = P_p_opt(Pp, P, Q, u);
			/*
			y = y_opt(y, xp, z);			
			S = S_opt(S, Pp, R, xp);
			K = K_opt(K, Pp, R, xp);
			x = x_u_opt(x, Pp, R, xp, z);
			P = P_u_opt(P, Pp, R, xp);
			*/
			x = xp;
			//P = Pp;
			adjust(x);
			
			this.x = x;
			notifyStateChange(x);
			switch (mode) {
			case DRIVE_TO_WALL:
				if (map.getMinimalFrontDistance(x) >= MIN_DISTANCE) {
					pl = (int) Math.round(defaultSpeed);
					pr = pl;
					break;
				} else {
					mode = Mode.TURN_30_RIGHT;
					startangle = getAngle(x);
				}
			case TURN_30_RIGHT:
				if (Math.abs(modAngle(startangle - getAngle(x))) < 30) { // keep turning
					pl = (int) Math.round(defaultSpeed/2);
					pr = -pl;
					break;
				} else {
					mode = Mode.FOLLOW_WALL;
					followWallLeftStartAngel = getAngle(x);
				}
			case FOLLOW_WALL:
				if (map.getMinimalFrontDistance(x) >= MIN_DISTANCE) {
					if (driveToPoint != null) {
						double[] b = rix(null, driveToPoint, x);
						double angl = modAngle(Math.atan2(b[1], b[0])*180/PI);
						if (angl <= 0) {
							mode = Mode.TURN_TO_POINT;
							pl = 0;
							pr = 0;
							break;
						}
					}
					double min = map.getMinimalLeftDistance(x);
					if (min > 50)
						min = 50;
					pl = (int) (defaultSpeed - (min - MIN_DISTANCE)*2);
					pr = (int) (defaultSpeed + (min - MIN_DISTANCE)*2);
					if (pl < pr) {// drive left
						if (Math.abs(modAngle(followWallLeftStartAngel-getAngle(x))) > 150)
							mode = Mode.DRIVE_TO_WALL;
					} else {
						followWallLeftStartAngel = getAngle(x);
					}
				} else {
					mode = Mode.TURN_30_RIGHT;
					startangle = getAngle(x);
					pl = (int) Math.round(defaultSpeed/2);
					pr = -pl;
					followWallLeftStartAngel = getAngle(x);
				}
				break;
			case TURN_TO_POINT:
				double[] b = rix(null, driveToPoint, x);
				double dist = Math.sqrt(b[0]*b[0]+b[1]*b[1]);
				if (dist < 1*1) { // under 1 cm distance => done
					mode = Mode.PAUSE;
					driveToPoint = null;
					pl = 0;
					pr = 0;
					break;
				} else {
					double angl = modAngle(Math.atan2(b[1], b[0])*180/PI);
					if (Math.abs(angl) < 5) {// lower than 10 degrees
						mode = Mode.DRIVE_TO_POINT;
					} else {
						if (angl > 0)
							pl = -(int) Math.round(defaultSpeed/2);
						else
							pl = (int) Math.round(defaultSpeed/2);
						pr = -pl;
						break;
					}
				}
			case DRIVE_TO_POINT:
				if (driveToPoint == null) {
					mode = Mode.PAUSE;
					pl = 0;
					pr = 0;
					break;
				}
				b = rix(null, driveToPoint, x);
				dist = Math.sqrt(b[0]*b[0]+b[1]*b[1]);
				if (dist < 1*1 || b[0] <= 0) { // under 1 cm distance or drove to far => done
					mode = Mode.PAUSE;
					driveToPoint = null;
				} else {
					if (map.getMinimalFrontDistance(x) < MIN_DISTANCE) {
						mode = Mode.TURN_30_RIGHT;
					} else {
						double am = modAngle(b[1]*180/PI) /4;
						pr = (int) ((defaultSpeed + am));
						pl = (int) ((defaultSpeed - am));
						break;
					}
				}
			case PAUSE:
				pl = 0;
				pr = 0;
				mleft.stop();
				mright.stop();
				while (head.isMoving())
					;
				//if (Math.abs(tacho) > 10)
					head.rotateTo(0, true);
				while (!stopped && mode == Mode.PAUSE)
					;
				break;
			}
			
			if (!stopped && !head.isMoving()) {
				if (clockwise) {
					clockwise = false;
					if (mode == Mode.TURN_30_RIGHT)
						head.rotateTo(0, true); // counterclockwise rotation
					else
						head.rotateTo(HEAD_LEFT_APEX_ANGLE, true); // counterclockwise rotation
				} else {
					clockwise = true;
					head.rotateTo(-HEAD_RIGHT_APEX_ANGLE, true); //	clockwise rotation
				}
			}

			
			mleft.setSpeed(Math.abs(pl));
			mright.setSpeed(Math.abs(pr));
			if (mlinv ? pl > 0 : pl < 0)
				mleft.backward();
			else
				mleft.forward();
			if (mrinv ? pr > 0 : pr < 0)
				mright.backward();
			else
				mright.forward();
			
		}

		// stop all motors
		mleft.stop();
		mright.stop();
		while(head.isMoving())
			; // wait till current rotation is finished
		if (mode != Mode.PAUSE)
			head.rotateTo(0); // center head and wait
		
		// check out
		Services.getInstance().getCommandProvider().removeCommandListener(this);
		fireStrategyStopped();
	}
	

    private static void print(double[][] A) {
    	for (int i = 0; i < A.length; i ++) {
    		System.out.print("|");
        	for (int j = 0; j < A[0].length; j ++) {
        		if (j > 0)
        			System.out.print(", ");
        		System.out.print(A[i][j]);
        	}
    		System.out.println("|");
    	}
    }
	
	private List<double[]> position;
	private double[] x;
	
	private void notifyStateChange(double[] _x) {
		double x = get_x_x(_x);
		double y = get_x_y(_x);
		if (position.isEmpty() 
				|| Math.abs(position.get(position.size()-1)[0] - x) > 0.15 
				|| Math.abs(position.get(position.size()-1)[1] - y) > 0.15) {
			position.add(new double[] {x, y});
		}
		paint = true;
	}

	public void paint(Map map, GC gc) {
		Device device = gc.getDevice();
		
		Rectangle clip = gc.getClipping();
		sx = clip.width/2;
		sy = clip.height/2;
		double[] x1 = null, buf1 = null, buf2 = new double[2];
		
		gc.setBackground(device.getSystemColor(SWT.COLOR_BLACK));
		if (position.size() >= 1) {
			x1 = position.get(0);
			x1 = rix(buf1, x1, x);
			for (int i = 1; i < position.size(); i++) {
				double[] x2 = position.get(i);
				x2 = rix(buf2, x2, x);
				gc.drawLine(sx+toScreen(x1[0]), sy-toScreen(x1[1]), 
						sx+toScreen(x2[0]), sy-toScreen(x2[1]));
				x1 = x2;
			}

		}
		gc.setForeground(device.getSystemColor(SWT.COLOR_DARK_GRAY));
		boolean first = true;
		for (double[] p : map.getLinePoints()) {
			buf1 = rix(buf1, p, x);
			// switch buffers
			if (first)
				first = false;
			else {
				gc.drawLine(sx+toScreen(buf2[0]), sy-toScreen(buf2[1]),
						sx+toScreen(buf1[0]), sy-toScreen(buf1[1]));
				first = true;
			}
			x1 = buf2;
			buf2 = buf1;
			buf1 = x1;
		}
		
		for (Point p : map.getPoints()) {
			buf2[0] = p.getX();
			buf2[1] = p.getY();
			buf1 = rix(buf1, buf2, x);
			gc.setForeground(device.getSystemColor(SWT.COLOR_DARK_GRAY));
			gc.drawOval(sx+toScreen(buf1[0]), sy-toScreen(buf1[1]), 3, 3);
		}

		if (driveToPoint == null)
			gc.setForeground(device.getSystemColor(SWT.COLOR_BLUE));
		else
			gc.setForeground(device.getSystemColor(SWT.COLOR_DARK_RED));
			
		gc.drawLine(sx-2, sy-2, sx-2, sy+2);
		gc.drawLine(sx-2, sy-2, sx+4, sy);
		gc.drawLine(sx-2, sy+2, sx+4, sy);
		
	}

	private double MAP_ZOOM = 3;
	private static final double ZOOM_STEPS = 0.2;
	
	public int toScreen(double c) {
		return (int)(Math.round(c*MAP_ZOOM));
	}
	
	private Shell shell;
	
	private boolean paint;

	private int sx = 300, sy = 300;

	private void actionDriveTo(double x, double y) {
		driveToPoint = rx(driveToPoint, new double[] {x, y}, this.x);
		mode = Mode.TURN_TO_POINT;
	}

	private void actionModeSwitch() {
		if (mode == Mode.PAUSE) {
			if (driveToPoint == null) {
				System.out.println("exploring current room");
				mode = Mode.DRIVE_TO_WALL;
			} else {
				System.out.println("driving to specified point");
				mode = Mode.TURN_TO_POINT;
			}
		}
		else if (driveToPoint == null) {
			System.out.println("pause exploring");
			mode = Mode.PAUSE;
		} else {
			System.out.println("exploring current room");
			driveToPoint = null;
			mode = Mode.DRIVE_TO_WALL;
		}
	}

	private void actionClearCache() {
		position.clear();
		map.clear();
	}
	
	private void createShell(final Map map, Display display) {
		shell = new Shell(display);
		shell.open();
		shell.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent event) {
				paint(map, event.gc);
			}
		});
		// nokia n800 keycodes: up=16777217, right=16777220, down=16777218, left=16777219
		shell.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent event) {}
			
			public void keyReleased(KeyEvent event) {
				int code = event.keyCode;
				switch (code) {
				case 16777217: // up
					actionModeSwitch();
					shell.redraw();
					break;
				case 16777218: // down
					actionClearCache();
					shell.redraw();
					break;
				case 16777219: // left
					if (MAP_ZOOM > 1)
						MAP_ZOOM -= ZOOM_STEPS;
					shell.redraw();
					break;
				case 16777220: // right
					MAP_ZOOM += ZOOM_STEPS;
					shell.redraw();
					break;
				default:
					System.out.println("pressed key with code: "+code);
				}
			}
		});
		shell.addMouseListener(new MouseListener() {

			public void mouseDoubleClick(MouseEvent event) {
			}

			public void mouseDown(MouseEvent event) {
				actionDriveTo((event.x-sx)/MAP_ZOOM, (sy-event.y)/MAP_ZOOM);
			}

			public void mouseUp(MouseEvent event) {}
			
		});
		//deactivated due to compatibility issues (MouseWheelListener is gtk 3.4)
		/*shell.addMouseWheelListener(new MouseWheelListener() {

			public void mouseScrolled(MouseEvent event) {
				MAP_ZOOM += event.count > 0 ? ZOOM_STEPS : event.count < 0 ? -ZOOM_STEPS : 0;
			}
			
		});*/
		while (!shell.isDisposed()) {
			if (paint) {
				shell.redraw();
				paint = false;
			}
			if (!display.readAndDispatch())
				;
		}
	}
	
	
	private void openRobotMap(final Map map) {
		Shell shell = Services.getInstance().getShell();
		final Display display = shell == null ? new Display() : shell
				.getDisplay();
		if (shell != null)
			display.asyncExec(new Runnable() {
				public void run() {
					createShell(map, display);
				}
			});
	}

	private int i = 0;
	
	private void adjust(double[] x) {
		if (++i == 1) { // how often should x be normalized? now: every time
			double a = get_x_ra(x);
			double b = get_x_rb(x);
			double f = Math.sqrt(a*a+b*b);
			set_x_ra(x, a / f);
			set_x_rb(x, b / f);
			i = 0;
		}
	}

	private static double getAngle(double[] x) {
		return modAngle(Math.atan2(get_x_rb(x), get_x_ra(x))*180/PI);
	}
	
	/**
	 * Get the absolut angle of the tacho angle.
	 * 
	 * @param x
	 * @param tacho
	 * @return
	 */
	private static double getAngle(double[] x, int tacho) {
		double a = getAngle(x);
		a += tacho;
		return modAngle(a);
	}

	private static double modAngle(double a) {
		while (a > 180)
			a -= 360;
		while (a < -180)
			a += 360;
		return a;
	}
	
 	/* ultrasonic device modes (they are not public) */
	private static final byte MODE_OFF = 0x0;
	private static final byte MODE_SINGLE = 0x1;
	private static final byte MODE_CONTINUOUS = 0x2;
	private static final byte MODE_CAPTURE = 0x3;
	private static final byte MODE_RESET = 0x4;
	
	private int setSpeed(double p) {
		return rc.driveAhead((int) Math.round(p));
	}
	

	public void stop() {
		logger.info("stopping strategy");
		stopped = true;
		shell.setVisible(false);
	}

	public void commandReleased(Command command) {}

	public void commandSent(Command command) {

	}

	/*^ BEGIN GENERATED CODE */

    // PI = 3.1415926535897932384626433832795028841971693993751058209749445923078164
    private static final double PI = 3.141592653589793;

    // DISTANCE_REACT = 100
    private static final double DISTANCE_REACT = 100.0;

    // MIN_DISTANCE = 20
    private static final double MIN_DISTANCE = 20.0;

    // DEFAULT_SPEED = 5
    private static final double DEFAULT_SPEED = 5.0;

    // SPEED_MINIMUM = 20
    private static final double SPEED_MINIMUM = 20.0;

    // C = 3.1415926535897932384626433832795028841971693993751058209749445923078164 wd * 360 /
    private static double C(double wd) {
        return ((3.141592653589793*wd)/360.0);
    }

    // MEASURING_ERROR = 2
    private static final double MEASURING_ERROR = 2.0;

    // VAR_DIST = 1
    private static final double VAR_DIST = 1.0;

    // VAR_SPEED = 0.01
    private static final double VAR_SPEED = 0.01;

    // COVAR_SPEED_DIST = 0.0001
    private static final double COVAR_SPEED_DIST = 1.0E-4;

    // F = (1,0,ux,uy ~,0,0;0,1,uy,ux,0,0;0,0,ura,urb ~,0,0;0,0,urb,ura,0,0;0,0,0,0,1,0;0,0,0,0,0,1)
    private static double[][] F(double[][] buffer, double[] u) {
        if (buffer == null)
            buffer = new double[6][6];
        buffer[0][0] = 1.0;
        buffer[0][1] = 0.0;
        buffer[0][2] = u[0];
        buffer[0][3] = (-u[1]);
        buffer[0][4] = 0.0;
        buffer[0][5] = 0.0;
        buffer[1][0] = 0.0;
        buffer[1][1] = 1.0;
        buffer[1][2] = u[1];
        buffer[1][3] = u[0];
        buffer[1][4] = 0.0;
        buffer[1][5] = 0.0;
        buffer[2][0] = 0.0;
        buffer[2][1] = 0.0;
        buffer[2][2] = u[2];
        buffer[2][3] = (-u[3]);
        buffer[2][4] = 0.0;
        buffer[2][5] = 0.0;
        buffer[3][0] = 0.0;
        buffer[3][1] = 0.0;
        buffer[3][2] = u[3];
        buffer[3][3] = u[2];
        buffer[3][4] = 0.0;
        buffer[3][5] = 0.0;
        buffer[4][0] = 0.0;
        buffer[4][1] = 0.0;
        buffer[4][2] = 0.0;
        buffer[4][3] = 0.0;
        buffer[4][4] = 1.0;
        buffer[4][5] = 0.0;
        buffer[5][0] = 0.0;
        buffer[5][1] = 0.0;
        buffer[5][2] = 0.0;
        buffer[5][3] = 0.0;
        buffer[5][4] = 0.0;
        buffer[5][5] = 1.0;
        return buffer;
    }

    // x = (xx;xy;xra;xrb;xox;xoy)
    private static double[] x(double[] buffer, double xox, double xoy, 
            double xra, double xrb, double xx, double xy) {
        if (buffer == null)
            buffer = new double[6];
        buffer[0] = xx;
        buffer[1] = xy;
        buffer[2] = xra;
        buffer[3] = xrb;
        buffer[4] = xox;
        buffer[5] = xoy;
        return buffer;
    }

    // P = (Px,0,0,0,0,0;0,Py,0,0,0,0;0,0,Pra,0,0,0;0,0,0,Prb,0,0;0,0,0,0,Pox,0;0,0,0,0,0,Poy)
    private static double[][] P(double[][] buffer, double Pox, double Poy, 
            double Pra, double Prb, double Px, double Py) {
        if (buffer == null)
            buffer = new double[6][6];
        buffer[0][0] = Px;
        buffer[0][1] = 0.0;
        buffer[0][2] = 0.0;
        buffer[0][3] = 0.0;
        buffer[0][4] = 0.0;
        buffer[0][5] = 0.0;
        buffer[1][0] = 0.0;
        buffer[1][1] = Py;
        buffer[1][2] = 0.0;
        buffer[1][3] = 0.0;
        buffer[1][4] = 0.0;
        buffer[1][5] = 0.0;
        buffer[2][0] = 0.0;
        buffer[2][1] = 0.0;
        buffer[2][2] = Pra;
        buffer[2][3] = 0.0;
        buffer[2][4] = 0.0;
        buffer[2][5] = 0.0;
        buffer[3][0] = 0.0;
        buffer[3][1] = 0.0;
        buffer[3][2] = 0.0;
        buffer[3][3] = Prb;
        buffer[3][4] = 0.0;
        buffer[3][5] = 0.0;
        buffer[4][0] = 0.0;
        buffer[4][1] = 0.0;
        buffer[4][2] = 0.0;
        buffer[4][3] = 0.0;
        buffer[4][4] = Pox;
        buffer[4][5] = 0.0;
        buffer[5][0] = 0.0;
        buffer[5][1] = 0.0;
        buffer[5][2] = 0.0;
        buffer[5][3] = 0.0;
        buffer[5][4] = 0.0;
        buffer[5][5] = Poy;
        return buffer;
    }

    // u = (ux;uy;ura;urb)
    private static double[] u(double[] buffer, double ura, double urb, double ux
            , double uy) {
        if (buffer == null)
            buffer = new double[4];
        buffer[0] = ux;
        buffer[1] = uy;
        buffer[2] = ura;
        buffer[3] = urb;
        return buffer;
    }

    // B = (0,0,0,0;0,0,0,0;0,0,0,0;0,0,0,0;0,0,0,0;0,0,0,0)
    private static final double[][] B = {
        {0.0, 0.0, 0.0, 0.0},
        {0.0, 0.0, 0.0, 0.0},
        {0.0, 0.0, 0.0, 0.0},
        {0.0, 0.0, 0.0, 0.0},
        {0.0, 0.0, 0.0, 0.0},
        {0.0, 0.0, 0.0, 0.0}
    };

    // Q = (Qx,Qxy,0,0,0,0;Qxy,Qy,0,0,0,0;0,0,Qra,Qrab,0,0;0,0,Qrab,Qrb,0,0;0,0,0,0,Qox,Qoxy;0,0,0,0,Qoxy,Qoy)
    private static double[][] Q(double[][] buffer, double Qox, double Qoxy, 
            double Qoy, double Qra, double Qrab, double Qrb, double Qx, 
            double Qxy, double Qy) {
        if (buffer == null)
            buffer = new double[6][6];
        buffer[0][0] = Qx;
        buffer[0][1] = Qxy;
        buffer[0][2] = 0.0;
        buffer[0][3] = 0.0;
        buffer[0][4] = 0.0;
        buffer[0][5] = 0.0;
        buffer[1][0] = Qxy;
        buffer[1][1] = Qy;
        buffer[1][2] = 0.0;
        buffer[1][3] = 0.0;
        buffer[1][4] = 0.0;
        buffer[1][5] = 0.0;
        buffer[2][0] = 0.0;
        buffer[2][1] = 0.0;
        buffer[2][2] = Qra;
        buffer[2][3] = Qrab;
        buffer[2][4] = 0.0;
        buffer[2][5] = 0.0;
        buffer[3][0] = 0.0;
        buffer[3][1] = 0.0;
        buffer[3][2] = Qrab;
        buffer[3][3] = Qrb;
        buffer[3][4] = 0.0;
        buffer[3][5] = 0.0;
        buffer[4][0] = 0.0;
        buffer[4][1] = 0.0;
        buffer[4][2] = 0.0;
        buffer[4][3] = 0.0;
        buffer[4][4] = Qox;
        buffer[4][5] = Qoxy;
        buffer[5][0] = 0.0;
        buffer[5][1] = 0.0;
        buffer[5][2] = 0.0;
        buffer[5][3] = 0.0;
        buffer[5][4] = Qoxy;
        buffer[5][5] = Qoy;
        return buffer;
    }

    // H = (xra ~,xrb ~,0,0,xra,xrb;xrb ~ ~,xra ~,0,0,xrb ~,xra)
    private static double[][] H(double[][] buffer, double[] x) {
        if (buffer == null)
            buffer = new double[2][6];
        buffer[0][0] = (-x[2]);
        buffer[0][1] = (-x[3]);
        buffer[0][2] = 0.0;
        buffer[0][3] = 0.0;
        buffer[0][4] = x[2];
        buffer[0][5] = x[3];
        buffer[1][0] = (-(-x[3]));
        buffer[1][1] = (-x[2]);
        buffer[1][2] = 0.0;
        buffer[1][3] = 0.0;
        buffer[1][4] = (-x[3]);
        buffer[1][5] = x[2];
        return buffer;
    }

    // R = (Rx,Rxy;Rxy,Ry)
    private static double[][] R(double[][] buffer, double Rx, double Rxy, 
            double Ry) {
        if (buffer == null)
            buffer = new double[2][2];
        buffer[0][0] = Rx;
        buffer[0][1] = Rxy;
        buffer[1][0] = Rxy;
        buffer[1][1] = Ry;
        return buffer;
    }

    // x_p = (xx ux xra * + uy ~ xrb * +;xy uy xra * + ux xrb * +;ura xra * urb ~ xrb * +;urb xra * ura xrb * +;xox;xoy)
    private static double[] x_p(double[] buffer, double[] u, double[] x) {
        if (buffer == null)
            buffer = new double[6];
        buffer[0] = ((x[0]+(u[0]*x[2]))+((-u[1])*x[3]));
        buffer[1] = ((x[1]+(u[1]*x[2]))+(u[0]*x[3]));
        buffer[2] = ((u[2]*x[2])+((-u[3])*x[3]));
        buffer[3] = ((u[3]*x[2])+(u[2]*x[3]));
        buffer[4] = x[4];
        buffer[5] = x[5];
        return buffer;
    }

    // P_p = (Px ux Pra * ux * + uy ~ Prb * uy ~ * + Qx +,ux Pra * uy * uy ~ Prb * ux * + Qxy +,ux Pra * ura * uy ~ Prb * urb ~ * +,ux Pra * urb * uy ~ Prb * ura * +,0,0;uy Pra * ux * ux Prb * uy ~ * + Qxy +,Py uy Pra * uy * + ux Prb * ux * + Qy +,uy Pra * ura * ux Prb * urb ~ * +,uy Pra * urb * ux Prb * ura * +,0,0;ura Pra * ux * urb ~ Prb * uy ~ * +,ura Pra * uy * urb ~ Prb * ux * +,ura Pra * ura * urb ~ Prb * urb ~ * + Qra +,ura Pra * urb * urb ~ Prb * ura * + Qrab +,0,0;urb Pra * ux * ura Prb * uy ~ * +,urb Pra * uy * ura Prb * ux * +,urb Pra * ura * ura Prb * urb ~ * + Qrab +,urb Pra * urb * ura Prb * ura * + Qrb +,0,0;0,0,0,0,Pox Qox +,Qoxy;0,0,0,0,Qoxy,Poy Qoy +)
    private static double[][] P_p(double[][] buffer, double[][] P, double[][] Q
            , double[] u) {
        if (buffer == null)
            buffer = new double[6][6];
        buffer[0][0] = (((P[0][0]+((u[0]*P[2][2])*u[0]))+(((-u[1])*P[3][3])*(-u[
                1])))+Q[0][0]);
        buffer[0][1] = ((((u[0]*P[2][2])*u[1])+(((-u[1])*P[3][3])*u[0]))+Q[0][1]
                );
        buffer[0][2] = (((u[0]*P[2][2])*u[2])+(((-u[1])*P[3][3])*(-u[3])));
        buffer[0][3] = (((u[0]*P[2][2])*u[3])+(((-u[1])*P[3][3])*u[2]));
        buffer[0][4] = 0.0;
        buffer[0][5] = 0.0;
        buffer[1][0] = ((((u[1]*P[2][2])*u[0])+((u[0]*P[3][3])*(-u[1])))+Q[0][1]
                );
        buffer[1][1] = (((P[1][1]+((u[1]*P[2][2])*u[1]))+((u[0]*P[3][3])*u[0]))+
                Q[1][1]);
        buffer[1][2] = (((u[1]*P[2][2])*u[2])+((u[0]*P[3][3])*(-u[3])));
        buffer[1][3] = (((u[1]*P[2][2])*u[3])+((u[0]*P[3][3])*u[2]));
        buffer[1][4] = 0.0;
        buffer[1][5] = 0.0;
        buffer[2][0] = (((u[2]*P[2][2])*u[0])+(((-u[3])*P[3][3])*(-u[1])));
        buffer[2][1] = (((u[2]*P[2][2])*u[1])+(((-u[3])*P[3][3])*u[0]));
        buffer[2][2] = ((((u[2]*P[2][2])*u[2])+(((-u[3])*P[3][3])*(-u[3])))+Q[2]
                [2]);
        buffer[2][3] = ((((u[2]*P[2][2])*u[3])+(((-u[3])*P[3][3])*u[2]))+Q[2][3]
                );
        buffer[2][4] = 0.0;
        buffer[2][5] = 0.0;
        buffer[3][0] = (((u[3]*P[2][2])*u[0])+((u[2]*P[3][3])*(-u[1])));
        buffer[3][1] = (((u[3]*P[2][2])*u[1])+((u[2]*P[3][3])*u[0]));
        buffer[3][2] = ((((u[3]*P[2][2])*u[2])+((u[2]*P[3][3])*(-u[3])))+Q[2][3]
                );
        buffer[3][3] = ((((u[3]*P[2][2])*u[3])+((u[2]*P[3][3])*u[2]))+Q[3][3]);
        buffer[3][4] = 0.0;
        buffer[3][5] = 0.0;
        buffer[4][0] = 0.0;
        buffer[4][1] = 0.0;
        buffer[4][2] = 0.0;
        buffer[4][3] = 0.0;
        buffer[4][4] = (P[4][4]+Q[4][4]);
        buffer[4][5] = Q[4][5];
        buffer[5][0] = 0.0;
        buffer[5][1] = 0.0;
        buffer[5][2] = 0.0;
        buffer[5][3] = 0.0;
        buffer[5][4] = Q[4][5];
        buffer[5][5] = (P[5][5]+Q[5][5]);
        return buffer;
    }

    // z = (zx;zy)
    private static double[] z(double[] buffer, double zx, double zy) {
        if (buffer == null)
            buffer = new double[2];
        buffer[0] = zx;
        buffer[1] = zy;
        return buffer;
    }

    // y = (zx xra ~ xx * xrb ~ xy * + xra xox * + xrb xoy * + -;zy xrb ~ ~ xx * xra ~ xy * + xrb ~ xox * + xra xoy * + -)
    private static double[] y(double[] buffer, double[] x, double[] z) {
        if (buffer == null)
            buffer = new double[2];
        buffer[0] = (z[0]-(((((-x[2])*x[0])+((-x[3])*x[1]))+(x[2]*x[4]))+(x[3]*x
                [5])));
        buffer[1] = (z[1]-(((((-(-x[3]))*x[0])+((-x[2])*x[1]))+((-x[3])*x[4]))+(
                x[2]*x[5])));
        return buffer;
    }

    // y2_ = (xra ~ xx * xrb ~ xy * + xra xox * + xrb xoy * +;xrb ~ ~ xx * xra ~ xy * + xrb ~ xox * + xra xoy * +)
    private static double[] y2_(double[] buffer, double[] x) {
        if (buffer == null)
            buffer = new double[2];
        buffer[0] = (((((-x[2])*x[0])+((-x[3])*x[1]))+(x[2]*x[4]))+(x[3]*x[5]));
        buffer[1] = (((((-(-x[3]))*x[0])+((-x[2])*x[1]))+((-x[3])*x[4]))+(x[2]*x
                [5]));
        return buffer;
    }

    // S = (xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx +,xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy +;xrb ~ ~ Px * xra ~ * xra ~ Py * xrb ~ * + xrb ~ Pox * xra * + xra Poy * xrb * + Rxy +,xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry +)
    private static double[][] S(double[][] buffer, double[][] P, double[][] R, 
            double[] x) {
        if (buffer == null)
            buffer = new double[2][2];
        buffer[0][0] = (((((((-x[2])*P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3]
                )))+((x[2]*P[4][4])*x[2]))+((x[3]*P[5][5])*x[3]))+R[0][0]);
        buffer[0][1] = (((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x
                [2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])
                ;
        buffer[1][0] = (((((((-(-x[3]))*P[0][0])*(-x[2]))+(((-x[2])*P[1][1])*(-x
                [3])))+(((-x[3])*P[4][4])*x[2]))+((x[2]*P[5][5])*x[3]))+R[0][1])
                ;
        buffer[1][1] = (((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*
                (-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[
                1][1]);
        return buffer;
    }

    // K = (xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - /,xrb ~ ~ Px * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * -;xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - /,xra ~ Py * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * -;0,0;0,0;xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - /,xrb ~ Pox * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * -;xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - /,xra Poy * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * -)
    private static double[][] K(double[][] buffer, double[][] P, double[][] R, 
            double[] x) {
        if (buffer == null)
            buffer = new double[6][2];
        buffer[0][0] = ((((-x[2])*P[0][0])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+
                (((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[
                5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((
                -x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[
                5][5])*x[2]))+R[1][1]))*((-(-x[3]))*P[0][0])))/((((((((-x[2])*P[
                0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2
                ]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-
                (-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))
                +((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x
                [3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))
                +((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-
                x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3]))
                )+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x
                [3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((
                x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]
                )))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((
                x[2]*P[5][5])*x[2]))+R[1][1])))));
        buffer[0][1] = ((((-(-x[3]))*P[0][0])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]
                )))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((
                x[2]*P[5][5])*x[2]))+R[1][1]))-(((((((((-x[2])*P[0][0])*(-(-x[3]
                )))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3
                ]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))
                +(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2
                ]*P[5][5])*x[2]))+R[1][1]))*((((-x[2])*P[0][0])-(((((((((-x[2])*
                P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4]
                )*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0
                ][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4]
                )*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((-(-x[3]))*P[0][0]
                )))/((((((((-x[2])*P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3]))
                )+((x[2]*P[4][4])*x[2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-(((((((
                (((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[
                2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-
                x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3
                ])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(
                -x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[
                3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((
                -x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*
                P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3
                ]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*
                P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1])))))));
        buffer[1][0] = ((((-x[3])*P[1][1])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+
                (((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[
                5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((
                -x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[
                5][5])*x[2]))+R[1][1]))*((-x[2])*P[1][1])))/((((((((-x[2])*P[0][
                0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))
                +((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x
                [3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((
                x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]
                )))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((
                x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3
                ])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+(
                (x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3]
                )))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3
                ]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))
                +(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2
                ]*P[5][5])*x[2]))+R[1][1])))));
        buffer[1][1] = ((((-x[2])*P[1][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))
                +(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2
                ]*P[5][5])*x[2]))+R[1][1]))-(((((((((-x[2])*P[0][0])*(-(-x[3])))
                +(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P
                [5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+((
                (-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P
                [5][5])*x[2]))+R[1][1]))*((((-x[3])*P[1][1])-(((((((((-x[2])*P[0
                ][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(
                -x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0
                ])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(
                -x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((-x[2])*P[1][1])))/((
                ((((((-x[2])*P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[
                2]*P[4][4])*x[2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[
                2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4
                ][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))
                *P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4
                ][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3])
                )*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[
                4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])
                *P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4
                ])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[
                0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4
                ])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1])))))));
        buffer[2][0] = 0.0;
        buffer[2][1] = 0.0;
        buffer[3][0] = 0.0;
        buffer[3][1] = 0.0;
        buffer[4][0] = (((x[2]*P[4][4])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((
                -x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][
                5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[
                2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][
                5])*x[2]))+R[1][1]))*((-x[3])*P[4][4])))/((((((((-x[2])*P[0][0])
                *(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((
                x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3]
                )))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3
                ]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))
                +(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2
                ]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3]))
                )+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[
                2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))
                +(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P
                [5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+((
                (-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P
                [5][5])*x[2]))+R[1][1])))));
        buffer[4][1] = ((((-x[3])*P[4][4])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))
                +(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2
                ]*P[5][5])*x[2]))+R[1][1]))-(((((((((-x[2])*P[0][0])*(-(-x[3])))
                +(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P
                [5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+((
                (-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P
                [5][5])*x[2]))+R[1][1]))*(((x[2]*P[4][4])-(((((((((-x[2])*P[0][0
                ])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[
                3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*
                (-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[
                3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((-x[3])*P[4][4])))/(((((
                (((-x[2])*P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*
                P[4][4])*x[2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])
                *P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4
                ])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[
                0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4
                ])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P
                [0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][
                4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[
                0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*
                (-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][
                0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*
                (-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1])))))));
        buffer[5][0] = (((x[3]*P[5][5])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((
                -x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][
                5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[
                2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][
                5])*x[2]))+R[1][1]))*(x[2]*P[5][5])))/((((((((-x[2])*P[0][0])*(-
                x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3
                ]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))
                +(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P
                [5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+((
                (-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P
                [5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(
                ((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*
                P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+((
                (-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5]
                [5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x
                [2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5]
                [5])*x[2]))+R[1][1])))));
        buffer[5][1] = (((x[2]*P[5][5])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+((
                (-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P
                [5][5])*x[2]))+R[1][1]))-(((((((((-x[2])*P[0][0])*(-(-x[3])))+((
                (-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5]
                [5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x
                [2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5]
                [5])*x[2]))+R[1][1]))*(((x[3]*P[5][5])-(((((((((-x[2])*P[0][0])*
                (-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])
                ))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(
                -x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])
                ))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(x[2]*P[5][5])))/((((((((-x[
                2])*P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4
                ])*x[2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][
                0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x
                [3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])
                *(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x
                [3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0]
                )*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-
                x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])
                *(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3]
                )))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-
                (-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3]
                )))+((x[2]*P[5][5])*x[2]))+R[1][1])))))));
        return buffer;
    }

    // x_u = (xx xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / zx xra ~ xx * xrb ~ xy * + xra xox * + xrb xoy * + - * xrb ~ ~ Px * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - zy xrb ~ ~ xx * xra ~ xy * + xrb ~ xox * + xra xoy * + - * + +;xy xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / zx xra ~ xx * xrb ~ xy * + xra xox * + xrb xoy * + - * xra ~ Py * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - zy xrb ~ ~ xx * xra ~ xy * + xrb ~ xox * + xra xoy * + - * + +;xra;xrb;xox xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / zx xra ~ xx * xrb ~ xy * + xra xox * + xrb xoy * + - * xrb ~ Pox * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - zy xrb ~ ~ xx * xra ~ xy * + xrb ~ xox * + xra xoy * + - * + +;xoy xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / zx xra ~ xx * xrb ~ xy * + xra xox * + xrb xoy * + - * xra Poy * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - zy xrb ~ ~ xx * xra ~ xy * + xrb ~ xox * + xra xoy * + - * + +)
    private static double[] x_u(double[] buffer, double[][] P, double[][] R, 
            double[] x, double[] z) {
        if (buffer == null)
            buffer = new double[6];
        buffer[0] = (x[0]+((((((-x[2])*P[0][0])-(((((((((-x[2])*P[0][0])*(-(-x[3
                ])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[
                3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]))
                )+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[
                2]*P[5][5])*x[2]))+R[1][1]))*((-(-x[3]))*P[0][0])))/((((((((-x[2
                ])*P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4]
                )*x[2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0
                ])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[
                3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*
                (-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[
                3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])
                *(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x
                [3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*
                (-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])
                ))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(
                -x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])
                ))+((x[2]*P[5][5])*x[2]))+R[1][1])))))*(z[0]-(((((-x[2])*x[0])+(
                (-x[3])*x[1]))+(x[2]*x[4]))+(x[3]*x[5]))))+(((((-(-x[3]))*P[0][0
                ])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[
                2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1
                ]))-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[
                2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/
                (((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])
                ))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))
                *((((-x[2])*P[0][0])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[
                3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])
                *x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])
                *P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])
                *x[2]))+R[1][1]))*((-(-x[3]))*P[0][0])))/((((((((-x[2])*P[0][0])
                *(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((
                x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3]
                )))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3
                ]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))
                +(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2
                ]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3]))
                )+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[
                2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))
                +(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P
                [5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+((
                (-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P
                [5][5])*x[2]))+R[1][1])))))))*(z[1]-(((((-(-x[3]))*x[0])+((-x[2]
                )*x[1]))+((-x[3])*x[4]))+(x[2]*x[5]))))));
        buffer[1] = (x[1]+((((((-x[3])*P[1][1])-(((((((((-x[2])*P[0][0])*(-(-x[3
                ])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[
                3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]))
                )+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[
                2]*P[5][5])*x[2]))+R[1][1]))*((-x[2])*P[1][1])))/((((((((-x[2])*
                P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x
                [2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*
                (-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])
                ))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(
                -x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])
                ))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-
                (-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3]
                )))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(
                -x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+
                ((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[
                3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+
                ((x[2]*P[5][5])*x[2]))+R[1][1])))))*(z[0]-(((((-x[2])*x[0])+((-x
                [3])*x[1]))+(x[2]*x[4]))+(x[3]*x[5]))))+(((((-x[2])*P[1][1])/(((
                ((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+
                (((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-((
                (((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+
                ((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/((((((
                (-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((
                -x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((-
                x[3])*P[1][1])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[
                1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2])
                )+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][
                1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2])
                )+R[1][1]))*((-x[2])*P[1][1])))/((((((((-x[2])*P[0][0])*(-x[2]))
                +(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[5]
                [5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x
                [3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5]
                )*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2]
                )*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5]
                )*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2
                ])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5
                ])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3]
                )*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x
                [2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P
                [1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x
                [2]))+R[1][1])))))))*(z[1]-(((((-(-x[3]))*x[0])+((-x[2])*x[1]))+
                ((-x[3])*x[4]))+(x[2]*x[5]))))));
        buffer[2] = x[2];
        buffer[3] = x[3];
        buffer[4] = (x[4]+(((((x[2]*P[4][4])-(((((((((-x[2])*P[0][0])*(-(-x[3]))
                )+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*
                P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(
                ((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*
                P[5][5])*x[2]))+R[1][1]))*((-x[3])*P[4][4])))/((((((((-x[2])*P[0
                ][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]
                ))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(
                -x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+
                ((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[
                3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+
                ((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x
                [3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))
                +((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[
                3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x
                [3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])
                ))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x
                [2]*P[5][5])*x[2]))+R[1][1])))))*(z[0]-(((((-x[2])*x[0])+((-x[3]
                )*x[1]))+(x[2]*x[4]))+(x[3]*x[5]))))+(((((-x[3])*P[4][4])/((((((
                (-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((
                -x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-(((((
                ((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x
                [2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(
                -x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[
                3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((x[2]*
                P[4][4])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])
                *(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0]
                [1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-
                x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1]
                [1]))*((-x[3])*P[4][4])))/((((((((-x[2])*P[0][0])*(-x[2]))+(((-x
                [3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[5][5])*x
                [3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P
                [1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]
                ))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1]
                [1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]
                ))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1
                ][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2
                ]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1]
                [1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+
                R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1]
                )*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+
                R[1][1])))))))*(z[1]-(((((-(-x[3]))*x[0])+((-x[2])*x[1]))+((-x[3
                ])*x[4]))+(x[2]*x[5]))))));
        buffer[5] = (x[5]+(((((x[3]*P[5][5])-(((((((((-x[2])*P[0][0])*(-(-x[3]))
                )+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*
                P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(
                ((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*
                P[5][5])*x[2]))+R[1][1]))*(x[2]*P[5][5])))/((((((((-x[2])*P[0][0
                ])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+
                ((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[
                3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x
                [3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])
                ))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x
                [2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3]
                )))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((
                x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])
                ))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]
                *P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+
                (((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]
                *P[5][5])*x[2]))+R[1][1])))))*(z[0]-(((((-x[2])*x[0])+((-x[3])*x
                [1]))+(x[2]*x[4]))+(x[3]*x[5]))))+((((x[2]*P[5][5])/(((((((-(-x[
                3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])
                *P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-(((((((((-x
                [2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[
                4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3])
                )*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[
                4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((x[3]*P[5][5
                ])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2
                ])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(
                ((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2]))
                )+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*
                (x[2]*P[5][5])))/((((((((-x[2])*P[0][0])*(-x[2]))+(((-x[3])*P[1]
                [1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[5][5])*x[3]))+R[0
                ][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(
                -x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1
                ])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[
                2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1
                ]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x
                [2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][
                1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[
                2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/
                (((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])
                ))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))
                )))))*(z[1]-(((((-(-x[3]))*x[0])+((-x[2])*x[1]))+((-x[3])*x[4]))
                +(x[2]*x[5]))))));
        return buffer;
    }

    // P_u = (1 xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xra ~ * xrb ~ ~ Px * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xrb ~ ~ * + - Px *,xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xrb ~ * xrb ~ ~ Px * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xra ~ * + ~ Py *,0,0,xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xra * xrb ~ ~ Px * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xrb ~ * + ~ Pox *,xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xrb * xrb ~ ~ Px * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xra * + ~ Poy *;xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xra ~ * xra ~ Py * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xrb ~ ~ * + ~ Px *,1 xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xrb ~ * xra ~ Py * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xra ~ * + - Py *,0,0,xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xra * xra ~ Py * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xrb ~ * + ~ Pox *,xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xrb * xra ~ Py * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xra * + ~ Poy *;0,0,Pra,0,0,0;0,0,0,Prb,0,0;xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xra ~ * xrb ~ Pox * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xrb ~ ~ * + ~ Px *,xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xrb ~ * xrb ~ Pox * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xra ~ * + ~ Py *,0,0,1 xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xra * xrb ~ Pox * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xrb ~ * + - Pox *,xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xrb * xrb ~ Pox * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xra * + ~ Poy *;xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xra ~ * xra Poy * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xrb ~ ~ * + ~ Px *,xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xrb ~ * xra Poy * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xra ~ * + ~ Py *,0,0,xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xra * xra Poy * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xrb ~ * + ~ Pox *,1 xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xrb * xra Poy * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xra * + - Poy *)
    private static double[][] P_u(double[][] buffer, double[][] P, double[][] R
            , double[] x) {
        if (buffer == null)
            buffer = new double[6][6];
        buffer[0][0] = ((1.0-((((((-x[2])*P[0][0])-(((((((((-x[2])*P[0][0])*(-(-
                x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+(
                (x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3
                ])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+(
                (x[2]*P[5][5])*x[2]))+R[1][1]))*((-(-x[3]))*P[0][0])))/((((((((-
                x[2])*P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4]
                [4])*x[2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0
                ][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(
                -x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0
                ])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(
                -x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][
                0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*
                (-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0
                ])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[
                3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*
                (-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[
                3])))+((x[2]*P[5][5])*x[2]))+R[1][1])))))*(-x[2]))+(((((-(-x[3])
                )*P[0][0])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][
                1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2])
                )+R[1][1]))-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][
                1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R
                [0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])
                *(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R
                [1][1]))*((((-x[2])*P[0][0])-(((((((((-x[2])*P[0][0])*(-(-x[3]))
                )+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*
                P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(
                ((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*
                P[5][5])*x[2]))+R[1][1]))*((-(-x[3]))*P[0][0])))/((((((((-x[2])*
                P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x
                [2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*
                (-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])
                ))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(
                -x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])
                ))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-
                (-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3]
                )))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(
                -x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+
                ((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[
                3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+
                ((x[2]*P[5][5])*x[2]))+R[1][1])))))))*(-(-x[3])))))*P[0][0]);
        buffer[0][1] = ((-((((((-x[2])*P[0][0])-(((((((((-x[2])*P[0][0])*(-(-x[3
                ])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[
                3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]))
                )+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[
                2]*P[5][5])*x[2]))+R[1][1]))*((-(-x[3]))*P[0][0])))/((((((((-x[2
                ])*P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4]
                )*x[2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0
                ])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[
                3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*
                (-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[
                3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])
                *(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x
                [3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*
                (-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])
                ))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(
                -x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])
                ))+((x[2]*P[5][5])*x[2]))+R[1][1])))))*(-x[3]))+(((((-(-x[3]))*P
                [0][0])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])
                *(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R
                [1][1]))-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])
                *(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0]
                [1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-
                x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1]
                [1]))*((((-x[2])*P[0][0])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(
                ((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5
                ][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-
                x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5
                ][5])*x[2]))+R[1][1]))*((-(-x[3]))*P[0][0])))/((((((((-x[2])*P[0
                ][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]
                ))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(
                -x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+
                ((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[
                3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+
                ((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x
                [3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))
                +((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[
                3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x
                [3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])
                ))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x
                [2]*P[5][5])*x[2]))+R[1][1])))))))*(-x[2]))))*P[1][1]);
        buffer[0][2] = 0.0;
        buffer[0][3] = 0.0;
        buffer[0][4] = ((-((((((-x[2])*P[0][0])-(((((((((-x[2])*P[0][0])*(-(-x[3
                ])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[
                3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]))
                )+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[
                2]*P[5][5])*x[2]))+R[1][1]))*((-(-x[3]))*P[0][0])))/((((((((-x[2
                ])*P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4]
                )*x[2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0
                ])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[
                3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*
                (-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[
                3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])
                *(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x
                [3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*
                (-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])
                ))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(
                -x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])
                ))+((x[2]*P[5][5])*x[2]))+R[1][1])))))*x[2])+(((((-(-x[3]))*P[0]
                [0])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-
                x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1]
                [1]))-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-
                x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1]
                )/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2
                ])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]
                ))*((((-x[2])*P[0][0])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-
                x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5
                ])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2
                ])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5
                ])*x[2]))+R[1][1]))*((-(-x[3]))*P[0][0])))/((((((((-x[2])*P[0][0
                ])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+
                ((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[
                3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x
                [3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])
                ))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x
                [2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3]
                )))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((
                x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])
                ))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]
                *P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+
                (((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]
                *P[5][5])*x[2]))+R[1][1])))))))*(-x[3]))))*P[4][4]);
        buffer[0][5] = ((-((((((-x[2])*P[0][0])-(((((((((-x[2])*P[0][0])*(-(-x[3
                ])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[
                3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]))
                )+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[
                2]*P[5][5])*x[2]))+R[1][1]))*((-(-x[3]))*P[0][0])))/((((((((-x[2
                ])*P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4]
                )*x[2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0
                ])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[
                3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*
                (-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[
                3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])
                *(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x
                [3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*
                (-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])
                ))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(
                -x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])
                ))+((x[2]*P[5][5])*x[2]))+R[1][1])))))*x[3])+(((((-(-x[3]))*P[0]
                [0])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-
                x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1]
                [1]))-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-
                x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1]
                )/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2
                ])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]
                ))*((((-x[2])*P[0][0])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-
                x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5
                ])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2
                ])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5
                ])*x[2]))+R[1][1]))*((-(-x[3]))*P[0][0])))/((((((((-x[2])*P[0][0
                ])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+
                ((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[
                3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x
                [3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])
                ))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x
                [2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3]
                )))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((
                x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])
                ))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]
                *P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+
                (((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]
                *P[5][5])*x[2]))+R[1][1])))))))*x[2])))*P[5][5]);
        buffer[1][0] = ((-((((((-x[3])*P[1][1])-(((((((((-x[2])*P[0][0])*(-(-x[3
                ])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[
                3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]))
                )+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[
                2]*P[5][5])*x[2]))+R[1][1]))*((-x[2])*P[1][1])))/((((((((-x[2])*
                P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x
                [2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*
                (-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])
                ))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(
                -x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])
                ))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-
                (-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3]
                )))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(
                -x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+
                ((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[
                3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+
                ((x[2]*P[5][5])*x[2]))+R[1][1])))))*(-x[2]))+(((((-x[2])*P[1][1]
                )/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2
                ])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]
                ))-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2
                ])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(
                ((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2]))
                )+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*
                ((((-x[3])*P[1][1])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3
                ])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*
                x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*
                P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*
                x[2]))+R[1][1]))*((-x[2])*P[1][1])))/((((((((-x[2])*P[0][0])*(-x
                [2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]
                *P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+
                (((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[
                5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((
                -x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[
                5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+((
                (-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P
                [5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((
                -x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][
                5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[
                2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][
                5])*x[2]))+R[1][1])))))))*(-(-x[3])))))*P[0][0]);
        buffer[1][1] = ((1.0-((((((-x[3])*P[1][1])-(((((((((-x[2])*P[0][0])*(-(-
                x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+(
                (x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3
                ])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+(
                (x[2]*P[5][5])*x[2]))+R[1][1]))*((-x[2])*P[1][1])))/((((((((-x[2
                ])*P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4]
                )*x[2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0
                ])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[
                3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*
                (-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[
                3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])
                *(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x
                [3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*
                (-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])
                ))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(
                -x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])
                ))+((x[2]*P[5][5])*x[2]))+R[1][1])))))*(-x[3]))+(((((-x[2])*P[1]
                [1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-
                x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1]
                [1]))-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-
                x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1]
                )/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2
                ])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]
                ))*((((-x[3])*P[1][1])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-
                x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5
                ])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2
                ])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5
                ])*x[2]))+R[1][1]))*((-x[2])*P[1][1])))/((((((((-x[2])*P[0][0])*
                (-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x
                [3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])
                ))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]
                *P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+
                (((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]
                *P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))
                +(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2
                ]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+
                (((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[
                5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((
                -x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[
                5][5])*x[2]))+R[1][1])))))))*(-x[2]))))*P[1][1]);
        buffer[1][2] = 0.0;
        buffer[1][3] = 0.0;
        buffer[1][4] = ((-((((((-x[3])*P[1][1])-(((((((((-x[2])*P[0][0])*(-(-x[3
                ])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[
                3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]))
                )+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[
                2]*P[5][5])*x[2]))+R[1][1]))*((-x[2])*P[1][1])))/((((((((-x[2])*
                P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x
                [2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*
                (-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])
                ))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(
                -x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])
                ))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-
                (-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3]
                )))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(
                -x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+
                ((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[
                3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+
                ((x[2]*P[5][5])*x[2]))+R[1][1])))))*x[2])+(((((-x[2])*P[1][1])/(
                ((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2]))
                )+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-
                (((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2]))
                )+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/((((
                (((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(
                ((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((
                (-x[3])*P[1][1])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*
                P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2
                ]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1
                ][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2
                ]))+R[1][1]))*((-x[2])*P[1][1])))/((((((((-x[2])*P[0][0])*(-x[2]
                ))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[
                5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((
                -x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][
                5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[
                2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][
                5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x
                [2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5]
                [5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[
                3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])
                *x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])
                *P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])
                *x[2]))+R[1][1])))))))*(-x[3]))))*P[4][4]);
        buffer[1][5] = ((-((((((-x[3])*P[1][1])-(((((((((-x[2])*P[0][0])*(-(-x[3
                ])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[
                3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]))
                )+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[
                2]*P[5][5])*x[2]))+R[1][1]))*((-x[2])*P[1][1])))/((((((((-x[2])*
                P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x
                [2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*
                (-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])
                ))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(
                -x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])
                ))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-
                (-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3]
                )))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(
                -x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+
                ((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[
                3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+
                ((x[2]*P[5][5])*x[2]))+R[1][1])))))*x[3])+(((((-x[2])*P[1][1])/(
                ((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2]))
                )+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-
                (((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2]))
                )+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/((((
                (((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(
                ((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((
                (-x[3])*P[1][1])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*
                P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2
                ]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1
                ][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2
                ]))+R[1][1]))*((-x[2])*P[1][1])))/((((((((-x[2])*P[0][0])*(-x[2]
                ))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[
                5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((
                -x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][
                5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[
                2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][
                5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x
                [2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5]
                [5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[
                3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])
                *x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])
                *P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])
                *x[2]))+R[1][1])))))))*x[2])))*P[5][5]);
        buffer[2][0] = 0.0;
        buffer[2][1] = 0.0;
        buffer[2][2] = P[2][2];
        buffer[2][3] = 0.0;
        buffer[2][4] = 0.0;
        buffer[2][5] = 0.0;
        buffer[3][0] = 0.0;
        buffer[3][1] = 0.0;
        buffer[3][2] = 0.0;
        buffer[3][3] = P[3][3];
        buffer[3][4] = 0.0;
        buffer[3][5] = 0.0;
        buffer[4][0] = ((-(((((x[2]*P[4][4])-(((((((((-x[2])*P[0][0])*(-(-x[3]))
                )+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*
                P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(
                ((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*
                P[5][5])*x[2]))+R[1][1]))*((-x[3])*P[4][4])))/((((((((-x[2])*P[0
                ][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]
                ))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(
                -x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+
                ((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[
                3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+
                ((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x
                [3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))
                +((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[
                3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x
                [3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])
                ))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x
                [2]*P[5][5])*x[2]))+R[1][1])))))*(-x[2]))+(((((-x[3])*P[4][4])/(
                ((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2]))
                )+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-
                (((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2]))
                )+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/((((
                (((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(
                ((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((
                x[2]*P[4][4])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1
                ][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))
                +R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1
                ])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))
                +R[1][1]))*((-x[3])*P[4][4])))/((((((((-x[2])*P[0][0])*(-x[2]))+
                (((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[5][
                5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[
                3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])
                *x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])
                *P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])
                *x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2]
                )*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5]
                )*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])
                *P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[
                2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[
                1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[
                2]))+R[1][1])))))))*(-(-x[3])))))*P[0][0]);
        buffer[4][1] = ((-(((((x[2]*P[4][4])-(((((((((-x[2])*P[0][0])*(-(-x[3]))
                )+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*
                P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(
                ((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*
                P[5][5])*x[2]))+R[1][1]))*((-x[3])*P[4][4])))/((((((((-x[2])*P[0
                ][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]
                ))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(
                -x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+
                ((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[
                3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+
                ((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x
                [3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))
                +((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[
                3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x
                [3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])
                ))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x
                [2]*P[5][5])*x[2]))+R[1][1])))))*(-x[3]))+(((((-x[3])*P[4][4])/(
                ((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2]))
                )+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-
                (((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2]))
                )+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/((((
                (((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(
                ((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((
                x[2]*P[4][4])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1
                ][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))
                +R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1
                ])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))
                +R[1][1]))*((-x[3])*P[4][4])))/((((((((-x[2])*P[0][0])*(-x[2]))+
                (((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[5][
                5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[
                3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])
                *x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])
                *P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])
                *x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2]
                )*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5]
                )*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])
                *P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[
                2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[
                1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[
                2]))+R[1][1])))))))*(-x[2]))))*P[1][1]);
        buffer[4][2] = 0.0;
        buffer[4][3] = 0.0;
        buffer[4][4] = ((1.0-(((((x[2]*P[4][4])-(((((((((-x[2])*P[0][0])*(-(-x[3
                ])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[
                3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]))
                )+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[
                2]*P[5][5])*x[2]))+R[1][1]))*((-x[3])*P[4][4])))/((((((((-x[2])*
                P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x
                [2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*
                (-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])
                ))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(
                -x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])
                ))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-
                (-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3]
                )))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(
                -x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+
                ((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[
                3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+
                ((x[2]*P[5][5])*x[2]))+R[1][1])))))*x[2])+(((((-x[3])*P[4][4])/(
                ((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2]))
                )+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-
                (((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2]))
                )+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/((((
                (((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(
                ((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((
                x[2]*P[4][4])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1
                ][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))
                +R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1
                ])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))
                +R[1][1]))*((-x[3])*P[4][4])))/((((((((-x[2])*P[0][0])*(-x[2]))+
                (((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[5][
                5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[
                3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])
                *x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])
                *P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])
                *x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2]
                )*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5]
                )*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])
                *P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[
                2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[
                1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[
                2]))+R[1][1])))))))*(-x[3]))))*P[4][4]);
        buffer[4][5] = ((-(((((x[2]*P[4][4])-(((((((((-x[2])*P[0][0])*(-(-x[3]))
                )+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*
                P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(
                ((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*
                P[5][5])*x[2]))+R[1][1]))*((-x[3])*P[4][4])))/((((((((-x[2])*P[0
                ][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]
                ))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(
                -x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+
                ((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[
                3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+
                ((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x
                [3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))
                +((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[
                3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x
                [3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])
                ))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x
                [2]*P[5][5])*x[2]))+R[1][1])))))*x[3])+(((((-x[3])*P[4][4])/((((
                (((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(
                ((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-(((
                ((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+(
                (x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((
                -(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-
                x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((x[2
                ]*P[4][4])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1
                ])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[
                0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*
                (-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[
                1][1]))*((-x[3])*P[4][4])))/((((((((-x[2])*P[0][0])*(-x[2]))+(((
                -x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[5][5])
                *x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])
                *P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[
                2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[
                1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[
                2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P
                [1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x
                [2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[
                1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2])
                )+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][
                1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2])
                )+R[1][1])))))))*x[2])))*P[5][5]);
        buffer[5][0] = ((-(((((x[3]*P[5][5])-(((((((((-x[2])*P[0][0])*(-(-x[3]))
                )+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*
                P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(
                ((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*
                P[5][5])*x[2]))+R[1][1]))*(x[2]*P[5][5])))/((((((((-x[2])*P[0][0
                ])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+
                ((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[
                3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x
                [3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])
                ))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x
                [2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3]
                )))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((
                x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])
                ))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]
                *P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+
                (((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]
                *P[5][5])*x[2]))+R[1][1])))))*(-x[2]))+((((x[2]*P[5][5])/(((((((
                -(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-
                x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-((((((
                (((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[
                2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-
                x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3
                ])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((x[3]*P
                [5][5])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*
                (-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][
                1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x
                [2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][
                1]))*(x[2]*P[5][5])))/((((((((-x[2])*P[0][0])*(-x[2]))+(((-x[3])
                *P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[5][5])*x[3])
                )+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][
                1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R
                [0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])
                *(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R
                [1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1]
                )*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+
                R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])
                *(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0]
                [1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-
                x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1]
                [1])))))))*(-(-x[3])))))*P[0][0]);
        buffer[5][1] = ((-(((((x[3]*P[5][5])-(((((((((-x[2])*P[0][0])*(-(-x[3]))
                )+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*
                P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(
                ((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*
                P[5][5])*x[2]))+R[1][1]))*(x[2]*P[5][5])))/((((((((-x[2])*P[0][0
                ])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+
                ((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[
                3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x
                [3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])
                ))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x
                [2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3]
                )))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((
                x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])
                ))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]
                *P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+
                (((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]
                *P[5][5])*x[2]))+R[1][1])))))*(-x[3]))+((((x[2]*P[5][5])/(((((((
                -(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-
                x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-((((((
                (((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[
                2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-
                x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3
                ])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((x[3]*P
                [5][5])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*
                (-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][
                1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x
                [2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][
                1]))*(x[2]*P[5][5])))/((((((((-x[2])*P[0][0])*(-x[2]))+(((-x[3])
                *P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[5][5])*x[3])
                )+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][
                1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R
                [0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])
                *(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R
                [1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1]
                )*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+
                R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])
                *(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0]
                [1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-
                x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1]
                [1])))))))*(-x[2]))))*P[1][1]);
        buffer[5][2] = 0.0;
        buffer[5][3] = 0.0;
        buffer[5][4] = ((-(((((x[3]*P[5][5])-(((((((((-x[2])*P[0][0])*(-(-x[3]))
                )+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*
                P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(
                ((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*
                P[5][5])*x[2]))+R[1][1]))*(x[2]*P[5][5])))/((((((((-x[2])*P[0][0
                ])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+
                ((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[
                3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x
                [3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])
                ))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x
                [2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3]
                )))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((
                x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])
                ))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]
                *P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+
                (((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]
                *P[5][5])*x[2]))+R[1][1])))))*x[2])+((((x[2]*P[5][5])/(((((((-(-
                x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3
                ])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-(((((((((
                -x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*
                P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3
                ]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*
                P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((x[3]*P[5]
                [5])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x
                [2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])
                /(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2]
                )))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1])
                )*(x[2]*P[5][5])))/((((((((-x[2])*P[0][0])*(-x[2]))+(((-x[3])*P[
                1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[5][5])*x[3]))+R
                [0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])
                *(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0]
                [1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-
                x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1]
                [1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(
                -x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1
                ][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-
                x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1]
                )/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2
                ])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]
                )))))))*(-x[3]))))*P[4][4]);
        buffer[5][5] = ((1.0-(((((x[3]*P[5][5])-(((((((((-x[2])*P[0][0])*(-(-x[3
                ])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[
                3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]))
                )+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[
                2]*P[5][5])*x[2]))+R[1][1]))*(x[2]*P[5][5])))/((((((((-x[2])*P[0
                ][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]
                ))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(
                -x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+
                ((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[
                3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+
                ((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x
                [3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))
                +((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[
                3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x
                [3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])
                ))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x
                [2]*P[5][5])*x[2]))+R[1][1])))))*x[3])+((((x[2]*P[5][5])/(((((((
                -(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-
                x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-((((((
                (((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[
                2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-
                x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3
                ])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((x[3]*P
                [5][5])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*
                (-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][
                1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x
                [2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][
                1]))*(x[2]*P[5][5])))/((((((((-x[2])*P[0][0])*(-x[2]))+(((-x[3])
                *P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[5][5])*x[3])
                )+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][
                1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R
                [0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])
                *(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R
                [1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1]
                )*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+
                R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])
                *(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0]
                [1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-
                x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1]
                [1])))))))*x[2])))*P[5][5]);
        return buffer;
    }

    // x_p_opt = (xx ux xra * + uy ~ xrb * +;xy uy xra * + ux xrb * +;ura xra * urb ~ xrb * +;urb xra * ura xrb * +;xox;xoy)
    private static double[] x_p_opt(double[] buffer, double[] u, double[] x) {
        if (buffer == null)
            buffer = new double[6];
        buffer[0] = ((x[0]+(u[0]*x[2]))+((-u[1])*x[3]));
        buffer[1] = ((x[1]+(u[1]*x[2]))+(u[0]*x[3]));
        buffer[2] = ((u[2]*x[2])+((-u[3])*x[3]));
        buffer[3] = ((u[3]*x[2])+(u[2]*x[3]));
        buffer[4] = x[4];
        buffer[5] = x[5];
        return buffer;
    }

    // P_p_opt = (Px ux Pra * ux * + uy ~ Prb * uy ~ * + Qx +,ux Pra * uy * uy ~ Prb * ux * + Qxy +,ux Pra * ura * uy ~ Prb * urb ~ * +,ux Pra * urb * uy ~ Prb * ura * +,0,0;uy Pra * ux * ux Prb * uy ~ * + Qxy +,Py uy Pra * uy * + ux Prb * ux * + Qy +,uy Pra * ura * ux Prb * urb ~ * +,uy Pra * urb * ux Prb * ura * +,0,0;ura Pra * ux * urb ~ Prb * uy ~ * +,ura Pra * uy * urb ~ Prb * ux * +,ura Pra * ura * urb ~ Prb * urb ~ * + Qra +,ura Pra * urb * urb ~ Prb * ura * + Qrab +,0,0;urb Pra * ux * ura Prb * uy ~ * +,urb Pra * uy * ura Prb * ux * +,urb Pra * ura * ura Prb * urb ~ * + Qrab +,urb Pra * urb * ura Prb * ura * + Qrb +,0,0;0,0,0,0,Pox Qox +,Qoxy;0,0,0,0,Qoxy,Poy Qoy +)
    private static double[][] P_p_opt(double[][] buffer, double[][] P, double
            [][] Q, double[] u) {
        if (buffer == null)
            buffer = new double[6][6];
        buffer[0][0] = (((P[0][0]+((u[0]*P[2][2])*u[0]))+(((-u[1])*P[3][3])*(-u[
                1])))+Q[0][0]);
        buffer[0][1] = ((((u[0]*P[2][2])*u[1])+(((-u[1])*P[3][3])*u[0]))+Q[0][1]
                );
        buffer[0][2] = (((u[0]*P[2][2])*u[2])+(((-u[1])*P[3][3])*(-u[3])));
        buffer[0][3] = (((u[0]*P[2][2])*u[3])+(((-u[1])*P[3][3])*u[2]));
        buffer[0][4] = 0.0;
        buffer[0][5] = 0.0;
        buffer[1][0] = ((((u[1]*P[2][2])*u[0])+((u[0]*P[3][3])*(-u[1])))+Q[0][1]
                );
        buffer[1][1] = (((P[1][1]+((u[1]*P[2][2])*u[1]))+((u[0]*P[3][3])*u[0]))+
                Q[1][1]);
        buffer[1][2] = (((u[1]*P[2][2])*u[2])+((u[0]*P[3][3])*(-u[3])));
        buffer[1][3] = (((u[1]*P[2][2])*u[3])+((u[0]*P[3][3])*u[2]));
        buffer[1][4] = 0.0;
        buffer[1][5] = 0.0;
        buffer[2][0] = (((u[2]*P[2][2])*u[0])+(((-u[3])*P[3][3])*(-u[1])));
        buffer[2][1] = (((u[2]*P[2][2])*u[1])+(((-u[3])*P[3][3])*u[0]));
        buffer[2][2] = ((((u[2]*P[2][2])*u[2])+(((-u[3])*P[3][3])*(-u[3])))+Q[2]
                [2]);
        buffer[2][3] = ((((u[2]*P[2][2])*u[3])+(((-u[3])*P[3][3])*u[2]))+Q[2][3]
                );
        buffer[2][4] = 0.0;
        buffer[2][5] = 0.0;
        buffer[3][0] = (((u[3]*P[2][2])*u[0])+((u[2]*P[3][3])*(-u[1])));
        buffer[3][1] = (((u[3]*P[2][2])*u[1])+((u[2]*P[3][3])*u[0]));
        buffer[3][2] = ((((u[3]*P[2][2])*u[2])+((u[2]*P[3][3])*(-u[3])))+Q[2][3]
                );
        buffer[3][3] = ((((u[3]*P[2][2])*u[3])+((u[2]*P[3][3])*u[2]))+Q[3][3]);
        buffer[3][4] = 0.0;
        buffer[3][5] = 0.0;
        buffer[4][0] = 0.0;
        buffer[4][1] = 0.0;
        buffer[4][2] = 0.0;
        buffer[4][3] = 0.0;
        buffer[4][4] = (P[4][4]+Q[4][4]);
        buffer[4][5] = Q[4][5];
        buffer[5][0] = 0.0;
        buffer[5][1] = 0.0;
        buffer[5][2] = 0.0;
        buffer[5][3] = 0.0;
        buffer[5][4] = Q[4][5];
        buffer[5][5] = (P[5][5]+Q[5][5]);
        return buffer;
    }

    // y_opt = (zx xra ~ xx * xrb ~ xy * + xra xox * + xrb xoy * + -;zy xrb ~ ~ xx * xra ~ xy * + xrb ~ xox * + xra xoy * + -)
    private static double[] y_opt(double[] buffer, double[] x, double[] z) {
        if (buffer == null)
            buffer = new double[2];
        buffer[0] = (z[0]-(((((-x[2])*x[0])+((-x[3])*x[1]))+(x[2]*x[4]))+(x[3]*x
                [5])));
        buffer[1] = (z[1]-(((((-(-x[3]))*x[0])+((-x[2])*x[1]))+((-x[3])*x[4]))+(
                x[2]*x[5])));
        return buffer;
    }

    // S_opt = (xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx +,xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy +;xrb ~ ~ Px * xra ~ * xra ~ Py * xrb ~ * + xrb ~ Pox * xra * + xra Poy * xrb * + Rxy +,xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry +)
    private static double[][] S_opt(double[][] buffer, double[][] P, double[][] 
            R, double[] x) {
        if (buffer == null)
            buffer = new double[2][2];
        buffer[0][0] = (((((((-x[2])*P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3]
                )))+((x[2]*P[4][4])*x[2]))+((x[3]*P[5][5])*x[3]))+R[0][0]);
        buffer[0][1] = (((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x
                [2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])
                ;
        buffer[1][0] = (((((((-(-x[3]))*P[0][0])*(-x[2]))+(((-x[2])*P[1][1])*(-x
                [3])))+(((-x[3])*P[4][4])*x[2]))+((x[2]*P[5][5])*x[3]))+R[0][1])
                ;
        buffer[1][1] = (((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*
                (-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[
                1][1]);
        return buffer;
    }

    // K_opt = (xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - /,xrb ~ ~ Px * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * -;xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - /,xra ~ Py * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * -;0,0;0,0;xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - /,xrb ~ Pox * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * -;xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - /,xra Poy * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * -)
    private static double[][] K_opt(double[][] buffer, double[][] P, double[][] 
            R, double[] x) {
        if (buffer == null)
            buffer = new double[6][2];
        buffer[0][0] = ((((-x[2])*P[0][0])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+
                (((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[
                5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((
                -x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[
                5][5])*x[2]))+R[1][1]))*((-(-x[3]))*P[0][0])))/((((((((-x[2])*P[
                0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2
                ]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-
                (-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))
                +((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x
                [3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))
                +((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-
                x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3]))
                )+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x
                [3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((
                x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]
                )))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((
                x[2]*P[5][5])*x[2]))+R[1][1])))));
        buffer[0][1] = ((((-(-x[3]))*P[0][0])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]
                )))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((
                x[2]*P[5][5])*x[2]))+R[1][1]))-(((((((((-x[2])*P[0][0])*(-(-x[3]
                )))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3
                ]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))
                +(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2
                ]*P[5][5])*x[2]))+R[1][1]))*((((-x[2])*P[0][0])-(((((((((-x[2])*
                P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4]
                )*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0
                ][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4]
                )*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((-(-x[3]))*P[0][0]
                )))/((((((((-x[2])*P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3]))
                )+((x[2]*P[4][4])*x[2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-(((((((
                (((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[
                2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-
                x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3
                ])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(
                -x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[
                3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((
                -x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*
                P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3
                ]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*
                P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1])))))));
        buffer[1][0] = ((((-x[3])*P[1][1])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+
                (((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[
                5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((
                -x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[
                5][5])*x[2]))+R[1][1]))*((-x[2])*P[1][1])))/((((((((-x[2])*P[0][
                0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))
                +((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x
                [3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((
                x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]
                )))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((
                x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3
                ])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+(
                (x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3]
                )))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3
                ]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))
                +(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2
                ]*P[5][5])*x[2]))+R[1][1])))));
        buffer[1][1] = ((((-x[2])*P[1][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))
                +(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2
                ]*P[5][5])*x[2]))+R[1][1]))-(((((((((-x[2])*P[0][0])*(-(-x[3])))
                +(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P
                [5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+((
                (-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P
                [5][5])*x[2]))+R[1][1]))*((((-x[3])*P[1][1])-(((((((((-x[2])*P[0
                ][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(
                -x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0
                ])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(
                -x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((-x[2])*P[1][1])))/((
                ((((((-x[2])*P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[
                2]*P[4][4])*x[2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[
                2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4
                ][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))
                *P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4
                ][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3])
                )*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[
                4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])
                *P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4
                ])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[
                0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4
                ])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1])))))));
        buffer[2][0] = 0.0;
        buffer[2][1] = 0.0;
        buffer[3][0] = 0.0;
        buffer[3][1] = 0.0;
        buffer[4][0] = (((x[2]*P[4][4])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((
                -x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][
                5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[
                2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][
                5])*x[2]))+R[1][1]))*((-x[3])*P[4][4])))/((((((((-x[2])*P[0][0])
                *(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((
                x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3]
                )))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3
                ]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))
                +(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2
                ]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3]))
                )+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[
                2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))
                +(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P
                [5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+((
                (-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P
                [5][5])*x[2]))+R[1][1])))));
        buffer[4][1] = ((((-x[3])*P[4][4])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))
                +(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2
                ]*P[5][5])*x[2]))+R[1][1]))-(((((((((-x[2])*P[0][0])*(-(-x[3])))
                +(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P
                [5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+((
                (-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P
                [5][5])*x[2]))+R[1][1]))*(((x[2]*P[4][4])-(((((((((-x[2])*P[0][0
                ])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[
                3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*
                (-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[
                3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((-x[3])*P[4][4])))/(((((
                (((-x[2])*P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*
                P[4][4])*x[2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])
                *P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4
                ])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[
                0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4
                ])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P
                [0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][
                4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[
                0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*
                (-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][
                0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*
                (-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1])))))));
        buffer[5][0] = (((x[3]*P[5][5])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((
                -x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][
                5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[
                2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][
                5])*x[2]))+R[1][1]))*(x[2]*P[5][5])))/((((((((-x[2])*P[0][0])*(-
                x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3
                ]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))
                +(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P
                [5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+((
                (-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P
                [5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(
                ((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*
                P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+((
                (-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5]
                [5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x
                [2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5]
                [5])*x[2]))+R[1][1])))));
        buffer[5][1] = (((x[2]*P[5][5])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+((
                (-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P
                [5][5])*x[2]))+R[1][1]))-(((((((((-x[2])*P[0][0])*(-(-x[3])))+((
                (-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5]
                [5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x
                [2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5]
                [5])*x[2]))+R[1][1]))*(((x[3]*P[5][5])-(((((((((-x[2])*P[0][0])*
                (-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])
                ))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(
                -x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])
                ))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(x[2]*P[5][5])))/((((((((-x[
                2])*P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4
                ])*x[2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][
                0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x
                [3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])
                *(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x
                [3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0]
                )*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-
                x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])
                *(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3]
                )))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-
                (-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3]
                )))+((x[2]*P[5][5])*x[2]))+R[1][1])))))));
        return buffer;
    }

    // x_u_opt = (xx xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / zx xra ~ xx * xrb ~ xy * + xra xox * + xrb xoy * + - * xrb ~ ~ Px * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - zy xrb ~ ~ xx * xra ~ xy * + xrb ~ xox * + xra xoy * + - * + +;xy xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / zx xra ~ xx * xrb ~ xy * + xra xox * + xrb xoy * + - * xra ~ Py * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - zy xrb ~ ~ xx * xra ~ xy * + xrb ~ xox * + xra xoy * + - * + +;xra;xrb;xox xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / zx xra ~ xx * xrb ~ xy * + xra xox * + xrb xoy * + - * xrb ~ Pox * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - zy xrb ~ ~ xx * xra ~ xy * + xrb ~ xox * + xra xoy * + - * + +;xoy xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / zx xra ~ xx * xrb ~ xy * + xra xox * + xrb xoy * + - * xra Poy * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - zy xrb ~ ~ xx * xra ~ xy * + xrb ~ xox * + xra xoy * + - * + +)
    private static double[] x_u_opt(double[] buffer, double[][] P, double[][] R
            , double[] x, double[] z) {
        if (buffer == null)
            buffer = new double[6];
        buffer[0] = (x[0]+((((((-x[2])*P[0][0])-(((((((((-x[2])*P[0][0])*(-(-x[3
                ])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[
                3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]))
                )+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[
                2]*P[5][5])*x[2]))+R[1][1]))*((-(-x[3]))*P[0][0])))/((((((((-x[2
                ])*P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4]
                )*x[2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0
                ])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[
                3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*
                (-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[
                3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])
                *(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x
                [3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*
                (-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])
                ))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(
                -x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])
                ))+((x[2]*P[5][5])*x[2]))+R[1][1])))))*(z[0]-(((((-x[2])*x[0])+(
                (-x[3])*x[1]))+(x[2]*x[4]))+(x[3]*x[5]))))+(((((-(-x[3]))*P[0][0
                ])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[
                2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1
                ]))-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[
                2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/
                (((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])
                ))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))
                *((((-x[2])*P[0][0])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[
                3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])
                *x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])
                *P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])
                *x[2]))+R[1][1]))*((-(-x[3]))*P[0][0])))/((((((((-x[2])*P[0][0])
                *(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((
                x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3]
                )))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3
                ]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))
                +(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2
                ]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3]))
                )+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[
                2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))
                +(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P
                [5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+((
                (-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P
                [5][5])*x[2]))+R[1][1])))))))*(z[1]-(((((-(-x[3]))*x[0])+((-x[2]
                )*x[1]))+((-x[3])*x[4]))+(x[2]*x[5]))))));
        buffer[1] = (x[1]+((((((-x[3])*P[1][1])-(((((((((-x[2])*P[0][0])*(-(-x[3
                ])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[
                3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]))
                )+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[
                2]*P[5][5])*x[2]))+R[1][1]))*((-x[2])*P[1][1])))/((((((((-x[2])*
                P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x
                [2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*
                (-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])
                ))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(
                -x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])
                ))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-
                (-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3]
                )))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(
                -x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+
                ((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[
                3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+
                ((x[2]*P[5][5])*x[2]))+R[1][1])))))*(z[0]-(((((-x[2])*x[0])+((-x
                [3])*x[1]))+(x[2]*x[4]))+(x[3]*x[5]))))+(((((-x[2])*P[1][1])/(((
                ((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+
                (((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-((
                (((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+
                ((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/((((((
                (-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((
                -x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((-
                x[3])*P[1][1])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[
                1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2])
                )+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][
                1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2])
                )+R[1][1]))*((-x[2])*P[1][1])))/((((((((-x[2])*P[0][0])*(-x[2]))
                +(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[5]
                [5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x
                [3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5]
                )*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2]
                )*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5]
                )*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2
                ])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5
                ])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3]
                )*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x
                [2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P
                [1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x
                [2]))+R[1][1])))))))*(z[1]-(((((-(-x[3]))*x[0])+((-x[2])*x[1]))+
                ((-x[3])*x[4]))+(x[2]*x[5]))))));
        buffer[2] = x[2];
        buffer[3] = x[3];
        buffer[4] = (x[4]+(((((x[2]*P[4][4])-(((((((((-x[2])*P[0][0])*(-(-x[3]))
                )+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*
                P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(
                ((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*
                P[5][5])*x[2]))+R[1][1]))*((-x[3])*P[4][4])))/((((((((-x[2])*P[0
                ][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]
                ))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(
                -x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+
                ((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[
                3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+
                ((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x
                [3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))
                +((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[
                3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x
                [3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])
                ))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x
                [2]*P[5][5])*x[2]))+R[1][1])))))*(z[0]-(((((-x[2])*x[0])+((-x[3]
                )*x[1]))+(x[2]*x[4]))+(x[3]*x[5]))))+(((((-x[3])*P[4][4])/((((((
                (-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((
                -x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-(((((
                ((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x
                [2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(
                -x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[
                3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((x[2]*
                P[4][4])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])
                *(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0]
                [1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-
                x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1]
                [1]))*((-x[3])*P[4][4])))/((((((((-x[2])*P[0][0])*(-x[2]))+(((-x
                [3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[5][5])*x
                [3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P
                [1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]
                ))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1]
                [1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]
                ))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1
                ][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2
                ]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1]
                [1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+
                R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1]
                )*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+
                R[1][1])))))))*(z[1]-(((((-(-x[3]))*x[0])+((-x[2])*x[1]))+((-x[3
                ])*x[4]))+(x[2]*x[5]))))));
        buffer[5] = (x[5]+(((((x[3]*P[5][5])-(((((((((-x[2])*P[0][0])*(-(-x[3]))
                )+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*
                P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(
                ((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*
                P[5][5])*x[2]))+R[1][1]))*(x[2]*P[5][5])))/((((((((-x[2])*P[0][0
                ])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+
                ((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[
                3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x
                [3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])
                ))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x
                [2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3]
                )))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((
                x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])
                ))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]
                *P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+
                (((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]
                *P[5][5])*x[2]))+R[1][1])))))*(z[0]-(((((-x[2])*x[0])+((-x[3])*x
                [1]))+(x[2]*x[4]))+(x[3]*x[5]))))+((((x[2]*P[5][5])/(((((((-(-x[
                3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])
                *P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-(((((((((-x
                [2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[
                4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3])
                )*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[
                4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((x[3]*P[5][5
                ])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2
                ])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(
                ((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2]))
                )+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*
                (x[2]*P[5][5])))/((((((((-x[2])*P[0][0])*(-x[2]))+(((-x[3])*P[1]
                [1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[5][5])*x[3]))+R[0
                ][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(
                -x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1
                ])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[
                2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1
                ]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x
                [2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][
                1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[
                2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/
                (((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])
                ))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))
                )))))*(z[1]-(((((-(-x[3]))*x[0])+((-x[2])*x[1]))+((-x[3])*x[4]))
                +(x[2]*x[5]))))));
        return buffer;
    }

    // P_u_opt = (1 xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xra ~ * xrb ~ ~ Px * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xrb ~ ~ * + - Px *,xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xrb ~ * xrb ~ ~ Px * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xra ~ * + ~ Py *,0,0,xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xra * xrb ~ ~ Px * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xrb ~ * + ~ Pox *,xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xrb * xrb ~ ~ Px * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xra * + ~ Poy *;xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xra ~ * xra ~ Py * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xrb ~ ~ * + ~ Px *,1 xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xrb ~ * xra ~ Py * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xra ~ * + - Py *,0,0,xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xra * xra ~ Py * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xrb ~ * + ~ Pox *,xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xrb * xra ~ Py * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Py * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Py * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xra * + ~ Poy *;0,0,Pra,0,0,0;0,0,0,Prb,0,0;xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xra ~ * xrb ~ Pox * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xrb ~ ~ * + ~ Px *,xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xrb ~ * xrb ~ Pox * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xra ~ * + ~ Py *,0,0,1 xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xra * xrb ~ Pox * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xrb ~ * + - Pox *,xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xrb * xrb ~ Pox * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Pox * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ Pox * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xra * + ~ Poy *;xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xra ~ * xra Poy * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xrb ~ ~ * + ~ Px *,xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xrb ~ * xra Poy * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xra ~ * + ~ Py *,0,0,xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xra * xra Poy * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xrb ~ * + ~ Pox *,1 xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / xrb * xra Poy * xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb Poy * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xra Poy * * - xra ~ Px * xra ~ * xrb ~ Py * xrb ~ * + xra Pox * xra * + xrb Poy * xrb * + Rx + xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + * xra ~ Px * xrb ~ ~ * xrb ~ Py * xra ~ * + xra Pox * xrb ~ * + xrb Poy * xra * + Rxy + xrb ~ ~ Px * xrb ~ ~ * xra ~ Py * xra ~ * + xrb ~ Pox * xrb ~ * + xra Poy * xra * + Ry + / * - / * - xra * + - Poy *)
    private static double[][] P_u_opt(double[][] buffer, double[][] P, double
            [][] R, double[] x) {
        if (buffer == null)
            buffer = new double[6][6];
        buffer[0][0] = ((1.0-((((((-x[2])*P[0][0])-(((((((((-x[2])*P[0][0])*(-(-
                x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+(
                (x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3
                ])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+(
                (x[2]*P[5][5])*x[2]))+R[1][1]))*((-(-x[3]))*P[0][0])))/((((((((-
                x[2])*P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4]
                [4])*x[2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0
                ][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(
                -x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0
                ])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(
                -x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][
                0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*
                (-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0
                ])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[
                3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*
                (-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[
                3])))+((x[2]*P[5][5])*x[2]))+R[1][1])))))*(-x[2]))+(((((-(-x[3])
                )*P[0][0])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][
                1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2])
                )+R[1][1]))-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][
                1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R
                [0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])
                *(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R
                [1][1]))*((((-x[2])*P[0][0])-(((((((((-x[2])*P[0][0])*(-(-x[3]))
                )+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*
                P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(
                ((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*
                P[5][5])*x[2]))+R[1][1]))*((-(-x[3]))*P[0][0])))/((((((((-x[2])*
                P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x
                [2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*
                (-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])
                ))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(
                -x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])
                ))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-
                (-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3]
                )))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(
                -x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+
                ((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[
                3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+
                ((x[2]*P[5][5])*x[2]))+R[1][1])))))))*(-(-x[3])))))*P[0][0]);
        buffer[0][1] = ((-((((((-x[2])*P[0][0])-(((((((((-x[2])*P[0][0])*(-(-x[3
                ])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[
                3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]))
                )+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[
                2]*P[5][5])*x[2]))+R[1][1]))*((-(-x[3]))*P[0][0])))/((((((((-x[2
                ])*P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4]
                )*x[2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0
                ])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[
                3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*
                (-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[
                3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])
                *(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x
                [3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*
                (-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])
                ))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(
                -x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])
                ))+((x[2]*P[5][5])*x[2]))+R[1][1])))))*(-x[3]))+(((((-(-x[3]))*P
                [0][0])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])
                *(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R
                [1][1]))-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])
                *(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0]
                [1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-
                x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1]
                [1]))*((((-x[2])*P[0][0])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(
                ((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5
                ][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-
                x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5
                ][5])*x[2]))+R[1][1]))*((-(-x[3]))*P[0][0])))/((((((((-x[2])*P[0
                ][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]
                ))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(
                -x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+
                ((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[
                3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+
                ((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x
                [3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))
                +((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[
                3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x
                [3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])
                ))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x
                [2]*P[5][5])*x[2]))+R[1][1])))))))*(-x[2]))))*P[1][1]);
        buffer[0][2] = 0.0;
        buffer[0][3] = 0.0;
        buffer[0][4] = ((-((((((-x[2])*P[0][0])-(((((((((-x[2])*P[0][0])*(-(-x[3
                ])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[
                3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]))
                )+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[
                2]*P[5][5])*x[2]))+R[1][1]))*((-(-x[3]))*P[0][0])))/((((((((-x[2
                ])*P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4]
                )*x[2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0
                ])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[
                3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*
                (-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[
                3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])
                *(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x
                [3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*
                (-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])
                ))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(
                -x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])
                ))+((x[2]*P[5][5])*x[2]))+R[1][1])))))*x[2])+(((((-(-x[3]))*P[0]
                [0])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-
                x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1]
                [1]))-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-
                x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1]
                )/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2
                ])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]
                ))*((((-x[2])*P[0][0])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-
                x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5
                ])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2
                ])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5
                ])*x[2]))+R[1][1]))*((-(-x[3]))*P[0][0])))/((((((((-x[2])*P[0][0
                ])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+
                ((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[
                3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x
                [3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])
                ))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x
                [2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3]
                )))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((
                x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])
                ))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]
                *P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+
                (((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]
                *P[5][5])*x[2]))+R[1][1])))))))*(-x[3]))))*P[4][4]);
        buffer[0][5] = ((-((((((-x[2])*P[0][0])-(((((((((-x[2])*P[0][0])*(-(-x[3
                ])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[
                3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]))
                )+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[
                2]*P[5][5])*x[2]))+R[1][1]))*((-(-x[3]))*P[0][0])))/((((((((-x[2
                ])*P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4]
                )*x[2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0
                ])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[
                3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*
                (-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[
                3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])
                *(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x
                [3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*
                (-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])
                ))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(
                -x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])
                ))+((x[2]*P[5][5])*x[2]))+R[1][1])))))*x[3])+(((((-(-x[3]))*P[0]
                [0])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-
                x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1]
                [1]))-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-
                x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1]
                )/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2
                ])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]
                ))*((((-x[2])*P[0][0])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-
                x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5
                ])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2
                ])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5
                ])*x[2]))+R[1][1]))*((-(-x[3]))*P[0][0])))/((((((((-x[2])*P[0][0
                ])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+
                ((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[
                3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x
                [3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])
                ))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x
                [2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3]
                )))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((
                x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])
                ))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]
                *P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+
                (((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]
                *P[5][5])*x[2]))+R[1][1])))))))*x[2])))*P[5][5]);
        buffer[1][0] = ((-((((((-x[3])*P[1][1])-(((((((((-x[2])*P[0][0])*(-(-x[3
                ])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[
                3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]))
                )+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[
                2]*P[5][5])*x[2]))+R[1][1]))*((-x[2])*P[1][1])))/((((((((-x[2])*
                P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x
                [2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*
                (-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])
                ))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(
                -x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])
                ))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-
                (-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3]
                )))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(
                -x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+
                ((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[
                3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+
                ((x[2]*P[5][5])*x[2]))+R[1][1])))))*(-x[2]))+(((((-x[2])*P[1][1]
                )/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2
                ])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]
                ))-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2
                ])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(
                ((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2]))
                )+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*
                ((((-x[3])*P[1][1])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3
                ])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*
                x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*
                P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*
                x[2]))+R[1][1]))*((-x[2])*P[1][1])))/((((((((-x[2])*P[0][0])*(-x
                [2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]
                *P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+
                (((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[
                5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((
                -x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[
                5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+((
                (-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P
                [5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((
                -x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][
                5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[
                2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][
                5])*x[2]))+R[1][1])))))))*(-(-x[3])))))*P[0][0]);
        buffer[1][1] = ((1.0-((((((-x[3])*P[1][1])-(((((((((-x[2])*P[0][0])*(-(-
                x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+(
                (x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3
                ])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+(
                (x[2]*P[5][5])*x[2]))+R[1][1]))*((-x[2])*P[1][1])))/((((((((-x[2
                ])*P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4]
                )*x[2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0
                ])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[
                3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*
                (-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[
                3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])
                *(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x
                [3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*
                (-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])
                ))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(
                -x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])
                ))+((x[2]*P[5][5])*x[2]))+R[1][1])))))*(-x[3]))+(((((-x[2])*P[1]
                [1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-
                x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1]
                [1]))-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-
                x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1]
                )/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2
                ])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]
                ))*((((-x[3])*P[1][1])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-
                x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5
                ])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2
                ])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5
                ])*x[2]))+R[1][1]))*((-x[2])*P[1][1])))/((((((((-x[2])*P[0][0])*
                (-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x
                [3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])
                ))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]
                *P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+
                (((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]
                *P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))
                +(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2
                ]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+
                (((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[
                5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((
                -x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[
                5][5])*x[2]))+R[1][1])))))))*(-x[2]))))*P[1][1]);
        buffer[1][2] = 0.0;
        buffer[1][3] = 0.0;
        buffer[1][4] = ((-((((((-x[3])*P[1][1])-(((((((((-x[2])*P[0][0])*(-(-x[3
                ])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[
                3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]))
                )+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[
                2]*P[5][5])*x[2]))+R[1][1]))*((-x[2])*P[1][1])))/((((((((-x[2])*
                P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x
                [2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*
                (-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])
                ))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(
                -x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])
                ))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-
                (-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3]
                )))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(
                -x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+
                ((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[
                3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+
                ((x[2]*P[5][5])*x[2]))+R[1][1])))))*x[2])+(((((-x[2])*P[1][1])/(
                ((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2]))
                )+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-
                (((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2]))
                )+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/((((
                (((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(
                ((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((
                (-x[3])*P[1][1])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*
                P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2
                ]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1
                ][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2
                ]))+R[1][1]))*((-x[2])*P[1][1])))/((((((((-x[2])*P[0][0])*(-x[2]
                ))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[
                5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((
                -x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][
                5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[
                2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][
                5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x
                [2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5]
                [5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[
                3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])
                *x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])
                *P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])
                *x[2]))+R[1][1])))))))*(-x[3]))))*P[4][4]);
        buffer[1][5] = ((-((((((-x[3])*P[1][1])-(((((((((-x[2])*P[0][0])*(-(-x[3
                ])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[
                3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]))
                )+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[
                2]*P[5][5])*x[2]))+R[1][1]))*((-x[2])*P[1][1])))/((((((((-x[2])*
                P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x
                [2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*
                (-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])
                ))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(
                -x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])
                ))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-
                (-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3]
                )))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(
                -x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+
                ((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[
                3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+
                ((x[2]*P[5][5])*x[2]))+R[1][1])))))*x[3])+(((((-x[2])*P[1][1])/(
                ((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2]))
                )+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-
                (((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2]))
                )+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/((((
                (((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(
                ((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((
                (-x[3])*P[1][1])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*
                P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2
                ]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1
                ][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2
                ]))+R[1][1]))*((-x[2])*P[1][1])))/((((((((-x[2])*P[0][0])*(-x[2]
                ))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[
                5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((
                -x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][
                5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[
                2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][
                5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x
                [2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5]
                [5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[
                3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])
                *x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])
                *P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])
                *x[2]))+R[1][1])))))))*x[2])))*P[5][5]);
        buffer[2][0] = 0.0;
        buffer[2][1] = 0.0;
        buffer[2][2] = P[2][2];
        buffer[2][3] = 0.0;
        buffer[2][4] = 0.0;
        buffer[2][5] = 0.0;
        buffer[3][0] = 0.0;
        buffer[3][1] = 0.0;
        buffer[3][2] = 0.0;
        buffer[3][3] = P[3][3];
        buffer[3][4] = 0.0;
        buffer[3][5] = 0.0;
        buffer[4][0] = ((-(((((x[2]*P[4][4])-(((((((((-x[2])*P[0][0])*(-(-x[3]))
                )+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*
                P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(
                ((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*
                P[5][5])*x[2]))+R[1][1]))*((-x[3])*P[4][4])))/((((((((-x[2])*P[0
                ][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]
                ))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(
                -x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+
                ((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[
                3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+
                ((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x
                [3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))
                +((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[
                3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x
                [3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])
                ))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x
                [2]*P[5][5])*x[2]))+R[1][1])))))*(-x[2]))+(((((-x[3])*P[4][4])/(
                ((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2]))
                )+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-
                (((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2]))
                )+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/((((
                (((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(
                ((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((
                x[2]*P[4][4])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1
                ][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))
                +R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1
                ])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))
                +R[1][1]))*((-x[3])*P[4][4])))/((((((((-x[2])*P[0][0])*(-x[2]))+
                (((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[5][
                5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[
                3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])
                *x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])
                *P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])
                *x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2]
                )*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5]
                )*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])
                *P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[
                2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[
                1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[
                2]))+R[1][1])))))))*(-(-x[3])))))*P[0][0]);
        buffer[4][1] = ((-(((((x[2]*P[4][4])-(((((((((-x[2])*P[0][0])*(-(-x[3]))
                )+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*
                P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(
                ((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*
                P[5][5])*x[2]))+R[1][1]))*((-x[3])*P[4][4])))/((((((((-x[2])*P[0
                ][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]
                ))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(
                -x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+
                ((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[
                3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+
                ((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x
                [3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))
                +((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[
                3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x
                [3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])
                ))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x
                [2]*P[5][5])*x[2]))+R[1][1])))))*(-x[3]))+(((((-x[3])*P[4][4])/(
                ((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2]))
                )+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-
                (((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2]))
                )+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/((((
                (((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(
                ((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((
                x[2]*P[4][4])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1
                ][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))
                +R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1
                ])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))
                +R[1][1]))*((-x[3])*P[4][4])))/((((((((-x[2])*P[0][0])*(-x[2]))+
                (((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[5][
                5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[
                3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])
                *x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])
                *P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])
                *x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2]
                )*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5]
                )*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])
                *P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[
                2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[
                1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[
                2]))+R[1][1])))))))*(-x[2]))))*P[1][1]);
        buffer[4][2] = 0.0;
        buffer[4][3] = 0.0;
        buffer[4][4] = ((1.0-(((((x[2]*P[4][4])-(((((((((-x[2])*P[0][0])*(-(-x[3
                ])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[
                3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]))
                )+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[
                2]*P[5][5])*x[2]))+R[1][1]))*((-x[3])*P[4][4])))/((((((((-x[2])*
                P[0][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x
                [2]))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*
                (-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])
                ))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(
                -x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])
                ))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-
                (-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3]
                )))+((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(
                -x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+
                ((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[
                3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+
                ((x[2]*P[5][5])*x[2]))+R[1][1])))))*x[2])+(((((-x[3])*P[4][4])/(
                ((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2]))
                )+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-
                (((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2]))
                )+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/((((
                (((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(
                ((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((
                x[2]*P[4][4])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1
                ][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))
                +R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1
                ])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))
                +R[1][1]))*((-x[3])*P[4][4])))/((((((((-x[2])*P[0][0])*(-x[2]))+
                (((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[5][
                5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[
                3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])
                *x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])
                *P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])
                *x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2]
                )*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5]
                )*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])
                *P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[
                2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[
                1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[
                2]))+R[1][1])))))))*(-x[3]))))*P[4][4]);
        buffer[4][5] = ((-(((((x[2]*P[4][4])-(((((((((-x[2])*P[0][0])*(-(-x[3]))
                )+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*
                P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(
                ((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*
                P[5][5])*x[2]))+R[1][1]))*((-x[3])*P[4][4])))/((((((((-x[2])*P[0
                ][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]
                ))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(
                -x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+
                ((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[
                3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+
                ((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x
                [3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))
                +((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[
                3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x
                [3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])
                ))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x
                [2]*P[5][5])*x[2]))+R[1][1])))))*x[3])+(((((-x[3])*P[4][4])/((((
                (((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(
                ((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-(((
                ((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+(
                (x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((
                -(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-
                x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((x[2
                ]*P[4][4])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1
                ])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[
                0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*
                (-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[
                1][1]))*((-x[3])*P[4][4])))/((((((((-x[2])*P[0][0])*(-x[2]))+(((
                -x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[5][5])
                *x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])
                *P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[
                2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[
                1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[
                2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P
                [1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x
                [2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[
                1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2])
                )+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][
                1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2])
                )+R[1][1])))))))*x[2])))*P[5][5]);
        buffer[5][0] = ((-(((((x[3]*P[5][5])-(((((((((-x[2])*P[0][0])*(-(-x[3]))
                )+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*
                P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(
                ((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*
                P[5][5])*x[2]))+R[1][1]))*(x[2]*P[5][5])))/((((((((-x[2])*P[0][0
                ])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+
                ((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[
                3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x
                [3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])
                ))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x
                [2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3]
                )))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((
                x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])
                ))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]
                *P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+
                (((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]
                *P[5][5])*x[2]))+R[1][1])))))*(-x[2]))+((((x[2]*P[5][5])/(((((((
                -(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-
                x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-((((((
                (((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[
                2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-
                x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3
                ])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((x[3]*P
                [5][5])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*
                (-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][
                1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x
                [2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][
                1]))*(x[2]*P[5][5])))/((((((((-x[2])*P[0][0])*(-x[2]))+(((-x[3])
                *P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[5][5])*x[3])
                )+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][
                1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R
                [0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])
                *(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R
                [1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1]
                )*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+
                R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])
                *(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0]
                [1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-
                x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1]
                [1])))))))*(-(-x[3])))))*P[0][0]);
        buffer[5][1] = ((-(((((x[3]*P[5][5])-(((((((((-x[2])*P[0][0])*(-(-x[3]))
                )+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*
                P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(
                ((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*
                P[5][5])*x[2]))+R[1][1]))*(x[2]*P[5][5])))/((((((((-x[2])*P[0][0
                ])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+
                ((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[
                3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x
                [3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])
                ))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x
                [2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3]
                )))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((
                x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])
                ))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]
                *P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+
                (((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]
                *P[5][5])*x[2]))+R[1][1])))))*(-x[3]))+((((x[2]*P[5][5])/(((((((
                -(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-
                x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-((((((
                (((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[
                2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-
                x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3
                ])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((x[3]*P
                [5][5])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*
                (-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][
                1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x
                [2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][
                1]))*(x[2]*P[5][5])))/((((((((-x[2])*P[0][0])*(-x[2]))+(((-x[3])
                *P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[5][5])*x[3])
                )+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][
                1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R
                [0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])
                *(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R
                [1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1]
                )*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+
                R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])
                *(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0]
                [1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-
                x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1]
                [1])))))))*(-x[2]))))*P[1][1]);
        buffer[5][2] = 0.0;
        buffer[5][3] = 0.0;
        buffer[5][4] = ((-(((((x[3]*P[5][5])-(((((((((-x[2])*P[0][0])*(-(-x[3]))
                )+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*
                P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(
                ((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*
                P[5][5])*x[2]))+R[1][1]))*(x[2]*P[5][5])))/((((((((-x[2])*P[0][0
                ])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+
                ((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[
                3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x
                [3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])
                ))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x
                [2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3]
                )))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((
                x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])
                ))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]
                *P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+
                (((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]
                *P[5][5])*x[2]))+R[1][1])))))*x[2])+((((x[2]*P[5][5])/(((((((-(-
                x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3
                ])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-(((((((((
                -x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*
                P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3
                ]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*
                P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((x[3]*P[5]
                [5])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x
                [2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])
                /(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2]
                )))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1])
                )*(x[2]*P[5][5])))/((((((((-x[2])*P[0][0])*(-x[2]))+(((-x[3])*P[
                1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[5][5])*x[3]))+R
                [0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])
                *(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0]
                [1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-
                x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1]
                [1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(
                -x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1
                ][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-
                x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1]
                )/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2
                ])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]
                )))))))*(-x[3]))))*P[4][4]);
        buffer[5][5] = ((1.0-(((((x[3]*P[5][5])-(((((((((-x[2])*P[0][0])*(-(-x[3
                ])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[
                3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3]))
                )+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[
                2]*P[5][5])*x[2]))+R[1][1]))*(x[2]*P[5][5])))/((((((((-x[2])*P[0
                ][0])*(-x[2]))+(((-x[3])*P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]
                ))+((x[3]*P[5][5])*x[3]))+R[0][0])-((((((((((-x[2])*P[0][0])*(-(
                -x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+
                ((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[
                3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+
                ((x[2]*P[5][5])*x[2]))+R[1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x
                [3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))
                +((x[2]*P[5][5])*x[2]))+R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[
                3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x
                [3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])
                ))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x
                [2]*P[5][5])*x[2]))+R[1][1])))))*x[3])+((((x[2]*P[5][5])/(((((((
                -(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-
                x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))-((((((
                (((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*(-x[2])))+((x[
                2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][1])/(((((((-(-
                x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x[2])))+(((-x[3
                ])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][1]))*(((x[3]*P
                [5][5])-(((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])*
                (-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0][
                1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-x
                [2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1][
                1]))*(x[2]*P[5][5])))/((((((((-x[2])*P[0][0])*(-x[2]))+(((-x[3])
                *P[1][1])*(-x[3])))+((x[2]*P[4][4])*x[2]))+((x[3]*P[5][5])*x[3])
                )+R[0][0])-((((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][
                1])*(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R
                [0][1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])
                *(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R
                [1][1]))*(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1]
                )*(-x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+
                R[1][1]))*((((((((-x[2])*P[0][0])*(-(-x[3])))+(((-x[3])*P[1][1])
                *(-x[2])))+((x[2]*P[4][4])*(-x[3])))+((x[3]*P[5][5])*x[2]))+R[0]
                [1])/(((((((-(-x[3]))*P[0][0])*(-(-x[3])))+(((-x[2])*P[1][1])*(-
                x[2])))+(((-x[3])*P[4][4])*(-x[3])))+((x[2]*P[5][5])*x[2]))+R[1]
                [1])))))))*x[2])))*P[5][5]);
        return buffer;
    }

    // p = (px;py)
    private static double[] p(double[] buffer, double px, double py) {
        if (buffer == null)
            buffer = new double[2];
        buffer[0] = px;
        buffer[1] = py;
        return buffer;
    }

    // rx = (xra px * xrb ~ py * + xx +;xrb px * xra py * + xy +)
    private static double[] rx(double[] buffer, double[] p, double[] x) {
        if (buffer == null)
            buffer = new double[2];
        buffer[0] = (((x[2]*p[0])+((-x[3])*p[1]))+x[0]);
        buffer[1] = (((x[3]*p[0])+(x[2]*p[1]))+x[1]);
        return buffer;
    }

    // rix = (xra px xx - * xrb py xy - * +;xrb ~ px xx - * xra py xy - * +)
    private static double[] rix(double[] buffer, double[] p, double[] x) {
        if (buffer == null)
            buffer = new double[2];
        buffer[0] = ((x[2]*(p[0]-x[0]))+(x[3]*(p[1]-x[1])));
        buffer[1] = (((-x[3])*(p[0]-x[0]))+(x[2]*(p[1]-x[1])));
        return buffer;
    }

    private static double get_Q_ra(double[][] Q) {
        return Q[2][2];
    }
    
    private static void set_Q_ra(double[][] Q, double Qra) {
        Q[2][2] = Qra;
    }
    
    private static double get_Q_rb(double[][] Q) {
        return Q[3][3];
    }
    
    private static void set_Q_rb(double[][] Q, double Qrb) {
        Q[3][3] = Qrb;
    }
    
    private static double get_Q_ox(double[][] Q) {
        return Q[4][4];
    }
    
    private static void set_Q_ox(double[][] Q, double Qox) {
        Q[4][4] = Qox;
    }
    
    private static double get_Q_oy(double[][] Q) {
        return Q[5][5];
    }
    
    private static void set_Q_oy(double[][] Q, double Qoy) {
        Q[5][5] = Qoy;
    }
    
    private static double get_P_rb(double[][] P) {
        return P[3][3];
    }
    
    private static void set_P_rb(double[][] P, double Prb) {
        P[3][3] = Prb;
    }
    
    private static double get_Q_xy(double[][] Q) {
        return Q[0][1];
    }
    
    private static void set_Q_xy(double[][] Q, double Qxy) {
        Q[0][1] = Qxy;
    }
    
    private static double get_x_ra(double[] x) {
        return x[2];
    }
    
    private static void set_x_ra(double[] x, double xra) {
        x[2] = xra;
    }
    
    private static double get_z_y(double[] z) {
        return z[1];
    }
    
    private static void set_z_y(double[] z, double zy) {
        z[1] = zy;
    }
    
    private static double get_z_x(double[] z) {
        return z[0];
    }
    
    private static void set_z_x(double[] z, double zx) {
        z[0] = zx;
    }
    
    private static double get_u_y(double[] u) {
        return u[1];
    }
    
    private static void set_u_y(double[] u, double uy) {
        u[1] = uy;
    }
    
    private static double get_R_x(double[][] R) {
        return R[0][0];
    }
    
    private static void set_R_x(double[][] R, double Rx) {
        R[0][0] = Rx;
    }
    
    private static double get_u_x(double[] u) {
        return u[0];
    }
    
    private static void set_u_x(double[] u, double ux) {
        u[0] = ux;
    }
    
    private static double get_R_y(double[][] R) {
        return R[1][1];
    }
    
    private static void set_R_y(double[][] R, double Ry) {
        R[1][1] = Ry;
    }
    
    private static double get_x_oy(double[] x) {
        return x[5];
    }
    
    private static void set_x_oy(double[] x, double xoy) {
        x[5] = xoy;
    }
    
    private static double get_x_ox(double[] x) {
        return x[4];
    }
    
    private static void set_x_ox(double[] x, double xox) {
        x[4] = xox;
    }
    
    private static double get_Q_rab(double[][] Q) {
        return Q[2][3];
    }
    
    private static void set_Q_rab(double[][] Q, double Qrab) {
        Q[2][3] = Qrab;
    }
    
    private static double get_P_x(double[][] P) {
        return P[0][0];
    }
    
    private static void set_P_x(double[][] P, double Px) {
        P[0][0] = Px;
    }
    
    private static double get_P_y(double[][] P) {
        return P[1][1];
    }
    
    private static void set_P_y(double[][] P, double Py) {
        P[1][1] = Py;
    }
    
    private static double get_x_rb(double[] x) {
        return x[3];
    }
    
    private static void set_x_rb(double[] x, double xrb) {
        x[3] = xrb;
    }
    
    private static double get_u_rb(double[] u) {
        return u[3];
    }
    
    private static void set_u_rb(double[] u, double urb) {
        u[3] = urb;
    }
    
    private static double get_u_ra(double[] u) {
        return u[2];
    }
    
    private static void set_u_ra(double[] u, double ura) {
        u[2] = ura;
    }
    
    private static double get_R_xy(double[][] R) {
        return R[0][1];
    }
    
    private static void set_R_xy(double[][] R, double Rxy) {
        R[0][1] = Rxy;
    }
    
    private static double get_x_x(double[] x) {
        return x[0];
    }
    
    private static void set_x_x(double[] x, double xx) {
        x[0] = xx;
    }
    
    private static double get_x_y(double[] x) {
        return x[1];
    }
    
    private static void set_x_y(double[] x, double xy) {
        x[1] = xy;
    }
    
    private static double get_Q_oxy(double[][] Q) {
        return Q[4][5];
    }
    
    private static void set_Q_oxy(double[][] Q, double Qoxy) {
        Q[4][5] = Qoxy;
    }
    
    private static double get_p_x(double[] p) {
        return p[0];
    }
    
    private static void set_p_x(double[] p, double px) {
        p[0] = px;
    }
    
    private static double get_p_y(double[] p) {
        return p[1];
    }
    
    private static void set_p_y(double[] p, double py) {
        p[1] = py;
    }
    
    private static double get_Q_x(double[][] Q) {
        return Q[0][0];
    }
    
    private static void set_Q_x(double[][] Q, double Qx) {
        Q[0][0] = Qx;
    }
    
    private static double get_Q_y(double[][] Q) {
        return Q[1][1];
    }
    
    private static void set_Q_y(double[][] Q, double Qy) {
        Q[1][1] = Qy;
    }
    
    private static double get_P_ox(double[][] P) {
        return P[4][4];
    }
    
    private static void set_P_ox(double[][] P, double Pox) {
        P[4][4] = Pox;
    }
    
    private static double get_P_ra(double[][] P) {
        return P[2][2];
    }
    
    private static void set_P_ra(double[][] P, double Pra) {
        P[2][2] = Pra;
    }
    
    private static double get_P_oy(double[][] P) {
        return P[5][5];
    }
    
    private static void set_P_oy(double[][] P, double Poy) {
        P[5][5] = Poy;
    }
    
	/*^ END GENERATED CODE */
}
