/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.freedroidz.icommand.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * 
 * The class RobotProperties is responsible for handling the configuration 
 * of a robot, meaning its driving-related parameters, which motor is plugged in which
 * port and so on.
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class RobotProperties extends Properties {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7007419572407555326L;
	
	// The name of the robot
	public final static String ROBOT_NAME = "ROBOT_NAME";
	
	// Motors
	public final static String MOTOR_LEFT = "MOTOR_LEFT";
	public final static String MOTOR_RIGHT = "MOTOR_RIGHT";
	public final static String MOTOR_EXTRA = "MOTOR_EXTRA";
	public final static String INVERT_LEFT = "INVERT_LEFT";
	public final static String INVERT_RIGHT = "INVERT_RIGHT";
	
	// Sensors
	public final static String ULTRASONIC_PORT = "ULTRASONIC_PORT_";
	public final static String LIGHTSENSOR_PORT = "LIGHT_SENSOR_PORT_";
	public final static String TOUCHSENSOR_PORT = "TOUCH_SENSOR_PORT_";
	
	// Dimensions
	public final static String FULL_TURN_TACHO = "FULL_TURN_TACHO";
	protected Long fullTurnTacho;
	public final static String WHEEL_DIAMETER = "WHEEL_DIAMETER";
	public final static String WHEEL_DISTANCE = "WHEEL_DISTANCE";
	
	// BT-connection
	public final static String NXT_BTADDRESS = "NXT_BTADDRESS";
	
	public RobotProperties() {
		setProperty(ROBOT_NAME, "none");
		setProperty(MOTOR_LEFT, "a");
		setProperty(MOTOR_RIGHT, "b");
		
//		setProperty(ULTRASONIC_PORT, "1");
//		setProperty(LIGHTSENSOR_PORT, "2");
//		setProperty(TOUCHSENSOR_PORT, "3");
		
		setProperty(FULL_TURN_TACHO, "600");
	}
	
	public RobotProperties(File configFile) throws IOException {
		load(new FileInputStream(configFile));
	}

	public long getFullTurnTacho() {
		if(fullTurnTacho == null)
			fullTurnTacho = Long.valueOf(Long.parseLong(getProperty(FULL_TURN_TACHO)));
		
		return fullTurnTacho.longValue();
	}
}