package org.evolvis.freedroidz.ui;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

/**
 * Helper class for controlling swt table widgets.
 * @author ldegen
 *
 */
public class TableControl<T> {
	private Table table;
	private Map<Integer,T> values = new HashMap<Integer, T>();
	private Map<T,Integer> positions= new HashMap<T, Integer>();
	
	public TableControl(Table table) {
		this.table=table;
	}
	public void addEntry(String label,Image image,T value){
		int position = table.getItemCount();
		TableItem item = new TableItem(table, SWT.NONE);
		item.setText(label);
		item.setImage(image);
		values.put(position, value);
		positions.put(value,position);
	}
	public void select(T value){
		int position=positions.get(value);
		table.select(position);		
	}
	public T getSelectedValue(){
		int position = table.getSelectionIndex();
		T value = values.get(position);
		return value;
	}
	public void removeAll() {
		values.clear();
		positions.clear();
		table.removeAll();
	}
}
