/**
 * 
 */
package org.evolvis.freedroidz.lejos;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;

public final class ExternalProcessCompileStrategy implements
		CompileStrategy {
	/**
	 * 
	 */
	private final CompileLinkFile compileLinkFile;

	/**
	 * @param compileLinkFile
	 */
	ExternalProcessCompileStrategy(CompileLinkFile compileLinkFile) {
		this.compileLinkFile = compileLinkFile;
	}
 
	
	public boolean compile(Collection<File> sourceFiles,File destinationDirectory,
			OutputStream output, ProblemCollector collector) {
		byte[] blockBuffer = new byte[256];
		try {
			String[] args = this.compileLinkFile.buildJavacCommandLineArray(sourceFiles,destinationDirectory,true);
			Process compileProcess = Runtime.getRuntime().exec(args);
			
			InputStream in = compileProcess.getErrorStream();
			int blockSize = 0;
			int totalSize = 0;
			while ((blockSize = in.read(blockBuffer)) >= 0) {
				output.write(blockBuffer, 0, blockSize);
				totalSize += blockSize;
			}
			in.close();
			

			if (totalSize > 2) {// "if more then two bytes are read, there is an error"
				return false;
			}

		} catch (IOException e) { 
			e.printStackTrace();
		}
		return true;

	}
}