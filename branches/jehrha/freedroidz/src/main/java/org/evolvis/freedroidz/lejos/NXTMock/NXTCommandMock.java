package org.evolvis.freedroidz.lejos.NXTMock;

import java.io.File;
import java.util.HashMap;

import lejos.nxt.remote.FileInfo;
import lejos.nxt.remote.NXTCommand;
import lejos.pc.comm.NXTComm;


public class NXTCommandMock extends NXTCommand { 
	
	HashMap<String,File> fileSystem = null;
	NXTComm comm;

	
	public NXTCommandMock(){
		fileSystem = new HashMap<String,File>();
	}

	public byte delete(String toDelete) {
		if(fileSystem.get(toDelete) != null){
			fileSystem.remove(toDelete);
		}
		return (byte)1;
		
	}
	
	public void runNXTEmulation(Thread thread){
		thread.run();
	};

	public FileInfo findFirst(String wildcard) {
		
		File file = fileSystem.get(wildcard);
		
		String name = null;
		
		if(file != null)
			return new FileInfo(name);
		else
			return null;
		
	}

	public boolean open() {

		return false;
	}

	public void setNXTComm(NXTComm nxtcomm) {
		this.comm = nxtcomm;
	}

	public String uploadFile(File file) {
		fileSystem.put(file.getName(), file);
		return "Upload successful";
	}

	public void close() {

	}
	
}
