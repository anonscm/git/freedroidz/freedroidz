/**
 * 
 */
package org.evolvis.freedroidz.lejos;

import java.io.File;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Collection;
import java.util.List;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticListener;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;

final class NewAPICompileStrategy implements CompileStrategy {
	private CompileLinkFile compileLinkFile;

	NewAPICompileStrategy(CompileLinkFile compileLinkFile) {
		this.compileLinkFile = compileLinkFile;
	}
	
	
	public boolean compile(Collection<File> sourceFiles, File destinationDirectory, OutputStream out,
			final ProblemCollector collector)  {

		JavaCompiler javac = javax.tools.ToolProvider.getSystemJavaCompiler();
		
		Iterable<? extends JavaFileObject> fileObjects = javac
				.getStandardFileManager(null, null, null)
				.getJavaFileObjectsFromFiles(sourceFiles);

		DiagnosticListener<JavaFileObject> diagnosticListener = null;
		if (collector != null) {
			diagnosticListener = new DiagnosticListener<JavaFileObject>() {
				
				
				public void report(
						Diagnostic<? extends JavaFileObject> diagnostic) {
					File sourceFile = new File(diagnostic.getSource().toUri().toString());
					String message = diagnostic.getMessage(null);
					long lineNumber = diagnostic.getLineNumber();
					collector.addProblem(message, sourceFile, lineNumber);
				}
			};
		}
		List<String> options = this.compileLinkFile.buildJavacComandLineList(null, destinationDirectory, false);
		
		
		CompilationTask task = javac.getTask(new OutputStreamWriter(out), null,
				diagnosticListener, options, null, fileObjects);
		Boolean result = task.call();
		
		return result;

	}

	
}