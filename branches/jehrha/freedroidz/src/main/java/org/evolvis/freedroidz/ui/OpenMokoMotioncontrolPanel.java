package org.evolvis.freedroidz.ui;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import lejos.pc.comm.NXTCommException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Slider;
import org.evolvis.freedroidz.utilities.RemoteControl;

public class OpenMokoMotioncontrolPanel extends Composite implements Runnable{
	
	Shell parent;
	Slider sliderV;
	Slider sliderH;
	boolean loop = true;
	
	int x;
	int y;
	int z;
	
	String adress = "";
	RemoteControl remoteControl;
	
	int centerSpeed = 0;
	int oldCenterSpeed = 0;
	
	Thread motionTrackingThread;
	
	public OpenMokoMotioncontrolPanel(Shell parent){
		super(parent, 0);
		this.parent = parent;
		if(!initGui())
			return;
		
		motionTrackingThread = new Thread(this);
		motionTrackingThread.start();
	}
	
	public void run(){
		try {
			trackMotion();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		try {
			remoteControl.closeConnection();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Display.getDefault().asyncExec(new Runnable(){
			public void run(){
				parent.dispose();
			}
		});
	}
	
	public boolean initGui(){
		
		MessageBox messageBox = new MessageBox(parent, SWT.ICON_INFORMATION | SWT.OK);
		messageBox.setText("Important");
		messageBox.setMessage("It's necessary to run RemoteControl.nxj on your brick to use this feature.");
		messageBox.open();
		
		BluetoothScanPanel bluetoothScanPanel = new BluetoothScanPanel();
		adress = bluetoothScanPanel.runGUI();
		
		if(adress == null)
			return false;
		
		parent.setFullScreen(true);
		sliderV = new Slider (this, SWT.VERTICAL);
		sliderV.setBounds (180, 1, 32, 400);
		sliderH = new Slider (this, SWT.HORIZONTAL);
		sliderH.setBounds (1, 180, 400, 32);
		sliderV.setMaximum(1500);
		sliderH.setMaximum(1500);
		
		
		Image imagebtup = new Image(Display.getCurrent(), "/usr/share/icons/hicolor/100x70/apps/croco_open.png");
		ImageData imagedatabtup = imagebtup.getImageData();
		imagebtup = new Image(Display.getCurrent(), rotate(imagedatabtup, SWT.RIGHT));
		Button btup = new Button(this, 0);
		btup.setImage(imagebtup);
		btup.setBounds(240, 420, 230, 230);
		
		Image imagebtdown = new Image(Display.getCurrent(), "/usr/share/icons/hicolor/100x70/apps/croco_closed.png");
		ImageData imagedatabtdown = imagebtdown.getImageData();
		imagebtdown = new Image(Display.getCurrent(), rotate(imagedatabtdown, SWT.RIGHT));
		Button btdown = new Button(this, 0);
		btdown.setImage(imagebtdown);
		btdown.setBounds(5, 420, 230, 230);
		
		

		Image image = new Image(Display.getCurrent(), "/usr/share/icons/hicolor/128x128/apps/freedroidz.png");
		
		GC gc = new GC(image);
	    gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
	    gc.drawString("Stop", 38, 67, true);
	    gc.dispose();
	    
	    ImageData imagedata = image.getImageData();
		image = new Image(Display.getCurrent(), rotate(imagedata, SWT.RIGHT));
	    
	    Button stop = new Button(this, SWT.PUSH);
		stop.setBounds (277, 20, 128, 128);
		stop.setImage(image);
		
		btup.addFocusListener(new FocusListener(){
			public void focusGained(FocusEvent arg0) {
//				RobotControl.getInstance().getRobotHardware().getMotorExtra().forward();
				centerSpeed = 200;
				
			}
			public void focusLost(FocusEvent arg0) {
			}
		});
		
		btdown.addFocusListener(new FocusListener(){
			public void focusGained(FocusEvent arg0) {
//				RobotControl.getInstance().getRobotHardware().getMotorExtra().backward();
				centerSpeed = -200;
			}
			public void focusLost(FocusEvent arg0) {
			}
		});
		
		btup.addSelectionListener(new SelectionListener(){
			public void widgetDefaultSelected(
					SelectionEvent arg0) {
			}

			public void widgetSelected(SelectionEvent arg0) {
//				RobotControl.getInstance().getRobotHardware().getMotorExtra().stop();
				centerSpeed = 0;
				sliderV.setFocus();
			}
		});
		
		btdown.addSelectionListener(new SelectionListener(){
			public void widgetDefaultSelected(
					SelectionEvent arg0) {
			}

			public void widgetSelected(SelectionEvent arg0) {
//				RobotControl.getInstance().getRobotHardware().getMotorExtra().stop();
				centerSpeed = 0;
				sliderV.setFocus();
			}
		});
		
		
		stop.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent evt) {
				stopRemote();
			}
		});
		
		return true;
	}
	
	public void trackMotion() throws InterruptedException{
		
		remoteControl = new RemoteControl(adress);
		try {
			remoteControl.initConnection();
		} catch (NXTCommException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("Motiontracking started");
		FileInputStream inputStream = null;
		byte[] input = new byte[48];
		try {
			inputStream = new FileInputStream("/dev/input/event3");
		} catch (FileNotFoundException e2) {
			e2.printStackTrace();
		}
		
		while(loop)
		{
			try {
				Thread.sleep(20);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			try {
				inputStream.read(input);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			
			if(input[10] == 0){
				x = 256 * input[13] + (input[12] & 0xff);}
			if(input[26] == 0){
				x = 256 * input[29] + (input[28] & 0xff);}
			if(input[42] == 0){
				x = 256 * input[45] + (input[44] & 0xff);} 
			
			if(input[10] == 1){
				y = 256 * input[13] + (input[12] & 0xff);}  
			if(input[26] == 1){
				y = 256 * input[29] + (input[28] & 0xff);}
			if(input[42] == 1){
				y = 256 * input[45] + (input[44] & 0xff);}  
			
			if(input[10] == 2){
				z = 256 * input[13] + (input[12] & 0xff);}
			if(input[26] == 2){
				z = 256 * input[29] + (input[28] & 0xff);}
			if(input[42] == 2){
				z = 256 * input[45] + (input[44] & 0xff);}
			
			
			int powerx = x / 100;
			int powery = y / 100;
			
		
			if((x != 0) && (y != 0)){
//				System.out.println();
//				System.out.println("X: "+powerx);
//				System.out.println("Y: "+powery);
				
				
				int[] commands = new int[3];
				
				if(powery < -2){
						A(commands, true, true, powery*-50);
						C(commands, true, false, powery*50);
//						System.out.println("Right");
				}
				else if(powery > 2){
						A(commands, true, false, powery*-50);
						C(commands, true, true, powery*50);
//						System.out.println("Left");
				}
				
				else{
					if(powerx < -2){
							A(commands, true, false, powerx*100);
							C(commands, true, false, powerx*100);
//							System.out.println("Back");
					}
					else if(powerx > 2){
							A(commands, true, true, powerx*100);
							C(commands, true, true, powerx*100);
//							System.out.println("Go");
					}
					
					else{
//							System.out.println("Break");
							A(commands, false, false, powerx*-100);
							C(commands, false, false, powerx*-100);
	
					}	
				}
				
				remoteControl.centerMotor(commands, centerSpeed);
				
				if((changedA && changedC) || (centerSpeed != oldCenterSpeed)){
					remoteControl.flushCommand(commands);
					oldCenterSpeed = centerSpeed;
				}
				setPositionAsync((-1*y)+750, x+750);
			}
		}
	}
	
	public boolean changedA = false;
	public boolean powerA = false;
	public boolean directionA = false;
	public int speedA = 200;
	
	public boolean changedC = false;
	public boolean powerC = false;
	public boolean directionC = false;
	public int speedC = 200;
	
	public void A(int[] command, boolean power, boolean direction, int speed) throws InterruptedException{
		if((power != powerA) || (direction != directionA) || (speed != speedA)){
			if(power){
				remoteControl.leftMotor(command, speed);
			}
			else if (!power){
				remoteControl.leftMotor(command, 0);
			}
		powerA = power;
		directionA = direction;
		speedA = speed;
		changedA = true;
		}else{
			changedA = false;
		}
	}
	
	public void C(int[] command, boolean power, boolean direction, int speed) throws InterruptedException{
		if((power != powerC) || (direction != directionC) || (speed != speedC)){
			if(power){
				remoteControl.rightMotor(command, speed);
			}
			else if (!power){
				remoteControl.rightMotor(command, 0);
			}
			powerC = power;
			directionC = direction;
			speedC = speed;
			changedC = true;
			
		}else{
			changedC = false;
		}
	}
	
	
	public void setPositionAsync(final int v, final int h) {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				sliderH.setSelection(h);
				sliderV.setSelection(v);
			}			
		});
	}


	
	ImageData rotate(ImageData srcData, int direction) {
	    int bytesPerPixel = srcData.bytesPerLine / srcData.width;
	    int destBytesPerLine = (direction == SWT.DOWN) ? srcData.width
	        * bytesPerPixel : srcData.height * bytesPerPixel;
	    byte[] newData = new byte[srcData.data.length];
	    int width = 0, height = 0;
	    for (int srcY = 0; srcY < srcData.height; srcY++) {
	      for (int srcX = 0; srcX < srcData.width; srcX++) {
	        int destX = 0, destY = 0, destIndex = 0, srcIndex = 0;
	        switch (direction) {
	        case SWT.LEFT: // left 90 degrees
	          destX = srcY;
	          destY = srcData.width - srcX - 1;
	          width = srcData.height;
	          height = srcData.width;
	          break;
	        case SWT.RIGHT: // right 90 degrees
	          destX = srcData.height - srcY - 1;
	          destY = srcX;
	          width = srcData.height;
	          height = srcData.width;
	          break;
	        case SWT.DOWN: // 180 degrees
	          destX = srcData.width - srcX - 1;
	          destY = srcData.height - srcY - 1;
	          width = srcData.width;
	          height = srcData.height;
	          break;
	        }
	        destIndex = (destY * destBytesPerLine)
	            + (destX * bytesPerPixel);
	        srcIndex = (srcY * srcData.bytesPerLine)
	            + (srcX * bytesPerPixel);
	        System.arraycopy(srcData.data, srcIndex, newData, destIndex,
	            bytesPerPixel);
	      }
	    }
	    // destBytesPerLine is used as scanlinePad to ensure that no padding is
	    // required
	    return new ImageData(width, height, srcData.depth, srcData.palette,
	        destBytesPerLine, newData);
	  }
	
	public void stopRemote(){
		loop = false;
	}
	
}
