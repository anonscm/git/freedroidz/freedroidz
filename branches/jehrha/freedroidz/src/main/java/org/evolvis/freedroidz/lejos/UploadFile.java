package org.evolvis.freedroidz.lejos;

import lejos.nxt.remote.NXTCommand;
import lejos.pc.comm.NXTComm;
import lejos.pc.tools.NXJUploadException;
import lejos.pc.tools.Upload;

import org.evolvis.freedroidz.Log;
import org.evolvis.freedroidz.ui.ProcessPanel;

public class UploadFile implements LejosFunction{

	String message = "";
	NXTComm nxtComm = null;
	NXTCommand nxtCommand = null;

	static UploadFile instance = null;
	Exception tmp;
	private LejosUICallback callback;
	

	public UploadFile() {

	}

	public UploadFile(NXTComm comm, NXTCommand command) {
		this.nxtComm = comm;
		this.nxtCommand = command;
	}
 

	public void uploadFile(final String filename, final String mac,
			final int connection, final boolean run) {

		// try {
		// nxtComm = Connection.getInstance(connection).getNXTComm();
		// nxtCommand = Connection.getInstance(connection).getNXTCommand();
		//			
		// NXTInfo nxts = new NXTInfo(connection, null, mac);
		//
		// File nxjFile = new File(filename);
		//
		// if(!nxtComm.open(nxts, NXTComm.LCP))
		// return false;
		//			
		// nxtCommand.setNXTComm(nxtComm);
		//
		// message = nxtCommand.uploadFile(nxjFile, nxjFile.getName());
		// nxtCommand.close();
		// } catch (NXTCommException e) {
		// e.printStackTrace();
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		//
		// if (message.startsWith("Upload successful"))
		// return true;
		// return false;


		final Upload fUpload = new Upload();

		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				ProcessPanel pp = new ProcessPanel("Übertrage...");
				try {
					fUpload.upload(null, mac, connection, filename, run);
					pp.stopPanel();
					callback.processDone(LejosUICallback.OPERATION_SUCCESS);
				} catch (NXJUploadException e) {
					pp.stopPanel();
					e.printStackTrace();
					tmp = e;
					Log.log(e);
					callback.processDone(LejosUICallback.OPERATION_FAIL);
				}
			}

		});
		
		
		
		thread.start();
		
		
//		if(tmp == null){
//			return true;
//		}else{
//			return false;
//		}
		
		

	}

	public static UploadFile getInstance() {
		if (instance == null)
			instance = new UploadFile();
		return instance;
	}

	@Override
	public void registerCallback(LejosUICallback callback) {
		this.callback = callback;
		
	}

}
