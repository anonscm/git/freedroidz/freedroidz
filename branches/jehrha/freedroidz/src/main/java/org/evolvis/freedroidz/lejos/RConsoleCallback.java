package org.evolvis.freedroidz.lejos;

public interface RConsoleCallback {
	public void print(String toprint);
	public void clear();
}
