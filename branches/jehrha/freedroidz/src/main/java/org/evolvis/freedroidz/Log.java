package org.evolvis.freedroidz;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.evolvis.freedroidz.lejos.Utilities;

public class Log {

	protected static File logFile = new File(Utilities.getFreedroidzDir() + File.separator
			+ "freedroidz.log");
	
	protected static BufferedOutputStream bos;
	
	protected static PrintStream log_ps;
	
	protected static OutputStream registered_os;
	
	private static OutputStream os;

	public static void initLogging() throws FileNotFoundException {
		if(!logFile.exists()){
			try {
				logFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		bos = new BufferedOutputStream(new FileOutputStream(logFile));
		
		log_ps = new PrintStream(new BufferedOutputStream(new FileOutputStream(logFile))); 

	}
	
	public static void log(String message){
		try {
			bos.write((message+"\n").getBytes());
			bos.flush();
			print(message);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public static void log(Exception e){
			e.printStackTrace(log_ps);
			print(e);
			log_ps.flush();	
	}
	
	public static void print(Exception e) {
		if(registered_os != null){
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String stacktrace = sw.toString();
		
		try {
			registered_os.write(stacktrace.getBytes());
			registered_os.flush();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		}
	}
	
	public static void print(String message) {
		if(registered_os != null){
		try{
			registered_os.write((message+"\n").getBytes());
			registered_os.flush();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		}
	}
	
	public static void registerOutputStream(OutputStream os0){
		registered_os = os0;
	}
}
