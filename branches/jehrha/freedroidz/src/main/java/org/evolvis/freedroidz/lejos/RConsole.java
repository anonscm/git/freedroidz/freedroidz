package org.evolvis.freedroidz.lejos;

import lejos.pc.tools.ConsoleViewComms;
import lejos.pc.tools.ConsoleViewerUI;

public class RConsole implements ConsoleViewerUI{
	
	private RConsoleCallback callback;
	
	public void initConnection(String address, int protocol, RConsoleCallback callback){
		this.callback = callback;
		ConsoleViewComms cvc = new ConsoleViewComms(this, false, false);
		boolean status = cvc.connectTo(null, address, protocol);
	}

	@Override
	public void append(String arg0) {
		callback.print(arg0);
	}

	@Override
	public void connectedTo(String arg0, String arg1) {
		callback.clear();
		callback.print("Connected to "+arg0+" "+arg1+"\n");
	}

	@Override
	public void logMessage(String arg0) {
		callback.print(arg0);
	}

	@Override
	public void setStatus(String arg0) {
		System.out.println(arg0);
	}

	@Override
	public void updateLCD(byte[] arg0) {
		System.out.println(arg0);
	}

}
