package org.evolvis.freedroidz.ui;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ResourceBundle;

import lejos.pc.comm.NXTCommException;
import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.evolvis.freedroidz.Starter;
import org.evolvis.freedroidz.lejos.LejosUICallback;
import org.evolvis.freedroidz.lejos.UploadFirmware;

public class LejosUploadFirmwarePanel extends Composite implements LejosUICallback{
    String baseName = "org.evolvis.freedroidz.LejosUploadFirmwarePanel"; 
    final ResourceBundle bundle = ResourceBundle.getBundle( baseName ); 
    
	Shell parent = null;
	
	public LejosUploadFirmwarePanel(Shell parent){
		super(parent, 0);
		this.parent = parent;
//		parent.setText (bundle.getString("LejosUtilsPanel.8")); //$NON-NLS-1$
		
		initGUI();
	}
	
	public void initGUI(){
		setSize(200,300);
		parent.setImages(Starter.getIcons());
		
		setLayout(new MigLayout());
		
		Label headlineLabel = new Label(this, 0);
		headlineLabel.setFont(new Font(Display.getDefault(), new FontData("Sans", 9, SWT.BOLD))); //$NON-NLS-1$
		headlineLabel.setText(bundle.getString("LejosUploadFirmwarePanel.1"));// + //$NON-NLS-1$
//							bundle.getString("LejosUploadFirmwarePanel.2") + //$NON-NLS-1$
//							bundle.getString("LejosUploadFirmwarePanel.3")); //$NON-NLS-1$
		headlineLabel.setLayoutData("span, wrap"); //$NON-NLS-1$
		
		Link link = new Link(this, 0);
		link.setText("<a href=\"www.google.de\">"+bundle.getString("LejosUploadFirmwarePanel.11")+"</a>");
		link.addMouseListener(new MouseListener() {
			 public void mouseDoubleClick(MouseEvent arg0) {}
			 public void mouseUp(MouseEvent arg0) {}
			 public void mouseDown(MouseEvent arg0) {
				 Program.launch(bundle.getString("LejosUploadFirmwarePanel.4"));
			 }
		});
		link.setLayoutData("span, wrap");
		
		Button flashButton = new Button(this, 0);
		flashButton.setText("Flash"); //$NON-NLS-1$
		flashButton.setLayoutData("span, growx, wrap"); //$NON-NLS-1$
		
		flashButton.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent arg0) {	
			}

			public void widgetSelected(SelectionEvent arg0) {
				startUpload();			
			}
			
		});
	}
	
	private void startUpload() {
		FirmwareProgressPanel fpp = new FirmwareProgressPanel();
		UploadFirmware uf = new UploadFirmware();
		uf.registerFirmwareProgressPanel(fpp);
		uf.registerCallback(getThis());
		try {
			uf.flashBrick();	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NXTCommException e) {
			e.printStackTrace();
		}
		fpp.stopPanel();
	}

	@Override
	public void processDone(int status) {
		
		
			if(status == LejosUICallback.OPERATION_SUCCESS){
				MessageBox messageBox = new MessageBox(parent, SWT.ICON_WORKING| SWT.OK);
				messageBox.setText(bundle.getString("LejosUploadFirmwarePanel.7")); //$NON-NLS-1$
				messageBox.setMessage(bundle.getString("LejosUploadFirmwarePanel.8")); //$NON-NLS-1$
				messageBox.open();
				parent.dispose();
			}else{
				MessageBox messageBox = new MessageBox(parent, SWT.ICON_ERROR | SWT.RETRY | SWT.OK);
				messageBox.setText(bundle.getString("LejosUploadFirmwarePanel.9")); //$NON-NLS-1$
				messageBox.setMessage(bundle.getString("LejosUploadFirmwarePanel.10")); //$NON-NLS-1$
				switch(messageBox.open()){
				case SWT.RETRY :
					break;
				case SWT.CANCEL :
					parent.dispose();
					break;
				}
				
			}
		
		
	}
	
	
	private LejosUICallback getThis(){
		return this;
	}
}
