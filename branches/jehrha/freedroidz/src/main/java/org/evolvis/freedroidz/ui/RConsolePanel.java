package org.evolvis.freedroidz.ui;

import lejos.nxt.remote.NXTCommand;
import lejos.pc.comm.NXTComm;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;
import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.evolvis.freedroidz.config.Config;
import org.evolvis.freedroidz.lejos.RConsole;
import org.evolvis.freedroidz.lejos.RConsoleCallback;

public class RConsolePanel extends Composite implements RConsoleCallback{
	
	private Composite parent;
	private Text text;
	
	public RConsolePanel(Composite parent){
		super(parent, 0);
		this.parent = parent;
		initGUI();
	}
	
	public void initGUI(){
		
		
		Layout layout = new MigLayout("", "[49%!,fill][49%!,fill]", "[15%!,fill][78%!,fill]"); 
		setLayout(layout);


		final Combo connectionTypeCombo = new Combo(this, SWT.READ_ONLY);
		connectionTypeCombo.add("Zuletzt verwendeter NXT");
		connectionTypeCombo.add("Mit neuem NXT verbinden");
		connectionTypeCombo.select(0);
		
		
		
		Button connectButton = new Button(this, SWT.BORDER);
		connectButton.setText("Verbinden");
		connectButton.setLayoutData("span, wrap");
		
		text = new Text(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		text.setLayoutData("span, growx, growy");
		text.setEnabled(true);
		
		connectButton.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent arg0) {	
			}

			public void widgetSelected(SelectionEvent arg0) {
				connectToNXT(connectionTypeCombo.getSelectionIndex());
			}
			
		});

		
	}
	
	private void connectToNXT(int selection){
		Config.getInstance().loadSavedInfo();
		RConsole rconsole = new RConsole();
		if(selection == 0){
			NXTInfo nxt = Config.getInstance().getNXTInfo();
			rconsole.initConnection(nxt.deviceAddress, nxt.protocol, this);
		}else{
			NXTInfo nxt = LejosScanPanel.runGUI(NXTCommFactory.ALL_PROTOCOLS);
			rconsole.initConnection(nxt.deviceAddress, nxt.protocol, this);
		}
	}
	
	public void print(final String toprint){
		Display.getDefault().asyncExec(new Runnable(){
			public void run(){
				text.append(toprint);
			}
		});

	}
	
	public void clear(){
		Display.getDefault().asyncExec(new Runnable(){
			public void run(){
				text.setText("");
			}
		});

	}
	
	
	public static void main(String[] args){
		Shell shell = new Shell(Display.getDefault());
		shell.setLayout(new FillLayout());
		
		Composite compsite = new Composite(shell, SWT.BORDER);
		compsite.setLayout(new FillLayout());

		shell.setBounds(10,10,900,300);
		
		final RConsolePanel rcp = new RConsolePanel(compsite);
		shell.open();
		
		Thread thread = new Thread(new Runnable(){
			public void run(){
				for(int i=0;i<20;i++){
					rcp.print(""+i+"\n");
					try {
						Thread.sleep(250);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});
		thread.start();
		while(!shell.isDisposed()){
			Display.getDefault().readAndDispatch();
		}
		
		Display.getDefault().dispose();
	}

}
