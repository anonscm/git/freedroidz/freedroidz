/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.freedroidz;

import java.io.FileNotFoundException;
import java.util.ResourceBundle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.evolvis.freedroidz.lejos.NativeLibraryManager;
import org.evolvis.freedroidz.ui.AboutPanel;
import org.evolvis.freedroidz.ui.LejosUtilsPanel;



/**  
 * Class to start the freedroidz-application
 * 
 * @author Thomas Schmitz, tarent GmbH
 * @author Torsten Wylegala, tarent GmbH
 */
public class Starter {
	
	/*
	 * freedroidz version has to be changed manually 
	 */
	protected final static String freedroidzVersion = "0.3.1";

	/*
	 *  paths to the freedroidz-icon in different sizes
	 */
	protected final static String[] imagePaths = new String[] {
		"/usr/share/icons/hicolor/16x16/apps/freedroidz.png", //$NON-NLS-1$
		"/usr/share/icons/hicolor/22x22/apps/freedroidz.png", //$NON-NLS-1$
		"/usr/share/icons/hicolor/26x26/apps/freedroidz.png", //$NON-NLS-1$
		"/usr/share/icons/hicolor/32x32/apps/freedroidz.png", //$NON-NLS-1$
		"/usr/share/icons/hicolor/40x40/apps/freedroidz.png", //$NON-NLS-1$
		"/usr/share/icons/hicolor/48x48/apps/freedroidz.png", //$NON-NLS-1$
		"/usr/share/icons/hicolor/64x64/apps/freedroidz.png", //$NON-NLS-1$
		"/usr/share/icons/hicolor/128x128/apps/freedroidz.png", //$NON-NLS-1$
	}; 
	
	private static Display display;
	
	protected static Image[] images;

	public static void main(String[] args) {
		
		if(display == null)
			display = new Display();
		
		loadIcons();
		
		NativeLibraryManager.getInstance().checkAndAdd();
		
		try {
			Log.initLogging();
		} catch (FileNotFoundException e2) {
			e2.printStackTrace();
		}
		
	    String baseName = "org.evolvis.freedroidz.starter";
	    final ResourceBundle bundle = ResourceBundle.getBundle( baseName ); 

		Display.setAppName("freedroidz"); //$NON-NLS-1$

		final Shell shell = new Shell(display);
		shell.setText("freedroidz"); //$NON-NLS-1$

		

		final Display display = Display.getDefault();

		shell.setLayout(new FormLayout());
		shell.setImages(images);
	
		ToolBar toolbar = new ToolBar(shell, 0);
		toolbar.setLayout(new FormLayout());
		FormData toolbarData = new FormData();
		toolbarData.top = new FormAttachment(0,0);
		toolbarData.bottom = new FormAttachment(10,0);
		toolbarData.left = new FormAttachment(0,0);
		toolbarData.right = new FormAttachment(100,0);
		toolbar.setLayoutData(toolbarData);

		Image Info = new Image(display, Starter.class.getResourceAsStream("/org/evolvis/freedroidz/gfx/about.png")); //$NON-NLS-1$
		Image imageQuit = new Image(display, Starter.class.getResourceAsStream("/org/evolvis/freedroidz/gfx/system-log-out.png")); //$NON-NLS-1$
  
	    ToolItem toolItem4 = new ToolItem(toolbar, SWT.PUSH);
	    toolItem4.setImage(Info);
	    toolItem4.setToolTipText(bundle.getString("Starter.20")); //$NON-NLS-1$
	    ToolItem toolItem5 = new ToolItem(toolbar, SWT.PUSH);
	    toolItem5.setToolTipText(bundle.getString("Starter.21")); //$NON-NLS-1$
	    toolItem5.setImage(imageQuit);

	    //about listener
	    toolItem4.addListener(SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				Shell dialog = new Shell (shell);
				dialog.setText (bundle.getString("Starter.25")); //$NON-NLS-1$
				dialog.setLayout(new FillLayout());
				dialog.setImages(getIcons());
				
				@SuppressWarnings("unused")
				AboutPanel aboutPanel = new AboutPanel(dialog);

				dialog.pack();
				dialog.open ();
				while (!shell.isDisposed ()) {
					if (!display.readAndDispatch ()) display.sleep ();
				}
			}
		});
	    
	    //quit listener
	    toolItem5.addListener(SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				quitApplication();
			}
		});
		
		TabFolder tabFolder = new TabFolder(shell, 0);
		tabFolder.setLayout(new FillLayout());
		FormData tabLayoutData = new FormData();
		tabLayoutData.top = new FormAttachment(toolbar, 0);
		tabLayoutData.bottom = new FormAttachment(100,-5);
		tabLayoutData.left = new FormAttachment(0, 0);
		tabLayoutData.right = new FormAttachment(100, 0);
		tabFolder.setLayoutData(tabLayoutData);
		
		TabItem lejosUtilsPanelTabItem = new TabItem(tabFolder, 0);
		final LejosUtilsPanel lejosUtilsPanel = new LejosUtilsPanel(tabFolder);
		lejosUtilsPanelTabItem.setControl(lejosUtilsPanel);
		lejosUtilsPanelTabItem.setText(bundle.getString("Starter.26")); //$NON-NLS-1$

		Menu menuBar = new Menu(shell, SWT.BAR);
		shell.setMenuBar(menuBar);

		MenuItem fileItem =  new MenuItem(menuBar, SWT.CASCADE);
		fileItem.setText(bundle.getString("Starter.28")); //$NON-NLS-1$

		Menu submenu = new Menu (shell, SWT.DROP_DOWN);
		fileItem.setMenu (submenu);


		MenuItem aboutItem = new MenuItem (submenu, SWT.PUSH);
		aboutItem.setText(bundle.getString("Starter.35")); //$NON-NLS-1$
		aboutItem.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				Shell dialog = new Shell (shell);
				dialog.setText (bundle.getString("Starter.36")); //$NON-NLS-1$
				dialog.setLayout(new FillLayout());
				dialog.setImages(getIcons());
				
				@SuppressWarnings("unused")
				AboutPanel aboutPanel = new AboutPanel(dialog);

				dialog.pack();
				dialog.open ();
				while (!shell.isDisposed ()) {
					if (!display.readAndDispatch ()) display.sleep ();
				}
			}
		});

		MenuItem quitItem = new MenuItem (submenu, SWT.PUSH);
		quitItem.setText(bundle.getString("Starter.37")); //$NON-NLS-1$
		quitItem.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				quitApplication();
			}
		});

		shell.pack();
		shell.open();
		
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		quitApplication();
	}

	protected static void quitApplication() {
		System.exit(0);
	}
	
	/**
	 * 
	 * @return freedroidz version
	 */
	public static String getVersion(){
		return freedroidzVersion;
	}
	
	public static void loadIcons(){
		 images = new Image[] {
					new Image(display,Starter.class.getResourceAsStream("/org/evolvis/freedroidz/gfx/icons/16x16/freedroidz.png")),
					new Image(display,Starter.class.getResourceAsStream("/org/evolvis/freedroidz/gfx/icons/22x22/freedroidz.png")),
					new Image(display,Starter.class.getResourceAsStream("/org/evolvis/freedroidz/gfx/icons/26x26/freedroidz.png")),
					new Image(display,Starter.class.getResourceAsStream("/org/evolvis/freedroidz/gfx/icons/32x32/freedroidz.png")),
					new Image(display,Starter.class.getResourceAsStream("/org/evolvis/freedroidz/gfx/icons/40x40/freedroidz.png")),
					new Image(display,Starter.class.getResourceAsStream("/org/evolvis/freedroidz/gfx/icons/48x48/freedroidz.png")),
					new Image(display,Starter.class.getResourceAsStream("/org/evolvis/freedroidz/gfx/icons/64x64/freedroidz.png")),
					new Image(display,Starter.class.getResourceAsStream("/org/evolvis/freedroidz/gfx/icons/128x128/freedroidz.png"))
		}; 
	}
	
	/**
	 * 
	 * @return icons in different sizes
	 */
	public static Image[] getIcons(){
		if(images == null)
			loadIcons();
		return images;
	}
}
