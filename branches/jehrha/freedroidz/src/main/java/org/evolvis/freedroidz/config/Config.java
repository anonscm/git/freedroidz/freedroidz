package org.evolvis.freedroidz.config;

import java.io.IOException;

import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;

import org.evolvis.freedroidz.lejos.Configuration;

public class Config {
	public static final String FREEDROIDZ_UPLOAD_NXT_NAME = "freedroidz.upload.nxtName";

	public static final String FREEDROIDZ_UPLOAD_ADDRESS = "freedroidz.upload.address";

	public static final String FREEDROIDZ_UPLOAD_CONNECTION_TYPE = "freedroidz.upload.connectionType";
	public static final String FREEDROIDZ_UPLOAD_FILENAME = "freedroidz.upload.filename";
	
	public static Config instance;
	
	private Configuration config = Configuration.getInstance();
	
	private NXTInfo nxtInfo;
	
	/**
	 * Saves current configuration
	 */
	public void storeConfig(){
		try {
			config.store();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Loads settings from file (~/.freedroidz/configuration.properties)
	 * 
	 * @return returns a NXTInfo file made up from configuration file 
	 */
	public NXTInfo loadSavedInfo() {
		String name = config.getString(FREEDROIDZ_UPLOAD_NXT_NAME, null);
		String address = config.getString(FREEDROIDZ_UPLOAD_ADDRESS, null);
		int protocol = config.getInt(FREEDROIDZ_UPLOAD_CONNECTION_TYPE,
				NXTCommFactory.USB);
		if (address != null) {
			nxtInfo = new NXTInfo(protocol, name, address);
			nxtInfo.protocol = protocol;
		}else{
			nxtInfo=null;
		}
		return nxtInfo;
	}
	
	/**
	 * 
	 * @return NXTInfo of current NXT
	 */
	public NXTInfo getNXTInfo(){
		return loadSavedInfo();
	}
	
	/**
	 * 
	 * @param info nxt to save
	 */
	public void setNxtInfo(NXTInfo info){
		nxtInfo=info;		
		config.setValue(FREEDROIDZ_UPLOAD_ADDRESS, info.deviceAddress);
		config.setValue(FREEDROIDZ_UPLOAD_CONNECTION_TYPE, info.protocol);
		config.setValue(FREEDROIDZ_UPLOAD_NXT_NAME, info.name);
	}
	
	/**
	 * 
	 * @return Configuration Class
	 */
	public Configuration getConfig(){
		return config;
	}
	
	public static Config getInstance(){
		if(instance == null)
			instance = new Config();
		return instance;
	}
}
