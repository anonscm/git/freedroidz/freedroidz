package org.evolvis.freedroidz.lejos;

public interface LejosFunction {

	public void registerCallback(LejosUICallback callback);
}
