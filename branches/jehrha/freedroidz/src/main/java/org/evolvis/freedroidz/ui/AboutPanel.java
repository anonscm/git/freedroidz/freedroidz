/**
 * 
 */
package org.evolvis.freedroidz.ui;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.evolvis.freedroidz.Starter;

/**
 * 
 * This is a simple SWT-Panel which shows some information (like version-number).
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class AboutPanel extends Composite {
	
	protected Shell parent;
	
	/** 
	 * Creates a new instance.
	 * 
	 * @param parent the parent container
	 */
	public AboutPanel(Shell parent) {
		super(parent, SWT.BORDER);
		this.parent = parent;
		createGUI();
	}
	
	/**
	 * Creates the GUI components and lays them down on this Composite, using
	 * the MigLayout.
	 */
	private void createGUI() {
		setLayout(new MigLayout("fillx, wrap 1", "[] [grow, fill]"));
		
		Label versionLabel = new Label(this, SWT.NONE);
		
		versionLabel.setText("freedroidz "+Starter.getVersion());
		
		Image aboutImage = new Image(getDisplay(), this.getClass().getResourceAsStream("/org/evolvis/freedroidz/gfx/aboutfreedroidz.png")); //$NON-NLS-1$
		Label imageLabel = new Label(this, SWT.NONE);
		imageLabel.setImage(aboutImage);
		
		Link link = new Link(this, SWT.NONE);
	    String text = "For more information visit <A>freedroidz.org</A>";
	    link.setText(text);
		link.addMouseListener(new MouseListener() {
			 public void mouseDoubleClick(MouseEvent arg0) {}
			 public void mouseUp(MouseEvent arg0) {}
			 public void mouseDown(MouseEvent arg0) {
				 Program.launch("http://freedroidz.org");
			 }
		});
		
		parent.setImages(Starter.getIcons());

		Button closeButton = new Button(this, SWT.NONE);
		closeButton.setText("Close");
		closeButton.addSelectionListener(new SelectionListener() {

			/**
			 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			/**
			 * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			public void widgetSelected(SelectionEvent arg0) {

				parent.close();
				
			}
			
		});
	}	
}
