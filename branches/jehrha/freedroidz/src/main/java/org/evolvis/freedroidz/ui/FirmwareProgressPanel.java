package org.evolvis.freedroidz.ui;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.evolvis.freedroidz.Starter;

public class FirmwareProgressPanel {
	
	private ProgressBar bar;
	private Label actionLabel;
	private Shell shell;
	
	public FirmwareProgressPanel(){
		initPanel();
	}
	
	private void initPanel(){
		Display display = Display.getDefault();
		shell = new Shell(display);
		shell.setImages(Starter.getIcons());
		
		shell.setLayout(new MigLayout("","[100%, fill]",""));
		shell.setBounds(display.getBounds().width/2-150, display.getBounds().height/2-40, 300, 80);
		shell.setSize(300,80);
		
		shell.setText("Firmware übertragen");
		
		actionLabel = new Label(shell, 0);
//		headlineLabel.setFont(new Font(Display.getDefault(), new FontData("Sans", 9, SWT.BOLD))); //$NON-NLS-1$
		actionLabel.setLayoutData("span, growx, wrap"); //$NON-NLS-1$
		
		bar = new ProgressBar(shell, SWT.SMOOTH);
		bar.setLayoutData("span, growx, wrap"); //$NON-NLS-1$
		bar.setMaximum(101);
		

		shell.open();
	}
	
	public void updateProgressbar(int percent){
		this.bar.setSelection(percent);
	}
	
	public void updateProgressbar(String action){
		this.actionLabel.setText(action);
	}
	
	public void stopPanel(){
		shell.dispose();
	}
	
	public static void main(String[] args) {
		FirmwareProgressPanel fpp = new FirmwareProgressPanel();
		fpp.updateProgressbar("Schrieben");
		fpp.updateProgressbar(50);
		while (true) {
			Display.getDefault().readAndDispatch();
		}
	}
}
