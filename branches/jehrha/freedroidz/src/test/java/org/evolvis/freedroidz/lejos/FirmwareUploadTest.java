package org.evolvis.freedroidz.lejos;

import junit.framework.TestCase;

public class FirmwareUploadTest extends TestCase{

	public void testCreateFirmwareImage() throws Exception {
		UploadFirmware uf = new UploadFirmware();
		
		byte[] memoryImage = uf.createFirmwareImage(
				UploadFirmware.class.getResourceAsStream("/org/evolvis/freedroidz/lejos/lejos_nxt_rom.bin") ,
				UploadFirmware.class.getResourceAsStream("/org/evolvis/freedroidz/lejos/StartUpText.bin"));
		
		assertTrue(memoryImage.length == 90112);
	}
}
