package org.evolvis.freedroidz.lejos;



import java.io.File;
import java.io.IOException;
import java.util.Collection;

import javax.tools.JavaCompiler;

import junit.framework.TestCase;

import org.apache.commons.io.FileUtils;

public class CompileLinkFileTest extends TestCase {
	
	private CompileLinkFile compileLinkFile;
	private File testdir;
	private File javacOutputDir;
	private File javacInputDir;
	private File testSourceArchive;
	private String mainClassFQN;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		this.compileLinkFile = new CompileLinkFile();

		/* use classloader to locate our testdata */
		this.testdir = new File(this.getClass().getResource("compilelinkfiletest").toURI());		
		assertTrue(testdir.exists());
		this.testSourceArchive = new File(testdir,"testsource.zip");
		assertTrue(testSourceArchive.canRead());
		
		/* generate a temporary dir for keeping the source files */
		this.javacInputDir = CompileLinkFile.createTempDir(new File(System.getProperty("java.io.tmpdir")),this.getName()+"_input");
		
		/* generate a temporary dir for keeping the class files */
		this.javacOutputDir = CompileLinkFile.createTempDir(new File(System.getProperty("java.io.tmpdir")),this.getName()+"_output");
		
		/* unzip the test source */
		
		LejosTestUtils.unzipArchive(testSourceArchive, javacInputDir);
		
		/* fqn of the main class we want to link */
		this.mainClassFQN="ludeg.RemoteControl";
		 
	}
	
	@Override
	protected void tearDown() throws Exception {
		FileUtils.deleteDirectory(this.javacOutputDir);
		FileUtils.deleteDirectory(this.javacInputDir);
		super.tearDown();
	}
	public void testClassesJarAvailable(){
		File classesJar = this.compileLinkFile.getClassesJar();
		classesJar.delete();
		this.compileLinkFile.createClassesJar();
		assertNotNull(classesJar);
		assertTrue(classesJar.canRead());
				
	}
	
	public void testJavaCompilerApiAvailable(){
		JavaCompiler javac = javax.tools.ToolProvider.getSystemJavaCompiler();
		assertNotNull(javac);
	}
	
	public void testSearchSourceFiles() throws IOException {
		Collection<File> files = compileLinkFile.searchSourceFiles(javacInputDir);
		assertEquals(2,files.size());
		//Iterator<File> it = files.iterator();
		//assertEquals("Testor.java",it.next().getName());
		//assertEquals("ButtonTest.java",it.next().getName());		
	}

	public void testCompile() throws IOException {
		//make double sure there are no left-overs from a previous run.
		assertEquals(0,javacOutputDir.list().length); 
		Collection<File> files = compileLinkFile.searchSourceFiles(javacInputDir);
		boolean result = compileLinkFile.compile(files,javacOutputDir);
		assertTrue(result);
		Collection<File> classFiles = FileUtils.listFiles(javacOutputDir,null,true);
//		assertEquals(7,classFiles.size());
		
	} 
	
	public void testExternalProcessCompile() throws IOException{
		ExternalProcessCompileStrategy eps = new ExternalProcessCompileStrategy(compileLinkFile);
		Collection<File> files = compileLinkFile.searchSourceFiles(javacInputDir);
		assertTrue(files.size() > 0);
		assertTrue(eps.compile(files, javacOutputDir, System.out, null));
	}
	
	public void testOldApiCompileStrategy() throws Exception{
		
		OldApiCompileStrategy oacs = new OldApiCompileStrategy(compileLinkFile);
		Collection<File> files = compileLinkFile.searchSourceFiles(javacInputDir);
		assertTrue(files.size() > 0);
		assertTrue(oacs.compile(files, javacOutputDir, System.out, null));
	}

	public void testLink() throws IOException {
		testCompile();
		File nxjFile=new File(this.javacOutputDir,"test.nxj");
		assertTrue( compileLinkFile.link(mainClassFQN, javacOutputDir.getAbsolutePath(), nxjFile));
		assertTrue(nxjFile.canRead());
		
	}

	public void testGetErrors() {
//		fail("Not yet implemented");
	}

}
