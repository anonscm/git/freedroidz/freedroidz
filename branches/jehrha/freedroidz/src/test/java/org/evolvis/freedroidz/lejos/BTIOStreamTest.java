package org.evolvis.freedroidz.lejos;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import junit.framework.TestCase;
import lejos.nxt.remote.NXTCommand;
import lejos.pc.comm.NXTComm;
import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;

import org.evolvis.freedroidz.lejos.NXTMock.NXTCommMock;
import org.evolvis.freedroidz.lejos.NXTMock.NXTCommandMock;

public class BTIOStreamTest extends TestCase {
	

	
	public void BtConnection() throws Throwable {

		NXTComm comm = new NXTCommMock();
		String devString = System.getProperty("lejos.nxt.address");
		System.err.println("comm created, connecting to " + devString);

		/*
		 * wenn man die Addresse gerade nicht auf Tasche hat, kann man auch
		 * einfach suchen: NXTInfo[] nxtInfo =
		 * comm.search("NXT",NXTCommFactory.BLUETOOTH); for (int i = 0; i <
		 * nxtInfo.length; i++) {
		 * System.out.println(nxtInfo[i].btDeviceAddress); }
		 * assert(nxtInfo.length>0);
		 */
		
		NXTCommand command = new NXTCommandMock();
		command.setNXTComm(comm);
		
//		Thread thread = new Thread(){
//			public void run(){
//				
//			}
//		};
//		
//		((NXTCommandMock)command).runNXTEmulation(thread);

		NXTInfo nxtInfo = new NXTInfo(NXTCommFactory.BLUETOOTH, "NXT", devString);
		assertTrue(comm.open(nxtInfo));
		System.err.println("Connection established, opening IO-Streams");
		InputStream in = comm.getInputStream();
		DataInputStream din = new DataInputStream(in);
		OutputStream out = comm.getOutputStream();
		DataOutputStream dout = new DataOutputStream(out);
		System.out.println("sending 42");

		dout.writeInt(42);
		dout.flush();
		int v = din.readInt();
		System.err.println("Got " + v);
		assertEquals(-42,v);
		din.close();
		dout.close();
	}
	
	public void testObjectSend() throws NXTCommException, IOException{
		NXTComm comm = new NXTCommMock();
		String devString = System.getProperty("lejos.nxt.address");

		NXTInfo nxtInfo = new NXTInfo(NXTCommFactory.BLUETOOTH, "NXT", devString);
		assertTrue(comm.open(nxtInfo));
		
		InputStream in = comm.getInputStream();
		DataInputStream din = new DataInputStream(in);
		OutputStream out = comm.getOutputStream();
		DataOutputStream dout = new DataOutputStream(out);
		
		assertNotNull(in);
		assertNotNull(out);
		
		byte[] tmp = new byte[3];
		
		tmp[0] = (byte) 100;
		tmp[1] = (byte) 200;
		tmp[2] = (byte) 300;
		
		dout.write(tmp);
		dout.flush();
		
	}
	
}