package de.tarent.android.freedroidz;

public class MotorControl {

	
	private int PortA = 0;
	private int PortB = 1;
	private int PortC = 2;
	
	/** erwartet geschwindigkeiten + richtungen für A, B und C, 
	 *  	-power wird als "true" angenommen, sonst 0 als Speed übergeben!
	 * 		-dir == true, -> negative Geschwindigkeiten benutzen
	 */
	public void setMotorspeedABC(int speedA, int speedB, int speedC) {
		
		int[] commands = new int[3];
		boolean changedA = motorA(commands, speedA);
		boolean changedB = motorB(commands, speedB);
		boolean changedC = motorC(commands, speedC);
		
		if((changedA || changedB || changedC)){
			Freedroidz.sendMessage(commands);
		}
		
	}
	
	
	/** erwartet geschwindigkeiten + richtungen für A und C, 
	 *  	-power wird als "true" angenommen, sonst 0 als Speed übergeben!
	 * 		-dir == true, -> negative Geschwindigkeiten benutzen
	 */
	public void setMotorspeedAC(int speedA, int speedC) {
		
		int[] commands = new int[3];
		boolean changedA = motorA(commands, speedA);
		boolean changedC = motorC(commands, speedC);
		// B bleibt
		motorB(commands, lastSpeedB);
		
		if((changedA || changedC)){
			Freedroidz.sendMessage(commands);
		}
		
	}
	
	/** erwartet geschwindigkeiten + richtungen für B 
	 *  	-power wird als "true" angenommen, sonst 0 als Speed übergeben!
	 * 		-dir == true, -> negative Geschwindigkeiten benutzen
	 */
	public void setMotorSpeedB(int speedB){
		int[] commands = new int[3];
		// A und C bleiben
		motorA(commands, lastSpeedA);
		motorC(commands, lastSpeedC);
		
		// B wird geupdated:
		boolean changedB = motorB(commands, speedB);
		
		// falls B sich wirklich veraendert hat:
		if(changedB) {
			Freedroidz.sendMessage(commands);
		}
	}
	
	private int lastSpeedA = 0;
	private int lastSpeedB = 0;
	private int lastSpeedC = 0;
	
	/**
	 * sets Speed of Motor A in commands, returns if value changed.
	 * @param commands
	 * @param speedC
	 * @return
	 */
	private boolean motorA(int[] commands, int speedA) {
		commands[PortA] = speedA;
		if(speedA != lastSpeedA) {
			
			lastSpeedA = speedA;
			return true;
		} 
		return false;
	}
	
	/**
	 * sets Speed of Motor B in commands, returns if value changed.
	 * @param commands
	 * @param speedC
	 * @return
	 */
	private boolean motorB(int[] commands, int speedB) {
		commands[PortB] = speedB;
		if(speedB != lastSpeedB) {
			lastSpeedB = speedB;
			return true;
		} 
		return false;
	}
	
	/**
	 * sets Speed of Motor C in commands, returns if value changed.
	 * @param commands
	 * @param speedC
	 * @return
	 */
	private boolean motorC(int[] commands, int speedC) {
		commands[PortC] = speedC;
		if(speedC != lastSpeedC) {
			
			lastSpeedC = speedC;
			return true;
		} 
		return false;
	}

	
}
