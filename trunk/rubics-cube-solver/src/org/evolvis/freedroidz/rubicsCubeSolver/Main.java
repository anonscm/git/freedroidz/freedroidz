package org.evolvis.freedroidz.rubicsCubeSolver;

import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.addon.ColorSensor;


/*
 * Author: Wolf-Garrit Scholle
 * 15.07.2010 - 13.07.2010
 * 
 * This is a Rubik's Cube (Zauberwürfel) solver.
 * The program uses the Lego Mindstorms NXT 2.0.
 * It has to be build to a Tilted Twister.
 * If you don't know how to build a TT
 * then go to http://lmgtfy.com/.
 */


public class Main {

	private Solver solver;
	private Scanner scanner;
	
	
	public Main() {
		solver = new Solver(new Robot(new CubeFactory(), new MotorWrapper(Motor.B),new MotorWrapper(Motor.C)));
		Hardware hw = new Hardware();
		final ColorSensor sensor = new ColorSensor(SensorPort.S3);
		hw.setColorSensor(new IColorSensor() {
			
			@Override
			public int getColorNumber() {

				return sensor.getColorNumber();
			}
		});
		
		hw.setMotorA(new MotorWrapper(Motor.A));
		hw.setMotorB(new MotorWrapper(Motor.B));
		hw.setMotorC(new MotorWrapper(Motor.C));
		
		scanner = new Scanner(solver.robot,hw);

	}
	
	public static void main(String[] args) {

		new Main().go();

	}
	
	public void go()
	{
		solver.robot.waitForCube();
				
		scanner.scanCube();
	
		solver.cubieColorSort();
		
		solver.firstCrossAlgorithm();
		solver.firstCornerAlgorithm();
		solver.secondLayerAlgorithm();
		solver.secondCrossAlgorithm();
		solver.secondEdgeAlgorithm();
		solver.secondCornerAlgorithm();
		solver.secondCornerFinish();
	}
	
	public void wait(int i)
	{
		try
		{
			Thread.sleep(i);
		}catch(InterruptedException e)
		{}
	}

}
