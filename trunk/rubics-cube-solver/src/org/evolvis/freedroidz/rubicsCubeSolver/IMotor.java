package org.evolvis.freedroidz.rubicsCubeSolver;

public interface IMotor {

	public abstract void setPower(int i);

	public abstract void rotate(int i);

}