package org.evolvis.freedroidz.rubicsCubeSolver;

import lejos.nxt.Sound;


/*
 * The Cube-Class to set the cubies to know, where which cubie is
 */

public class Cube {
	
	// 4 MidCubies
	public class MidCubie {
		
		
		public String Color1; // RED, GREEN, YELLOW, BLUE, ORANGE, WHITE
		public String ColorPosition; // UP, DOWN, LEFT, RIGHT, FRONT, BACK
		
		MidCubie() // Constructor
		{
			Color1 = "null";
			ColorPosition = "null";
		}
		
		boolean hasOrientation(Direction direction){
			return ColorPosition.equals(direction.toString());
		}
		
		boolean colorChecker(Color color)
		{
			return Color1.equals(color.toString());
		}

		public void MidPositionChange(int c) // (c stands for Case) changes the
												// position of the colors, so
												// the robot knows in which
												// positions the cubies are and
												// how it has to move them
		{
			switch (c) {
			// If the arm is turning the cube
			case 1:
				if (ColorPosition.equals("UP")) {
					ColorPosition = "FRONT";
				} else if (ColorPosition.equals("FRONT")) {
					ColorPosition = "DOWN";
				} else if (ColorPosition.equals("DOWN")) {
					ColorPosition = "BACK";
				} else if (ColorPosition.equals("BACK")) {
					ColorPosition = "UP";
				}
				break;

			// If the cube is turning anti-clockwise
			case 2:
				if (ColorPosition.equals("LEFT")) {
					ColorPosition = "FRONT";
				} else if (ColorPosition.equals("FRONT")) {
					ColorPosition = "RIGHT";
				} else if (ColorPosition.equals("RIGHT")) {
					ColorPosition = "BACK";
				} else if (ColorPosition.equals("BACK")) {
					ColorPosition = "LEFT";
				}
				break;

			// If the cube is turning clockwise
			case 3:
				if (ColorPosition.equals("LEFT")) {
					ColorPosition = "BACK";
				} else if (ColorPosition.equals("BACK")) {
					ColorPosition = "RIGHT";
				} else if (ColorPosition.equals("RIGHT")) {
					ColorPosition = "FRONT";
				} else if (ColorPosition.equals("FRONT")) {
					ColorPosition = "LEFT";
				}

				break;

			// If there is a wrong parameter
			default:
				Sound.playTone(1760, 2000, 100);
				System.out.println("Fehler");
				System.out.println("Roboter wird ausgeschaltet.");

				sleep();
				System.exit(0);
			}
		}
	}

	// 12 EdgeCubies
	public class EdgeCubie {
		public String Color1;
		public String Color2;

		public String Color1Position;
		public String Color2Position;

		public boolean hasOrientation(Direction direction1, Direction direction2) {
			return Color1Position.equals(direction1.toString())
					&& Color2Position.equals(direction2.toString());
		}
		
		public boolean colorCheck(Color color1, Color color2)
		{
			return Color1.equals(color1.toString()) && Color2
			.equals(color2.toString());
		}
		
		public boolean color1Orientation(Direction direction){
			return Color1Position.equals(direction.toString());
		}
		
		public boolean color2Orientation(Direction direction){
			return Color2Position.equals(direction.toString());
		}
		
		EdgeCubie() {
			Color1 = "null";
			Color2 = "null";
			Color1Position = "null";
			Color2Position = "null";
		}

		//Sorts the color of the cubie in alphabetical order
		public void colorSort()
		{
			//If compare is positive, color1 is after color2
			int compare = Color1.charAt(0) - Color2.charAt(0);
			String bufferColor = "null";
			String bufferPosition = "null";
			
			if(compare > 0)
			{
				bufferColor = Color1;
				Color1 = Color2;
				Color2 = bufferColor;
				
				bufferPosition = Color1Position;
				Color1Position = Color2Position;
				Color2Position = bufferPosition;
				
			}else if(compare == 0)
			{
				System.out.println("Farben sind gleich");
			}
		}
		
		public void EdgePositionChange(int c) {
			switch (c) {
			// If the arm twists
			case 1:
				if (Color1Position.equals("UP"))
					Color1Position = "FRONT";
				else if (Color1Position.equals("FRONT"))
					Color1Position = "DOWN";
				else if (Color1Position.equals("DOWN"))
					Color1Position = "BACK";
				else if (Color1Position.equals("BACK"))
					Color1Position = "UP";

				if (Color2Position.equals("UP"))
					Color2Position = "FRONT";
				else if (Color2Position.equals("FRONT"))
					Color2Position = "DOWN";
				else if (Color2Position.equals("DOWN"))
					Color2Position = "BACK";
				else if (Color2Position.equals("BACK"))
					Color2Position = "UP";

				break;

			// Anti-clockwise
			case 2:
				if (Color1Position.equals("FRONT"))
					Color1Position = "RIGHT";
				else if (Color1Position.equals("RIGHT"))
					Color1Position = "BACK";
				else if (Color1Position.equals("BACK"))
					Color1Position = "LEFT";
				else if (Color1Position.equals("LEFT"))
					Color1Position = "FRONT";

				if (Color2Position.equals("FRONT"))
					Color2Position = "RIGHT";
				else if (Color2Position.equals("RIGHT"))
					Color2Position = "BACK";
				else if (Color2Position.equals("BACK"))
					Color2Position = "LEFT";
				else if (Color2Position.equals("LEFT"))
					Color2Position = "FRONT";

				break;

			// Clockwise
			case 3:
				if (Color1Position.equals("FRONT"))
					Color1Position = "LEFT";
				else if (Color1Position.equals("LEFT"))
					Color1Position = "BACK";
				else if (Color1Position.equals("BACK"))
					Color1Position = "RIGHT";
				else if (Color1Position.equals("RIGHT"))
					Color1Position = "FRONT";

				if (Color2Position.equals("FRONT"))
					Color2Position = "LEFT";
				else if (Color2Position.equals("LEFT"))
					Color2Position = "BACK";
				else if (Color2Position.equals("BACK"))
					Color2Position = "RIGHT";
				else if (Color2Position.equals("RIGHT"))
					Color2Position = "FRONT";

				break;

			// If the arm holds the cube and the appliance twists it
			case 4:
				if (Color1Position.equals("FRONT")
						&& Color2Position.equals("DOWN"))
					Color1Position = "LEFT";
				else if (Color1Position.equals("LEFT")
						&& Color2Position.equals("DOWN"))
					Color1Position = "BACK";
				else if (Color1Position.equals("BACK")
						&& Color2Position.equals("DOWN"))
					Color1Position = "RIGHT";
				else if (Color1Position.equals("RIGHT")
						&& Color2Position.equals("DOWN"))
					Color1Position = "FRONT";

				if (Color2Position.equals("FRONT")
						&& Color1Position.equals("DOWN"))
					Color2Position = "LEFT";
				else if (Color2Position.equals("LEFT")
						&& Color1Position.equals("DOWN"))
					Color2Position = "BACK";
				else if (Color2Position.equals("BACK")
						&& Color1Position.equals("DOWN"))
					Color2Position = "RIGHT";
				else if (Color2Position.equals("RIGHT")
						&& Color1Position.equals("DOWN"))
					Color2Position = "FRONT";

				break;

			// Same like 4 but invert
			case 5:
				if (Color1Position.equals("FRONT")
						&& Color2Position.equals("DOWN"))
					Color1Position = "RIGHT";
				else if (Color1Position.equals("RIGHT")
						&& Color2Position.equals("DOWN"))
					Color1Position = "BACK";
				else if (Color1Position.equals("BACK")
						&& Color2Position.equals("DOWN"))
					Color1Position = "LEFT";
				else if (Color1Position.equals("LEFT")
						&& Color2Position.equals("DOWN"))
					Color1Position = "FRONT";

				if (Color2Position.equals("FRONT")
						&& Color1Position.equals("DOWN"))
					Color2Position = "RIGHT";
				else if (Color2Position.equals("RIGHT")
						&& Color1Position.equals("DOWN"))
					Color2Position = "BACK";
				else if (Color2Position.equals("BACK")
						&& Color1Position.equals("DOWN"))
					Color2Position = "LEFT";
				else if (Color2Position.equals("LEFT")
						&& Color1Position.equals("DOWN"))
					Color2Position = "FRONT";

				break;

			// Same default like in MidCubie
			default:
				Sound.playTone(1760, 2000, 100);
				System.out.println("Fehler");
				System.out.println("Roboter wird ausgeschaltet.");
				sleep();
				System.exit(0);
			}
		}
	}

	// Only different, just one color and one colorposition more

	public class CornerCubie {

		public String[] getColors() {

			return new String[] { Color1, Color2, Color3 };
		}

		String Color1;
		String Color2;
		String Color3;

		String Color1Position;
		String Color2Position;
		String Color3Position;
		
		/**
		 * @return true iff one of the three sides is of color
		 *         <code>color</code> and facing direction
		 *         <code>direction</code>.
		 */
		public boolean hasOrientation(Direction direction1, Direction direction2,	Direction direction3) {
			return Color1Position.equals(direction1.toString())
					&& Color2Position.equals(direction2.toString())
					&&Color3Position.equals(direction3.toString());
		}
		
		public boolean colorCheck(Color color1, Color color2, Color color3)
		{
			return Color1.equals(color1.toString()) && Color2
			.equals(color2.toString()) && Color3.equals(color3.toString());
		}

		//sorts the color in alphabetical order
		public void colorSort()
		{
			//If compare is positiv, color1 is after color2
			int compare = Color1.charAt(0) - Color2.charAt(0);
			String bufferColor = "null";
			String bufferPosition = "null";
			
			//the if functions switch  the two colors which are compared and also switch their positions
			if(compare > 0)
			{
				bufferColor = Color1;
				Color1 = Color2;
				Color2 = bufferColor;
				
				bufferPosition = Color1Position;
				Color1Position = Color2Position;
				Color2Position = bufferPosition;
				
			}else if(compare == 0)
			{
				System.out.println("Farben sind gleich");
			}
			
			compare = Color2.charAt(0) - Color3.charAt(0);
			
			if(compare > 0)
			{
				bufferColor = Color2;
				Color2 = Color3;
				Color3 = bufferColor;
				
				bufferPosition = Color2Position;
				Color2Position = Color3Position;
				Color3Position = bufferPosition;
				
			}else if(compare == 0)
			{
				System.out.println("Farben sind gleich");
			}
			
			compare = Color1.charAt(0) - Color2.charAt(0);
			
			if(compare > 0)
			{
				bufferColor = Color1;
				Color1 = Color2;
				Color2 = bufferColor;
				
				bufferPosition = Color1Position;
				Color1Position = Color2Position;
				Color2Position = bufferPosition;
				
			}else if(compare == 0)
			{
				System.out.println("Farben sind gleich");
			}
		}
		
		CornerCubie(String color1, String color2, String color3) {
			Color1 = color1;
			Color2 = color2;
			Color3 = color3;

		}

		CornerCubie() {
			Color1 = "null";
			Color2 = "null";
			Color3 = "null";
			Color1Position = "null";
			Color2Position = "null";
			Color3Position = "null";
		}

		public void CornerPositionChange(int c) {
			switch (c) {

			case 1:
				if (Color1Position.equals("UP"))
					Color1Position = "FRONT";
				else if (Color1Position.equals("FRONT"))
					Color1Position = "DOWN";
				else if (Color1Position.equals("DOWN"))
					Color1Position = "BACK";
				else if (Color1Position.equals("BACK"))
					Color1Position = "UP";

				if (Color2Position.equals("UP"))
					Color2Position = "FRONT";
				else if (Color2Position.equals("FRONT"))
					Color2Position = "DOWN";
				else if (Color2Position.equals("DOWN"))
					Color2Position = "BACK";
				else if (Color2Position.equals("BACK"))
					Color2Position = "UP";

				if (Color3Position.equals("UP"))
					Color3Position = "FRONT";
				else if (Color3Position.equals("FRONT"))
					Color3Position = "DOWN";
				else if (Color3Position.equals("DOWN"))
					Color3Position = "BACK";
				else if (Color3Position.equals("BACK"))
					Color3Position = "UP";

				break;

			case 2:
				if (Color1Position.equals("FRONT"))
					Color1Position = "RIGHT";
				else if (Color1Position.equals("RIGHT"))
					Color1Position = "BACK";
				else if (Color1Position.equals("BACK"))
					Color1Position = "LEFT";
				else if (Color1Position.equals("LEFT"))
					Color1Position = "FRONT";

				if (Color2Position.equals("FRONT"))
					Color2Position = "RIGHT";
				else if (Color2Position.equals("RIGHT"))
					Color2Position = "BACK";
				else if (Color2Position.equals("BACK"))
					Color2Position = "LEFT";
				else if (Color2Position.equals("LEFT"))
					Color2Position = "FRONT";

				if (Color3Position.equals("FRONT"))
					Color3Position = "RIGHT";
				else if (Color3Position.equals("RIGHT"))
					Color3Position = "BACK";
				else if (Color3Position.equals("BACK"))
					Color3Position = "LEFT";
				else if (Color3Position.equals("LEFT"))
					Color3Position = "FRONT";

				break;

			case 3:
				if (Color1Position.equals("FRONT"))
					Color1Position = "LEFT";
				else if (Color1Position.equals("LEFT"))
					Color1Position = "BACK";
				else if (Color1Position.equals("BACK"))
					Color1Position = "RIGHT";
				else if (Color1Position.equals("RIGHT"))
					Color1Position = "FRONT";

				if (Color2Position.equals("FRONT"))
					Color2Position = "LEFT";
				else if (Color2Position.equals("LEFT"))
					Color2Position = "BACK";
				else if (Color2Position.equals("BACK"))
					Color2Position = "RIGHT";
				else if (Color2Position.equals("RIGHT"))
					Color2Position = "FRONT";

				if (Color3Position.equals("FRONT"))
					Color3Position = "LEFT";
				else if (Color3Position.equals("LEFT"))
					Color3Position = "BACK";
				else if (Color3Position.equals("BACK"))
					Color3Position = "RIGHT";
				else if (Color3Position.equals("RIGHT"))
					Color3Position = "FRONT";

				break;

			case 4:
				if ((Color1Position.equals("FRONT") && Color2Position
						.equals("DOWN"))
						|| (Color1Position.equals("FRONT") && Color3Position
								.equals("DOWN")))
					Color1Position = "LEFT";
				else if ((Color1Position.equals("LEFT") && Color2Position
						.equals("DOWN"))
						|| (Color1Position.equals("LEFT") && Color3Position
								.equals("DOWN")))
					Color1Position = "BACK";
				else if ((Color1Position.equals("BACK") && Color2Position
						.equals("DOWN"))
						|| (Color1Position.equals("BACK") && Color3Position
								.equals("DOWN")))
					Color1Position = "RIGHT";
				else if ((Color1Position.equals("RIGHT") && Color2Position
						.equals("DOWN"))
						|| (Color1Position.equals("RIGHT") && Color3Position
								.equals("DOWN")))
					Color1Position = "FRONT";

				if ((Color2Position.equals("FRONT") && Color1Position
						.equals("DOWN"))
						|| (Color2Position.equals("FRONT") && Color3Position
								.equals("DOWN")))
					Color2Position = "LEFT";
				else if ((Color2Position.equals("LEFT") && Color1Position
						.equals("DOWN"))
						|| (Color2Position.equals("LEFT") && Color3Position
								.equals("DOWN")))
					Color2Position = "BACK";
				else if ((Color2Position.equals("BACK") && Color1Position
						.equals("DOWN"))
						|| (Color2Position.equals("BACK") && Color3Position
								.equals("DOWN")))
					Color2Position = "RIGHT";
				else if ((Color2Position.equals("RIGHT") && Color1Position
						.equals("DOWN"))
						|| (Color2Position.equals("RIGHT") && Color3Position
								.equals("DOWN")))
					Color2Position = "FRONT";

				if ((Color3Position.equals("FRONT") && Color2Position
						.equals("DOWN"))
						|| (Color3Position.equals("FRONT") && Color1Position
								.equals("DOWN")))
					Color3Position = "LEFT";
				else if ((Color3Position.equals("LEFT") && Color2Position
						.equals("DOWN"))
						|| (Color3Position.equals("LEFT") && Color1Position
								.equals("DOWN")))
					Color3Position = "BACK";
				else if ((Color3Position.equals("BACK") && Color2Position
						.equals("DOWN"))
						|| (Color3Position.equals("BACK") && Color1Position
								.equals("DOWN")))
					Color3Position = "RIGHT";
				else if ((Color3Position.equals("RIGHT") && Color2Position
						.equals("DOWN"))
						|| (Color3Position.equals("RIGHT") && Color1Position
								.equals("DOWN")))
					Color3Position = "FRONT";

				break;

			case 5:
				if ((Color1Position.equals("FRONT") && Color2Position
						.equals("DOWN"))
						|| (Color1Position.equals("FRONT") && Color3Position
								.equals("DOWN")))
					Color1Position = "RIGHT";
				else if ((Color1Position.equals("RIGHT") && Color2Position
						.equals("DOWN"))
						|| (Color1Position.equals("RIGHT") && Color3Position
								.equals("DOWN")))
					Color1Position = "BACK";
				else if ((Color1Position.equals("BACK") && Color2Position
						.equals("DOWN"))
						|| (Color1Position.equals("BACK") && Color3Position
								.equals("DOWN")))
					Color1Position = "LEFT";
				else if ((Color1Position.equals("LEFT") && Color2Position
						.equals("DOWN"))
						|| (Color1Position.equals("LEFT") && Color3Position
								.equals("DOWN")))
					Color1Position = "FRONT";

				if ((Color2Position.equals("FRONT") && Color1Position
						.equals("DOWN"))
						|| (Color2Position.equals("FRONT") && Color3Position
								.equals("DOWN")))
					Color2Position = "RIGHT";
				else if ((Color2Position.equals("RIGHT") && Color1Position
						.equals("DOWN"))
						|| (Color2Position.equals("RIGHT") && Color3Position
								.equals("DOWN")))
					Color2Position = "BACK";
				else if ((Color2Position.equals("BACK") && Color1Position
						.equals("DOWN"))
						|| (Color2Position.equals("BACK") && Color3Position
								.equals("DOWN")))
					Color2Position = "LEFT";
				else if ((Color2Position.equals("LEFT") && Color1Position
						.equals("DOWN"))
						|| (Color2Position.equals("LEFT") && Color3Position
								.equals("DOWN")))
					Color2Position = "FRONT";

				if ((Color3Position.equals("FRONT") && Color2Position
						.equals("DOWN"))
						|| (Color3Position.equals("FRONT") && Color1Position
								.equals("DOWN")))
					Color3Position = "RIGHT";
				else if ((Color3Position.equals("RIGHT") && Color2Position
						.equals("DOWN"))
						|| (Color3Position.equals("RIGHT") && Color1Position
								.equals("DOWN")))
					Color3Position = "BACK";
				else if ((Color3Position.equals("BACK") && Color2Position
						.equals("DOWN"))
						|| (Color3Position.equals("BACK") && Color1Position
								.equals("DOWN")))
					Color3Position = "LEFT";
				else if ((Color3Position.equals("LEFT") && Color2Position
						.equals("DOWN"))
						|| (Color3Position.equals("LEFT") && Color1Position
								.equals("DOWN")))
					Color3Position = "FRONT";

				break;

			default:
				Sound.playTone(1760, 2000, 100);
				System.out.println("Fehler");
				System.out.println("Roboter wird ausgeschaltet.");
				sleep();
				System.exit(0);
			}
		}
	}

	private void sleep() {
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
		}
	}
}