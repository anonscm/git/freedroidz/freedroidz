package org.evolvis.freedroidz.rubicsCubeSolver;

public interface IColorSensor {

	int getColorNumber();

}
