package org.evolvis.freedroidz.rubicsCubeSolver;

import org.evolvis.freedroidz.rubicsCubeSolver.Cube.CornerCubie;
import org.evolvis.freedroidz.rubicsCubeSolver.Cube.EdgeCubie;
import org.evolvis.freedroidz.rubicsCubeSolver.Cube.MidCubie;

public class Solver {
	
	
	Robot robot;

	public Solver(Robot robot) {
		this.robot = robot;
	}

	public Robot getRobot() {
		return robot;
	}

	public void setRobot(Robot robot) {
		this.robot = robot;
	}
	
	//Sorts all colors of all cubie in alphabetical order
	public void cubieColorSort()
	{
		for(int i=0; i<12; i++)
			robot.edgeCubie[i].colorSort();
		
		for(int i=0; i<8; i++)
			robot.cornerCubie[i].colorSort();
	}

	
	
	/*
	 * The happening bedings here...
	 * 
	 * 
	 * Begins to make a cross on the red side
	 */
	
	public void firstCrossAlgorithm() {
		// Memories for the red MidCubie, the red EdgeCubies and the blue
		// MidCubie
		short redMidCubieChecker = 0; // memory for the red MidCubie
		short[] EdgeCubieChecker = new short[4];
		short blueMidCubieChecker = 0;

		/*
		 * Places the Cube to the state: RED-UP, BLUE-BACK, YELLOW-RIGHT,
		 * GREEN-FRONT, WHITE-LEFT, ORANGE-DOWN
		 */

		// Checks, which MidCubie is the red one and which the blue one
		for (short i = 0; i < 6; i++) {
			if (robot.midCubie[i].colorChecker(Color.RED))
				redMidCubieChecker = i;
			else if (robot.midCubie[i].colorChecker(Color.BLUE))
				blueMidCubieChecker = i;
		}

		MidCubie mcr = robot.midCubie[redMidCubieChecker];

		// Checks where the red MidCubie is and twists it to the Direction.UP side
		if (mcr.hasOrientation(Direction.FRONT))
			robot.Armtwist(3);
		else if (mcr.hasOrientation(Direction.DOWN))
			robot.Armtwist(2);
		else if (mcr.hasOrientation(Direction.BACK))
			robot.Armtwist(1);
		else if (mcr.hasOrientation(Direction.LEFT)) {
			robot.Cubetwist(1);
			robot.Armtwist(1);
		} else if (mcr.hasOrientation(Direction.RIGHT)) {
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
		}

		MidCubie mcb = robot.midCubie[blueMidCubieChecker];
		
		// Checks where the blue MidCubie is and twists it to the Direction.BACK side
		if (mcb.hasOrientation(Direction.FRONT))
			robot.Cubetwist(2);
		else if (mcb.hasOrientation(Direction.LEFT))
			robot.Cubetwist(1);
		else if (mcb.hasOrientation(Direction.RIGHT))
			robot.CubetwistInvert(1);

		/*
		 * Solves the RED-Cross on the UP-side
		 */

		// Checks, which EdgeCubies are red
		// It solves it in the order like below
		for (short i = 0; i < 12; i++) {
			// First one will be the RED-YELLOW-Cubie
			if (robot.edgeCubie[i].colorCheck(Color.RED, Color.YELLOW)) {
				EdgeCubieChecker[0] = i;
			}
			// RED-GREEN Cubie
			if (robot.edgeCubie[i].colorCheck(Color.GREEN, Color.RED)) {
				EdgeCubieChecker[1] = i;
			}
			// RED-WHITE-Cubie
			if (robot.edgeCubie[i].colorCheck(Color.RED, Color.WHITE)) {
				EdgeCubieChecker[2] = i;
			}
			// RED-BLUE-Cubie
			if (robot.edgeCubie[i].colorCheck(Color.BLUE, Color.RED)) {
				EdgeCubieChecker[3] = i;
			}
		}

		/*
		 * First EdgeCubie RED-YELLOW
		 * 
		 * 23 conditions
		 */


		EdgeCubie ec0 = robot.edgeCubie[EdgeCubieChecker[0]];
		
		// If it is on the right place, but the colors are switched
		if (ec0.hasOrientation(Direction.RIGHT, Direction.UP)) {
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Cubetwist(2);
			robot.Armtwist(1);
			robot.Cubetwist(1);
		}
		// If RED-Cubie is on the DOWN side but in the right order
		else if (ec0.hasOrientation(Direction.DOWN, Direction.RIGHT)) {
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Cubetwist(2);
			robot.Armtwist(1);
			robot.Cubetwist(1);
		}
		// If RED is UP and YELLOW is FRONT
		else if (ec0.hasOrientation(Direction.UP, Direction.FRONT)) {
			robot.Armtwist(2);
			robot.HoldAndTwist(1);
			robot.Armtwist(2);
		}
		// If RED is UP and YELLOW is BACK
		else if (ec0.hasOrientation(Direction.UP, Direction.BACK)) {
			robot.Armtwist(2);
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(2);
		}
		// If RED is UP and YELLOW is LEFT
		else if (ec0.hasOrientation(Direction.UP, Direction.LEFT)) {
			robot.Armtwist(2);
			robot.HoldAndTwist(2);
			robot.Armtwist(2);
		}
		// If RED is FRONT and YELLOW is UP
		else if (ec0.hasOrientation(Direction.FRONT, Direction.UP)) {
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.Cubetwist(1);
		}
		// If RED is BACK and YELLOW is UP
		else if (ec0.hasOrientation(Direction.BACK, Direction.UP)) {
			robot.Armtwist(3);
			robot.HoldAndTwist(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(1);
			robot.Armtwist(3);
			robot.CubetwistInvert(1);
		}
		// If RED is LEFT and YELLOW is UP
		else if (ec0.hasOrientation(Direction.LEFT, Direction.UP)) {
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Armtwist(3);
			robot.CubetwistInvert(1);
		}
		// If RED is RIGHT and YELLOW is DOWN
		else if (ec0.hasOrientation(Direction.RIGHT, Direction.DOWN)) {
			robot.HoldAndTwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.Cubetwist(1);
		}
		// If RED is FRONT and YELLOW is RIGHT
		else if (ec0.hasOrientation(Direction.FRONT, Direction.RIGHT)) {
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(3);
			robot.CubetwistInvert(1);
		}
		// If RED is BACK and YELLOW is RIGHT
		else if (ec0.hasOrientation(Direction.BACK, Direction.RIGHT)) {
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Armtwist(3);
			robot.CubetwistInvert(1);
		}
		// If RED is RIGHT and YELLOW is FRONT
		else if (ec0.hasOrientation(Direction.RIGHT, Direction.FRONT)) {
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(3);
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(2);
			robot.Armtwist(3);
			robot.CubetwistInvert(1);
		}
		// If RED is RIGHT and YELLOW is BACK
		else if (ec0.hasOrientation(Direction.RIGHT, Direction.BACK)) {
			robot.Armtwist(3);
			robot.HoldAndTwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Armtwist(3);
			robot.CubetwistInvert(1);
		}
		// If RED is BACK and YELLOW is DOWN
		else if (ec0.hasOrientation(Direction.BACK, Direction.DOWN)) {
			robot.Armtwist(3);
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(1);
			robot.Armtwist(3);
			robot.CubetwistInvert(1);
		}
		// If RED is BACK and YELLOW is LEFT
		else if (ec0.hasOrientation(Direction.BACK, Direction.LEFT)) {
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Armtwist(2);
			robot.Cubetwist(1);
		}
		// If RED is FRONT and YELLOW is DOWN
		else if (ec0.hasOrientation(Direction.FRONT, Direction.DOWN)) {
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.Cubetwist(1);
		}
		// If RED is FRONT and YELLOW is LEFT
		else if (ec0.hasOrientation(Direction.FRONT, Direction.LEFT)) {
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(2);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.Cubetwist(1);
		}
		// If RED is LEFT and YELLOW is DOWN
		else if (ec0.hasOrientation(Direction.LEFT, Direction.DOWN)) {
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.Cubetwist(1);
		}
		// If RED is LEFT and YELLOW is FRONT
		else if (ec0.hasOrientation(Direction.LEFT, Direction.FRONT)) {
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Armtwist(3);
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.CubetwistInvert(2);
			robot.Armtwist(1);
			robot.Cubetwist(1);
		}
		// If RED is LEFT and YELLOW is BACK
		else if (ec0.hasOrientation(Direction.LEFT, Direction.BACK)) {
			robot.Armtwist(3);
			robot.HoldAndTwist(1);
			robot.Armtwist(3);
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(2);
		}
		// If RED is DOWN and YELLOW is LEFT
		else if (ec0.hasOrientation(Direction.DOWN, Direction.LEFT)) {
			robot.HoldAndTwist(2);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(2);
			robot.Armtwist(3);
			robot.CubetwistInvert(1);
		}
		// If RED is DOWN and YELLOW is FRONT
		else if (ec0.hasOrientation(Direction.DOWN, Direction.FRONT)) {
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Armtwist(3);
			robot.CubetwistInvert(1);
		}
		// If RED is DOWN and YELLOW is BACK
		else if (ec0.hasOrientation(Direction.DOWN, Direction.BACK)) {
			robot.HoldAndTwist(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(2);
			robot.Armtwist(3);
			robot.CubetwistInvert(1);
		}

		/*
		 * Second EdgeCubie RED-GREEN
		 * 
		 * 21 conditions
		 */
		
		EdgeCubie ec1 = robot.edgeCubie[EdgeCubieChecker[1]];
		
		// If RED is FRONT and GREEN is UP
		if (ec1.hasOrientation(Direction.UP, Direction.FRONT)) {
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Armtwist(3);
		}
		// If RED is FRONT and GREEN is LEFT
		else if (ec1.hasOrientation(Direction.LEFT, Direction.FRONT)) {
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(2);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Armtwist(3);
		}
		// If RED is FRONT and GREEN is RIGHT
		else if (ec1.hasOrientation(Direction.RIGHT, Direction.FRONT)) {
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.CubetwistInvert(2);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(2);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.Cubetwist(2);
		}
		// If RED is FRONT and GREEN is DOWN
		else if (ec1.hasOrientation(Direction.DOWN, Direction.FRONT)) {
			robot.HoldAndTwist(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.CubetwistInvert(2);
		}
		// If RED is UP and GREEN is LEFT
		else if (ec1.hasOrientation(Direction.LEFT, Direction.UP)) {
			robot.Armtwist(2);
			robot.HoldAndTwist(1);
			robot.Armtwist(3);
			robot.HoldAndTwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(3);
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(3);
		}
		// If RED is UP and GREEN is BACK
		else if (ec1.hasOrientation(Direction.BACK, Direction.UP)) {
			robot.Cubetwist(2);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(3);
		}
		// If RED is LEFT and GREEN is FRONT
		else if (ec1.hasOrientation(Direction.FRONT, Direction.LEFT)) {
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(3);
		}
		// If RED is LEFT and GREEN is UP
		else if (ec1.hasOrientation(Direction.UP, Direction.LEFT)) {
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(3);
		}
		// If RED is LEFT and GREEN is DOWN
		else if (ec1.hasOrientation(Direction.DOWN, Direction.LEFT)) {
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(3);
		}
		// If RED is LEFT and GREEN is BACK
		else if (ec1.hasOrientation(Direction.BACK, Direction.LEFT)) {
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(3);
		}
		// If RED is BACK and GREEN is LEFT
		else if (ec1.hasOrientation(Direction.LEFT, Direction.BACK)) {
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(2);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Armtwist(3);
		}
		// If RED is BACK and GREEN is RIGHT
		else if (ec1.hasOrientation(Direction.RIGHT, Direction.BACK)) {
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(3);
			robot.HoldAndTwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Cubetwist(1);
			robot.Armtwist(3);
		}
		// If RED is BACK and GREEN is UP
		else if (ec1.hasOrientation(Direction.UP, Direction.BACK)) {
			robot.Armtwist(3);
			robot.HoldAndTwist(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(2);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(2);
			robot.Cubetwist(1);
			robot.Armtwist(3);
		}
		// If RED is BACK and GREEN is DOWN
		else if (ec1.hasOrientation(Direction.DOWN, Direction.BACK)) {
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(3);
		}
		// If RED is DOWN and GREEN is LEFT
		else if (ec1.hasOrientation(Direction.LEFT, Direction.DOWN)) {
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Armtwist(3);
		}
		// If RED is DOWN and GREEN is RIGHT
		else if (ec1.hasOrientation(Direction.RIGHT, Direction.DOWN)) {
			robot.HoldAndTwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Armtwist(3);
		}
		// If RED is DOWN and GREEN is FRONT
		else if (ec1.hasOrientation(Direction.FRONT, Direction.DOWN)) {
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Armtwist(3);
		}
		// If RED is DOWN and GREEN is BACK
		else if (ec1.hasOrientation(Direction.BACK, Direction.DOWN)) {
			robot.HoldAndTwist(2);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(2);
			robot.Armtwist(3);
		}
		// If RED is RIGHT and GREEN is FRONT
		else if (ec1.hasOrientation(Direction.FRONT, Direction.RIGHT)) {
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Armtwist(3);
		}
		// If RED is RIGHT and GREEN is BACK
		else if (ec1.hasOrientation(Direction.BACK, Direction.RIGHT)) {
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(2);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(2);
			robot.CubetwistInvert(1);
			robot.Armtwist(3);
			robot.CubetwistInvert(1);
		}
		// If RED is RIGHT and GREEN is DOWN
		else if (ec1.hasOrientation(Direction.DOWN, Direction.RIGHT)) {
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(2);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(3);
			robot.CubetwistInvert(1);
		}

		
		/*
		 * Third EdgeCubie RED-WHITE
		 * 
		 * 19 conditions
		 */

		/*
		 * Hier weiter prüfen
		 */
		EdgeCubie ec2 = robot.edgeCubie[EdgeCubieChecker[2]];
		
		// If RED is UP and WHITE is BACK
		if (ec2.hasOrientation(Direction.UP, Direction.BACK)) {
			robot.Armtwist(3);
			robot.HoldAndTwist(2);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Armtwist(3);
			robot.Cubetwist(1);
		}
		// If RED is LEFT and WHITE is UP
		else if (ec2.hasOrientation(Direction.LEFT, Direction.UP)) {
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Armtwist(3);
			robot.Cubetwist(1);
		}
		// If RED is LEFT and WHITE is DOWN
		else if (ec2.hasOrientation(Direction.LEFT, Direction.DOWN)) {
			robot.HoldAndTwist(1);
			robot.Cubetwist(2);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.CubetwistInvert(1);
		}
		// If RED is LEFT and WHITE is FRONT
		else if (ec2.hasOrientation(Direction.LEFT, Direction.FRONT)) {
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(2);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.CubetwistInvert(1);
		}
		// If RED is LEFT and WHITE is BACK
		else if (ec2.hasOrientation(Direction.LEFT, Direction.BACK)) {
			robot.Armtwist(3);
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Armtwist(3);
			robot.Cubetwist(1);
		}
		// If RED is DOWN and WHITE is FRONT
		else if (ec2.hasOrientation(Direction.DOWN, Direction.FRONT)) {
			robot.HoldAndTwist(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Armtwist(3);
			robot.Cubetwist(1);
		}
		// If RED is DOWN and WHITE is BACK
		else if (ec2.hasOrientation(Direction.DOWN, Direction.BACK)) {
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Armtwist(3);
			robot.Cubetwist(1);
		}
		// If RED is DOWN and WHITE is LEFT
		else if (ec2.hasOrientation(Direction.DOWN, Direction.LEFT)) {
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Armtwist(3);
			robot.Cubetwist(1);
		}
		// If RED is DOWN and WHITE is RIGHT
		else if (ec2.hasOrientation(Direction.DOWN, Direction.RIGHT)) {
			robot.HoldAndTwist(2);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Armtwist(3);
			robot.Cubetwist(1);
		}
		// If RED is BACK and WHITE is UP
		else if (ec2.hasOrientation(Direction.BACK, Direction.UP)) {
			robot.Armtwist(3);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.CubetwistInvert(1);
		}
		// If RED is BACK and WHITE is DOWN
		else if (ec2.hasOrientation(Direction.BACK, Direction.DOWN)) {
			robot.Armtwist(3);
			robot.HoldAndTwist(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.CubetwistInvert(1);
		}
		// If RED is BACK and WHITE is LEFT
		else if (ec2.hasOrientation(Direction.BACK, Direction.LEFT)) {
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(3);
			robot.Cubetwist(1);
		}
		// If RED is BACK and WHITE is RIGHT
		else if (ec2.hasOrientation(Direction.BACK, Direction.RIGHT)) {
			robot.Armtwist(3);
			robot.HoldAndTwist(2);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(3);
			robot.Cubetwist(1);
		}
		// If RED is FRONT and WHITE is LEFT
		else if (ec2.hasOrientation(Direction.FRONT, Direction.LEFT)) {
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Armtwist(3);
			robot.Cubetwist(1);
		}
		// If RED is FRONT and WHITE is RIGHT
		else if (ec2.hasOrientation(Direction.FRONT, Direction.RIGHT)) {
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(2);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(2);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.CubetwistInvert(1);
			robot.Armtwist(3);
		}
		// If RED is FRONT and WHITE is DOWN
		else if (ec2.hasOrientation(Direction.FRONT, Direction.DOWN)) {
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(2);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(3);
		}
		// If RED is RIGHT and WHITE is BACK
		else if (ec2.hasOrientation(Direction.RIGHT, Direction.BACK)) {
			robot.Armtwist(3);
			robot.HoldAndTwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Armtwist(3);
			robot.Cubetwist(1);
		}
		// If RED is RIGHT and WHITE is FRONT
		else if (ec2.hasOrientation(Direction.RIGHT, Direction.FRONT)) {
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(3);
			robot.HoldAndTwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Cubetwist(1);
			robot.Armtwist(3);
			robot.Cubetwist(1);
		}
		// If RED is RIGHT and WHITE is DOWN
		else if (ec2.hasOrientation(Direction.RIGHT, Direction.DOWN)) {
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(3);
			robot.HoldAndTwist(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(3);
			robot.Cubetwist(1);
		}

		
		/*
		 * Fourth EdgeCubie RED-BLUE
		 * 
		 * 17 conditions
		 */
		
		EdgeCubie ec3 = robot.edgeCubie[EdgeCubieChecker[3]];
		
		// If RED is BACK and BLUE is UP
		if (ec3.hasOrientation(Direction.UP, Direction.BACK)) {
			robot.Armtwist(3);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(2);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
		}
		// If RED is BACK and BLUE is DOWN
		else if (ec3.hasOrientation(Direction.DOWN, Direction.BACK)) {
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(2);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.CubetwistInvert(1);
		}
		// If RED is BACK and BLUE is LEFT
		else if (ec3.hasOrientation(Direction.LEFT, Direction.BACK)) {
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Armtwist(3);
			robot.HoldAndTwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
		}
		// If RED is BACK and BLUE is RIGHT
		else if (ec3.hasOrientation(Direction.RIGHT, Direction.BACK)) {
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(3);
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Cubetwist(1);
			robot.Armtwist(1);
		}
		// If RED is DOWN and BLUE is BACK
		else if (ec3.hasOrientation(Direction.BACK, Direction.DOWN)) {
			robot.Armtwist(3);
			robot.HoldAndTwist(2);
			robot.Armtwist(1);
		}
		// If RED is DOWN and BLUE is FRONT
		else if (ec3.hasOrientation(Direction.FRONT, Direction.DOWN)) {
			robot.HoldAndTwist(2);
			robot.Armtwist(3);
			robot.HoldAndTwist(2);
			robot.Armtwist(1);
		}
		// If RED is DOWN and BLUE is LEFT
		else if (ec3.hasOrientation(Direction.LEFT, Direction.DOWN)) {
			robot.HoldAndTwist(1);
			robot.Armtwist(3);
			robot.HoldAndTwist(2);
			robot.Armtwist(1);
		}
		// If RED is DOWN and BLUE is RIGHT
		else if (ec3.hasOrientation(Direction.RIGHT, Direction.DOWN)) {
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(3);
			robot.HoldAndTwist(2);
			robot.Armtwist(1);
		}
		// If RED is LEFT and BLUE is FRONT
		else if (ec3.hasOrientation(Direction.FRONT, Direction.LEFT)) {
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(2);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(2);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.CubetwistInvert(1);
		}
		// If RED is LEFT and BLUE is BACK
		else if (ec3.hasOrientation(Direction.BACK, Direction.LEFT)) {
			robot.Armtwist(3);
			robot.HoldAndTwist(1);
			robot.Armtwist(1);
		}
		// If RED is LEFT and BLUE is DOWN
		else if (ec3.hasOrientation(Direction.DOWN, Direction.LEFT)) {
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(2);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.CubetwistInvert(1);
		}
		// If RED is FRONT and BLUE is DOWN
		else if (ec3.hasOrientation(Direction.DOWN, Direction.FRONT)) {
			robot.HoldAndTwist(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(2);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.CubetwistInvert(1);
		}
		// If RED is FRONT and BLUE is LEFT
		else if (ec3.hasOrientation(Direction.LEFT, Direction.FRONT)) {
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(3);
			robot.HoldAndTwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
		}
		// If RED is FRONT and BLUE is RIGHT
		else if (ec3.hasOrientation(Direction.RIGHT, Direction.FRONT)) {
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Armtwist(3);
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(2);
			robot.Cubetwist(1);
			robot.Armtwist(1);
		}
		// If RED is RIGHT and BLUE is FRONT
		else if (ec3.hasOrientation(Direction.FRONT, Direction.RIGHT)) {
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(2);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.Cubetwist(1);
		}
		// If RED is RIGHT and BLUE is BACK
		else if (ec3.hasOrientation(Direction.BACK, Direction.RIGHT)) {
			robot.Armtwist(3);
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(1);
		}
		// If RED is RIGHT and BLUE is DOWN
		else if (ec3.hasOrientation(Direction.DOWN, Direction.RIGHT)) {
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(2);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.Cubetwist(1);
		}

	}

	
	
	/*
	 * for the first 4 red corners
	 */
	
	public void firstCornerAlgorithm() {
		// Memory for the CornerCubies
		short[] CornerCubieChecker = new short[4];

		for (short i = 0; i < 8; i++) {
			// First one will be the RED-YELLOW-GREEN-Cubie
			if (robot.cornerCubie[i].colorCheck(Color.GREEN, Color.RED, Color.YELLOW)) {
				CornerCubieChecker[0] = i;
			}
			// 2. RED-GREEN-WHITE Cubie
			if (robot.cornerCubie[i].colorCheck(Color.GREEN, Color.RED, Color.WHITE)) {
				CornerCubieChecker[1] = i;
			}
			// 3. RED-WHITE-BLUE.Cubie
			if (robot.cornerCubie[i].colorCheck(Color.BLUE, Color.RED, Color.WHITE)) {
				CornerCubieChecker[2] = i;
			}
			// 4. RED-BLUE-YELLOW-Cubie
			if (robot.cornerCubie[i].colorCheck(Color.BLUE, Color.RED, Color.YELLOW)) {
				CornerCubieChecker[3] = i;
			}
		}		


		
		/*
		 * First CornerCubie RED-GREEN-YELLOW
		 * 
		 * 8 no more big conditions (xD)
		 *
		 *
		 * It checks all possible color positions of the first CornerCubie
		 * First the right-front-up corner. In it the cubie has 2
		 * different position in which it is not solved. This if function checks
		 * if it is in the right upper corner and in which position.
		 */

		CornerCubie cc0 = robot.cornerCubie[CornerCubieChecker[0]];

		
		if ( cc0.hasOrientation(Direction.RIGHT, Direction.FRONT, Direction.UP)

				|| cc0.hasOrientation(Direction.UP,Direction.RIGHT,Direction.FRONT)) 
		{
			while (!cc0.hasOrientation(Direction.FRONT, Direction.UP, Direction.RIGHT)) {
				cornerTwister();
				cc0 = robot.cornerCubie[CornerCubieChecker[0]];
			}
		}
		else if (
				cc0.hasOrientation(Direction.FRONT, Direction.LEFT, Direction.UP)
					
				|| cc0.hasOrientation(Direction.UP, Direction.FRONT, Direction.LEFT)
					
				|| cc0.hasOrientation(Direction.LEFT, Direction.UP, Direction.FRONT)
				) {
				robot.CubetwistInvert(1);
				robot.Armtwist(1);
				robot.HoldAndTwistInvert(1);
				robot.Cubetwist(2);
				robot.Armtwist(1);
				robot.HoldAndTwistInvert(1);
				robot.CubetwistInvert(2);
				robot.Armtwist(1);
				robot.HoldAndTwist(1);
				robot.Cubetwist(2);
				robot.Armtwist(1);
				robot.CubetwistInvert(1);

				while (!cc0.hasOrientation(Direction.FRONT, Direction.UP, Direction.RIGHT)) {
					cornerTwister();
					cc0 = robot.cornerCubie[CornerCubieChecker[0]];
				}
			} else
			if (
					cc0.hasOrientation(Direction.LEFT, Direction.BACK, Direction.UP)
					
				|| cc0.hasOrientation(Direction.UP, Direction.LEFT, Direction.BACK)
				
				|| cc0.hasOrientation(Direction.BACK, Direction.UP, Direction.LEFT)
				) {
				robot.CubetwistInvert(1);
				robot.Armtwist(1);
				robot.HoldAndTwist(1);
				robot.CubetwistInvert(2);
				robot.Armtwist(1);
				robot.HoldAndTwistInvert(2);
				robot.Cubetwist(2);
				robot.Armtwist(1);
				robot.HoldAndTwistInvert(1);
				robot.Cubetwist(2);
				robot.Armtwist(1);
				robot.CubetwistInvert(1);

				while (!cc0.hasOrientation(Direction.FRONT, Direction.UP, Direction.RIGHT)) {
					cornerTwister();
					cc0 = robot.cornerCubie[CornerCubieChecker[0]];
				}
			} else 
				if (
						cc0.hasOrientation(Direction.BACK, Direction.RIGHT, Direction.UP)
						
					|| cc0.hasOrientation(Direction.UP, Direction.BACK, Direction.RIGHT)
						
					|| cc0.hasOrientation(Direction.RIGHT, Direction.UP, Direction.BACK)
					) {
					robot.Cubetwist(1);
					robot.Armtwist(1);
					robot.HoldAndTwistInvert(1);
					robot.Cubetwist(2);
					robot.Armtwist(1);
					robot.HoldAndTwist(1);
					robot.CubetwistInvert(2);
					robot.Armtwist(1);
					robot.HoldAndTwist(1);
					robot.Cubetwist(2);
					robot.Armtwist(1);
					robot.Cubetwist(1);
					robot.HoldAndTwist(1);

					while (!cc0.hasOrientation(Direction.FRONT, Direction.UP, Direction.RIGHT)) {
						cornerTwister();
						cc0 = robot.cornerCubie[CornerCubieChecker[0]];
					}
			} else 
				if (
						cc0.hasOrientation(Direction.DOWN, Direction.FRONT, Direction.RIGHT)
							
					|| cc0.hasOrientation(Direction.RIGHT, Direction.DOWN, Direction.FRONT)
							
					|| cc0.hasOrientation(Direction.FRONT, Direction.RIGHT, Direction.DOWN)
						) {

					while (!cc0.hasOrientation(Direction.FRONT, Direction.UP, Direction.RIGHT)) {
						cornerTwister();
						cc0 = robot.cornerCubie[CornerCubieChecker[0]];
					}
			}
			// If it is in the front-down-left corner
			else if (
						cc0.hasOrientation(Direction.LEFT, Direction.FRONT, Direction.DOWN)
						
					|| cc0.hasOrientation(Direction.DOWN, Direction.LEFT, Direction.FRONT)
						
					|| cc0.hasOrientation(Direction.FRONT, Direction.DOWN, Direction.LEFT)
					) {
						
					robot.HoldAndTwistInvert(1);

					while (!cc0.hasOrientation(Direction.FRONT, Direction.UP, Direction.RIGHT)) {
						cornerTwister();
						cc0 = robot.cornerCubie[CornerCubieChecker[0]];
					}
			}
			// If it is in the back-down-left corner
			else if (
						cc0.hasOrientation(Direction.LEFT, Direction.DOWN, Direction.BACK)
							
					|| cc0.hasOrientation(Direction.DOWN, Direction.LEFT, Direction.BACK)
							
					|| cc0.hasOrientation(Direction.DOWN, Direction.BACK, Direction.LEFT)
					) {
						
					robot.HoldAndTwistInvert(2);
					
					while (!cc0.hasOrientation(Direction.FRONT, Direction.UP, Direction.RIGHT)) {
							cornerTwister();
							cc0 = robot.cornerCubie[CornerCubieChecker[0]];
					}
			}
			// If it is in the back-down-right corner
			else if (
						cc0.hasOrientation(Direction.DOWN, Direction.RIGHT, Direction.BACK)
					
					|| cc0.hasOrientation(Direction.BACK, Direction.DOWN, Direction.RIGHT)
					
					|| cc0.hasOrientation(Direction.RIGHT, Direction.BACK, Direction.DOWN)
					) {
						
					robot.HoldAndTwist(1);

					while (!cc0.hasOrientation(Direction.FRONT, Direction.UP, Direction.RIGHT)) {
						cornerTwister();
						cc0 = robot.cornerCubie[CornerCubieChecker[0]];
					}
			}	
		
		

		/*
		 * Second CornerCubie RED-GREEN-WHITE
		 * 
		 * 7 conditions
		 */
		
		CornerCubie cc1 = robot.cornerCubie[CornerCubieChecker[1]];
		
		//If cubie is in the left-up-front corner
		if ( cc1.hasOrientation(Direction.UP, Direction.LEFT, Direction.FRONT)

				|| cc1.hasOrientation(Direction.LEFT,Direction.FRONT,Direction.UP)) 
		{

			robot.CubetwistInvert(1);
			
			while (!cc1.hasOrientation(Direction.RIGHT, Direction.UP, Direction.FRONT)) {
				cornerTwister();
				cc1 = robot.cornerCubie[CornerCubieChecker[1]];
			}
			robot.Cubetwist(1);
		}
		//If cubie is in the left-up-back corner
		else if ( cc1.hasOrientation(Direction.LEFT, Direction.UP, Direction.BACK)

				|| cc1.hasOrientation(Direction.UP,Direction.BACK,Direction.LEFT)
				
				|| cc1.hasOrientation(Direction.BACK,Direction.LEFT,Direction.UP)) 
		{

			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(2);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.CubetwistInvert(2);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(2);
			robot.Armtwist(1);
			robot.HoldAndTwist(2);
			robot.CubetwistInvert(2);
			while (!cc1.hasOrientation(Direction.RIGHT, Direction.UP, Direction.FRONT)) {
				cornerTwister();
				cc1 = robot.cornerCubie[CornerCubieChecker[1]];
			}
			robot.Cubetwist(1);
		}
		//If cubie is in the right-up-back corner
		else if ( cc1.hasOrientation(Direction.BACK, Direction.UP, Direction.RIGHT)

				|| cc1.hasOrientation(Direction.UP, Direction.RIGHT,Direction.BACK)
				
				|| cc1.hasOrientation(Direction.RIGHT, Direction.BACK,Direction.UP)) 
		{
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(2);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(2);
			robot.CubetwistInvert(2);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(2);
			robot.Armtwist(1);
			
			while (!cc1.hasOrientation(Direction.RIGHT, Direction.UP, Direction.FRONT)) {
				cornerTwister();
				cc1 = robot.cornerCubie[CornerCubieChecker[1]];
			}
			robot.Cubetwist(1);
		}
		//If cubie is in the right-down-front corner
		else if ( cc1.hasOrientation(Direction.DOWN, Direction.RIGHT, Direction.FRONT)

				|| cc1.hasOrientation(Direction.RIGHT, Direction.FRONT,Direction.DOWN)
				
				|| cc1.hasOrientation(Direction.FRONT, Direction.DOWN,Direction.RIGHT)) 
		{
			robot.CubetwistInvert(1);
			robot.HoldAndTwist(1);
			
			while (!cc1.hasOrientation(Direction.RIGHT, Direction.UP, Direction.FRONT)) {
				cornerTwister();
				cc1 = robot.cornerCubie[CornerCubieChecker[1]];
			}
			robot.Cubetwist(1);
		}
		//If cubie is in the left-down-front corner
		else if ( cc1.hasOrientation(Direction.LEFT, Direction.DOWN, Direction.FRONT)

				|| cc1.hasOrientation(Direction.DOWN, Direction.FRONT,Direction.LEFT)
				
				|| cc1.hasOrientation(Direction.FRONT, Direction.LEFT,Direction.DOWN)) 
		{
			robot.CubetwistInvert(1);
			
			while (!cc1.hasOrientation(Direction.RIGHT, Direction.UP, Direction.FRONT)) {
				cornerTwister();
				cc1 = robot.cornerCubie[CornerCubieChecker[1]];
			}
			robot.Cubetwist(1);
		}
		//If cubie is in the left-down-back corner
		else if ( cc1.hasOrientation(Direction.LEFT, Direction.BACK, Direction.DOWN)

				|| cc1.hasOrientation(Direction.BACK, Direction.DOWN,Direction.LEFT)
				
				|| cc1.hasOrientation(Direction.DOWN, Direction.LEFT,Direction.BACK)) 
		{
			robot.CubetwistInvert(1);
			robot.HoldAndTwistInvert(1);
			
			while (!cc1.hasOrientation(Direction.RIGHT, Direction.UP, Direction.FRONT)) {
				cornerTwister();
				cc1 = robot.cornerCubie[CornerCubieChecker[1]];
			}
			robot.Cubetwist(1);
		}
		//If cubie is in the right-down-back corner
		else if ( cc1.hasOrientation(Direction.DOWN, Direction.BACK, Direction.RIGHT)

				|| cc1.hasOrientation(Direction.BACK, Direction.RIGHT,Direction.DOWN)
				
				|| cc1.hasOrientation(Direction.RIGHT, Direction.DOWN,Direction.BACK)) 
		{
			robot.CubetwistInvert(1);
			robot.HoldAndTwistInvert(2);
			
			while (!cc1.hasOrientation(Direction.RIGHT, Direction.UP, Direction.FRONT)) {
				cornerTwister();
				cc1 = robot.cornerCubie[CornerCubieChecker[1]];
			}
			robot.Cubetwist(1);
		}
		
		
		
		/*
		 * Third CornerCubie RED-BLUE-WHITE
		 * 
		 * 6 conditions
		 */
		
		CornerCubie cc2 = robot.cornerCubie[CornerCubieChecker[2]];
		
		//If cubie is in the left-up-back corner
		if ( cc2.hasOrientation(Direction.LEFT, Direction.BACK, Direction.UP)

				|| cc2.hasOrientation(Direction.UP, Direction.LEFT,Direction.BACK)) 
		{

			robot.CubetwistInvert(2);
			
			while (!cc2.hasOrientation(Direction.FRONT, Direction.UP, Direction.RIGHT)) {
				cornerTwister();
				cc2 = robot.cornerCubie[CornerCubieChecker[2]];
			}
			robot.Cubetwist(2);
		}
		//If cubie is in the right-up-back corner
		else if ( cc2.hasOrientation(Direction.RIGHT, Direction.UP, Direction.BACK)

				|| cc2.hasOrientation(Direction.BACK, Direction.RIGHT,Direction.UP)
				
				|| cc2.hasOrientation(Direction.UP, Direction.BACK,Direction.RIGHT)) 
		{

			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(2);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(2);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Cubetwist(2);
			robot.Armtwist(1);
			robot.CubetwistInvert(1);
			
			while (!cc2.hasOrientation(Direction.FRONT, Direction.UP, Direction.RIGHT)) {
				cornerTwister();
				cc2 = robot.cornerCubie[CornerCubieChecker[2]];
			}
			robot.Cubetwist(2);
		}
		//If cubie is in the right-down-front corner
		else if ( cc2.hasOrientation(Direction.FRONT, Direction.RIGHT, Direction.DOWN)

				|| cc2.hasOrientation(Direction.DOWN, Direction.FRONT,Direction.RIGHT)
				
				|| cc2.hasOrientation(Direction.RIGHT, Direction.DOWN,Direction.FRONT)) 
		{

			robot.Cubetwist(2);
			robot.HoldAndTwistInvert(2);
			
			while (!cc2.hasOrientation(Direction.FRONT, Direction.UP, Direction.RIGHT)) {
				cornerTwister();
				cc2 = robot.cornerCubie[CornerCubieChecker[2]];
			}
			robot.CubetwistInvert(2);
		}
		//If cubie is in the left-down-front corner
		else if ( cc2.hasOrientation(Direction.FRONT, Direction.DOWN, Direction.LEFT)

				|| cc2.hasOrientation(Direction.LEFT, Direction.FRONT,Direction.DOWN)
				
				|| cc2.hasOrientation(Direction.DOWN, Direction.LEFT,Direction.FRONT)) 
		{

			robot.Cubetwist(2);
			robot.HoldAndTwist(1);
			
			while (!cc2.hasOrientation(Direction.FRONT, Direction.UP, Direction.RIGHT)) {
				cornerTwister();
				cc2 = robot.cornerCubie[CornerCubieChecker[2]];
			}
			robot.CubetwistInvert(2);
		}
		//If cubie is in the left-down-back corner
		else if ( cc2.hasOrientation(Direction.DOWN, Direction.BACK, Direction.LEFT)

				|| cc2.hasOrientation(Direction.LEFT, Direction.DOWN,Direction.BACK)
				
				|| cc2.hasOrientation(Direction.BACK, Direction.LEFT,Direction.DOWN)) 
		{

			robot.Cubetwist(2);
			
			while (!cc2.hasOrientation(Direction.FRONT, Direction.UP, Direction.RIGHT)) {
				cornerTwister();
				cc2 = robot.cornerCubie[CornerCubieChecker[2]];
			}
			robot.CubetwistInvert(2);
		}
		//If cubie is in the right-down-back corner
		else if ( cc2.hasOrientation(Direction.RIGHT, Direction.BACK, Direction.DOWN)

				|| cc2.hasOrientation(Direction.DOWN, Direction.RIGHT,Direction.BACK)
				
				|| cc2.hasOrientation(Direction.BACK, Direction.DOWN,Direction.RIGHT)) 
		{

			robot.Cubetwist(2);
			robot.HoldAndTwistInvert(1);
			
			while (!cc2.hasOrientation(Direction.FRONT, Direction.UP, Direction.RIGHT)) {
				cornerTwister();
				cc2 = robot.cornerCubie[CornerCubieChecker[2]];
			}
			robot.CubetwistInvert(2);
		}
		
		
		
		/*
		 * Fourth CornerCubie RED-BLUE-YELLOW
		 * 
		 * 5 conditions
		 */
		
		CornerCubie cc3 = robot.cornerCubie[CornerCubieChecker[3]];
		
		//If cubie is in the right-up-back corner
		if ( cc3.hasOrientation(Direction.UP, Direction.RIGHT, Direction.BACK)

				|| cc3.hasOrientation(Direction.RIGHT, Direction.BACK,Direction.UP)) 
		{

			robot.Cubetwist(1);
			
			while (!cc3.hasOrientation(Direction.RIGHT, Direction.UP, Direction.FRONT)) {
				cornerTwister();
				cc3 = robot.cornerCubie[CornerCubieChecker[3]];
			}
			robot.CubetwistInvert(1);
		}
		//If cubie is in the right-down-front corner
		else if ( cc3.hasOrientation(Direction.DOWN, Direction.RIGHT, Direction.FRONT)

				|| cc3.hasOrientation(Direction.RIGHT, Direction.FRONT,Direction.DOWN)
				
				|| cc3.hasOrientation(Direction.FRONT, Direction.DOWN,Direction.RIGHT)) 
		{

			robot.Cubetwist(1);
			robot.HoldAndTwistInvert(1);
			
			while (!cc3.hasOrientation(Direction.RIGHT, Direction.UP, Direction.FRONT)) {
				cornerTwister();
				cc3 = robot.cornerCubie[CornerCubieChecker[3]];
			}
			robot.CubetwistInvert(1);
		}
		//If cubie is in the left-down-front corner
		else if ( cc3.hasOrientation(Direction.LEFT, Direction.DOWN, Direction.FRONT)

				|| cc3.hasOrientation(Direction.DOWN, Direction.FRONT,Direction.LEFT)
				
				|| cc3.hasOrientation(Direction.FRONT, Direction.LEFT,Direction.DOWN)) 
		{

			robot.Cubetwist(1);
			robot.HoldAndTwistInvert(2);
			
			while (!cc3.hasOrientation(Direction.RIGHT, Direction.UP, Direction.FRONT)) {
				cornerTwister();
				cc3 = robot.cornerCubie[CornerCubieChecker[3]];
			}
			robot.CubetwistInvert(1);
		}
		//If cubie is in the left-down-back corner
		else if ( cc3.hasOrientation(Direction.LEFT, Direction.BACK, Direction.DOWN)

				|| cc3.hasOrientation(Direction.BACK, Direction.DOWN,Direction.LEFT)
				
				|| cc3.hasOrientation(Direction.DOWN, Direction.LEFT,Direction.BACK)) 
		{

			robot.Cubetwist(1);
			robot.HoldAndTwist(1);
			
			while (!cc3.hasOrientation(Direction.RIGHT, Direction.UP, Direction.FRONT)) {
				cornerTwister();
				cc3 = robot.cornerCubie[CornerCubieChecker[3]];
			}
			robot.CubetwistInvert(1);
		}
		//If cubie is in the right-down-back corner
		else if ( cc3.hasOrientation(Direction.DOWN, Direction.BACK, Direction.RIGHT)

				|| cc3.hasOrientation(Direction.BACK, Direction.RIGHT,Direction.DOWN)
				
				|| cc3.hasOrientation(Direction.RIGHT, Direction.DOWN,Direction.BACK)) 
		{

			robot.Cubetwist(1);
			
			while (!cc3.hasOrientation(Direction.RIGHT, Direction.UP, Direction.FRONT)) {
				cornerTwister();
				cc3 = robot.cornerCubie[CornerCubieChecker[3]];
			}
			robot.CubetwistInvert(1);
		}
	}

	private void cornerTwister() {
		robot.Cubetwist(1);
		robot.Armtwist(1);
		robot.HoldAndTwist(1);
		robot.CubetwistInvert(2);
		robot.Armtwist(1);
		robot.HoldAndTwist(1);
		robot.Cubetwist(2);
		robot.Armtwist(1);
		robot.HoldAndTwistInvert(1);
		robot.CubetwistInvert(2);
		robot.Armtwist(1);
		robot.HoldAndTwistInvert(1);
		robot.Cubetwist(1);
	}
	
	public void secondLayerAlgorithm(){
		
		short[] edgeCubieChecker = new short[4];

		for (short i = 0; i < 12; i++) {
			// 1. BLUE-WHITE
			if (robot.edgeCubie[i].colorCheck(Color.BLUE, Color.WHITE)) {
				edgeCubieChecker[0] = i;
			}
			// 2. BLUE-YELLOW
			if (robot.edgeCubie[i].colorCheck(Color.BLUE, Color.YELLOW)) {
				edgeCubieChecker[1] = i;
			}
			// 3. WHITE-GREEN.Cubie
			if (robot.edgeCubie[i].colorCheck(Color.GREEN, Color.WHITE)) {
				edgeCubieChecker[2] = i;
			}
			// 4. GREEN-YELLOW-Cubie
			if (robot.edgeCubie[i].colorCheck(Color.GREEN, Color.YELLOW)) {
				edgeCubieChecker[3] = i;
			}
		}
		
		//To twist the cube upside down
		robot.Armtwist(2);
		
		
		
		/*
		 * begin with the BLUE-WHITE cubie
		 */
		
		EdgeCubie ec0 = robot.edgeCubie[edgeCubieChecker[0]];
		
		//if blue is left and white is front
		if(ec0.hasOrientation(Direction.LEFT, Direction.FRONT)){
			secondLayerEdgeLeft();
		}
		//if blue is front and white is right or blue is right and white is front
		else if(ec0.hasOrientation(Direction.FRONT, Direction.RIGHT)
				|| ec0.hasOrientation(Direction.RIGHT, Direction.FRONT)){
			secondLayerEdgeRight();
		}
		//if blue is left and white is back or blue is back and white is left
		else if(ec0.hasOrientation(Direction.LEFT, Direction.BACK)
				|| ec0.hasOrientation(Direction.BACK, Direction.LEFT)){
			robot.CubetwistInvert(1);
			
			secondLayerEdgeLeft();
			
			robot.Cubetwist(1);
		}
		
		//if blue is back and white is right or blue is right and white is back
		else if(ec0.hasOrientation(Direction.BACK, Direction.RIGHT)
				|| ec0.hasOrientation(Direction.RIGHT, Direction.BACK)){
			robot.Cubetwist(1);
			
			secondLayerEdgeRight();
			
			robot.CubetwistInvert(1);
		}
		
		/*
		 * Now the Edgecubie is on the up side so the solver can begin to solve it
		 */
		
		ec0 = robot.edgeCubie[edgeCubieChecker[0]];
		
		//if blue is front and white is up
		if(ec0.hasOrientation(Direction.FRONT, Direction.UP)){
			secondLayerEdgeLeft();
		}
		//if blue is left and white is up
		else if(ec0.hasOrientation(Direction.LEFT, Direction.UP)){
			robot.Armtwist(2);
			robot.HoldAndTwist(1);
			robot.Armtwist(2);
			
			secondLayerEdgeLeft();
		}
		//if blue is back and white is up
		else if(ec0.hasOrientation(Direction.BACK, Direction.UP)){
			robot.Armtwist(2);
			robot.HoldAndTwist(2);
			robot.Armtwist(2);
			
			secondLayerEdgeLeft();
		}
		//if blue is right and white is up
		else if(ec0.hasOrientation(Direction.RIGHT, Direction.UP)){
			robot.Armtwist(2);
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(2);
			
			secondLayerEdgeLeft();
		}
		//if blue is up and white is front
		else if(ec0.hasOrientation(Direction.UP, Direction.FRONT)){
			robot.Armtwist(2);
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(2);
			robot.CubetwistInvert(1);
			
			secondLayerEdgeRight();
			
			robot.Cubetwist(1);
		}
		//if blue is up and white is left
		else if(ec0.hasOrientation(Direction.UP, Direction.LEFT)){
			robot.CubetwistInvert(1);
			
			secondLayerEdgeRight();
			
			robot.Cubetwist(1);
		}
		//if blue is up and white is back
		else if(ec0.hasOrientation(Direction.UP, Direction.BACK)){
			robot.Armtwist(2);
			robot.HoldAndTwist(1);
			robot.Armtwist(2);
			robot.CubetwistInvert(1);
			
			secondLayerEdgeRight();
			
			robot.Cubetwist(1);
		}
		//if blue is up and white is right
		else if(ec0.hasOrientation(Direction.UP, Direction.RIGHT)){
			robot.Armtwist(2);
			robot.HoldAndTwist(2);
			robot.Armtwist(2);
			robot.CubetwistInvert(1);
			
			secondLayerEdgeRight();
			
			robot.Cubetwist(1);
		}
		
		
		
		/*
		 * begin with the BLUE-YELLOW
		 */
		
		EdgeCubie ec1 = robot.edgeCubie[edgeCubieChecker[1]];
		
		//if blue is right and yellow is front
		if(ec1.hasOrientation(Direction.RIGHT, Direction.FRONT)){
			secondLayerEdgeRight();
		}
		//if blue is back and yellow is right or blue is right and yellow is back
		else if(ec1.hasOrientation(Direction.BACK, Direction.RIGHT)
				|| ec1.hasOrientation(Direction.RIGHT, Direction.BACK)){
			robot.Cubetwist(1);
			
			secondLayerEdgeRight();
			
			robot.CubetwistInvert(1);
		}
		//if blue is back and yellow is left or blue is left and yellow is back
		else if(ec1.hasOrientation(Direction.BACK, Direction.LEFT)
				|| ec1.hasOrientation(Direction.LEFT, Direction.BACK)){
			robot.CubetwistInvert(1);
			
			secondLayerEdgeLeft();
			
			robot.Cubetwist(1);
		}
		
		
		//now the cubie is on the up side
		ec1 = robot.edgeCubie[edgeCubieChecker[1]];
		
		//if blue is front and yellow is up
		if(ec1.hasOrientation(Direction.FRONT, Direction.UP)){
			secondLayerEdgeRight();
		}
		//if blue is up and yellow is front
		else if(ec1.hasOrientation(Direction.UP, Direction.FRONT)){
			robot.Armtwist(2);
			robot.HoldAndTwist(1);
			robot.Armtwist(2);
			robot.Cubetwist(1);
			
			secondLayerEdgeLeft();
			
			robot.CubetwistInvert(1);
		}
		//if blue is left and yellow is up
		else if(ec1.hasOrientation(Direction.LEFT, Direction.UP)){
			robot.Armtwist(2);
			robot.HoldAndTwist(1);
			robot.Armtwist(2);
			
			secondLayerEdgeRight();
		}
		//if blue is back and yellow is up
		else if(ec1.hasOrientation(Direction.BACK, Direction.UP)){
			robot.Armtwist(2);
			robot.HoldAndTwist(2);
			robot.Armtwist(2);
			
			secondLayerEdgeRight();
		}
		//if blue is right and yellow is up
		else if(ec1.hasOrientation(Direction.RIGHT, Direction.UP)){
			robot.Armtwist(2);
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(2);
			
			secondLayerEdgeRight();
		}
		//if blue is up and yellow is left
		else if(ec1.hasOrientation(Direction.UP, Direction.LEFT)){
			robot.Armtwist(2);
			robot.HoldAndTwist(2);
			robot.Armtwist(2);
			robot.Cubetwist(1);
			
			secondLayerEdgeLeft();
			
			robot.CubetwistInvert(1);
		}
		//if blue is up and yellow is back
		else if(ec1.hasOrientation(Direction.UP, Direction.BACK)){
			robot.Armtwist(2);
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(2);
			robot.Cubetwist(1);
			
			secondLayerEdgeLeft();
			
			robot.CubetwistInvert(1);
		}
		//if blue is up and yellow is right
		else if(ec1.hasOrientation(Direction.UP, Direction.RIGHT)){
			robot.Cubetwist(1);
			
			secondLayerEdgeLeft();
			
			robot.CubetwistInvert(1);
		}
		
		
		/*
		 * GREEN-WHITE EdgeCubie
		 */
		
		EdgeCubie ec2 = robot.edgeCubie[edgeCubieChecker[2]];
		
		//if green is left and white is back
		 if(ec2.hasOrientation(Direction.LEFT, Direction.BACK)){
			 robot.CubetwistInvert(1);
			 
			 secondLayerEdgeLeft();
			 
			 robot.Cubetwist(1);
		 }
		 //if green is back and white is right or green is right and white is back
		 else if(ec2.hasOrientation(Direction.BACK, Direction.RIGHT)
				 || ec2.hasOrientation(Direction.RIGHT, Direction.BACK)){
			 robot.Cubetwist(1);
			 
			 secondLayerEdgeRight();
			 
			 robot.CubetwistInvert(1);
		 }
		 
		 //now the cubie is on the up side
		 ec2 = robot.edgeCubie[edgeCubieChecker[2]];
		 
		 //if green is front and white is up
		 if(ec2.hasOrientation(Direction.FRONT, Direction.UP)){
			 robot.Armtwist(2);
			 robot.HoldAndTwist(2);
			 robot.Armtwist(2);
			 robot.Cubetwist(2);
			 
			 secondLayerEdgeRight();
			 
			 robot.CubetwistInvert(2);
		 }
		//if green is right and white is up
		 else if(ec2.hasOrientation(Direction.RIGHT, Direction.UP)){
			 robot.Armtwist(2);
			 robot.HoldAndTwist(1);
			 robot.Armtwist(2);
			 robot.Cubetwist(2);
			 
			 secondLayerEdgeRight();
			 
			 robot.CubetwistInvert(2);
		 }
		//if green is back and white is up
		 else if(ec2.hasOrientation(Direction.BACK, Direction.UP)){
			 robot.Cubetwist(2);
			 
			 secondLayerEdgeRight();
			 
			 robot.CubetwistInvert(2);
		 }
		//if green is left and white is up
		 else if(ec2.hasOrientation(Direction.LEFT, Direction.UP)){
			 robot.Armtwist(2);
			 robot.HoldAndTwistInvert(1);
			 robot.Armtwist(2);
			 robot.Cubetwist(2);
			 
			 secondLayerEdgeRight();
			 
			 robot.CubetwistInvert(2);
		 }
		 //if green is up and white is front
		 else if(ec2.hasOrientation(Direction.UP, Direction.FRONT)){
			 robot.Armtwist(2);
			 robot.HoldAndTwistInvert(1);
			 robot.Armtwist(2);
			 robot.CubetwistInvert(1);
			 
			 secondLayerEdgeLeft();
			 
			 robot.Cubetwist(1);
		 }
		 //if green is up and white is right
		 else if(ec2.hasOrientation(Direction.UP, Direction.RIGHT)){
			 robot.Armtwist(2);
			 robot.HoldAndTwistInvert(2);
			 robot.Armtwist(2);
			 robot.CubetwistInvert(1);
			 
			 secondLayerEdgeLeft();
			 
			 robot.Cubetwist(1);
		 }
		 //if green is up and white is back
		 else if(ec2.hasOrientation(Direction.UP, Direction.BACK)){
			 robot.Armtwist(2);
			 robot.HoldAndTwist(1);
			 robot.Armtwist(2);
			 robot.CubetwistInvert(1);
			 
			 secondLayerEdgeLeft();
			 
			 robot.Cubetwist(1);
		 }
		 //if green is up and white is left
		 else if(ec2.hasOrientation(Direction.UP, Direction.LEFT)){
			 robot.CubetwistInvert(1);
			 
			 secondLayerEdgeLeft();
			 
			 robot.Cubetwist(1);
		 }
		 
		 
		 
		 /*
		  * begin with GREEN-YELLOW
		  */
		 
		 EdgeCubie ec3 = robot.edgeCubie[edgeCubieChecker[3]];
		 
		 //if green is right and yellow is back
		 if(ec3.hasOrientation(Direction.RIGHT, Direction.BACK)){
			 robot.Cubetwist(1);
			 
			 secondLayerEdgeRight();
			 
			 robot.CubetwistInvert(1);
		 }
		 
		 
		 //now the cubie is on the up side
		 ec3 = robot.edgeCubie[edgeCubieChecker[3]];
		 
		 //if green is back and yellow is up
		 if(ec3.hasOrientation(Direction.BACK, Direction.UP)){
			 robot.Cubetwist(2);
			 
			 secondLayerEdgeLeft();
			 
			 robot.CubetwistInvert(2);
		 }
		 //if green is left and yellow is up
		 else if(ec3.hasOrientation(Direction.LEFT, Direction.UP)){
			 robot.Armtwist(2);
			 robot.HoldAndTwistInvert(1);
			 robot.Armtwist(2);
			 robot.Cubetwist(2);
			 
			 secondLayerEdgeLeft();
			 
			 robot.CubetwistInvert(2);			 
		 }
		 //if green is front and yellow is up
		 else if(ec3.hasOrientation(Direction.FRONT, Direction.UP)){
			 robot.Armtwist(2);
			 robot.HoldAndTwistInvert(2);
			 robot.Armtwist(2);
			 robot.Cubetwist(2);
			 
			 secondLayerEdgeLeft();
			 
			 robot.CubetwistInvert(2);		
		 }
		 //if green is right and yellow is up
		 else if(ec3.hasOrientation(Direction.RIGHT, Direction.UP)){
			 robot.Armtwist(2);
			 robot.HoldAndTwist(1);
			 robot.Armtwist(2);
			 robot.Cubetwist(2);
			 
			 secondLayerEdgeLeft();
			 
			 robot.CubetwistInvert(2);		
		 }
		 //if green is up and yellow is front
		 else if(ec3.hasOrientation(Direction.UP, Direction.FRONT)){
			 robot.Armtwist(2);
			 robot.HoldAndTwist(1);
			 robot.Armtwist(2);
			 robot.Cubetwist(1);
			 
			 secondLayerEdgeRight();
			 
			 robot.CubetwistInvert(1);
		 }
		 //if green is up and yellow is left
		 else if(ec3.hasOrientation(Direction.UP, Direction.LEFT)){
			 robot.Armtwist(2);
			 robot.HoldAndTwist(2);
			 robot.Armtwist(2);
			 robot.Cubetwist(1);
			 
			 secondLayerEdgeRight();
			 
			 robot.CubetwistInvert(1);			 
		 }
		 //if green is up and yellow is back
		 else if(ec3.hasOrientation(Direction.UP, Direction.BACK)){
			 robot.Armtwist(2);
			 robot.HoldAndTwistInvert(1);
			 robot.Armtwist(2);
			 robot.Cubetwist(1);
			 
			 secondLayerEdgeRight();
			 
			 robot.CubetwistInvert(1);
		 }
		 //if green is up and yellow is right
		 else if(ec3.hasOrientation(Direction.UP, Direction.RIGHT)){
			 
			 robot.Cubetwist(1);
			 
			 secondLayerEdgeRight();
			 
			 robot.CubetwistInvert(1);
		 }
	}
	
	
	/*
	 * for the edgeCubies in the secondLayer which are to switch to the left side
	 */
	
	private void secondLayerEdgeLeft(){
		
		robot.Armtwist(2);
		robot.HoldAndTwist(1);
		robot.CubetwistInvert(1);
		robot.Armtwist(1);
		robot.HoldAndTwist(1);
		robot.Cubetwist(2);
		robot.Armtwist(1);
		robot.HoldAndTwistInvert(1);
		robot.CubetwistInvert(2);
		robot.Armtwist(1);
		robot.HoldAndTwistInvert(1);
		robot.Cubetwist(2);
		robot.Armtwist(1);
		robot.HoldAndTwistInvert(1);
		robot.Cubetwist(1);
		robot.Armtwist(1);
		robot.HoldAndTwistInvert(1);
		robot.Cubetwist(2);
		robot.Armtwist(1);
		robot.HoldAndTwist(1);
		robot.Cubetwist(2);
		robot.Armtwist(1);
		robot.HoldAndTwist(1);
		robot.Armtwist(1);
		robot.Cubetwist(2);
	}
	
	
	
	/*
	 * for the edgeCubies in the secondLayer which are to switch to the right side
	 */
	
	private void secondLayerEdgeRight(){

		robot.Armtwist(2);
		robot.HoldAndTwistInvert(1);
		robot.Cubetwist(1);
		robot.Armtwist(1);
		robot.HoldAndTwistInvert(1);
		robot.Cubetwist(2);
		robot.Armtwist(1);
		robot.HoldAndTwist(1);
		robot.CubetwistInvert(2);
		robot.Armtwist(1);
		robot.HoldAndTwist(1);
		robot.Cubetwist(2);
		robot.Armtwist(1);
		robot.HoldAndTwist(1);
		robot.CubetwistInvert(1);
		robot.Armtwist(1);
		robot.HoldAndTwist(1);
		robot.Cubetwist(2);
		robot.Armtwist(1);
		robot.HoldAndTwistInvert(1);
		robot.Cubetwist(2);
		robot.Armtwist(1);
		robot.HoldAndTwistInvert(1);
		robot.Armtwist(1);
		robot.Cubetwist(2);
	}
	
	
	
	/*
	 * the second cross
	 */
	
	public void secondCrossAlgorithm(){
		short[] edgeCubieChecker = new short[4];

		for (short i = 0; i < 12; i++) {
			// 1. ORANGE-BLUE
			if (robot.edgeCubie[i].colorCheck(Color.BLUE, Color.ORANGE)) {
				edgeCubieChecker[0] = i;
			}
			// 2. ORANGE-WHITE
			if (robot.edgeCubie[i].colorCheck(Color.ORANGE, Color.WHITE)) {
				edgeCubieChecker[1] = i;
			}
			// 3. ORANGE-GREEN
			if (robot.edgeCubie[i].colorCheck(Color.GREEN, Color.ORANGE)) {
				edgeCubieChecker[2] = i;
			}
			// 4. ORANGE-YELLOW-Cubie
			if (robot.edgeCubie[i].colorCheck(Color.ORANGE, Color.YELLOW)) {
				edgeCubieChecker[3] = i;
			}
		}
		
		EdgeCubie ec0 = robot.edgeCubie[edgeCubieChecker[0]];
		EdgeCubie ec1 = robot.edgeCubie[edgeCubieChecker[1]];
		EdgeCubie ec2 = robot.edgeCubie[edgeCubieChecker[2]];
		EdgeCubie ec3 = robot.edgeCubie[edgeCubieChecker[3]];
		
		//if it isn't solved
		if(orangeOnUpSolved(ec0, ec1, ec2, ec3) == false){
			//If there is no orange on the upside (maybe the condition is big, but I wanted it so)
			if(noOrangeOnUpside(ec0, ec1, ec2, ec3)){
				secondCrossTwister(1);
				
				robot.Cubetwist(2);
				
				secondCrossTwister(2);
			}
			//if on the upside is a horizontal line
			else if(upsideHorizontal(ec0, ec1, ec2, ec3)){
				secondCrossTwister(1);
			}
			//if on the upside is a vertical line
			else if(upsideVertical(ec0, ec1, ec2, ec3)){
				robot.Cubetwist(1);
				secondCrossTwister(1);
				robot.CubetwistInvert(1);
			}
			//if there is an "L" on the upside and in the right position
			else if(upsideLcorrect(ec0, ec1, ec2, ec3)){
				secondCrossTwister(2);
			}
			//if the L is inverted
			else if(upsideLInvert(ec0, ec1, ec2, ec3)){
				robot.CubetwistInvert(1);
				
				secondCrossTwister(2);
				
				robot.Cubetwist(1);
			}
			//if the L is upsidedown and inverted
			else if(upsideLUpsidedownInverted(ec0, ec1, ec2, ec3)){
				robot.Cubetwist(2);
				
				secondCrossTwister(2);
				
				robot.Cubetwist(2);
			}
			//if the L is upsidedown
			else if(upsideLUpsidedown(ec0, ec1, ec2, ec3)){
				robot.Cubetwist(1);
				
				secondCrossTwister(2);
				
				robot.CubetwistInvert(1);
			}
		}
	}
	
	private boolean orangeOnUpSolved(EdgeCubie ec0, EdgeCubie ec1, EdgeCubie ec2, EdgeCubie ec3){
		if(ec0.color2Orientation(Direction.UP) && ec1.color1Orientation(Direction.UP) && ec2.color2Orientation(Direction.UP) && ec3.color1Orientation(Direction.UP))
		return true;
		else return false;
	}
	
	private boolean noOrangeOnUpside(EdgeCubie ec0, EdgeCubie ec1, EdgeCubie ec2, EdgeCubie ec3){
		if(
			(ec0.color2Orientation(Direction.FRONT) || ec0.color2Orientation(Direction.LEFT) || ec0.color2Orientation(Direction.BACK) || ec0.color2Orientation(Direction.RIGHT))
			&& (ec1.color1Orientation(Direction.FRONT) || ec1.color1Orientation(Direction.LEFT) || ec1.color1Orientation(Direction.BACK) || ec1.color1Orientation(Direction.RIGHT))
			&& (ec2.color2Orientation(Direction.FRONT) || ec2.color2Orientation(Direction.LEFT) || ec2.color2Orientation(Direction.BACK) || ec2.color2Orientation(Direction.RIGHT))
			&& (ec3.color1Orientation(Direction.FRONT) || ec3.color1Orientation(Direction.LEFT) || ec3.color1Orientation(Direction.BACK) || ec3.color1Orientation(Direction.RIGHT))){
			return true;
		}else {
			return false;
		}
	}
	
	private boolean upsideLcorrect(EdgeCubie ec0, EdgeCubie ec1, EdgeCubie ec2, EdgeCubie ec3){
		
		if(		//orange-blue back
				(ec0.hasOrientation(Direction.BACK, Direction.UP) && ec1.hasOrientation(Direction.UP, Direction.LEFT))
				||(ec0.hasOrientation(Direction.BACK, Direction.UP) && ec2.hasOrientation(Direction.LEFT, Direction.UP))
				||(ec0.hasOrientation(Direction.BACK, Direction.UP) && ec3.hasOrientation(Direction.UP, Direction.LEFT))
				//orange white back
				||(ec1.hasOrientation(Direction.UP, Direction.BACK) && ec0.hasOrientation(Direction.LEFT, Direction.UP))
				||(ec1.hasOrientation(Direction.UP, Direction.BACK) && ec2.hasOrientation(Direction.LEFT, Direction.UP))
				||(ec1.hasOrientation(Direction.UP, Direction.BACK) && ec3.hasOrientation(Direction.UP, Direction.LEFT))
				//orange green back
				||(ec2.hasOrientation(Direction.BACK, Direction.UP) && ec0.hasOrientation(Direction.LEFT, Direction.UP))
				||(ec2.hasOrientation(Direction.BACK, Direction.UP) && ec1.hasOrientation(Direction.UP, Direction.LEFT))
				||(ec2.hasOrientation(Direction.BACK, Direction.UP) && ec3.hasOrientation(Direction.UP, Direction.LEFT))
				//orange yellow back
				||(ec3.hasOrientation(Direction.UP, Direction.BACK) && ec0.hasOrientation(Direction.LEFT, Direction.UP))
				||(ec3.hasOrientation(Direction.UP, Direction.BACK) && ec1.hasOrientation(Direction.UP, Direction.LEFT))
				||(ec3.hasOrientation(Direction.UP, Direction.BACK) && ec2.hasOrientation(Direction.LEFT, Direction.UP))){
				
		return true;
		} else{
			return false;
		}
	}

	private boolean upsideLInvert(EdgeCubie ec0, EdgeCubie ec1, EdgeCubie ec2, EdgeCubie ec3){
		
		if(		//orange-blue back
				(ec0.hasOrientation(Direction.BACK, Direction.UP) && ec1.hasOrientation(Direction.UP, Direction.RIGHT))
				||(ec0.hasOrientation(Direction.BACK, Direction.UP) && ec2.hasOrientation(Direction.RIGHT, Direction.UP))
				||(ec0.hasOrientation(Direction.BACK, Direction.UP) && ec3.hasOrientation(Direction.UP, Direction.RIGHT))
				//orange white back
				||(ec1.hasOrientation(Direction.UP, Direction.BACK) && ec0.hasOrientation(Direction.RIGHT, Direction.UP))
				||(ec1.hasOrientation(Direction.UP, Direction.BACK) && ec2.hasOrientation(Direction.RIGHT, Direction.UP))
				||(ec1.hasOrientation(Direction.UP, Direction.BACK) && ec3.hasOrientation(Direction.UP, Direction.RIGHT))
				//orange green back
				||(ec2.hasOrientation(Direction.BACK, Direction.UP) && ec0.hasOrientation(Direction.RIGHT, Direction.UP))
				||(ec2.hasOrientation(Direction.BACK, Direction.UP) && ec1.hasOrientation(Direction.UP, Direction.RIGHT))
				||(ec2.hasOrientation(Direction.BACK, Direction.UP) && ec3.hasOrientation(Direction.UP, Direction.RIGHT))
				//orange yellow back
				||(ec3.hasOrientation(Direction.UP, Direction.BACK) && ec0.hasOrientation(Direction.RIGHT, Direction.UP))
				||(ec3.hasOrientation(Direction.UP, Direction.BACK) && ec1.hasOrientation(Direction.UP, Direction.RIGHT))
				||(ec3.hasOrientation(Direction.UP, Direction.BACK) && ec2.hasOrientation(Direction.RIGHT, Direction.UP))){
				
		return true;
		} else{
			return false;
		}
	}
	
	private boolean upsideLUpsidedownInverted(EdgeCubie ec0, EdgeCubie ec1, EdgeCubie ec2, EdgeCubie ec3){
		
		if(		//orange-blue front
				(ec0.hasOrientation(Direction.FRONT, Direction.UP) && ec1.hasOrientation(Direction.UP, Direction.RIGHT))
				||(ec0.hasOrientation(Direction.FRONT, Direction.UP) && ec2.hasOrientation(Direction.RIGHT, Direction.UP))
				||(ec0.hasOrientation(Direction.FRONT, Direction.UP) && ec3.hasOrientation(Direction.UP, Direction.RIGHT))
				//orange white front
				||(ec1.hasOrientation(Direction.UP, Direction.FRONT) && ec0.hasOrientation(Direction.RIGHT, Direction.UP))
				||(ec1.hasOrientation(Direction.UP, Direction.FRONT) && ec2.hasOrientation(Direction.RIGHT, Direction.UP))
				||(ec1.hasOrientation(Direction.UP, Direction.FRONT) && ec3.hasOrientation(Direction.UP, Direction.RIGHT))
				//orange green front
				||(ec2.hasOrientation(Direction.FRONT, Direction.UP) && ec0.hasOrientation(Direction.RIGHT, Direction.UP))
				||(ec2.hasOrientation(Direction.FRONT, Direction.UP) && ec1.hasOrientation(Direction.UP, Direction.RIGHT))
				||(ec2.hasOrientation(Direction.FRONT, Direction.UP) && ec3.hasOrientation(Direction.UP, Direction.RIGHT))
				//orange yellow front
				||(ec3.hasOrientation(Direction.UP, Direction.FRONT) && ec0.hasOrientation(Direction.RIGHT, Direction.UP))
				||(ec3.hasOrientation(Direction.UP, Direction.FRONT) && ec1.hasOrientation(Direction.UP, Direction.RIGHT))
				||(ec3.hasOrientation(Direction.UP, Direction.FRONT) && ec2.hasOrientation(Direction.RIGHT, Direction.UP))){
				
		return true;
		} else{
			return false;
		}
	}
	
	private boolean upsideLUpsidedown(EdgeCubie ec0, EdgeCubie ec1, EdgeCubie ec2, EdgeCubie ec3){
		
		if(		//orange-blue front
				(ec0.hasOrientation(Direction.FRONT, Direction.UP) && ec1.hasOrientation(Direction.UP, Direction.LEFT))
				||(ec0.hasOrientation(Direction.FRONT, Direction.UP) && ec2.hasOrientation(Direction.LEFT, Direction.UP))
				||(ec0.hasOrientation(Direction.FRONT, Direction.UP) && ec3.hasOrientation(Direction.UP, Direction.LEFT))
				//orange white front
				||(ec1.hasOrientation(Direction.UP, Direction.FRONT) && ec0.hasOrientation(Direction.LEFT, Direction.UP))
				||(ec1.hasOrientation(Direction.UP, Direction.FRONT) && ec2.hasOrientation(Direction.LEFT, Direction.UP))
				||(ec1.hasOrientation(Direction.UP, Direction.FRONT) && ec3.hasOrientation(Direction.UP, Direction.LEFT))
				//orange green front
				||(ec2.hasOrientation(Direction.FRONT, Direction.UP) && ec0.hasOrientation(Direction.LEFT, Direction.UP))
				||(ec2.hasOrientation(Direction.FRONT, Direction.UP) && ec1.hasOrientation(Direction.UP, Direction.LEFT))
				||(ec2.hasOrientation(Direction.FRONT, Direction.UP) && ec3.hasOrientation(Direction.UP, Direction.LEFT))
				//orange yellow front
				||(ec3.hasOrientation(Direction.UP, Direction.FRONT) && ec0.hasOrientation(Direction.LEFT, Direction.UP))
				||(ec3.hasOrientation(Direction.UP, Direction.FRONT) && ec1.hasOrientation(Direction.UP, Direction.LEFT))
				||(ec3.hasOrientation(Direction.UP, Direction.FRONT) && ec2.hasOrientation(Direction.LEFT, Direction.UP))){
				
		return true;
		} else{
			return false;
		}
	}
	
	//ec0.blue-orange, ec1.orange-white, ec2.green-orange, ec3.orange-yellow
	private boolean upsideHorizontal(EdgeCubie ec0, EdgeCubie ec1, EdgeCubie ec2, EdgeCubie ec3){
		
		if(		//orange-blue front
				(ec0.hasOrientation(Direction.RIGHT, Direction.UP) && ec1.hasOrientation(Direction.UP, Direction.LEFT))
				||(ec0.hasOrientation(Direction.RIGHT, Direction.UP) && ec2.hasOrientation(Direction.LEFT, Direction.UP))
				||(ec0.hasOrientation(Direction.RIGHT, Direction.UP) && ec3.hasOrientation(Direction.UP, Direction.LEFT))
				//orange white front
				||(ec1.hasOrientation(Direction.UP, Direction.RIGHT) && ec0.hasOrientation(Direction.LEFT, Direction.UP))
				||(ec1.hasOrientation(Direction.UP, Direction.RIGHT) && ec2.hasOrientation(Direction.LEFT, Direction.UP))
				||(ec1.hasOrientation(Direction.UP, Direction.RIGHT) && ec3.hasOrientation(Direction.UP, Direction.LEFT))
				//orange green front
				||(ec2.hasOrientation(Direction.RIGHT, Direction.UP) && ec0.hasOrientation(Direction.LEFT, Direction.UP))
				||(ec2.hasOrientation(Direction.RIGHT, Direction.UP) && ec1.hasOrientation(Direction.UP, Direction.LEFT))
				||(ec2.hasOrientation(Direction.RIGHT, Direction.UP) && ec3.hasOrientation(Direction.UP, Direction.LEFT))
				//orange yellow front
				||(ec3.hasOrientation(Direction.UP, Direction.RIGHT) && ec0.hasOrientation(Direction.LEFT, Direction.UP))
				||(ec3.hasOrientation(Direction.UP, Direction.RIGHT) && ec1.hasOrientation(Direction.UP, Direction.LEFT))
				||(ec3.hasOrientation(Direction.UP, Direction.RIGHT) && ec2.hasOrientation(Direction.LEFT, Direction.UP))){
				
		return true;
		} else{
			return false;
		}
	}
	
	private boolean upsideVertical(EdgeCubie ec0, EdgeCubie ec1, EdgeCubie ec2, EdgeCubie ec3){
		
		if(		//orange-blue front
				(ec0.hasOrientation(Direction.FRONT, Direction.UP) && ec1.hasOrientation(Direction.UP, Direction.BACK))
				||(ec0.hasOrientation(Direction.FRONT, Direction.UP) && ec2.hasOrientation(Direction.BACK, Direction.UP))
				||(ec0.hasOrientation(Direction.FRONT, Direction.UP) && ec3.hasOrientation(Direction.UP, Direction.BACK))
				//orange white front
				||(ec1.hasOrientation(Direction.UP, Direction.FRONT) && ec0.hasOrientation(Direction.BACK, Direction.UP))
				||(ec1.hasOrientation(Direction.UP, Direction.FRONT) && ec2.hasOrientation(Direction.BACK, Direction.UP))
				||(ec1.hasOrientation(Direction.UP, Direction.FRONT) && ec3.hasOrientation(Direction.UP, Direction.BACK))
				//orange green front
				||(ec2.hasOrientation(Direction.FRONT, Direction.UP) && ec0.hasOrientation(Direction.BACK, Direction.UP))
				||(ec2.hasOrientation(Direction.FRONT, Direction.UP) && ec1.hasOrientation(Direction.UP, Direction.BACK))
				||(ec2.hasOrientation(Direction.FRONT, Direction.UP) && ec3.hasOrientation(Direction.UP, Direction.BACK))
				//orange yellow front
				||(ec3.hasOrientation(Direction.UP, Direction.FRONT) && ec0.hasOrientation(Direction.BACK, Direction.UP))
				||(ec3.hasOrientation(Direction.UP, Direction.FRONT) && ec1.hasOrientation(Direction.UP, Direction.BACK))
				||(ec3.hasOrientation(Direction.UP, Direction.FRONT) && ec2.hasOrientation(Direction.BACK, Direction.UP))){
				
		return true;
		} else{
			return false;
		}
	}

	private void secondCrossTwister(int times){
		for(short i=0; i<times; i++){
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Cubetwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwistInvert(1);
			robot.Armtwist(3);
			robot.HoldAndTwist(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.CubetwistInvert(1);
			robot.Armtwist(1);
			robot.HoldAndTwist(1);
			robot.Armtwist(1);
			robot.Cubetwist(2);
		}
	}
	
	
	
	
	/*
	 * For the EdgeCubies on the 3. layer
	 */
	
	public void secondEdgeAlgorithm(){
		short[] edgeCubieChecker = new short[4];

		for (short i = 0; i < 12; i++) {
			// 1. ORANGE-BLUE
			if (robot.edgeCubie[i].colorCheck(Color.BLUE, Color.ORANGE)) {
				edgeCubieChecker[0] = i;
			}
			// 2. ORANGE-WHITE
			if (robot.edgeCubie[i].colorCheck(Color.ORANGE, Color.WHITE)) {
				edgeCubieChecker[1] = i;
			}
			// 3. ORANGE-GREEN
			if (robot.edgeCubie[i].colorCheck(Color.GREEN, Color.ORANGE)) {
				edgeCubieChecker[2] = i;
			}
			// 4. ORANGE-YELLOW-Cubie
			if (robot.edgeCubie[i].colorCheck(Color.ORANGE, Color.YELLOW)) {
				edgeCubieChecker[3] = i;
			}
		}
		
		EdgeCubie ec0 = robot.edgeCubie[edgeCubieChecker[0]];
		EdgeCubie ec1 = robot.edgeCubie[edgeCubieChecker[1]];
		EdgeCubie ec2 = robot.edgeCubie[edgeCubieChecker[2]];
		EdgeCubie ec3 = robot.edgeCubie[edgeCubieChecker[3]];
		
		boolean solved = false;
		
		//if blue and green are opposite
		for(short i=0; i<4 && !blueAndGreenOppositeTrue(ec0,ec2); i++){
			
			robot.Armtwist(2);
			robot.HoldAndTwist(1);
			robot.Armtwist(2);
			
			ec0 = robot.edgeCubie[edgeCubieChecker[0]];
			ec1 = robot.edgeCubie[edgeCubieChecker[1]];
			ec2 = robot.edgeCubie[edgeCubieChecker[2]];
			ec3 = robot.edgeCubie[edgeCubieChecker[3]];
			
			if(blueAndGreenOppositeTrue(ec0, ec2)){
				thirdLayerEdgeTwister();
				robot.Armtwist(2);
				robot.HoldAndTwist(1);
				robot.Armtwist(2);
				robot.CubetwistInvert(1);
				thirdLayerEdgeTwister();
				robot.Cubetwist(1);
				
				solved = true;
			}
		}
		
		//if white and yellow are opposite
		if(solved == false){
			for(short i=0; i<4 && !whiteAndYellowOppositeTrue(ec1, ec3); i++){
				
				robot.Armtwist(2);
				robot.HoldAndTwist(1);
				robot.Armtwist(2);
				
				ec0 = robot.edgeCubie[edgeCubieChecker[0]];
				ec1 = robot.edgeCubie[edgeCubieChecker[1]];
				ec2 = robot.edgeCubie[edgeCubieChecker[2]];
				ec3 = robot.edgeCubie[edgeCubieChecker[3]];
				
				if(whiteAndYellowOppositeTrue(ec1, ec3)){
					robot.Cubetwist(1);
					thirdLayerEdgeTwister();
					robot.Armtwist(2);
					robot.HoldAndTwist(1);
					robot.Armtwist(2);
					robot.CubetwistInvert(1);
					thirdLayerEdgeTwister();
					
					solved = true;
				}
			}
		}
		
		//if blue is  front and yellow is right
		if(solved == false){
			for(short i=0; i<4 && !blueAndYellowL(ec0, ec3); i++){
				robot.Armtwist(2);
				robot.HoldAndTwist(1);
				robot.Armtwist(2);

				ec0 = robot.edgeCubie[edgeCubieChecker[0]];
				ec1 = robot.edgeCubie[edgeCubieChecker[1]];
				ec2 = robot.edgeCubie[edgeCubieChecker[2]];
				ec3 = robot.edgeCubie[edgeCubieChecker[3]];
				
				if(blueAndYellowL(ec0, ec3)){
					robot.CubetwistInvert(1);
					thirdLayerEdgeTwister();
					robot.Cubetwist(1);
					
					solved = true;
				}
			}
		}
		
		//if blue is front and white is left
		if(solved == false){
			for(short i=0; i<4 && !blueAndWhiteL(ec0, ec1); i++){
				robot.Armtwist(2);
				robot.HoldAndTwist(1);
				robot.Armtwist(2);

				ec0 = robot.edgeCubie[edgeCubieChecker[0]];
				ec1 = robot.edgeCubie[edgeCubieChecker[1]];
				ec2 = robot.edgeCubie[edgeCubieChecker[2]];
				ec3 = robot.edgeCubie[edgeCubieChecker[3]];
				
				if(blueAndWhiteL(ec0, ec1)){
					robot.Cubetwist(2);
					thirdLayerEdgeTwister();
					robot.Cubetwist(2);
					
					solved = true;
				}
			}
		}
		
		//if green is back and yellow is right
		if(solved == false){
			for(short i=0; i<4 && !greenAndYellowL(ec2, ec3); i++){
				robot.Armtwist(2);
				robot.HoldAndTwist(1);
				robot.Armtwist(2);

				ec0 = robot.edgeCubie[edgeCubieChecker[0]];
				ec1 = robot.edgeCubie[edgeCubieChecker[1]];
				ec2 = robot.edgeCubie[edgeCubieChecker[2]];
				ec3 = robot.edgeCubie[edgeCubieChecker[3]];
				
				if(greenAndYellowL(ec2, ec3)){
					thirdLayerEdgeTwister();
					
					solved = true;
				}
			}
		}
		
		//if green is back and white is left
		if(solved == false){
			for(short i=0; i<4 && !greenAndWhiteL(ec2, ec1); i++){
				robot.Armtwist(2);
				robot.HoldAndTwist(1);
				robot.Armtwist(2);

				ec0 = robot.edgeCubie[edgeCubieChecker[0]];
				ec1 = robot.edgeCubie[edgeCubieChecker[1]];
				ec2 = robot.edgeCubie[edgeCubieChecker[2]];
				ec3 = robot.edgeCubie[edgeCubieChecker[3]];
				
				if(greenAndYellowL(ec2, ec1)){
					robot.Cubetwist(1);
					thirdLayerEdgeTwister();
					robot.CubetwistInvert(1);
					
					solved = true;
				}
			}
		}
	}
	
	//blue and yellow cornered
	private boolean blueAndYellowL(EdgeCubie ec0, EdgeCubie ec3){
		if(ec0.hasOrientation(Direction.FRONT, Direction.UP) && ec3.hasOrientation(Direction.UP, Direction.RIGHT)){
			return true;
		}else {
			return false;
		}
	}
	//blue and white cornered
	private boolean blueAndWhiteL(EdgeCubie ec0, EdgeCubie ec1){
		if(ec0.hasOrientation(Direction.FRONT, Direction.UP) && ec1.hasOrientation(Direction.UP, Direction.LEFT)){
			return true;
		}else {
			return false;
		}
	}
	//green and yellow cornered
	private boolean greenAndYellowL(EdgeCubie ec2, EdgeCubie ec3){
		if(ec2.hasOrientation(Direction.BACK, Direction.UP) && ec3.hasOrientation(Direction.UP, Direction.RIGHT)){
			return true;
		}else {
			return false;
		}
	}
	//green and white cornered
	private boolean greenAndWhiteL(EdgeCubie ec2, EdgeCubie ec1){
		if(ec2.hasOrientation(Direction.BACK, Direction.UP) && ec1.hasOrientation(Direction.UP, Direction.LEFT)){
			return true;
		}else {
			return false;
		}
	}
	
	//ec0.orange-blue, ec1.orange-white, ec2.orange-green, ec3.orange-yellow
	private boolean blueAndGreenOppositeTrue(EdgeCubie ec0, EdgeCubie ec2){
		
		if(ec0.hasOrientation(Direction.FRONT, Direction.UP) && ec2.hasOrientation(Direction.BACK, Direction.UP)){
			return true;
		}else {
			return false;
		}
	}
	
	private boolean whiteAndYellowOppositeTrue(EdgeCubie ec1, EdgeCubie ec3){
		
		if(ec1.hasOrientation(Direction.UP, Direction.LEFT) && ec3.hasOrientation(Direction.UP, Direction.RIGHT)){
			return true;
		}else {
			return false;
		}
	}
	
	private void thirdLayerEdgeTwister(){
		robot.Cubetwist(1);
		robot.Armtwist(1);
		robot.HoldAndTwistInvert(1);
		robot.Armtwist(1);
		robot.HoldAndTwistInvert(1);
		robot.Armtwist(3);
		robot.HoldAndTwist(1);
		robot.Armtwist(1);
		robot.HoldAndTwistInvert(1);
		robot.Armtwist(3);
		robot.HoldAndTwistInvert(1);
		robot.Armtwist(1);
		robot.HoldAndTwist(2);
		robot.Armtwist(3);
		robot.HoldAndTwist(1);
		robot.Armtwist(1);
		robot.HoldAndTwistInvert(1);
		robot.Armtwist(2);
		robot.CubetwistInvert(1);
	}
	
	public void secondCornerAlgorithm(){
		short[] cornerCubieChecker = new short[4];

		for (short i = 0; i < 8; i++) {
			// First one will be the BLUE-ORANGE-YELLOW-Cubie
			if (robot.cornerCubie[i].colorCheck(Color.BLUE, Color.ORANGE, Color.YELLOW)) {
				cornerCubieChecker[0] = i;
			}
			// 2. BLUE-ORANGE-WHITE Cubie
			if (robot.cornerCubie[i].colorCheck(Color.BLUE, Color.ORANGE, Color.WHITE)) {
				cornerCubieChecker[1] = i;
			}
			// 3. GREEN-ORANGE-WHITE.Cubie
			if (robot.cornerCubie[i].colorCheck(Color.GREEN, Color.ORANGE, Color.WHITE)) {
				cornerCubieChecker[2] = i;
			}
			// 4. GREEN-ORANGE-YELLOW-Cubie
			if (robot.cornerCubie[i].colorCheck(Color.GREEN, Color.ORANGE, Color.YELLOW)) {
				cornerCubieChecker[3] = i;
			}
		}
		CornerCubie cc0 = robot.cornerCubie[cornerCubieChecker[0]];
		CornerCubie cc1 = robot.cornerCubie[cornerCubieChecker[1]];
		CornerCubie cc2 = robot.cornerCubie[cornerCubieChecker[2]];
		CornerCubie cc3 = robot.cornerCubie[cornerCubieChecker[3]];
		
		boolean solved = false;
		
		//if no cubie is on its place
		if (solved == false){
			if(noCornerRight(cc0, cc1, cc2, cc3)){
				cornerTwister2();
				
				cc0 = robot.cornerCubie[cornerCubieChecker[0]];
				cc1 = robot.cornerCubie[cornerCubieChecker[1]];
				cc2 = robot.cornerCubie[cornerCubieChecker[2]];
				cc3 = robot.cornerCubie[cornerCubieChecker[3]];
			}
		}
		
		//if all cubies are on their right places
		if(solved == false){
			if(!noCornerRight(cc0, cc1, cc2, cc3)){
				solved = true;
			}
		}
		
		//if the blue orange yellow cubie is on its right place
		if (solved == false){
			if(blueOrangeYellowTrue(cc0)){
				cornerTwister2();
				
				cc0 = robot.cornerCubie[cornerCubieChecker[0]];
				cc1 = robot.cornerCubie[cornerCubieChecker[1]];
				cc2 = robot.cornerCubie[cornerCubieChecker[2]];
				cc3 = robot.cornerCubie[cornerCubieChecker[3]];
				if(!(blueOrangeWhiteTrue(cc1) && greenOrangeWhiteTrue(cc2) && greenOrangeYellowTrue(cc3))){
					cornerTwister2();
					
					cc0 = robot.cornerCubie[cornerCubieChecker[0]];
					cc1 = robot.cornerCubie[cornerCubieChecker[1]];
					cc2 = robot.cornerCubie[cornerCubieChecker[2]];
					cc3 = robot.cornerCubie[cornerCubieChecker[3]];
				}
				solved = true;
			}
		}
		
		//if blue-orange-white is on its right place
		if(solved == false){
			if(blueOrangeWhiteTrue(cc1)){
				robot.CubetwistInvert(1);
				cornerTwister2();
				robot.Cubetwist(1);
				
				cc0 = robot.cornerCubie[cornerCubieChecker[0]];
				cc1 = robot.cornerCubie[cornerCubieChecker[1]];
				cc2 = robot.cornerCubie[cornerCubieChecker[2]];
				cc3 = robot.cornerCubie[cornerCubieChecker[3]];
				
				if(!(blueOrangeYellowTrue(cc0) && greenOrangeWhiteTrue(cc2) && greenOrangeYellowTrue(cc3))){
					robot.CubetwistInvert(1);
					cornerTwister2();
					robot.Cubetwist(1);
					
					cc0 = robot.cornerCubie[cornerCubieChecker[0]];
					cc1 = robot.cornerCubie[cornerCubieChecker[1]];
					cc2 = robot.cornerCubie[cornerCubieChecker[2]];
					cc3 = robot.cornerCubie[cornerCubieChecker[3]];
				}
				
				solved = true;
			}
		}
		
		//if green-orange-white is on its right place
		if(solved == false){
			if(greenOrangeWhiteTrue(cc2)){
				robot.CubetwistInvert(2);
				cornerTwister2();
				robot.Cubetwist(2);
				
				cc0 = robot.cornerCubie[cornerCubieChecker[0]];
				cc1 = robot.cornerCubie[cornerCubieChecker[1]];
				cc2 = robot.cornerCubie[cornerCubieChecker[2]];
				cc3 = robot.cornerCubie[cornerCubieChecker[3]];
				
				if(!(blueOrangeYellowTrue(cc0) && blueOrangeWhiteTrue(cc1) && greenOrangeYellowTrue(cc3))){
					robot.CubetwistInvert(2);
					cornerTwister2();
					robot.Cubetwist(2);
					
					cc0 = robot.cornerCubie[cornerCubieChecker[0]];
					cc1 = robot.cornerCubie[cornerCubieChecker[1]];
					cc2 = robot.cornerCubie[cornerCubieChecker[2]];
					cc3 = robot.cornerCubie[cornerCubieChecker[3]];
				}
				
				solved = true;
			}
		}
		
		//if green-orange-yellow is on its right place
		if(solved == false){
			if(greenOrangeYellowTrue(cc3)){
				robot.Cubetwist(1);
				cornerTwister2();
				robot.CubetwistInvert(1);
				
				cc0 = robot.cornerCubie[cornerCubieChecker[0]];
				cc1 = robot.cornerCubie[cornerCubieChecker[1]];
				cc2 = robot.cornerCubie[cornerCubieChecker[2]];
				cc3 = robot.cornerCubie[cornerCubieChecker[3]];
				
				if(!(blueOrangeYellowTrue(cc0) && blueOrangeWhiteTrue(cc1) && greenOrangeWhiteTrue(cc2))){
					robot.Cubetwist(1);
					cornerTwister2();
					robot.CubetwistInvert(1);
					
					cc0 = robot.cornerCubie[cornerCubieChecker[0]];
					cc1 = robot.cornerCubie[cornerCubieChecker[1]];
					cc2 = robot.cornerCubie[cornerCubieChecker[2]];
					cc3 = robot.cornerCubie[cornerCubieChecker[3]];
				}
				
				solved = true;
			}
		}
	}
	
	//if the blue-orange-yellow cubie is on its right position
	private boolean blueOrangeYellowTrue(CornerCubie cc0){
		if(cc0.hasOrientation(Direction.FRONT, Direction.UP, Direction.RIGHT)
						|| cc0.hasOrientation(Direction.RIGHT, Direction.FRONT, Direction.UP)
						|| cc0.hasOrientation(Direction.UP, Direction.RIGHT, Direction.FRONT)){
		return true;
		}else
		return false;
	}
	
	//blue-orange-white
	private boolean blueOrangeWhiteTrue(CornerCubie cc1){
		if(cc1.hasOrientation(Direction.FRONT, Direction.UP, Direction.LEFT)
				|| cc1.hasOrientation(Direction.UP, Direction.LEFT, Direction.FRONT)
				|| cc1.hasOrientation(Direction.LEFT, Direction.FRONT, Direction.UP)){
			return true;
		}
		return false;
	}
	
	//green-orange-white
	private boolean greenOrangeWhiteTrue(CornerCubie cc2){
		if(cc2.hasOrientation(Direction.BACK, Direction.UP, Direction.LEFT)
						|| cc2.hasOrientation(Direction.LEFT, Direction.BACK, Direction.UP)
						|| cc2.hasOrientation(Direction.UP, Direction.LEFT, Direction.BACK)){
			return true;
		}else
			return false;
	}
	
	//green-orange-yellow
	private boolean greenOrangeYellowTrue(CornerCubie cc3){
		if(cc3.hasOrientation(Direction.BACK, Direction.UP, Direction.RIGHT)
						|| cc3.hasOrientation(Direction.UP, Direction.RIGHT, Direction.BACK)
						|| cc3.hasOrientation(Direction.RIGHT, Direction.BACK, Direction.UP)){
			return true;
		}else
			return false;
	}
	
	//if no cubie is on its place
	private boolean noCornerRight(CornerCubie cc0, CornerCubie cc1, CornerCubie cc2, CornerCubie cc3){
		if(
				!((cc0.hasOrientation(Direction.FRONT, Direction.UP, Direction.RIGHT)
						|| cc0.hasOrientation(Direction.RIGHT, Direction.FRONT, Direction.UP)
						|| cc0.hasOrientation(Direction.UP, Direction.RIGHT, Direction.FRONT))
						
				&& (cc1.hasOrientation(Direction.FRONT, Direction.UP, Direction.LEFT)
						|| cc1.hasOrientation(Direction.UP, Direction.LEFT, Direction.FRONT)
						|| cc1.hasOrientation(Direction.LEFT, Direction.FRONT, Direction.UP))
						
				&& (cc2.hasOrientation(Direction.BACK, Direction.UP, Direction.LEFT)
						|| cc2.hasOrientation(Direction.LEFT, Direction.BACK, Direction.UP)
						|| cc2.hasOrientation(Direction.UP, Direction.LEFT, Direction.BACK))
						
				&& (cc3.hasOrientation(Direction.BACK, Direction.UP, Direction.RIGHT)
						|| cc3.hasOrientation(Direction.UP, Direction.RIGHT, Direction.BACK)
						|| cc3.hasOrientation(Direction.RIGHT, Direction.BACK, Direction.UP)))){
			return true;
		} else
			return false;
	}
	
	private void cornerTwister2(){
		robot.Armtwist(2);
		robot.HoldAndTwistInvert(1);
		robot.Cubetwist(1);
		robot.Armtwist(1);
		robot.HoldAndTwistInvert(1);
		robot.Cubetwist(2);
		robot.Armtwist(1);
		robot.HoldAndTwist(1);
		robot.Armtwist(1);
		robot.HoldAndTwist(1);
		robot.Cubetwist(2);
		robot.Armtwist(1);
		robot.HoldAndTwistInvert(1);
		robot.Armtwist(1);
		robot.HoldAndTwist(1);
		robot.Cubetwist(2);
		robot.Armtwist(1);
		robot.HoldAndTwist(1);
		robot.Armtwist(1);
		robot.HoldAndTwistInvert(1);
		robot.Armtwist(1);
		robot.CubetwistInvert(1);
	}
	
	public void secondCornerFinish(){
		short[] cornerCubieChecker = new short[4];

		for (short i = 0; i < 8; i++) {
			// First one will be the BLUE-ORANGE-YELLOW-Cubie
			if (robot.cornerCubie[i].colorCheck(Color.BLUE, Color.ORANGE, Color.YELLOW)) {
				cornerCubieChecker[0] = i;
			}
			// 2. BLUE-ORANGE-WHITE Cubie
			if (robot.cornerCubie[i].colorCheck(Color.BLUE, Color.ORANGE, Color.WHITE)) {
				cornerCubieChecker[1] = i;
			}
			// 3. GREEN-ORANGE-WHITE.Cubie
			if (robot.cornerCubie[i].colorCheck(Color.GREEN, Color.ORANGE, Color.WHITE)) {
				cornerCubieChecker[2] = i;
			}
			// 4. GREEN-ORANGE-YELLOW-Cubie
			if (robot.cornerCubie[i].colorCheck(Color.GREEN, Color.ORANGE, Color.YELLOW)) {
				cornerCubieChecker[3] = i;
			}
		}
		CornerCubie cc0 = robot.cornerCubie[cornerCubieChecker[0]];
		CornerCubie cc1 = robot.cornerCubie[cornerCubieChecker[1]];
		CornerCubie cc2 = robot.cornerCubie[cornerCubieChecker[2]];
		CornerCubie cc3 = robot.cornerCubie[cornerCubieChecker[3]];
		
		if(cc0.hasOrientation(Direction.FRONT, Direction.UP, Direction.RIGHT)){
			robot.CubetwistInvert(1);
			cc1 = robot.cornerCubie[cornerCubieChecker[1]];
			cc2 = robot.cornerCubie[cornerCubieChecker[2]];
			cc3 = robot.cornerCubie[cornerCubieChecker[3]];
			
			if(cc1.hasOrientation(Direction.RIGHT, Direction.UP, Direction.FRONT)){
				robot.CubetwistInvert(1);
				cc2 = robot.cornerCubie[cornerCubieChecker[2]];
				cc3 = robot.cornerCubie[cornerCubieChecker[3]];
				
				if(cc2.hasOrientation(Direction.FRONT, Direction.UP, Direction.RIGHT)){
					robot.CubetwistInvert(1);
					cc3 = robot.cornerCubie[cornerCubieChecker[3]];
					
					if(cc3.hasOrientation(Direction.RIGHT, Direction.UP, Direction.FRONT)){
						System.out.println("Cube solved. Thank you!");
						wait(10000);
						System.exit(0);
					}
					while(!cc3.hasOrientation(Direction.RIGHT, Direction.UP, Direction.FRONT)){
						cornerTwister();
						cc3 = robot.cornerCubie[cornerCubieChecker[3]];
					}
					
					System.out.println("Cube solved. Thank you!");
					wait(10000);
					System.exit(0);
				}
				while(!cc2.hasOrientation(Direction.FRONT, Direction.UP, Direction.RIGHT)){
					cornerTwister();
					
					cc2 = robot.cornerCubie[cornerCubieChecker[2]];
				}
				
				robot.Armtwist(2);
				robot.HoldAndTwist(1);
				robot.Armtwist(2);

				cc3 = robot.cornerCubie[cornerCubieChecker[3]];
				
				while(!cc3.hasOrientation(Direction.RIGHT, Direction.UP, Direction.FRONT)){
					cornerTwister();
					
					cc3 = robot.cornerCubie[cornerCubieChecker[3]];
				}
				
				robot.Armtwist(2);
				robot.HoldAndTwistInvert(1);
								
				System.out.println("Cube solved. Thank you!");
				wait(10000);
				System.exit(0);
			}

			while(!cc1.hasOrientation(Direction.RIGHT, Direction.UP, Direction.FRONT)){
				cornerTwister();

				cc1 = robot.cornerCubie[cornerCubieChecker[1]];
			}
			
			robot.Armtwist(2);
			robot.HoldAndTwist(1);
			robot.Armtwist(2);

			cc2 = robot.cornerCubie[cornerCubieChecker[2]];
			cc3 = robot.cornerCubie[cornerCubieChecker[3]];
			while(!cc2.hasOrientation(Direction.FRONT, Direction.UP, Direction.RIGHT)){
				cornerTwister();

				cc2 = robot.cornerCubie[cornerCubieChecker[2]];
			}
			
			robot.Armtwist(2);
			robot.HoldAndTwist(1);
			robot.Armtwist(2);


			cc3 = robot.cornerCubie[cornerCubieChecker[3]];
			while(!cc3.hasOrientation(Direction.RIGHT, Direction.UP, Direction.FRONT)){
				cornerTwister();

				cc3 = robot.cornerCubie[cornerCubieChecker[3]];
			}
			
			robot.Armtwist(2);
			robot.HoldAndTwistInvert(2);
							
			System.out.println("Cube solved. Thank you!");
			wait(10000);
			System.exit(0);
			
		}else{
			
			while(!cc0.hasOrientation(Direction.FRONT, Direction.UP, Direction.RIGHT)){
				cornerTwister();

				cc0 = robot.cornerCubie[cornerCubieChecker[0]];
			}
			
			robot.Armtwist(2);
			robot.HoldAndTwist(1);
			robot.Armtwist(2);
			
			cc1 = robot.cornerCubie[cornerCubieChecker[1]];
			cc2 = robot.cornerCubie[cornerCubieChecker[2]];
			cc3 = robot.cornerCubie[cornerCubieChecker[3]];
			while(!cc1.hasOrientation(Direction.RIGHT, Direction.UP, Direction.FRONT)){
				cornerTwister();

				cc1 = robot.cornerCubie[cornerCubieChecker[1]];
			}
			
			robot.Armtwist(2);
			robot.HoldAndTwist(1);
			robot.Armtwist(2);

			cc2 = robot.cornerCubie[cornerCubieChecker[2]];
			cc3 = robot.cornerCubie[cornerCubieChecker[3]];
			while(!cc2.hasOrientation(Direction.FRONT, Direction.UP, Direction.RIGHT)){
				cornerTwister();

				cc2 = robot.cornerCubie[cornerCubieChecker[2]];
			}
			
			robot.Armtwist(2);
			robot.HoldAndTwist(1);
			robot.Armtwist(2);

			cc3 = robot.cornerCubie[cornerCubieChecker[3]];
			while(!cc3.hasOrientation(Direction.RIGHT, Direction.UP, Direction.FRONT)){
				cornerTwister();

				cc3 = robot.cornerCubie[cornerCubieChecker[3]];
			}
			
			robot.Armtwist(2);
			robot.HoldAndTwist(1);
							
			System.out.println("Cube solved. Thank you!");
			wait(10000);
			System.exit(0);
		}
		
	}
	
	private void wait(int i){
		try{
			Thread.sleep(i);
		}catch(Exception e){}
	}
}
