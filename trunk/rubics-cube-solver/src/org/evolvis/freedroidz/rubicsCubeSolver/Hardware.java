package org.evolvis.freedroidz.rubicsCubeSolver;

public class Hardware {
	IColorSensor colorSensor;
	
	public IColorSensor getColorSensor() {
		return colorSensor;
	}
	public void setColorSensor(IColorSensor colorSensor) {
		this.colorSensor = colorSensor;
	}
	public IMotor getMotorA() {
		return motorA;
	}
	public void setMotorA(IMotor motorA) {
		this.motorA = motorA;
	}
	public IMotor getMotorB() {
		return motorB;
	}
	public void setMotorB(IMotor motorB) {
		this.motorB = motorB;
	}
	public IMotor getMotorC() {
		return motorC;
	}
	public void setMotorC(IMotor motorC) {
		this.motorC = motorC;
	}
	IMotor motorA ;
	IMotor motorB ;
	IMotor motorC ;

}
