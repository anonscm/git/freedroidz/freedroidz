package org.evolvis.freedroidzEclipsePlugin.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.evolvis.freedroidzEclipsePlugin.wizards.NewProjectWizard;

public class ProjectHandler extends AbstractHandler{

	private static final String LAUNCH_GROUP = "freedroidz-eclipse-plugin.launchGroup";
	
	@Override
	public Object execute(ExecutionEvent arg0) throws ExecutionException {
		

		new NewProjectWizard();
		return null;
	}

}
