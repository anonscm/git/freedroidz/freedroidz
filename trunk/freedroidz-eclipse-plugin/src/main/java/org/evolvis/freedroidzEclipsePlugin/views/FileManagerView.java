package org.evolvis.freedroidzEclipsePlugin.views;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;
import org.evolvis.freedroidz.ui.FileManagerPanel;

public class FileManagerView extends ViewPart{

	@Override
	public void createPartControl(Composite arg0) {
		new FileManagerPanel(arg0);
	}

	@Override
	public void setFocus() {
	}

}
