package org.evolvis.freedroidzEclipsePlugin.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.evolvis.freedroidz.ui.RemoteControlPanel;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class RemoteHandler extends AbstractHandler {
	/**
	 * The constructor.
	 */
	public RemoteHandler() {
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Shell dialog = new Shell (Display.getDefault());
		dialog.setText ("NXT Fernbedienung");
		dialog.setBounds(Display.getDefault().getBounds().width/2-180,Display.getDefault().getBounds().height/2-125,360,250);
		dialog.setLayout(new FillLayout());

			RemoteControlPanel remoteControlPanel = new RemoteControlPanel(dialog);

		if(!dialog.isDisposed())
			dialog.open ();
		
		return null;
	}
}
