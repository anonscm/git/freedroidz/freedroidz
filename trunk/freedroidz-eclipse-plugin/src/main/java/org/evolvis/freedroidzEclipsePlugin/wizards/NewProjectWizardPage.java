package org.evolvis.freedroidzEclipsePlugin.wizards;

import net.miginfocom.swt.MigLayout;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.dialogs.IDialogPage;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * The "New" wizard page allows setting the container for the new file as well
 * as the file name. The page will only accept file name without the extension
 * OR with the extension that matches the expected one (mpe).
 */

public class NewProjectWizardPage extends WizardPage {

	private Text projectText;
	private Button checkButton;

	private ISelection selection;

	/**
	 * Constructor for SampleNewWizardPage.
	 * 
	 * @param pageName
	 */
	public NewProjectWizardPage(ISelection selection) {
		super("wizardPage");
		setTitle("freedroidz Projekt");
		setDescription("Dieser Wizard hilft dir, ein freedroidz Projekt zu erstellen");
		this.selection = selection;
	}

	/**
	 * @see IDialogPage#createControl(Composite)
	 */
	public void createControl(Composite parent) {
		
		Composite standartComp = new Composite(parent, SWT.BORDER);
		standartComp.setLayout(new MigLayout("","[20%!,fill][60%!,fill][15%!,fill]","")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		
		Label headlineLabel = new Label(standartComp, 0);
		headlineLabel.setFont(new Font(Display.getDefault(), new FontData("Sans", 10, SWT.BOLD))); //$NON-NLS-1$
		headlineLabel.setText("Bitte gib den Namen deines Projektes an:"); //$NON-NLS-1$
		headlineLabel.setLayoutData("wrap"); //$NON-NLS-1$
		headlineLabel.setLayoutData("span, center"); //$NON-NLS-1$
		
		Label firstSeperator = new Label(standartComp,SWT.SEPARATOR | SWT.HORIZONTAL);
		firstSeperator.setLayoutData("span, growx, wrap 20"); //$NON-NLS-1$
		
		Label sourceFilenameLabel = new Label(standartComp, 0);
		sourceFilenameLabel.setText("Projektname:"); //$NON-NLS-1$
		
		projectText= new Text(standartComp, SWT.SINGLE | SWT.BORDER);		
		projectText.setText("");//$NON-NLS-1$ //$NON-NLS-2$
		projectText.setLayoutData("width 60%!, wrap 20");
		projectText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});
		
		checkButton = new Button(standartComp, SWT.CHECK);
		checkButton.setText("Füge dem Projekt eine Beispielsklasse als Grundlage hinzu\n(empfohlen).");
		checkButton.setSelection(true);
		checkButton.setLayoutData("width 90%!");
		
		initialize();
		dialogChanged();
		setControl(standartComp);
	}

	/**
	 * Tests if the current workbench selection is a suitable container to use.
	 */

	private void initialize() {
		if (selection != null && selection.isEmpty() == false
				&& selection instanceof IStructuredSelection) {
			IStructuredSelection ssel = (IStructuredSelection) selection;
			if (ssel.size() > 1)
				return;
			Object obj = ssel.getFirstElement();
			if (obj instanceof IResource) {
				IContainer container;
				if (obj instanceof IContainer)
					container = (IContainer) obj;
				else
					container = ((IResource) obj).getParent();
			}
		}
		projectText.setText("");
	}

	/**
	 * Ensures that projectName is set.
	 */

	private void dialogChanged() {
		String projectName = getProjectName();

		if (projectName.length() == 0) {
			updateStatus("Bitte gib einen Namen an!");
			return;
		}
		if (projectName.replace('\\', '/').indexOf('/', 1) > 0) {
			updateStatus("Bitte gib einen gültigen Namen an!");
			return;
		}
		updateStatus(null);
	}

	private void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);
	}


	public String getProjectName() {
		return projectText.getText();
	}
	
	public boolean getCheckBox() {
		return checkButton.getSelection();
	}
}