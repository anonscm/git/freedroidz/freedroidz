/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.tarent.android.freedroidz;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import lejos.pc.comm.NXTComm;
import lejos.pc.comm.NXTCommAndroid;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

/**
 * This class does all the work for setting up and managing Bluetooth
 * connections with other devices. It has a thread that listens for incoming
 * connections, a thread for connecting with a device, and a thread for
 * performing data transmissions when connected.
 */
public class BluetoothService {
    // Constants that indicate the current connection state
    public static final int STATE_NONE = 0; // we're doing nothing
    public static final int STATE_LISTEN = 1; // now listening for incoming
					      // connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing
						  // connection
    public static final int STATE_CONNECTED = 3; // now connected to a remote
						 // device
    public static final int STATE_ERROR = 4; // on error

    // Debugging
    private static final String TAG = "BluetoothService";
    private static final boolean D = true;

    // Member fields
    private final BluetoothAdapter mAdapter;
    private final Handler mHandler;
    private ConnectedThread mConnectedThread;
    private int mState;

    /**
     * Constructor. Prepares a new BluetoothChat session.
     * 
     * @param context
     *            The UI Activity Context
     * @param handler
     *            A Handler to send messages back to the UI Activity
     */
    public BluetoothService(Handler handler) {
	mAdapter = BluetoothAdapter.getDefaultAdapter();
	mState = STATE_NONE;
	mHandler = handler;
    }

    /**
     * Set the current state of the chat connection
     * 
     * @param state
     *            An integer defining the current connection state
     */
    private synchronized void setState(int state) {
	if (D)
	    Log.d(TAG, "setState() " + mState + " -> " + state);
	mState = state;

	// Give the new state to the Handler so the UI Activity can update
	mHandler.obtainMessage(Freedroidz.MESSAGE_STATE_CHANGE, state, -1).sendToTarget();
    }

    /**
     * Return the current connection state.
     */
    public synchronized int getState() {
	return mState;
    }

    /**
     * Start the chat service. Specifically start AcceptThread to begin a
     * session in listening (server) mode. Called by the Activity onResume()
     */
    public synchronized void start() {
	if (D)
	    Log.d(TAG, "start");

	// Cancel any thread currently running a connection
	if (mConnectedThread != null) {
	    mConnectedThread.cancel();
	    mConnectedThread = null;
	}

	setState(STATE_LISTEN);
    }

    /**
     * Start the ConnectThread to initiate a connection to a remote device.
     * 
     * @param device
     *            The BluetoothDevice to connect
     * 
     * @returns if device is connected
     */
    public synchronized boolean connect(BluetoothDevice device) {
	if (D)
	    Log.d(TAG, "connect to: " + device);

	// Cancel any thread currently running a connection
	if (mConnectedThread != null) {
	    mConnectedThread.cancel();
	    mConnectedThread = null;
	}

	// connect
	setState(STATE_CONNECTING);
	NXTComm nxtComm = connectNoThread(device);
	if (nxtComm != null) {
	    return connected(nxtComm);
	} else {
	    connectionFailed();

	    setState(STATE_NONE);

	    return false;
	}
    }

    /**
     * Start the ConnectedThread to begin managing a Bluetooth connection
     * 
     * @param socket
     *            The BluetoothSocket on which the connection was made
     * @param device
     *            The BluetoothDevice that has been connected
     */
    public synchronized boolean connected(NXTComm nxtComm) {
	if (D)
	    Log.d(TAG, "connected");

	// Cancel any thread currently running a connection
	if (mConnectedThread != null) {
	    mConnectedThread.cancel();
	    mConnectedThread = null;
	}

	// Start the thread to manage the connection and perform transmissions
	mConnectedThread = new ConnectedThread(nxtComm);
	mConnectedThread.start();

	// Send the name of the connected device back to the UI Activity
	Message msg = mHandler.obtainMessage(Freedroidz.MESSAGE_DEVICE_NAME);
	Bundle bundle = new Bundle();
	bundle.putString(Freedroidz.DEVICE_NAME, nxtComm.toString());
	msg.setData(bundle);
	mHandler.sendMessage(msg);

	setState(STATE_CONNECTED);
	return true;
    }

    /**
     * Stop all threads
     */
    public synchronized void stop() {
	if (D)
	    Log.d(TAG, "stop");

	if (mConnectedThread != null) {
	    mConnectedThread.cancel();
	    mConnectedThread = null;
	}

	setState(STATE_NONE);
    }

    public void stopService() {
	stop();
    }

    /**
     * Write to the ConnectedThread in an unsynchronized manner
     * 
     * @param out
     *            The bytes to write
     * @see ConnectedThread#write(byte[])
     */
    public void write(byte[] out) {
	// Create temporary object
	ConnectedThread r;
	// Synchronize a copy of the ConnectedThread
	synchronized (this) {
	    if (mState != STATE_CONNECTED)
		return;
	    r = mConnectedThread;
	}
	// Perform the write unsynchronized
	r.write(out);
    }

    /**
     * Indicate that the connection attempt failed and notify the UI Activity.
     */
    private void connectionFailed() {
	setState(STATE_LISTEN);

	// Send a failure message back to the Activity
	Message msg = mHandler.obtainMessage(Freedroidz.MESSAGE_TOAST);
	Bundle bundle = new Bundle();
	bundle.putString(Freedroidz.TOAST, "Unable to connect device");
	msg.setData(bundle);
	mHandler.sendMessage(msg);
    }

    /**
     * Indicate that the connection was lost and notify the UI Activity.
     */
    private void connectionLost() {
	setState(STATE_LISTEN);

	// Send a failure message back to the Activity
	Message msg = mHandler.obtainMessage(Freedroidz.MESSAGE_TOAST);
	Bundle bundle = new Bundle();
	bundle.putString(Freedroidz.TOAST, "Device connection was lost");
	msg.setData(bundle);
	mHandler.sendMessage(msg);
    }

    private NXTComm connectNoThread(BluetoothDevice device) {
	NXTComm comm = null;
	// Always cancel discovery because it will slow down a connection
	mAdapter.cancelDiscovery();

	try {

	    String mac = device.getAddress();

	    NXTInfo nxtInfo = new NXTInfo(NXTCommFactory.BLUETOOTH, "NXT", mac);

	    // We don't use the NXTCommFactory here because it doesn't know
	    // android..
	    // this might change in a future version of lejos, because
	    // NXTCommAndroid is copied from a lejos-source
	    comm = new NXTCommAndroid();

	    if (!comm.open(nxtInfo)) {
		throw new Exception("Opening of NXTCommAndroid failed");
	    }

	    Log.i(TAG, "NXTComm opened");

	} catch (Exception e) {
	    Log.e(TAG, e.getMessage());
	    return null;
	}
	return comm;
    }

    /**
     * This thread runs during a connection with a remote device. It handles all
     * incoming and outgoing transmissions.
     */
    private class ConnectedThread extends Thread {

	private final DataInputStream mmInStream;
	private final OutputStream mmOutStream;
	private final NXTComm nxtComm;

	public ConnectedThread(NXTComm nxtComm) {
	    Log.d(TAG, "create ConnectedThread");
	    this.nxtComm = nxtComm;

	    InputStream tmpIn = null;
	    OutputStream tmpOut = null;

	    // Get the BluetoothSocket input and output streams
	    try {
		tmpIn = nxtComm.getInputStream();
		tmpOut = nxtComm.getOutputStream();
	    } catch (Exception e) {
		Log.e(TAG, "temp sockets not created", e);
	    }

	    mmInStream = new DataInputStream(tmpIn);
	    mmOutStream = tmpOut;
	}

	public void run() {
	    Log.i(TAG, "BEGIN mConnectedThread");
//	    byte[] buffer = new byte[1024];
	    

	    // TODO: InputStream aus bugfixgründen deaktiviert, (JNI local
	    // reference table overflow) wird momentan eh nicht gebraucht.
	    // Keep listening to the InputStream while connected
	    
	   
	    while (true) {
		try {
		    // Read from the InputStream
		    
		    
		    String readLine = mmInStream.readLine();
		    
//		    mmInStream.read(buffer);
//		    ByteArrayOutputStream bytearray = new ByteArrayOutputStream();
//		    bytearray.write(buffer);
//		    String readLine = bytearray.toString();
//		    
		    // Send the obtained bytes to the UI Activity
		    mHandler.obtainMessage(Freedroidz.MESSAGE_READ, readLine.length() , -1, readLine).sendToTarget();
		} catch (IOException e) {
		    Log.e(TAG, "disconnected", e);
		    connectionLost();
		    break;
		}
	    }
	}

	/**
	 * Write to the connected OutStream.
	 * 
	 * @param buffer
	 *            The bytes to write
	 */
	public void write(byte[] buffer) {
	    try {
		mmOutStream.write(buffer);
		mmOutStream.flush();

		// Share the sent message back to the UI Activity
		mHandler.obtainMessage(Freedroidz.MESSAGE_WRITE, -1, -1, buffer).sendToTarget();
	    } catch (IOException e) {
		Log.e(TAG, "Exception during write", e);
		if (e.getMessage().contains("[107]")) {
		    // Endpoint not connected
		    mHandler.obtainMessage(Freedroidz.MESSAGE_TOAST, -1, -1,
			    "Remote device not available. Please press back-button.").sendToTarget();
		    stopService();
		} else if (e.getMessage().contains("Stream closed")) {
		    // stream closed
		    mHandler.obtainMessage(Freedroidz.MESSAGE_TOAST, -1, -1,
			    "Remote device has closed the connection. Please press back-button.").sendToTarget();
		    stopService();
		}
	    }
	}

	public void cancel() {
	    try {
		nxtComm.close();
	    } catch (IOException e) {
		Log.e(TAG, "close() of connect socket failed", e);
	    }
	}
    }
}