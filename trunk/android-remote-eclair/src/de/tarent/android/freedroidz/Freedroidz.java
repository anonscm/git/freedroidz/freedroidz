package de.tarent.android.freedroidz;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.util.Linkify;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Freedroidz extends Activity {
    private ImageButton discover;

    // Debugging
    private static final String TAG = "Freedroidz";

    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    private static final int CLOSE = 0;
    private static final int ABOUT = 1;
    private static final int SETTINGS = 2;

    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;

    private String mConnectedDeviceName = null;
    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the chat services
    private static BluetoothService mChatService = null;

    private MotorControl motorControl = new MotorControl();
    
    private SensorDataHandler sensorDataHandler;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

	requestWindowFeature(Window.FEATURE_NO_TITLE);
	setContentView(R.layout.starter);

	// Get local Bluetooth adapter
	mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

	// If the adapter is null, then Bluetooth is not supported
	if (mBluetoothAdapter == null) {
	    Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
	    finish();
	    return;
	}

	if (!mBluetoothAdapter.isEnabled()) {
	    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
	    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
	}

	discover = (ImageButton) findViewById(R.id.freedroidzButton);

	discover.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v) {

		if (!mBluetoothAdapter.isEnabled()) {

		    Toast.makeText(Freedroidz.this, "Bitte aktivieren Sie Bluetooth", Toast.LENGTH_LONG).show();
		} else {

		    // Launch the DeviceListActivity to see devices and do scan
		    Intent serverIntent = new Intent(Freedroidz.this, DeviceListActivity.class);
		    startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
		}
	    }
	});

	// disable screen lock
	// KeyguardManager keyguardManager = (KeyguardManager)
	// getSystemService(Activity.KEYGUARD_SERVICE);
	// KeyguardLock lock =
	// keyguardManager.newKeyguardLock(KEYGUARD_SERVICE);
	// lock.disableKeyguard();

    }

    @Override
    public void onStart() {
	super.onStart();
	// if(D) Log.e(TAG, "++ ON START ++");

	// If BT is not on, request that it be enabled.
	// setupChat() will then be called during onActivityResult
	if (!mBluetoothAdapter.isEnabled()) {
	    // Intent enableIntent = new
	    // Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
	    // startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
	    Toast.makeText(this, "Please enable Bluetooth", Toast.LENGTH_LONG).show();
	    // Otherwise, setup the chat session
	} else {
	    if (mChatService == null) {
		setupChat();
	    }
	}
    }

    public void onDestroy() {
	super.onDestroy();

	motorControl.setMotorspeedABC(0, 0, 0);

	if (mChatService != null) {
	    mChatService.stop();
	}
    }

    private void setupChat() {
	Log.d(TAG, "setupChat()");

	// Initialize the BluetoothChatService to perform bluetooth connections
	mChatService = new BluetoothService(mHandler);

    }

    /**
     * Sends a message.
     * 
     * @param message
     *            A string of text to send.
     */
    static void sendMessage(int[] motor) {
	// Check that we're actually connected before trying anything
	// if (mChatService.getState() != BluetoothChatService.STATE_CONNECTED)
	// {
	// Toast.makeText(this, "Not connected", Toast.LENGTH_SHORT).show();
	// return;
	// }

	// Check that there's actually something to send
	// if (message.length() > 0) {
	// Get the message bytes and tell the BluetoothChatService to write
	byte[] b = new byte[12];

	intToByteArray(b, 0, motor[0]);
	intToByteArray(b, 4, motor[1]);
	intToByteArray(b, 8, motor[2]);

	mChatService.write(b);

	// Reset out string buffer to zero and clear the edit text field
	// mOutStringBuffer.setLength(0);
	// mOutEditText.setText(mOutStringBuffer);
	// }
    }

    private static void intToByteArray(byte[] input, int offset, int value) {
	input[offset] = (byte) (value >>> 24);
	input[offset + 1] = (byte) (value >>> 16);
	input[offset + 2] = (byte) (value >>> 8);
	input[offset + 3] = (byte) value;
    }

    // The Handler that gets information back from the BluetoothChatService
    private final Handler mHandler = new Handler() {
	public void handleMessage(Message msg) {
	    switch (msg.what) {
	    case MESSAGE_STATE_CHANGE:
		// if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
		String message;
		switch (msg.arg1) {

		case BluetoothService.STATE_CONNECTED:
		    message = getString(R.string.state_connected);
		    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
		    break;
		case BluetoothService.STATE_CONNECTING:
		    // this is down via ProgressDialog
		    // message = getString(R.string.state_connecting);
		    // Toast.makeText(getApplicationContext(), message,
		    // Toast.LENGTH_SHORT).show();
		    break;
		case BluetoothService.STATE_LISTEN:
		    break;
		case BluetoothService.STATE_NONE:
		    message = getString(R.string.state_not_connected);
		    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
		    break;

		case BluetoothService.STATE_ERROR:
		    message = getString(R.string.state_error);
		    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
		    break;
		}

		break;
	    case MESSAGE_WRITE:
		// byte[] writeBuf = (byte[]) msg.obj;
		// construct a string from the buffer
		// String writeMessage = new String(writeBuf);
		// mConversationArrayAdapter.add("Me:  " + writeMessage);

		break;
	    case MESSAGE_READ:
		// byte[] readBuf = (byte[]) msg.obj;
		// construct a string from the valid bytes in the buffer
		// String readMessage = new String(readBuf, 0, msg.arg1);
		// mConversationArrayAdapter.add(mConnectedDeviceName+":  " +
		// readMessage);
		final String data = (String) msg.obj;
		getSensorDataHandler().incomingData(data);
		break;
	    case MESSAGE_DEVICE_NAME:
		// save the connected device's name
		mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
		Toast.makeText(getApplicationContext(), "Connected to " + mConnectedDeviceName, Toast.LENGTH_SHORT)
			.show();
		break;
	    case MESSAGE_TOAST:
		Toast.makeText(getApplicationContext(), msg.getData().getString("toast"), Toast.LENGTH_LONG).show();
		break;
	    }
	}


    };

   

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
	// if(D) Log.d(TAG, "onActivityResult " + resultCode);
	switch (requestCode) {
	case REQUEST_CONNECT_DEVICE:
	    // When DeviceListActivity returns with a device to connect
	    if (resultCode == Activity.RESULT_OK) {
		// Get the device MAC address
		final String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
		Log.d(TAG, "Connect to: " + address);

		final ProgressDialog dialog = ProgressDialog.show(this, "", getString(R.string.state_connecting), true);
		dialog.setOwnerActivity(this);

		// start a new thread in order to not block the UI
		new Thread(new Runnable() {

		    @Override
		    public void run() {
			// Get the BLuetoothDevice object
			BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);

			// Attempt to connect to the device
			if (mChatService.connect(device)) {
			    dialog.dismiss();
			    startRemoteOnNXT();
			    
			    Intent remotecontrolIntent = new Intent(Freedroidz.this, RemoteControlActivity.class);
			    startActivity(remotecontrolIntent);
			}
			dialog.dismiss();
		    }

		}).start();
	    }
	    break;

	case REQUEST_ENABLE_BT:
	    // When the request to enable Bluetooth returns
	    if (resultCode == Activity.RESULT_OK) {
		// Bluetooth is now enabled, so set up a chat session
		setupChat();
	    } else {
		// User did not enable Bluetooth or an error occured
		Log.d(TAG, "BT not enabled");
		// Toast.makeText(this, R.string.bt_not_enabled_leaving,
		// Toast.LENGTH_SHORT).show();
		finish();
	    }
	}
    }

    private void startRemoteOnNXT() {
	// TODO: Upload File and start
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	menu.add(Menu.NONE, SETTINGS, 0, getString(R.string.menu_settings));
	menu.add(Menu.NONE, ABOUT, 1, getString(R.string.menu_about));
	menu.add(Menu.NONE, CLOSE, 2, getString(R.string.menu_close));
	return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
	switch (item.getItemId()) {
	case CLOSE:
	    this.finish();
	    return true;
	case ABOUT:
	    showDialog(ABOUT);
	    return true;
	case SETTINGS:
	    Intent settingsActivity = new Intent(getBaseContext(), PreferenceScreenActivity.class);
	    startActivity(settingsActivity);
	    return true;
	}

	return false;
    }

    protected Dialog onCreateDialog(int id) {
	Dialog dialog = null;
	switch (id) {
	case ABOUT:
	    // do the work to define the pause Dialog
	    Log.d(TAG, "about<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
	    dialog = new Dialog(this);

	    dialog.setContentView(R.layout.about);
	    dialog.setTitle("About");

	    TextView text = (TextView) dialog.findViewById(R.id.text);
	    text.setText(getString(R.string.about_text));
	    Linkify.addLinks(text, Linkify.WEB_URLS);

	    ImageView image = (ImageView) dialog.findViewById(R.id.image);
	    image.setImageResource(R.drawable.freedroidz_icon);
	    dialog.show();
	    break;
	default:
	    dialog = null;
	}
	return dialog;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
	if (keyCode == KeyEvent.KEYCODE_BACK) {
	    System.out.println("Back Key pressed");
	    this.finish();
	    return true;
	} else {
	    return super.onKeyDown(keyCode, event);
	}
    }

    public static void closeBTConnection() {
	if (mChatService != null) {
	    mChatService.stop();
	}
    }

    public SensorDataHandler getSensorDataHandler() {
	if(sensorDataHandler ==null) {
	    sensorDataHandler = new SensorDataHandler();
	}
	return sensorDataHandler;
    }
}