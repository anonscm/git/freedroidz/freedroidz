package org.evolvis.freedroidz.ui;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.evolvis.freedroidz.bluetooth.BluetoothCallback;
import org.evolvis.freedroidz.bluetooth.BluetoothUtils;

public class BluetoothScanPanel implements BluetoothCallback{
	
	public Shell shell;
	
	public List list;
	
	public Button button;
	
	public String adress;
	
	public BluetoothScanPanel(){
		initGUI();
		
	}
	
	public void initGUI(){
		shell = new Shell(Display.getDefault());
		shell.setBounds(Display.getDefault().getBounds().width/2-150,Display.getDefault().getBounds().height/2-160,300,320);
		
		final BluetoothUtils bluetoothUtils = new BluetoothUtils(this);
		bluetoothUtils.startDeviceInquiry();
		
		list = new List(shell,SWT.BORDER | SWT.MULTI);
		list.setBounds(0,0,295,260);
		list.add("Searching...");
		
		button = new Button(shell, 0);
		button.setText("Scan");
		button.setBounds(0, 262, 295, 30);
		
		button.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			public void widgetSelected(SelectionEvent arg0) {
				button.setEnabled(false);
				list.removeAll();
				list.add("Searching...");
				bluetoothUtils.startDeviceInquiry();
			}
			
		});
		
		list.addMouseListener(new MouseListener(){

			public void mouseDoubleClick(MouseEvent arg0) {
				adress = bluetoothUtils.getDeviceMAC(bluetoothUtils.deviceList.get(list.getSelectionIndex()));
				shell.dispose();
				
			}

			public void mouseDown(MouseEvent arg0) {}
			public void mouseUp(MouseEvent arg0) {}
		});
		
	}
	
	public String runGUI(){
		shell.open();
		while(!shell.isDisposed())
			Display.getDefault().readAndDispatch();
		return adress;
	}
	
	public void newDeviceFound(final String device){
		Display.getDefault().asyncExec(new Runnable(){
			public void run(){
				if(list.isDisposed())
					return;
				if(list.getItem(0).equals("Searching...")){
					list.removeAll();
				}
				list.add(device);
			}
		});
	}
	
	public void searchFinished(){
		button.setEnabled(true);
		list.add("Scan finished");
	}
}
