package org.evolvis.freedroidz.ui;

import java.io.IOException;
import java.util.ResourceBundle;

import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Widget;
import org.evolvis.freedroidz.utilities.RemoteControl;

public class RemoteControlPanel extends Composite{

	
	Shell parent;
	RemoteControl remoteControl;
	
	public RemoteControlPanel(Shell parent){
		super(parent, 0);
		this.parent = parent;
		initGui();
	}
	
	public void initGui(){
		
		MessageBox messageBox = new MessageBox(parent, SWT.ICON_INFORMATION | SWT.OK);
		messageBox.setText("Important"); //$NON-NLS-1$
		messageBox.setMessage("It's necessary to run RemoteControl.nxj on your brick to use this feature."); //$NON-NLS-1$
		messageBox.open();
		
/*		BluetoothScanPanel bluetoothScanPanel = new BluetoothScanPanel();
		String adress = bluetoothScanPanel.runGUI();*/
		
		NXTInfo nxtInfo = LejosScanPanel.runGUI(NXTCommFactory.BLUETOOTH|NXTCommFactory.USB);
		
		if(nxtInfo == null){
			this.parent.dispose();
			return;
		}
			
		remoteControl = new RemoteControl(nxtInfo);
		try {
			remoteControl.initConnection();
		} catch (NXTCommException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		

		
		setLayout(new MigLayout("", "[95%!,fill]", ""));
		
		Label headlineLabel = new Label(this, 0);
		headlineLabel.setLayoutData("span, growx, growy, wrap");
		
		Font font1 = new Font(Display.getDefault(),"Arial", 12, SWT.BOLD);
		headlineLabel.setFont(font1);
		
		headlineLabel.setText("Control your robot by pressing the arrowkeys");
		
		Label firstSeperator = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		firstSeperator.setLayoutData("span, wrap");
		
		Font font2 = new Font(Display.getDefault(),"Arial", 10, SWT.NORMAL);
		
		Label mainLabel = new Label(this, 0);
		mainLabel.setLayoutData("span, wrap 20");
		mainLabel.setFont(font2);
		
		final Button reverseButton = new Button(this, SWT.CHECK);
		reverseButton.setText("Reverse motor");
		
		mainLabel.setText("Left motor	= Port A\nRight motor	= Port C\nExtended motor	= Port B\n\n[ALT]	+ Zusatzmotor\n[Strg]	- Zusatzmotor");
		
		reverseButton.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent arg0) {
				if(arg0.keyCode == SWT.ARROW_UP){
					remoteControl.foreward();
				}
				if(arg0.keyCode == SWT.ARROW_DOWN){
					remoteControl.backward();
				}
				if(arg0.keyCode == SWT.ARROW_LEFT){
					remoteControl.turnLeft();
				}
				if(arg0.keyCode == SWT.ARROW_RIGHT){
					remoteControl.turnRight();
				}
				if(arg0.keyCode == 65536){
					int[] tmp = new int[3];
					remoteControl.centerMotor(tmp, 300);
					remoteControl.flushCommand(tmp);
				}
				if(arg0.keyCode == 262144){
					int[] tmp = new int[3];
					remoteControl.centerMotor(tmp, -300);
					remoteControl.flushCommand(tmp);
				}
				
			}

			public void keyReleased(KeyEvent arg0) {
				remoteControl.stop();
			}
			
		});
		
		reverseButton.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {}

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				remoteControl.reverseDirection(reverseButton.getSelection());
			}
			
		});
		
		parent.addDisposeListener(new DisposeListener(){
			public void widgetDisposed(DisposeEvent arg0) {
				try {
					remoteControl.closeConnection();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
		});
	}
	
	public void handle(Widget widget){
		if(widget.toString().equals("Button {right}")){ //$NON-NLS-1$
			remoteControl.turnRight();
		}
		if(widget.toString().equals("Button {left}")){ //$NON-NLS-1$
			remoteControl.turnLeft();
		}
		if(widget.toString().equals("Button {stop}")){ //$NON-NLS-1$
			remoteControl.stop();
		}
		if(widget.toString().equals("Button {go}")){ //$NON-NLS-1$
			remoteControl.foreward();
		}
		if(widget.toString().equals("Button {back}")){ //$NON-NLS-1$
			remoteControl.backward();
		}
	}
}
