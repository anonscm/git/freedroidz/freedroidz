/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.freedroidzI.icommand.control;

import icommand.nxt.Motor;
import icommand.nxt.comm.NXTCommand;

import java.io.File;
import java.util.logging.Logger;

import org.evolvis.freedroidz.icommand.Services;
import org.evolvis.freedroidz.icommand.config.IcommandProperties;
import org.evolvis.freedroidz.icommand.config.RobotManager;
import org.evolvis.freedroidz.icommand.config.RobotProperties;

/**
 * 
 * This class provides an API for performing common tasks with
 * LEGO Mindstorms robots like driving, turning etc without knowing
 * the concrete assembly of the robot.
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class RobotControl {

	public static final int MAX_SPEED = 900;

	protected final static Logger logger = Logger.getLogger(RobotControl.class.getName());

	protected static RobotControl instance;

	protected RobotHardware robotHardware;

	protected File configFile;

	protected boolean connected;
	
	protected final static String NOT_CONNECTED_LOG = "Not connected to robot. Doing nothing.";

	/**
	 * Do not initialize. Use factory-method getInstance() instead
	 *
	 */
	protected RobotControl() {

	}

	public static RobotControl getInstance() {
		if(instance == null)
			instance = new RobotControl();

		return instance;
	}
	
	public void connect() {
		if(isConnected())
			disconnect();
		
		try {
			NXTCommand.open();
			NXTCommand.setVerify(true);
			connected = true;
		} catch (Exception excp) {
			
			Services.getInstance().showWarning("Could not connect to Robot.", excp);
		}
	}

	public void connect(RobotProperties robotProperties)  {
		if(robotProperties != null) {

			setRobotHardware(new RobotHardware(robotProperties));
			logger.info("Connecting to robot \"" + getRobotHardware().getRobotProperties().getProperty(RobotProperties.ROBOT_NAME)+"\"");
		} else {
			
			IcommandProperties.removeConfiguration();
			
			Services.getInstance().showWarning("Robot is not configured properly.\nPlease select File -> Configure Robot in menu");
			return;
		}
	
		connect();
	}

	public void disconnect() {
		if(!isConnected())
			return;
		
		try {
			NXTCommand.close();
			connected = false;
		} catch(Exception excp) {
			Services.getInstance().showWarning("Could not disconnect from Robot.", excp);
		}
	}

	public boolean isConnected() {
		return connected;
	}

	public static boolean isInitialized() {
		return instance != null;
	}

	public void dispose() {
		disconnect();
		instance = null;
	}

	public RobotHardware getRobotHardware() {
		if(robotHardware == null)
			robotHardware = new RobotHardware(RobotManager.getInstance().getActiveRobot());

		return robotHardware;
	}
	
	public void setRobotHardware(RobotHardware robotHardware) {
		this.robotHardware = robotHardware;
	}
	
	private int checkBoundariesForSpeed(int speed) {
		if (speed > MAX_SPEED)
			speed = MAX_SPEED;
		else if (speed < 0)
			speed = 0;
		
		return speed;
	}

	public int driveForward(int speed) {
		
		speed = checkBoundariesForSpeed(speed);
		
		driveLeftForward(speed);
		driveRightForward(speed);
		
		return speed;
	}
	
	public int driveBackward(int speed) {
		speed = checkBoundariesForSpeed(speed);
		
		driveLeftBackward(speed);
		driveRightBackward(speed);
		
		return speed;
	}

	public int driveAhead(int speed) {
		if (speed > 0)
			return driveForward(speed);
		if (speed < 0)
			return -driveBackward(-speed);
		brake();
		return 0;
	}
	
	public void driveLeftForward(int speed) {
		driveMotorForward(getRobotHardware().getMotorLeft(), speed);
	}

	public void driveLeftBackward(int speed) {
		driveMotorBackward(getRobotHardware().getMotorLeft(), speed);
	}

	public void driveRightForward(int speed) {
		driveMotorForward(getRobotHardware().getMotorRight(), speed);
	}

	public void driveRightBackward(int speed) {
		driveMotorBackward(getRobotHardware().getMotorRight(), speed);
	}
	
	public void driveMotorForward(Motor motor, int speed) {
		driveMotor(motor, speed, false);
	}
	
	public void driveMotorBackward(Motor motor, int speed) {
		driveMotor(motor, speed, true);
	}
	
	private void driveMotor(Motor motor, int speed, boolean backward) {
		motor.setSpeed(speed);
		
		if(backward ^ getRobotHardware().isMotorInverted(motor))
			motor.backward();
		else
			motor.forward();
	}
	
	public void driveLeft(int speed, boolean backward, int turnRatio) {
		turnWhileDriving(getRobotHardware().getMotorLeft(), getRobotHardware().getMotorRight(), speed, backward, turnRatio);
	}

	public void driveRight(int speed, boolean backward, int turnRatio) {
		turnWhileDriving(getRobotHardware().getMotorRight(), getRobotHardware().getMotorLeft(), speed, backward, turnRatio);
	}
	
	public void turnWhileDriving(int speed, int turnRatio) {
		turnWhileDriving(speed, speed > 0, turnRatio);
	}
	
	public void turnWhileDriving(int speed, boolean backward, int turnRatio) {
		if(turnRatio > 0)
			driveRight(speed, backward, turnRatio);
		else
			driveLeft(speed, backward, turnRatio);
	}
	
	public void turnWhileDriving(Motor slowMotor, Motor fastMotor, int speed, boolean backward, int turnRatio) {
		if(!isConnected()) {
			logger.warning(NOT_CONNECTED_LOG);
			return;
		}
			
		speed = checkBoundariesForSpeed(speed);
		
		// set fast motor to full amount of given sped
		fastMotor.setSpeed(speed);
		
		// the slower motor's speed is between 0 (turnRatio == 180) and given speed (turnRatio == 0)
		slowMotor.setSpeed(getSpeedForTurnRatio(speed, turnRatio));
		
		System.out.println("slow motor is running at speed "+getSpeedForTurnRatio(speed, turnRatio));
		
		
		if(getRobotHardware().isMotorInverted(fastMotor) ^ backward)
			fastMotor.backward();
		else
			fastMotor.forward();
		
		if(getRobotHardware().isMotorInverted(slowMotor) ^ backward)
			slowMotor.backward();
		else
			slowMotor.forward();
	}
	
	private int getSpeedForTurnRatio(int speed, int turnRatio) {
		// turnRatio must be positive
		int turnRatioAbs = Math.abs(turnRatio);
		
		// turnRatio must be between 0 and 180
		if(turnRatioAbs > 180)
			turnRatioAbs = 180;
				
		return speed - (int)(speed * ((double)turnRatioAbs / (double)180));
	}

	/**
	 * 
	 * Stops left and right motor of the robot
	 *
	 */
	public void brake() {
		if(!isConnected()) {
			logger.warning(NOT_CONNECTED_LOG);
			return;
		}
		
		getRobotHardware().getMotorLeft().stop();
		getRobotHardware().getMotorRight().stop();
	}

	/**
	 * No acceleration on left and right motor.
	 *
	 */
	public void rollOut() {
		if(!isConnected()) {
			logger.warning(NOT_CONNECTED_LOG);
			return;
		}
		
		getRobotHardware().getMotorLeft().flt();
		getRobotHardware().getMotorRight().flt();
	}

	/**
	 * Turn the robot left to the given radius
	 * 
	 * @param degrees
	 */
	public void turnLeft(int degrees) {
		turn(degrees, true);
	}

	/**
	 * Turn the robot left to the given radius
	 * 
	 * @param degrees
	 */
	public void turnRight(int degrees) {	
		turn(degrees, false);
	}

	/**
	 * Turn the robot to the given radius in the given direction
	 * @param degrees
	 * @param left - if true turn left, otherwise right
	 */
	public void turn(int degrees, boolean left) {
		if(!isConnected()) {
			logger.warning(NOT_CONNECTED_LOG);
			return;
		}
		
		long tachoCount = getRobotHardware().getRobotProperties().getFullTurnTacho() * degrees / 360;

		getRobotHardware().getMotorLeft().resetTachoCount();
		getRobotHardware().getMotorRight().resetTachoCount();
	
		getRobotHardware().getMotorLeft().rotateTo(left ? -tachoCount : tachoCount, true);
		getRobotHardware().getMotorRight().rotateTo(left ? tachoCount : -tachoCount);
	}

	/**
	 * Sets the speed of the drive-motors
	 *  
	 * @param speed
	 * @see icommand.nxt.Motor#setSpeed(int)
	 */
	public void setSpeed(int speed) {
		if(!isConnected()) {
			logger.warning(NOT_CONNECTED_LOG);
			return;
		}
		
		getRobotHardware().getMotorLeft().setSpeed(speed);
		getRobotHardware().getMotorRight().setSpeed(speed);
	}
}
