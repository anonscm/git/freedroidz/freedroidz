/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.freedroidzI.icommand.control;

import icommand.nxt.Motor;
import icommand.nxt.SensorPort;
import icommand.nxt.TouchSensor;
import icommand.nxt.UltrasonicSensor;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.evolvis.freedroidz.icommand.config.RobotProperties;

/**
 * 
 * This class abstracts the hardware of a LEGO Mindstorms robot.
 * 
 * Classes can use the API of this class to get the desired component
 * relative to the vehicle without knowing the concrete assembly of the robot.
 * 
 * For example the method getMotorLeft() returns the vehicles left-motor
 * (in driving-direction).
 * 
 * The concrete assembly for each robot is defined through an instance of
 * RobotProperties. 
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class RobotHardware {

	protected final static Logger logger = Logger.getLogger(RobotHardware.class.getName());
	protected RobotProperties props;

	protected Motor motorLeft;
	protected Motor motorRight;
	protected Motor motorExtra;

	protected Double wheelDiameter;
	protected Double wheelDistance;
	
	protected Map<Integer, TouchSensor> touchSensors;
	protected Map<Integer, UltrasonicSensor> ultrasonicSensors;
	protected UltrasonicSensor ultrasonic2;
	
	protected Boolean invertLeft;
	protected Boolean invertRight;

	public RobotHardware(RobotProperties props) {
		this.props = props;
		touchSensors = new HashMap<Integer, TouchSensor>();
		ultrasonicSensors = new HashMap<Integer, UltrasonicSensor>();
	}

	public Motor getMotorLeft() {
		if(motorLeft == null)
			motorLeft = getMotor(props.getProperty(RobotProperties.MOTOR_LEFT));

		return motorLeft;
	}

	public Motor getMotorRight() {
		if(motorRight == null)
			motorRight = getMotor(props.getProperty(RobotProperties.MOTOR_RIGHT));

		return motorRight;
	}
	
	public Motor getMotorExtra() {
		if(motorExtra == null)
			motorExtra = getMotor(props.getProperty(RobotProperties.MOTOR_EXTRA));
		
		return motorExtra;
	}
	
	public boolean isLeftMotorInverted() {
		if(invertLeft == null)
			invertLeft = Boolean.parseBoolean(props.getProperty(RobotProperties.INVERT_LEFT));
		
		return invertLeft;
	}
	
	public boolean isRightMotorInverted() {
		if(invertRight == null)
			invertRight = Boolean.parseBoolean(props.getProperty(RobotProperties.INVERT_RIGHT));
		
		return invertRight;
	}
	
	public boolean isMotorInverted(Motor motor) {
		if(motor.equals(getMotorLeft()))
			return isLeftMotorInverted();
		
		else if(motor.equals(getMotorRight()))
			return isRightMotorInverted();
		
		return false;
	}

	protected Motor getMotor(String port) {
		if(port == null)
			;
		else if(port.toLowerCase().equals("a"))
			return Motor.A;
		else if(port.toLowerCase().equals("b"))
			return Motor.B;
		else if(port.toLowerCase().equals("c"))
			return Motor.C;

		logger.warning("unexpected motor-port configuration-value: \""+port+"\".\r\n Allowed values are: \"A\", \"B\" or \"C\"");
		return null;
	}
	
	public double getWheelDiameter() {
		if(wheelDiameter == null)
			wheelDiameter = Double.parseDouble(props.getProperty(RobotProperties.WHEEL_DIAMETER));
		return wheelDiameter;
	}

	public double getWheelDistance() {
		if(wheelDistance == null)
			wheelDistance = Double.parseDouble(props.getProperty(RobotProperties.WHEEL_DISTANCE));
		return wheelDistance;
	}
	
	protected SensorPort getSensorPort(String port) {
		if(port == null)
			;
		else if(port.equals("1"))
			return SensorPort.S1;
		else if(port.equals("2"))
			return SensorPort.S2;
		else if(port.equals("3"))
			return SensorPort.S3;
		else if(port.equals("4"))
			return SensorPort.S4;
		
		logger.warning("unexpected sensor-port configuration-value: \""+port+"\".\r\n Allowed values are: \"1\", \"2\", \"3\" or \"4\"");
		return null;
	}
	
	public UltrasonicSensor getUltrasonicSensor(int number) {
		UltrasonicSensor ultrasonic = ultrasonicSensors.get(number);
		
		if(ultrasonic == null) {
			ultrasonicSensors.put(number, 
					ultrasonic = new UltrasonicSensor(getSensorPort(props.getProperty(RobotProperties.ULTRASONIC_PORT + number))));
			ultrasonic.setMetric(true);
		}
		
		return ultrasonic;
	}
	
	public TouchSensor getTouchSensor(int number) {
		TouchSensor touchSensor = touchSensors.get(number);
		
		if(touchSensor == null)
			touchSensors.put(number,
					touchSensor = new TouchSensor(getSensorPort(props.getProperty(RobotProperties.TOUCHSENSOR_PORT + number))));
		
		return touchSensor;
	}
	
	public RobotProperties getRobotProperties() {
		return props;
	}
}
