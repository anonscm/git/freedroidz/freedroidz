/*
 * freedroidz,
 * An easy to use framework to control the lego mindstorms robots with
		java.
 * Copyright (C) 2000-2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'freedroidz'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package org.evolvis.freedroidz.icommand.events;

/**
 * This class contains the loop-method which checks the sensors
 * in a given interval 
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public abstract class AbstractSensorListener implements SensorListener, Runnable {
	
	/** The interval in millisecons for checking sensor values */
	protected long interval = 10;
	
	/**
	 * This method must be implemented by SensorListeners for checking if a defined
	 * condition is fulfilled
	 * @return
	 */
	protected abstract SensorEvent checkSensorValues();
	
	/**
	 * This is the loop which checks the sensor values in defined intervals
	 */
	public void run() {
		
		while(true) {
			
			// check the sensor values
			SensorEvent event = checkSensorValues();
			
			// if a event happened, call eventOccured
			if(event != null)
				eventOccured(event);
			
			// sleep for the time defined in 'interval'
			try {
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
