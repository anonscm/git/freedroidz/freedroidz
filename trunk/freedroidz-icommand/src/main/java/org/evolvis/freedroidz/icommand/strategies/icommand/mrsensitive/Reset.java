package org.evolvis.freedroidz.icommand.strategies.icommand.mrsensitive;

import icommand.nxt.Motor;

public class Reset extends StateMachineControlState {

	public Reset(StateMachineData data) {
		super(data);
	}

	@Override
	public StateMachineControlState enter() {
		
		RobotHardware hardware = data.getHardware();
		if(hardware.isTouching(RobotHardware.UPPER)){
			return new Reset2(data);
		}
		
		hardware.move(RobotHardware.UPPER, -90,600);
		
		return null;
	}
	@Override
	public StateMachineControlState complete(int[] i) {
		RobotHardware hardware = data.getHardware();
		
	if(hardware.isComplete(RobotHardware.UPPER)){
		hardware.move(RobotHardware.UPPER, -90,600);
	}
		return null;
	}
	@Override
	public StateMachineControlState touch(int[] i) {
		RobotHardware hardware = data.getHardware();
		
		if(hardware.isTouching(RobotHardware.UPPER)){
			
			hardware.resetTachoCount(RobotHardware.UPPER);
			hardware.stop(RobotHardware.UPPER,100);
			hardware.setSpeed(RobotHardware.UPPER,0);
			return new Reset2(data);
		}
		return null;
	}
	
	@Override
	public boolean isMotorInfoRequired(int i) {
		return i==RobotHardware.UPPER;
	}
	@Override
	public boolean isSensorInfoRequired(int i) {
		return i==RobotHardware.UPPER;
	}
	
}
