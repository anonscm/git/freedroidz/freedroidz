
package org.evolvis.freedroidz.icommand.strategies.icommand.mrsensitive;

import icommand.nxt.Motor;

import org.eclipse.swt.SWT;

public class Moving extends StateMachineControlState{

	public Moving(StateMachineData data) {
		super(data);
	}

	@Override
	public StateMachineControlState enter() {
		RobotHardware hardware = data.getHardware();
		
		for(int i=0;i<3;i++){
			 hardware.stop(i,900);
			 //data.getHardware().setSpeed(i,0);
			 
			 
		}
		
		if(hardware.isTouching(RobotHardware.UPPER)){
			 return new Turn(data,-720);
		}
		if(hardware.getDistance()<=32){
			int sign = -1; 
			 return new Turn(data,sign*720);
		}
		return new Forward(data);
	}
	
	@Override
	public boolean isSensorInfoRequired(int i) {
		// TODO Auto-generated method stub
		return true;
	}

	
}
