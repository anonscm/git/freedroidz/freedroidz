package org.evolvis.freedroidz.icommand.strategies.icommand.mrsensitive;

public class Forward extends StateMachineControlState {

	public Forward(StateMachineData data) {
		super(data);
	}
	@Override
	public StateMachineControlState enter() {
		RobotHardware hw = data.getHardware();
		int dist = hw.getDistance();
		
		return new Stomp(data,new ForwardPull(data));
	}
	
	

}
