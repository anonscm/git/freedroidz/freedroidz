/**
 * 
 */
package org.evolvis.freedroidz.icommand.midi;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiFileFormat;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;


/**
 * 
 * A helper-class for parsing and formatting MIDI-files
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class MidiHelper {

	public static void main(String[] args) {
		File inFile = new File(args[0]);

		getTonesFromFile(inFile);
	}

	public static List<Pitch> getTonesFromFile(File file) {
		List<Pitch> tones = new ArrayList<Pitch>();

		try {
			MidiFileFormat format = MidiSystem.getMidiFileFormat(file);
			Sequence sequence = MidiSystem.getSequence(file);

			Track[] tracks = sequence.getTracks();

//			System.out.println(tracks.length + " Tracks");

			for (int nTrack = 0; nTrack < tracks.length; nTrack++) {

				Track   track = tracks[nTrack];

//				System.out.println("Ticks: "+track.ticks());                   

				for(int i=0; i < track.size(); i++) {
					MidiEvent event = track.get(i);

					System.out.println("Tick: "+event.getTick());

					if(event.getMessage() instanceof ShortMessage) {
						ShortMessage message = (ShortMessage)event.getMessage();

//						System.out.println("Channel: "+message.getChannel());
//						System.out.println("Command: "+getNameForCommmand((message.getCommand())));
//						System.out.println("Length: "+message.getLength());
//						System.out.println("Data1: "+message.getData1());
//						System.out.println("Data2: "+message.getData2());

						if(message.getCommand() == ShortMessage.NOTE_ON) {

							int note = message.getData1();

							System.out.println("Tone is semitone: "+isSemitone(note));
							
							tones.add(new Pitch(note));

						} else if(message.getCommand() == ShortMessage.NOTE_OFF) {

						}

					} else if(event.getMessage() instanceof MetaMessage) {

						MetaMessage message = (MetaMessage)event.getMessage();

//						System.out.println("META");
					} else
						System.out.println("Unknown message-type: "+event.getMessage());

				}
			}



		} catch (InvalidMidiDataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return tones;
	}

	public static String getNameForCommmand(int command) {

		if(command == ShortMessage.PITCH_BEND)
			return "PITCH_BEND";
		else if(command == ShortMessage.NOTE_OFF)
			return "NOTE_OFF";
		else if(command == ShortMessage.NOTE_ON)
			return "NOTE_ON";
		else if(command == ShortMessage.CONTROL_CHANGE)
			return "CONTROL_CHANGE";
		else if(command == ShortMessage.PROGRAM_CHANGE)
			return "PROGRAM_CHANGE";


		return String.valueOf(command);
	}
	
	public static boolean isSemitone(int tone) {
		int toneN = normalizeToFirstOctave(tone);
			
		if((toneN <= 4 && (toneN % 2 != 0))
				|| (toneN > 4 && (toneN % 2 == 0)))
				return true;
				
		return false;
	}
	
	public static int normalizeToFirstOctave(int tone) {
		int toneN = tone;
		
		while(toneN > 11)
			toneN -= 12;
		
		System.out.println("normalized tone "+tone+" to tone "+toneN);
			
		return toneN;
	}
}
