package org.evolvis.freedroidz.icommand.strategy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.evolvis.freedroidzI.icommand.control.RobotControl;

/**
 * The aim of this class is to simplify the implementation of concrete strategies. 
 * @author Steffi Tinder, tarent GmbH
 *
 */
public abstract class AbstractStrategy implements Strategy {
	
	private boolean run = false;
	private boolean init=true;
	
	protected List<StrategyListener> strategyListeners;

	public void run() {
		fireStrategyStarted();
		if (init){
			init();
			init = false;
		}
		startStrategy();
		run=true;
		while (run){
		}
	}
	
	public void stop() {
		fireStrategyStopped();
		stopStrategy();
		RobotControl.getInstance().brake();
		RobotControl.getInstance().disconnect();
		run=false;	
	}	
	
	/*
	 * template method pattern: the abstract methods startStrategy and stopStrategy are called by the 
	 * template methods run and stop. This makes it easier to develop a concrete Strategy. 
	 */
	
	/**
	 * Use this method for example to register the concrete class as a sensor and/or command listener like this:<br>
	 * <code>
	 * Services.getInstance().getSensorProvider().addSensorListener(this);<br>
	 * Services.getInstance().getCommandProvider().addCommandListener(this);
	 * </code> <br>
	 * If you don't need this additional behavior, just leave the method body empty. 
	 */
	public void startStrategy(){
		//method stub, do nothing
	}
	/** 
	 * Use this method for example to deregister the concrete class as a sensor and/or command listener like this:<br>
	 * <code>
	 *  Services.getInstance().getSensorProvider().removeSensorListener(this);<br>
	 *	Services.getInstance().getCommandProvider().removeCommandListener(this);
	 * </code> <br>
	 * If you don't need this additional behavior, just leave the method body empty. 
	 */
	public void stopStrategy(){
		//method stub, do nothing
	}
	
	/**
	 * If you need any sensors you should override this method and initialize them inside the body of this Method. 
	 */
	protected void init(){
		//do nothing
	}
	
	
	// Code for StrategyListeners	
	protected List<StrategyListener> getStrategyListeners() {
		if(strategyListeners == null)
			strategyListeners = new ArrayList<StrategyListener>();
		
		return strategyListeners;
	}
	
	public void fireStrategyStarted() {
		Iterator<StrategyListener> it = getStrategyListeners().iterator();
		
		while(it.hasNext())
			it.next().strategyStarted(new StrategyEvent());
	}
	
	public void fireStrategyFinished() {
	
		Iterator<StrategyListener> it = getStrategyListeners().iterator();
		
		while(it.hasNext())
			it.next().strategyFinished(new StrategyEvent());
	}
	
	public void fireStrategyStopped() {
		Iterator<StrategyListener> it = getStrategyListeners().iterator();
		
		while(it.hasNext())
			it.next().strategyStopped(new StrategyEvent());
	}

	public void addStrategyListener(StrategyListener listener) {
		getStrategyListeners().add(listener);
	}	

}
