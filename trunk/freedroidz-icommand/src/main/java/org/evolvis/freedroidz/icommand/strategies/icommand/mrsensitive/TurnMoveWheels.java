package org.evolvis.freedroidz.icommand.strategies.icommand.mrsensitive;

public class TurnMoveWheels extends StateMachineControlState {
	private int turn;

	public TurnMoveWheels(StateMachineData data, int turn) {
		super(data);
		this.turn = turn;
	}
	
	@Override
	public StateMachineControlState enter() {
		RobotHardware hardware = data.getHardware();
		hardware.move(RobotHardware.BODY, -turn, 600);
		return super.enter();
	}
	
	@Override
	public StateMachineControlState complete(int[] i) {
		RobotHardware hardware = data.getHardware();
		if(hardware.isComplete(RobotHardware.BODY)){
			return new Neutral(data,new Moving(data));
			
		}
		return null;
	}
	@Override
	public boolean isMotorInfoRequired(int i) {
		return i==RobotHardware.BODY;
	}
		
}
