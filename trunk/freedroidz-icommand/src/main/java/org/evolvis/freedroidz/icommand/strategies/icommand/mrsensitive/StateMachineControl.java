package org.evolvis.freedroidz.icommand.strategies.icommand.mrsensitive;

import java.util.Collections;
import java.util.List;
import java.util.Vector;

/**
 * Default implementation for robot control. delegates work to control states
 * like in the state pattern.
 */
public class StateMachineControl implements RobotControl {

	private StateMachineControlState state;

	
	private List<StateMachineControlState> getPath(StateMachineControlState state){		
		if(state==null){
			return new Vector<StateMachineControlState>();
		}
		List<StateMachineControlState> result = null;
		StateMachineControlState parent = state.getParentState();
		if(parent == null){			
			result = new Vector<StateMachineControlState>();			
		}
		else{
			result = getPath(parent);
		}
		result.add(state);
		return result;
	}

	private void removeCommonPrefix(List<StateMachineControlState> a,
			List<StateMachineControlState> b) {
		while(!a.isEmpty()&&!b.isEmpty()&&a.get(0)==b.get(0)) {
			a.remove(0);
			b.remove(0);
		}		
	}
	
	private void setState(StateMachineControlState newState) {
		if(newState==null){
			return;
		}
		StateMachineControlState startState = null;
		List<StateMachineControlState> currentPath = getPath(state);
		List<StateMachineControlState> newPath = getPath(newState);
		removeCommonPrefix(currentPath,newPath);
		//call exit on previous state(s)
		//starting with the innermost state.
		Collections.reverse(currentPath);
		for(StateMachineControlState s:currentPath){
			System.err.println("exit:" +s.getClass().getSimpleName());
			s.exit();
		}
		//call enter on new state(s)
		// starting with the outermost state.
		for(StateMachineControlState s:newPath){
			System.err.println("enter:" +s.getClass().getSimpleName());
			startState=s.enter();
		}
		//if the new state specifies a nested state as start state, 
		//transit to it. Continue recursively as long as there are 
		// start states specified.
		while(startState!=null&&startState!=newState){
			newState=startState;
			System.err.println("enter:" +newState.getClass().getSimpleName());
			startState=newState.enter();
		}
		this.state=newState;
	}

	
	
	


	public StateMachineControl(StateMachineControlState initialState) {
		setState(initialState);
	}

	public void complete(int[] i) {
		System.err.println("complete "+arrayToString(i));
		setState(getState().complete(i));

	}

	private String arrayToString(int[] is) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < is.length; i++) {
			if(i>0){
				sb.append(", ");
			}
			sb.append(""+i);
		}
		return sb.toString();
	}
	private StateMachineControlState getState() {

		return this.state;
	}

		

		
	public void keyDown(int code) {
		System.err.println("keyDown("+code+")");
		setState(getState().keyDown(code));

	}


	public void keyUp(int code) {
		System.err.println("keyUp("+code+")");
		setState(getState().keyUp(code));

	}

	
	public void release(int[] i) {
		System.err.println("release "+arrayToString(i));
		setState(getState().release(i));

	}

	
	public void touch(int[] i) {
		System.err.println("touch "+arrayToString(i));
		setState(getState().touch(i));

	}

	public boolean isMotorInfoRequired(int i) {
		return getState().isMotorInfoRequired( i);
	}

	public boolean isSensorInfoRequired(int i) {
		return getState().isSensorInfoRequired(i);
	}

	

}
