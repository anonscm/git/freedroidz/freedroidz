/**
 * 
 */
package org.evolvis.freedroidz.icommand.strategies;

import org.evolvis.freedroidz.icommand.strategy.AbstractStrategy;
import org.evolvis.freedroidz.icommand.strategy.Strategy;
import org.evolvis.freedroidz.icommand.strategy.StrategyManager;
import org.evolvis.freedroidzI.icommand.control.RobotControl;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class Splash extends AbstractStrategy {

	/**
	 * @see org.evolvis.freedroidz.icommand.strategy.Strategy#getDescription()
	 */
	public String getDescription() {
		return "A splash-player";
	}

	/**
	 * @see org.evolvis.freedroidz.icommand.strategy.Strategy#getName()
	 */
	public String getName() {
		return "Splash";
	}

//	/**
//	 * @see org.evolvis.freedroidz.strategy.Strategy#stop()
//	 */
//	public void stop() {
//		StrategyManager.getInstance().fireStrategyStopped();
//	}
//
//	/**
//	 * @see java.lang.Runnable#run()
//	 */
//	public void run() {
//		StrategyManager.getInstance().fireStrategyStarted();
//		
//		RobotControl.getInstance().getRobotHardware().getMotorExtra().setSpeed(900);
//		RobotControl.getInstance().getRobotHardware().getMotorLeft().setSpeed(900);
//		
//		RobotControl.getInstance().getRobotHardware().getMotorExtra().forward();
//		RobotControl.getInstance().getRobotHardware().getMotorLeft().forward();
//	}

	@Override
	public void startStrategy() {
		RobotControl.getInstance().getRobotHardware().getMotorExtra().setSpeed(900);
		RobotControl.getInstance().getRobotHardware().getMotorLeft().setSpeed(900);
		
		RobotControl.getInstance().getRobotHardware().getMotorExtra().forward();
		RobotControl.getInstance().getRobotHardware().getMotorLeft().forward();
		
	}

	@Override
	public void stopStrategy() {
		// Tdo nothing
		
	}

}
