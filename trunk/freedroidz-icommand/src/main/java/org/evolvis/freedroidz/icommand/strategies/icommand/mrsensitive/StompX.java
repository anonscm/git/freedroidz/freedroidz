package org.evolvis.freedroidz.icommand.strategies.icommand.mrsensitive;

public class StompX extends StateMachineControlState {

	public StompX(StateMachineData data) {
		super(data);
	}

	@Override
	public StateMachineControlState enter() {
		RobotHardware hw = data.getHardware();
		hw.move(RobotHardware.UPPER, 60, 800);
		return super.enter();
	}
}
