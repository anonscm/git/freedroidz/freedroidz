package org.evolvis.freedroidz.icommand.strategies.icommand.mrsensitive;

import java.util.logging.Logger;

import org.evolvis.freedroidz.icommand.Services;
import org.evolvis.freedroidz.icommand.command.Command;
import org.evolvis.freedroidz.icommand.strategies.SensorTest;
import org.evolvis.freedroidz.icommand.strategy.AbstractStrategy;
import org.evolvis.freedroidz.icommand.strategy.Strategy;
import org.evolvis.freedroidz.icommand.strategy.StrategyListener;
import org.evolvis.freedroidz.icommand.strategy.StrategyManager;
import org.evolvis.freedroidzI.icommand.control.RobotHardware;
public class MrSensitive extends AbstractStrategy {
	private static class _Event{
		Command cmd;
		boolean released;
	}
	
	private boolean run = false;

	private MrSensitiveHardware hardware;

	private StateMachineControl control;
	
	protected final static Logger logger = Logger.getLogger(SensorTest.class
			.getName());

	
	public String getDescription() {

		return "A: turn left/right\n" + "B: arm back/forth\n"
				+ "C: arm up/down\n" + "S1: light (pressure)";

	}

	public String getName() {
		return "Mr Sensitive";
	}

	public void stop() {
		run = false;
	}

	public void run() {
		hardware = new MrSensitiveHardware();
		
		control = new StateMachineControl(new Reset2(new StateMachineData(hardware)));
		logger.fine("SensorTesting: registering myself as SensorListener");
		Services.getInstance().getCommandProvider().addCommandListener(hardware);
		fireStrategyStarted();
		
		run = true;
		while (run) {
			hardware.dispatch(control, System.currentTimeMillis());
		}
		hardware.cleanup();
		fireStrategyStopped();
	}

	@Override
	public void startStrategy() {
		// TODO change implementation to template method pattern
		
	}

	@Override
	public void stopStrategy() {
		// TODO change implementation to template method pattern
		
	}

		
}