
package org.evolvis.freedroidz.icommand.strategies;

import org.evolvis.freedroidz.icommand.strategy.AbstractStrategy;
import org.evolvis.freedroidz.icommand.strategy.StrategyManager;
import org.evolvis.freedroidzI.icommand.control.RobotControl;

/**
 * 
 * 
 * @author Pascal Puth (p.puth@tarent.de) tarent GmbH Bonn
 *
 */
public class DriveSquare extends AbstractStrategy {

	/**
	 * @see org.evolvis.freedroidz.icommand.strategy.Strategy#getDescription()
	 */
	public String getDescription() {
		return "Drives the robot along a square";
	}

	/**
	 * @see org.evolvis.freedroidz.icommand.strategy.Strategy#getName()
	 */
	public String getName() {
		return "Drive Square";
	}

	/**
	 * @see org.evolvis.freedroidz.icommand.strategy.Strategy#stop()
	 */
	public void stop() {
		// first stop the robot
		RobotControl.getInstance().brake();
		
		// then release the motors
		RobotControl.getInstance().rollOut();
		
		fireStrategyStopped();
	}

	/**
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		fireStrategyStarted();
		
		RobotControl.getInstance().driveForward(300);
		RobotControl.getInstance().turnRight(90);
		RobotControl.getInstance().driveForward(300);
		RobotControl.getInstance().turnRight(90);
		RobotControl.getInstance().driveForward(300);
		RobotControl.getInstance().turnRight(90);
		RobotControl.getInstance().driveForward(300);
		
		fireStrategyFinished();
	}
	

}
