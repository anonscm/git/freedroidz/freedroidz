package org.evolvis.freedroidz.icommand.strategies;

import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.evolvis.freedroidz.icommand.Services;
import org.evolvis.freedroidz.icommand.command.Command;
import org.evolvis.freedroidz.icommand.command.CommandListener;
import org.evolvis.freedroidz.icommand.strategy.AbstractStrategy;
import org.evolvis.freedroidzI.icommand.control.RobotControl;

public class RemoteControlWithoutAdditionalUI extends AbstractStrategy implements CommandListener{

	protected static final Logger logger = Logger.getLogger(RemoteControlWithoutAdditionalUI.class.getName());
	
	protected void init() {
		// no sensors needed		
	}

	public void startStrategy() {
		Services.getInstance().getCommandProvider().addCommandListener(this);
		
	}

	public void stopStrategy() {
		RobotControl.getInstance().brake();
		RobotControl.getInstance().getRobotHardware().getMotorExtra().stop();
		Services.getInstance().getCommandProvider().removeCommandListener(this);
		
	}
	
	public String getDescription() {
		return "Simple remote control strategy\n";
	}

	public String getName() {
		return "Remote Control";
	}

	public void commandSent(Command command) {
		logger.info("processing "+command);
		processCommand(command);
	}

	public void commandReleased(Command command){
		//do nothing	
	}
	
	/**
	 * The current speed of the robot. If negative robot is driving backwards
	 */
	private int currentSpeed = 0;
	/**
	 * The current turn of the robot in degrees. If negative robot is turning left, otherwise right. 
	 */
	private int currentTurnRatio = 0;

	/**
	 * The speed is in-/decreased by this constant per step.
	 */
	private int speedPerStep = 100;
	/**
	 * The turning is in-/decreased by this constant per step.
	 */
	private int turnWhileMovingStep = 20;

	/**
	 * The robot turns by this degrees if it is not moving.
	 */
	private int turnWhileStandingInDegrees = 25;

	/**
	 * The maximum speed of the robot.
	 */
	private int maximumSpeed = 900;


	private void faster() {
		if(currentSpeed < maximumSpeed)
			currentSpeed += speedPerStep;
	}

	private void slower() {
		if(currentSpeed > -maximumSpeed)
			currentSpeed -= speedPerStep;
	}

	private void turnLeft() {
		if(currentTurnRatio > -180)
			currentTurnRatio -= turnWhileMovingStep;
	}

	private void turnRight() {
		if(currentTurnRatio < 180)
			currentTurnRatio += turnWhileMovingStep;
	}

	private void driveForOrBackward(int speed){
		if (speed > 0)
			RobotControl.getInstance().driveForward(speed);

		else if (speed < 0)
			RobotControl.getInstance().driveBackward(-speed);

		else
			RobotControl.getInstance().brake();
	}

	private void processCommand(Command command){

		/*
		 * Available commands on a Nokia Internet Tablet:
		 * 
		 * Key			-> Keycode
		 * 
		 * Home  		-> 16777230
		 * Fullscreen	-> 16777231
		 * Plus			-> 16777232
		 * Minus		-> 16777233
		 * Back			-> 27 (Escape) 
		 * 
		 */

		switch (command.getCode()) {

		case SWT.ARROW_UP:
			if (currentTurnRatio == 0)
				faster();

			currentTurnRatio = 0;
			driveForOrBackward(currentSpeed);

			break;

		case SWT.ARROW_DOWN:
			if (currentTurnRatio == 0)
				slower();

			currentTurnRatio = 0;
			driveForOrBackward(currentSpeed);

			break;

		case SWT.ARROW_LEFT:
			turnLeft();

			if(currentSpeed == 0)
				RobotControl.getInstance().turnLeft(turnWhileStandingInDegrees);

			else
				RobotControl.getInstance().turnWhileDriving(Math.abs(currentSpeed), currentSpeed < 0, currentTurnRatio);

			break;

		case SWT.ARROW_RIGHT:
			turnRight();

			if(currentSpeed == 0)
				RobotControl.getInstance().turnRight(turnWhileStandingInDegrees);

			else
				RobotControl.getInstance().turnWhileDriving(Math.abs(currentSpeed), currentSpeed < 0, currentTurnRatio);

			break;

		case SWT.ESC:
		case ' ': // Space-bar
			currentTurnRatio = 0;
			driveForOrBackward(currentSpeed = 0);
			break;

		case 'y':
		case 16777233:

			RobotControl.getInstance().getRobotHardware().getMotorExtra().backward();
			break;

		case 'c':
		case 16777232:

			RobotControl.getInstance().getRobotHardware().getMotorExtra().forward();
			break;

		case 'x':

			RobotControl.getInstance().getRobotHardware().getMotorExtra().stop();
			break;

		default:
			System.out.println(command.getCode());
		}
	}

}
