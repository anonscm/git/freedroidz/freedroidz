package org.evolvis.freedroidz.icommand.strategies.icommand.mrsensitive;

public class Backward extends StateMachineControlState {

	public Backward(StateMachineData data) {
		super(data);
	}
	@Override
	public StateMachineControlState enter() {
		//start with to stomp
		return new Stomp(data,new BackwardPush(data));
	}
	
	

}
