/**
 * 
 */
package org.evolvis.freedroidz.icommand.midi;

/**
 * 
 * Bean which represents a pitch in music.
 * See http://en.wikipedia.org/wiki/Pitch_(music)
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class Pitch {

	int note;

	public Pitch(int note) {
		this.note = note;
	}
	
	public int getNote() {
		return note;
	}
}
