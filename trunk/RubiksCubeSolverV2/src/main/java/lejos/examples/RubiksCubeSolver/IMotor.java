package lejos.examples.RubiksCubeSolver;

public interface IMotor {

	public abstract void setPower(int i);
	public abstract void setSpeed(int i);

	public abstract void rotate(int i);
	
	public void rotateTo(int i);

}