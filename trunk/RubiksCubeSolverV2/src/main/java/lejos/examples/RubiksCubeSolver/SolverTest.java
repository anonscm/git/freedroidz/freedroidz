package lejos.examples.RubiksCubeSolver;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import lejos.examples.RubiksCubeSolver.Cube.CornerCubie;
import lejos.examples.RubiksCubeSolver.Cube.EdgeCubie;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;



public class SolverTest {

	private final class DebugAnswer implements Answer<Object> {
		private String string;

		public DebugAnswer(String string) {
			super();
			this.string = string;
		}

		@Override
		public Object answer(InvocationOnMock invocation) throws Throwable {			
			System.out.println(string);
			return string;
		}
	}

	protected static final int MAX_STEPS = 50000;

	private Solver solver;
	
	@Mock
	private IMotor motorB;
	@Mock
	private IMotor motorC;
	
	private Robot robot;

	protected int counter;

	@Before
	public void setup() throws Throwable{
		MockitoAnnotations.initMocks(this);
		doAnswer(new DebugAnswer("c rotate")).when(motorC).rotate(anyInt());
		doAnswer(new DebugAnswer("c setPower")).when(motorC).setPower(anyInt());
		doAnswer(new DebugAnswer("b rotate")).when(motorB).rotate(anyInt());
		doAnswer(new DebugAnswer("b setPower")).when(motorB).setPower(anyInt());
		CubeFactory factory = new CubeFactory();
		robot=new Robot(factory, motorB,motorC);
		robot.setWaitBetweenSteps(0);
		this.solver = new Solver(robot);		
	}
	
	
	@Test
	public void testBla() throws Throwable{
		
		solver.firstCrossAlgorithm();
		solver.firstCornerAlgorithm();
		solver.secondLayerAlgorithm();
		solver.secondCrossAlgorithm();
		solver.secondEdgeAlgorithm();
		solver.secondCornerAlgorithm();
		solver.secondCornerFinish();
		
	}
	
	
	
	//@Test
	public void sort_edge_cubie_alreadySorted() throws Throwable{
		CubeFactory factory = new CubeFactory();
		Cube cube = factory.createCube();
		EdgeCubie cubie = factory.createEdgeCubie(cube)[0];
		cubie.Color1="A";
		cubie.Color2="B";
		cubie.Color1Position="1";
		cubie.Color2Position="2";
		cubie.colorSort();
		assertEquals("A", cubie.Color1);
		assertEquals("B", cubie.Color2);
		assertEquals("1", cubie.Color1Position);
		assertEquals("2", cubie.Color2Position);
	}
	//@Test
	public void sort_edge_cubie_notSorted() throws Throwable{
		CubeFactory factory = new CubeFactory();
		Cube cube = factory.createCube();
		EdgeCubie cubie = factory.createEdgeCubie(cube)[0];
		cubie.Color1="B";
		cubie.Color2="A";
		cubie.Color1Position="1";
		cubie.Color2Position="2";
		cubie.colorSort();
		assertEquals("A", cubie.Color1);
		assertEquals("B", cubie.Color2);
		assertEquals("2", cubie.Color1Position);
		assertEquals("1", cubie.Color2Position);
	}
	
	//@Test
	public void corner_cubie_alreadySorted() throws Throwable{
		CubeFactory factory = new CubeFactory();
		Cube cube = factory.createCube();
		CornerCubie cubie = factory.createCornerCubie(cube)[0];
		cubie.Color1="A";
		cubie.Color2="B";
		cubie.Color3="C";
		cubie.Color1Position="1";
		cubie.Color2Position="2";
		cubie.Color3Position="3";
		cubie.colorSort();
		assertEquals("A", cubie.Color1);
		assertEquals("B", cubie.Color2);
		assertEquals("C", cubie.Color3);
		assertEquals("1", cubie.Color1Position);
		assertEquals("2", cubie.Color2Position);
		assertEquals("3", cubie.Color3Position);
	}
	
	//@Test
	public void corner_cubie_rot1() throws Throwable{
		CubeFactory factory = new CubeFactory();
		Cube cube = factory.createCube();
		CornerCubie cubie = factory.createCornerCubie(cube)[0];
		cubie.Color1="C";
		cubie.Color2="A";
		cubie.Color3="B";
		cubie.Color1Position="1";
		cubie.Color2Position="2";
		cubie.Color3Position="3";
		cubie.colorSort();
		assertEquals("A", cubie.Color1);
		assertEquals("B", cubie.Color2);
		assertEquals("C", cubie.Color3);
		assertEquals("2", cubie.Color1Position);
		assertEquals("3", cubie.Color2Position);
		assertEquals("1", cubie.Color3Position);
	}
	
	//@Test
	public void corner_cubie_reversed() throws Throwable{
		CubeFactory factory = new CubeFactory();
		Cube cube = factory.createCube();
		CornerCubie cubie = factory.createCornerCubie(cube)[0];
		cubie.Color1="C";
		cubie.Color2="B";
		cubie.Color3="A";
		cubie.Color1Position="1";
		cubie.Color2Position="2";
		cubie.Color3Position="3";
		cubie.colorSort();
		assertEquals("A", cubie.Color1);
		assertEquals("B", cubie.Color2);
		assertEquals("C", cubie.Color3);
		assertEquals("3", cubie.Color1Position);
		assertEquals("2", cubie.Color2Position);
		assertEquals("1", cubie.Color3Position);
	}
	private Object checkStepCounter() {
		if(counter++ > MAX_STEPS) {
			throw new RuntimeException("MAX STEPS exceeded.");
		}
		
		return null;
	}
	//@Test
	public void corner_cubie_duplicate() throws Throwable{
		
		CubeFactory factory = new CubeFactory();
		Cube cube = factory.createCube();
		CornerCubie cubie = factory.createCornerCubie(cube)[0];
		cubie.Color1="B";
		cubie.Color2="B";
		cubie.Color3="A";
		cubie.Color1Position="1";
		cubie.Color2Position="2";
		cubie.Color3Position="3";
		cubie.colorSort();
		assertEquals("A", cubie.Color1);
		assertEquals("B", cubie.Color2);
		assertEquals("B", cubie.Color3);
		assertEquals("3", cubie.Color1Position);
		assertEquals("1", cubie.Color2Position);
		assertEquals("2", cubie.Color3Position);
		
		
	}
	
	
}
