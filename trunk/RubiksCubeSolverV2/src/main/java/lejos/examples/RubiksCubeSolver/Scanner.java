package lejos.examples.RubiksCubeSolver;

import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;


/**
 * The Scanner-class scans the cube and saves the color of the cubies
 */
public class Scanner {
	
	//Werte sind zu ändern, wenn der Robot umgebaut wurde.
	private static final int ROTATE_APPLIANCE_90_DEGREE = -90;
	private static final int ROTATE_APPLIANCE_MINUS_90_DEGREE = 90;
	private static final int ROTATE_ARM_FORWARD_FLIP = 80;
	private static final int ROTATE_ARM_BACKWARD_FLIP = -80;
	private static final int EDGE_CUBIE_DEGREE = 15;
	private static final int EDGE_CUBIE_DEGREE_BACK = -15;
	private static final int CAMERA_ROTATE_1 = 75;
	private static final int CAMERA_ROTATE_2 = 90;
	private static final int CAMERA_ROTATE_3 = 115;

	private Robot robot;

	private final Hardware hardware;
	
	
	public Scanner(Robot robot, Hardware hw) {
		super();
		this.robot = robot;
		this.hardware = hw;
	}


	private void sleep(int i) {
		
		try
		{
			Thread.sleep(i);
		}catch(InterruptedException e)
		{}
	}


	//starts to scan
	public void scanCube()
	{
		sideOne();
		armtwist();
		sideTwo();
		cubetwist();
		armtwist();
		cubetwistInvert();
		sideThree();
		armtwist();
		cubetwist();
		sideFour();
		armtwist();
		sideFive();
		cubetwistInvert();
		armtwist();
		cubetwist();
		sideSix();
		armtwist();
		cubetwistInvert();
	}
	
	private void armtwist()
	{
//		hardware.getMotorC().setPower(1000);
//		hardware.getMotorC().setSpeed(500);

		hardware.getMotorC().rotate(ROTATE_ARM_FORWARD_FLIP);
		hardware.getMotorC().rotate(-15);
		hardware.getMotorC().rotate(15);
		hardware.getMotorC().setSpeed(80);
		hardware.getMotorC().rotate(ROTATE_ARM_BACKWARD_FLIP);
		hardware.getMotorC().setSpeed(500);
		
		
		if(new UltrasonicSensor(SensorPort.S1).getDistance()<3)
			armtwist();
	}
	
	private void cubetwist()
	{
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_90_DEGREE);
	}
	
	private void cubetwistInvert()
	{
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
	}
	
	//saves to color from the first side
	private void sideOne()
	{
		//EdgeCubie color check
		hardware.getMotorA().rotateTo(CAMERA_ROTATE_1);
		hardware.getMotorB().rotate(EDGE_CUBIE_DEGREE);
		
		sleep(500);
		getRobot().edgeCubie[0].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().edgeCubie[1].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().edgeCubie[2].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().edgeCubie[3].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
			
		hardware.getMotorB().rotate(EDGE_CUBIE_DEGREE_BACK);
		
		
		//MidCubie color check
		hardware.getMotorA().rotateTo(CAMERA_ROTATE_2);
		sleep(500);
		getRobot().midCubie[0].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		
		
		//CornerCubie color check
		hardware.getMotorA().rotateTo(CAMERA_ROTATE_3);
		
		sleep(500);
		getRobot().cornerCubie[0].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().cornerCubie[1].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().cornerCubie[2].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().cornerCubie[3].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
				
		hardware.getMotorA().rotateTo(0);
	}


	
	public Robot getRobot() {
		return robot;
	}
	
	public void setRobot(Robot robot) {
		this.robot = robot;
	}


	private void sideTwo()
	{
		//EdgeCubie color check
		hardware.getMotorA().rotateTo(CAMERA_ROTATE_1);
		hardware.getMotorB().rotate(EDGE_CUBIE_DEGREE);
		
		sleep(500);
		getRobot().edgeCubie[2].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().edgeCubie[4].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().edgeCubie[5].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().edgeCubie[6].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
			
		hardware.getMotorB().rotate(EDGE_CUBIE_DEGREE_BACK);
		
		
		//MidCubie color check
		hardware.getMotorA().rotateTo(CAMERA_ROTATE_2);
		sleep(500);
		getRobot().midCubie[1].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		
		
		//CornerCubie color check
		hardware.getMotorA().rotateTo(CAMERA_ROTATE_3);
		
		sleep(500);
		getRobot().cornerCubie[4].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().cornerCubie[5].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().cornerCubie[1].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().cornerCubie[0].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
				
		hardware.getMotorA().rotateTo(0);
	}
	
	private void sideThree()
	{
		//EdgeCubie color check
		hardware.getMotorA().rotateTo(CAMERA_ROTATE_1);
		hardware.getMotorB().rotate(EDGE_CUBIE_DEGREE);
		
		sleep(500);
		getRobot().edgeCubie[1].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().edgeCubie[10].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().edgeCubie[7].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().edgeCubie[4].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
			
		hardware.getMotorB().rotate(EDGE_CUBIE_DEGREE_BACK);
		
		
		//MidCubie color check
		hardware.getMotorA().rotateTo(CAMERA_ROTATE_2);
		sleep(500);
		getRobot().midCubie[5].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		
		
		//CornerCubie color check
		hardware.getMotorA().rotateTo(CAMERA_ROTATE_3);
		
		sleep(500);
		getRobot().cornerCubie[6].Color3 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().cornerCubie[4].Color3 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().cornerCubie[0].Color3 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().cornerCubie[3].Color3 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
				
		hardware.getMotorA().rotateTo(0);
	}
	
	private void sideFour()
	{
		//EdgeCubie color check
		hardware.getMotorA().rotateTo(CAMERA_ROTATE_1);
		hardware.getMotorB().rotate(EDGE_CUBIE_DEGREE);
		
		sleep(500);
		getRobot().edgeCubie[5].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().edgeCubie[7].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().edgeCubie[8].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().edgeCubie[9].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
			
		hardware.getMotorB().rotate(EDGE_CUBIE_DEGREE_BACK);
		
		
		//MidCubie color check
		hardware.getMotorA().rotateTo(CAMERA_ROTATE_2);
		sleep(500);
		getRobot().midCubie[2].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		
		
		//CornerCubie color check
		hardware.getMotorA().rotateTo(CAMERA_ROTATE_3);
		
		sleep(500);
		getRobot().cornerCubie[6].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().cornerCubie[7].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().cornerCubie[5].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().cornerCubie[4].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
				
		hardware.getMotorA().rotateTo(0);
	}
	
	private void sideFive()
	{
		//EdgeCubie color check
		hardware.getMotorA().rotateTo(CAMERA_ROTATE_1);
		hardware.getMotorB().rotate(EDGE_CUBIE_DEGREE);
		
		sleep(500);
		getRobot().edgeCubie[8].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().edgeCubie[10].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().edgeCubie[0].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().edgeCubie[11].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
			
		hardware.getMotorB().rotate(EDGE_CUBIE_DEGREE_BACK);
		
		
		//MidCubie color check
		hardware.getMotorA().rotateTo(CAMERA_ROTATE_2);
		sleep(500);
		getRobot().midCubie[3].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		
		
		//CornerCubie color check
		hardware.getMotorA().rotateTo(CAMERA_ROTATE_3);
		
		sleep(500);
		getRobot().cornerCubie[3].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().cornerCubie[2].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().cornerCubie[7].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().cornerCubie[6].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
				
		hardware.getMotorA().rotateTo(0);
	}
	
	private void sideSix()
	{
		//EdgeCubie color check
		hardware.getMotorA().rotateTo(CAMERA_ROTATE_1);
		hardware.getMotorB().rotate(EDGE_CUBIE_DEGREE);
		
		sleep(500);
		getRobot().edgeCubie[9].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().edgeCubie[11].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().edgeCubie[3].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().edgeCubie[6].Color2 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
			
		hardware.getMotorB().rotate(EDGE_CUBIE_DEGREE_BACK);
		
		
		//MidCubie color check
		hardware.getMotorA().rotateTo(CAMERA_ROTATE_2);
		sleep(500);
		getRobot().midCubie[4].Color1 = colorChecker(hardware.getColorSensor().getColorNumber());
		
		
		//CornerCubie color check
		hardware.getMotorA().rotateTo(CAMERA_ROTATE_3);
		
		sleep(500);
		getRobot().cornerCubie[2].Color3 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().cornerCubie[1].Color3 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().cornerCubie[5].Color3 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
		sleep(500);
		getRobot().cornerCubie[7].Color3 = colorChecker(hardware.getColorSensor().getColorNumber());
		hardware.getMotorB().rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);
				
		hardware.getMotorA().rotateTo(0);
	}
	
	private String colorChecker(int colorValue)
	{
		String colorBuffer = "null";
		
		switch(colorValue)
		{
		case 2:
			colorBuffer = Color.BLUE.toString();
			break;
			
		case 3:
			colorBuffer = Color.WHITE.toString();
			break;
			
		case 4:
			colorBuffer = Color.GREEN.toString();
			break;
			
		case 6:
			colorBuffer = Color.YELLOW.toString();
			break;
			
		case 7:
			colorBuffer = Color.ORANGE.toString();
			break;
			
		case 8:
			colorBuffer = Color.RED.toString();	//Sometimes it is ORANGE
			break;
			
		case 9:
			colorBuffer = Color.RED.toString();
			break;
			
		case 12:
			colorBuffer = Color.WHITE.toString();
			break;
			
		default:
			//Sound.playTone(1760, 2000, 100);
			System.out.println("Fehler");
			System.out.println("Roboter wird ausgeschaltet.");

			sleep(10000);
			System.exit(0);
		}
		
		return colorBuffer;
	}
	
}
