package lejos.examples.RubiksCubeSolver;

import lejos.nxt.Motor;

public class MotorWrapper implements IMotor {

	Motor motor;
	
	int motorLastState;
	
	public MotorWrapper(Motor motor) {
		super();
		this.motor = motor;
		motorLastState = motor.getTachoCount();
	}

	/* (non-Javadoc)
	 * @see lejos.examples.RubiksCubeSolver.IMotor#setPower(int)
	 */
	@Override
	public void setPower(int i) {
		motor.setPower(i);
	}
	
	@Override
	public void setSpeed(int i){
		motor.setSpeed(i);
	}

	/* (non-Javadoc)
	 * @see lejos.examples.RubiksCubeSolver.IMotor#rotate(int)
	 */
	@Override
	public void rotate(int i) {
//		System.out.println("rotate: " + i);
		motorLastState += i;
		rotate();
	}

	@Override
	public void rotateTo(int i) {
//		System.out.println("rotateTo: " + i + " (" + (i-motorLastState) + ")");
		motorLastState = i;
		rotate();
	}
	
	private void rotate() {
		motor.rotateTo(motorLastState);
		motor.stop();		
	}

}
