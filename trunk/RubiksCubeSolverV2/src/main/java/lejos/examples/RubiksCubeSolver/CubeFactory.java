package lejos.examples.RubiksCubeSolver;

import lejos.examples.RubiksCubeSolver.Cube.CornerCubie;
import lejos.examples.RubiksCubeSolver.Cube.EdgeCubie;
import lejos.examples.RubiksCubeSolver.Cube.MidCubie;


/*
 * Initializes the cube in its original state
 */

public class CubeFactory {

	Cube.CornerCubie[] createCornerCubie(Cube cube) {
		CornerCubie[] cornerCubie = new CornerCubie[8];
	
		cornerCubie[0] = cube.new CornerCubie();
		cornerCubie[0].Color1 = Color.BLUE.toString();
		cornerCubie[0].Color2 = Color.RED.toString();
		cornerCubie[0].Color3 = Color.WHITE.toString();
		cornerCubie[0].Color1Position = Direction.BACK.toString();
		cornerCubie[0].Color2Position = Direction.UP.toString();
		cornerCubie[0].Color3Position = Direction.LEFT.toString();
		cornerCubie[1] = cube.new CornerCubie();
		cornerCubie[1].Color1 = Color.BLUE.toString();
		cornerCubie[1].Color2 = Color.RED.toString();
		cornerCubie[1].Color3 = Color.YELLOW.toString();
		cornerCubie[1].Color1Position = Direction.BACK.toString();
		cornerCubie[1].Color2Position = Direction.UP.toString();
		cornerCubie[1].Color3Position = Direction.RIGHT.toString();
		cornerCubie[2] = cube.new CornerCubie();
		cornerCubie[2].Color1 = Color.GREEN.toString();
		cornerCubie[2].Color2 = Color.RED.toString();
		cornerCubie[2].Color3 = Color.YELLOW.toString();
		cornerCubie[2].Color1Position = Direction.FRONT.toString();
		cornerCubie[2].Color2Position = Direction.UP.toString();
		cornerCubie[2].Color3Position = Direction.RIGHT.toString();
		cornerCubie[3] = cube.new CornerCubie();
		cornerCubie[3].Color1 = Color.GREEN.toString();
		cornerCubie[3].Color2 = Color.RED.toString();
		cornerCubie[3].Color3 = Color.WHITE.toString();
		cornerCubie[3].Color1Position = Direction.FRONT.toString();
		cornerCubie[3].Color2Position = Direction.UP.toString();
		cornerCubie[3].Color3Position = Direction.LEFT.toString();
		cornerCubie[4] = cube.new CornerCubie();
		cornerCubie[4].Color1 = Color.BLUE.toString();
		cornerCubie[4].Color2 = Color.ORANGE.toString();
		cornerCubie[4].Color3 = Color.WHITE.toString();
		cornerCubie[4].Color1Position = Direction.BACK.toString();
		cornerCubie[4].Color2Position = Direction.DOWN.toString();
		cornerCubie[4].Color3Position = Direction.LEFT.toString();
		cornerCubie[5] = cube.new CornerCubie();
		cornerCubie[5].Color1 = Color.BLUE.toString();
		cornerCubie[5].Color2 = Color.ORANGE.toString();
		cornerCubie[5].Color3 = Color.YELLOW.toString();
		cornerCubie[5].Color1Position = Direction.BACK.toString();
		cornerCubie[5].Color2Position = Direction.DOWN.toString();
		cornerCubie[5].Color3Position = Direction.RIGHT.toString();
		cornerCubie[6] = cube.new CornerCubie();
		cornerCubie[6].Color1 = Color.GREEN.toString();
		cornerCubie[6].Color2 = Color.ORANGE.toString();
		cornerCubie[6].Color3 = Color.WHITE.toString();
		cornerCubie[6].Color1Position = Direction.FRONT.toString();
		cornerCubie[6].Color2Position = Direction.DOWN.toString();
		cornerCubie[6].Color3Position = Direction.LEFT.toString();
		cornerCubie[7] = cube.new CornerCubie();
		cornerCubie[7].Color1 = Color.GREEN.toString();
		cornerCubie[7].Color2 = Color.ORANGE.toString();
		cornerCubie[7].Color3 = Color.YELLOW.toString();
		cornerCubie[7].Color1Position = Direction.FRONT.toString();
		cornerCubie[7].Color2Position = Direction.DOWN.toString();
		cornerCubie[7].Color3Position = Direction.RIGHT.toString();
		return cornerCubie;
	}

	Cube createCube() {
		Cube cube = new Cube();
		return cube;
	}

	Cube.EdgeCubie[] createEdgeCubie(Cube cube) {
		EdgeCubie[] edgeCubie = new EdgeCubie[12];
	
		edgeCubie[0] = cube.new EdgeCubie();
		edgeCubie[0].Color1 = Color.GREEN.toString();
		edgeCubie[0].Color2 = Color.RED.toString();
		edgeCubie[0].Color1Position = Direction.FRONT.toString();
		edgeCubie[0].Color2Position = Direction.UP.toString();
		edgeCubie[1] = cube.new EdgeCubie();
		edgeCubie[1].Color1 = Color.RED.toString();
		edgeCubie[1].Color2 = Color.WHITE.toString();
		edgeCubie[1].Color1Position = Direction.UP.toString();
		edgeCubie[1].Color2Position = Direction.LEFT.toString();
		edgeCubie[2] = cube.new EdgeCubie();
		edgeCubie[2].Color1 = Color.BLUE.toString();
		edgeCubie[2].Color2 = Color.RED.toString();
		edgeCubie[2].Color1Position = Direction.BACK.toString();
		edgeCubie[2].Color2Position = Direction.UP.toString();
		edgeCubie[3] = cube.new EdgeCubie();
		edgeCubie[3].Color1 = Color.RED.toString();
		edgeCubie[3].Color2 = Color.YELLOW.toString();
		edgeCubie[3].Color1Position = Direction.UP.toString();
		edgeCubie[3].Color2Position = Direction.RIGHT.toString();
		edgeCubie[4] = cube.new EdgeCubie();
		edgeCubie[4].Color1 = Color.BLUE.toString();
		edgeCubie[4].Color2 = Color.WHITE.toString();
		edgeCubie[4].Color1Position = Direction.BACK.toString();
		edgeCubie[4].Color2Position = Direction.LEFT.toString();
		edgeCubie[5] = cube.new EdgeCubie();	
		edgeCubie[5].Color1 = Color.BLUE.toString();
		edgeCubie[5].Color2 = Color.ORANGE.toString();
		edgeCubie[5].Color1Position = Direction.BACK.toString();
		edgeCubie[5].Color2Position = Direction.DOWN.toString();
		edgeCubie[6] = cube.new EdgeCubie();
		edgeCubie[6].Color1 = Color.BLUE.toString();
		edgeCubie[6].Color2 = Color.YELLOW.toString();
		edgeCubie[6].Color1Position = Direction.BACK.toString();
		edgeCubie[6].Color2Position = Direction.RIGHT.toString();
		edgeCubie[7] = cube.new EdgeCubie();
		edgeCubie[7].Color1 = Color.ORANGE.toString();
		edgeCubie[7].Color2 = Color.WHITE.toString();
		edgeCubie[7].Color1Position = Direction.DOWN.toString();
		edgeCubie[7].Color2Position = Direction.LEFT.toString();
		edgeCubie[8] = cube.new EdgeCubie();
		edgeCubie[8].Color1 = Color.GREEN.toString();
		edgeCubie[8].Color2 = Color.ORANGE.toString();
		edgeCubie[8].Color1Position = Direction.FRONT.toString();
		edgeCubie[8].Color2Position = Direction.DOWN.toString();
		edgeCubie[9] = cube.new EdgeCubie();
		edgeCubie[9].Color1 = Color.ORANGE.toString();
		edgeCubie[9].Color2 = Color.YELLOW.toString();
		edgeCubie[9].Color1Position = Direction.DOWN.toString();
		edgeCubie[9].Color2Position = Direction.RIGHT.toString();
		edgeCubie[10] = cube.new EdgeCubie();
		edgeCubie[10].Color1 = Color.GREEN.toString();
		edgeCubie[10].Color2 = Color.WHITE.toString();
		edgeCubie[10].Color1Position = Direction.FRONT.toString();
		edgeCubie[10].Color2Position = Direction.LEFT.toString();
		edgeCubie[11] = cube.new EdgeCubie();
		edgeCubie[11].Color1 = Color.GREEN.toString();
		edgeCubie[11].Color2 = Color.YELLOW.toString();
		edgeCubie[11].Color1Position = Direction.FRONT.toString();
		edgeCubie[11].Color2Position = Direction.RIGHT.toString();
		return edgeCubie;
	}

	MidCubie[] createMidCubie(Cube cube) {
		MidCubie[] midCubie2 = new MidCubie[6];
	
		midCubie2[0] = cube.new MidCubie();
		midCubie2[0].Color1 = Color.RED.toString();
		midCubie2[0].ColorPosition = Direction.UP.toString();
		midCubie2[1] = cube.new MidCubie();
		midCubie2[1].Color1 = Color.BLUE.toString();
		midCubie2[1].ColorPosition = Direction.BACK.toString();
		midCubie2[2] = cube.new MidCubie();
		midCubie2[2].Color1 = Color.ORANGE.toString();
		midCubie2[2].ColorPosition = Direction.DOWN.toString();
		midCubie2[3] = cube.new MidCubie();
		midCubie2[3].Color1 = Color.GREEN.toString();
		midCubie2[3].ColorPosition = Direction.FRONT.toString();
		midCubie2[4] = cube.new MidCubie();
		midCubie2[4].Color1 = Color.YELLOW.toString();
		midCubie2[4].ColorPosition = Direction.RIGHT.toString();
		midCubie2[5] = cube.new MidCubie();
		midCubie2[5].Color1 = Color.WHITE.toString();
		midCubie2[5].ColorPosition = Direction.LEFT.toString();
		return midCubie2;
	}

}
