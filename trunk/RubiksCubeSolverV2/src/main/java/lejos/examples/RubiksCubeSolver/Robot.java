package lejos.examples.RubiksCubeSolver;

import lejos.nxt.Button;

import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;


public class Robot {

	private CubeFactory cubeFactory;

	//Werte sind zu ändern, wenn der Robot umgebaut wurde.
	private static final int ROTATE_APPLIANCE_90_DEGREE = -90;
	private static final int ROTATE_APPLIANCE_MINUS_90_DEGREE = 90;
	private static final int ROTATE_ARM_FORWARD_FLIP = 80;
	private static final int ROTATE_ARM_BACKWARD_FLIP = -80;
	private static final int ROTATE_ARM_FORWARD_HOLD = 40;
	private static final int ROTATE_ARM_BACKWARD_HOLD = -40;
	
	public Robot(CubeFactory cubeFactory, IMotor motorB, IMotor motorC) {

		this.cubeFactory = cubeFactory;
		this.motorB = motorB;
		this.motorC = motorC;

		cube = cubeFactory.createCube();

		midCubie = cubeFactory.createMidCubie(cube);

		edgeCubie = cubeFactory.createEdgeCubie(cube);

		cornerCubie = cubeFactory.createCornerCubie(cube);
	}


	// Initializes the cube
	Cube cube;

	// Initializes the MidCubies (6 MidCubies)
	Cube.MidCubie[] midCubie;

	// Initializes the EdgeCubies
	Cube.EdgeCubie[] edgeCubie;

	// Initializes the CornerCubies
	Cube.CornerCubie[] cornerCubie;

	private IMotor motorC;

	public IMotor getMotorC() {
		return motorC;
	}

	public void setMotorC(IMotor motorC) {
		this.motorC = motorC;
	}

	public IMotor getMotorB() {
		return motorB;
	}

	public void setMotorB(IMotor motorB) {
		this.motorB = motorB;
	}

	private IMotor motorB;

	private int waitBetweenSteps=500;

	public int getWaitBetweenSteps() {
		return waitBetweenSteps;
	}

	public void setWaitBetweenSteps(int waitBetweenSteps) {
		this.waitBetweenSteps = waitBetweenSteps;
	}

	public void waitForCube() {
		UltrasonicSensor sonic = new UltrasonicSensor(SensorPort.S1);

		System.out.println("Waiting for Cube...");

		// While there is no cube in the appliance
		while (sonic.getDistance() > 10 && !Button.ESCAPE.isPressed());
		
		// If there is a cube, say "Thank you" and wait 5sec
		if (sonic.getDistance() < 10) {
			System.out.println("Thank you!");
			sleep(3000);
		}
	}

	private void sleep(int i) {
		
		try
		{
			Thread.sleep(i);
		}catch(InterruptedException e)
		{}
	}

	// Uses the robots arm to twist the cube (there is only one turn)
	public void Armtwist(int times) // times stands for how many "times"
	{
		motorC.setPower(100);

		for (short i = 0; i < times; i++) {
			motorC.rotate(ROTATE_ARM_FORWARD_FLIP);
			motorC.rotate(-15);
			motorC.rotate(15);
			motorC.setSpeed(80);
			motorC.rotate(ROTATE_ARM_BACKWARD_FLIP);
			motorC.setSpeed(500);
			
			for (short j = 0; j < 6; j++) {
				midCubie[j].MidPositionChange(1);
			}

			for (short j = 0; j < 12; j++) {
				edgeCubie[j].EdgePositionChange(1);
			}

			for (short j = 0; j < 8; j++) {
				cornerCubie[j].CornerPositionChange(1);
			}
		}
	}

	// Twists the cube clockwise
	public void Cubetwist(int times) {
		for (short i = 0; i < times; i++) {

			motorB.rotate(ROTATE_APPLIANCE_90_DEGREE);

			for (short j = 0; j < 6; j++) {
				midCubie[j].MidPositionChange(3);
			}

			for (short j = 0; j < 12; j++) {
				edgeCubie[j].EdgePositionChange(3);
			}

			for (short j = 0; j < 8; j++) {
				cornerCubie[j].CornerPositionChange(3);
			}
		}
	}

	// Twists the cube inverse (anti-clockwise)
	public void CubetwistInvert(int times) {
		for (short i = 0; i < times; i++) {
			motorB.rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);

			for (short j = 0; j < 6; j++) {
				midCubie[j].MidPositionChange(2);
			}

			for (short j = 0; j < 12; j++) {
				edgeCubie[j].EdgePositionChange(2);
			}

			for (short j = 0; j < 8; j++) {
				cornerCubie[j].CornerPositionChange(2);
			}
		}
	}

	// Holds the cube with the arm and twists the DOWN side clockwise
	public void HoldAndTwist(int times) {
	//	motorC.setPower(100);

		motorC.rotate(ROTATE_ARM_FORWARD_HOLD);
		
		for (short i = 0; i < times; i++) {
			motorB.rotate(ROTATE_APPLIANCE_90_DEGREE);

			for (short j = 0; j < 12; j++) {
				edgeCubie[j].EdgePositionChange(4);
			}

			for (short j = 0; j < 8; j++) {
				cornerCubie[j].CornerPositionChange(4);
			}
		}
		motorC.rotate(ROTATE_ARM_BACKWARD_HOLD);
	}

	// Holds the cube with the arm and twists the DOWN side inverse
	public void HoldAndTwistInvert(int times) {
		
		//motorC.setPower(100);

		motorC.rotate(ROTATE_ARM_FORWARD_HOLD);
		
		for (short i = 0; i < times; i++) {
			motorB.rotate(ROTATE_APPLIANCE_MINUS_90_DEGREE);

			for (short j = 0; j < 12; j++) {
				edgeCubie[j].EdgePositionChange(5);
			}

			for (short j = 0; j < 8; j++) {
				cornerCubie[j].CornerPositionChange(5);
			}
		}
		
		motorC.rotate(ROTATE_ARM_BACKWARD_HOLD);
	}

}
