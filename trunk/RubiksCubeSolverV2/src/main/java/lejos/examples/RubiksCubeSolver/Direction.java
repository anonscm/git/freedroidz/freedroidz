package lejos.examples.RubiksCubeSolver;

public enum Direction {
	UP, DOWN, LEFT, RIGHT, FRONT, BACK
}
