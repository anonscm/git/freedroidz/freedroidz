package lejos.example.square;

import lejos.nxt.Motor;
import lejos.robotics.navigation.Pilot;
import lejos.robotics.navigation.TachoPilot;

public class QuadratSchleife {
	Pilot pilot;

	public static void main(String[] args) {
		QuadratSchleife quadrat = new QuadratSchleife();
		quadrat.pilot = new TachoPilot(56, 173, Motor.A, Motor.C);
		quadrat.run();
	}

	private void run() {
		int entfernung = 800;
		int drehen = 180;

		for (int i = 0; i < 4; i++) {
			pilot.travel(entfernung);
			pilot.rotate(drehen);
		}
	}

}
