package lejos.example.square;

import lejos.nxt.Motor;
import lejos.robotics.navigation.Pilot;
import lejos.robotics.navigation.TachoPilot;

public class QuadratVariablen {
	Pilot pilot;
	
	public static void main(String[] args) {
		QuadratVariablen quadrat = new QuadratVariablen();
		quadrat.pilot = new TachoPilot(56, 173, Motor.A, Motor.C);
		quadrat.run();
	}
	
	private void run() {
		int entfernung = 800;
		int drehen = 90;
		
		pilot.travel(entfernung);
		pilot.rotate(drehen);
		pilot.travel(entfernung);
		pilot.rotate(drehen);
		pilot.travel(entfernung);
		pilot.rotate(drehen);
		pilot.travel(entfernung);
		pilot.rotate(drehen);
	}
}
