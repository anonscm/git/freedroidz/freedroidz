package lejos.example.lightdetector;

import lejos.nxt.SensorPort;
import lejos.nxt.addon.ColorSensor;

public class Color {
	

	
	public static void main(String args[]){
		
		//Note: ColorSensor is for the HiTechnic Sensor. For the Lego Sensor use ColorLightSensor
		ColorSensor color = new ColorSensor(SensorPort.S3);
		
		while(!lejos.nxt.Button.ESCAPE.isPressed()){
			System.out.println(color.getBlueComponent());
			System.out.println(color.getColorNumber());
			
			lejos.nxt.Button.waitForPress();
		}
	}
}
