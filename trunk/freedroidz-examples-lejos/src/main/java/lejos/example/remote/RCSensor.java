package lejos.example.remote;

import java.io.IOException;

import lejos.nxt.Button;
import lejos.nxt.ButtonListener;
import lejos.nxt.ColorLightSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.SoundSensor;
import lejos.nxt.UltrasonicSensor;

public class RCSensor implements RemoteControlListener {

	
	private static int NXT_ID = 1;

	public static void main(String[] args) throws IOException {
		new RCSensor().doIt();
	}

	private void doIt() {
		final RemoteControlLib remoteControlLib = new RemoteControlLib(this);

		Button.ESCAPE.addButtonListener(new ButtonListener() {

			public void buttonPressed(Button arg0) {
				remoteControlLib.stop();
			}

			public void buttonReleased(Button arg0) {
			}

		});
		remoteControlLib.setUpConnection();
		startSensorThread(remoteControlLib);
		remoteControlLib.remoteControl();
		remoteControlLib.stop();
	}

	private void startSensorThread(final RemoteControlLib remoteControlLib) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					
					if (NXT_ID == 0) {
						TouchCounter touchCounter = new TouchCounter(SensorPort.S1);
						final SoundSensor sound = new SoundSensor(SensorPort.S2);
						final UltrasonicSensor us = new UltrasonicSensor(SensorPort.S3);
						
						
						while (true) {
							sendSensorData(remoteControlLib, "PORT1", "TOUCH_COUNT", touchCounter.getCount());
							sendSensorData(remoteControlLib, "PORT2", "SOUND", sound.readValue());
							sendSensorData(remoteControlLib, "PORT3", "SONIC", us.getDistance());
							sendSensorData(remoteControlLib, "MOTORA", "TACHO", Motor.A.getTachoCount());
							sendSensorData(remoteControlLib, "MOTORB", "TACHO", Motor.B.getTachoCount());
							sendSensorData(remoteControlLib, "MOTORC", "TACHO", Motor.C.getTachoCount());

							Thread.sleep(5000);
						}
					} else if (NXT_ID == 1) {
						TouchCounter touchCounter = new TouchCounter(SensorPort.S1);
						final SoundSensor sound = new SoundSensor(SensorPort.S2);
						final UltrasonicSensor us = new UltrasonicSensor(SensorPort.S3);
						final ColorLightSensor cls = new ColorLightSensor(SensorPort.S4, ColorLightSensor.TYPE_COLORFULL);
						
						while (true) {
							sendSensorData(remoteControlLib, "PORT1", "TOUCH_COUNT", touchCounter.getCount());
							sendSensorData(remoteControlLib, "PORT2", "SOUND", sound.readValue());
							sendSensorData(remoteControlLib, "PORT3", "SONIC", us.getDistance());
							sendSensorData(remoteControlLib, "PORT4", "COLOR", cls.readValue());
							sendSensorData(remoteControlLib, "MOTORA", "TACHO", Motor.A.getTachoCount());
							sendSensorData(remoteControlLib, "MOTORB", "TACHO", Motor.B.getTachoCount());
							sendSensorData(remoteControlLib, "MOTORC", "TACHO", Motor.C.getTachoCount());

							Thread.sleep(5000);
						}
					}
				} catch (InterruptedException e) {
				}
			}

			private void sendSensorData(final RemoteControlLib remoteControlLib, String port, String type, int value) {
				// We do this kind of hacked, because we don't need any
				// further commands.
				final String json = "[{\"sensor\":\"NXT" + NXT_ID + "_" + port + "_" + type + "\", \"value\":" + value
						+ "}]";
				remoteControlLib.sendLine(json);
			}
		}).start();

	}

	public static void setMotor(Motor motor, int speed) {
		if (speed > 0) {
			motor.setSpeed(speed);
			motor.forward();
		} else if (speed < 0) {
			motor.setSpeed(speed);
			motor.backward();
		} else {
			motor.stop();
		}
	}

	@Override
	public void onCommand(int motorA, int motorB, int motorC) {
		setMotor(Motor.A, motorA);
		setMotor(Motor.B, motorB);
		setMotor(Motor.C, motorC);
	}

}
