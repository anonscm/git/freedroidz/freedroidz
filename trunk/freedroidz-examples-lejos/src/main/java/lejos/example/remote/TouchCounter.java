package lejos.example.remote;

import lejos.nxt.SensorPort;
import lejos.nxt.SensorPortListener;

public class TouchCounter {

	private int count = 0;

	private SensorPortListener touchSensorListener = new SensorPortListener() {

		@Override
		public void stateChanged(SensorPort port, int oldValue, int newValue) {
			
			if(oldValue > 500 && newValue < 500) {
			//	onTouchPress();
				
				count++;
			} else if(oldValue < 500 && newValue > 500) {
//				onTouchRelease();
			}
		}
	};

	public TouchCounter(SensorPort sp) {
		sp.addSensorPortListener(touchSensorListener);

	}

	public int getCount() {
		return count;
	}
}
