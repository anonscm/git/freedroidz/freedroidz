package lejos.example.remote;

public interface RemoteControlListener {
	public void onCommand(int motorA, int motorB, int motorC);
}
