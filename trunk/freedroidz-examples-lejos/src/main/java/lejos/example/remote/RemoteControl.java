package lejos.example.remote;

import java.io.IOException;

import lejos.nxt.Button;
import lejos.nxt.ButtonListener;
import lejos.nxt.Motor;

public class RemoteControl implements RemoteControlListener {

	public static void main(String[] args) throws IOException {
		new RemoteControl().doIt();
	}

	private void doIt() {
		final RemoteControlLib remoteControlLib = new RemoteControlLib(this);

		Button.ESCAPE.addButtonListener(new ButtonListener() {

			public void buttonPressed(Button arg0) {
				remoteControlLib.stop();
			}

			public void buttonReleased(Button arg0) {
			}

		});		
		remoteControlLib.setUpConnection();
		remoteControlLib.remoteControl();
		remoteControlLib.stop();
	}

	public static void setMotor(Motor motor, int speed) {
		if (speed > 0) {
			motor.setSpeed(speed);
			motor.forward();
		} else if (speed < 0) {
			motor.setSpeed(speed);
			motor.backward();
		} else {
			motor.stop();
		}
	}

	@Override
	public void onCommand(int motorA, int motorB, int motorC) {
		setMotor(Motor.A, motorA);
		setMotor(Motor.B, motorB);
		setMotor(Motor.C, motorC);
	}

}
