package lejos.example.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import lejos.nxt.comm.BTConnection;
import lejos.nxt.comm.Bluetooth;

public class RemoteControlLib {

	public BTConnection c;
	public DataInputStream in;
	public DataOutputStream out;

	public RemoteControlLib(RemoteControlListener listener) {
		super();
		this.listener = listener;
	}

	public boolean run = true;

	private final RemoteControlListener listener;

	public void setUpConnection() {
		System.out.println("Waiting for incomming connection...");
		c = Bluetooth.waitForConnection();
		in = c.openDataInputStream();
		out = c.openDataOutputStream();

		System.out.println("Connection established!");
		System.out.println("waiting for commands");

	}

	public void remoteControl() {
		try {
			byte[] b = new byte[12];

			while (run) {
				in.read(b);

				int portA = byteArrayToInt(b, 0);
				int portB = byteArrayToInt(b, 4);
				int portC = byteArrayToInt(b, 8);

				if (portA == 666 && portB == 666 && portC == 666)
					closeConnection();

				listener.onCommand(portA, portB, portC);

			}
		} catch (IOException e) {
			stop();
		}
	}
	
	public void sendLine(String line) {
		try {
			out.writeBytes(line + '\n');
			out.flush();
//			System.out.println(s);
		} catch (IOException e) {
			stop();
		}
	}

	public void stop() {
		run = false;
	}

	public void closeConnection() {
		try {
			in.close();
			out.close();
		} catch (IOException e) {
		}

		c.close();
		System.exit(0);
	}

	public static int byteArrayToInt(byte[] b, int offset) {
		return (b[offset] << 24) + ((b[offset + 1] & 0xFF) << 16)
				+ ((b[offset + 2] & 0xFF) << 8) + (b[offset + 3] & 0xFF);
	}
	

}
