package lejos.example.simple;

import lejos.robotics.navigation.Pilot;
import lejos.robotics.navigation.TachoPilot;
import lejos.nxt.Motor;

public class FirstExample {

	public Pilot pilot;

	public static void main(String[] args) {
		FirstExample example = new FirstExample();
		example.setPilot(new TachoPilot(56, 105, Motor.A, Motor.C));
		example.run();
	}

	public Pilot getPilot() {
		return pilot;
	}

	public void setPilot(Pilot pilot) {
		this.pilot = pilot;
	}
	
	protected void run(){
		pilot.travel(100);
		pilot.rotate(90);
		pilot.travel(100);
	}
	

}
