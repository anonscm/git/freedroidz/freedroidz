package lejos.example;

import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;
import lejos.robotics.navigation.Pilot;
import lejos.robotics.navigation.TachoPilot;

public class TouchSensorProgramm {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		TouchSensor touchMe = new TouchSensor(SensorPort.S1);
		
		Pilot pilot = new TachoPilot(56, 173, Motor.A, Motor.C);
		
		pilot.forward();
		
		if(touchMe.isPressed()) {
			System.out.println("Hinderniss!");
			pilot.travel(-100); // 10 cm zurueck fahren
			pilot.rotate(-70); // um 70Grad drehen (nach rechts)
			pilot.forward(); // wieder vorwärts fahren
			System.out.println("Warte auf Hinderniss...");
		}
	}
}
