package lejos.example.vampir;

import lejos.robotics.navigation.Pilot;
import lejos.robotics.navigation.TachoPilot;
import lejos.nxt.Button;
import lejos.nxt.ButtonListener;
import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;


public class Vampir {

	Pilot pilot;

	static float diameter = 56; // Reifendurchmesser in mm

	static float trackWidth = 105; // Abstand der Reifen in mm

	public static void main(String[] args) {
		Vampir vampir = new Vampir();
		vampir.setPilot(new TachoPilot(diameter, trackWidth, Motor.B, Motor.C));
		vampir.run();
	}

	public Pilot getPilot() {
		return pilot;
	}

	public void setPilot(Pilot pilot) {
		this.pilot = pilot;
	}

	LightSensor lLinks;

	LightSensor lRechts;
	
	int hoodRotate = 50;

	protected void run() {

		Button.ESCAPE.addButtonListener(new ButtonListener() {

			public void buttonPressed(Button arg0) {
				System.exit(0);
			}

			public void buttonReleased(Button arg0) {
				;
			} // is never called
		});

		// Ab und zu blinzeln
		new Thread(){
			public void run() {
				while (true) {
					try {
						Thread.sleep((int) (Math.random() * 20000));
						lLinks.setFloodlight(false);
						lRechts.setFloodlight(false);
						Thread.sleep(100);
						lLinks.setFloodlight(true);
						lRechts.setFloodlight(true);
					} catch (InterruptedException e) {
						;// passiert eh nicht }
					}
				}

			}
		}.start();
		

		int lightValues[] = new int[100];
		int current = 0;

		lLinks = new LightSensor(SensorPort.S1);
		lRechts = new LightSensor(SensorPort.S4);

		// füllen
		for (current = 0; current < 100; current++) {
			lightValues[current] = lLinks.readNormalizedValue()
					+ lRechts.readNormalizedValue();
		}
		current = 0;

		while (true) {
			if (current == 100)
				current = 0;
			lightValues[current] = lLinks.readNormalizedValue()
					+ lRechts.readNormalizedValue();

			// if (current % 10 == 0)
			// System.out.println(Integer.toString(lLinks.readNormalizedValue())
			// + " "
			// + Integer.toString(lRechts.readNormalizedValue()));

			int avg = 0;
			for (int i = 0; i < 100; i++) {
				avg += lightValues[i];
			}
			avg /= 100;

			int tmp = lLinks.readNormalizedValue()
					+ lRechts.readNormalizedValue();
			if (tmp > avg + 50) { // +50: puffer
				lLinks.setFloodlight(false);
				lRechts.setFloodlight(false);
				/*Sound.setVolume(Sound.VOL_MAX);
				Sound.beep();
				Sound.setVolume(0);*/
				Motor.A.rotate(-hoodRotate);
				// lLinks.setFloodlight(true);
				// lRechts.setFloodlight(true);
				if (lLinks.readNormalizedValue() > lRechts
						.readNormalizedValue()) {
					pilot.rotate(180);
				} else {
					pilot.rotate(-180);
				}
				pilot.travel(500);

				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					;// passiert eh nicht }
				}

				Motor.A.rotate(hoodRotate);
				lLinks.setFloodlight(true);
				lRechts.setFloodlight(true);
				//Motor.A.rotate(80);

				for (int i = 0; i < 3; i++) {
					lLinks.setFloodlight(false);
					lRechts.setFloodlight(false);
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						;// passiert eh nicht }
					}
					lLinks.setFloodlight(true);
					lRechts.setFloodlight(true);
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						;// passiert eh nicht }
					}
				}
				// neu füllen
				for (current = 0; current < 100; current++) {
					lightValues[current] = lLinks.readNormalizedValue()
							+ lRechts.readNormalizedValue();
				}
				current = 0;
			}
			current++;
		}

	}

}
