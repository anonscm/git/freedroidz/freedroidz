package lejos.example.remoteInactive;


import java.io.IOException;

import lejos.nxt.Motor;

import lejos.example.remote.RemoteControl;
import lejos.example.walker1.Walker1;

public class RemoteControlInactive extends RemoteControl {

	public static boolean run = true;
	/**
	 * @param args
	 * @throws IOException 
	 */
	
	
	static long lastCommand = 0;
	
	static Thread idleThread = new Thread() {
		public void run() {
			System.out.println("idleThread started.");
			while(true) {
				while(lastCommand == 0); // am anfang warten...
				System.out.println(Integer.toString((int)(System.currentTimeMillis()-lastCommand)));
				while(System.currentTimeMillis()-lastCommand > 15000) {
					 // TODO: sauberer Programmieren ;)
					new Thread() {
						public void run() {
							//Walker1 walker = new Walker1();
							Walker1.PREFERED_DISTANCE = 15;
							Walker1.main(null);
							
						}
					}.start();
					while(System.currentTimeMillis()-lastCommand > 15000) {
						try {
							Thread.sleep(10);
						} catch (InterruptedException e) {;}
					}
					Walker1.shouldBeRunning = false;
				}
				
				
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {;}
			}
		}
	};
	
	public static void main(String[] args) throws IOException {
		
		idleThread.start();
		setUpConnection();
		remoteControl();
		closeConnection();
	}
	
	public static void remoteControl() throws IOException {
		
		byte[] b = new byte[12];
		
		while(run){
			
			lastCommand = System.currentTimeMillis();
			
			RemoteControl.in.read(b);	
			
			int portA = byteArrayToInt(b, 0);
			int portB = byteArrayToInt(b, 4);
			int portC = byteArrayToInt(b, 8);
			
			if(portA == 666 && portB == 666 && portC == 666)
				closeConnection();
			
			setMotor(Motor.A, portA);
			setMotor(Motor.B, portB);
			setMotor(Motor.C, portC);
			
			
			
		}
	}
	
}
