package lejos.examples.lightcontrol;

import lejos.nxt.Button;
import lejos.nxt.ButtonListener;
import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.robotics.navigation.Pilot;
import lejos.robotics.navigation.TachoPilot;

public class LightControl implements ButtonListener {
	
	static float diameter = 5.7f;	//Reifendurchmesser in cm
	static float trackWidth = 10.6f;	//Abstand der Reifen in cm
	
	private Pilot pilot = new TachoPilot(diameter, trackWidth, Motor.A, Motor.B);
	LightSensor lightSensor = new LightSensor(SensorPort.S1,false);
	
	int threshold = 45;
	int absoluteTurnRate = 90;
	int angle = 30;
	//gap between average light value and threshold
	int gap=15;
	
	public LightControl(){
		Button.ESCAPE.addButtonListener(this);
		Button.ENTER.addButtonListener(this);
		Button.RIGHT.addButtonListener(this);
		Button.LEFT.addButtonListener(this); 
//		System.out.println("low value: "+lightSensor.getLow());
//		System.out.println("high value: "+lightSensor.getHigh());
	}
	
	public static void main(String[] args) {	
		
		LightControl lightControl = new LightControl();
		lightControl.calibrate();
		lightControl.run();
		Button.waitForPress();
	}
	
	protected void run(){
		
		while(true){			
			int turnRate = calculateTurnRate(threshold, absoluteTurnRate);
			pilot.steer(turnRate, angle, true);
		}
	}
	
	private int calculateTurnRate(int threshold, int absoluteTurnRate){
		int lightValue = lightSensor.getNormalizedLightValue();
//		System.out.println(lightValue);
		int turnRate = 0;
		if (lightValue>threshold){
			turnRate=absoluteTurnRate;
		} else if (lightValue<threshold){
			turnRate=-absoluteTurnRate;
		} 
		return turnRate;
	}
	
	private void calibrate(){
		int count = 100;
		int sum=0;
		for (int i = 0; i < count; i++) {
			sum+=lightSensor.getNormalizedLightValue();
		}
		int average = sum/count;
		System.out.println("average normalized light: "+average);	
		threshold=average+gap;
		System.out.println("treshold: "+threshold);
		Button.waitForPress();
	}
	
	@Override
	public void buttonPressed(Button arg0) {
		//ESCAPE button pressed -> exit		
		if (arg0==Button.ESCAPE){
			System.exit(0);
		} 
	}

	@Override
	public void buttonReleased(Button arg0) {
		// do nothing		
	}
}
