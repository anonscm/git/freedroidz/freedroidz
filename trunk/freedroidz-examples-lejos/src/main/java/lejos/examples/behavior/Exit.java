package lejos.examples.behavior;

import lejos.nxt.Button;
import lejos.robotics.subsumption.Behavior;

public class Exit implements Behavior{

	public Exit(){
		
	}
	
	@Override
	public boolean takeControl(){
		
		return Button.ESCAPE.isPressed();
	}
	
	@Override
	public void action(){
		
		System.exit(0);
	}
	
	@Override
	public void suppress(){
		
	}
}
