package lejos.examples.behavior;

import lejos.nxt.Motor;
import lejos.robotics.navigation.Pilot;
import lejos.robotics.navigation.TachoPilot;
import lejos.robotics.subsumption.Behavior;

public class Move implements Behavior{

	Pilot pilot;
	
	public Move(){
		pilot = new TachoPilot(56, 173, Motor.A, Motor.C);
	}
	
	@Override
	public boolean takeControl(){
		
		return !new Dodge().takeControl();
	}
	
	@Override
	public void action(){
		pilot.forward();
	}
	
	@Override
	public void suppress(){
		pilot.stop();
	}
}
