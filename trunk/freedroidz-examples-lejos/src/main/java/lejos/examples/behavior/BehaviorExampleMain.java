package lejos.examples.behavior;

import lejos.robotics.subsumption.Arbitrator;
import lejos.robotics.subsumption.Behavior;

public class BehaviorExampleMain {

	Arbitrator arbitrator;
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		BehaviorExampleMain test = new BehaviorExampleMain();
		test.initialize();
		test.go();
	}
	
	private void initialize(){
		Behavior[] behavior = {new Move(), new Dodge(), new Exit()};
		arbitrator = new Arbitrator(behavior);
	}
	
	private void go(){
		arbitrator.start();
	}

}