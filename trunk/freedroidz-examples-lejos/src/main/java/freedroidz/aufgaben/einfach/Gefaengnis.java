package freedroidz.aufgaben.einfach;

import lejos.nxt.Button;
import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;


/**
 * Diese Klasse implementiert einen einfachen Roboter, der innerhalb eines Schwarzen Kreises bleibt.
 * 
 *  * 
 * Sensoren: 
 * 	Lichtsensor Links an S1
 * 	Lichtsensor Rechts an S2	
 * 
 * Motoren:
 * 	Links an A
 * 	Rechts an C	
 * 
 * 
 * @author mpasch
 *
 */
public class Gefaengnis {
	
	private static final int BLACK = 50;
	
	LightSensor sensorLinks = new LightSensor(SensorPort.S1);
	LightSensor sensorRechts = new LightSensor(SensorPort.S2);
			
	
	Motor links = Motor.A;
	Motor rechts = Motor.C;
	
	private void starte() throws InterruptedException {
		
		System.out.println("Bleibe im Gefaengis...");
					
		// so lange bis escape button gedrueckt ist
		while(!Button.ESCAPE.isPressed()) {

			if(sensorLinks.getLightValue() < BLACK) {
				
				// wenn der linke sensor auf der linie ist, dreh ein bisschen nach hinten
				links.stop();
				rechts.backward();
				

				Thread.sleep(300);
			} else if(sensorRechts.getLightValue() < BLACK) {
			
				// wenn der rechte sensor auf der linie ist, dreh ein bisschen nach hinten
				links.backward();
				rechts.stop();

				Thread.sleep(300);
			} else {
				
				// sonst vorwaerts fahren
				links.forward();
				rechts.forward();
				
			}
			
			Thread.sleep(10); // absturz verhindern
			
		}
		
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			// erstellt eine neue Instanz und ruft starte() auf
			new Gefaengnis().starte();
		} catch (InterruptedException e) {
			// passiert eigentlich nie
		}
	}
}
