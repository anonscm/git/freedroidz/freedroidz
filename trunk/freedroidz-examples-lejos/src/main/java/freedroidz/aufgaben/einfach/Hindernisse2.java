package freedroidz.aufgaben.einfach;

import lejos.nxt.Button;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;
import lejos.robotics.navigation.Pilot;
import lejos.robotics.navigation.TachoPilot;


/**
 * Diese Klasse implementiert einen einfachen Roboter, der Hindernissen ausweicht.
 * 
 * 
 * Sensoren: 
 * 	Touchsensor Vorne Links an S1
 * 	Touchsensor Vorne Rechts an S2	
 * 
 * Motoren:
 * 	Links an A
 * 	Rechts an C	
 * 
 * 
 * @author mpasch
 *
 */
public class Hindernisse2 {

	
	TouchSensor links = new TouchSensor(SensorPort.S1);
	TouchSensor rechts = new TouchSensor(SensorPort.S2);
	
	long lastTouch = System.currentTimeMillis();
	
	// alle angaben in mm
	Pilot pilot = new TachoPilot(58, 174, Motor.A, Motor.C);
	
	private void starte() throws InterruptedException {
		// geradeaus fahren:
		pilot.forward();
		System.out.println("Warte auf Hinderniss...");
		
		// so lange bis escape button gedrueckt ist
		while(!Button.ESCAPE.isPressed()) {

			if(links.isPressed()) {
				System.out.println("Hinderniss Links!");
				weicheAus();
			}
			if(rechts.isPressed()) {
				System.out.println("Hinderniss Rechts!");
				weicheAus();
			}
			
			long now = System.currentTimeMillis();
			
			if(now - lastTouch > 6000) {
				System.out.println("6 Sekunden sind zu lang ;)!");
				weicheAus();
			}
			
			
			Thread.sleep(10); // absturz verhindern
			
		}
		
	}
	
	public void weicheAus() {
		pilot.travel(-100); // 10 cm zurueck fahren
		pilot.rotate(randomDegree()); // um 70Grad drehen (nach rechts)
		pilot.forward(); // wieder vorwärts fahren
		System.out.println("Warte auf Hinderniss...");
		lastTouch = System.currentTimeMillis();
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			// erstellt eine neue Instanz und ruft starte() auf
			new Hindernisse2().starte();
		} catch (InterruptedException e) {
			// passiert eigentlich nie
		}
	}

	
	public static int randomDegree() {
		return (int) ( (Math.random()-0.5f) * 360);
	}	
	
}
