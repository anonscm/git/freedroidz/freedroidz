package freedroidz.aufgaben.fortgeschritten;

import lejos.nxt.Button;
import lejos.nxt.Motor;

public class Roboarm {

	int option;
	int buffer;
	
	boolean hand;
	boolean arm;
	boolean shoulder;
	
	Motor motorA;
	Motor motorB;
	Motor motorC;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Roboarm main = new Roboarm();
		
		main.initialize();
		main.go();
	}

	//Initializes all components.
	private void initialize(){
		
		option = 1;
		buffer = 1;
		
		hand = true;
		arm = false;
		shoulder = false;
		
		motorA = Motor.A;	//hannd
		motorA.setSpeed(50);
		motorA.setPower(100);
		motorB = Motor.B;	//arm
		motorB.setSpeed(1000);
		motorB.setPower(100);
		motorC = Motor.C;	//shoulder
		motorC.setSpeed(1000);
		motorC.setPower(100);
		
		System.out.println("Handmove on." +
		"Option change with enterbutton.");	
	}
	
	//Lets the programm start.
	private void go(){
		
		while(true){
			handMove();
			armMove();
			shoulderMove();
		}
	}
	
	//Shows which option is chosen on the screen.
	private void output(){
		
		if(option != buffer){
			
			option = buffer;
			
			if(option == 1){
				for(int i = 0; i<8; i++)
					System.out.println(" ");
				
				System.out.println("Handmove on." +
						"Option change with enterbutton.");
			}
			else if (option == 2){
				for(int i = 0; i<8; i++)
					System.out.println(" ");
				
				System.out.println("Armmove on." +
						"Option change with enterbutton.");
			}
			else if (option == 3){
				for(int i = 0; i<8; i++)
					System.out.println(" ");
				
				System.out.println("Shouldermove on." +
				"Option change with enterbutton.");
			}
		}
	}
	
	//Changes the option.
	private void optionChange(){
		if(option == 1){
			
			hand = true;
			arm = false;
			shoulder = false;
		}
		else if(option == 2){

			hand = false;
			arm = true;
			shoulder = false;
		}
		else if(option == 3){
			
			hand = false;
			arm = false;
			shoulder = true;
		}
	}
	
	//Sets the buffer when the Enterbutton is pressed.
	private void setBuffer(){
		if(Button.ENTER.isPressed()){
			
			while(Button.ENTER.isPressed());
			
			if(buffer == 1)
				buffer = 2;
			else if(buffer == 2)
				buffer = 3;
			else if (buffer == 3)
				buffer = 1;
		}
	}
	
	//Lets the motor move forward
	private void myForward(Motor motor){
		motor.forward();
	}
	
	//Lets the motor move backward
	private void myBackward(Motor motor){
		motor.backward();
	}
	
	
	//Lets the hand move when user uses the left or right button
	private void handMove(){
		
		while(hand){
			
			while(Button.LEFT.isPressed()){
				myBackward(motorA);
			}
			motorA.stop();
			
			while(Button.RIGHT.isPressed()){
				myForward(motorA);
			}
			motorA.stop();

			setBuffer();
			output();
			optionChange();
		}
	}
	
	//Lets the arm move when user uses the left or right button
	private void armMove(){
		
		while(arm){
			
			while(Button.RIGHT.isPressed()){
				myBackward(motorB);
			}
			motorB.stop();
			
			while(Button.LEFT.isPressed()){
				myForward(motorB);
			}
			motorB.stop();

			setBuffer();
			output();
			optionChange();
		}
	}

	//Lets the shoulder move when user uses the left or right button
	private void shoulderMove(){
		
		while(shoulder){
			
			while(Button.RIGHT.isPressed()){
				myBackward(motorC);
			}
			motorC.stop();
			
			while(Button.LEFT.isPressed()){
				myForward(motorC);
			}
			motorC.stop();

			setBuffer();
			output();
			optionChange();
		}
	}
}
