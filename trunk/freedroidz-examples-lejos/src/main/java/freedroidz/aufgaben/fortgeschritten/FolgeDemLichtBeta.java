package freedroidz.aufgaben.fortgeschritten;

import lejos.nxt.Button;
import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.robotics.navigation.Pilot;
import lejos.robotics.navigation.TachoPilot;

public class FolgeDemLichtBeta {

	LightSensor sensorLeft;
	LightSensor sensorRight;
	Pilot pilot;
	
	int highestLightValue;
	public static final int TOLERANCE = 5;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		FolgeDemLichtBeta folgeDemLicht = new FolgeDemLichtBeta();
		
		folgeDemLicht.initialize();
		folgeDemLicht.go();

	}

	private void initialize(){
		sensorLeft = new LightSensor(SensorPort.S1);
		sensorRight = new LightSensor(SensorPort.S3);
		pilot = new TachoPilot(58, 174, Motor.A, Motor.C);
		//pilot.setMoveSpeed(10);
		highestLightValue = 0;
	}
	
	private void go(){
		pilot.forward();	
		
		//Solange der Escape-Button nicht gedrückt wurde.
		while(!Button.ESCAPE.isPressed()){
		
			if(leftHigherRight()){
				rotateLeft();
				pilot.forward();
			}
			else if(rightHigherLeft()){
				rotateRight();
				pilot.forward();
			}			
	
		}
	}
	
	private boolean leftHigherRight(){
		if(sensorLeft.getLightValue() > sensorRight.getLightValue()+TOLERANCE)
			return true;
		else
			return false;
	}
	
	private boolean rightHigherLeft(){
		if(sensorLeft.getLightValue()+TOLERANCE < sensorRight.getLightValue())
			return true;
		else
			return false;
	}
	
	//Lässt den Roboter solange nach links drehen, bis die Lichtwerte gleich sind und speichert den höchsten Wert.
	private void rotateLeft(){
		
		//while(sensorLeft.getLightValue() != sensorRight.getLightValue()){
		while(!lightValueEqual()){
			pilot.rotate(+1);
			
			if(sensorLeft.getLightValue() < sensorRight.getLightValue() &&
					sensorRight.getLightValue() > highestLightValue)
				highestLightValue = sensorRight.getLightValue();
			else if(sensorLeft.getLightValue() > sensorRight.getLightValue() &&
					sensorLeft.getLightValue() > highestLightValue)
				highestLightValue = sensorLeft.getLightValue();
		}
	}

	//Lässt den Roboter solange nach rechts drehen, bis die Lichtwerte gleich sind und speichert den höchsten Wert.
	private void rotateRight(){
		
	//	while(sensorLeft.getLightValue() != sensorRight.getLightValue()){
		while(!lightValueEqual()){
			pilot.rotate(-1);
			
			if(sensorLeft.getLightValue() < sensorRight.getLightValue() &&
					sensorRight.getLightValue() > highestLightValue)
				highestLightValue = sensorRight.getLightValue();
			else if(sensorLeft.getLightValue() > sensorRight.getLightValue() &&
					sensorLeft.getLightValue() > highestLightValue)
				highestLightValue = sensorLeft.getLightValue();
		}
	}
	
	private boolean lightValueEqual(){
		if(sensorLeft.getLightValue() <= sensorRight.getLightValue()+TOLERANCE &&
				sensorLeft.getLightValue() >= sensorRight.getLightValue()-TOLERANCE)
			return true;
		else
			return false;
	}
}
