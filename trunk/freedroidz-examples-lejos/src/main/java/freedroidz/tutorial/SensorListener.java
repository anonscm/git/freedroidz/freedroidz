package freedroidz.tutorial;

import lejos.nxt.Button;
import lejos.nxt.SensorPort;
import lejos.nxt.SensorPortListener;
import lejos.nxt.TouchSensor;

public class SensorListener {

	// Sensor initialisieren, dies muss man beim TouchSensor nicht machen, 
	//	bei Licht oder Ultraschall macht es aber Sinn um deren Funktion zu aktivieren
	private TouchSensor touch = new TouchSensor(SensorPort.S1);
	
	// den SensorPortListener bauen
	private SensorPortListener touchSensorListener = new SensorPortListener() {
		
		/** 
		 * diese Methode wird jedesmal aufgerufen, wenn die Roh-Werte des Sensors sich verändern.
		 */
		@Override
		public void stateChanged(SensorPort port, int oldValue, int newValue) {
			
			// hiermit kann man sich einen Eindruck verschaffen, wie oft diese Methode aufgerufen wird und mit welchen Werten.
//			System.out.println(oldValue + "      " + newValue); 
			
			// wie man sieht ändert sich der Wert manchmal nur geringfügig, 
			// d.h. für den TouchSensor würde z.B. folgende Abfrage Sinn machen:
			if(oldValue > 500 && newValue < 500) {
				onTouchPress();
			} else if(oldValue < 500 && newValue > 500) {
				onTouchRelease();
			}
		}
		
	};
	
	/** 
	 * wird aufgerufen wenn der TouchSensor gedrückt wird
	 */
	protected void onTouchPress() {
		System.out.println("Touch gedrückt!");
	}
	
	/** 
	 * wird aufgerufen wenn der TouchSensor wieder losgelassen wird
	 */
	protected void onTouchRelease() {
		System.out.println("Touch losgelassen!");
	}
	
	public void go() throws InterruptedException {
		// Einen Listener hinzufügen
		SensorPort.S1.addSensorPortListener(touchSensorListener);
		
		// Wenn man sonst nichts weiter macht, muss man das Programm am laufen halten:
		while(!Button.ESCAPE.isPressed()) {
			Thread.sleep(10);
		}
		
	}
	
	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		// wir starten unser Programm mit der go()-Methode:
		new SensorListener().go();

	}
	

}
