package freedroidz.tutorial.FlyerRobo;

/**
 * Nur eine Klasse für eine statische Sleep-Methode.
 * @author wschol
 *
 */
public class MySleep {

	//Nur eine statische Wartemethode
	public static void sleep(int timeInMilli){
		try{
			Thread.sleep(timeInMilli);
		}
		catch(Exception except){
			System.out.println(except.toString());
		}
	}
}
