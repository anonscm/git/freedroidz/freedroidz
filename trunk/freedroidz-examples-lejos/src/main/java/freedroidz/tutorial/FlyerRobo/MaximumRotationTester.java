package freedroidz.tutorial.FlyerRobo;

import lejos.nxt.Motor;

public class MaximumRotationTester {

	private final int maxLeft;
	private final int maxRight;

	/** DOES NOT WORK YET */
	public MaximumRotationTester(Motor motor) {
		final int oldPower = motor.getPower();
		motor.setPower(10);
		motor.resetTachoCount();
		motor.forward();
		sleep(5000);
		maxRight = motor.getTachoCount();
		motor.backward();
		sleep(10000);
		maxLeft = motor.getTachoCount();
		motor.rotateTo(0);
		motor.setPower(oldPower);
	}

	private void sleep(int i) {
		try {
			Thread.sleep(i);
		} catch (InterruptedException e) {
		}
	}

	public int getMaxLeft() {
		return maxLeft;
	}

	public int getMaxRight() {
		return maxRight;
	}

}
