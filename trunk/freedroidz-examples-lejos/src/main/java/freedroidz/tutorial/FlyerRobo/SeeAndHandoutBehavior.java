package freedroidz.tutorial.FlyerRobo;

import lejos.nxt.Motor;
import lejos.nxt.UltrasonicSensor;
import lejos.robotics.subsumption.Behavior;

/**
 * Lässt den Roboter anhalten und einen Flyer austeilen, wenn jemand vor ihm steht.
 * 
 * @author wschol
 *
 */
public class SeeAndHandoutBehavior implements Behavior {

	Motor handoutMotor;
	UltrasonicSensor seeSonic;
	UltrasonicSensor checkSonic;
	
	public SeeAndHandoutBehavior(Motor motor, UltrasonicSensor seeSonic, UltrasonicSensor checkSonic){
		
		this.handoutMotor = motor;	//Der Motor, der die Flyer rausschiebt und sie anbietet.
		this.seeSonic = seeSonic;	//Der Ultraschallsensor, der prüft, ob jemand vor ihm steht.
		this.checkSonic = checkSonic;	//Der Ultraschallsensor, der prüft, ob der Flyer genommen wurde.
	}
	
	public SeeAndHandoutBehavior(UltrasonicSensor sonic){	//Kann gelöscht werden, wird nur in ApllianceRotateBehavior verwendet.
		
		this.seeSonic = sonic;
	}
	
	@Override
	public void action() {
		System.out.println("SeeAndHandout action start");
//		Sound.beep();
		this.handoutMotor.rotate(-180);	//Lässt den Motor den Flyer rausgeben.
		MySleep.sleep(5000);	//Wartet, dass der Flyer genommen wird.
		
		if(checkSonic.getDistance() < 10){	//Wenn der Flyer nicht genommen wurde,
			this.handoutMotor.rotate(190);	//wird er wieder eingefahren.
		}

		System.out.println("SeeAndHandout action stop");
	}

	@Override
	public void suppress() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean takeControl() {
		
		return seeSonic.getDistance() < 60 && seeSonic.getDistance() > 30;	//Wenn jemand vor dem Roboter steht.
	}
}
