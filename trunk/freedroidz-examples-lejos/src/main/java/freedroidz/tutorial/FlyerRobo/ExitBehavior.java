package freedroidz.tutorial.FlyerRobo;

import lejos.nxt.Button;
import lejos.robotics.subsumption.Behavior;

/**
 * Behavior, um das Programm zu beenden.
 * @author wschol
 *
 */
public class ExitBehavior implements Behavior {
	
	@Override
	public boolean takeControl(){
		
		return Button.ESCAPE.isPressed();	//Wenn der Escape-Button gedrückt wird...
	}
	
	@Override
	public void action(){
		
		System.exit(0);	//... wird das Programm ausgeschaltet.
	}
	
	@Override
	public void suppress(){
		
	}
}
