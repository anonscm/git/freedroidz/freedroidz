package freedroidz.tutorial.FlyerRobo;

import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;
import lejos.robotics.subsumption.Behavior;

/**
 * Behavior, um die Vorrichtung des Roboters drehen zu lassen.
 * 
 * @author wschol
 * 
 */
public class ApplianceRotateBehavior implements Behavior {

	private Motor applianceMotor;

	private final int maxLeft;
	private final int maxRight;

	/** false is left, true is right */
	private boolean dir = false;

	private boolean stop = false;

	public ApplianceRotateBehavior(Motor motor, final int maxLeft,
			final int maxRight) {
		this.maxLeft = maxLeft;
		this.maxRight = maxRight;

		this.applianceMotor = motor; // Der Motor, der die Vorrichtung dreht.
		this.applianceMotor.setSpeed(100); // Geschwindigkeit runter schrauben,
											// damit sich die Vorrichtung nicht
											// zu schnell dreht.
	}

	@Override
	public boolean takeControl() {
		return true; // we'll take Control if noone else wants to..
	}

	@Override
	public void action() {
		stop = false;
		System.out.println("Rotate action start");
		while (!stop) {
			this.applianceMotor.setPower(100); // Power hochdrehen, damit sich die Vorrichtung auch dreht.
			if (dir) { // right
				this.applianceMotor.rotateTo(maxRight);

			} else { //left 
				this.applianceMotor.rotateTo(maxLeft);
			}
			MySleep.sleep(500); // Eine halbe Sekunde warten, damit es nicht zu
			// schnell geht.

			dir = !dir;
		}

		System.out.println("Rotate action stop");
	}

	@Override
	public void suppress() {
		stop = true;
		this.applianceMotor.stop(); // Den Motor sofort stoppen.
	}
}