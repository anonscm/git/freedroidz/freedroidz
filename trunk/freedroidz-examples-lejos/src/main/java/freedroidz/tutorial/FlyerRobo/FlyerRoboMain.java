package freedroidz.tutorial.FlyerRobo;

import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;
import lejos.robotics.subsumption.Arbitrator;
import lejos.robotics.subsumption.Behavior;

/**
 * Der Roboter soll sich von links nach rechts drehen. Wenn er jemand vor sich
 * sieht, soll er anhalten und demjenigen einen Flyer anbieten. Nimmer derjenige
 * den Flyer innerhalb von 5 Sekunden nicht, wird der Flyer wieder zurück
 * gezogen.
 * 
 * @author wschol
 */
public class FlyerRoboMain {

	private final static Motor applianceMotor = Motor.A;

	Arbitrator arbitrator;

	/**
	 * Main-Methode
	 * 
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {

		FlyerRoboMain flyerRobo = new FlyerRoboMain();
		flyerRobo.initialize();
		flyerRobo.go();
	}

	private void initialize() {

		// MaximumRotationTester maxRotationTester = new
		// MaximumRotationTester(applianceMotor);
		// System.out.println("MaxLeft: " + maxRotationTester.getMaxLeft());
		// System.out.println("MaxRight: " + maxRotationTester.getMaxRight());

		final ApplianceRotateBehavior applianceRotateBehavior = new ApplianceRotateBehavior(
				applianceMotor, -90, 90);
		final SeeAndHandoutBehavior seeAndHandoutBehavior = new SeeAndHandoutBehavior(
				Motor.C, new UltrasonicSensor(SensorPort.S1),
				new UltrasonicSensor(SensorPort.S3));
		final ExitBehavior exitBehavior = new ExitBehavior();

		Behavior[] behavior = { applianceRotateBehavior, seeAndHandoutBehavior, exitBehavior };

		arbitrator = new Arbitrator(behavior);
	}

	private void go() throws InterruptedException {

		arbitrator.start();
	}

}
