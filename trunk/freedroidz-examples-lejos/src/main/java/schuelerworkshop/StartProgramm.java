package schuelerworkshop;

import lejos.nxt.Motor;
import lejos.robotics.navigation.Pilot;
import lejos.robotics.navigation.TachoPilot;

public class StartProgramm {
	Pilot pilot;
	
	public static void main(String[] args) {
		StartProgramm startProgramm = new StartProgramm();
		startProgramm.pilot = new TachoPilot(56, 173, Motor.A, Motor.C);
		startProgramm.run();
	}

	private void run() {
		pilot.travel(100);
		pilot.rotate(90);
	}

}
