package shooterBot;

import freedroidz.tutorial.FlyerRobo.ApplianceRotateBehavior;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;
import lejos.robotics.subsumption.Arbitrator;
import lejos.robotics.subsumption.Behavior;

public class BillyTheKidBot {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Behavior rotate = new ApplianceRotateBehavior(Motor.A, 0, 90);
		Behavior shootBehaviour = new ShootBehavior(Motor.B, Motor.C, new UltrasonicSensor(SensorPort.S1));
		
		Behavior[] behaviors = {rotate, shootBehaviour};
		new Arbitrator(behaviors).start();

	}

}
