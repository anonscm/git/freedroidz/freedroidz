package shooterBot;

import lejos.nxt.Motor;
import lejos.nxt.UltrasonicSensor;
import lejos.robotics.subsumption.Behavior;

public class ShootBehavior implements Behavior {

	final private UltrasonicSensor us;
	final private Motor r;
	final private Motor l;
	final static private int limitAngle = 158;
	
	public ShootBehavior(Motor l, Motor r, UltrasonicSensor ultrasonicSensor) {
		this.l = l;
		this.r = r;
		this.us = ultrasonicSensor;
	}

	@Override
	public void action() {
		r.setPower(100);
		l.setPower(100);
		
		r.rotateTo(-limitAngle, true);
		l.rotateTo(-limitAngle, false);
		
		r.rotateTo(0, true);
		l.rotateTo(0, false);
		
	}

	@Override
	public void suppress() {
		//there's nothing to do
	}

	@Override
	public boolean takeControl() {
		return us.getDistance() < 60;
	}

}
