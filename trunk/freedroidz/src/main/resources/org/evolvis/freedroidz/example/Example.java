package org.freedroidz.example;

import lejos.nxt.Button;
import lejos.nxt.ButtonListener;
import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.robotics.navigation.TachoPilot;

public class Example {
	
	float wheelDiameter = 55;
	float trackWidth = 134;
	Motor leftMotor = Motor.A;
	Motor rightMotor = Motor.C;
	TachoPilot pilot = new TachoPilot(wheelDiameter, trackWidth, leftMotor, rightMotor);
	
	public static void main(int[] args) throws InterruptedException {
		Example example = new Example();	
		example.doSomething();
	}

	private void doSomething() throws InterruptedException {	
		pilot.forward();		// fahre vorwaerts
		Thread.sleep(1000);		// tue dies 1 Sekunde		
		pilot.travel(-10);		// fahre 10mm nach hinten		
		pilot.rotate(90); 		//drehe dich um 90Grad		
		pilot.arc(100, 45);		//fahre 45 Grad eines Kreises mit 100mm Durchmesser	
	}	
	
	void lichtSensor() {	
		LightSensor light = new LightSensor(SensorPort.S1);
		if(light.getLightValue() > 50) {
		   // wenn der Lichtwert ueber 50% ist
		} else {
		     // wird aufgerufen wenn der Lichtwert unter/gleich 50% ist.
		}	
	}
	
	void schreibeEtwasAufsDisplay() {		
		System.out.println("Hallo Leute!!!");	
	}
	
	boolean runProgram = true;	
	void tueIrgendwas() {	
		Button.ESCAPE.addButtonListener(new ButtonListener() {			
			public void buttonReleased(Button arg0) {
				// nichts machen
			}			
			public void buttonPressed(Button arg0) {
				// Programm beenden
				runProgram = false;
			}
		});
		while(runProgram) {
			//tue etwas... bis ESCAPE Button gedrückt wird
		}
	}
	
	
	void forSchleife() {	
		for(int i = 0; i < 10; i++) {
			// das hier wird 10 mal aufgerufen
		}	
	}
	
}
