#!/bin/sh

user=$1

echo Benutzer: $user
sleep 3s

apt-get -y install libusb-dev libbluetooth-dev

addgroup nxj
adduser  $user nxj
adduser root nxj

sleep 3s

echo SUBSYSTEM==\"usb\", ATTR{idVendor}==\"0694\", ATTR{idProduct}==\"0002\", GROUP=\"nxj\", MODE=\"0666\" > /etc/udev/rules.d/01-lego-nxj.rules
echo SUBSYSTEM==\"usb\", ATTR{idVendor}==\"03eb\", ATTR{idProduct}==\"6124\", GROUP=\"nxj\", MODE=\"0666\" >> /etc/udev/rules.d/01-lego-nxj.rules

dist=$(cat /etc/issue)

echo $dist

for i in $dist
do
	if [ $i==*8.* ];
	then
		udevcontrol reload_rules
		echo Ubuntu 8.X
	else
		echo Ubuntu 9.04 - latest
		udevadm control --reload-rules
	fi
done
