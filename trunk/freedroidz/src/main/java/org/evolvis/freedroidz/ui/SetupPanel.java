package org.evolvis.freedroidz.ui;

import java.util.ResourceBundle;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.evolvis.freedroidz.Starter;
import org.evolvis.freedroidz.lejos.Utilities;

/**
 * Simple panel to ask if freedroidz should run it's setup script.
 * @author "T.Wylegala"
 *
 */
public class SetupPanel{

	
	String baseName = "org.evolvis.freedroidz.SetupPanel"; 
    final ResourceBundle bundle = ResourceBundle.getBundle( baseName ); 
    
	Shell parent = null;
	
	public static void main(String[] args){
		new SetupPanel();
	}
	
	public SetupPanel(){
		parent = new Shell(Display.getDefault());
		initGUI();
		parent.pack();
		parent.open();
		
		while(!parent.isDisposed())
			Display.getDefault().readAndDispatch();
	}
	
	public void initGUI(){
		parent.setBounds(Display.getDefault().getBounds().width/2 - 150, Display.getDefault().getBounds().height/2 - 100, 300, 200);
		parent.setLayout(new MigLayout());
		parent.setImages(Starter.getIcons());
		
		Label headlineLabel = new Label(parent, 0);
		headlineLabel.setFont(new Font(Display.getDefault(), new FontData("Sans", 9, SWT.NONE))); //$NON-NLS-1$
		headlineLabel.setText(bundle.getString("SetupPanel.0"));// + //$NON-NLS-1$
		headlineLabel.setLayoutData("span, wrap"); //$NON-NLS-1$
		
		Button scriptButton = new Button(parent, 0);
		scriptButton.setText(bundle.getString("SetupPanel.3"));
		scriptButton.setLayoutData("span, growx");
		
		Button okButton = new Button(parent, 0);
		okButton.setText(bundle.getString("SetupPanel.1")); //$NON-NLS-1$
		okButton.setLayoutData("span, growx"); //$NON-NLS-1$
		
		Button cancelButton = new Button(parent, 0);
		cancelButton.setText(bundle.getString("SetupPanel.2")); //$NON-NLS-1$
		cancelButton.setLayoutData("span, growx, wrap"); //$NON-NLS-1$
		
		okButton.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent arg0) {	
			}

			public void widgetSelected(SelectionEvent arg0) {
				parent.dispose();
				Utilities.runScript();
			}
			
		});
		
		cancelButton.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent arg0) {	
			}

			public void widgetSelected(SelectionEvent arg0) {
				parent.dispose();
			}
			
		});
		
		scriptButton.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			public void widgetSelected(SelectionEvent arg0) {
				new ScriptPanel(parent);
			}
			
		});
	}

}
