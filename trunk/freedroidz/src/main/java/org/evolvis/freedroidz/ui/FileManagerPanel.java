package org.evolvis.freedroidz.ui;

import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;
import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.evolvis.freedroidz.config.Config;
import org.evolvis.freedroidz.lejos.FileManager;


public class FileManagerPanel extends Composite {
	
	private Composite parent;
	private Text text;
	private List list; 
	private boolean connected = false;
	private Button connectButton;
	private Button updateButton;
	private Button deleteButton;
	private Button runButton;
	
	public FileManagerPanel(Composite parent){
		super(parent, 0);
		this.parent = parent;
		initGUI();
	}
	
	public void initGUI(){
		
		
		Layout layout = new MigLayout("", "[49%!,fill][49%!,fill]", "[10%!,fill][10%!,fill][55%!,fill][10%!,fill]"); 
		setLayout(layout);


		final Combo connectionTypeCombo = new Combo(this, SWT.READ_ONLY);
		connectionTypeCombo.add("Zuletzt verwendeter NXT");
		connectionTypeCombo.add("Mit neuem NXT verbinden");
		connectionTypeCombo.select(0);
		
		
		connectButton = new Button(this, SWT.BORDER);
		connectButton.setText("Verbinden");
		connectButton.setLayoutData("wrap");
		
		updateButton = new Button(this, SWT.NONE);
		updateButton.setText("Update");
		updateButton.setLayoutData("span 2, growx, growy, wrap");
		
		updateButton.addSelectionListener(new SelectionListener(){
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				updateFileList();
			}
			
		});
		
		updateButton.setEnabled(false);
		
		connectButton.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent arg0) {	
			}

			public void widgetSelected(SelectionEvent arg0) {
				connectToNXT(connectionTypeCombo.getSelectionIndex());
			}
			
		});

		list = new List(this, SWT.NONE);
		list.setLayoutData("span 2, growx, growy, wrap");
		list.setEnabled(false);
		
		list.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				Object[] objects = FileManager.getInstance().getFiles();
				String tmp = (String)objects[list.getSelectionIndex()];
				if(tmp.endsWith(".nxj")){
					runButton.setEnabled(true);
				}else{
					runButton.setEnabled(false);
				}
			}
			
		});
		
		
		runButton = new Button(this, SWT.NONE);
		runButton.setImage(new Image(getDisplay(), this.getClass().getResourceAsStream("/org/evolvis/freedroidz/gfx/run.png")));
		runButton.setToolTipText("Starten");
		runButton.addSelectionListener(new SelectionListener(){
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}
			public void widgetSelected(SelectionEvent arg0) {				
				Object[] objects = FileManager.getInstance().getFiles();
				if(objects.length > 0){
					runFile((String)objects[list.getSelectionIndex()]);
				}
			}	
		});
		runButton.setEnabled(false);
		
		deleteButton = new Button(this, SWT.NONE);
		deleteButton.setImage(new Image(getDisplay(), this.getClass().getResourceAsStream("/org/evolvis/freedroidz/gfx/delete.gif")));
		deleteButton.setToolTipText("Löschen");
		deleteButton.addSelectionListener(new SelectionListener(){
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}
			public void widgetSelected(SelectionEvent arg0) {
				Object[] objects = FileManager.getInstance().getFiles();
				if(objects.length > 0){
					deleteFile((String)objects[list.getSelectionIndex()]);
				}
				updateFileList();
			}	
		});
		deleteButton.setEnabled(false);
		
	}
	
	private void deleteFile(String filename){
		FileManager.getInstance().deleteFile(filename);
	}
	
	private void runFile(String filename){
		FileManager.getInstance().runFile(filename);
	}
	
	private void updateFileList(){
		Object[] files = FileManager.getInstance().getFiles();
		list.removeAll();
		
		if(files != null){
			for(Object file: files){
				list.add((String)file);
			}
		}
	}
	
	private void connectToNXT(int selection){
		Config.getInstance().loadSavedInfo();
		if(connected == false){
			if(selection == 0){
				NXTInfo nxt = Config.getInstance().getNXTInfo();
				FileManager.getInstance().initConnection(nxt);
				updateFileList();
			}else{
				NXTInfo nxt = LejosScanPanel.runGUI(NXTCommFactory.ALL_PROTOCOLS);
				FileManager.getInstance().initConnection(nxt);
				updateFileList();
			}
			connectButton.setText("Disconnect");
			list.setEnabled(true);
			updateButton.setEnabled(true);
			runButton.setEnabled(true);
			deleteButton.setEnabled(true);
			connected=true;
		}else{
			FileManager.getInstance().disconnect();
			connectButton.setText("Verbinden");
			list.setEnabled(false);
			updateButton.setEnabled(false);
			runButton.setEnabled(false);
			deleteButton.setEnabled(false);
			connected=false;
		}
	}
	
	
	public static void main(String[] args){
		Shell shell = new Shell(Display.getDefault());
		shell.setLayout(new FillLayout());
		
		Composite compsite = new Composite(shell, SWT.BORDER);
		FillLayout fillLayout = new FillLayout();
		fillLayout.spacing = 1;
		compsite.setLayout(fillLayout);

		shell.setBounds(10,10,900,300);
		
		final FileManagerPanel fmp = new FileManagerPanel(compsite);
		shell.open();
		while(!shell.isDisposed()){
			Display.getDefault().readAndDispatch();
		}
		
		Display.getDefault().dispose();
	}

}
