package org.evolvis.freedroidz.lejos;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;

import org.evolvis.freedroidz.Log;

public class NativeLibraryManager {

	public static NativeLibraryManager nativeLibraryManager = null;
	
	private File libjlibnxt = null;
	
	public void checkAndAdd(){
		try {	
			File nativeDir = new File(System.getProperty("java.io.tmpdir"),"JNIFiles");
			this.libjlibnxt = new File(nativeDir,"libjlibnxt.so");
			
			
			if(!nativeDir.exists()){
				nativeDir.mkdir();
				createNativeLibs();
			}
			
			addToJavaLibraryPath(nativeDir.getAbsolutePath());
			
		} catch (Throwable e) {
			RuntimeException re = new RuntimeException("cannot locate lejos runtime lib (libjibnxt.so)",e);
			Log.log(re);
			throw re;
		}
	}
	
	public void addToJavaLibraryPath(String s) throws IOException {
		try {
			Field field = ClassLoader.class.getDeclaredField("usr_paths");
			field.setAccessible(true);
			String[] paths = (String[])field.get(null);
			for (int i = 0; i < paths.length; i++) {
				if (s.equals(paths[i])) {
					return;
				}
			}
			String[] tmp = new String[paths.length+1];
			System.arraycopy(paths,0,tmp,0,paths.length);
			tmp[paths.length] = s;
			field.set(null,tmp);
		} catch (IllegalAccessException e) {
			throw new IOException("Permission denied");
		} catch (NoSuchFieldException e) {
			throw new IOException("unable to find library");
		}
	}
	
	
	public File createNativeLibs(){
		
		FileOutputStream fos = null; 
		InputStream nativeInputStream = null;
		try {
			this.libjlibnxt.createNewFile();
			fos = new FileOutputStream(this.libjlibnxt);
			String path = "native"+File.separatorChar+System.getProperty("os.arch")+File.separatorChar+"libjlibnxt.so";
			nativeInputStream = this.getClass().getResourceAsStream(path);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		int ARRAY_SIZE = 2048;		
		byte[] buf = new byte[ARRAY_SIZE];
			
		int Len = 0;
        int NumRead = 0;
        try {
			while ((NumRead=nativeInputStream.read(buf, 0, ARRAY_SIZE)) > 0) {
				Len += NumRead;
				fos.write(buf, 0, NumRead);
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		return libjlibnxt;
	}
	
	public static NativeLibraryManager getInstance(){
		if(nativeLibraryManager == null)
			nativeLibraryManager = new NativeLibraryManager();
		return nativeLibraryManager;
	}
}
