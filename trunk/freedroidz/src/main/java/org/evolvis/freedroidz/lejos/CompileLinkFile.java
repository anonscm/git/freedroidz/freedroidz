package org.evolvis.freedroidz.lejos;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;

import js.common.ToolProgressMonitor;
import js.tinyvm.TinyVM;
import js.tinyvm.TinyVMException;

import org.apache.commons.io.FileUtils;
import org.evolvis.freedroidz.Log;
import org.evolvis.freedroidz.Starter;

public class CompileLinkFile {

	public String errors = "";
	List<Diagnostic<JavaFileObject>> diagnostics = null;

	
	private File classesJar = null;
	public static Random random = null;

	public CompileLinkFile(){
		try {	
			this.classesJar = new File(Utilities.getFreedroidzDir(),"classes.jar");
			
			if(!this.classesJar.exists() || !checkVersion())
				createClassesJar();
			
		} catch (Throwable e) {
			RuntimeException ex = new RuntimeException("cannot locate lejos runtime classes (classes.jar)",e);
			Log.log(ex);
			throw ex;
		}
	}
	
	public CompileLinkFile(File classesJar){
		this.classesJar=classesJar;
	}
	
	public boolean startCompile(String mainFile) {
		
		String mainFileWithoutJava = "";
		
		int dot = mainFile.lastIndexOf(".");
		mainFileWithoutJava = mainFile.substring(0, dot);
		
		return compile(mainFileWithoutJava);
	}
	
	public boolean startLink(String classDefinition, String classpath, File outputFile) {
//		System.out.println(outputFile.getAbsolutePath());
		return link(classDefinition, classpath, outputFile);
	}

	
	public Collection<File> searchSourceFiles(File root) throws IOException {
		String[] extensions = { "java" };
		return FileUtils.listFiles(root, extensions, true);
	}

	/**
	 * 
	 * 
	 * @param pathToSourceFile
	 * @return true on success, false if there are compile errors.
	 * @deprecated If this method is supposed to compile all source files within
	 *             a directory, why not just pass a reference to that directory?
	 *             My suggestion: use compile(Collection&lt;File&gt;)) instead.
	 */
	@Deprecated
	public boolean compile(String pathToSourceFile) {
		File sourceFolder = new File(pathToSourceFile).getParentFile();
		try {
			Collection<File> sourceFiles = searchSourceFiles(sourceFolder);
			return compile(sourceFiles,sourceFolder);
		} catch (IOException e) {
			e.printStackTrace();
			Log.log(e);
			return false;
		}

	}

	public boolean compile(Collection<File> sourceFiles,File destinationDirectory) {
		return new NewAPICompileStrategy(this).compile(sourceFiles, destinationDirectory, System.out,
				new ProblemCollector(){

					@Override
					public void addProblem(String message, File sourceFile,
							long lineNumber) {
						
					}});
	}

	/**
	 * Constructs an array of javac command line arguments. This is a
	 * convenience method for contructing command line options in format
	 * suitable for passing to functions like Runtime.exec() and friends.
	 * 
	 * @param sourcesList
	 *            A list of strings containing the path to each source file that
	 *            should be compiled.
	 * @param destinationDirectory 
	 * @param includeExecutable
	 *            If set, the first element of the returned array will be the
	 *            string "javac"
	 * @return A string array.
	 */
	public String[] buildJavacCommandLineArray(Collection<File> sourcesList,
			File destinationDirectory, boolean includeExecutable) {
		Collection<String> args = buildJavacComandLineList(sourcesList,destinationDirectory,
				includeExecutable);
		return args.toArray(new String[args.size()]);
	}

	/**
	 * Constructs an List of javac command line arguments. Same as
	 * buildJavacCommandLineArray(), but uses a list. This is more convenient
	 * when working with the JRS 199 API (have a look at NewAPICompileStrategy).
	 * 
	 * @param sourcesList
	 *            A list of strings containing the path to each source file that
	 *            should be compiled.
	 * @param destinationDirectory 
	 * @param includeExecutable
	 *            If set, the first element of the returned array will be the
	 *            string "javac"
	 * @return A string array.
	 */
	public List<String> buildJavacComandLineList(Collection<File> sourcesList,
			File destinationDirectory, boolean includeExecutable) {
		ArrayList<String> args = new ArrayList<String>();
		// String[] args = new String[3 + sourcesList.size()];
		if (includeExecutable) {
			args.add("javac");
		}

		args.add("-bootclasspath");

		// @Torsten: you tried to do
		// args[2] = pathToClasses+":$CLASSPATH";
		//		   
		// This will breaks, since it:
		// - assumes a posix-like system
		// - assumes bash-like shell expansion which doesn't happen with
		// Runtime.exec()
		//		  
		// Below is my solution. As another alternative, you could probably
		// also pass the updated classpath to the child process environment.
		//		
		String globalClassPath = System.getenv("CLASSPATH");
		if (globalClassPath == null) {
			args.add(getPathToClassesJar());
		} else {
			args.add(getPathToClassesJar() + File.pathSeparator
					+ globalClassPath);
		}
		if (sourcesList != null) {
			for (File file : sourcesList) {
				args.add(file.getAbsolutePath());
			}
		}
		args.add("-d");
		args.add(destinationDirectory.getAbsolutePath());
		return args;
	}
	
	public boolean link(String fullyQualifiedClassName, String classPath, File nxjFile) {
		
		TinyVM fTinyVM = new TinyVM();
		String[] VMArgs = new String[1];
		fTinyVM.addProgressMonitor(new ToolProgressMonitor(){

			@Override
			public boolean isCanceled() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void log(String arg0) {
				Log.log(arg0);
			}

			@Override
			public void operation(String arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void progress(int arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setVerbose(boolean arg0) {
				// TODO Auto-generated method stub
				
			}});
		String outputName = nxjFile.getAbsolutePath();
		
		if(!outputName.endsWith(".nxj"))
			outputName = outputName+".nxj";

		VMArgs[0] = fullyQualifiedClassName;
		
		try {
			fTinyVM.start(getPathToClassesJar()+File.pathSeparatorChar+classPath, VMArgs, false, outputName, false, false, 0, false);
		} catch (TinyVMException e) {
			e.printStackTrace();
		}
		
		return true;
	}
	
	public void createClassesJar(){
		
		FileOutputStream fos = null; 
		InputStream classesInputStream = null;
		try {
			
			this.classesJar.createNewFile();
			fos = new FileOutputStream(this.classesJar);
			classesInputStream = this.getClass().getResourceAsStream("classes.jar");
			
		} catch (IOException e) {
			e.printStackTrace();
			Log.log(e);
		}
		
		int ARRAY_SIZE = 2048;		
		byte[] buf = new byte[ARRAY_SIZE];
			
		int Len = 0;
        int NumRead = 0;
        try {
			while ((NumRead=classesInputStream.read(buf, 0, ARRAY_SIZE)) > 0) {
				Len += NumRead;
				fos.write(buf, 0, NumRead);
			}
		} catch (IOException e1) {
			e1.printStackTrace();
			Log.log(e1);
		}
	}


	public String getErrors() {
		return errors;
	}

	public void setClassesJar(File classesJar) {
		this.classesJar= classesJar;

	}

	public File getClassesJar() {
		return this.classesJar;
	}

	/**
	 * @deprecated use getClassesJar().getAbsolutePath();	  
	 */
	@Deprecated 
	private String getPathToClassesJar() {
		return classesJar.getAbsolutePath();
	}

	/**
	 * Convenience method for creating temporary directories. This works in a
	 * similar fashion as File.createTempFile(). Note that the directory is
	 * <b>not</b> automatically deleted.
	 * 
	 * @param parent
	 *            the parent directory
	 * @param prefix
	 *            the prefix to use for the filename. A random number will be
	 *            appended.
	 * @return the created directory
	 * @throws IOException
	 */
	public static File createTempDir(File parent, String prefix)
			throws IOException {
		if (CompileLinkFile.random == null) {
			CompileLinkFile.random = new Random();
		}
	
		File tmpdir = null;
		do {
			long l = CompileLinkFile.random.nextLong();
			tmpdir = new File(parent, prefix + l);
		} while (tmpdir.exists());
		tmpdir.mkdir();
		return tmpdir;
	}
	
	public boolean checkVersion(){
		File versionFile = new File(Utilities.getFreedroidzDir(),"freedroidz.version");
		
		
		if(!versionFile.exists()){
			writeVersionToFile();
			return false;
		}
		
		BufferedInputStream bis = null;
		try {
			bis = new BufferedInputStream(new FileInputStream(versionFile));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		byte[] input = new byte[1028];
		try {
			bis.read(input);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String localVersionString = new String(input);
		
		if(localVersionString.equals(Starter.getVersion())){
			return true;
		}else{
			writeVersionToFile();
			return false;
		}
	}
	
	private void writeVersionToFile(){
		File versionFile = new File(Utilities.getFreedroidzDir(),"freedroidz.version");

		BufferedOutputStream bos = null;
		try {
			bos = new BufferedOutputStream(new FileOutputStream(versionFile));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			bos.write(Starter.getVersion().getBytes());
			bos.flush();
			bos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
