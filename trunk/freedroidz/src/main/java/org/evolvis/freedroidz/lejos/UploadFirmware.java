package org.evolvis.freedroidz.lejos;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTConnector;
import lejos.pc.comm.NXTInfo;
import lejos.pc.comm.NXTSamba;
import lejos.pc.tools.NXJFlashUI;
import lejos.pc.tools.NXJFlashUpdate;

import org.evolvis.freedroidz.Log;
import org.evolvis.freedroidz.ui.FirmwareProgressPanel;


public class UploadFirmware implements NXJFlashUI, LejosFunction{

	private static final int MAX_FIRMWARE_PAGES = 368;
	private static final int TOTAL_PAGES = 1024;
	private static final int SETTINGS_PAGES = 1;
	private static final int DIRECTORY_PAGES = 2;
	private static final int MENU_ADDRESS_LOC = 0x40;
	private static final int MENU_LENGTH_LOC = MENU_ADDRESS_LOC + 4;
	private static final int FLASH_START_PAGE_LOC = MENU_LENGTH_LOC + 4;
	private static final int REBOOT_ADDRESS = 0x00100000;
	
	
	NXJFlashUpdate updater = new NXJFlashUpdate(this);

	private LejosUICallback callback;
	private FirmwareProgressPanel fpp;
	
	public UploadFirmware(){
		NativeLibraryManager.getInstance().checkAndAdd();
	}
	
	/**
	 * Format and store a 32 bit value into a memory image.
	 * 
	 * @param mem
	 *            The image in which to store the value
	 * @param offset
	 *            The location in bytes in the image
	 * @param val
	 *            The value to be stored.
	 */
	void storeWord(byte[] mem, int offset, int val) {
		mem[offset++] = (byte) (val & 0xff);
		mem[offset++] = (byte) ((val >> 8) & 0xff);
		mem[offset++] = (byte) ((val >> 16) & 0xff);
		mem[offset++] = (byte) ((val >> 24) & 0xff);
	}

	public static void main(String[] args) {
		
//		try {
//			new UploadFirmware().flashBrick();
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (NXTCommException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

	}

	public boolean flashBrick() throws FileNotFoundException, IOException,
			NXTCommException {
		
//		addToJavaLibraryPath("/home/twyleg/workspace/freedroidz/target/native/i386/");
		
		byte[] memoryImage = createFirmwareImage(
				UploadFirmware.class.getResourceAsStream("/org/evolvis/freedroidz/lejos/lejos_nxt_rom.bin") ,
				UploadFirmware.class.getResourceAsStream("/org/evolvis/freedroidz/lejos/StartUpText.bin")); 
		
		NXTSamba nxt = openDevice();
		if (nxt != null) {
			updateDevice(nxt, memoryImage);
			callback.processDone(LejosUICallback.OPERATION_SUCCESS);
			return true;
		}else{
			callback.processDone(LejosUICallback.OPERATION_FAIL);
		}
		return false;
	}
	
	private static int getPageAddr(int page){
		return NXTSamba.FLASH_BASE + page * NXTSamba.PAGE_SIZE; 
	}
	
	public void writePages(NXTSamba nxt, int first, byte[] memoryImage) throws IOException {
		int pages = memoryImage.length / NXTSamba.PAGE_SIZE;
		int p = -1;
		updateProgress("Upload");
		for (int page = 0; page < pages; page++) {
			int np = page * 100 / pages;
			if (np > p){
				p = np;
			}
			updateProgress(page * 100 / pages);

			nxt.writePage(first + page, memoryImage, page	* NXTSamba.PAGE_SIZE);
		}
		updateProgress("Fertig");
		updateProgress(100);

		//workaround the problem, that verification and rebooting fails directly after write
		nxt.readWord(getPageAddr(first));

	}
	
	public byte[] createFilesystemImage() {
		byte[] fs = new byte[NXTSamba.PAGE_SIZE
				* (TOTAL_PAGES - MAX_FIRMWARE_PAGES)];
		// First few pages are settings are directory, these must all be
		// cleared to zero. After that we fill with a test pattern, to help
		// spot any flash problems
		int addr = (SETTINGS_PAGES + DIRECTORY_PAGES) * NXTSamba.PAGE_SIZE;
		while (addr <= (fs.length - 32)) {
			storeWord(fs, addr, addr);
			addr += 4;
			storeWord(fs, addr, ~addr);
			addr += 4;
			storeWord(fs, addr, 0xf0f0f0f0);
			addr += 4;
			storeWord(fs, addr, 0x0f0f0f0f);
			addr += 4;
			storeWord(fs, addr, 0xaaaaaaaa);
			addr += 4;
			storeWord(fs, addr, 0x55555555);
			addr += 4;
			storeWord(fs, addr, 0x00000000);
			addr += 4;
			storeWord(fs, addr, 0xffffffff);
			addr += 4;
		}
		return fs;
	}
	
	public void writeFilesystem(NXTSamba nxt, byte[] fs) throws IOException {
		nxt.unlockAllPages();
		writePages(nxt, MAX_FIRMWARE_PAGES, fs);
	}
	
	public void rebootDevice(NXTSamba nxt) throws IOException {
		nxt.reboot();
		nxt.close();
	}

	void updateDevice(NXTSamba nxt, byte[] memoryImage) throws IOException {
		// System.out.println("NXT now open in firmware update mode.");
		// System.out.println("Unlocking pages.");
		nxt.unlockAllPages();
		// System.out.println("Writing memory image...");
//		nxt.writePages(0, memoryImage, 0, memoryImage.length);
		
		writePages(nxt, 0, memoryImage);	
		writeFilesystem(nxt, createFilesystemImage());
		
		// System.out.println("Starting new image.");
//		nxt.jump(REBOOT_ADDRESS);
		
		rebootDevice(nxt);
		
		nxt.close();
	}

	NXTSamba openDevice() throws IOException {
		// First look to see if there are any devices already in SAM-BA mode
		NXTSamba samba = null;
		try{
			samba = updater.openSambaDevice(0);
		}catch(NXTCommException e){
			Log.log(e);
		}
		// Look for devices in SAM-BA mode
		if (samba == null) {
			NXTInfo[] nxts;
			System.out
					.println("No devices in firmware update mode were found.\nSearching for other NXT devices.");
			Log.log("No devices in firmware update mode were found.\nSearching for other NXT devices.");
			NXTConnector conn = new NXTConnector();
			nxts = conn.search(null, null, NXTCommFactory.USB);
			if (nxts.length <= 0) {
				System.out
						.println("No NXT found. Please check that the device is turned on and connected.");
				Log.log("No NXT found. Please check that the device is turned on and connected.");
				return null;
			}
			int devNo = 0; 
			do {
				System.out
						.println("The following NXT devices have been found:");
				for (int i = 0; i < nxts.length; i++)
					System.out.println("  " + (i + 1) + ":  " + nxts[i].name
							+ "  " + nxts[i].deviceAddress);
				System.out
						.println("Select the device to update, or enter 0 to exit.");
				System.out.print("Device number to update: ");
				devNo = getChoice();
			} while (devNo < 0 || devNo > nxts.length);
			if (devNo == 0)
				return null;
			try{
				updater.resetDevice(nxts[devNo - 1]);
				samba = updater.openSambaDevice(30000);
			}catch(NXTCommException e){
				Log.log(e);
			}
		}
		if (samba == null)
			System.out
					.println("No NXT found. Please check that the device is turned on and connected.");
		return samba;
	}
	
	public byte[] createFirmwareImage(InputStream vmName, InputStream menuName) throws IOException, FileNotFoundException {
		byte[] memoryImage = new byte[MAX_FIRMWARE_PAGES * NXTSamba.PAGE_SIZE];
		
		
		int vmLen = 0;
        int vmNumRead = 0;
        try {
			while (vmLen < memoryImage.length && (vmNumRead=vmName.read(memoryImage, vmLen, memoryImage.length-vmLen)) > 0) {
				vmLen += vmNumRead;
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		int menuStart = ((vmLen + NXTSamba.PAGE_SIZE - 1) / NXTSamba.PAGE_SIZE)
				* NXTSamba.PAGE_SIZE;
		
		int menuLen = 0;
        int menuNumRead = 0;
        try {
			while (menuLen+vmLen < memoryImage.length && (menuNumRead=menuName.read(memoryImage, menuStart+menuLen, memoryImage.length-(menuStart+menuLen))) > 0) {
				menuLen += menuNumRead;
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		storeWord(memoryImage, MENU_LENGTH_LOC, menuLen);
		storeWord(memoryImage, MENU_ADDRESS_LOC, menuStart
				+ NXTSamba.FLASH_BASE);
		storeWord(memoryImage, FLASH_START_PAGE_LOC, MAX_FIRMWARE_PAGES);
		
//		System.out.println("menuLen: "+menuLen+" vmLen: "+vmLen+"  "+memoryImage.length);
		
		if (menuStart + menuLen >= memoryImage.length) {
			throw new IOException("Combined size of VM and Menu > "
					+ memoryImage.length);
		}
		return memoryImage;
	}
	

	int getChoice() throws IOException {
		// flush any old input
		while (System.in.available() > 0)
			System.in.read();
		char choice = (char) System.in.read();
		// flush any old input
		while (System.in.available() > 0)
			System.in.read();
		if (choice >= '0' && choice <= '9')
			return (int) (choice - '0');
		return -1;
	}

	@Override
	public void message(String arg0) { 
		
	}

	@Override
	public void progress(String arg0, int arg1) {
	}
	
//	public static void addToJavaLibraryPath(String s) throws IOException {
//		try {
//			Field field = ClassLoader.class.getDeclaredField("usr_paths");
//			field.setAccessible(true);
//			String[] paths = (String[])field.get(null);
//			for (int i = 0; i < paths.length; i++) {
//				if (s.equals(paths[i])) {
//					return;
//				}
//			}
//			String[] tmp = new String[paths.length+1];
//			System.arraycopy(paths,0,tmp,0,paths.length);
//			tmp[paths.length] = s;
//			field.set(null,tmp);
//		} catch (IllegalAccessException e) {
//			throw new IOException("Permission denied");
//		} catch (NoSuchFieldException e) {
//			throw new IOException("unable to find library");
//		}
//	}
//	
//	
//	public File createNativeLibs(){
//		
//		FileOutputStream fos = null; 
//		InputStream nativeInputStream = null;
//		try {
//			this.libjlibnxt.createNewFile();
//			fos = new FileOutputStream(this.libjlibnxt);
//			String path = "native"+File.separatorChar+System.getProperty("os.arch")+File.separatorChar+"libjlibnxt.so";
//			nativeInputStream = this.getClass().getResourceAsStream(path);
//			
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		
//		int ARRAY_SIZE = 2048;		
//		byte[] buf = new byte[ARRAY_SIZE];
//			
//		int Len = 0;
//        int NumRead = 0;
//        try {
//			while ((NumRead=nativeInputStream.read(buf, 0, ARRAY_SIZE)) > 0) {
//				Len += NumRead;
//				fos.write(buf, 0, NumRead);
//			}
//		} catch (IOException e1) {
//			e1.printStackTrace();
//		}
//		
//		return libjlibnxt;
//	}

	@Override
	public void registerCallback(LejosUICallback callback) {
		this.callback = callback;
		
	}
	
	private void updateProgress(int percent){
		if(this.fpp != null){
			fpp.updateProgressbar(percent);
		}
	}
	
	private void updateProgress(String action){
		if(this.fpp != null){
			fpp.updateProgressbar(action);
		}
	}
	
	public void registerFirmwareProgressPanel(FirmwareProgressPanel fpp){
		this.fpp = fpp;
	}

}
