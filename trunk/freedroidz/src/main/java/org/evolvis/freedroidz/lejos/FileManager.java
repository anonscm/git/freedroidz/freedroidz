package org.evolvis.freedroidz.lejos;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lejos.nxt.remote.NXTCommand;
import lejos.pc.comm.NXTComm;
import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;
import lejos.pc.tools.ExtendedFileModel;

public class FileManager {
	private NXTCommand nxtCommand;
	private static FileManager fileManager;
	private NXTComm nxtComm;
	private ArrayList<String> files;

	public void initConnection(NXTInfo nxt) {

		try {
			nxtComm = NXTCommFactory.createNXTComm(nxt.protocol);
			nxtComm.open(nxt, NXTComm.LCP);
			nxtCommand = NXTCommand.getSingleton();
			nxtCommand.setNXTComm(nxtComm);
		} catch (NXTCommException e) {
			e.printStackTrace();
		}
	}
	
	public void disconnect(){
		try {
			nxtComm.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void deleteFile(String name) {
		try {
			if (nxtCommand != null)
				nxtCommand.delete(name);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void runFile(String name) {
		try {
			if (nxtCommand != null)
				nxtCommand.startProgram(name);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Object[] getFiles() {
		if (nxtCommand != null) {
			ExtendedFileModel efm = new ExtendedFileModel(nxtCommand);
			int i = efm.getRowCount();
			ArrayList<String> files = new ArrayList<String>();

			for (int j = 0; j < i; j++) {
				files.add(efm.getFile(j).fileName);
			}
			return files.toArray();
		} else {
			return null;
		}
	}
	
	public static FileManager getInstance(){
		if(fileManager == null){
			fileManager = new FileManager();
		}
		return fileManager;
			
	}
}
