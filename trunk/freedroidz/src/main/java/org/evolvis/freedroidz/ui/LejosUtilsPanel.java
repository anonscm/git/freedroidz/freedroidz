package org.evolvis.freedroidz.ui;

import java.util.ResourceBundle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.evolvis.freedroidz.Starter;

public class LejosUtilsPanel extends Composite{
	
    String baseName = "org.evolvis.freedroidz.LejosUtilsPanel"; //$NON-NLS-1$
    final ResourceBundle bundle = ResourceBundle.getBundle( baseName ); 
	
	public LejosUtilsPanel(Composite parent){
		super(parent,SWT.BORDER);
		initGui();
		pack();
	}
	
	public void initGui(){
		
		Image UploadFileImage = new Image(getDisplay(), this.getClass().getResourceAsStream("/org/evolvis/freedroidz/gfx/go-jump.png")); //$NON-NLS-1$
		Button uploadButton = new Button(this, 0);
		uploadButton.setText(bundle.getString("LejosUtilsPanel.2")); //$NON-NLS-1$
		uploadButton.setBounds(0,0,180,80);
		uploadButton.setImage(UploadFileImage);
		uploadButton.setToolTipText(bundle.getString("LejosUtilsPanel.3")); //$NON-NLS-1$
		uploadButton.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			public void widgetSelected(SelectionEvent arg0) {
				Shell dialog = new Shell (Display.getDefault());
				dialog.setText (bundle.getString("LejosUtilsPanel.4")); //$NON-NLS-1$
				dialog.setBounds(Display.getDefault().getBounds().width/2-360,Display.getDefault().getBounds().height/2-125,720,250);
				dialog.setLayout(new FillLayout());
				dialog.setImages(Starter.getIcons());
		
				LejosUploadFilePanel lejosUploadPanel = new LejosUploadFilePanel(dialog);

				dialog.pack();
				dialog.open ();
			}
			
		}); 
		
		Image FirmwareUploadImage = new Image(getDisplay(), this.getClass().getResourceAsStream("/org/evolvis/freedroidz/gfx/document-save.png")); //$NON-NLS-1$
		Button firmwareUploadButton = new Button(this, 0);
		firmwareUploadButton.setText(bundle.getString("LejosUtilsPanel.6")); //$NON-NLS-1$
		firmwareUploadButton.setBounds(185, 0, 180, 80);
		firmwareUploadButton.setImage(FirmwareUploadImage);
		firmwareUploadButton.setToolTipText(bundle.getString("LejosUtilsPanel.7")); //$NON-NLS-1$
		firmwareUploadButton.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent arg0) {		
			}

			public void widgetSelected(SelectionEvent arg0) {
				Shell dialog = new Shell (Display.getDefault());
				dialog.setText (bundle.getString("LejosUtilsPanel.8")); //$NON-NLS-1$
				dialog.setBounds(Display.getDefault().getBounds().width/2-100,Display.getDefault().getBounds().height/2-100,200,200);
				dialog.setLayout(new FillLayout());
				dialog.setImages(Starter.getIcons());
		
				LejosUploadFirmwarePanel lejosUploadFirmwarePanel = new LejosUploadFirmwarePanel(dialog);

				dialog.pack();
				dialog.open ();
			}
		});

		
		Image CompileImage = new Image(getDisplay(), this.getClass().getResourceAsStream("/org/evolvis/freedroidz/gfx/application-x-executable.png")); //$NON-NLS-1$
		Button compileButton = new Button(this, 0);
		compileButton.setText(bundle.getString("LejosUtilsPanel.10")); //$NON-NLS-1$
		compileButton.setBounds(0, 90, 180, 80);
		compileButton.setImage(CompileImage);
		compileButton.setToolTipText("compiles a java-class to a LeJOS-class and uploads it to the NXT if desired"); //$NON-NLS-1$
		compileButton.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent arg0) {		
			}

			public void widgetSelected(SelectionEvent arg0) {
				Shell dialog = new Shell (Display.getDefault());
				dialog.setText (bundle.getString("LejosUtilsPanel.12")); //$NON-NLS-1$
				dialog.setBounds(Display.getDefault().getBounds().width/2-360,Display.getDefault().getBounds().height/2-125,720,250);
				dialog.setLayout(new FillLayout());
				dialog.setImages(Starter.getIcons());
		
				LejosCompileLinkPanel lejosCompileLinkPanel = new LejosCompileLinkPanel(dialog);

				dialog.pack();
				dialog.open ();
			}
		});
		
		
		Image remoteIcon = new Image(getDisplay(), this.getClass().getResourceAsStream("/org/evolvis/freedroidz/gfx/input-gaming.png"));
		Button remoteButton = new Button(this, 0);
		remoteButton.setText(bundle.getString("LejosUtilsPanel.13")); //$NON-NLS-1$
		remoteButton.setBounds(185, 90, 180, 80);
		remoteButton.setImage(remoteIcon);
		
		remoteButton.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent arg0) {		
			}

			public void widgetSelected(SelectionEvent arg0) {
				
				Shell dialog = new Shell (Display.getDefault());
				dialog.setText ("Lejos - Remote control");
				dialog.setBounds(Display.getDefault().getBounds().width/2-180,Display.getDefault().getBounds().height/2-125,360,250);
				dialog.setLayout(new FillLayout());
				dialog.setImages(Starter.getIcons());
		
				if(!System.getProperty("os.arch").equals("arm")){
					RemoteControlPanel remoteControlPanel = new RemoteControlPanel(dialog);
				}else{
					OpenMokoMotioncontrolPanel openMokoMotionControlPanel = new OpenMokoMotioncontrolPanel(dialog);
				}
				if(!dialog.isDisposed())
					dialog.open ();
			}
		});
		
		Image consoleIcon = new Image(getDisplay(), this.getClass().getResourceAsStream("/org/evolvis/freedroidz/gfx/console.png"));
		Button consoleButton = new Button(this, 0);
		consoleButton.setText(bundle.getString("LejosUtilsPanel.14")); //$NON-NLS-1$
		consoleButton.setBounds(0, 180, 180, 80);
		consoleButton.setImage(consoleIcon);
		
		consoleButton.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent arg0) {		
			}

			public void widgetSelected(SelectionEvent arg0) {
				
				Shell dialog = new Shell (Display.getDefault());
				dialog.setText ("Lejos - NXT Console");
				dialog.setBounds(Display.getDefault().getBounds().width/2-180,Display.getDefault().getBounds().height/2-125,600,250);
				dialog.setLayout(new FillLayout());
				dialog.setImages(Starter.getIcons());
		
				RConsolePanel rcp = new RConsolePanel(dialog);
				
				dialog.open();
			}
		});
		
		Image fileManagerIcon = new Image(getDisplay(), this.getClass().getResourceAsStream("/org/evolvis/freedroidz/gfx/file-manager.png"));
		Button fileManagerButton = new Button(this, 0);
		fileManagerButton.setText(bundle.getString("LejosUtilsPanel.15")); //$NON-NLS-1$
		fileManagerButton.setBounds(185, 180, 180, 80);
		fileManagerButton.setImage(fileManagerIcon);
		
		fileManagerButton.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent arg0) {		
			}

			public void widgetSelected(SelectionEvent arg0) {
				
				Shell dialog = new Shell (Display.getDefault());
				dialog.setText ("Lejos - NXT File Manager");
				dialog.setBounds(Display.getDefault().getBounds().width/2-180,Display.getDefault().getBounds().height/2-125,600,250);
				dialog.setLayout(new FillLayout());
				dialog.setImages(Starter.getIcons());
		
				FileManagerPanel fmp = new FileManagerPanel(dialog);
				
				dialog.open();
			}
		});
		
	}
}
