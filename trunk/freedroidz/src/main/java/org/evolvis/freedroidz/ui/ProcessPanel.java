package org.evolvis.freedroidz.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.evolvis.freedroidz.Starter;

public class ProcessPanel {

	private Label label;
	private Shell shell;
	
	public ProcessPanel(final String message) {
		Display.getDefault().asyncExec(new Runnable(){
			public void run(){
				initPanel(message);
			}
		});
		
	}

	private void initPanel(final String message){
		Display display = Display.getDefault();
		shell = new Shell(display, SWT.SYSTEM_MODAL);
		shell.setImages(Starter.getIcons());
		
		label = new Label(shell, SWT.NONE);	
		label.setBounds(0, 5, 200, 50);
		label.setText(message);
		label.setFont(new Font(Display.getDefault(), new FontData(
				"Sans", 21, SWT.BOLD))); //$NON-NLS-1$
		shell.setBounds(display.getBounds().width/2-100, display.getBounds().height/2-25, 200, 50);
		shell.setText(message);
		shell.open();
		
	}
	
	public void stopPanel(){
		Display.getDefault().asyncExec(new Runnable(){
			public void run(){
				shell.dispose();
			}
		});
	}

	public static void main(String[] args) {
		
		ProcessPanel pp = new ProcessPanel("Übertrage...");
		long tmp = System.currentTimeMillis();
		while (true) {
			if(System.currentTimeMillis() - tmp > 2000)
				pp.stopPanel();
			Display.getDefault().readAndDispatch();
		}
	}
	
}
