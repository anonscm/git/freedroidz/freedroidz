/**
 * 
 */
package org.evolvis.freedroidz.lejos;

import java.io.File;
import java.io.OutputStream;
import java.util.Collection;

public interface CompileStrategy {
	public boolean compile(Collection<File> sourceFiles, File destinationDirectory,
			OutputStream output, ProblemCollector collector);

	

}