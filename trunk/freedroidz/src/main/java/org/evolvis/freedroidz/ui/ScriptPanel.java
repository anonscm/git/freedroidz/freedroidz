package org.evolvis.freedroidz.ui;

import java.util.ResourceBundle;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.evolvis.freedroidz.lejos.Utilities;

/**
 * Shows the setup script content
 * @author "T.Wylegala"
 *
 */
public class ScriptPanel {

	String baseName = "org.evolvis.freedroidz.ScriptPanel"; 
    final ResourceBundle bundle = ResourceBundle.getBundle( baseName ); 
	
	private Shell parent;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public ScriptPanel(Shell parent){
		this.parent = parent;
		init();
	}
	
	private void init(){
		final Shell shell = new Shell(parent);
		shell.setLayout(new MigLayout());
		shell.setText(bundle.getString("ScriptPanel.0"));
		
		Label contentLabel = new Label(shell, SWT.BORDER);
		contentLabel.setLayoutData("span, growx, growy");
		
		final String scriptString = Utilities.getScriptCode();
		
		contentLabel.setText(scriptString);
		
		Button copyButton = new Button(shell, SWT.NONE);
		copyButton.setLayoutData("span, growx");
		copyButton.setText(bundle.getString("ScriptPanel.1"));
		
		Button closeButton = new Button(shell, SWT.NONE);
		closeButton.setLayoutData("span, growx");
		closeButton.setText(bundle.getString("ScriptPanel.2"));
		
		shell.pack();
		shell.open();
		
		closeButton.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			public void widgetSelected(SelectionEvent arg0) {
				shell.dispose();
			}
			
		});
		
		copyButton.addSelectionListener(new SelectionListener(){
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				Clipboard clipboard = new Clipboard(Display.getDefault());
				TextTransfer textTransfer = TextTransfer.getInstance();
			    clipboard.setContents(new String[]{scriptString}, new Transfer[]{textTransfer});
			    clipboard.dispose();
			}
			
		});
		
	}
	

}
