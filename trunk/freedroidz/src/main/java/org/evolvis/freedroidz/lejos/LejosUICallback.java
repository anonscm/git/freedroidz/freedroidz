package org.evolvis.freedroidz.lejos;

/**
 * An interface to specify a GUI as a callback. 
 * Every function should call processDone if a
 * callback is registered. Result Codes a supposed
 * to be specified below in this interface.
 * @author "T.Wylegala"
 *
 */
public interface LejosUICallback {
	
	public static final int OPERATION_FAIL = 0;
	public static final int OPERATION_SUCCESS = 1;

	/**
	 * gets called when action is finished
	 * @param status result code specified above
	 */
	public void processDone(int status);
}
