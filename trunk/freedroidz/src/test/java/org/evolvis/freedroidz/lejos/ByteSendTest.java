package org.evolvis.freedroidz.lejos;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import junit.framework.TestCase;
import lejos.pc.comm.NXTComm;
import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;

import org.evolvis.freedroidz.lejos.NXTMock.NXTCommMock;

public class ByteSendTest extends TestCase{

	public void testByteArraySend() throws NXTCommException, IOException{
		NXTComm comm = new NXTCommMock();
		String devString = System.getProperty("lejos.nxt.address");

		NXTInfo nxtInfo = new NXTInfo(NXTCommFactory.BLUETOOTH,"NXT", devString);
		assertTrue(comm.open(nxtInfo));
		
		InputStream in = comm.getInputStream();
		DataInputStream din = new DataInputStream(in);
		OutputStream out = comm.getOutputStream();
		DataOutputStream dout = new DataOutputStream(out);
		
		assertNotNull(in);
		assertNotNull(out);
		
		
		byte[] tmp = new byte[12];
		
		intToByteArray(tmp, 0, 100);
		intToByteArray(tmp, 4, 200);
		intToByteArray(tmp, 8, 300);
		
		dout.write(tmp);
		dout.flush();
		
	}
	
	
	public void intToByteArray(byte[] input, int offset, int value) {
                input[offset] = (byte)(value >>> 24);
                input[offset+1] = (byte)(value >>> 16);
                input[offset+2] = (byte)(value >>> 8);
                input[offset+3] = (byte)value;
	}
}
