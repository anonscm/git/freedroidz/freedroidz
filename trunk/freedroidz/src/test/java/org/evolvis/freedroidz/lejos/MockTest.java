package org.evolvis.freedroidz.lejos;

import java.io.File;

import junit.framework.TestCase;
import lejos.nxt.remote.FileInfo;
import lejos.nxt.remote.NXTCommand;
import lejos.pc.comm.NXTComm;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;

import org.evolvis.freedroidz.lejos.NXTMock.NXTCommMock;
import org.evolvis.freedroidz.lejos.NXTMock.NXTCommandMock;


public class MockTest extends TestCase {

	NXTCommand nxtCommand = new NXTCommandMock();

	
	private UploadFile uploadFile;
	private File testdir;
	private File nxjFile;


	protected void setUp() throws Exception {
		super.setUp();
		this.uploadFile = new UploadFile();

		/* use classloader to locate our testdata */
		this.testdir = new File(this.getClass().getResource(
				"nxtfilemanagementtest").toURI());
		assertTrue(testdir.exists());
		this.nxjFile = new File(testdir, "test.nxj");
		assertTrue(nxjFile.canRead());
//		this.nxtCommand = LejosTestUtils.openConnection(System.getProperty("lejos.nxt.address"), NXTCommFactory.BLUETOOTH);
		
		NXTInfo nxts = new NXTInfo(NXTCommFactory.BLUETOOTH, null, System.getProperty("lejos.nxt.address") );
		
		
		NXTComm nxtComm = new NXTCommMock();
		boolean open = nxtComm.open(nxts, NXTComm.LCP); 
		nxtCommand.setNXTComm(nxtComm);
		
//		nxtCommand.uploadFile(nxjFile);
//		FileInfo fileInfo = nxtCommand.findFirst("test.nxj");
//		assertNotNull(fileInfo);

	}

	protected void tearDown() throws Exception {
		super.tearDown();

		nxtCommand.delete("test.nxj");
		FileInfo fileInfo = nxtCommand.findFirst("test.nxj");
		
		nxtCommand.close();

	}

	public void testUploadViaBlueTooth() throws Throwable {
		
		String message = nxtCommand.uploadFile(nxjFile, nxjFile.getName());
		assertTrue(message.startsWith("Upload successful"));

		FileInfo fileInfo = nxtCommand.findFirst("test.nxj");
		assertNotNull(fileInfo);

	}

}
