package NXTProgram;

import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;
import lejos.nxt.UltrasonicSensor;
import lejos.nxt.addon.ColorSensor;
import lejos.robotics.navigation.Pilot;
import lejos.robotics.navigation.TachoPilot;


//geht alle varianten ab, wenn keine dabei ist, dann wird auch nichts initialisiert und die
//vorherige initialisierung bleibt bestehen

/**
 * initArray Steelle	|	Klasse
 * 		0					Motor
 * 		1					Pilot
 * 		2					Pilot
 * 		3					Pilot
 * 		4					Pilot
 * 		5					LightSensor
 * 		6					LightSensor
 * 		7					UltrasonicSensor
 * 		8					TouchSensor
 * 		9					ColorSensor
 */

public class Initializer {

	static String initString;
	static String[] initArray;
	
	static int arraySize = 10;
	
	public Initializer(String initString){
	
		Initializer.initString = initString;
		parse();
	}
	
	private void parse(){
		
		int counter = 0;
		
//		try{
//			//Counts the commas
//			for(int i=0; i<initString.length(); i++){
//				if(initString.charAt(i) == ',')
//					counter++;
//			}
//		}
//		catch(Exception except){
//			System.out.println("Kommazählen Fehler!");
//		}

		//Sets the String-Array
		Initializer.initArray = new String[arraySize];
		
		for(int i = 0; i < arraySize; i++)
			initArray[i] = "";
		
		//Counter = 0
//		counter = 0;
		
		//Parses the String into a String-Array
		for(int i=0; i<initString.length(); i++){
			if(initString.charAt(i) == ','){
				counter++;
				i++;
			}
			
			initArray[counter] += initString.charAt(i);
		}
		//If the Array at i is "", than it shall be 0
		for(int i=0; i<initArray.length;i++){
			if(initArray[i].equals(""))
				initArray[i] = "0";
		}
	}
	
	/**
	 * Arraynumbers: 0
	 * Sets the Motor.
	 * @param motor
	 * @return new setted motor
	 */
	public static Motor setMotor(Motor motor){
		
		if(initArray[0].equals("A"))
			motor = Motor.A;
		else if(initArray[0].equals("B"))
			motor = Motor.B;
		else if(initArray[0].equals("C"))
			motor = Motor.C;
		else
			motor = Motor.A;
			
		return motor;
	}
	
	/**
	 * Arraynumbers: 1,2,3,4
	 * 
	 * @param pilot
	 * @return
	 */
	public static Pilot setPilot(Pilot pilot){

		float diameter = 46;
		try{
			diameter = (float) new Integer(initArray[1]).intValue();
		}
		catch(Exception except){
			System.out.println("diameter");
		}
		
		float trackWidth = 156;
		try{
			trackWidth = (float) new Integer(initArray[2]).intValue();
		}
		catch(Exception except){
			System.out.println("trackwidth");
		}
		Motor motor1;
		Motor motor2;
		
		//motor1
		if(initArray[3].equals("A"))
			motor1 = Motor.A;
		else if(initArray[3].equals("B"))
			motor1 = Motor.B;
		else if(initArray[3].equals("C"))
			motor1 = Motor.C;
		else
			motor1 = Motor.A;	//default
		
		//motor2
		if(initArray[4].equals("A"))
			motor2 = Motor.A;
		else if(initArray[4].equals("B"))
			motor2 = Motor.B;
		else if(initArray[4].equals("C"))
			motor2 = Motor.C;
		else
			motor2 = Motor.C;	//default
		
		try{
			pilot = new TachoPilot(diameter, trackWidth, motor1, motor2);
		}
		catch(Exception except){
			System.out.println("Instanziierung");
			System.out.println(diameter + trackWidth + motor1.toString() + motor2.toString());
		}
		return pilot;
	}
	
	/**
	 * Arraynumbers: 5,6
	 * 
	 * @param light
	 * @return
	 */
	public static LightSensor setLight(LightSensor light){
		
		SensorPort port;
		
		if(initArray[5].equals("1"))
			port = SensorPort.S1;
		else if(initArray[5].equals("2"))
			port = SensorPort.S2;
		else if(initArray[5].equals("3"))
			port = SensorPort.S3;
		else if(initArray[5].equals("4"))
			port = SensorPort.S4;
		else
			port = SensorPort.S2;	//default
		
		boolean bLight;
		
		if(initArray[6].equals("true"))
			bLight = true;
		else
			bLight = false;	//default
		
		light = new LightSensor(port, bLight);
		
		return light;
	}
	
	/**
	 * Arraynumbers: 7
	 * 
	 * @param sonic
	 * @return
	 */
	public static UltrasonicSensor setSonic(UltrasonicSensor sonic){
		
		SensorPort port;
		
		if(initArray[7].equals("1"))
			port = SensorPort.S1;
		else if(initArray[7].equals("2"))
			port = SensorPort.S2;
		else if(initArray[7].equals("3"))
			port = SensorPort.S3;
		else if(initArray[7].equals("4"))
			port = SensorPort.S4;
		else
			port = SensorPort.S1;	//default
		
		sonic = new UltrasonicSensor(port);
			
		return sonic;
	}
	
	/**
	 * Arraynumbers: 8
	 * 
	 * @param touch
	 * @return
	 */
	public static TouchSensor setTouch(TouchSensor touch){
		
		SensorPort port;
		
		if(initArray[8].equals("1"))
			port = SensorPort.S1;
		else if(initArray[8].equals("2"))
			port = SensorPort.S2;
		else if(initArray[8].equals("3"))
			port = SensorPort.S3;
		else if(initArray[8].equals("4"))
			port = SensorPort.S4;
		else
			port = SensorPort.S3;	//default
		
		touch = new TouchSensor(port);
		
		return touch;
	}
	
	/**
	 * Arraynumber: 9
	 * 
	 * @param color
	 * @return
	 */
	public static ColorSensor setColor(ColorSensor color){
		
		SensorPort port;
		
		if(initArray[9].equals("1"))
			port = SensorPort.S1;
		else if(initArray[9].equals("2"))
			port = SensorPort.S2;
		else if(initArray[9].equals("3"))
			port = SensorPort.S3;
		else if(initArray[9].equals("4"))
			port = SensorPort.S4;
		else
			port = SensorPort.S4;	//default
		
		color = new ColorSensor(port);
		
		return color;
	}
	
	@Override
	public String toString() {
		
		String thisAsString = "";
		
		for(String s: initArray){
			thisAsString += s;
		}
		
		return thisAsString;
	}
}
