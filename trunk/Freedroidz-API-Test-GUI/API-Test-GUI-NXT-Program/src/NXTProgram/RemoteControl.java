package NXTProgram;


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import lejos.nxt.Button;
import lejos.nxt.ButtonListener;
import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;
import lejos.nxt.UltrasonicSensor;
import lejos.nxt.addon.ColorSensor;
import lejos.nxt.comm.BTConnection;
import lejos.nxt.comm.Bluetooth;
import lejos.robotics.navigation.Pilot;
import lejos.robotics.navigation.TachoPilot;
import NXTClasses.TestPilot;
import NXTHardware.TestColorSensor;
import NXTHardware.TestLCD;
import NXTHardware.TestLightSensor;
import NXTHardware.TestMotor;
import NXTHardware.TestTouchSensor;
import NXTHardware.TestUltrasonicSensor;


/**
 * 
 * @author wschol
 *
 *	Dieses Programm soll auf dem NXT laufen und ihn alle Kommandos ausführen lassen, die ihm geschickt werden.
 *	Die Kommandos sollen von der API-Test-GUI geschickt werden.
 */
public class RemoteControl {

	public static boolean run = true;
	/**
	 * @param args
	 * @throws IOException 
	 */
	
	//Bluetooth and DataStreams
	public static BTConnection c;
	public static DataInputStream in;
	public static DataOutputStream out;
	
	public static Initializer initializer;
	
	//NXT-Classes
	public static Pilot pilot;
	public static Motor motor;
	public static LightSensor light;
	public static TouchSensor touch;
	public static UltrasonicSensor sonic;
	public static ColorSensor color;
	
	public static void main(String[] args) throws IOException {
		setUpConnection();	
		initialize();
		remoteControl();
		closeConnection();
	}
		
	public static void setUpConnection() {
		
		LCD.drawString("Warte auf", 0, 1);
		LCD.drawString("Verbindung...", 0, 2);

		c = Bluetooth.waitForConnection();
		in = c.openDataInputStream();
		out = c.openDataOutputStream();
	
		LCD.clear();
		
		LCD.drawString("Verbindung", 0, 1);
		LCD.drawString("hergestellt!", 0, 2);		
		
		Button.ESCAPE.addButtonListener(new ButtonListener(){

			public void buttonPressed(Button arg0) {
				closeConnection();
			}
			public void buttonReleased(Button arg0) {	
			}
			
		});
	}

	private static void initialize() throws IOException {
		
		//default
		pilot = new TachoPilot(46, 156, Motor.A, Motor.C);
		motor = Motor.A;
		sonic = new UltrasonicSensor(SensorPort.S1);
		light = new LightSensor(SensorPort.S2);
		touch = new TouchSensor(SensorPort.S3);
		color = new ColorSensor(SensorPort.S4);

		//params by user
		reinitAll(0);
	}
	
	public static void remoteControl() throws IOException {
		
		int commandLength = 0;	//To get the right length of the command
		byte[] readCommand;	//to get the command in bytes
		String[] commands = new String[]{"","","",""};	//to get the command in a String and a String-Array
		
		while(run){

			LCD.drawString("Warte auf", 0, 3);
			LCD.drawString("Befehle...", 0, 4);
			
			try{
				//Sets null / 0
				commandLength = 0;
				readCommand = null;
				commands = new String[]{"","","",""};
				
				//gets length of command
				commandLength = in.readInt();
				readCommand = new byte[commandLength];
				
				//gets the command as Bytes
				for(int i = 0; in.available()>0; i++){
					
					readCommand[i] = in.readByte();
				}
				
				//converts the Bytes into a String
				commands[0] = new String(readCommand, "utf-8");
			}
			catch(Exception except){
				System.out.println("Beim Empfangen geschah ein Fehler...");
			}
			
			try{
				//parses the String into a String-Array
				commands = parse(commands[0]);
			}
			catch(Exception except){
				System.out.println("Beim Parsen geschah ein Fehler...");
			}
			//Checks if the command is close
			if(commands[0].equals("close")){
//				closeConnection();
				stop();
			}
			
			LCD.clear();
			
			//inquiry
			if(commands[0].equals("reinit"))
				reinitialize(commands[1], commands[2], commands[3], commands[4], commands[5]);
			else if(commands[0].equals("reinitAll"))
				reinitAll(2000);
			if(commands[0].equals("motor"))
				new TestMotor(motor).doCommand(commands[1], new Integer(commands[2]).intValue(), new Integer(commands[3]).intValue());
			else if(commands[0].equals("ultrasonic"))
				new TestUltrasonicSensor(sonic).doCommand();
			else if(commands[0].equals("pilot"))
				new TestPilot(pilot).doCommand(commands[1], new Integer(commands[2]).intValue(), new Integer(commands[3]).intValue());
			else if(commands[0].equals("touchSensor"))
				new TestTouchSensor(touch).doCommand(commands[1]);
			else if(commands[0].equals("lightSensor"))
				new TestLightSensor(light).doCommand(commands[1]);
			else if(commands[0].equals("colorSensor"))
				new TestColorSensor(color).doCommand(commands[1]);
			else if(commands[0].equals("LCD"))
				TestLCD.doCommand(commands[1], commands[2], new Integer(commands[3]), new Integer(commands[4]));
			
			LCD.clear();			
		}
		
		Button.ESCAPE.addButtonListener(new ButtonListener(){

			public void buttonPressed(Button arg0) {
				closeConnection();
			}
			public void buttonReleased(Button arg0) {	
			}
			
		});
	}
	
	private static void reinitAll(int wait) throws IOException{
		
		/**
		 * Reads the InitString from the computer and initializes the NXT the users way.
		 */
		byte[] readInitString;
		String initString;
		int initStringLength = 0;
		
		//gets length of initcommand
		initStringLength = in.readInt();
		readInitString = new byte[initStringLength];
		
		//gets the initcommand as Bytes
		for(int i = 0; in.available()>0; i++){
			
			readInitString[i] = in.readByte();
		}
		
		initString = new String(readInitString, "utf-8");
		
		initializer = new Initializer(initString);	//Should be the standard initialization or the last saved
		
		//user
		pilot = initializer.setPilot(pilot);
		motor = initializer.setMotor(motor);
		sonic = initializer.setSonic(sonic);
		light = initializer.setLight(light);
		touch = initializer.setTouch(touch);
		color = initializer.setColor(color);

		LCD.drawString("Alle Klassen", 0, 0);
		LCD.drawString("wurden neu", 0, 1);
		LCD.drawString("initialisiert.", 0, 2);
		SleepClass.sleep(wait);
	}
	
	private static String[] parse(String s){
		
		int counter = 0;
		String[] stringArray = {"","","","","",""};	//Darf im Extremfall (Worstcase) nicht weniger als 4 Stellen haben, sonst Absturz.
		
		//Parses the String into a String-Array
		for(int i=0; i<s.length(); i++){
			if(s.charAt(i) == ','){
				counter++;
				i++;
			}
			
			stringArray[counter] += s.charAt(i);
		}
		
		if(stringArray[1].equals(""))
			stringArray[1] = "0";
		if(stringArray[2].equals(""))
			stringArray[2] = "0";
		if(stringArray[3].equals(""))
			stringArray[3] = "0";
		if(stringArray[4].equals(""))
			stringArray[4] = "0";
		if(stringArray[5].equals(""))
			stringArray[5] = "0";
		
		return stringArray;
	}
	
	private static void reinitialize(String type, String param1, String param2, String param3, String param4){
		
		if(type.equals("pilot")){
			Motor motor1 = getMotor(param3);
			Motor motor2 = getMotor(param4);
			pilot = new TachoPilot(new Integer(param1).intValue(), new Integer(param2).intValue(), motor1, motor2);
			
			LCD.drawString("Pilot neu", 0, 1);
			LCD.drawString("definiert", 0, 2);
			SleepClass.sleep(2000);
		}
		else if(type.equals("motor")){
			motor = getMotor(param1);
			
			LCD.drawString("Motor neu", 0, 1);
			LCD.drawString("definiert", 0, 2);
			SleepClass.sleep(2000);
		}
		else if(type.equals("lightSensor")){
			SensorPort port = getSensorPort(param1);
			boolean floodlight = getBoolean(param2);
			
			light = new LightSensor(port, floodlight);
			
			LCD.drawString("Lichtsensor neu", 0, 1);
			LCD.drawString("definiert", 0, 2);
			SleepClass.sleep(2000);
		}
		else if(type.equals("ultrasonicSensor")){
			SensorPort port = getSensorPort(param1);
			
			sonic = new UltrasonicSensor(port);
			
			LCD.drawString("Ultraschallsensor", 0, 1);
			LCD.drawString("neu definiert", 0, 2);
			SleepClass.sleep(2000);
		}
		else if(type.equals("touchSensor")){
			SensorPort port = getSensorPort(param1);
			
			touch = new TouchSensor(port);

			LCD.drawString("Touchsensor", 0, 1);
			LCD.drawString("neu definiert", 0, 2);
			SleepClass.sleep(2000);
		}
		else if(type.equals("colorSensor")){
			SensorPort port = getSensorPort(param1);
			
			color = new ColorSensor(port);

			LCD.drawString("Farbsensor", 0, 1);
			LCD.drawString("neu definiert", 0, 2);
			SleepClass.sleep(2000);
		}
	}
	
	private static Motor getMotor(String motor){
		if(motor.equals("motorA"))
			return Motor.A;
		else if(motor.equals("motorB"))
			return Motor.B;
		else
			return Motor.C;
	}
	
	private static SensorPort getSensorPort(String port){
		if(port.equals("SensorPort.S1"))
			return SensorPort.S1;
		else if(port.equals("SensorPort.S2"))
			return SensorPort.S2;
		else if(port.equals("SensorPort.S3"))
			return SensorPort.S3;
		else
			return SensorPort.S4;
	}
	
	private static boolean getBoolean(String bool){
		if(bool.equals("true"))
			return true;
		else
			return false;
	}
	
	public static void stop(){
		run = false;
	}
	
	public static void closeConnection(){
		try {
			in.close();
			out.close();
		} catch (IOException e) {
		}

		try{
			c.close();
		} catch (Exception e){
		}
		
		System.exit(0);
	}
}
