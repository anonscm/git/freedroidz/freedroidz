package NXTClasses;

import lejos.nxt.Button;
import lejos.robotics.navigation.Pilot;
import lejos.robotics.navigation.TachoPilot;

public class TestPilot {

	Pilot pilot;
	
	public TestPilot(Pilot pilot){
		this.pilot = pilot;
	}
	
	public void doCommand(String command, int integer, int integer2){
		
		if(command.equals("forward")){
			forward();
		}
		else if(command.equals("backward")){
			backward();
		}
		else if(command.equals("rotate")){
			rotate(integer);
		}
		else if(command.equals("stop")){
			stop();
		}
		else if(command.equals("travel")){
			travel(integer);
		}
		else if(command.equals("travelArc")){
			travelArc(integer, integer2);
		}
	}

	private void travelArc(int integer, int integer2) {		
		pilot.travelArc(integer, integer2);
	}

	private void travel(int integer) {
		pilot.travel(integer);
	}

	private void stop() {
		
		pilot.forward();
		try{
			Thread.sleep(1500);
		}
		catch(InterruptedException except){}
		
		pilot.stop();
	}

	private void rotate(int degree) {
		pilot.rotate(degree);
	}

	private void backward() {
		
		while(!Button.ENTER.isPressed())
			pilot.backward();	
	}

	private void forward() {
		while(!Button.ENTER.isPressed())
			pilot.forward();	
	}
}
