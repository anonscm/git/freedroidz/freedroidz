package NXTHardware;

import lejos.nxt.Button;
import lejos.nxt.LCD;

public class TestLCD {
	
	public static void doCommand(String command, String text, int X, int Y){
		
		if(command.equals("drawString"))
			drawString(text, X, Y);
		else if(command.equals("drawInt"))
			drawInt(text, X, Y);
	}	
	
	private static void drawString(String text, int X, int Y){
		
		LCD.drawString(text, X, Y);
		
		while(!Button.ENTER.isPressed());		
	}
	
	private static void drawInt(String integer, int X, int Y){
		
		LCD.drawInt(new Integer(integer), X, Y);
		
		while(!Button.ENTER.isPressed());
	}
}
