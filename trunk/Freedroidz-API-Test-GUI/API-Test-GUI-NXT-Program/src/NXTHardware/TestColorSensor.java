package NXTHardware;

import NXTProgram.SleepClass;
import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.addon.ColorSensor;

public class TestColorSensor {

	ColorSensor color;
	
	public TestColorSensor(ColorSensor color){
		this.color = color;
	}
	
	/**
	 * Public
	 */
	
	public void doCommand(String command){
		
		if(command.equals("colorNumber"))
			this.getColorNumber();
		else if(command.equals("blueComponent"))
			this.getBlueComponent();
		else if(command.equals("redComponent"))
			this.getRedComponent();
		else if(command.equals("greenComponent"))
			this.getGreenComponent();
	}
	
	/**
	 * private
	 */
	
	private void getColorNumber(){

		while(!Button.ENTER.isPressed()){
			
			LCD.drawString("Die Farbennummer", 0, 2);
			LCD.drawString("ist:", 0, 3);
			LCD.drawInt(color.getColorNumber(), 0, 4);
			SleepClass.sleep(200);
			LCD.clear();
		}
	}
	
	private void getBlueComponent(){
		
		while(!Button.ENTER.isPressed()){

			LCD.drawString("Der Blauanteil", 0, 2);
			LCD.drawString("ist:", 0, 3);
			LCD.drawInt(color.getBlueComponent(), 0, 4);
			SleepClass.sleep(200);
			LCD.clear();
		}
	}
	
	private void getRedComponent(){
		
		while(!Button.ENTER.isPressed()){

			LCD.drawString("Der Rotanteil", 0, 2);
			LCD.drawString("ist:", 0, 3);
			LCD.drawInt(color.getRedComponent(), 0, 4);
			SleepClass.sleep(200);
			LCD.clear();
		}
	}
	
	private void getGreenComponent(){
		
		while(!Button.ENTER.isPressed()){

			LCD.drawString("Der Gruenanteil", 0, 2);
			LCD.drawString("ist:", 0, 3);
			LCD.drawInt(color.getGreenComponent(), 0, 4);
			SleepClass.sleep(200);
			LCD.clear();
		}
	}
}
