package NXTHardware;

import NXTProgram.SleepClass;
import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.LightSensor;

public class TestLightSensor {

	LightSensor light;
	
	public TestLightSensor(LightSensor light){
		
		this.light = light;
	}
	
	public void doCommand(String command){
		
		if(command.equals("lightValue"))
			getLightValue();
		else if(command.equals("floodlight"))
			setFloodLight();
	}
	
	private void getLightValue(){
		
		while(!Button.ENTER.isPressed()){
			
			LCD.drawString("Der Lichtwert", 0, 2);
			LCD.drawString("betraegt:", 0, 3);
			LCD.drawInt(light.getLightValue(), 0, 4);
			SleepClass.sleep(200);
			LCD.clear();
		}
	}
	
	private void setFloodLight(){
		
		while(!Button.ENTER.isPressed()){
			
			LCD.drawString("Der Lichtwert", 0, 2);
			LCD.drawString("betraegt:", 0, 3);
			LCD.drawInt(light.getLightValue(), 0, 4);
			SleepClass.sleep(200);
			LCD.clear();
		}
		
		if(light.isFloodlightOn())
			light.setFloodlight(false);
		else
			light.setFloodlight(true);
		
		while(Button.ENTER.isPressed());
		
		while(!Button.ENTER.isPressed()){
			
			LCD.drawString("Der Lichtwert", 0, 2);
			LCD.drawString("betraegt:", 0, 3);
			LCD.drawInt(light.getLightValue(), 0, 4);
			SleepClass.sleep(200);
			LCD.clear();
		}
	}
}
