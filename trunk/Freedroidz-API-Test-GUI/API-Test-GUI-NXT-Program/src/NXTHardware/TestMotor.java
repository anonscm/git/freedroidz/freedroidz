package NXTHardware;

import lejos.nxt.Button;
import lejos.nxt.Motor;

public class TestMotor {
	
	Motor motor;
	
	public TestMotor(Motor motor){
		
		this.motor = motor;
	}
	
	/**
	 * Public method. To do the right command.
	 * @param command
	 * @param integer parameter for the command
	 */
	public void doCommand(String command, int param1, int param2){
		
		if(command.equals("forward")){
			forward();
		}
		else if(command.equals("backward")){
			backward();
		}
		else if(command.equals("rotate")){
			rotate(param1);
		}
		else if(command.equals("rotateTo")){
			rotateTo(param1, param2);
		}
		else if(command.equals("stop")){
			stopAndFloat();
		}
	}
	
	/**
	 * Private methods
	 */
	
	private void forward(){
		
		while(!Button.ENTER.isPressed())
			motor.forward();
		
		motor.stop();
	}
	
	private void backward(){
		
		while(!Button.ENTER.isPressed())
			motor.backward();
		
		motor.stop();
	}
	
	private void rotate(int degree){
		
		motor.rotate(degree);
	}
	
	private void rotateTo(int degree1, int degree2){
		try{
			motor.rotateTo(degree1);
			Thread.sleep(500);
			motor.rotateTo(degree2);
		}
		catch(InterruptedException except){
			System.out.println(except.getMessage());
		}
	}
	
	private void stopAndFloat(){
		try{
			motor.forward();
			Thread.sleep(1500);
			motor.stop();
			Thread.sleep(500);
			motor.forward();
			Thread.sleep(1500);
			motor.flt();
			Thread.sleep(1500);
		}
		catch(InterruptedException except){
			System.out.println(except);
		}
	}
}
