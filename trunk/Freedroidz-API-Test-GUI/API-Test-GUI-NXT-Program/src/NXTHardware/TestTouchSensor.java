package NXTHardware;

import NXTProgram.SleepClass;
import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.Motor;
import lejos.nxt.Sound;
import lejos.nxt.TouchSensor;
import lejos.robotics.navigation.Pilot;
import lejos.robotics.navigation.TachoPilot;

public class TestTouchSensor {

	TouchSensor touch;
	
	public TestTouchSensor(TouchSensor ts){
		this.touch = ts;
	}
	
	public void doCommand(String command){
		
		if(command.equals("touch"))
			touchTest();
		else if(command.equals("example"))
			example();
	}
	
	//just for touching the sensor
	private void touchTest(){
				
		LCD.drawString("Bitte druecke", 0, 0);
		LCD.drawString("meinen Touch-", 0, 1);
		LCD.drawString("Sensor =)", 0, 2);
		
		while(!touch.isPressed());
		
		LCD.clear();
		LCD.drawString("Danke", 0, 2);
		
		Sound.beep();
		
		SleepClass.sleep(2000);
		
		LCD.clear();
	}
	
	//explicit example what the touchsensor is for
	private void example(){
		Pilot pilot = new TachoPilot(46, 156, Motor.A, Motor.C);
		
		while(!Button.ENTER.isPressed()){
			pilot.forward();
			
			if(touch.isPressed()){
				pilot.travel(-100);
				pilot.rotate(90);
			}
		}
		
		pilot.stop();
	}
}
