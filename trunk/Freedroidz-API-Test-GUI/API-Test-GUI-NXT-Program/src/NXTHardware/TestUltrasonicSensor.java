package NXTHardware;

import NXTProgram.SleepClass;
import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.UltrasonicSensor;

public class TestUltrasonicSensor {

	UltrasonicSensor sonic;
	
	public TestUltrasonicSensor(UltrasonicSensor sonic){
		
		this.sonic = sonic;
	}

	public void doCommand() {
		
		while(!Button.ENTER.isPressed()){
			
			LCD.drawString("Die Entfernung", 0, 1);
			LCD.drawString("zum naechsten", 0, 2);
			LCD.drawString("Objekt betraegt", 0, 3);
			LCD.drawString(sonic.getDistance() + " cm", 0, 4);
			SleepClass.sleep(200);
			LCD.clear();
		}
	}
}
