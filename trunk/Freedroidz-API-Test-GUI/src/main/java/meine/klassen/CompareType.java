package meine.klassen;

public class CompareType {
	
	public CompareType(){}
	
	public static boolean isInteger(String string){
		
		try{
			int testInteger = new Integer(string).intValue();
			return true;
		}
		catch(Exception except){
			
			return false;
		}
	}
	
	public static boolean isMotor(String string){
		
		if(string.equals("Motor.A") || string.equals("A"))
			return true;
		else if(string.equals("Motor.B") || string.equals("B"))
			return true;
		else if(string.equals("Motor.C") || string.equals("C"))
			return true;
		else
			return false;
	}
	
	public static boolean isSensorPort(String string){
		
		if(string.equals("SensorPort.S1") || string.equals("1") || string.equals("S1"))
			return true;
		else if(string.equals("SensorPort.S2") || string.equals("2") || string.equals("S2"))
			return true;
		else if(string.equals("SensorPort.S3") || string.equals("3") || string.equals("S3"))
			return true;
		else if(string.equals("SensorPort.S4") || string.equals("4") || string.equals("S4"))
			return true;
		else
			return false;
	}
	
	public static boolean isBoolean(String string){
		
		if(string.equals("true") || string.equals("false"))
			return true;
		else
			return false;
	}
}
