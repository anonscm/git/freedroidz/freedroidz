package meine.klassen;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import lejos.nxt.remote.FileInfo;
import lejos.nxt.remote.NXTCommand;
import lejos.pc.comm.NXTComm;
import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;

public class RemoteControl {

	InputStream in;
	DataInputStream din;
	OutputStream out;
	DataOutputStream dout;

	static NXTInfo nxtInfo;
	NXTComm comm;

	public static String nxtAdress = null; // "00:16:53:03:66:B3";
	String nxtName = null;
	
	FileToStringArray ftsa = new FileToStringArray("/NXTAdress.txt");

	public static RemoteControl instance = new RemoteControl(nxtAdress);

	public RemoteControl(){
		this.nxtAdress = ftsa.getStringArray()[0];
		
		try {
			comm = NXTCommFactory.createNXTComm(NXTCommFactory.BLUETOOTH);
		} catch (NXTCommException e) {
			e.printStackTrace();
		}
		nxtInfo = new NXTInfo(NXTCommFactory.BLUETOOTH, "NXT", nxtAdress);
	}
	
	public RemoteControl(String nxtName, String nxtAdress) {
		this.nxtAdress = nxtAdress;
		this.nxtName = nxtName;
	}

	public RemoteControl(String nxtAdress) {
		this.nxtAdress = nxtAdress;
		
		try {
			comm = NXTCommFactory.createNXTComm(NXTCommFactory.BLUETOOTH);
		} catch (NXTCommException e) {
			e.printStackTrace();
		}
		nxtInfo = new NXTInfo(NXTCommFactory.BLUETOOTH, "NXT", nxtAdress);
	}

	public RemoteControl(NXTInfo nxtInfo) {
		this.nxtInfo = nxtInfo;
	}

	public void initConnection() throws NXTCommException, IOException {
		
//		checkRemoteControl(nxtInfo);
		comm = NXTCommFactory.createNXTComm(NXTCommFactory.BLUETOOTH);
		
//		if(!remoteControlExists(new NXTCommand()))
			comm.open(nxtInfo);
			
		in = comm.getInputStream();
		din = new DataInputStream(in);
		out = comm.getOutputStream();
		dout = new DataOutputStream(out);
		
		//default initialize
		String[] commandStringArray = Initializer.readInitStringArray();
				
		//reinits the NXT
		new Initializer(commandStringArray);
	}

	public void closeConnection() throws IOException {
		
		sendCommand("close");
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		din.close();
		dout.close();		
	}
	
	public void sendCommand(String command){
		
		byte[] commandAsBytes = command.getBytes();
		
		try{
			dout.writeInt(commandAsBytes.length);
			dout.write(commandAsBytes);
			dout.flush();
		}
		catch(IOException except){
			System.out.println(except.getMessage());
		}
	}
	
	public boolean connectionExists() throws NXTCommException{
		
		return false;
	}

	/*
	private boolean remoteControlExists(NXTCommand nxtCommand)
			throws NXTCommException {


		int numFiles = 0;
		FileInfo files[] = new FileInfo[30];
		try {
			files[0] = nxtCommand.findFirst("*.*");
			if(files[0].fileName.equals("RemoteControl.nxj"))
				return true;
			
			if (files[0] != null) {
				numFiles = 1;

				for (int i = 1; i < 30; i++) {
					files[i] = nxtCommand.findNext(files[i - 1].fileHandle);
					if (files[i] == null)
						break;
					else {
						numFiles++;
						if(files[i].fileName.equals("RemoteControl.nxj"))
							return true;
					}
				}
			}
		} catch (IOException ioe) {
		}
		return false;
	}
	*/
	
	/*
	private void checkRemoteControl(NXTInfo nxtInfo){
	
		NXTCommand nxtCommand = new NXTCommand();
		NXTCommand nxtCommand2 = new NXTCommand();
		try {
			nxtCommand.open(nxtInfo);
			if(!remoteControlExists(nxtCommand)){
				File f = new File("/tmp/RemoteControl.nxj");
				writeToFile(this.getClass()
				.getResourceAsStream(
						"/org/evolvis/freedroidz/nxtUtilities/RemoteControl.nxj"), f);
				
				SendFile.sendFile(nxtCommand, f);
			}		
			
			
			Thread.sleep(1000);
			nxtCommand.close();
			Thread.sleep(5000);
			nxtCommand2.open(nxtInfo);
			
			nxtCommand2.startProgram("RemoteControl.nxj");

			nxtCommand2.close();
			Thread.sleep(5000);
			
		} catch (NXTCommException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
*/
	/*
	public void writeToFile(InputStream is, File file) {
		try {
			DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
			int c;
			while((c = is.read()) != -1) {
				out.writeByte(c);
			}
			is.close();
			out.close();

		}
		catch(IOException e) {
			System.err.println("Error Writing/Reading Streams.");
		}
	}
	*/

}
