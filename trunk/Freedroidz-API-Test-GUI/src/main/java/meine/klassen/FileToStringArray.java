package meine.klassen;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @author wschol
 *
 */
public class FileToStringArray {

	String userName;
	String[] fileAsStringArray;
	String file;
	private InputStream is;
	
	public FileToStringArray(String file){
		
		this.userName = new Properties(System.getProperties()).getProperty("user.name");
		
//		is = new FileInputStream(file);
		is = getClass().getResourceAsStream(file);
	}
	
	/**
	 * Hier ist ein Fehler drin.
	 * @return
	 */
	public String[] getStringArray(){

		List<String> fileAsStringList = new ArrayList<String>();
		try{
			int counter = 0;
			InputStreamReader inputStreamReader = new InputStreamReader(is);	/**An dieser Stelle, bzw. im Konstruktor!!! */
			BufferedReader br = new BufferedReader(inputStreamReader);
			
			while (br.ready()) {
				fileAsStringList.add(br.readLine());
				counter++;
			}
			
			fileAsStringArray = new String[counter];
			
			for(int i=0; i< fileAsStringArray.length; i++){
				
				fileAsStringArray[i] = fileAsStringList.remove(0);
			}
			
		}catch(IOException ioe){
			ioe.printStackTrace();
		}
		
		return fileAsStringArray;
	}
}
