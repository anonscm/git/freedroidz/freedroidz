package meine.klassen;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import lejos.pc.comm.NXTCommException;

/**
 * initArray Steelle	|	Klasse
 * 		0					Motor
 * 		1					Pilot
 * 		2					Pilot
 * 		3					Pilot
 * 		4					Pilot
 * 		5					LightSensor
 * 		6					LightSensor
 * 		7					UltrasonicSensor
 * 		8					TouchSensor
 * 		9
 */

public class Initializer {

	//Motor
	String motorParam;
	//Pilot
	String motorDiameterParam;
	String motorTrackWidthParam;
	String motor1Param;
	String motor2Param;
	//LightSensor
	String lightPortParam;
	String lightBoolParam;
	//UltrasonicSensor
	String sonicParam;
	//TouchSensor
	String touchParam;
	//ColorSensor
	String colorParam;

	RemoteControl rc = new RemoteControl("00:16:53:08:CB:77");
	
	/**
	 * Gets parameters to reinitialize all classes on the NXT.
	 * @param paramString Contents all parameters for the NXT-Classes.
	 * @throws IOException
	 */
	public Initializer(String[] paramString) throws IOException{
		
		setParams(paramString);
		setDefault();
		sendParams();
	}
	
	/**
	 * Sets the Parameters into separate Strings.
	 * @param paramString
	 */
	private void setParams(String[] paramString){

		motorParam = paramString[0];
		
		motorDiameterParam = paramString[1];
		motorTrackWidthParam = paramString[2];
		motor1Param = paramString[3];
		motor2Param = paramString[4];
		
		lightPortParam = paramString[5];
		lightBoolParam = paramString[6];
		
		sonicParam = paramString[7];
		
		touchParam = paramString[8];
		
		colorParam = paramString[9];
	}
	
	/**
	 * Sets the String to default if they are equal "".
	 */
	private void setDefault(){
		
		if(motorParam.equals(""))
			motorParam = "A";
		
		if(motorDiameterParam.equals(""))
			motorDiameterParam = "46";
		if(motorTrackWidthParam.equals(""))
			motorTrackWidthParam = "156";
		if(motor1Param.equals(""))
			motor1Param = "A";
		if(motor2Param.equals(""))
			motor2Param = "C";
		
		if(lightPortParam.equals(""))
			lightPortParam = "2";
		if(lightBoolParam.equals(""))
			lightBoolParam = "true";
		
		if(sonicParam.equals(""))
			sonicParam = "1";
		
		if(touchParam.equals(""))
			touchParam = "3";
		
		if(colorParam.equals(""))
			colorParam = "4";
		
	}
	
	/**
	 * Sets up a connection to the NXT, sends command to reinitialize all, waits for command and
	 * sends parameters to reinitialize.
	 * @throws IOException
	 */
	private void sendParams() throws IOException{
		
		
		String sendString = motorParam + "," + motorDiameterParam + "," + motorTrackWidthParam + "," + motor1Param + "," +
							motor2Param +	"," + lightPortParam + "," + lightBoolParam + "," + sonicParam + "," + touchParam +
							"," + colorParam;
		
		//Setup connection if it is not connected
		try{
			rc.instance.initConnection();
		}
		catch(NXTCommException except){
			System.out.println(except);
		}
		//Sends command to reinit all
		try{
			rc.instance.sendCommand("reinitAll");
		}
		catch(Exception except){}
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException IE){}
		//sends the initString
		try{
			rc.instance.sendCommand(sendString);
		}
		catch(Exception except){
			//TODO
		}
	}
	
	public static void compareParamsAndReplace(String[] paramStringArray){

		String userName = new Properties(System.getProperties()).getProperty("user.name");
		File file = new File("src/main/resources/initNXTFile.txt");
//		File file = new File("/home/" + userName + "/workspace/Freedroidz-API-Test-GUI/initNXTFile.txt");
		boolean fileExists = file.exists();
		String[] fileAsStringArray;
		
		if(fileExists){
			
			fileAsStringArray = new FileToStringArray("/initNXTFile.txt").getStringArray();
			
			//compare & replace			
			for(int i=0; i<paramStringArray.length; i++){
				if(!fileAsStringArray[i].equals(paramStringArray[i]) && !paramStringArray[i].equals(""))
					fileAsStringArray[i] = paramStringArray[i];
			}
			
			//Save as File
			StringArrayToFile.save(fileAsStringArray, "initNXTFile.txt");
		}
		else{
			
			StringArrayToFile.save(paramStringArray, "initNXTFile.txt");
		}
	}
	
	public static String[] readInitStringArray(){

		String[] initStringArray;
		
		initStringArray = new FileToStringArray("/initNXTFile.txt").getStringArray();
		
		return initStringArray;
	}
}
