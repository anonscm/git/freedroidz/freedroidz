package meine.klassen;

public interface BluetoothCallback {
	public void newDeviceFound(final String Device);
	public void searchFinished();
}
