package meine.klassen;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class StringArrayToFile {
	
	public StringArrayToFile(){}
	/**
	 * Saves a string in a .txt-file.
	 * @param fileAsString the string to be saved
	 * @param fileName the name of the file to be saved
	 */
	public static void save(String[] fileAsStringArray, String fileName) {
		
		String userName = new Properties(System.getProperties()).getProperty("user.name");
		File file = new File("src/main/resources/" + fileName);
		
		try {
		    BufferedWriter writer = new BufferedWriter(new FileWriter(file));

		    for(String s: fileAsStringArray){
		    	writer.write(s);
		    	writer.write("\n");
		    }
		    
		    writer.close();
		 
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
