package meine.klassen;

import java.util.ArrayList;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.RemoteDevice;


public class BluetoothSearch implements BluetoothCallback{

	BluetoothUtils btUtils;
	public ArrayList <RemoteDevice> deviceList = new ArrayList<RemoteDevice>();
	
	public BluetoothSearch() throws BluetoothStateException{

		btUtils = new BluetoothUtils(this);
	}
	
	public void searchDevices() {
		
		btUtils.startDeviceInquiry();
		
		try{
			btUtils.getLatch().await();
		}
		catch(InterruptedException except){
			System.out.println(except.getMessage());
		}
		
		deviceList = btUtils.deviceList;
	}
	
	@Override
	public void newDeviceFound(String Device) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void searchFinished() {
		// TODO Auto-generated method stub
		
	}
}
