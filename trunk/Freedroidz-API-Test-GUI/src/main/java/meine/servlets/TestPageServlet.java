package meine.servlets;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TestPageServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 323242167470320154L;

		
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String name = req.getParameter("username");
		if (name == null) {
			name = "Unbekannter";
		}

		req.setAttribute("name", name);

		req.setAttribute("dateString", new Date().toString());

		getServletContext().getRequestDispatcher("/jsp/testPage.jsp")
				.forward(req, resp);
	}
}
