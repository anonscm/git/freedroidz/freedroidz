package meine.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lejos.pc.comm.NXTCommException;
import meine.klassen.CompareType;
import meine.klassen.RemoteControl;

public class LightSensorServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8502850499649087498L;

	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		RemoteControl rc = new RemoteControl(); //"00:16:53:08:CB:77");
		
		//Initializevariables
		String init = req.getParameter("submitInit");
		String port = req.getParameter("port");
		String floodBool = req.getParameter("floodlight");
		
		//Methodvariables
		String lightValue = req.getParameter("submitLightValue");
		String floodlight = req.getParameter("submitFloodLight");
		
		if(init != null){
			if(CompareType.isSensorPort(port) && CompareType.isBoolean(floodBool)){
				try{
					rc.instance.initConnection();
				}
				catch(NXTCommException except){
					System.out.println(except);
				}
				try{
					rc.instance.sendCommand("reinit,lightSensor," + port + "," + floodBool);
				}
				catch(Exception except){
					//TODO
				}
			}
			else{
				//TODO: Fehler
			}
		}
		else if(lightValue != null){
			try{
				rc.instance.initConnection();
			}
			catch(NXTCommException except){
				System.out.println(except);
			}
			
			try{
				rc.instance.sendCommand("lightSensor,lightValue");
			}
			catch(Exception except){
				//TODO
			}
		}
		else if(floodlight != null){
			try{
				rc.instance.initConnection();
			}
			catch(NXTCommException except){
				System.out.println(except);
			}
			
			try{
				rc.instance.sendCommand("lightSensor,floodlight");
			}
			catch(Exception except){
				//TODO
			}
		}
				
		getServletContext().getRequestDispatcher("/jsp/lightSensorPage.jsp").forward(
				req, resp);
	}
}
