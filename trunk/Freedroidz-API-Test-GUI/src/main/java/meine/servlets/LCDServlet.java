package meine.servlets;

import java.io.IOException;
import java.rmi.Remote;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lejos.pc.comm.NXTCommException;
import meine.klassen.CompareType;
import meine.klassen.RemoteControl;

public class LCDServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -826188485727815464L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		RemoteControl rc = new RemoteControl();

		String defaultString = req.getParameter("drawStringDefault");
		String textString = req.getParameter("textInput");
		String intString = req.getParameter("integerInput");
		String xCoords = req.getParameter("XCoordinate");
		String yCoords = req.getParameter("YCoordinate");
		
		if(defaultString != null){
			try{
				rc.instance.initConnection();
			}
			catch(NXTCommException except){
				System.out.println(except);
			}
			try{
				rc.instance.sendCommand("LCD,drawString,Hallo Welt,0,0");
			}
			catch(Exception except){
				//TODO
			}
		}
		else if(textString != null){
			if(CompareType.isInteger(xCoords) && CompareType.isInteger(yCoords)){
				try{
					rc.instance.initConnection();
				}
				catch(NXTCommException except){
					System.out.println(except);
				}
				try{
					rc.instance.sendCommand("LCD,drawString," + textString + "," + xCoords + "," + yCoords);
				}
				catch(Exception except){
					//TODO
				}
			}
		}
		else if(intString != null){
			if(CompareType.isInteger(intString) && CompareType.isInteger(xCoords) && CompareType.isInteger(yCoords)){
				try{
					rc.instance.initConnection();
				}
				catch(NXTCommException except){
					System.out.println(except);
				}
				try{
					rc.instance.sendCommand("LCD,drawInt," + intString + "," + xCoords + "," + yCoords);
				}
				catch(Exception except){
					//TODO
				}
			}
		}
		
		getServletContext().getRequestDispatcher("/jsp/lcdPage.jsp").forward(
				req, resp);
	}
}
