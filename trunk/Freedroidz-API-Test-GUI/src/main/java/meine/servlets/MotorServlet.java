package meine.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lejos.pc.comm.NXTCommException;
import meine.klassen.CompareType;
import meine.klassen.RemoteControl;

public class MotorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4654299771718844925L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		RemoteControl rc = new RemoteControl(); //"00:16:53:08:CB:77");
		
		//Initializevariables
		String init = req.getParameter("submitInit");
		String motor = req.getParameter("motor");
		
		//Methodvariables
		String degree = req.getParameter("degree");
		String toDegree1 = req.getParameter("toDegree1");
		String toDegree2 = req.getParameter("toDegree2");
		String forward = req.getParameter("submitForward");
		String backward = req.getParameter("submitBackward");
		String stop = req.getParameter("submitStopFloat");
		
		if(init != null){
			if(CompareType.isMotor(motor)){
				try{
					rc.instance.initConnection();
				}
				catch(NXTCommException except){
					System.out.println(except);
				}
				
				try{
					rc.instance.sendCommand("reinit,motor," + motor);
				}
				catch(Exception except){
					req.setAttribute("motorException", "Verbindungsfehler!");
				}
			}
			else{
				if(!CompareType.isMotor(motor) && !motor.equals("")){
					req.setAttribute("motorException", "Falsche Eingabe!");
				}
			}
		}
		else if (degree != null) {

			if(CompareType.isInteger(degree)){
				try{
					rc.instance.initConnection();
				}
				catch(NXTCommException except){
					System.out.println(except);
				}
				
				try{
					rc.instance.sendCommand("motor,rotate," + degree);
				}
				catch(Exception except){
					req.setAttribute("degreeException", "Verbindungsfehler!");
				}
			}
			else{
				req.setAttribute("degreeException", "Falsche Eingabe!");
			}
		}
		else if(toDegree1 != null){
			if(CompareType.isInteger(toDegree1) && CompareType.isInteger(toDegree2)){
				
				try{
					rc.instance.initConnection();
				}
				catch(NXTCommException except){
					System.out.println(except);
				}
				
				try{
					rc.instance.sendCommand("motor,rotateTo," + toDegree1 + "," + toDegree2);
				}
				catch(Exception except){
					req.setAttribute("toDegree1Exception", "Verbindungsfehler!");
				}
			}
			else{
				if(!CompareType.isInteger(toDegree1) && !toDegree1.equals(""))
					req.setAttribute("toDegree1Exception", "Falsche Eingabe!");
				else if(!toDegree1.equals(""))
					req.setAttribute("toDegree1Exception", toDegree1);

				if(!CompareType.isInteger(toDegree2) && !toDegree2.equals(""))
					req.setAttribute("toDegree2Exception", "Falsche Eingabe!");
				else if(!toDegree2.equals(""))
					req.setAttribute("toDegree2Exception", toDegree2);				
			}
		}
		else if (forward != null) {
			
			try{
				rc.instance.initConnection();
			}
			catch(NXTCommException except){
				System.out.println(except);
			}
			
			try{
				rc.instance.sendCommand("motor,forward");
			}
			catch(Exception except){
				req.setAttribute("toDegree1Exception", "Verbindungsfehler!");
			}
		}
		else if(backward != null){
			
			try{
				rc.instance.initConnection();
			}
			catch(NXTCommException except){
				System.out.println(except);
			}
			
			try{
				rc.instance.sendCommand("motor,backward");
			}
			catch(Exception except){
				req.setAttribute("toDegree1Exception", "Verbindungsfehler!");
			}
		}
		else if(stop != null){
			
			try{
				rc.instance.initConnection();
			}
			catch(NXTCommException except){
				System.out.println(except);
			}
			
			try{
				rc.instance.sendCommand("motor,stop");
			}
			catch(Exception except){
				req.setAttribute("toDegree1Exception", "Verbindungsfehler!");
			}
		}
		
		getServletContext().getRequestDispatcher("/jsp/motorPage.jsp").forward(
				req, resp);
	}
}
