package meine.servlets;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HalloWeltServlet extends HttpServlet {
	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String programSource = req.getParameter("program");
		
		if(programSource!=null){
			System.out.println("Wenn ich nicht nur ein Beispiel wäre, dann würde ich jetzt folgendes kompilieren und auf den Brick laden:\n");
			System.out.println(programSource);			
		}
		req.setAttribute("program", programSource);
		req.setAttribute("dateString", new Date().toString());
		
		getServletContext().getRequestDispatcher("/jsp/helloworld.jsp")
				.forward(req, resp);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		req.setAttribute("dateString", new Date().toString());
		
		getServletContext().getRequestDispatcher("/jsp/helloworld.jsp")
				.forward(req, resp);
	}
}
