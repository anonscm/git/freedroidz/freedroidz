package meine.servlets;

import java.io.IOException;

import javax.bluetooth.RemoteDevice;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import meine.klassen.BluetoothSearch;
import meine.klassen.CompareType;
import meine.klassen.FileToStringArray;
import meine.klassen.Initializer;
import meine.klassen.RemoteControl;
import meine.klassen.StringArrayToFile;

public class InitializerServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6873636241804374177L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		String initializerUrl = "/jsp/initializerPage.jsp";
		
		RemoteControl rc = new RemoteControl(); //"00:16:53:08:CB:77");
		Initializer init;
		
		String motorParam = "";
		String motorDiameterParam = "";
		String motorTrackWidthParam = "";
		String motor1Param = "";
		String motor2Param = "";
		String lightPortParam = "";
		String lightBoolParam = "";
		String sonicParam = "";
		String touchParam = "";
		String colorParam = "";
		
		String submitButton = req.getParameter("submitButton");
		motorParam += req.getParameter("motorParam");
		motorDiameterParam += req.getParameter("motorDiameterParam");
		motorTrackWidthParam += req.getParameter("motorTrackWidthParam");
		motor1Param += req.getParameter("motor1Param");
		motor2Param += req.getParameter("motor2Param");
		lightPortParam += req.getParameter("lightPortParam");
		lightBoolParam += req.getParameter("lightBoolParam");
		sonicParam += req.getParameter("sonicParam");
		touchParam += req.getParameter("touchParam");
		colorParam += req.getParameter("colorPortParam");
		String suchen = req.getParameter("suchenSubmit");
		String deviceAdress = req.getParameter("drop1");
		String macSubmit = req.getParameter("macSubmit");
		
		String[] initString = new FileToStringArray("/initNXTFile.txt").getStringArray();
		
		//writes the saved parameter in the text-input
		req.setAttribute("motorException", initString[0]);		
		req.setAttribute("motor1Exception", initString[3]);		
		req.setAttribute("motor2Exception", initString[4]);		
		req.setAttribute("motorTrackWidthException", initString[2]);		
		req.setAttribute("motorDiameterException", initString[1]);		
		req.setAttribute("lightPortException", initString[5]);		
		req.setAttribute("sonicException", initString[7]);		
		req.setAttribute("touchException", initString[8]);		
		req.setAttribute("lightBoolException", initString[6]);		
		req.setAttribute("colorException", initString[9]);		
		
		if(suchen != null){
		
			String foundAdresses = "";
			
			BluetoothSearch btSearch = new BluetoothSearch();
			
			btSearch.searchDevices();
			
			for(RemoteDevice rm: btSearch.deviceList){
				foundAdresses += "<option value=\"" + rm.toString() + "\">" + rm.toString() + "</option> \n";
			}
			
			if(btSearch.deviceList.isEmpty())
				foundAdresses += "<option value=\"Kein Gerät gefunden\">Kein Gerät gefunden</option> \n";
			
			req.setAttribute("foundAdresses", foundAdresses);
		}
		else if(macSubmit != null && !deviceAdress.equals("Kein Gerät gefunden")){
			
			if(deviceAdress != null){
				
				String[] macAdress = {""};
				
				for(int i=0; i<deviceAdress.length(); i++){

					if(i % 2 == 0 && i != 0){
						
						macAdress[0] += ":";
					}
					
					macAdress[0] += deviceAdress.charAt(i);					
				}
				
				System.out.println(macAdress);	//TODO: in datei speichern
				
				StringArrayToFile.save(macAdress, "NXTAdress.txt");
			}
		}
		else if(submitButton != null){
			
			if((CompareType.isMotor(motorParam) || motorParam.equals("")) && (CompareType.isMotor(motor1Param) || motor1Param.equals("")) &&
					(CompareType.isMotor(motor2Param) || motor2Param.equals(""))&& (CompareType.isInteger(motorTrackWidthParam) || motorTrackWidthParam.equals("")) &&
					(CompareType.isInteger(motorDiameterParam) || motorDiameterParam.equals("")) && (CompareType.isSensorPort(lightPortParam) || lightPortParam.equals("")) &&
					(CompareType.isSensorPort(sonicParam) || sonicParam.equals("")) && (CompareType.isSensorPort(touchParam) || touchParam.equals("")) &&
					(CompareType.isBoolean(lightBoolParam) || lightBoolParam.equals("")) && (CompareType.isSensorPort(colorParam) || colorParam.equals(""))){
		
				//All inputs as a Stringarray
				String[] initStringArray = {motorParam, motorDiameterParam, motorTrackWidthParam, motor1Param, motor2Param, lightPortParam,
						lightBoolParam, sonicParam, touchParam, colorParam};
				
				//Compares old initialization with new
				Initializer.compareParamsAndReplace(initStringArray);
				//returns an Array of a string
				initStringArray = Initializer.readInitStringArray();
				
				//reinits the NXT
				init = new Initializer(initStringArray);
			}
			else{	//replaces old values or "throws exception"

				if(!CompareType.isMotor(motorParam) && !motorParam.equals(""))
					req.setAttribute("motorException", "Falsche Eingabe!");
				else if(!motorParam.equals(""))
					req.setAttribute("motorException", motorParam);
				
				if(!CompareType.isMotor(motor1Param) && !motor1Param.equals(""))
					req.setAttribute("motor1Exception", "Falsche Eingabe!");
				else if(!motor1Param.equals(""))
					req.setAttribute("motor1Exception", motor1Param);
				
				if(!CompareType.isMotor(motor2Param) && !motor2Param.equals(""))
					req.setAttribute("motor2Exception", "Falsche Eingabe!");
				else if(!motor2Param.equals(""))
					req.setAttribute("motor2Exception", motor2Param);
				
				if(!CompareType.isInteger(motorTrackWidthParam) && !motorTrackWidthParam.equals(""))
					req.setAttribute("motorTrackWidthException", "Falsche Eingabe!");
				else if(!motorTrackWidthParam.equals(""))
					req.setAttribute("motorTrackWidthException", motorTrackWidthParam);
				
				if(!CompareType.isInteger(motorDiameterParam) && !motorDiameterParam.equals(""))
					req.setAttribute("motorDiameterException", "Falsche Eingabe!");
				else if(!motorDiameterParam.equals(""))
					req.setAttribute("motorDiameterException", motorDiameterParam);
				
				if(!CompareType.isSensorPort(lightPortParam) && !lightPortParam.equals(""))
					req.setAttribute("lightPortException", "Falsche Eingabe!");
				else if(!lightPortParam.equals(""))
					req.setAttribute("lightPortException", lightPortParam);
				
				if(!CompareType.isSensorPort(sonicParam) && !sonicParam.equals(""))
					req.setAttribute("sonicException", "Falsche Eingabe!");
				else if(!sonicParam.equals(""))
					req.setAttribute("sonicException", sonicParam);
				
				if(!CompareType.isSensorPort(touchParam) && !touchParam.equals(""))
					req.setAttribute("touchException", "Falsche Eingabe!");
				else if(!touchParam.equals(""))
					req.setAttribute("touchException", touchParam);
				
				if(!CompareType.isBoolean(lightBoolParam) && !lightBoolParam.equals(""))
					req.setAttribute("lightBoolException", "Falsche Eingabe!");
				else if(!lightBoolParam.equals(""))
					req.setAttribute("lightBoolException", lightBoolParam);
				
				if(!CompareType.isSensorPort(colorParam) && !colorParam.equals(""))
					req.setAttribute("colorException", "Falsche Eingabe!");
				else if(!colorParam.equals(""))
					req.setAttribute("colorException", colorParam);
			}
		}
		
			getServletContext().getRequestDispatcher(initializerUrl).forward(
				req, resp);
	}

}
