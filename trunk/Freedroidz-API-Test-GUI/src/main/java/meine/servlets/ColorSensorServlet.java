package meine.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lejos.pc.comm.NXTCommException;
import meine.klassen.CompareType;
import meine.klassen.RemoteControl;

public class ColorSensorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1275942875991868372L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		RemoteControl rc = new RemoteControl(); //"00:16:53:08:CB:77");
		
		//Initializevariables
		String init = req.getParameter("submitInit");
		String port = req.getParameter("port");

		//Methodveriables
		String colorNumber = req.getParameter("colorNumberSubmit");
		String blueComp = req.getParameter("blueComponentSubmit");
		String redComp = req.getParameter("redComponentSubmit");
		String greenComp = req.getParameter("greenComponentSubmit");
		
		
		if(init != null){
			if(CompareType.isSensorPort(port)){
				try{
					rc.instance.initConnection();
				}
				catch(NXTCommException except){
					System.out.println(except);
				}
				try{
					rc.instance.sendCommand("reinit,colorSensor," + port);
				}
				catch(Exception except){
					//TODO
				}
			}
			else{
				//TODO: Fehler
			}
		}
		else if(colorNumber != null){
			try{
				rc.instance.initConnection();
			}
			catch(NXTCommException except){
				System.out.println(except);
			}
			try{
				rc.instance.sendCommand("colorSensor,colorNumber");
			}
			catch(Exception except){
				//TODO
			}
		}
		else if(blueComp != null){
			try{
				rc.instance.initConnection();
			}
			catch(NXTCommException except){
				System.out.println(except);
			}
			try{
				rc.instance.sendCommand("colorSensor,blueComponent");
			}
			catch(Exception except){
				//TODO
			}
		}
		else if(redComp != null){
			try{
				rc.instance.initConnection();
			}
			catch(NXTCommException except){
				System.out.println(except);
			}
			try{
				rc.instance.sendCommand("colorSensor,redComponent");
			}
			catch(Exception except){
				//TODO
			}
		}
		else if(greenComp != null){
			try{
				rc.instance.initConnection();
			}
			catch(NXTCommException except){
				System.out.println(except);
			}
			try{
				rc.instance.sendCommand("colorSensor,greenComponent");
			}
			catch(Exception except){
				//TODO
			}
		}

		getServletContext().getRequestDispatcher("/jsp/colorSensorPage.jsp").forward(
				req, resp);
	}

}
