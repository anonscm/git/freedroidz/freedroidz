package meine.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lejos.pc.comm.NXTCommException;
import meine.klassen.CompareType;
import meine.klassen.RemoteControl;

public class TouchSensorServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2323444273104451913L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		RemoteControl rc = new RemoteControl(); //"00:16:53:08:CB:77");
		
		//Initializevariables
		String init = req.getParameter("submitInit");
		String port = req.getParameter("port");
		
		//Methodvariables
		String test = req.getParameter("submitTouchTest");
		String example = req.getParameter("submitExample");
		
		if(init != null){
			if(CompareType.isSensorPort(port)){
				try{
					rc.instance.initConnection();
				}
				catch(NXTCommException except){
					System.out.println(except);
				}
				
				try{
					rc.instance.sendCommand("reinit,touchSensor," + port);
				}
				catch(Exception except){
					//TODO
				}
			}
			else{
				//TODO: Fehler
			}
		}
		else if(test != null){
			try{
				rc.instance.initConnection();
			}
			catch(NXTCommException except){
				System.out.println(except);
			}
			
			try{
				rc.instance.sendCommand("touchSensor,touch");
			}
			catch(Exception except){
				//TODO
			}
		}
		else if(example != null){
			try{
				rc.instance.initConnection();
			}
			catch(NXTCommException except){
				System.out.println(except);
			}
			
			try{
				rc.instance.sendCommand("touchSensor,example");
			}
			catch(Exception except){
				//TODO
			}
		}
				
		getServletContext().getRequestDispatcher("/jsp/touchSensorPage.jsp").forward(
				req, resp);
	}
	

}
