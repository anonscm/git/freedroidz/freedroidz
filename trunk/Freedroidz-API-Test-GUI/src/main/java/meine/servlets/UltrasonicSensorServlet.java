package meine.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lejos.pc.comm.NXTCommException;
import meine.klassen.CompareType;
import meine.klassen.RemoteControl;

public class UltrasonicSensorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5349983077037294526L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		RemoteControl rc = new RemoteControl(); //"00:16:53:08:CB:77");
		
		//Initializevariables
		String init = req.getParameter("submitInit");
		String port = req.getParameter("port");
		
		//Methodvariables
		String getDistance = req.getParameter("submitGetDistance");
		
		if(init != null){
			if(CompareType.isSensorPort(port)){
				try{
					rc.instance.initConnection();
				}
				catch(NXTCommException except){
					System.out.println(except);
				}
				
				try{
					rc.instance.sendCommand("reinit,ultrasonicSensor," + port);
				}
				catch(Exception except){
					//TODO
				}
			}
			else{
				req.setAttribute("portException", "Falsche Eingabe!");
			}
		}
		else if(getDistance != null){
			
			try{
				rc.instance.initConnection();
			}
			catch(NXTCommException except){
				System.out.println(except);
			}
			
			try{
				rc.instance.sendCommand("ultrasonic");
			}
			catch(Exception except){
				//TODO
			}
		}
		
		getServletContext().getRequestDispatcher("/jsp/ultrasonicSensorPage.jsp").forward(
				req, resp);
	}

}