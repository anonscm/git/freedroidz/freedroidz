package meine.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lejos.pc.comm.NXTCommException;
import meine.klassen.CompareType;
import meine.klassen.RemoteControl;

public class PilotServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6281707562834749520L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		

		RemoteControl rc = new RemoteControl(); //"00:16:53:08:CB:77");
		
		//Initilizevariables
		String init = req.getParameter("submitInit");
		String diameter = req.getParameter("diameter");
		String trackWidth = req.getParameter("trackWidth");
		String motor1 = req.getParameter("motor1");
		String motor2 = req.getParameter("motor2");
		
		//Methodvariables
		String distance = req.getParameter("distance");
		String forward = req.getParameter("submitForward");
		String backward = req.getParameter("submitBackward");
		String degree = req.getParameter("degree");
		String radius = req.getParameter("radius");
		String distanceArc = req.getParameter("distanceArc");
		String stop = req.getParameter("submitStop");
		
		if(init != null){
			if(CompareType.isInteger(diameter) && CompareType.isInteger(trackWidth) && CompareType.isMotor(motor1) && CompareType.isMotor(motor2)){
				try{
					rc.instance.initConnection();
				}
				catch(NXTCommException except){
					System.out.println(except);
				}
				
				try{
					rc.instance.sendCommand("reinit,pilot," + diameter + "," + trackWidth +"," + motor1 + "," + motor2);
				}
				catch(Exception except){
					//TODO
				}
			}
			else{
				//TODO: Fehler
			}
		}
		else if(forward != null){
			try{
				rc.instance.initConnection();
			}
			catch(NXTCommException except){
				System.out.println(except);
			}
			
			try{
				rc.instance.sendCommand("pilot,forward");
			}
			catch(Exception except){
				//TODO
			}
		}
		else if(backward != null){
			try{
				rc.instance.initConnection();
			}
			catch(NXTCommException except){
				System.out.println(except);
			}
			
			try{
				rc.instance.sendCommand("pilot,backward");
			}
			catch(Exception except){
				//TODO
			}
		}
		else if(degree != null){
			
			if(CompareType.isInteger(degree)){
				try{
					rc.instance.initConnection();
				}
				catch(NXTCommException except){
					System.out.println(except);
				}
				
				try{
					rc.instance.sendCommand("pilot,rotate," + degree);
				}
				catch(Exception except){
					//TODO
				}
			}
			else{
				//TODO: Fehler
			}
		}
		else if(radius != null){
			if(CompareType.isInteger(radius) && CompareType.isInteger(distanceArc)){
				
				try{
					rc.instance.initConnection();
				}
				catch(NXTCommException except){
					System.out.println(except);
				}
				
				try{
					rc.instance.sendCommand("pilot,travelArc," + radius + "," + distanceArc);
				}
				catch(Exception except){
					//TODO
				}
			}
			else{
				//TODO: Fehler
			}
		}
		else if (distance != null) {
			
			if(CompareType.isInteger(distance)){
				try{
					rc.instance.initConnection();
				}
				catch(NXTCommException except){
					System.out.println(except);
				}
				
				try{
					rc.instance.sendCommand("pilot,travel," + distance);
				}
				catch(Exception except){
					//TODO
				}
			}
			else{
				//TODO: Fehler aussprucken
			}
		}
		else if(stop != null){
			
			try{
				rc.instance.initConnection();
			}
			catch(NXTCommException except){
				System.out.println(except);
			}
			
			try{
				rc.instance.sendCommand("pilot,stop");
			}
			catch(Exception except){
				//TODO
			}
		}
		
		getServletContext().getRequestDispatcher("/jsp/pilotPage.jsp")
		.forward(req, resp);
	}
}
