<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>


<%@ include file="inc/header.jsp" %>

	<h1>TouchSensor / Ber&uuml;hrungssensor</h1>
	
	<p>
		<table border="0" width="100%">
			<tr>
				<td style="vertical-align:top; width:50%">
					
					Der TouchSensor bestitzt nur einen Knopf, den man dr&uuml;cken kann. Man kann ihn abfragen,
					um bestimmte Ereignisse auszul&ouml;sen. Beispielsweise, um einer Wand auszuweichen, auf die
					der Nxt zuf&auml;hrt.
					Nat&uuml;rlich gibt es noch jede Menge anderer Sachen, f&uuml;r die man den TouchSensor
					benutzen kann.
				
				</td>
				<td style="vertical-align:right; width:50%">
					<img width="35%" src="images/touchSensor.png" alt="TouchSensor.jpg" align=right />
				</td>			
			</tr>
		</table>
	</p>
	
	
	<h2>Erstellen eines TouchSensors / Deklaration und Initialisierung</h2>
	
	Der TouchSensor ist eine eigene Klasse. Wir sagen dem Roboter, dass wir einen TouchSensor haben m&ouml;chten,<br />
	geben diesem Sensor einen Namen, z.B. touch, und m&uuml;ssen dem Roboter nurnoch sagen, an welchem Sensorport sich<br />
	der Sensor befindet.<br /><br />
	Das kann in Java dann so aussehen:
	
	<pre>
		TouchSensor touch = new TouchSensor(SensorPort.S3);
	</pre>
	
	Wir haben unseren TouchSensor nun an Port 3 angeschlossen.
	
		<form name="initForm">
			<h3>Eigene Initialisierung</h3>
			
			Hier kannst du deinen eigenen TouchSensor initialisieren.
			
			<pre>
				TouchSensor touch = new TouchSensor(<input type="text" name="port" />);		Floodlight im Format true oder false.
					
				<input type="submit" name="submitInit" value="TouchSensor neu definieren." />
			</pre>
		</form>
		
	<br />
	<br />
	<br />
	
	<form name="testForm">
		<h2>Den TouchSensor abfragen</h2>
		
		Der TouchSensor besitzt eine boolsche Variable. D.h., dass er einen Wahrheitswert besitzt, also<br />
		ja oder nein. Dieser Wert gibt wieder, ob der Sensor gedr&uuml;ckt ist oder nicht. Standardm&auml;ßig<br />
		ist er nat&uuml;rlich nicht gedr&uuml;ckt, also besitzt der Wahrheitswert den Wert nein (false).<br />
		Soll der Sensor den Wert ja (true) besitzen, m&uuml;ssen wir den Sensor nur dr&uuml;cken.<br />
		In unserem Beispielprogramm wird der NXT solange warten, bis Du den TouchSensor gedr&uuml;ckt hast.<br /><br />
		In java fragt man den Sensor so ab:
		
		<pre>
			touch.isPressed();		<input type="submit" name="submitTouchTest" value="Beispiel hochladen." />
		</pre>
	</form>
	
	<form name="exampleForm">
		<h2>Ein Beispielprogramm</h2>
		
		Hier wollen wir mal ein Beispielprogramm hochladen, um zu zeigen, wof&uuml;r man den TouchSensor z.B.<br />
		verwenden kann.<br />
		Der NXT wird solange geradeaus fahren, bis er gegen eine Wand f&auml;hrt. Der Sensor wird dort gedr&uuml;ckt<br />
		und der NXT dreht sich und f&auml;hrt weiter.<br />
		Ihr solltet also darauf achten, dass der Sensor nach vorne zeigt. Außerdem sollten die Motoren an den Ports A<br />
		und C angeschlossen sein.
		
		<pre>
			<input type="submit" name="submitExample" value="Beispiel hochladen." />
		</pre>
	</form>
	

<%@ include file="inc/footer.jsp" %>