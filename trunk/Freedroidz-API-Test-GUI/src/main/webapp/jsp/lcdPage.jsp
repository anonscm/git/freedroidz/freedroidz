<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>


<%@ include file="inc/header.jsp" %>

		<h1>Das LCD-Display</h1>
		
		<p>
			<table border="0" width="100%">
				<tr>
					<td style="vertical-align:top; width:50%">
						
						Das LCD-Display ist eine Klasse, die benutzt wird, um Text schöner darstellen
						zu können.
						Man kann genau entscheiden, in welcher Zeile und welcher Spalte man anfangen
						möchte zu schreiben.
						Man wählt diese Zeilen und Spalten mit X- und Y-Koordinaten an, wobei der
						Koordinatenursprung oben links ist.
						Das Display ist 16 Buchstaben breit und 8 Buchstaben hoch.
						Da der Computer aber die 0 mitzählt, müssen wir immer eine Zeile oder Spalte
						abziehen, um auf unsere gewünschte Position zu kommen.
						
						Noch anzumerken ist, dass die LCD-Klasse eine statische Klasse ist.
						D.h., man kann zwar eine Instanz von ihr festlegen, man sollte es aber
						vermeiden.
						Somit können wir das Display direkt ansteuern, ohne uns Gedanken darüber zu machen,
						wie unser Display heißen soll oder wo es angeschlossen ist, etc.
						
					</td>
					<td style="vertical-align:right; width:50%">
						<img width="35%" src="images/LCDDisplay.png" alt="LCDDisplay.png" align=right />
					</td>			
				</tr>
			</table>
		</p>
		
		
		
		<h2>Einen Text schreiben / drawString</h2>
			
		Wie schon gesagt, ist unser Display nur 16 Buchstaben breit und 8 hoch. Also müssen<br />
		wir darauf achten, dass wir nur einen Text schicken, der auch genau so lang ist.<br />
		Wollen wir einen Text schreiben, der länger ist als 16 Buchstaben, so müssen wir<br />
		einen neuen Befehl senden.<br />
		Wie schon gesagt, ist die LCD-Klasse statisch und kann sofort angewählt werden.<br />
		In Java sieht das dann so aus:
			
			
		<form name="drawStringForm">
			
			<pre>
				LCD.drawString("Hallo Welt", 0, 0);	<input type="submit" name="drawStringDefault" value="Beispiel hochladen." />
			</pre>
			
			
		</form>
		
		Auf dem Display werden nun in der ersten Zeile die beiden Wörter "Hallo Welt" erscheinen.<br />
		
		Um nun Deinen eigenen Text schicken zu können, kannst du hier einen Text und die<br />
		dazugehörigen Koordinaten eingeben.
		
		<form name="customDrawStringForm">
			<pre>
				LCD.drawString(	<input type="text" name="textInput" />,
						<input type="text" name="XCoordinate" />,
						<input type="text" name="YCoordinate" />);
								
								<input type="submit" name="submitCustomDrawString" value="Beispiel hochladen." />
			</pre>
		</form>
		
		
		Die Methode "drawString" übernimmt allerdings nur Strings. Will man also einen Integer auf dem
		Display ausgeben lassen, so muss man die Methode "drawInt" benutzen. Sie funktioniert ansonsten
		genauso, wie die "drawString"-Methode.
		
		<form name="drawIntForm">
			<pre>
				LCD.drawInt(<input type="text" name="integerInput" />,
					    <input type="text" name="XCoordinate" />,
					    <input type="text" name="YCoordinate" />);
						
								<input type="submit" name="submitDrawInt" value="Beispiel hochladen." />
			</pre>
		</form>
		
		
		<h2>Bemerkung</h2>
		
		Man kann natürlich auch
		
			<pre>
				System.out.println("Text");
			</pre>
			
		benutzen. Aber was man schreibt, erscheint immer ganz unten und man kann mit "System" den Bildschirm<br />
		nicht säubern.

<%@ include file="inc/footer.jsp" %>