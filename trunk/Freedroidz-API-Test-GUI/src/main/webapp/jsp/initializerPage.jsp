<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>


<%@ include file="inc/header.jsp" %>

	<h1>Eigenen NXT definieren / initialisieren</h1>
		
		<p>
			<table border="0" width="100%">
				<tr>
					<td style="vertical-align:top; width:50%">
						
						Diese Seite ist daf&uuml;r da, damit Du deinen selbst gebauten NXT<br />
						definieren kannst. Es ist sehr wahrscheinlich, dass du deine Motoren<br />
						und Sensoren an anderen Ports angeschlossen hast und unsere Beispiele<br />
						deshalb leider nicht mit deinem NXT kompatibel sind.<br />
						&Auml;ndere hier die Parameter, wie in den einzelnen Artikeln beschrieben,<br />
						damit die Beispielprogramme auf deinem NXT funktionieren.<br /><br />
						
						Du musst nicht alles ausf&uuml;llen. Es reicht, wenn du nur eine Klasse<br />
						definierst. Dies kannst du auch in jedem Artikel speziel für diese<br />
						Klasse machen.<br />
						Es w&auml;re aber ratsam einmal jede Klasse genau auf deinen NXT abzustimmen.<br />
						Wenn die Standardeinstellungen mit deinem NXT &uuml;bereinstimmen, musst du nichts<br />
						eintragen.<br /><br />
						
						Zum &uuml;bertragen der Daten bitte den Button ganz unten dr&uuml;cken.

					</td>
					<td style="vertical-align:right; width:50%">
						<img width="100%" src="images/NXTTeile.png" alt="NXTTeile.png" align=right />
					</td>			
				</tr>
			</table>
		</p>
		
		<!-- Auswahl der MAC-Adresse adden. -->
		
		<form name="macForm">
			<h2>W&auml;hle deinen NXT aus</h2>
			
			Um erfolgreich Programme auf deinen NXT laden zu k&ouml;nnen, musst du ihn erst einmal<br />
			ausw&auml;hlen. Das machst du, indem du den NXT suchen l&auml;sst und dann die richtige<br />
			MAC-Adresse ausw&auml;hlst.<br /><br />
			
			Hier kannst du die MAC-Adresse aktiver Bluetoothger&auml;te suchen.<br />
			W&auml;hle dann die richtige aus und dr&uuml;cke dann auf "best&auml;tigen".
			
			<pre>
				<table>
					<tr>
						<td>
							<select name="drop1" id="Select1" size="4" multiple="multiple">
	
								${foundAdresses}
							
							</select>
						</td>
						<td>
							<input type="submit" value="Best&auml;tigen" name="macSubmit" />
						</td>
					</tr>
					<tr>
						<td>
							<input type="submit" value="Suchen" name="suchenSubmit" />
						</td>
					</tr>
				</table>
		</pre>
			
		</form>
		
		<!-- Überlegen, ob überall Buttons hin sollen. Wenns überschaulich ist, dann reicht ein Button. -->
		
		<form name="initAllForm">
		
		<h2>Motor</h2>
		
		Stelle hier deinen Motor fest. In allen Beispielen des Motors wird nur ein Motor benutzt.<br />
		Als Standard ist der Motor an Port A eingestellt. Trage den Buchstaben des Ports ein, an dem<br />
		dein Motor angeschlossen ist. Achte darauf, dass du den Buchstaben groß schreibst.
		
		<pre>
			Motor motor = Motor. <input type="text" value="${motorException}" name="motorParam" /> ;		
		</pre>
		
		<br /><br /><br />
		
		<h2>UltrasonicSensor</h2>
		
		Standardm&auml;ßig ist der Ultraschallsensor an Port 1 eingestellt. &Auml;ndere den Port, indem Du<br />
		die Zahl des Ports eintr&auml;gst.
		
		<pre>
			UltrasonicSensor sonic = new UltrasonicSensor(SensorPort.S<input type="text" value="${sonicException}" name="sonicParam" />);
		</pre>
		
		<br /><br /><br />
		
		<h2>TouchSensor</h2>
		
		Der TouchSensor ist standardm&auml;ßig an Port 3 definiert. Trage einfach die Portnummer ein,<br />
		in der Dein Sensor steckt.
		
		<pre>
			TouchSensor touch = new TouchSensor(SensorPort.S<input type="text" name="touchParam" value="${touchException}" />);
		</pre>
		
		<br /><br /><br />
		
		<h2>LightSensor</h2> S2
		
		Dem Lichtsensor ist standardm&auml;ßig der Port 2 zugewiesen und das Licht ist an.<br />
		Trage im ersten Feld die Zahl des Ports ein, an dem Dein Lichtsensor eingesteckt ist<br />
		und in das zweite Feld tr&auml;gst Du true ein, wenn das Licht an sein soll oder false,<br />
		wenn das Licht aus sein soll.
		
		<pre>
			LightSensor light = new LightSensor(SensorPort.S<input type="text" name="lightPortParam" value="${lightPortException}" />, <input type="text" name="lightBoolParam" value="${lightBoolException}" />);
		</pre>
		
		
		<br /><br /><br />
		
		<h2>ColorSensor</h2>
		
		Der ColorSensor ist standardm&auml;ßig an Port 4 definiert. Trage die Portnummer ein, in dem
		dein Sensor steckt.
		
		<pre>
			ColorSensor color = new ColorSensor(SensorPort.S<input type="text" name="colorPortParam" value="${colorException}" />);
		</pre>
		
		<br /><br /><br />
		
		<h2>Pilot</h2>
		
		Der Pilot ist standardm&auml;ßig mit der Parametern Durchmesser = 46mm, Radabstand = 156mm,<br />
		1. Motor Port A und 2. Motor Port C definiert.<br />
		Trage im ersten Feld den Durchmesser in mm, im zweiten Feld den Radabstand in mm (dabei gilt<br />
		erste Radmitte bis zweite Radmitte) und im dritten und vierten Feld den Buchstaben der Ports,<br />
		an denen deine Motoren angeschlossen sind, ein. Achte darauf, dass du die Buchstaben groß schreibst.
		
		<pre>
			Pilot pilot = new TachoPilot(<input type="text" name="motorDiameterParam" value="${motorDiameterException}" />,
						     <input type="text" name="motorTrackWidthParam" value="${motorTrackWidthException}" />,
					       Motor.<input type="text" name="motor1Param" value="${motor1Exception}" />,
					       Motor.<input type="text" name="motor2Param" value="${motor2Exception}" />);
		</pre>
		
		<input type="submit" name="submitButton" value="Parameter hochladen" />
		
		</form>

<%@ include file="inc/footer.jsp" %>