<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>


<%@ include file="inc/header.jsp" %>
		
		<h1>Der Farbsensor / ColorSensor</h1>

		<p>
			<table border="0" width="100%">
				<tr>
					<td style="vertical-align:top; width:50%">
						
						Der Farbsensor ist, wie der Name schon sagt, ein Sensor, der Farben erkennen kann.<br />
						Er kann die die Farbwerte als Integer oder als Farbindex zur&uuml;ck geben.<br />
						Der Farbindex deckt eine kleine Anzahl von Farben ab und gibt nur den Wert der Farbe<br />
						als Integer wieder zur&uuml;ck. Bei dem Farbsensor ist eine kleine Liste dabei, die dir<br />
						sagt, welche Zahl zu welcher Farbe geh&ouml;rt.<br />
						Du kannst auch nur die Rot-, Blau- oder Gr&uuml;nanteile auslesen lassen. Diese Werte<br />
						werden dann ebenfalls als Integer ausgegeben.

					</td>
					<td style="vertical-align:right; width:50%">
						<img width="50%" src="images/colorSensor.png" alt="colorSensor.png" align=right />
					</td>			
				</tr>
			</table>
		</p>
		
		<h2>Einen Farbsensor erstellen / Deklaration und Initialisierung</h2>
		
		Der Farbsensor hat, wie alle Sensoren, seine eigene Klasse in Java. Damit der NXT mit dem Sensor arbeiten kann,<br />
		m&uuml;ssen wir ihm sagen, wo der Sensor angeschlossen ist. Mehr m&uml;ssen wir nicht tun.<br />
		In Java k&ouml;nnte das dann so aussehen:
		
		<pre>
			ColorSensor color = new ColorSensor(SensorPort.S4);
		</pre>
		
		
		<form name="initForm">
			<h3>Eigene Initialisierung</h3>
			
			Hier kannst du deinen eigenen LightSensor initialisieren.
			
			<pre>
				ColorSensor color = new ColorSensor(
					<input type="text" name="port" />); 		SensorPort im Format SensorPort.Port
					
				<input type="submit" name="submitInit" value="ColorSensor neu definieren." />
			</pre>
		</form>
		
		
		<br />
		<br />
		<br />
		
		<form name="colorNumberForm">
			<h2>Farbwert auslesen / getColorNumber</h2>
			
			Es gibt verschiedene Arten, um den Farbwert bestimmen zu lassen. Der einfachste ist, sich die Nummer<br />
			ausgeben zu lassen. Bei dem ColorSensor ist eine Anleitung dabei, in der eine Liste steht, die anzeigt<br />
			welcher Wert zu welcher Farbe gehört. Aber am besten ist es, man probiert es selbst aus. Die Werte<br />
			k&ouml;nnen n&auml;mlich abweichen.<br />
			Die Methode, um sich die Nummer ausgeben zu lassen, heißt getColorNumber. In unserem Beispiel wird<br />
			die Nummer auf dem Bildschirm des NXTs  angezeigt. Die Aktuellste steht immer ganz unten.<br />
			
			<pre>
				color.getColorNumber();		<input type="submit" name="colorNumberSubmit" value="Beispiel hochladen." />
			</pre>
			
			Wenn Du keine Liste zur Hand hast, benutze einfach diese hier:<br /><br />
			
			0 = schwarz<br />
			1 = violett<br />
			2 = purpur<br />
			3 = blau<br />
			4 = gr&uuml;n<br />
			5 = hellgr&uuml;n<br />
			6 = gelb<br />
			7 = orange<br />
			8 = rot<br />
			9 = dunkelrot<br />
			10 = magenta<br />
			11 to 16 = pastel<br />
			17 = weiß

		</form>
		
		
		<br />
		<br />
		<br />
		
		<form name="blueComponentForm">
			<h2>Den Blau-Wert auslesen / getBlueComponent</h2>
			
			Man kann sich auch nur den blauen Teil der Farbe ausgeben lassen. Das ist dann vorteilhaft, wenn man<br />
			einen genauen Wert ermitteln m&ouml;chte und sich nicht auf die Liste verlassen will.<br />
			In Java kann das dann so aussehen:
			
			<pre>
				color.getBlueComponent();	<input type="submit" name="blueComponentSubmit" value="Beispiel hochladen." />
			</pre>			
		</form>
		
		
		<br />
		<br />
		<br />
		
		<form name="redComponentForm">
			<h2>Den Rot-Wert auslesen / getRedComponent</h2>
			
			Man kann sich auch nur den roten Teil der Farbe ausgeben lassen. Das ist dann vorteilhaft, wenn man<br />
			einen genauen Wert ermitteln m&ouml;chte und sich nicht auf die Liste verlassen will.<br />
			In Java kann das dann so aussehen:
			
			<pre>
				color.getRedComponent();	<input type="submit" name="redComponentSubmit" value="Beispiel hochladen." />
			</pre>			
		</form>
		
		<br />
		<br />
		<br />
		
		<form name="greenComponentForm">
			<h2>Den Gr&uuml;n-Wert auslesen / getGreenComponent</h2>
			
			Man kann sich auch nur den gr&uml;nen Teil der Farbe ausgeben lassen. Das ist dann vorteilhaft, wenn man<br />
			einen genauen Wert ermitteln m&ouml;chte und sich nicht auf die Liste verlassen will.<br />
			In Java kann das dann so aussehen:
			
			<pre>
				color.getGreenComponent();	<input type="submit" name="greenComponentSubmit" value="Beispiel hochladen." />
			</pre>			
		</form>
<%@ include file="inc/footer.jsp" %>