<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>


<%@ include file="inc/header.jsp" %>

      	<h1>Der Lichtsensor / LightSensor</h1>
		
		<p>
			<table border="0" width="100%">
				<tr>
					<td style="vertical-align:top; width:50%">
						
						Der Lichtsensor ist dazu da, um den aktuellen Lichtwert zu messen.<br />
						Er besitzt zwei LEDs. Eine, um den Lichtwert zu messen und die andere, um extra Licht<br />
						zu machen.<br />
						Der Lichtsensor ist aber nicht nur dazu da, um Licht zu messen. Er kann auch zwischen schwarz<br />
						und weiß unterscheiden. So kann er z.B. auch einer schwarzen Spur auf dem Boden folgen (wird<br />
						hier nicht weiter behandelt).
					
					</td>
					<td style="vertical-align:right; width:50%">
						<img width="35%" src="images/lightSensor.png" alt="lightSensor.jpg" align=right />
					</td>			
				</tr>
			</table>
		</p>

		<h2>Einen Lichtsensor erstellen / Deklaration und Initialisierung</h2>
		
		Der Lichtsensor, bzw. LightSensor, hat seine eigene Klasse. Wie auch andere Sensoren, muss der<br />
		LightSensor an einem SensorPort angeschlossen werden. Initialisieren wir einen LightSensor,<br />
		m&uuml;ssen wir sagen, an welchem Port dieser angeschlossen ist. Wahlweise k&ouml;nnen wir noch<br />
		angeben, ob das Licht an oder aus sein soll. Zudem sollte ein treffender Name nicht fehlen.<br />
		In Java sieht das dann so aus:
		
		<pre>
			LightSensor light = new LightSensor(SensorPort.S2);
			oder
			LightSensor light = new LightSensor(SensorPort.S2, false);
		</pre>
		
		<form name="initForm">
			<h3>Eigene Initialisierung</h3>
			
			Hier kannst du deinen eigenen LightSensor initialisieren.
			
			<pre>
				LightSensor light = new LightSensor(
					<input type="text" name="port" />, 		SensorPort im Format SensorPort.Port
					<input type="text" name="floodlight" />);		Floodlight im Format true oder false.
					
				<input type="submit" name="submitInit" value="LightSensor neu definieren." />
			</pre>
		</form>
		
		<br />
		<br />
		<br />
		
		<form name="lightValueForm">
			<h2>Den Lichtwert abfragen / getLightValue</h2>
			
			Um den Lichtwert abfragen zu k&ouml;nnen, stellt der LightSensor eine eigene Methode bereit.<br />
			Sie heißt getLightValue.<br />
			In unserem Beispielprogramm wird auf dem Bildschirm des NXTs nun der Lichtwert ausgegeben.<br />
			Der Aktuellste steht dabei immer ganz unten.
			
			<pre>
				light.getLightValue();	<input type="submit" name="submitLightValue" value="Beispiel hochladen." />
			</pre>
		</form>
		
		<br />
		<br />
		<br />
		
		<form name="floodLightForm">
			<h2>Licht ein und ausschalten / setFloodlight</h2>
			
			Um das Licht am LightSensor auszuschalten, m&uuml;ssen wir eine boolsche Variable, also einen<br />
			Wahrheitswert &uuml;bergeben. Standardm&auml;ßig ist das Licht an. Man kann aber schon beim<br />
			Initialisieren entscheiden, ob das Licht an oder aus sein soll.<br />
			In Java sieht das so aus:
			
			<pre>
				LightSensor light = new LightSensor(SensorPort.S2, false);
			</pre>
			
			In diesem Fall w&auml;re das Licht aus.<br />
			
			Hat man den LightSensor aber schon initialisiert und das Licht ist an, man m&ouml;chte es aber aus<br />
			haben, dann muss man das mit der LightSensor eigenen Methode setFloodlight machen.<br />
			In unserem Beispiel Programm ist das Licht standardm&auml;ßig an und gibt die Lichtwerte wieder.<br />
			Dr&uuml;ckst du den Enterbutton (das ist der große orangene in der Mitte des NXTs), so geht das Licht<br />
			aus und du erh&auml;lst andere Lichtwerte.<br />
			In Java k&ouml;nnte das so aussehen:
			
			<pre>
				light.setFloodlight(false);	<input type="submit" name="submitFloodLight" value="Beispiel hochladen." />
			</pre>
		</form>
		
<%@ include file="inc/footer.jsp" %>