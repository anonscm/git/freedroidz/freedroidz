<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>


<%@ include file="inc/header.jsp" %> 

	<h1>Der Ultraschallsensor / UltrasonicSensor</h1>
	
	<p>
		<table border="0" width="100%">
			<tr>
				<td style="vertical-align:top; width:50%">
					
					Der Ultraschallsensor wird verwendet um Entfernungen zu messen. Er ist quasi das Auge des NXTs.<br />
					Z.B. kann man ihn verwenden, um zu pr&uuml;fen, wie weit eine Wand entfernt ist, auf die der NXT<br />
					zuf&auml;hrt.
				
				</td>
				<td style="vertical-align:right; width:50%">
					<img width="35%" src="images/ultrasonicSensor.png" alt="ultrasonicSensor.jpg" align=right />
				</td>			
			</tr>
		</table>
	</p>
	
	
	<h2>Erstellen eines Ultraschallsensors / Deklaration und Initialisierung</h2>

	Der Ultrasonicsensor ist eine eigene Klasse und muss auch wie eine Initialisiert werden.<br />
	Man sagt dem Programm man will einen Ultrasonicsensor erstellen und gibt diesem einen Namen, z.B. sonic.<br />
	Damit weiß jeder, dass es ein Ultrasonicsensor ist und man benutzt einen nicht zu langen Namen.<br />
	Hat man einen treffenderen Namen, so sollte man diesen benutzen.<br />
	Man muss dem Sensor auch einen Port zuweisen, damit der NXT sp&auml;ter weiß, wo sich der Sensor befindet.<br />
	Das macht man mit dem Sensorport.<br />
	Die Initialisierung sieht dann also so aus:<br />
	
	<pre>
		UltrasonicSensor sonic = new UltrasonicSensor(SensorPort.S1);
	</pre>
	
	Das Programm weiß jetzt, dass ein Ultraschallsensor existiert und dass er am Sensorport 1 angeschlossen ist.
	
	<form name="initForm">
		<h3>Eigene Initialisierung</h3>
			
		Hier kannst du deinen eigenen UltrasonicSensor initialisieren.
			
		<pre>
			UltrasonicSensor sonic = new UltrasonicSensor(<input type="text" name="port" value="${portException}" />); 		SensorPort im Format SensorPort.Port
					
			<input type="submit" name="submitInit" value="UltrasonicSensor neu definieren." />
		</pre>
	</form>
	
	<br />
	<br />
	<br />
	
	<form name="distanceForm">
		<h2>Entfernund bzw. Distanz messen / getDistance</h2>
		
		Um die Entfernung zu einem Gegenstand zu messen, m&uuml;ssen wir die getDistance-Methode benutzen.<br />
		Sie gibt einen Wert von 0 (ganz nah) bis 255 (ganz weit / nichts) in cm an. Der Wert kommt zustande, da ein ganzes Byte<br />
		den Wert, der vom Sensor geschickt wurde, wiedergibt. Ein Byte besteht aus 8 bits, was soviel heißt wie<br />
		8 Nullen und / oder Einsen. Im Bin&auml;ren sind das 2 hoch 8 = 256 M&ouml;glichkeiten. Da Computer aber<br />
		die 0 mitz&auml;hlen, geht der Wert nur bis 255.<br />
		In unserem Beispiel wird der Wert, der momentan vom Sensor gemessen wird auf dem Bildschirm des NXTs ausgegeben.<br />
		Der aktuellste Wert steht dabei immer an unterster Stelle.<br />
		Im Code sieht das ganze dann so aus:
		
		<pre>
			sonic.getDistance();	<input type="submit" name="submitGetDistance" value="Beispiel hochladen." />
		</pre>
		
		<h3>Achtung!</h3>
		Mit dem Code-Schnippsel wird nichts auf dem Bildschirm ausgegeben. Es ist lediglich die Methode,<br />
		die den Wert vom Sensor abholt. Die Bildschirmausgabe wird hier nicht behandelt, sie funktioniert aber<br />
		wie eine Konsolenausgabe.
	</form>
	
	<br />
	<br />
	<br />
	
	<h2>Fehlerbehandlung</h2>
	
	Es kann vorkommen, dass der Ultraschallsensor nicht an allen Sensorports funktioniert. Der betroffene Port<br />
	ist der Port 4. An ihm kann die Entfernung nicht gelesen werden.
	
	
	<br />
	<br />		
	
<%@ include file="inc/footer.jsp" %> 