<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>


<%@ include file="inc/header.jsp" %> 
	
		
		      		<h1>Der Motor</h1>
		<p>
			<table border="0" width="100%">
				<tr>
					<td style="vertical-align:top;">
						Der Motor (rechts im Bild) hat seine ganz eigene Klasse in Java. Seine Hauptarbeit ist das Vorw&auml;rts-<br />
						und R&uuml;ckw&auml;rtsfahren. H&auml;ufig wird er nur f&uuml;r einfache Sachen benutzt, wof&uuml;r nur ein Motor ben&ouml;tigt wird.<br />
						Zum Fahren an sich sollte man versuchen die direkte Benutzung des Motors zu vermeiden.<br />
						Der Code w&uuml;rde sonst sehr un&uuml;bersichlich werden und man m&uuml;sste mehr schreiben.<br />
						Um deinen Roboter fahren zu lassen, solltest Du lieber den Piloten benutzen,<br />
						der <a href="Pilot">hier</a> erkl&auml;rt wird.
					</td>
					<td style="vertical-align:right;">
						<img src="images/motor.png" alt="motor.png" align=right />
					</td>			
				</tr>
			</table>
		</p>		      			
		
		<br />
		<br />
		
		<h2>Erstellen eines Motors / Deklaration und Initialisierung</h2>
		
		Um den Motor benutzen zu k&ouml;nnen kann man ihn direkt ansprechen oder eine Variable vom Typ Motor anlegen.<br />
		Um eine Motor-Variable anzulegen m&uuml;ssen wir wissen, dass unser Objekt Motor heißt. Ausserdem brauchen wir<br />
		Noch einen geeigenten Namen, den wir dem Motor geben. In unserem Fall haben wir keine besonderen Aufgaben f&uuml;r<br />
		unseren Motor, deßhalb nennen wir ihn einfach motorA. Wir nennen ihn deshalb motorA, weil er an der Schnittstelle A angeschlossen ist.<br />
		(Achtung! Variablenbezeichnungen d&uuml;rfen niemals genauso geschrieben werden, wie die Typen, z.B. darf der Name<br />
		des Motors nicht Motor heißen, sondern sollte klein geschrieben werden.)<br />
		Wir schreiben also:<br />
		
		<pre>
			Motor motorA;
		</pre>
		
		<br /><br />
		
		Jetzt weiß unser Programm zwar, dass wir einen Motor haben, aber immernoch nicht, wo er angeschlossen ist.<br />
		Wir haben den Motor zwar motorA genannt, aber wir h&auml;tten ihn auch Herbert nennen k&ouml;nnen. Dann ist es nicht<br />
		mehr klar, wo der Motor angeschlossen ist, und unser Programm weiß es schon gar nicht.<br />
		Wir m&uuml;ssen dem Programm also sagen, wo der Motor angeschlossen ist.<br />
		Das machen wir also so:
		
		<pre>
			Motor motorA = Motor.A;
		</pre>
		
		<br />
		
		Nun k&ouml;nnen wir den Motor benutzen.<br /><br />
		
		<form name="reinit">
		 	<h3>Eigene Initialisierung</h3>
		 	
		 	Wenn du einen Motor an einem anderem Port angeschlossen hast, kannst du diesen hier nun gerne angeben.
		 	
		 	<pre>
		 		Motor motor = <input type="text" name="motor" value="${motorException}" />;	Hier bitte den Motor im Format "Motor.Port"
		 		<input type="submit" name="submitInit" value="Motor neu definieren." />
		 	</pre>
		</form>
				
		
		<br />
		<br />
		<br />
		
		<form name="forwardForm">
			<h2>Vorw&auml;rtsfahren / forward</h2>
			
			Die Methode zum vorw&auml;rtsfahren heißt in Java forward. <br />
			Sie lässt den Motor solange vorw&auml;rtsfahren, bis man<br />
			dem Motor einen anderen Befehl gibt. Z.B. stoppen oder<br />
			r&uuml;ckw&auml;rtsfahren.
			 
			<br />
			<br />
			
			<pre>
				Motor.A.forward();	<input type="submit" name="submitForward" value="Beispiel hochladen." />
			</pre>
			
			<br />
			<br />
			
			Oder du l&auml;sst ihn r&uuml;ckw&auml;rts fahren.
			
			<br />
			<br />
			
			<pre>
			Motor.A.backward();	<input type="submit" name="submitBackward" value="Beispiel hochladen." />
			</pre>
		</form>
		
		<br />
		<br />
		<br />
		
		<form name="rotateForm">
			<h2>Um ein bestimmtest Grad rotieren bzw. drehen lassen / rotate</h2>
			
			Die Methode, um den Motor um ein bestimmtes Grad drehen zu lassen, <br />
			heißt in Java rotate. Sie lässt den Motor sich um das angegebene<br/>
			Grad rotieren.
			 
			<br />
			<br />
			
			<pre>
				Motor.A.rotate(<input type="text" name="degree" value="${degreeException}" />);	<input type="submit" name="submitRotate" value="Beispiel hochladen." />
			</pre>
		</form>
		
		<br />
		<br />
		<br />
		
		<form name="rotateToForm">
			<h2>Zu einem Gradpunkt rotieren bzw. drehen lassen / rotateTo</h2>
			
			Jeder Motor startet bei dem Grad 0°. Die Zahl 0 wird also in einer Variable gespeichert,<br />
			die zu dem Motor gehört. Sagt man dem Motor, er soll sich zum Grad 1 drehen,<br />
			dreht er sich um 1 Grad vorw&auml;rts. Sagt man ihm dann, er soll sich zum Grad 359 drehen,<br />
			dann dreht er sich 2 Grad r&uuml;ckw&auml;rts. Der Motor w&auml;hlt also den k&uuml;rzesten Weg,<br />
			um zu dieser Position zu kommen. Sagt man dem Motor eine Gradzahl gr&ouml;sser als 360,<br />
			so f&auml;ngt der Motor wieder bei 0 an. Ausserdem ist das Grad 0 = Grad 360.<br /><br />
			In unserem Beispiel werden wir zu zwei verschiedenen Graden rotieren lassen,<br />
			damit die Methode auch klar wird. Probiere ruhig ein wenig rum, damit du siehst,<br />
			dass diese Methode wirklich etwas anderes macht, als die rotate-Methode.
			 
			<br />
			<br />
			
			<pre>
				Motor.A.rotateTo(<input type="text" name="toDegree1" value="${toDegree1Exception}" />);	<input type="submit" name="submitRotateTo" value="Beispiel hochladen." />
				Motor.A.rotateTo(<input type="text" name="toDegree2" value="${toDegree2Exception}" />);
			</pre>
		</form>
		
		<br />
		<br />
		<br />
		
		<form name="stopFloatForm">
			<h2>Den Motor stoppen oder ausrollen lassen / stop / float</h2>
			
			Um einen Motor zu stoppen benutzt man die Methode stop.<br />
			Um ihn ausrollen zu lassen benutzt man die Mehtode flt, was für float steht.<br /><br />
			In unserem Beispielprogramm f&auml;hrt der Motor ersteinmal und stoppt dann abruppt,<br />
			er benutzt also die stop-Methode. Dann fährt er wieder ein bisschen und<br />
			benutzt dann die flt-Methode. Daran kann man den Unterschied sehr sch&ouml;n erkennen.<br />
			In Java könnte das ganze (vereinfacht) so aussehen:
			
			<br />
			<br />
			
			<pre>
				Motor.A.stop();
				Motor.A.flt();	<input type="submit" name="submitStopFloat" value="Beispiel hochladen." />
			</pre>			
		</form>
					
<%@ include file="inc/footer.jsp" %> 