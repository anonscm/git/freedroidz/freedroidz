<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>


<%@ include file="inc/header.jsp" %> 

		<h1>Der Pilot bzw. Tachopilot</h1>
		
		Der Pilot ist eigentlich nur eine Schnittstelle (Interface), was uns hier aber nicht<br />
		weiter st&ouml;ren soll. Wir besch&auml;ftigen uns hier speziefisch mit dem Tachopiloten.<br />
		Der Pilot ist eine Klasse, die zwei Motoren steuert, die an dem NXT angeschlossen sind.<br />
		Er übernimmt also jede Menge Rechenarbeit, die wir nicht mehr machen m&uuml;ssen.<br />
		Z.B. ausrechnen, wie weit ein Motor drehen muss, damit der NXT sich um 90° gedreht oder,<br />
		wie weit sich die Motoren drehen m&uuml;ssen, damit der NXT 10cm weit gefahren ist.<br />
		Das alles errechnet der Pilot f&uuml;r uns.
		
		
		<br />
		<br />
		<br />
		
		<h2>Deklaration und Initialisierung</h2>
		
		Wie schon gesagt, geh&ouml;ren zu dem Piloten zwei Motoren. Der Pilot geht davon aus,<br />
		dass an diesen Motoren jeweils Reifen stecken. Er muss jetzt nur noch wissen, wie groß<br />
		die Reifen sind (Durchmesser in mm), wie weit sie von einander entfernt (in mm) sind und an welchen<br />
		Ports die Motoren angeschlossen sind.<br />
		Wir initialisieren den Piloten wie eine Klasse. Das sieht dann so aus:<br />
		
		<pre>
			Pilot pilot = new TachoPilot(46, 156, Motor.A, Motor.C);
		</pre>
		
		Der erste Wert, der angegeben wird, ist der Reifendurchmesser. Der zweite Wert ist der Abstand<br />
		der Reifen und die letzten Beiden sagen dem Piloten, welche Motoren er benutzen soll, also<br />
		wo die Motoren angeschlossen sind.<br />
		
		<form name="initForm">
			<h3>Eigene Initialisierung</h3>
			
			Hier kannst du deinen eigenen Piloten initialisieren, falls du einen NXT gebaut hast, der andere Maße hat.
			
			<pre>
				Pilot pilot = new TachoPilot(
					<input type="text" name="diameter" />, 		Reifendurchmesser
					<input type="text" name="trackWidth" />,		Reifenabstand
					<input type="text" name="motor1" />,		Motor1
					<input type="text" name="motor2" />);		Motor2
					
				<input type="submit" name="submitInit" value="Pilot neu definieren." />
			</pre>
		</form>
		
		<br />
		<br />
		<br />
		
		<form name="travelForm">
			<h2>vorw&auml;rts bzw. r&uuml;ckw&auml;rts fahren / forward and backward</h2>
			
			Der Tachopilot kann viele Sachen, die der Motor auch kann. Somit kann er auch<br />
			vorw&auml;rts als auch r&uuml;ckw&auml;rts fahren.<br />
			Das sieht auch genauso aus:
			
			<pre>
				pilot.forward();	<input type="submit" name="submitForward" value="Beispiel hochladen." />
				
				pilot.backward();	<input type="submit" name="submitBackward" value="Beispiel hochladen." />
			</pre>
			
			
		</form>		
		
		<br />
		<br />
		<br />
		
		<form name="travelArcForm">
		
			<h2>Ein bestimmtes St&uuml;ck im Kreis fahren / travelArc</h2>
			
			Will man den Roboter im Kreis fahren lassen, kann man die travelArc-Methode verwenden.<br />
			Man sagt der Methode, wie groß der Radius des Kreises sein soll, den der Roboter fahren<br />
			soll. Außerdem muss man noch angeben, wie weit der Roboter fahren soll (in mm).<br />
			Sagt man z.B., dass der Kreis 10cm Radius haben soll und der Roboter 15,7cm also 157mm fahren soll,<br />
			dann ist er genau um 90° des Kreises gefahren.<br />
			
			<pre>
				pilot.travelArc(<input type="text" name="radius" />, <input type="text" name="distanceArc" />); <input type="submit" name="submitTravelArc" value="Beispiel hochladen." />
			</pre>
		
		</form>
		
		<br />
		<br />
		<br />
		
		<form name="rotateForm">
			<h2>Drehen / rotate</h2>
			Auch hier haben wir wieder eine Methode, die genauso klingt, wie die des Motors.
			Der einzige unterschied ist, dass sich die Motoren nicht um die angegebene Gradzahl
			drehen, sondern der komplette NXT tut dies nun.
			Haben wir also die richtigen Parameter bei der Initialisierung eingegeben und geben
			dem Piloten nun an, er solle sich um 90° drehen, dann dreht sich der komplette
			Roboter um 90° auf der Stelle.
			
			<pre>
				pilot.rotate( <input type="text" name="degree" /> );	<input type="submit" name="submitRotate" value="Beispiel hochladen." />
			</pre>
			
		</form>		
		
		<br />
		<br />
		<br />
		
		<form name="stopForm">
			<h2>Den Piloten stoppen / stop</h2>
			
			Um den Piloten zu stoppen benutzt man die Methode stop.<br />
			Das funktioniert genauso, wie bei dem Motor. Der Roboter stoppt also abrupt.<br />
			Das sieht dann in Java so aus:
			
			<br />
			<br />
			
			<pre>
				pilot.stop();	<input type="submit" name="submitStop" value="Beispiel hochladen." />
			</pre>			
		</form>
		
		<br />
		<br />
		<br />
		
	
<%@ include file="inc/footer.jsp" %> 