<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
 
import lejos.nxt.Motor;
import lejos.robotics.navigation.Pilot;
import lejos.robotics.navigation.TachoPilot;

public class Test {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			
			Pilot pilot = new TachoPilot(58, 174, Motor.A, Motor.C);
			pilot.travel(${distance});
		} catch (InterruptedException e) {
			// passiert eigentlich nie
		}
	}
}
