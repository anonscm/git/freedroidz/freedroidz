Inhalt:

- Tests ausführen 
- Neue tests schreiben
- Tipps / Sonstiges

#### TESTS AUSFÜHREN #####

1) Selenium-Server starten:

cd path/to/freedroidz-community-features

mvn -Pserver selenium:xvfb selenium:start-server

- Das -Pserver aktiviert die richtigen Einstellungen
- selenium:xvfb ist optional, sorgt dafür, dass die Browserfenster nicht angezeigt werden

2) Integrations-Tests ausführen:

cd path/to/freedroidz-community-features

mvn integration-test -P{firefox,chromium,mock}

Je nach dem funktionieren die Browser auf manchen Systemen nicht... Firefox scheint auf manchen System Shell-skripte zum starten zu benutzen, damit komm selenium nicht klar.

#### neue Tests schreiben ####

1) unter freedroidz-community-features/features eine neue *.feature Datei anlegen, jedes Feature testet ein Programm-Feature beinhaltet aber mehrere Tests (sogenannte Szenarios)

2) Ein Test besteht immer aus

Angenommen xxxxxx
Wenn xxxxx
Dann xxxx

"Und" steht im Prinzip für eine Wiederholung des letztes Angenommen / Wenn / Dann

3) Tests ausführen, wie oben beschrieben, dann wird angezeigt, welche Schritte (Steps) noch nicht implementiert sind, gleichzeitig wird schon eine Vorlage für je eine Implementierung in Java und Ruby angezeigt. Wir benutzen nur Ruby, da Java für das testen unnötige Umstände und Unübersichtlichkeit mitbringt (Methodennamen z.B.)

4) Die Steps implementieren in steps_definitions, jede neue Datei wird automatisch eingebunden, keine weitere Konfiguration ist nötig.

5) Sich daran erfreuen, dass es grün wird :P


#### Tipps / Sonstiges 

- bei Fragen: Mattis, Nasko (Atanas Alexandrov), Lukas, Ulli
- in cucumber-Tests werden die Anführungsstriche benutzt um Text-teile zu markieren, die Variabel sind (Bspw. Namen etc)
- Alle Tests sollten auch beim 2. Mal noch laufen (und nicht daran scheitern, dass der Benutzer schon existiert etc..)
- ALLE Features der Software sollten im optimalen Fall abgedeckt sein.
- kein Seitenlayout testen ;)
- Der Mock-Browser funktioniert nicht richtig, auch wenn er schön schnell ist
- (TODO: Saubere Lösung finden!!!) Es gibt Annotations z.B. will man oft nur den Test ausführen, an dem man arbeitet. Dann einfach @current über das Szenario schreiben. Um sie zu starten muss man die cucumber-args in der pom.xml um 
<cucumberArg>--tags</cucumberArg>                                                                                <cucumberArg>${cucumber.tags}</cucumberArg>

erweitern und -Dcucumber.tags=@current beim starten hinzufügen... ohne das -Dcucumber.tags läuft das ganze dann allerdings nicht mehr.