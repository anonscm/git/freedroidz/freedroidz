# language: de
Funktionalität: Registrieren, Einloggen, meine Daten bearbeiten

	Szenario: Registriere einen Test-User
Angenommen ich öffne die freedroidz-community Seite
Wenn ich auf "Registrieren" klicke
Und "CucumberTest" in das Feld "Benutzername" eingebe
Und "1234" in das Feld "Passwort *:" eingebe
Und "1234" in das Feld "Passwort wiederholen" eingebe
Und ich auf den Knopf "Registrieren!" drücke
Dann sollte da "Du hast dich erfolgreich registriert" oder "There already is a user with this username:" stehen.

	Szenario: Ich logge mich mit dem gerade angelegten Benutzer ein und logge mich wieder aus.
Angenommen ich öffne die freedroidz-community Seite
Wenn ich "CucumberTest" in das Feld "Benutzer" eingebe
Und "1234" in das Feld "Passwort:" eingebe
Und ich auf den Knopf "Login" drücke
Dann sollte da "Du bist eingeloggt." stehen.
Dann sollte der Button "Login" nicht vorhanden sein.
Wenn ich auf "Abmelden" klicke
Dann sollte da "Du wurdest abgemeldet." stehen.
Und es sollte der Link "Abmelden" nicht vorhanden sein.

	Szenario: Ich versuche mich mit einem falschen Passwort einzuloggen, darüber sollte ich benachrichtigt werden.
Angenommen ich öffne die freedroidz-community Seite
Wenn ich "CucumberTest" in das Feld "Benutzer" eingebe
Und "4321" in das Feld "Passwort:" eingebe
Und ich auf den Knopf "Login" drücke
Dann sollte da "Benutzername oder Passwort falsch" stehen.

	Szenario: Ich will meine persönlichen Daten ändern: meine Alten sollten drin stehen.
Angenommen ich öffne die freedroidz-community Seite
Wenn ich "CucumberTest" in das Feld "Benutzer" eingebe
Und "1234" in das Feld "Passwort:" eingebe
Und ich auf den Knopf "Login" drücke
Und ich auf "Meine Daten bearbeiten" klicke
Dann sollte "CucumberTest" in dem Feld "Benutzername" stehen