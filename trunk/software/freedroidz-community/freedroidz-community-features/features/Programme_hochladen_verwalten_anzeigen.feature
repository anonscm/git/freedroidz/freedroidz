# language: de
Funktionalität:  Programme hochladen / verwalten / herunterladen etc.

	Szenario: Ich lade ein Programm hoch und lösche es wieder. 
Angenommen ich registriere den Benutzer "CucumberTest_Programme" mit dem Passwort "1234" und logge mich ein.
Wenn ich auf "Programme hochladen" klicke
Und "cucumber_test_programm" in das Feld "Name:" eingebe
Und "Dieses Programm ist nur ein Test" in das Feld "Beschreibung:" eingebe
Und "simpel" als "Komplexität" auswähle
Und "6" in das Feld "Aufwand" eingebe
Und ein temporäres Programm anlege und dieses als Datei auswähle
Und ich auf den Knopf "Hochladen" drücke
Dann sollte da "Aufwand: 6" stehen
Und es sollte da "Dieses Programm ist nur ein Test" stehen
Und es sollte da "Inhalt des Programms" stehen