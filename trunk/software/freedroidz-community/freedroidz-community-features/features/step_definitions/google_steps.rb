# encoding: utf-8
begin require 'rspec/expectations'; rescue LoadError; require 'spec/expectations'; end
require 'cucumber/formatter/unicode'
require "rubygems"

# Demonstrating that nothing stops you from mixing step definitions in different languages
# even in a single scenario -- provided you find a way to share your session state between
# the different implementations. See env.rb and SeleniumSingleton.java for one  
# example of how to do this with Java, Spring and JRuby.

Dann /^befindet sich "([^"]*)" unter den Ergebnissen$/ do |erwartetesErgebnis|  
  # results are loaded using AJAX magic. We wait for at least one result before we continue.
  browser.waitForCondition "selenium.isElementPresent(\"xpath=//ol[@id='rso']/li\")", "10000"
  if not browser.is_text_present?(erwartetesErgebnis) then
    puts browser.body_text
    raise "Taucht nicht auf: "+erwartetesErgebnis 
  end
end
