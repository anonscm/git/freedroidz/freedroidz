# encoding: utf-8
begin require 'rspec/expectations'; rescue LoadError; require 'spec/expectations'; end
require 'cucumber/formatter/unicode'
require "rubygems"

Wenn /^"([^"]*)" als "([^"]*)" auswähle$/ do |was, select_box_label|
	xpath = "xpath=//select[@id=//label[contains(.,'#{select_box_label}')]/@for]"
	browser.select xpath, was
end

Dann /^(es )?sollte da "([^"]*)" stehen([\.]*)$/ do |_,text, _|
	if not browser.is_text_present?(text)
		puts "Inhalt der Seite: " + browser.body_text
		raise "Text nicht vorhanden: " + text
	end
end