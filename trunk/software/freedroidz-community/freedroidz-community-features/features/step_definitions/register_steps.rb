# encoding: utf-8
begin require 'rspec/expectations'; rescue LoadError; require 'spec/expectations'; end
require 'cucumber/formatter/unicode'
require "rubygems"

Angenommen /^ich öffne die freedroidz\-community Seite$/ do
   	browser.open "freedroidz-community-webapp"
   	#browser.speed= "5000"
	browser.wait_for_page_to_load "10000"
end

Wenn /^ich auf "([^"]*)" klicke$/ do |link_text|
	browser.click "//a[contains(.,'#{link_text}')]"
	browser.wait_for_page_to_load "10000"
end

Wenn /^(ich )?"([^"]*)" in das Feld "([^"]*)" eingebe$/ do |_,text, feld_label|
	browser.type  "xpath=//input[@id=//label[contains(.,'#{feld_label}')]/@for]", text
end

Wenn /^ich auf den Knopf "([^"]*)" drücke$/ do |button|
	browser.click "//input[@value=\"" + button + "\"]"
	browser.wait_for_page_to_load "10000"
end

Dann /^sollte da "([^"]*)" oder "([^"]*)" stehen\.$/ do |text1, text2|
	if not browser.is_text_present?(text1) and not browser.is_text_present?(text2)
		puts "Inhalt der Seite: " + browser.body_text
		raise "Texte nicht vorhanden, 1.:" + text1 + " 2.:" + text2 
	end
end

Dann /^sollte "([^"]*)" in dem Feld "([^"]*)" stehen$/ do |text, feld_label|
	if not browser.is_element_present?("xpath=//input[@id=//label[contains(.,'#{feld_label}')]/@for and contains(@value, '#{text}') ]")
		raise "Tuts aber nicht!"
	end
end

Dann /^sollte der Button "([^"]*)" nicht vorhanden sein\.$/ do |button_name|
	if browser.is_element_present?("xpath=//input[contains(@value, '#{button_name}')]")
		raise "Der Button '#{button_name}' ist aber vorhanden!"
	end
end

Dann /^es sollte der Link "([^"]*)" nicht vorhanden sein\.$/ do |link_name|
	if browser.is_element_present?("xpath=//a[contains(., '#{link_name}')]")
		raise "Der Link '#{link_name}' ist aber vorhanden!"
	end
end

Angenommen /^ich registriere den Benutzer "([^"]*)" mit dem Passwort "([^"]*)" und logge mich ein\.$/ do |benutzer, passwort|
	Angenommen "ich öffne die freedroidz-community Seite"
	Wenn "ich auf \"Registrieren\" klicke"
	Und "\"#{benutzer}\" in das Feld \"Benutzername\" eingebe"
	Und "\"#{passwort}\" in das Feld \"Passwort *:\" eingebe"
	Und "\"#{passwort}\" in das Feld \"Passwort wiederholen\" eingebe"
	Und "ich auf den Knopf \"Registrieren!\" drücke"
	#Muss man nochmal machen, weil eine Fehler-Seite angezeigt wird, falls der User bereits vorhanden ist.
	Angenommen "ich öffne die freedroidz-community Seite"
	Wenn "ich \"#{benutzer}\" in das Feld \"Benutzer\" eingebe"
	Und "\"#{passwort}\" in das Feld \"Passwort:\" eingebe"
	Und "ich auf den Knopf \"Login\" drücke"
end
