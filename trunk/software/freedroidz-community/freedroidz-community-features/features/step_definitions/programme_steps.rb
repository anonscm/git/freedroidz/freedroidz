# encoding: utf-8
begin require 'rspec/expectations'; rescue LoadError; require 'spec/expectations'; end
require 'cucumber/formatter/unicode'
require "rubygems"
require 'tempfile'

Wenn /^ein temporäres Programm anlege und dieses als Datei auswähle$/ do
	browser.speed = "5000"
	tf = Tempfile.open('cucumber_program_upload.java')
 	tf.puts "Inhalt meines Programms"
 	tf.rewind
 	feld_label = "Datei:"
 	xpath = "xpath=//input[@id=//label[contains(.,'#{feld_label}')]/@for]"
 	puts "XPATH: #{xpath}"
 	puts "TempFile: #{tf.path}"
# 	browser.type(xpath, tf.path)

	type_into_file_upload xpath, tf.path

#TEST
# 	xpath2 = "xpath=//input[@id=//label[contains(.,'Beschreibung')]/@for]"
# 	browser.type(xpath2, tf.path)
end

def type_into_file_upload(xpath, path)
	browser.windowFocus
	browser.focus xpath
	js = "if(selenium.browserbot.getCurrentWindow().clipboardData){window.clipboardData.setData('Text',' #{path}');};"
	browser.getEval js
	#do Ctrl+V
	browser.keyDownNative "17" #VK_CONTROL
	browser.keyPressNative "86" #V
	browser.keyUpNative "17" #VK_CONTROL
end