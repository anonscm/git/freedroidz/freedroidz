# encoding: utf-8
require "rubygems"
require "java"

# This little hackery makes sure that our step definitions
# can refer to 'browser' and will always access the same
# instance of the selenium client driver that is also use within
# the java step definitions. Do NOT store a direct reference to the driver!
# For some reason, cuke4duke re-initializes the spring application context
# right AFTER the Ruby-before-hooks are run, leaving your steps refering to an
# "old" instance. This leads to problems when you are mixing java and ruby step 
# definitions, because they will end up using uninitilized or simply 
# different browser sessions.

Before do
  java_import java.lang.System
  java_import "org.evolvis.freedroidz.community.SeleniumSingleton"
  def browser
    SeleniumSingleton.instance.selenium
  end
end

