package org.evolvis.freedroidz.community;

import java.awt.event.KeyEvent;

import org.springframework.beans.factory.annotation.Autowired;

import com.thoughtworks.selenium.DefaultSelenium;
import com.thoughtworks.selenium.Selenium;

import cuke4duke.spring.StepDefinitions;
import cuke4duke.annotation.After;
import cuke4duke.annotation.Before;
import cuke4duke.annotation.I18n.DE.Dann;
import cuke4duke.annotation.I18n.DE.*;

@StepDefinitions
public class GoogleSteps {

    @Autowired
    DefaultSelenium selenium;

    @Before
    public void starteSeleniumSession() {
	selenium.start();
    }

    @After
    public void beendeSeleniumSession() {
	selenium.stop();
    }

    @Angenommen("ich öffne Google")
    public void iCallAnotherStep() {

	selenium.open("http://www.google.de");
    }

    /*
     * Careful: there seems to be a bug in cuke4duke that will trigger if your expression starts with an optional group
     * like "(ich )?". If you encounter a situation where you want to do this, consider writing your step definition in
     * ruby.
     */
    @Angenommen("^suche nach \"([^\"]*)\"$")
    public void suche(String suchbegriff) {
	selenium.type("q", suchbegriff);
	//selenium.click("btnG");
	selenium.submit("f");
	selenium.waitForCondition("selenium.getTitle().indexOf(\"" + suchbegriff + "\")!=-1", "10000");
    }

    /*
     * this was moved to google_steps.rb to demonstrate that it is in fact quite feasible to mix step definitions in
     * different programming languages - even within a single scenario.
     * 
     * @param erwartetesErbgebnis
     */
    // @Dann("befindet sich \"(.+?)\" unter den Ergebnissen")
    public void prüfeErgebnis(String erwartetesErbgebnis) {
	;
	int vkControl = KeyEvent.VK_CONTROL;
    }
}
