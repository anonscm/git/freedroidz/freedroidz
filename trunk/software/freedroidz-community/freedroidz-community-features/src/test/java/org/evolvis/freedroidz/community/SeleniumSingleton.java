package org.evolvis.freedroidz.community;

import com.thoughtworks.selenium.Selenium;


/**
 * Singleton class for sharing a single selenium driver instance between java and ruby world.
 * <br><br>
 * 
 * I experimented with a couple of solutions and this one seemed to be the most reliable one.
 * Please let me know if you figure out a way to solve this in a more "Springy" manner.
 * 
 * @author lukas
 *
 */
public class SeleniumSingleton {
    private final static SeleniumSingleton instance= new SeleniumSingleton();
    private SeleniumSingleton(){
    }
    public static SeleniumSingleton getInstance(){
	return instance;
    }
    
    private Selenium selenium;
    
    public Selenium getSelenium() {
	if(selenium==null){
	    throw new RuntimeException("Selenium is not yet initialized. ");
	}
        return selenium;
    }
    public void setSelenium(Selenium selenium) {
        this.selenium = selenium;
    }
    

}
