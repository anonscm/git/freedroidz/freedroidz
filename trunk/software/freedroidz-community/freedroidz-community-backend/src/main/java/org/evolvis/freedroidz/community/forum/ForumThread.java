package org.evolvis.freedroidz.community.forum;

import org.evolvis.freedroidz.community.data.Attribute;
import org.evolvis.freedroidz.community.data.Entity;
import org.evolvis.freedroidz.community.data.Version;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class ForumThread implements Entity{

	@Id
	private long id;
	private String name;
	private long categorieId;
	
	@Override
	public Version getVersion() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Attribute
	public long getId() {
		return id;
	}

	public void setCategorieId(long categorieId) {
		this.categorieId = categorieId;
	}

	@Attribute
	public long getCategorieId() {
		return categorieId;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Attribute
	public String getName() {
		return name;
	}

	
}
