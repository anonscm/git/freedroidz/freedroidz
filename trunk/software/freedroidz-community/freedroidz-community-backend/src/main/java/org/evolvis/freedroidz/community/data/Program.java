package org.evolvis.freedroidz.community.data;

import org.springframework.data.annotation.Id;

public class Program implements Entity {
	public static final Version LATEST = new Version(0,2);

	
	@Id
	private long id;
	private String name;
	private String description;
	private String aufwand;
	private String difficulty;
	private String category;
	private String userId;
	private byte[] file;
		
	@Attribute(sinceVersion="0.2")
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	@Attribute
	public String getAufwand() {
		return aufwand;
	}
	public void setAufwand(String aufwand) {
		this.aufwand = aufwand;
	}
	
	@Attribute
	public String getDifficulty() {
		return difficulty;
	}
	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}
	@Attribute
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Attribute
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public Version getVersion() {
		return LATEST;
	}
	
	public void setFile(byte[] file) {
		this.file = file;
	}
	@Attribute
	public byte[] getFile() {
		return file;
	}
	public void setId(long id) {
		this.id = id;
	}
	@Attribute
	public long getId() {
		return id;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCategory() {
		return category;
	}
}
