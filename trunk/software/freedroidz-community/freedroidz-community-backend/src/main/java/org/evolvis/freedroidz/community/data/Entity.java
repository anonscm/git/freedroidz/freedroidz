package org.evolvis.freedroidz.community.data;

public interface Entity {
	@Attribute
	public Version getVersion();
}
