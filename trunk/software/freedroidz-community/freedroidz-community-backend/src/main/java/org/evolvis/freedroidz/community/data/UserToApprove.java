package org.evolvis.freedroidz.community.data;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class UserToApprove implements Entity {

private static final Version LATEST_VERSION = new Version(0, 2);
	
	@Id
	private String username;
	private String password;
	private String firstname;
	private String lastname;
	private String email;
	private String about;
	private byte[] image;
	private List<Group> groups;
	private List<String> friendList;
	private long approveKey;
	
	private Version version;
	
	public UserToApprove() {
		friendList = new ArrayList<String>();
	}
	
	public UserToApprove(String username) {
		this.username = username;
	}
	
	@Attribute
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	@Attribute
	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	@Attribute
	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	@Attribute
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Attribute(optional = true)
	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	@Attribute
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	@Attribute(optional = true)
	public Version getVersion() {
		
		return version != null ? version : LATEST_VERSION;
	}
	
	public void setVersion(Version version) {
		this.version = version;
	}
	
	public void setVersion(String version) {
		this.version = new Version(version);
	}

	public void setAbout(String about) {
		this.about = about;
	}

	@Attribute(sinceVersion = "0.2")
	public String getAbout() {
		return about;
	}
		
	@Override
	public String toString() {
		
		return username;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public byte[] getImage() {
		return image;
	}

	public void addFriend(String newFriend) {
		this.friendList.add(newFriend);
	}

	public void deleteFriend(String friend){
		this.friendList.remove(friend);
	}
	
	public List<String> getFriendList() {
		return friendList;
	}

	public void setApproveKey(long approveKey) {
		this.approveKey = approveKey;
	}

	public long getApproveKey() {
		return approveKey;
	}
}
