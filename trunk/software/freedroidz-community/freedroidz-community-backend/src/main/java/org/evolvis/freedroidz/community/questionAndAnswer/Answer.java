package org.evolvis.freedroidz.community.questionAndAnswer;

import java.util.Date;

import org.springframework.data.annotation.Id;

public class Answer {

	@Id
	private long ID;
	private String answer;
	private long questionID;
	private String author;
	private Date date;
	
	public void setID(long iD) {
		ID = iD;
	}
	public long getID() {
		return ID;
	}
	public void setQuestionID(long questionID) {
		this.questionID = questionID;
	}
	public long getQuestionID() {
		return questionID;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getAnswer() {
		return answer;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Date getDate() {
		return date;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getAuthor() {
		return author;
	}
}
