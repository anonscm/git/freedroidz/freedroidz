package org.evolvis.freedroidz.community.forum;

import java.util.Date;

import org.evolvis.freedroidz.community.data.Attribute;
import org.evolvis.freedroidz.community.data.Entity;
import org.evolvis.freedroidz.community.data.Version;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class ForumMessage implements Entity{

	@Id
	private long id;
	private String message;
	private String userId;
	private long threadId;
	private Date date;
	
	@Override
	public Version getVersion() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Attribute
	public long getId() {
		return id;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Attribute
	public String getMessage() {
		return message;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Attribute
	public String getUserId() {
		return userId;
	}

	public void setThreadId(long threadId) {
		this.threadId = threadId;
	}

	@Attribute
	public long getThreadId() {
		return threadId;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getDate() {
		return date;
	}	
}
