package org.evolvis.freedroidz.community.email;


public enum MailAccount {

	FREEDROIDZ("smtp.gmail.com", 25, "community.webapp", "1590mt!2580", "community.webapp@googlemail.com");
	
	private String smtpHost;
	private int port;
	private String username;
	private String password;
	private String email;
	
	private MailAccount(String smtpHost, int port, String username, String password, String email) {
		this.smtpHost = smtpHost;
		this.port = port;
		this.username = username;
		this.password = password;
		this.email = email;
	}
	
	public int getPort(){
		return port;
	}
	
	public String getSmtpHost(){
		return smtpHost;
	}
	
	public MailAuthenticator getPasswordAuthentication(){
		return new MailAuthenticator(username, password);
	}
	
	public String getEmail(){
		return email;
	}
}
