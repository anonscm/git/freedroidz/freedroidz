package org.evolvis.freedroidz.community.data;

public class Group implements Entity{
	private String name;

	public Group() {
		
	}
	
	public Group(String name) {
		this.name = name;
	}
	
	@Attribute
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public Version getVersion() {
		return null;
	}
}
