package org.evolvis.freedroidz.community.questionAndAnswer;

public class Notification {

	private String username;
	private long questionId;
	
	public Notification(String username, long questionId){
		this.username = username;
		this.questionId = questionId;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUsername() {
		return username;
	}
	
	public void setQuestionId(long questionId) {
		this.questionId = questionId;
	}
	public long getQuestionId() {
		return questionId;
	}
}
