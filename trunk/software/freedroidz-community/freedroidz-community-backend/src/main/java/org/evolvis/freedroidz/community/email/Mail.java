package org.evolvis.freedroidz.community.email;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.evolvis.freedroidz.community.data.User;
import org.evolvis.freedroidz.community.data.UserToApprove;
import org.evolvis.freedroidz.community.questionAndAnswer.Question;

public class Mail {
	public static void send(MailAccount acc, String recipient, String subject,
			String text)throws AddressException, javax.mail.MessagingException{
		
		Properties properties = System.getProperties();		
		properties.setProperty("mail.smtp.host", acc.getSmtpHost());		
		properties.setProperty("mail.smtp.port", String.valueOf(acc.getPort()));
		properties.setProperty("mail.smtp.auth", "true");
		properties.setProperty("mail.smtp.starttls.enable", "true");
		
		Session session = Session.getDefaultInstance(properties, acc.getPasswordAuthentication());
		
		MimeMessage msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(acc.getEmail()));
		msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient, false));
		msg.setSubject(subject);
		msg.setText(text);
		Transport.send(msg);
	}
	
	public void sendNotification(User user, Question question) throws AddressException, MessagingException{
		MailAccount acc = MailAccount.FREEDROIDZ;
		String subject = "freedroidz-community: Antwort auf deine Frage";
		String text = getNotificationEmail(user.getUsername(), question.getID(), question.getTitle());
		
		Properties properties = System.getProperties();		
		properties.setProperty("mail.smtp.host", acc.getSmtpHost());		
		properties.setProperty("mail.smtp.port", String.valueOf(acc.getPort()));
		properties.setProperty("mail.smtp.auth", "true");
		properties.setProperty("mail.smtp.starttls.enable", "true");
		
		Session session = Session.getDefaultInstance(properties, acc.getPasswordAuthentication());
		
		MimeMessage msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(acc.getEmail()));
		msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(user.getEmail(), false));
		msg.setSubject(subject);
		msg.setText(text);
		Transport.send(msg);
	}
	
	private String getNotificationEmail(String recipient, long questionId, String title){
		
		InputStream is = getClass().getResourceAsStream("/emails/Notification.txt");
		
		String fileAsString = "";
		try{
			int counter = 0;
			InputStreamReader inputStreamReader = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(inputStreamReader);
			
			while (br.ready()) {
				fileAsString += br.readLine() + "\n";
				counter++;
			}			
		}catch(IOException ioe){
			ioe.printStackTrace();
		}
		
		fileAsString = fileAsString.replace("${recipient}", recipient);
		fileAsString = fileAsString.replace("${title}", title);
		fileAsString = fileAsString.replace("${linkToQuestion}", "http://localhost:8080/freedroidz-community-webapp/FrageUndAntwort/Frage?qId=" + questionId);
		
		return fileAsString;
	}
	
	public void sendApproveMail(UserToApprove user) throws AddressException, MessagingException{
		MailAccount acc = MailAccount.FREEDROIDZ;
		String subject = "freedroidz-community: E-Mail Bestätigung";
		String text = getApproveEmail(user);
		
		Properties properties = System.getProperties();		
		properties.setProperty("mail.smtp.host", acc.getSmtpHost());		
		properties.setProperty("mail.smtp.port", String.valueOf(acc.getPort()));
		properties.setProperty("mail.smtp.auth", "true");
		properties.setProperty("mail.smtp.starttls.enable", "true");
		
		Session session = Session.getDefaultInstance(properties, acc.getPasswordAuthentication());
		
		MimeMessage msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(acc.getEmail()));
		msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(user.getEmail(), false));
		msg.setSubject(subject);
		msg.setText(text);
		Transport.send(msg);
	}
	
	public String getApproveEmail(UserToApprove user){
		
		InputStream is = getClass().getResourceAsStream("/emails/Registration.txt");
		
		String fileAsString = "";
		try{
			int counter = 0;
			InputStreamReader inputStreamReader = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(inputStreamReader);
			
			while (br.ready()) {
				fileAsString += br.readLine() + "\n";
				counter++;
			}			
		}catch(IOException ioe){
			ioe.printStackTrace();
		}
		
		fileAsString = fileAsString.replace("${recipient}", user.getUsername());
		fileAsString = fileAsString.replace("${linkToApprove}", "http://localhost:8080/freedroidz-community-webapp/Benutzer/Bestätigen?approveKey=" + user.getApproveKey());
		
		return fileAsString;
	}
}
