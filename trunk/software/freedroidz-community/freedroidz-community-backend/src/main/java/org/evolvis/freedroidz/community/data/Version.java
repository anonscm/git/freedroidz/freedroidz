package org.evolvis.freedroidz.community.data;

public class Version implements Comparable<Version> {

	private final int second;
	private final int first;

	public Version(int first, int second) {
		this.first = first;
		this.second = second;
	}

	public Version(String v) {
		try {
			String[] split = v.split("\\.");

			first = Integer.valueOf(split[0]);
			second = Integer.valueOf(split[1]);
			if (split.length != 2) {
				throw new IllegalArgumentException("Version-String may only be x.y");
			}

		} catch (Exception e) {
			throw new IllegalArgumentException("not a valid Version-String");
		}

	}



	@Override
	public String toString() {
		return first + "." + second;
	}

	@Override
	public int compareTo(Version o) {
		if (o.first > first) {
			return -1;
		} else if (o.first < first) {
			return 1;
		} else {
			if (o.second > second) {
				return -1;
			} else if (o.second < second) {
				return 1;
			} else {
				return 0;
			}
		}
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Version) {
			Version v = (Version) o;
			if (v.first == first && v.second == second) {
				return true;
			} else {
				return false;
			}
		} else if (o instanceof String) {
			String s = (String) o;
			return s.equals(toString());
		}

		return false;
	}

	public boolean isHigherThan(Version entityVersion) {
		return compareTo(entityVersion) > 0;
	}

}
