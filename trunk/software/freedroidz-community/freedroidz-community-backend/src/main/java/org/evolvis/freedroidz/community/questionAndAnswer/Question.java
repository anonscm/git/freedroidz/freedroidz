package org.evolvis.freedroidz.community.questionAndAnswer;

import java.util.Date;

import org.springframework.data.annotation.Id;

public class Question {
	@Id
	private long ID;
	private String title;
	private String question;
	private String author;
	private Date date;
	private String categorie;
	private int numberOfAnwsers;
	private boolean isOpen;
	
	public void setID(long iD) {
		ID = iD;
	}
	public long getID() {
		return ID;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getQuestion() {
		return question;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getAuthor() {
		return author;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Date getDate() {
		return date;
	}
	public void setNumberOfAnwsers(int numberOfAnwsers) {
		this.numberOfAnwsers = numberOfAnwsers;
	}
	public int getNumberOfAnwsers() {
		return numberOfAnwsers;
	}
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}
	public String getCategorie() {
		return categorie;
	}
	public void setIsOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}
	public boolean getIsOpen() {
		return isOpen;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitle() {
		return title;
	}
}
