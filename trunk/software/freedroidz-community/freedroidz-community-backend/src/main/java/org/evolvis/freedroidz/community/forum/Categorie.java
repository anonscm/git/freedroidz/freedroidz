package org.evolvis.freedroidz.community.forum;

import org.evolvis.freedroidz.community.data.Attribute;
import org.evolvis.freedroidz.community.data.Entity;
import org.evolvis.freedroidz.community.data.Version;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Categorie implements Entity{
	
	@Id
	private long id;
	private String name;
	
	private Version version;
	
	public Categorie(){
		
	}
	
	@Override
	public Version getVersion() {
	
		return version;
	}

	public void setID(long iD) {
		id = iD;
	}
	
	@Attribute
	public long getID() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Attribute
	public String getName() {
		return name;
	}

}
