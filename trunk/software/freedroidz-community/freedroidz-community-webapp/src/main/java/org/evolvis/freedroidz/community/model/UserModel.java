package org.evolvis.freedroidz.community.model;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.io.IOUtils;
import org.evolvis.freedroidz.community.data.User;
import org.evolvis.freedroidz.community.data.UserToApprove;
import org.evolvis.freedroidz.community.security.StringCipher;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class UserModel {

	private String username;
	private String currentPassword;
	private String newPassword;
	private String newPassword2;
	private String firstname;
	private String lastname;
	private String email;
	private String about;
	private CommonsMultipartFile image;
	
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUsername() {
		return username;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmail() {
		return email;
	}
	public void setAbout(String about) {
		this.about = about;
	}
	public String getAbout() {
		return about;
	}
	
	public UserToApprove getUserToApprove(UserModel userModel) throws NoSuchAlgorithmException, IOException{
		UserToApprove user = new UserToApprove();
		user.setUsername(userModel.getUsername());
		user.setPassword(StringCipher.stringSipher(userModel.getNewPassword()));
		user.setFirstname(userModel.getFirstname());
		user.setLastname(userModel.getLastname());
		user.setEmail(userModel.getEmail());
		user.setAbout(userModel.getAbout());
		
		/*
		 * commonsfile zu byte[]
		 */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		IOUtils.copy(image.getInputStream(), out);
		user.setImage(image.getBytes());
		
		return user;
	}
	
	public User getUser(UserModel userModel) throws IOException, NoSuchAlgorithmException{
		
		User user = new User();
		user.setUsername(userModel.getUsername());
		user.setPassword(StringCipher.stringSipher(userModel.getNewPassword()));
		user.setFirstname(userModel.getFirstname());
		user.setLastname(userModel.getLastname());
		user.setEmail(userModel.getEmail());
		user.setAbout(userModel.getAbout());
		
		/*
		 * commonsfile zu byte[]
		 */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		IOUtils.copy(image.getInputStream(), out);
		user.setImage(image.getBytes());
		
		return user;
	}
	
	public static UserModel userToUserModel(User user){
		
		UserModel userModel = new UserModel();

		userModel.setUsername(user.getUsername());
		userModel.setCurrentPassword("");
		userModel.setNewPassword("");
		userModel.setNewPassword2("");
		userModel.setFirstname(user.getFirstname());
		userModel.setLastname(user.getLastname());
		userModel.setEmail(user.getEmail());
		userModel.setAbout(user.getAbout());
//		userModel.setImage(user.getImage());
		userModel.setImage(null);
		
		return userModel;
	}
	
	@Override
	public String toString() {
		
		return username;
	}
	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}
	public String getCurrentPassword() {
		return currentPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword2(String newPassword2) {
		this.newPassword2 = newPassword2;
	}
	public String getNewPassword2() {
		return newPassword2;
	}
	public void setImage(CommonsMultipartFile image) {
		this.image = image;
	}
	public CommonsMultipartFile getImage() {
		return image;
	}
}
