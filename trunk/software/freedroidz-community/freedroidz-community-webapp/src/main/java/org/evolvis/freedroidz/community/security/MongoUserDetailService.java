package org.evolvis.freedroidz.community.security;

import java.util.Collection;
import java.util.HashSet;

import javax.inject.Inject;

import org.evolvis.freedroidz.community.data.User;
import org.springframework.dao.DataAccessException;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class MongoUserDetailService implements UserDetailsService {

	@Inject
	private MongoOperations mongoOperations;
	
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException, DataAccessException {

		if(!mongoOperations.collectionExists(User.class)){
			mongoOperations.createCollection(User.class);
		}
		
		User user = mongoOperations.findById(username, User.class);
		
		if (user == null) {
			return null;
		}

		Collection<GrantedAuthorityImpl> authorities = new HashSet<GrantedAuthorityImpl>();
		authorities.add(new GrantedAuthorityImpl("ROLE_USER"));

		org.springframework.security.core.userdetails.User springUser = new org.springframework.security.core.userdetails.User(
				user.getUsername(), user.getPassword(), true, true, true, true,
				authorities);

		return springUser;
	}
}
