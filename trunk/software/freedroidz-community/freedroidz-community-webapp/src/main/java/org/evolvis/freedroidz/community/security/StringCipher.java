package org.evolvis.freedroidz.community.security;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class StringCipher {

	/**
	 * MD5 cipher
	 * @param s The String, that shall be cipher.
	 * @return	The ciphered String.
	 * @throws NoSuchAlgorithmException
	 * @throws UnsupportedEncodingException 
	 */
	public static String stringSipher(String s) throws NoSuchAlgorithmException, UnsupportedEncodingException{
		MessageDigest digest = MessageDigest.getInstance("MD5");	//Verschlüsselung
		digest.update(s.getBytes("UTF-8"));	//Bytes des Strings im Digest speichern.
		byte[] digestAsBytes = digest.digest();	//Digest als Bytes speichern.
		String digestAsString = "";
		int low, hi;	//Für die 4 ersten und 4 letzten bits eines bytes.
		
		for(int i = 0; i < digestAsBytes.length; i++){
			low = digestAsBytes[i] & 0x0f;	//nur die ersten 4 bits
			hi = (digestAsBytes[i] & 0xf0) >> 4;	//nur die zweiten 4 bits
		
			digestAsString += Integer.toHexString(hi);	//im String speichern
			digestAsString += Integer.toHexString(low);	//s.o.
		}
		
		return digestAsString;
	}
}
