package org.evolvis.freedroidz.community.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/login")
public class IndexController {
	
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView index(@RequestParam(value="failed", required = false) Boolean loginFailed) {
		final ModelAndView modelAndView = new ModelAndView("index");
		
		if(loginFailed != null && loginFailed){	//Wenn der Login fehlgeschlagen ist.
			modelAndView.addObject("errorMessage", "Deine angegebenen Daten sind leider inkorrekt.<br />Versuche es noch einmal.");
		}

		return modelAndView;
	}
}
