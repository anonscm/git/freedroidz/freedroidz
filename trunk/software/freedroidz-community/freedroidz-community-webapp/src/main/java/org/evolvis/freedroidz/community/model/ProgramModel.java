package org.evolvis.freedroidz.community.model;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class ProgramModel {

	private CommonsMultipartFile sourceCode;
	private String name;
	private String description;
	private String difficulty;
	private String category;
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}

	public String getAufwand() {
		return aufwand;
	}

	public void setAufwand(String aufwand) {
		this.aufwand = aufwand;
	}

	private String aufwand;
	

	public void setSourceCode(CommonsMultipartFile sourceCode) {
		this.sourceCode = sourceCode;
	}

	public CommonsMultipartFile getSourceCode() {
		return sourceCode;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCategory() {
		return category;
	}
}
