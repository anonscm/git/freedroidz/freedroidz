package org.evolvis.freedroidz.community.controller;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.evolvis.freedroidz.community.data.Program;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


//@Controller
//@RequestMapping(value ="/Testseite")
public class TestseiteController {

	@Inject
	private MongoOperations mongoOperations;
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView testseite(HttpServletRequest request){
		ModelAndView mav = new ModelAndView("/testseite");
		
		int limit = 1;
		int aktuelleSeite = Integer.parseInt(request.getParameter("aktuelleSeite"));
		String sortBy = request.getParameter("sortBy");
		String value = request.getParameter("value");
		value = null;
		
		int anzahlElemente = mongoOperations.find(new Query(Criteria.where(sortBy).is(value)), Program.class).size();
		int anzahlSeiten = anzahlElemente / limit;
		if((anzahlElemente % limit) > 0){
			anzahlSeiten ++;
		}
		int offset = aktuelleSeite*limit-limit;
		
		List<Program> programList = mongoOperations.find(new Query(Criteria.where(sortBy).is(value)).skip(offset).limit(limit), Program.class);

		mav.addObject("programme", programList);

		//untere Leiste
		String firstPage = "";
		if(aktuelleSeite != 1){
			firstPage = "<a href=\"/freedroidz-community-webapp/Testseite?aktuelleSeite=1&sortBy=&value=\"><<</a>";
		}
		
		String lastPage = "";
		if(aktuelleSeite != anzahlSeiten){
			lastPage = "<a href=\"/freedroidz-community-webapp/Testseite?aktuelleSeite=" + anzahlSeiten + "&sortBy=&value=\">>></a>";
		}
		
		String previousPage = "";
		if(aktuelleSeite != 1){
			previousPage = "<a href=\"/freedroidz-community-webapp/Testseite?aktuelleSeite=" + (aktuelleSeite-1) + "&sortBy=&value=\"><</a>";
		}
		
		String nextPage = "";
		if(aktuelleSeite != anzahlSeiten){
			nextPage = "<a href=\"/freedroidz-community-webapp/Testseite?aktuelleSeite=" + (aktuelleSeite+1) + "&sortBy=&value=\">></a>";
		}
		
		String leiste = firstPage + " " + previousPage + " Seite " + aktuelleSeite + " von " + anzahlSeiten + " " + nextPage + " " + lastPage;
		
		mav.addObject("leiste", leiste);
		
		return mav;
	}
}
