package org.evolvis.freedroidz.community.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.mail.internet.AddressException;
import javax.servlet.http.HttpServletRequest;

import org.evolvis.freedroidz.community.data.User;
import org.evolvis.freedroidz.community.email.Mail;
import org.evolvis.freedroidz.community.questionAndAnswer.Answer;
import org.evolvis.freedroidz.community.questionAndAnswer.Notification;
import org.evolvis.freedroidz.community.questionAndAnswer.Question;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Order;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value ="/FrageUndAntwort")
public class QuestionAndAnswerController {

	@Inject
	private MongoOperations mongoOperations;
	
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showForumMainPage(Principal principal, HttpServletRequest request){
		ModelAndView mav = new ModelAndView("FrageUndAntwort/FUAMainPage");

		if(principal == null){
			mav = new ModelAndView("FrageUndAntwort/Unregistriert/FUAMainPage");
		}
		
		int limit = 20;
		int aktuelleSeite = 1;
		if(request.getParameter("actuallyPage") != null)
			aktuelleSeite = Integer.parseInt(request.getParameter("actuallyPage"));
		int anzahlElemente = mongoOperations.findAll(Question.class).size();
		
		int anzahlSeiten = anzahlElemente / limit;
		if((anzahlElemente % limit) > 0){
			anzahlSeiten ++;
		}
		int offset = aktuelleSeite*limit-limit;
		
		String firstPage = "";
		String lastPage = "";
		String previousPage = "";
		String nextPage = "";
		
		/*testserver
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			firstPage = "<a href=\"/freedroidz-community-webapp/FrageUndAntwort?actuallyPage=1\"><<</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			lastPage = "<a href=\"/freedroidz-community-webapp/FrageUndAntwort?actuallyPage=" + anzahlSeiten + "\">>></a>";
		}
		
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			previousPage = "<a href=\"/freedroidz-community-webapp/FrageUndAntwort?actuallyPage=" + (aktuelleSeite-1) + "\"><</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			nextPage = "<a href=\"/freedroidz-community-webapp/FrageUndAntwort?actuallyPage=" + (aktuelleSeite+1) + "\">></a>";
		}*/
		
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			firstPage = "<a href=\"/FrageUndAntwort?actuallyPage=1\"><<</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			lastPage = "<a href=\"/FrageUndAntwort?actuallyPage=" + anzahlSeiten + "\">>></a>";
		}
		
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			previousPage = "<a href=\"/FrageUndAntwort?actuallyPage=" + (aktuelleSeite-1) + "\"><</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			nextPage = "<a href=\"/FrageUndAntwort?actuallyPage=" + (aktuelleSeite+1) + "\">></a>";
		}
		
		String auswahlLeiste = firstPage + " " + previousPage + " Seite " + aktuelleSeite + " von " + anzahlSeiten + " " + nextPage + " " + lastPage;
		
		mav.addObject("auswahlLeiste", auswahlLeiste);
		
		Query q = new Query();
		q.sort().on("date", Order.DESCENDING);	
		List<Question> qList = mongoOperations.find(q.skip(offset).limit(limit), Question.class);
		
		mav.addObject("questions", qList);
		
		if(principal != null){
			mav.addObject("unreadAnswers", getUnreadAnswers(mongoOperations.findById(principal.getName(), User.class)));
		}
		
		return mav;
	}

	@RequestMapping(value = "/Offen")
	public ModelAndView offeneZeigen(Principal principal, HttpServletRequest request){
		
		ModelAndView modelAndView = new ModelAndView("FrageUndAntwort/FUAMainPage");
		
		if(principal == null){
			modelAndView = new ModelAndView("FrageUndAntwort/Unregistriert/FUAMainPage");
		}
		
		int limit = 20;
		int aktuelleSeite = 1;
		if(request.getParameter("actuallyPage") != null)
			aktuelleSeite = Integer.parseInt(request.getParameter("actuallyPage"));
		int anzahlElemente = mongoOperations.find(new Query(Criteria.where("isOpen").is(true)), Question.class).size();
		
		int anzahlSeiten = anzahlElemente / limit;
		if((anzahlElemente % limit) > 0){
			anzahlSeiten ++;
		}
		int offset = aktuelleSeite*limit-limit;
		
		String firstPage = "";
		String lastPage = "";
		String previousPage = "";
		String nextPage = "";
		
		/*testserver
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			firstPage = "<a href=\"/freedroidz-community-webapp/FrageUndAntwort/Offen?actuallyPage=1\"><<</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			lastPage = "<a href=\"/freedroidz-community-webapp/FrageUndAntwort/Offen?actuallyPage=" + anzahlSeiten + "\">>></a>";
		}
		
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			previousPage = "<a href=\"/freedroidz-community-webapp/FrageUndAntwort/Offen?actuallyPage=" + (aktuelleSeite-1) + "\"><</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			nextPage = "<a href=\"/freedroidz-community-webapp/FrageUndAntwort/Offen?actuallyPage=" + (aktuelleSeite+1) + "\">></a>";
		}*/
		
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			firstPage = "<a href=\"/FrageUndAntwort/Offen?actuallyPage=1\"><<</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			lastPage = "<a href=\"/FrageUndAntwort/Offen?actuallyPage=" + anzahlSeiten + "\">>></a>";
		}
		
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			previousPage = "<a href=\"/FrageUndAntwort/Offen?actuallyPage=" + (aktuelleSeite-1) + "\"><</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			nextPage = "<a href=\"/FrageUndAntwort/Offen?actuallyPage=" + (aktuelleSeite+1) + "\">></a>";
		}
		
		String auswahlLeiste = firstPage + " " + previousPage + " Seite " + aktuelleSeite + " von " + anzahlSeiten + " " + nextPage + " " + lastPage;
		
		modelAndView.addObject("auswahlLeiste", auswahlLeiste);
		
		Query q = new Query(Criteria.where("isOpen").is(true));
		q.sort().on("date", Order.DESCENDING);
		List<Question> qList = mongoOperations.find(q.skip(offset).limit(limit), Question.class);
		
		modelAndView.addObject("questions", qList);
		
		if(principal != null)
			modelAndView.addObject("unreadAnswers", getUnreadAnswers(mongoOperations.findById(principal.getName(), User.class)));
		
		return modelAndView;
	}

	@RequestMapping(value = "/Beantwortet")
	public ModelAndView beantwortetZeigen(Principal principal, HttpServletRequest request){
		
		ModelAndView modelAndView = new ModelAndView("FrageUndAntwort/FUAMainPage");
		
		if(principal == null){
			modelAndView = new ModelAndView("FrageUndAntwort/Unregistriert/FUAMainPage");
		}
		
		int limit = 20;
		int aktuelleSeite = 1;
		if(request.getParameter("actuallyPage") != null)
			aktuelleSeite = Integer.parseInt(request.getParameter("actuallyPage"));
		int anzahlElemente = mongoOperations.find(new Query(Criteria.where("isOpen").is(false)), Question.class).size();
		
		int anzahlSeiten = anzahlElemente / limit;
		if((anzahlElemente % limit) > 0){
			anzahlSeiten ++;
		}
		int offset = aktuelleSeite*limit-limit;
		
		String firstPage = "";
		String lastPage = "";
		String previousPage = "";
		String nextPage = "";
		
		/*testserver
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			firstPage = "<a href=\"/freedroidz-community-webapp/FrageUndAntwort/Beantwortet?actuallyPage=1\"><<</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			lastPage = "<a href=\"/freedroidz-community-webapp/FrageUndAntwort/Beantwortet?actuallyPage=" + anzahlSeiten + "\">>></a>";
		}
		
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			previousPage = "<a href=\"/freedroidz-community-webapp/FrageUndAntwort/Beantwortet?actuallyPage=" + (aktuelleSeite-1) + "\"><</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			nextPage = "<a href=\"/freedroidz-community-webapp/FrageUndAntwort/Beantwortet?actuallyPage=" + (aktuelleSeite+1) + "\">></a>";
		}*/

		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			firstPage = "<a href=\"/FrageUndAntwort/Beantwortet?actuallyPage=1\"><<</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			lastPage = "<a href=\"/FrageUndAntwort/Beantwortet?actuallyPage=" + anzahlSeiten + "\">>></a>";
		}
		
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			previousPage = "<a href=\"/FrageUndAntwort/Beantwortet?actuallyPage=" + (aktuelleSeite-1) + "\"><</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			nextPage = "<a href=\"/FrageUndAntwort/Beantwortet?actuallyPage=" + (aktuelleSeite+1) + "\">></a>";
		}
		
		String auswahlLeiste = firstPage + " " + previousPage + " Seite " + aktuelleSeite + " von " + anzahlSeiten + " " + nextPage + " " + lastPage;
		
		modelAndView.addObject("auswahlLeiste", auswahlLeiste);
		
		Query q = new Query(Criteria.where("isOpen").is(false));
		q.sort().on("date", Order.DESCENDING);
		List<Question> qList = mongoOperations.find(q.skip(offset).limit(limit), Question.class);		
		
		modelAndView.addObject("questions", qList);
		
		if(principal != null)
			modelAndView.addObject("unreadAnswers", getUnreadAnswers(mongoOperations.findById(principal.getName(), User.class)));
		
		return modelAndView;
	}
	
	@RequestMapping(value="/NeueFrage", method = RequestMethod.GET)
	public ModelAndView newQuestion(Principal principal){
		ModelAndView mav = new ModelAndView("FrageUndAntwort/NeueFrage");
		mav.addObject("unreadAnswers", getUnreadAnswers(mongoOperations.findById(principal.getName(), User.class)));
		
		return mav;
	}
	
	@RequestMapping(value="/NeueFrage", method = RequestMethod.POST)
	public ModelAndView newQuestion(Principal principal, HttpServletRequest request){
		
		ModelAndView mav;
		
		//Alles einzeln behandeln
		if(!request.getParameter("questionTitle").equals("")){
			if(!request.getParameter("question").equals("")){
				if(!request.getParameter("categorie").equals("- Bitte auswählen -")){
					
					Question q = new Question();
					q.setAuthor(principal.getName());
					q.setID(getQuestionId());
					q.setCategorie(request.getParameter("categorie"));
					q.setDate(new Date());
					q.setNumberOfAnwsers(0);
					q.setIsOpen(true);
					q.setQuestion(request.getParameter("question").replaceAll("<", "?lt;"));
					q.setQuestion(q.getQuestion().replaceAll(">", "?gt;"));
					q.setQuestion(q.getQuestion().replaceAll("\n", "<br />"));
					q.setTitle(request.getParameter("questionTitle"));
					
					mongoOperations.insert(q);
					mav = new ModelAndView("redirect:/FrageUndAntwort/Frage?qId=" + q.getID());
				}else{
					mav = new ModelAndView("/FrageUndAntwort/NeueFrage");
					mav.addObject("errorMessage", "Bitte wähle eine Kategorie aus.");
				}
			}else{
				mav = new ModelAndView("/FrageUndAntwort/NeueFrage");
				mav.addObject("errorMessage", "Bitte gib eine Frage ein.");
			}
		}else{
			mav = new ModelAndView("/FrageUndAntwort/NeueFrage");
			mav.addObject("errorMessage", "Bitte gib einen Titel ein.");
		}
		mav.addObject("unreadAnswers", getUnreadAnswers(mongoOperations.findById(principal.getName(), User.class)));
		
		return mav;
	}
	
	private long getQuestionId(){
		
		long ID = 0;
		List<Question> list = mongoOperations.findAll(Question.class);
		List<Long> IDs = new ArrayList<Long>();
		
		for(Question q :list){
			IDs.add(q.getID());
		}
		
		try{
			while(IDs.contains(ID)){
				ID++;
			}
		}
		catch(Exception except){
			System.out.println(except.getMessage());
		}

		return ID;
	}
	
	@RequestMapping(value = "/Frage")
	public ModelAndView showFrage(HttpServletRequest request, Principal principal){
		ModelAndView mav = new ModelAndView("/FrageUndAntwort/Frage");
		
		if(principal == null){
			mav = new ModelAndView("FrageUndAntwort/Unregistriert/Frage");
		}
		
		long id = Long.parseLong(request.getParameter("qId"));
		Question q = mongoOperations.findById(id, Question.class);
		
		if(q != null){
			List<Answer> answerList = mongoOperations.find(new Query(Criteria.where("questionID").is(id)), Answer.class);
			
			User currentUser = null;
			if(principal != null){
				currentUser = mongoOperations.findById(principal.getName(), User.class);
			
				//Löscht die Notification, damit wieder bescheid gegeben werden kann, dass jemand geantwortet hat.
				if(!mongoOperations.find(new Query(Criteria.where("username").is(currentUser.getUsername()).and("questionId").is(q.getID())), Notification.class).isEmpty()){
					mongoOperations.remove(new Query(Criteria.where("username").is(currentUser.getUsername()).and("questionId").is(q.getID())), Notification.class);
				}
			}
			mav.addObject("question", q);
			mav.addObject("answers", answerList);
			
			if(principal != null){
				mav.addObject("userIsOwnerAndIsOpen", q.getAuthor().equals(currentUser.getUsername()) && q.getIsOpen());
				mav.addObject("unreadAnswers", getUnreadAnswers(mongoOperations.findById(principal.getName(), User.class)));
			}
		}else{
			if(principal != null){
				mav = new ModelAndView("FrageUndAntwort/FrageNichtVorhanden");
				mav.addObject("unreadAnswers", getUnreadAnswers(mongoOperations.findById(principal.getName(), User.class)));
			}else{
				mav = new ModelAndView("FrageUndAntwort/Unregistriert/FrageNichtVorhanden");
			}
		}
		return mav;
	}

	@RequestMapping(value="/Antwort", method= RequestMethod.POST)
	public ModelAndView antworten(Principal principal, HttpServletRequest request) throws AddressException, javax.mail.MessagingException{
		
		Long qId = Long.parseLong(request.getParameter("qId"));	//ID der Frage

		String answerAsString = request.getParameter("answer");
		String answerAuthor = principal.getName();
		
		ModelAndView mav = new ModelAndView("redirect:/FrageUndAntwort/Frage?qId=" + qId);
		
		if(!answerAsString.equals("")){
			
			//Neue Antwort
			Answer answer = new Answer();
			answer.setAnswer(answerAsString.replaceAll("<", "?lt;"));
			answer.setAnswer(answer.getAnswer().replaceAll(">", "?gt;"));
			answer.setAnswer(answer.getAnswer().replaceAll("\n", "<br />"));		
			answer.setAuthor(answerAuthor);
			answer.setDate(new Date());
			answer.setID(getAnswerId());
			answer.setQuestionID(qId);
			mongoOperations.insert(answer);
			
			//Update der Frage
			Question question = mongoOperations.findById(qId, Question.class);
			mongoOperations.remove(question);
			question.setNumberOfAnwsers(question.getNumberOfAnwsers()+1);
			
			mongoOperations.insert(question);
			
			if(mongoOperations.find(new Query(Criteria.where("username").is(question.getAuthor()).and("questionId").is(question.getID())), Notification.class).isEmpty()){
				User user = mongoOperations.findById(question.getAuthor(), User.class);
				
				//Notification in DB
				Notification questionNotification = new Notification(question.getAuthor(), question.getID());
				mongoOperations.insert(questionNotification);
				
				if(!user.getEmail().equals("")){
					//Send Email
					Mail mail = new Mail();
					mail.sendNotification(user, question);
				}
			}
			
			//Alle User, die geantwortet haben.
			List<Answer> answerList = mongoOperations.find(new Query(Criteria.where("questionID").is(question.getID())), Answer.class);
			List<String> usernames = new ArrayList<String>();
			for(Answer a: answerList){
				usernames.add(a.getAuthor());
			}
			List<User> answererList = mongoOperations.find(new Query(Criteria.where("username").in(usernames)), User.class);
			
			//Genauso behandeln, wie den Fragesteller
			for(User u: answererList){
				if(mongoOperations.find(new Query(Criteria.where("username").is(u.getUsername()).and("questionId").is(question.getID())), Notification.class).isEmpty()){
					
					//Notification in DB
					Notification questionNotification = new Notification(u.getUsername(), question.getID());
					mongoOperations.insert(questionNotification);
					
					if(!u.getEmail().equals("")){
						//Send Email
						Mail mail = new Mail();
						mail.sendNotification(u, question);
					}
				}
			}
			
//			Query q = new Query(Criteria.where("ID").is(qId));
//			Update u = new Update();	//Funktioniert anscheinend nciht, oder ich hab was falsch gemacht...
//			u.set("numberOfAnwsers", question.getNumberOfAnwsers()+1);
//			mongoOperations.updateFirst(q, u, Question.class);
		}else{
			mav = new ModelAndView("/FrageUndAntwort/Frage");
			mav.addObject("qId", qId);
			mav.addObject("errorMessage", "Du muss eine Antwort schreiben, um antworten zu können.");
		}
		mav.addObject("unreadAnswers", getUnreadAnswers(mongoOperations.findById(principal.getName(), User.class)));
		
		return mav;
	}
	
	private long getAnswerId(){
		
		long ID = 0;
		List<Answer> list = mongoOperations.findAll(Answer.class);
		List<Long> IDs = new ArrayList<Long>();
		
		for(Answer a :list){
			IDs.add(a.getID());
		}
		
		try{
			while(IDs.contains(ID)){
				ID++;
			}
		}
		catch(Exception except){
			System.out.println(except.getMessage());
		}

		return ID;
	}
	
	@RequestMapping(value="/Schliessen", method = RequestMethod.POST)
	public ModelAndView schliessen(HttpServletRequest request, Principal principal){
		
		Long qId = Long.parseLong(request.getParameter("qId"));
		User user = mongoOperations.findById(principal.getName(), User.class);
		Question question = mongoOperations.findById(qId, Question.class);

		ModelAndView mav = new ModelAndView("redirect:/FrageUndAntwort/Frage?qId=" + qId);
		
		if(question.getAuthor().equals(user.getUsername())){
			Update update = new Update();
			update.set("isOpen", false);
			Query query = new Query(Criteria.where("ID").is(question.getID()));
			
			mongoOperations.updateFirst(query, update, Question.class);
		}else{
			mav = new ModelAndView("/FrageUndAntwort/Frage?qId=" + question.getID());
			mav.addObject("errorMessage", "Sie sind nicht berechtigt diese Frage zu schließen.");
		}
		mav.addObject("unreadAnswers", getUnreadAnswers(mongoOperations.findById(principal.getName(), User.class)));
		
		return mav;
	}
	
	@RequestMapping(value="/Suche", method = RequestMethod.POST)
	public ModelAndView suchen(HttpServletRequest request, Principal principal){
		ModelAndView mav = new ModelAndView("FrageUndAntwort/FUAMainPage");
		
		if(principal == null){
			mav = new ModelAndView("FrageUndAntwort/Unregistriert/FUAMainPage");
		}
		
		String suchbegriff = request.getParameter("suchBegriff").toLowerCase();	//Der Name, nach dem gesucht werden soll.
		
		List<Question> qList2 = mongoOperations.findAll(Question.class);	//Alle Programme
		List<Question> qList = new ArrayList<Question>();
		
		//Zuerst alle Fragen, die den Suchbegriff im Titel haben...
		for(Question q: qList2){

			if(q.getTitle().toLowerCase().contains(suchbegriff)){	//Wenn der Begriff im Namen enthalten ist...
				qList.add(q);					//... dann ab in die Liste.
			}
		}

		//... dann alle Fragen, die den Suchbegriff in der Frage selbst haben. Vielleicht später gegen Schlagworte austauschen.
		for(Question q: qList2){

			if(q.getQuestion().toLowerCase().contains(suchbegriff)){	//Wenn der Begriff im Namen enthalten ist...
				qList.add(q);					//... dann ab in die Liste.
			}
		}
		
		mav.addObject("questions", qList);
		
		if(principal != null)
			mav.addObject("unreadAnswers", getUnreadAnswers(mongoOperations.findById(principal.getName(), User.class)));
		
		return mav;
	}
	
	public int getUnreadAnswers(User user){
		
		List<Notification> notifications = mongoOperations.find(new Query(Criteria.where("username").is(user.getUsername())), Notification.class);
		
		return notifications.size();
	}
}