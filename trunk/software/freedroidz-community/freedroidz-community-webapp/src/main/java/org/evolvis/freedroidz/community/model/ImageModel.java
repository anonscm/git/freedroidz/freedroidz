package org.evolvis.freedroidz.community.model;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class ImageModel {

	private CommonsMultipartFile image;

	public void setImage(CommonsMultipartFile image) {
		this.image = image;
	}

	public CommonsMultipartFile getImage() {
		return image;
	}
}
