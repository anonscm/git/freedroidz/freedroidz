package org.evolvis.freedroidz.community.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.imageio.stream.MemoryCacheImageInputStream;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.evolvis.freedroidz.community.data.Program;
import org.evolvis.freedroidz.community.data.User;
import org.evolvis.freedroidz.community.data.UserToApprove;
import org.evolvis.freedroidz.community.email.Mail;
import org.evolvis.freedroidz.community.model.ImageModel;
import org.evolvis.freedroidz.community.model.NewUserModel;
import org.evolvis.freedroidz.community.model.UserModel;
import org.evolvis.freedroidz.community.questionAndAnswer.Answer;
import org.evolvis.freedroidz.community.questionAndAnswer.Notification;
import org.evolvis.freedroidz.community.questionAndAnswer.Question;
import org.evolvis.freedroidz.community.security.StringCipher;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Order;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/Benutzer")
public class BenutzerController {

	@Inject
	private MongoOperations mongoOperations;

	@RequestMapping(value = "/Bearbeiten")
	public ModelAndView showBearbeiten(Principal principal, HttpServletRequest request) {
		final ModelAndView modelAndView = new ModelAndView(
				"Benutzer/DatenBearbeiten");

		String username = principal.getName();
		final User requestedUser = mongoOperations.findById(username,
				User.class);

		UserModel userModel = UserModel.userToUserModel(requestedUser);
		userModel.setCurrentPassword("");	//Aus Sicherheit wird das Passwort leer gesetzt. Mit einem MD5 Passwort kann sowieso niemand etwas anfangen.

		modelAndView.addObject("user", userModel);
		modelAndView.addObject("errorMessage", request.getParameter("errorMessage"));
	
		return modelAndView;
	}

//	@RequestMapping(value="/Benutzernamenaendern", method = RequestMethod.POST)
//	public ModelAndView alterUsername(HttpServletRequest request, Principal principal){
//		ModelAndView mav = new ModelAndView("redirect:/Benutzer/bearbeiten");
//		
//		String newUsername = request.getParameter("username");
//		User isUsernameForgiven = mongoOperations.findById(newUsername, User.class);
//		
//		if(isUsernameForgiven == null){
//			User user = mongoOperations.findById(principal.getName(), User.class);
//			mongoOperations.remove(user);
//			user.setUsername(newUsername);
//			mongoOperations.insert(user);
//			
//			String message = "Ihr Benutzername wurde erfolgreich geändert.";
//			mav.addObject("errorMessage", message);
//		}else{
////			mav = new ModelAndView("Benutzer/DatenBearbeiten");
//			String errorMessage = "Benutzername ist schon vorhanden.";
//			mav.addObject("errorMessage", errorMessage);
//		}		
//		
//		return mav;
//	}
	
	@RequestMapping(value="/Passwortaendern", method = RequestMethod.POST)
	public ModelAndView alterPassword(HttpServletRequest request, Principal principal) throws NoSuchAlgorithmException, UnsupportedEncodingException{
		ModelAndView mav = new ModelAndView(":redirect:/Benutzer/Bearbeiten");
		
		User user = mongoOperations.findById(principal.getName(), User.class);
		String currentPassword = request.getParameter("currentPassword");
		String currentPasswordMD5 = StringCipher.stringSipher(currentPassword);
		
		String newPassword = request.getParameter("newPassword");
		String newPassword2 = request.getParameter("newPassword2");
		
		if(user.getPassword().equals(currentPasswordMD5)){
			if(newPassword.equals(newPassword2)){
				
				mongoOperations.remove(user);
				String newPasswordMD5 = StringCipher.stringSipher(newPassword);
				user.setPassword(newPasswordMD5);
				mongoOperations.insert(user);
				
				mav.addObject("errorMessage", "Ihr Passwort wurde erfolgreich geändert.");
				
			}else{
				mav.addObject("errorMessage", "Die erneute Eingabe des neuen Passwortes ist nicht identisch mit der ersten.");
			}
		}else{
			mav.addObject("errorMessage", "Sie haben ein falsches Passwort eingegeben. Es wurde nichts verändert.");
		}
		
		return mav;
	}
	
	@RequestMapping(value = "/Detailsaendern", method = RequestMethod.POST)
	public ModelAndView alterDetails(HttpServletRequest request, Principal principal){
		ModelAndView mav = new ModelAndView("redirect:/Benutzer/Bearbeiten");
		
		User user = mongoOperations.findById(principal.getName(), User.class);
		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");
		String email = request.getParameter("email");
		String about = request.getParameter("about");
		
		if(!email.equals("") && validateEmail(email)){
			mongoOperations.remove(user);
			user.setFirstname(firstname);
			user.setLastname(lastname);
			user.setEmail(email);
			user.setAbout(about);
			mongoOperations.insert(user);
			
			mav.addObject("errorMessage", "Ihre Daten wurden erfolgreich geändert.");
		}else{
			mav.addObject("errorMessage", "Bitte geben Sie eine korrekte E-Mail Adresse an.");
		}		
		
		return mav;
	}
	
	@RequestMapping(value="/Bildaendern", method = RequestMethod.POST)
	public ModelAndView alterImage(@ModelAttribute(value="imageForm") ImageModel image, Principal principal) throws IOException{
		ModelAndView mav = new ModelAndView("redirect:/Benutzer/Bearbeiten");
		
		User user = mongoOperations.findById(principal.getName(), User.class);

		mongoOperations.remove(user);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		IOUtils.copy(image.getImage().getInputStream(), out);
		user.setImage(image.getImage().getBytes());
		mongoOperations.insert(user);
		
		mav.addObject("errorMessage", "Ihr Bild wurde erfolgreich gerändert.");
		
		return mav;
	}
	
//	@RequestMapping(value = "/bearbeiten", method = RequestMethod.POST)
//	public ModelAndView bearbeiten(@ModelAttribute("user") UserModel userModel,
//			Principal principal) throws NoSuchAlgorithmException, IOException {
//
//		ModelAndView modelAndView = new ModelAndView(":/Benutzer/bearbeiten");
//		User currentUser = mongoOperations.findById(principal.getName(),
//				User.class);
//
//		userModel.setCurrentPassword(StringCipher.stringSipher(userModel.getCurrentPassword()));	//Passwort wird verschlüsselt, damit damit gearbeitet werden kann.
//
//		if (userModel.getUsername() != null
//				&& !userModel.getUsername().equals("")) {	//Username darf nicht leer sein.
//			if (userModel.getCurrentPassword() != null
//					&& !userModel.getCurrentPassword().equals("")) {	//Passwort darf nicht leer sein.
//				if (userModel.getCurrentPassword().equals(
//						currentUser.getPassword())) {	//Passwort muss richtig sein.
//					if (userModel.getNewPassword() != null
//							&& !userModel.getNewPassword().equals("")
//							&& userModel.getNewPassword().equals(
//									userModel.getNewPassword2())) {	//Neues Passwort darf nicht leer sein und muss 2 mal gleich sein.
//
//						User newUser = userModel.getUser(userModel);
//						
//						User oldUser = mongoOperations.findById(principal.getName(), User.class);
//						
//						if(newUser.getImage().length == 0){		//Falls das Bild des neuen Users nicht existiert, wird das alte genommen.
//							newUser.setImage(oldUser.getImage());
//						}
//						
//						if(newUser.getEmail().equals("") || validateEmail(newUser.getEmail())){
//						
//						mongoOperations.remove(oldUser);	//Alter User löschen
//						mongoOperations.insert(newUser);	//Neuen User eintragen
//
//						modelAndView.addObject("currentUser", newUser);
//						}else{
//							modelAndView.addObject("errorMessage",
//									"Die Emailaddresse hat ein falsches Format.");
//						}
//					} else {
//						modelAndView
//								.addObject(
//										"errorMessage",
//										"Die Eingabe in \"Neues Passwort erneut eingeben\" ist nicht identisch mit dem neuen Passwort <br />"
//												+ "oder die Felder sind leer.");
//					}
//				} else {
//					modelAndView.addObject("errorMessage",
//							"Das Passwort ist falsch.");
//				}
//			} else {
//				modelAndView
//						.addObject("errorMessage",
//								"Das Feld \"Aktuelles Passwort\" muss ausgefüllt sein.");
//			}
//		} else {
//			modelAndView.addObject("errorMessage",
//					"Das Feld \"Benutzername\" muss ausgefüllt sein.");
//		}
//
//		return modelAndView;
//	}

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView register(Principal principal) {
		ModelAndView modelAndView;
		if(principal == null){
			modelAndView = new ModelAndView("Register");
	
			modelAndView.addObject("user", new UserModel());
		}else{
			modelAndView = new ModelAndView("redirect:/Benutzer/Profil");
		}
		return modelAndView;
	}

	@RequestMapping(value = "/Profil", method = RequestMethod.GET)
	public ModelAndView showProfile(Principal principal) {

		final ModelAndView modelAndView = new ModelAndView(
				"Benutzer/EigenesProfil");
		User currentUser = mongoOperations.findById(principal.getName(),
				User.class);

		modelAndView.addObject("currentUser", currentUser);		//Damit der User im Profil angezeigt werden kann.
		
		return modelAndView;
	}

	@RequestMapping(value = "/EigeneProgramme", method = RequestMethod.GET)
	public ModelAndView showOwnPrograms(Principal principal, HttpServletRequest request) {

		final ModelAndView modelAndView = new ModelAndView(
				"Benutzer/EigeneProgramme");
		int limit = 20;
		int aktuelleSeite = 1;
		if(request.getParameter("actuallyPage") != null)
			aktuelleSeite = Integer.parseInt(request.getParameter("actuallyPage"));
		
		int anzahlElemente = mongoOperations.find(new Query(
				Criteria.where("userId").is(principal.getName())),
				Program.class).size();
		
		int anzahlSeiten = anzahlElemente / limit;
		if((anzahlElemente % limit) > 0){
		}
		int offset = aktuelleSeite*limit-limit;
		
		String firstPage = "";
		String lastPage = "";
		String previousPage = "";
		String nextPage = "";
		
		final List<Program> userPrograms = mongoOperations.find(new Query(
				Criteria.where("userId").is(principal.getName())).skip(offset).limit(limit),
				Program.class);		//Alle Programme, die vom User sind.
		
		/*testserver
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			firstPage = "<a href=\"/freedroidz-community-webapp/Benutzer/EigeneProgramme?actuallyPage=1\"><<</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			lastPage = "<a href=\"/freedroidz-community-webapp/Benutzer/EigeneProgramme?actuallyPage=" + anzahlSeiten + "\">>></a>";
		}
		
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			previousPage = "<a href=\"/freedroidz-community-webapp/Benutzer/EigeneProgramme?actuallyPage=" + (aktuelleSeite-1) + "\"><</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			nextPage = "<a href=\"/freedroidz-community-webapp/Benutzer/EigeneProgramme?actuallyPage=" + (aktuelleSeite+1) + "\">></a>";
		}
		*/

		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			firstPage = "<a href=\"/Benutzer/EigeneProgramme?actuallyPage=1\"><<</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			lastPage = "<a href=\"/Benutzer/EigeneProgramme?actuallyPage=" + anzahlSeiten + "\">>></a>";
		}
		
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			previousPage = "<a href=\"/Benutzer/EigeneProgramme?actuallyPage=" + (aktuelleSeite-1) + "\"><</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			nextPage = "<a href=\"/Benutzer/EigeneProgramme?actuallyPage=" + (aktuelleSeite+1) + "\">></a>";
		}
		
		String auswahlLeiste = firstPage + " " + previousPage + " Seite " + aktuelleSeite + " von " + anzahlSeiten + " " + nextPage + " " + lastPage;
		
		modelAndView.addObject("auswahlLeiste", auswahlLeiste);
		
		modelAndView.addObject("programme", userPrograms); // liste ans
															// modelandview
															// übergeben

		modelAndView.addObject("programmeEmpty", userPrograms.isEmpty());	//Für den Fall, dass der User keine Programme hat.

		return modelAndView;
	}

	@RequestMapping(value = "/EigeneProgramme/Delete", method = RequestMethod.GET)
	public ModelAndView deleteProgram(HttpServletRequest request,
			Principal principal) {

		final ModelAndView modelAndView = new ModelAndView(
				"redirect:/Benutzer/EigeneProgramme");

		String programIdAsString = request.getParameter("programId"); // ID des
																		// Programms
																		// holen
		long programId = Long.parseLong(programIdAsString);
		Program program = mongoOperations.findById(programId, Program.class); // Programm
																				// holen

		if (program.getUserId().equals(principal.getName())) {		//Zur Sicherheit prüfen, ob das Programm auch vom User ist.
			mongoOperations.remove(program); // Program löschen
			modelAndView.addObject("programDeleted", "Das Programm \""
					+ program.getName() + "\" wurde gelöscht.");
		} else {
			modelAndView
					.addObject("programDeleted",
							"Das Programm gehört nicht Dir und wurde deshalb nicht gelöscht.");
		}
		
		//Sucht alle Programme vom User.
		List<Program> userPrograms = mongoOperations.find(new Query(Criteria.where("userId").is(principal.getName())), Program.class);

		modelAndView.addObject("programme", userPrograms);	//testList = userPrograms
		modelAndView.addObject("programmeEmpty", userPrograms.isEmpty());

		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView createUser(
			@ModelAttribute("user") NewUserModel userModel)
			throws NoSuchAlgorithmException, IOException, AddressException, MessagingException {

		ModelAndView modelAndView = new ModelAndView("RegisterSuccess");

		if (mongoOperations.findById(userModel.getUsername(), User.class) == null) {	//Existiert der User schon?
			if (userModel.getUsername() != null
					&& !userModel.getUsername().equals("")) {	//Ist der Username leer?
				if (userModel.getNewPassword() != null
						&& userModel.getNewPassword().equals(
								userModel.getNewPassword2())) {	//2mal das gleiche Passwort eingegeben und es ist nicht leer?
					if(this.validateEmail(userModel.getEmail()) || !userModel.getEmail().equals("")){	//Email eingegeben?
						UserToApprove user = new UserToApprove();
						user = userModel.getUserToApprove(userModel);
						//user.setPassword(StringCipher.stringSipher(user.getPassword()));	//Passwortverschlüsselung, wird von UserModel übernommen
	
						String approveKeyAsString = "";
						long approveKey = 0;
						
						//Erstelle Key
						do{
							approveKeyAsString = "";
							
							for(int i = 0; i < 10; i++){	//Erstelle 10-stelligen Key
								approveKeyAsString += (int)(Math.random() * 10);
							}
							approveKey = Long.parseLong(approveKeyAsString);
						}while(mongoOperations.find(new Query(Criteria.where("approveKey").is(approveKey)), UserToApprove.class).size() != 0); //Auf Einmaligkeit des Keys prüfen.
						
						user.setApproveKey(approveKey);
						mongoOperations.insert(user);
						
						//email verschicken
						Mail mail = new Mail();
						mail.sendApproveMail(user);
						
						String message = "Herzlichen Glückwunsch, Du hast Dich erfolgreich registriert.<br />" +
											"In Kürze erhälst du eine Bestätigungs-E-Mail. Bitte folge dem darin liegenden Link, um deinen Account zu bestätigen.<br /><br />" +
											"Viel Spaß in der community.";
						modelAndView.addObject("message", message);
					}else{
						modelAndView = new ModelAndView("Register");
						modelAndView.addObject("errorMessage",
								"Die Emailaddresse hat ein falsches Format oder wurde nicht eingegeben.");
					}
				} else {
					modelAndView = new ModelAndView("Register");
					modelAndView.addObject("errorMessage",
							"Die Passwörter sind nicht identisch.");
				}
			} else {
				modelAndView = new ModelAndView("Register");
				modelAndView.addObject("errorMessage",
						"Der Benutzername ist leer.");
			}
		} else {
			modelAndView = new ModelAndView("Register");
			modelAndView.addObject("errorMessage",
					"Der Benutzername ist bereits vergeben.");
		}

		return modelAndView;
	}
	
	@RequestMapping(value="/Bestätigen", method = RequestMethod.GET)
	public ModelAndView approveUser(HttpServletRequest request){
		ModelAndView mav = new ModelAndView("RegisterSuccess");

		if(request.getParameter("approveKey") != null){
			long approveKey = new Long(request.getParameter("approveKey"));	//hole key
			//suche UserToApprove mit diesem Key
			List<UserToApprove> utaList = mongoOperations.find(new Query(Criteria.where("approveKey").is(approveKey)), UserToApprove.class);
			
			if(utaList.size() == 1){	//prüfe auf einmaligkeit
				
				UserToApprove uta = utaList.get(0);
				
				/**
				 * Konvertiere UserToApprove zu User und stecke User in die DB.
				 */
				User user = new User();
				user.setFirstname(uta.getFirstname());
				user.setLastname(uta.getLastname());
				user.setAbout(uta.getAbout());
				user.setEmail(uta.getEmail());
				user.setImage(uta.getImage());
				user.setPassword(uta.getPassword());
				user.setUsername(uta.getUsername());
				
				mongoOperations.remove(uta);	//lösche UserToApprove
				
				mongoOperations.insert(user);
				
				String message = "Herzlichen Glückwunsch. Du wurdest erfolgreich registriert.";
				mav.addObject("message", message);
			}else{
				mav = new ModelAndView("redirect:/login");
			}
		}else{
			mav = new ModelAndView("redirect:/login");
		}
		
		return mav;
	}
	
	private boolean validateEmail(String eMail){
		
		Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
		Matcher m = p.matcher(eMail);
		
		return m.matches();
	}

	@RequestMapping(value = "/EigeneProgramme/Download")
	public ModelAndView download(Principal principal,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		
		String programIdAsString = request.getParameter("programId");
		
		long programId = Long.parseLong(programIdAsString);
		
		Program program = mongoOperations.findById(programId, Program.class);	//Program aus der DB suchen.
		
		response.setContentType("program");
		response.setContentLength(program.getFile().length);	//länge des byte-arrays
		
		response.setHeader("Content-Disposition", "attachment; filename=" + program.getName());
		
		FileCopyUtils.copy(program.getFile(), response.getOutputStream());	//Program zum Download bereit stellen.
		
		return null;
	}

	@RequestMapping(value = "/EigeneProgramme/Anzeigen")
	public ModelAndView showProgram(Principal principal, HttpServletRequest request)
			throws IOException {

		ModelAndView mav = new ModelAndView("Programme/Anzeigen");
		String programIdAsString = request.getParameter("programId");

		long programId = Long.parseLong(programIdAsString);

		Program program = mongoOperations.findById(programId, Program.class);	//Program aus der DB suchen.

		String sourceCode = new String(program.getFile());	//Umwandlung der Datei (byte[]) in einen String.
		
		if(isCode(sourceCode)){	//Prüfen, ob es sich um Code/Text handelt.
			sourceCode = sourceCode.replace(System.getProperty("line.separator"), "<br />");	//Für die Leserlichkeit.
		}
		else{
			sourceCode = "Die Datei enthält leider keinen Code.";
		}
		
		mav.addObject("program", program);
		mav.addObject("sourceCode", sourceCode);

		return mav;
	}

	/**
	 * Prüft, ob der String Code / Text enthält.
	 * @param sourceCode
	 * @return
	 */
	private boolean isCode(String sourceCode){
		
		for(int i = 0; i < sourceCode.length(); i++){
			if(sourceCode.charAt(i) < 0){
				System.out.println(i + " " + (int)sourceCode.charAt(i));
				return false;
			}else if(sourceCode.charAt(i) > 255){
				System.out.println(i + " " + (int)sourceCode.charAt(i));
				return false;
			}
		}
		
		return true;
	}
	
	@RequestMapping(value = "/Details")
	public ModelAndView benutzerAnzeigen(HttpServletRequest request, Principal principal) {

		ModelAndView mav = new ModelAndView("Benutzer/Benutzerprofil");

		User user = mongoOperations.findById(request.getParameter("userId"),
				User.class);	

		if(user != null){
			mav.addObject("user", user);	//User, der angezeigt werden soll.
		
			if(principal.getName().equals(user.getUsername())){
				mav = this.showProfile(principal);
			}
		}else{
			mav = new ModelAndView("Benutzer/Unregistered");
		}
		
		return mav;
	}

//	@RequestMapping(value = "/Suchen", method = RequestMethod.GET)
//	public ModelAndView benutzerSuche() {
//
//		ModelAndView mav = new ModelAndView("Benutzer/Benutzersuche");
//
//		return mav;
//	}
	
	
	@RequestMapping(value = "/Suchen", method = RequestMethod.GET)
	public ModelAndView benutzerSuche(HttpServletRequest request) {

		ModelAndView mav = new ModelAndView("Benutzer/Benutzersuche");

		String userName = request.getParameter("userName");
		
		int limit = 20;
		int aktuelleSeite = 1;
		if(request.getParameter("actuallyPage") != null){
			aktuelleSeite = Integer.parseInt(request.getParameter("actuallyPage"));
		}
		
		int anzahlElemente = mongoOperations.find(new Query(Criteria.where("username").regex("([A-Z]*[a-z]*)*"+ userName + "([A-Z]*[a-z]*)*")), User.class).size();	//$regex:[/.*" + userName  + ".*/]
//		mongoOperations.find(new Query(Criteria.where("username").re is("$regex:[/.*" + userName  + ".*/]")), User.class).size();
		
		int anzahlSeiten = anzahlElemente / limit;
		if((anzahlElemente % limit) > 0){
			anzahlSeiten ++;
		}
		int offset = aktuelleSeite*limit-limit;
		
		String firstPage = "";
		String lastPage = "";
		String previousPage = "";
		String nextPage = "";
		
		if(!userName.equals("")){
//			List<User> userList = mongoOperations.find(new Query(Criteria.where("username").is("/" + userName  + "/")).skip(offset).limit(limit), User.class);
			List<User> userList = mongoOperations.find(new Query(Criteria.where("username").regex("([A-Z]*[a-z]*)*"+ userName + "([A-Z]*[a-z]*)*")).skip(offset).limit(limit), User.class);
			mav.addObject("userList", userList);
		}
		
		/*	Testserver
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			firstPage = "<a href=\"/freedroidz-community-webapp/Benutzer/Suche?actuallyPage=1&userName=" + userName + "\"><<</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			lastPage = "<a href=\"/freedroidz-community-webapp/Benutzer/Suche?actuallyPage=" + anzahlSeiten + "&userName=" + userName + "\">>></a>";
		}
		
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			previousPage = "<a href=\"/freedroidz-community-webapp/Benutzer/Suche?actuallyPage=" + (aktuelleSeite-1) + "&userName=" + userName + "\"><</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			nextPage = "<a href=\"/freedroidz-community-webapp/Benutzer/Suche?actuallyPage=" + (aktuelleSeite+1) + "&userName=" + userName + "\">></a>";
		}*/
		
		//community.freedroidz.org
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			firstPage = "<a href=\"/Benutzer/Suche?actuallyPage=1&userName=" + userName + "\"><<</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			lastPage = "<a href=\"/Benutzer/Suche?actuallyPage=" + anzahlSeiten + "&userName=" + userName + "\">>></a>";
		}
		
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			previousPage = "<a href=\"/Benutzer/Suche?actuallyPage=" + (aktuelleSeite-1) + "&userName=" + userName + "\"><</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			nextPage = "<a href=\"/Benutzer/Suche?actuallyPage=" + (aktuelleSeite+1) + "&userName=" + userName + "\">></a>";
		}
		
		String auswahlLeiste = firstPage + " " + previousPage + " Seite " + aktuelleSeite + " von " + anzahlSeiten + " " + nextPage + " " + lastPage;
		
		mav.addObject("auswahlLeiste", auswahlLeiste);
		return mav;
	}
	
	@RequestMapping(value="/Bild")
	public void bildAnzeigen(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		try{
			String username = request.getParameter("username");
			User user = mongoOperations.findById(username, User.class);
		
			response.setContentType("image/jpeg");	//Antwort wird ein Bild sein.
			
			MemoryCacheImageInputStream input = new MemoryCacheImageInputStream(new ByteArrayInputStream(user.getImage())); //Holt das Bild vom User.
			
			BufferedImage image = ImageIO.read(input);
			
			if(image == null){	//Wenn kein Bild vorhanden ist...
				image = ImageIO.read(getClass().getResource("/images/profilbild.jpeg"));	//...Dann wird ein Standardbild gesetzt.
			}
			
			OutputStream out = response.getOutputStream();
			ImageIO.write(image, "jpg", out);	//Gibt das Bild aus.
		}catch(Exception except){
			System.out.println(except.getMessage());
		}
	}
	
	@RequestMapping(value="/Loeschen", method= RequestMethod.GET)
	public ModelAndView profilLoeschen(){
		ModelAndView mav = new ModelAndView("/Benutzer/BenutzerLoeschen");
		mav.addObject("message", "Möchtest du dein Profil wirklich löschen?<br />Dann gib bitte dein Passwort ein, um dies zu bestätigen.");
		return mav;
	}
	
	@RequestMapping(value="/Loeschen", method= RequestMethod.POST)
	public ModelAndView profilLoeschen(HttpServletRequest request, Principal principal) throws NoSuchAlgorithmException, UnsupportedEncodingException{
		ModelAndView mav = new ModelAndView("redirect:/j_spring_security_logout");
		
		User user = mongoOperations.findById(principal.getName(), User.class);	//User, der gelöscht werden soll.
		String pw = StringCipher.stringSipher(request.getParameter("password"));	//Passwort verschlüsseln, zum vergleich.
		
		if(user.getPassword().equals(pw)){	//Passwort vergleichen.
			String username = principal.getName();
			
			//Alle Daten zwischenspeichern
			List<Program> programList = mongoOperations.find(new Query(Criteria.where("userId").is(username)), Program.class);
			
			//Alle Daten ändern und wieder einfügen. Nur Programme und Forumeinträge.
			for(Program p: programList){
				mongoOperations.remove(p);
				p.setUserId("Benutzer gelöscht");
				mongoOperations.insert(p);
			}
			
			List<Question> qList = mongoOperations.find(new Query(Criteria.where("author").is(username)), Question.class);
			for(Question q: qList){
				mongoOperations.remove(q);
				q.setAuthor("Benutzer gelöscht");
				mongoOperations.insert(q);
			}
			
			List<Answer> aList = mongoOperations.find(new Query(Criteria.where("author").is(username)), Answer.class);
			for(Answer a: aList){
				mongoOperations.remove(a);
				a.setAuthor("Benutzer gelöscht");
				mongoOperations.insert(a);
			}
			
			//User löschen
			mongoOperations.remove(user);
		}else{	//Passwort falsch.
			mav = new ModelAndView("/Benutzer/BenutzerLoeschen");
			mav.addObject("message", "Du hast ein falsches Passwort eingegeben.<br />Bitte versuche es erneut.");	//Fehlermeldung
		}
		
		return mav;
	}
	
	@RequestMapping(value="/MeineFragen")
	public ModelAndView meineFragen(Principal principal, HttpServletRequest request){
		ModelAndView mav = new ModelAndView("FrageUndAntwort/FUAMainPage");

		User user = mongoOperations.findById(principal.getName(), User.class);
		Query q = new Query(Criteria.where("author").is(user.getUsername()));
		int limit = 20;
		int aktuelleSeite = 1;
		if(request.getParameter("actuallyPage") != null)
			aktuelleSeite = Integer.parseInt(request.getParameter("actuallyPage"));
		int anzahlElemente = mongoOperations.find(q, Question.class).size();
		
		int anzahlSeiten = anzahlElemente / limit;
		if((anzahlElemente % limit) > 0){
			anzahlSeiten ++;
		}
		int offset = aktuelleSeite*limit-limit;
		
		String firstPage = "";
		String lastPage = "";
		String previousPage = "";
		String nextPage = "";
		
		/* testserver
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			firstPage = "<a href=\"/freedroidz-community-webapp/Benutzer/MeineFragen?actuallyPage=1\"><<</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			lastPage = "<a href=\"/freedroidz-community-webapp/Benutzer/MeineFragen?actuallyPage=" + anzahlSeiten + "\">>></a>";
		}
		
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			previousPage = "<a href=\"/freedroidz-community-webapp/Benutzer/MeineFragen?actuallyPage=" + (aktuelleSeite-1) + "\"><</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			nextPage = "<a href=\"/freedroidz-community-webapp/Benutzer/MeineFragen?actuallyPage=" + (aktuelleSeite+1) + "\">></a>";
		}*/
		

		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			firstPage = "<a href=\"/Benutzer/MeineFragen?actuallyPage=1\"><<</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			lastPage = "<a href=\"/Benutzer/MeineFragen?actuallyPage=" + anzahlSeiten + "\">>></a>";
		}
		
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			previousPage = "<a href=\"/Benutzer/MeineFragen?actuallyPage=" + (aktuelleSeite-1) + "\"><</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			nextPage = "<a href=\"/Benutzer/MeineFragen?actuallyPage=" + (aktuelleSeite+1) + "\">></a>";
		}
		
		String auswahlLeiste = firstPage + " " + previousPage + " Seite " + aktuelleSeite + " von " + anzahlSeiten + " " + nextPage + " " + lastPage;
		
		mav.addObject("auswahlLeiste", auswahlLeiste);
		
		q.sort().on("date", Order.DESCENDING);
		List<Question> qList = mongoOperations.find(q.skip(offset).limit(limit), Question.class);
		
		mav.addObject("questions", qList);
		mav.addObject("unreadAnswers", getUnreadAnswers(mongoOperations.findById(principal.getName(), User.class)));
		
		return mav;
	}
	
	
	
	@Deprecated
	@RequestMapping(value="/MeineUnbeantwortetenFragen")
	public ModelAndView meineUnbeantwortetenFragen(Principal principal){
		ModelAndView mav = new ModelAndView("FrageUndAntwort/FUAMainPage");
		
		User user = mongoOperations.findById(principal.getName(), User.class);
		Query q = new Query(Criteria.where("author").is(user.getUsername()).and("isOpen").is(true));
		q.sort().on("date", Order.DESCENDING);
		List<Question> qList = mongoOperations.find(q, Question.class);
		
		mav.addObject("questions", qList);
		
		return mav;
	}

	@RequestMapping(value="/Notifications")
	public ModelAndView notifications(Principal principal, HttpServletRequest request){
		ModelAndView mav = new ModelAndView("FrageUndAntwort/FUAMainPage");
		int limit = 20;
		int aktuelleSeite = 1;
		if(request.getParameter("actuallyPage") != null)
			aktuelleSeite = Integer.parseInt(request.getParameter("actuallyPage"));
		int anzahlElemente = mongoOperations.find(new Query(Criteria.where("username").is(principal.getName())), Notification.class).size();
		
		int anzahlSeiten = anzahlElemente / limit;
		if((anzahlElemente % limit) > 0){
			anzahlSeiten ++;
		}
		int offset = aktuelleSeite*limit-limit;
		
		String firstPage = "";
		String lastPage = "";
		String previousPage = "";
		String nextPage = "";
		
		/*testserver
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			firstPage = "<a href=\"/freedroidz-community-webapp/Benutzer/Notifications?actuallyPage=1\"><<</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			lastPage = "<a href=\"/freedroidz-community-webapp/Benutzer/Notifications?actuallyPage=" + anzahlSeiten + "\">>></a>";
		}
		
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			previousPage = "<a href=\"/freedroidz-community-webapp/Benutzer/Notifications?actuallyPage=" + (aktuelleSeite-1) + "\"><</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			nextPage = "<a href=\"/freedroidz-community-webapp/Benutzer/Notifications?actuallyPage=" + (aktuelleSeite+1) + "\">></a>";
		}*/
		
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			firstPage = "<a href=\"/Benutzer/Notifications?actuallyPage=1\"><<</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			lastPage = "<a href=\"/Benutzer/Notifications?actuallyPage=" + anzahlSeiten + "\">>></a>";
		}
		
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			previousPage = "<a href=\"/Benutzer/Notifications?actuallyPage=" + (aktuelleSeite-1) + "\"><</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			nextPage = "<a href=\"/Benutzer/Notifications?actuallyPage=" + (aktuelleSeite+1) + "\">></a>";
		}
		
		String auswahlLeiste = firstPage + " " + previousPage + " Seite " + aktuelleSeite + " von " + anzahlSeiten + " " + nextPage + " " + lastPage;
		
		mav.addObject("auswahlLeiste", auswahlLeiste);
		List<Notification> notifications = mongoOperations.find(new Query(Criteria.where("username").is(principal.getName())).skip(offset).limit(limit), Notification.class);
		
		List<Long> qIDs = new ArrayList<Long>();
		
		for(int i = 0; i < notifications.size(); i++){
			qIDs.add(notifications.get(i).getQuestionId());
		}
		
		List<Question> qList = mongoOperations.find(new Query(Criteria.where("ID").in(qIDs)), Question.class);
		mav.addObject("questions", qList);
		mav.addObject("unreadAnswers", getUnreadAnswers(mongoOperations.findById(principal.getName(), User.class)));
		
		return mav;
	}
	
	public int getUnreadAnswers(User user){
		
		List<Notification> notifications = mongoOperations.find(new Query(Criteria.where("username").is(user.getUsername())), Notification.class);
		
		return notifications.size();
	}
}
