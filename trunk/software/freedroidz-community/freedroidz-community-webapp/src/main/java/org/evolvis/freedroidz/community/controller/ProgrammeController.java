package org.evolvis.freedroidz.community.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.evolvis.freedroidz.community.data.Program;
import org.evolvis.freedroidz.community.model.ProgramModel;
import org.evolvis.fupco.UploadedFile;
import org.evolvis.fupco.Validator;
import org.evolvis.fupco.config.Configuration;
import org.evolvis.fupco.exceptions.ValidatorInitException;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/Programme")
public class ProgrammeController {

	@Inject
	private MongoOperations mongoOperations;
	
	@RequestMapping(value="/Suche", method = RequestMethod.POST)
	public ModelAndView programmeGefunden(Principal principal, HttpServletRequest request){
		
		ModelAndView mav = new ModelAndView("Programme/Alle");

		if(principal == null){
			mav = new ModelAndView("Programme/Unregistriert/Alle");
		}
		
		String suchbegriff = request.getParameter("suchbegriff").toLowerCase();	//Der Name, nach dem gesucht werden soll.
		
		List<Program> programList2 = mongoOperations.findAll(Program.class);	//Alle Programme
		List<Program> programList = new ArrayList<Program>();
		
		for(Program p: programList2){

			if(p.getName().toLowerCase().contains(suchbegriff)){	//Wenn der Begriff im Namen enthalten ist...
				programList.add(p);					//... dann ab in die Liste.
			}
		}
		
		mav.addObject("programme", programList);
		
		return mav;
	}
	
	@RequestMapping(value = "/Hochladen", method = RequestMethod.GET)
	public ModelAndView showHochladen(Principal principal) {
		final ModelAndView modelAndView = new ModelAndView(
				"Programme/Hochladen");

		modelAndView.addObject("program", new ProgramModel());

		return modelAndView;
	}
	
	@RequestMapping(value = "/Hochladen", method = RequestMethod.POST)
	public ModelAndView programmHochladen(@ModelAttribute("program") ProgramModel file, Principal principal) throws IOException, ValidatorInitException{
		
		ModelAndView modelAndView = new ModelAndView("redirect:/Benutzer/eigeneProgramme");		
		
		if(file.getSourceCode().getSize() <= 10485760){	//10MB
			
			List<Program> programme = mongoOperations.findAll(Program.class);	//holt alle programme aus der DB
			Validator validator = new  Validator();
			
			Configuration config = Configuration.readConfig(getClass().getResource("/freedroidz-community-fupco.xml").getFile());
			
			UploadedFile uf = validator.validate(file.getSourceCode().getInputStream(), config);
			
			if(uf.isValidationSuccess()){
				if(file.getSourceCode().getSize() != 0){	//Wenn eine Datei angegeben wurde.
					if(!file.getName().equals("")){	//Wenn ein Name angegeben wurde.
						
						Program program = new Program();
						program.setName(file.getName());
						program.setDifficulty(file.getDifficulty());
						program.setAufwand(file.getAufwand());
						program.setDescription(file.getDescription());
						program.setUserId(principal.getName());
						program.setFile(file.getSourceCode().getBytes());
						program.setId(findHighestProgramID(programme));	//neu
						program.setCategory(file.getCategory());
						try{
							mongoOperations.insert(program);
						}catch(Exception except){
							modelAndView = new ModelAndView("Programme/Hochladen");
							//Wenn die Datei für die MDB zu groß sein sollte, was eigentlich nicht mehr der Fall sein sollte.
							modelAndView.addObject("errorMessage", "<font color=\"#ff0000\">Die Datei ist zu groß.</font>");
							
							return modelAndView;
						}
						programme = mongoOperations.findAll(Program.class);	//holt alle programme aus der DB
						
						final List<Program> userPrograms = new ArrayList<Program>();	//hier sollen alle programme vom user rein
						
						for(Program p : programme){	//für jedes programm
							try{	//für den fall, dass ein wert null ist
								if(p.getUserId().equals(principal.getName())){	//wenn das programm vom user ist...
									userPrograms.add(p);	//...dann ab in die liste
								}
							}
							catch(Exception except){}	//hier soll nichts passieren
						}
						
						
						modelAndView.addObject("programme", userPrograms);	//liste ans modelandview übergeben
						modelAndView.addObject("programmeEmpty", userPrograms.isEmpty());
				
					}else{
							modelAndView = new ModelAndView("Programme/Hochladen");
							modelAndView.addObject("errorMessage", "<font color=\"#ff0000\">Dein Program hat keinen Namen.</font>");
					}
				}
				else{
					modelAndView = new ModelAndView("Programme/Hochladen");
					modelAndView.addObject("errorMessage", "<font color=\"#ff0000\">Du hast keine Datei Ausgewählt.</font>");
				}
			}else{
				modelAndView = new ModelAndView("Programme/Hochladen");
				modelAndView.addObject("errorMessage", "<font color=\"#ff0000\">Die Datei besitzt ein nicht zugelassenes Format.<br />Bitte stelle sicher, dass die Datei das richtige Format hat.</font>");
			}
		}else{
			modelAndView = new ModelAndView("Programme/Hochladen");
			modelAndView.addObject("errorMessage", "<font color=\"#ff0000\">Die Datei ist zu groß.<br />Sie darf maximal 10MB groß sein.</font>");
		}
		return modelAndView;
	}
	
	/**
	 * Sucht die niedrigste, zur Verfügung stehende ID.
	 * @param list ist die Liste aller Programme.
	 * @return Gibt die ID wieder.
	 */
	private long findHighestProgramID(List<Program> list){
		
		long ID = 0;
		List<Long> IDs = new ArrayList<Long>();
		
		for(Program p :list){
			IDs.add(p.getId());
		}
		
		try{
			while(IDs.contains(ID)){
				ID++;
			}
		}
		catch(Exception except){
			System.out.println(except.getMessage());
		}

		return ID;
	}
	
	/**
	 * Zeigt alle Programm an.
	 * @param principal
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView alle(HttpServletRequest request, Principal principal) {
		ModelAndView modelAndView = new ModelAndView("Programme/Alle");

		if(principal == null){
			modelAndView = new ModelAndView("Programme/Unregistriert/Alle");
		}
		
		int limit = 20;
		int aktuelleSeite = 1;
		if(request.getParameter("actuallyPage") != null)
			aktuelleSeite = Integer.parseInt(request.getParameter("actuallyPage"));
		int anzahlElemente = mongoOperations.findAll(Program.class).size();
		
		int anzahlSeiten = anzahlElemente / limit;
		if((anzahlElemente % limit) > 0){
			anzahlSeiten ++;
		}
		int offset = aktuelleSeite*limit-limit;
		
		String firstPage = "";
		String lastPage = "";
		String previousPage = "";
		String nextPage = "";
		
		
		final List<Program> programme = mongoOperations.find(new Query().skip(offset).limit(limit), Program.class);
		modelAndView.addObject("programme", programme);
		
		/*testserver
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			firstPage = "<a href=\"/freedroidz-community-webapp/Benutzer/EigeneProgramme?actuallyPage=1\"><<</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			lastPage = "<a href=\"/freedroidz-community-webapp/Benutzer/EigeneProgramme?actuallyPage=" + anzahlSeiten + "\">>></a>";
		}
		
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			previousPage = "<a href=\"/freedroidz-community-webapp/Benutzer/EigeneProgramme?actuallyPage=" + (aktuelleSeite-1) + "\"><</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			nextPage = "<a href=\"/freedroidz-community-webapp/Benutzer/EigeneProgramme?actuallyPage=" + (aktuelleSeite+1) + "\">></a>";
		}
		*/

		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			firstPage = "<a href=\"/Benutzer/EigeneProgramme?actuallyPage=1\"><<</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			lastPage = "<a href=\"/Benutzer/EigeneProgramme?actuallyPage=" + anzahlSeiten + "\">>></a>";
		}
		
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			previousPage = "<a href=\"/Benutzer/EigeneProgramme?actuallyPage=" + (aktuelleSeite-1) + "\"><</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			nextPage = "<a href=\"/Benutzer/EigeneProgramme?actuallyPage=" + (aktuelleSeite+1) + "\">></a>";
		}
		
		
		String auswahlLeiste = firstPage + " " + previousPage + " Seite " + aktuelleSeite + " von " + anzahlSeiten + " " + nextPage + " " + lastPage;
		
		modelAndView.addObject("auswahlLeiste", auswahlLeiste);
		
		return modelAndView;
	}
	
	@RequestMapping(value = "/Download")
	public ModelAndView download(Principal principal, HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		String programIdAsString = request.getParameter("programId");
		
		long programId = Long.parseLong(programIdAsString);
		
		Program program = mongoOperations.findById(programId, Program.class);	//Holt das Program, welches gedownloaded werden soll.
		
		if(program != null){
			response.setContentType("program");
			response.setContentLength(program.getFile().length);	//länge des byte-arrays
		
			response.setHeader("Content-Disposition", "attachment; filename=" + program.getName());
	
			FileCopyUtils.copy(program.getFile(), response.getOutputStream());	//Stellt das Programm zum Download bereit.
			
			return null;
		}else{
			ModelAndView mav = new ModelAndView("Programme/Anzeigen");
			program = new Program();
			program.setName("Die Datei ist nicht vorhanden.");
			mav.addObject("program", program);
			mav.addObject("sourceCode", "Sorry, die Datei ist nicht vorhanden.<br />Sie wurde entweder gelöscht oder ist zur Zeit nicht erreichbar.");
			
			return mav;
		}
	}

	@RequestMapping(value = "/Anzeigen")
	public ModelAndView show(Principal principal, HttpServletRequest request) throws IOException{
		
		ModelAndView mav = new ModelAndView("Programme/Anzeigen");
		if(principal == null){
			mav = new ModelAndView("Programme/Unregistriert/Anzeigen");
		}
		String programIdAsString = request.getParameter("programId");
		
		long programId = Long.parseLong(programIdAsString);
		
		Program program = mongoOperations.findById(programId, Program.class);	//Holt das anzuzeigende Programm.
		
		if(program != null){
			String sourceCode = new String(program.getFile());	//Konvertiert das byte[] zu einem String.
		
			
			if(isCode(sourceCode)){	//Wenn es sich um Code/Text handelt.
				sourceCode = sourceCode.replace(System.getProperty("line.separator"), "<br />");
			}
			else{
				sourceCode = "Die Datei enthält leider keinen Code.";
			}

			mav.addObject("program", program);
			mav.addObject("sourceCode", sourceCode);
		}else{			
			program = new Program();
			program.setName("Die Datei ist nicht vorhanden.");
			mav.addObject("program", program);
			mav.addObject("sourceCode", "Sorry, die Datei ist nicht vorhanden.<br />Sie wurde entweder gelöscht oder ist zur Zeit nicht erreichbar.");
		}
		return mav;
	}

	/**
	 * Prüft, ob es sich bei der Datei um Code/Text handelt.
	 * @param sourceCode ist der String, der geprüft werden soll.
	 * @return
	 */
	private boolean isCode(String sourceCode){
		
		for(int i = 0; i < sourceCode.length(); i++){
			if(sourceCode.charAt(i) < 0){
				System.out.println(i + " " + (int)sourceCode.charAt(i));
				return false;
			}else if(sourceCode.charAt(i) > 255){
				System.out.println(i + " " + (int)sourceCode.charAt(i));
				return false;
			}
		}
		
		return true;
	}
	
	@RequestMapping(value = "/SortiertNach")
	public ModelAndView sortBySchwierigkeitsgrad(HttpServletRequest request, Principal principal){
		ModelAndView mav = new ModelAndView("Programme/SortiertNach");
		
		if(principal == null){
			mav = new ModelAndView("Programme/Unregistriert/SortiertNach");
		}
		
		int limit = 20;
		int aktuelleSeite = 1;
		if(request.getParameter("actuallyPage") != null)
			aktuelleSeite = Integer.parseInt(request.getParameter("actuallyPage"));
		String sortBy = request.getParameter("sortBy");
		String value = request.getParameter("value");
		int anzahlElemente = mongoOperations.find(new Query(Criteria.where(sortBy).is(value)), Program.class).size();
		
		int anzahlSeiten = anzahlElemente / limit;
		if((anzahlElemente % limit) > 0){
			anzahlSeiten ++;
		}
		int offset = aktuelleSeite*limit-limit;
		List<Program> programs = mongoOperations.find(new Query(Criteria.where(sortBy).is(value)).skip(offset).limit(limit), Program.class);

		String sortiertLinks = "";
		String firstPage = "";
		String lastPage = "";
		String previousPage = "";
		String nextPage = "";
		
		if(sortBy.equals("difficulty")){
			sortiertLinks = "<table>"+
						"<tr>"+
							"<td>"+
								"<a href=\"http://community.freedroidz.org/freedroidz-community-webapp/Programme/SortiertNach?sortBy=difficulty&value=Anfaenger&actuallyPage=1\">Anfänger</a>"+
							"</td>"+
							"<td>"+
							"	<a href=\"http://community.freedroidz.org/freedroidz-community-webapp/Programme/SortiertNach?sortBy=difficulty&value=Unterstufe&actuallyPage=1\">Unterstufe</a>"+
							"</td>"+
							"<td>"+
							"	<a href=\"http://community.freedroidz.org/freedroidz-community-webapp/Programme/SortiertNach?sortBy=difficulty&value=Oberstufe&actuallyPage=1\">Oberstufe</a>"+
							"</td>"+
							"<td>"+
							"	<a href=\"http://community.freedroidz.org/freedroidz-community-webapp/Programme/SortiertNach?sortBy=difficulty&value=Fortgeschrittene&actuallyPage=1\">Fortgeschrittene</a>"+
							"</td>"+
							"<td>"+
							"	<a href=\"http://community.freedroidz.org/freedroidz-community-webapp/Programme/SortiertNach?sortBy=difficulty&value=Entwickler&actuallyPage=1\">Entwickler</a>"+
							"</td>"+
						"</tr>"+
					"</table>";
		}
		if(sortBy.equals("category")){
			sortiertLinks = "<table>"+
			"<tr>"+
				"<td>"+
					"<a href=\"http://community.freedroidz.org/freedroidz-community-webapp/Programme/SortiertNach?sortBy=category&value=Anfaenger&actuallyPage=1\">Anfänger</a>"+
				"</td>"+
				"<td>"+
				"	<a href=\"http://community.freedroidz.org/freedroidz-community-webapp/Programme/SortiertNach?sortBy=category&value=Spass&actuallyPage=1\">Spaß</a>"+
				"</td>"+
				"<td>"+
				"	<a href=\"http://community.freedroidz.org/freedroidz-community-webapp/Programme/SortiertNach?sortBy=category&value=Unterricht&actuallyPage=1\">Unterricht</a>"+
				"</td>"+
				"<td>"+
				"	<a href=\"http://community.freedroidz.org/freedroidz-community-webapp/Programme/SortiertNach?sortBy=category&value=Wissenschaftlich&actuallyPage=1\">Wissenschaftlich</a>"+
				"</td>"+
			"</tr>"+
		"</table>";
		}
		
		/*
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			firstPage = "<a href=\"/freedroidz-community-webapp/Programme/SortiertNach?sortBy=" + sortBy + "&value=" + value + "&actuallyPage=1\"><<</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			lastPage = "<a href=\"/freedroidz-community-webapp/Programme/SortiertNach?sortBy=" + sortBy + "&value=" + value + "&actuallyPage=" + anzahlSeiten + "\">>></a>";
		}
		
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			previousPage = "<a href=\"/freedroidz-community-webapp/Programme/SortiertNach?sortBy=" + sortBy + "&value=" + value + "&actuallyPage=" + (aktuelleSeite-1) + "\"><</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			nextPage = "<a href=\"/freedroidz-community-webapp/Programme/SortiertNach?sortBy=" + sortBy + "&value=" + value + "&actuallyPage=" + (aktuelleSeite+1) + "\">></a>";
		}*/

		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			firstPage = "<a href=\"/Programme/SortiertNach?sortBy=" + sortBy + "&value=" + value + "&actuallyPage=1\"><<</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			lastPage = "<a href=\"/Programme/SortiertNach?sortBy=" + sortBy + "&value=" + value + "&actuallyPage=" + anzahlSeiten + "\">>></a>";
		}
		
		if(aktuelleSeite != 1 && anzahlSeiten != 0){
			previousPage = "<a href=\"/Programme/SortiertNach?sortBy=" + sortBy + "&value=" + value + "&actuallyPage=" + (aktuelleSeite-1) + "\"><</a>";
		}
		
		if(aktuelleSeite != anzahlSeiten && anzahlSeiten != 0){
			nextPage = "<a href=\"/Programme/SortiertNach?sortBy=" + sortBy + "&value=" + value + "&actuallyPage=" + (aktuelleSeite+1) + "\">></a>";
		}
		
		String auswahlLeiste = firstPage + " " + previousPage + " Seite " + aktuelleSeite + " von " + anzahlSeiten + " " + nextPage + " " + lastPage;
		
		mav.addObject("auswahlLeiste", auswahlLeiste);
		
		mav.addObject("programme", programs);
		if(sortBy.equals("difficulty")){
			mav.addObject("sortiertNach", "Schwierigkeitsgrad: " + value);
		}
		if(sortBy.equals("category")){
			mav.addObject("sortiertNach", "Kategorie: " + value);
		}
		mav.addObject("sortiertLinks", sortiertLinks);
		
		return mav;
	}
}
