
 <%@page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>
<%@page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ include file="BenutzerInc.jsp"%>

<div id="mainDiv">
	<!-- In ein Formular stecken und dann löschen lassen, damit das Programm nicht in der URL steht. -->
	<h3>Meine Programme</h3>
	
	<p>${programDeleted}</p>
	
	<c:if test="${programmeEmpty == false}">
	<div id="liste">
		<table>
			<tr>
				<th>Name</th>
				<th>Beschreibung</th>
				<th>von..</th>
				<th></th>
			</tr>
			<c:forEach var="programm" items="${programme}">
				<c:url var="benutzerHref" value="/Benutzer/Details?userId=${programm.userId}" />
				<c:url var="programmAnzeigenHref" value="/Programme/Anzeigen?programId=${programm.id}" />
				<c:url var="programmDownloadHref" value="/Programme/Download?programId=${programm.id}" />
				<tr>
					<td>${programm.name}</td>
					<td>${programm.description}</td>
					<c:if test="${programm.userId != 'Benutzer gelöscht'}">
						<td><a href="${benutzerHref}">${programm.userId}</a></td>
					</c:if>
					<c:if test="${programm.userId == 'Benutzer gelöscht'}">
						<td>${programm.userId}</td>
					</c:if>
					<td><a href="${programmAnzeigenHref}">Anzeigen</a> | <a href="${programmDownloadHref}">Download</a></td>
				</tr>
			</c:forEach>
		</table>
		<br />
		${auswahlLeiste}
	</div>
	</c:if>
	<c:if test="${programmeEmpty == true}">
		<p>
			Du hast noch keine Programme hochgeladen.
		</p>
	</c:if>
	
</div>

<%@ include file="../inc/footer.jsp"%>