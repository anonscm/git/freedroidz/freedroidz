<%@page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>
<%@page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
  	<link rel="shortcut icon" content="image/x-icon" href="/images/favicon.png">
    <link href="/css/community.css" type="text/css" rel="stylesheet" />
    <link href="/css/login.css" type="text/css" rel="stylesheet" />
  </head>
  <body>
  	<h1>freedroidz-community</h1>
  	<div id="centered_page">
  		<div id="header">
			
			<div id="logo">
				<img src="images/logo.png" alternative="logo.png" />
			</div>
			
			<div id="loginDiv">

				<form name="login" action="/j_spring_security_check" method="POST">
					<table>
						<tr>
							<td align="left">
								<p>Benutzername:</p>
								<p><input name="j_username" type="text" /></p>
							</td>
							<td align="left">
								<p>Passwort:</p>
								<p><input name="j_password" type="password" /></p>
							</td>
							<td align="left">
								<p><br /></p>
								<p><input type="submit" name="loginSubmit" value="Anmelden" /></p>
							</td>
						</tr>
						<tr>
							<td>
								<a href="/Benutzer">Registrieren</a>
							</td>
						</tr>
					</table>
				</form>
			</div>
			
			<div id="communityLogo">
				<img src="images/community-logo.gif" alternative="comLog.gif" />
			</div>		
			<!-- später gegen bilder austauschen -->
			<div id="navigation">
				<a href="/Programme?acutallyPage=1">Programme</a> &nbsp;&nbsp;&nbsp;&nbsp;
				<a href="/FrageUndAntwort?actuallyPage=1">Frage und Antwort</a>
			</div>
			
		</div>
		