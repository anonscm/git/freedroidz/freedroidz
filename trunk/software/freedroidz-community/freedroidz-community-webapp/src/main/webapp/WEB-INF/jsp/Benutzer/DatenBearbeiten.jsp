
 <%@page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>
<%@page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ include file="BenutzerInc.jsp"%>

<div id="mainDiv">
	<h3>Meine Daten bearbeiten</h3>
		<p>
			${errorMessage}	<!-- oder auch info -->
		</p>
        <p>
            Felder die mit * gekennzeichnet sind, sind Pflichtfelder.
        </p>
          
          <div>          	
          	<form:form modelAttribute="user">
          	
          		<div id="passwordAendern">
          	    <form method="post" action="/Benutzer/Passwortaendern" >
	              <h4>Passwort ändern</h4>
	              
	              <table>
	              	<tr>
	              		<td>
			              <p>
		    	            <label for="currentPassword">Aktuelles Passwort: *</label>
	            		  </p>
	                	</td>
	                	<td>
	                		<p>
								<form:input path="currentPassword" type="password" name="oldPassword" />
		          			</p>
		          		</td>
		          	</tr>
		          	<tr>
		          		<td>
	              			<p>
	                			<label for="newPassword">Neues Passwort: *</label>
	                		</p>
	                	</td>
	                	<td>
	                		<p>
								<form:input path="newPassword" type="password" name="newPassword" />
		          			</p>
		          		</td>
		          	</tr>
		          	<tr>
		          		<td>
	              			<p>
	                			<label for="newPassword2">Neues Passwort erneut eigeben: *</label>
							</p>
						</td>
						<td>
							<p>
								<form:input path="newPassword2" type="password" name="newPassword2" />
		          			</p>
		          		</td>
		          	</tr>
		          	<tr>
		          		<td>
		          			<input type="submit" value="Passwort ändern" />
		          		</td>
		          	</tr>
		          </table>
		        </form>
		       </div>
		       
				<div id="detailsAendern">
			       <form method="post" action="/Benutzer/Detailsaendern" >
			       <h4>Details ändern</h4>
			           	<table>
			          		<tr>
			           			<td>
						           	<p>
						               	<label for="firstname">Vorname:</label>
						            </p>
			              		</td>
			              		<td>
			              			<p>
			              				<form:input path="firstname" name="firstname" />
			              			</p>
			              		</td>
			              	</tr>
			              	<tr>
			              		<td>
							        <p>
							            <label for="lastname">Nachname:</label>	
			              			</p>
			              		</td>
			              		<td>
			              			<p>
							          	<form:input path="lastname" name="lastname" />
							        </p>
			              		</td>
			              	</tr>
			              	<tr>
			              		<td>
							        <p>
							            <label for="email">E-Mail: *</label>
			              			</p>
			              		</td>
			              		<td>
			              			<p>
							            <form:input path="email" name="email" />	<!-- auf validität prüfen -->
							        </p>
			              		</td>
			              	</tr>
			              	<tr>
			              		<td>
							        <p>
							            <label for="about">Über dich:</label>
							        </p>
			              		</td>
			              		<td>
			              		<p>
								   		<form:input path="about" name="about" />
						           	</p>
			              		</td>
			              	</tr>
			              	<tr>
				            	<td>
							        <p><input type="submit" value="Details ändern" /></p>
				            	</td>
			              	</tr>
			              </table>			
						</form>
					</div>
				
				<div id="bildAendern">
				<form method="post" action="/Benutzer/Bildaendern" commandName="imageForm" enctype="multipart/form-data">
	              <h4>Bild ändern</h4>
	              
	              <table>
	              	<tr>
	              		<td>
			              <p>
			              	<label for="image">Profilbild:</label>
		              	  </p>
		              	</td>
		              	<td>
		              		<p>
				              	<form:input path="image" type="file" name="image" size="40" maxlength="100000" accept="text/*" />
			                </p>
		      			</td>
		      		</tr>
		      		<tr>
		      			<td>
			              <p>
			                <input type="submit" value="Profilbild ändern"/>
			              </p>
	        			</td>
	        		</tr>
	        	  </table>
	            </form>
	           	</div>
	          </form:form>
          </div>
</div>

<%@ include file="../inc/footer.jsp"%>