 <%@page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>
<%@page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ include file="FUAInc.jsp"%>

<div id="frageNichtDa">
	<h3>Die Frage ist nicht vorhanden</h3>
	
	<p>
		Sorry, die Frage ist nicht vorhanden.
		<br />
		Sie wurde entwedern gelöscht oder ist zur Zeit nicht erreichbar.
	</p>
</div>

<%@ include file="../inc/footer.jsp"%>