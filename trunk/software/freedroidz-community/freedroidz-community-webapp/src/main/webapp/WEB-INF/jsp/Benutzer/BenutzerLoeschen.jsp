<%@page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>
<%@page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ include file="BenutzerInc.jsp"%>

<div id="mainDiv">
	<h3>Benutzerprofil löschen</h3>
	
	<form method="POST" action="/Benutzer/Loeschen">
		<p>
			${message}
			<br />
			<br />
			
			<input type="password" name="password" />
			<input type="submit" value="Löschen" />
		</p>
	</form>
</div>

<%@ include file="../inc/footer.jsp"%>