<%@page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>
<%@page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ include file="BenutzerInc.jsp"%>

<div id="mainDiv">
	<h3>Dies ist das Profil von ${user.username}</h3>
	
	<div id="profilbild">
		<img src="/Benutzer/Bild?username=${user.username}" width="200" height="200" alt="profilbild" />
	</div>
	
	<div id="profilDetails">
		<table>
			<tr>
				<td>
					<p>Benutzername:</p>
				</td>
				<td>
					<p>${user.username}</p>
				</td>
			</tr>
			<tr>
				<td>
					<p>Vorname:</p>
				</td>
				<td>
					<p>${user.firstname}</p>
				</td>
			</tr>
			<tr>
				<td>
					<p>Nachname:</p>
				</td>
				<td>
					<p>${user.lastname}</p>
				</td>
			</tr>
			<tr>
				<td>
					<p>E-Mail:</p>
				</td>
				<td>
					<p>${user.email}</p>
				</td>
			</tr>
			<tr>
				<td>
					<p>Über mich:</p>
				</td>
				<td>
					<p>${user.about}</p>
				</td>
			</tr>
		</table>
	</div>
</div>

<%@ include file="../inc/footer.jsp"%>