<%@page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>
<%@page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ include file="ProgrammeInc.jsp"%>

<div id="mainDiv">
	<div id="programmeSuche">
		<h3>Suche</h3>	
		<p>
			Hier kannst Du Programme nach ihrem Namen suchen. <br />
		</p>
		<form method="POST" action="/Programme/Suche">
			<input type="text" name="suchbegriff" />
			<input type="submit" name="suchen" value="Suchen" />	
		</form>
	</div>
	
	<div id="liste">
		<table>
			<tr>
				<th>Name</th>
				<th>Beschreibung</th>
				<th>von..</th>
				<th></th>
			</tr>
			<c:forEach var="programm" items="${programme}">
				<c:url var="benutzerHref" value="/Benutzer/Details?userId=${programm.userId}" />
				<c:url var="programmAnzeigenHref" value="/Programme/Anzeigen?programId=${programm.id}" />
				<c:url var="programmDownloadHref" value="/Programme/Download?programId=${programm.id}" />
				<tr>
					<td>${programm.name}</td>
					<td>${programm.description}</td>
					<c:if test="${programm.userId != 'Benutzer gelöscht'}">
						<td><a href="${benutzerHref}">${programm.userId}</a></td>
					</c:if>
					<c:if test="${programm.userId == 'Benutzer gelöscht'}">
						<td>${programm.userId}</td>
					</c:if>
					<td><a href="${programmAnzeigenHref}">Anzeigen</a> | <a href="${programmDownloadHref}">Download</a></td>
				</tr>
			</c:forEach>
		</table>
		<br />
		${auswahlLeiste}
	</div>
</div>

<%@ include file="../inc/footer.jsp"%>