 <%@page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>
<%@page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ include file="FUAInc.jsp"%>


<div id="mainDiv">
	<h3>${question.title}</h3>
	
	<div class="frage">
		<div class="frageLinks">
			<img class="profilbild" src="/Benutzer/Bild?username=${question.author}"  width="100" height="100" alt="profilbild"/>

			<p>Von: <a href="/Benutzer/Details?userId=${question.author}">${question.author}</a></p>
			<p>Am: ${question.date}</p>
		</div>
		<div class="frageRechts">
			<p>
				${question.question}
			</p>
		</div>
	</div>
	
	<c:forEach var="answer" items="${answers}">
		<div class="frage">
			<div class="frageLinks">
				<img class="profilbild" src="/Benutzer/Bild?username=${answer.author}"  width="100" height="100" alt="profilbild"/>

					<p>Von: <a href="/Benutzer/Details?userId=${answer.author}">${answer.author}</a></p>
					<p>Am: ${answer.date}</p>
			</div>
			<div class="frageRechts">
				<p>
					${answer.answer}
				</p>
			</div>
		</div>
	</c:forEach>
	
	<!-- seiten einfügen, pro 10 antworten... -->
	
	<div id="frageSchliessen">
		<c:if test="${userIsOwnerAndIsOpen}">
			<form method="POST" action="/FrageUndAntwort/Schliessen">
				<p>
					Frage beantwortet?
				</p>
				<input type="hidden" name="qId" value="${question.ID}" />
				<br />
				<input type="submit" value="Frage schließen" />
			</form>
		</c:if>
	</div>
	<div id="antwort">
		<c:if test="${question.isOpen}">
			<form method=POST action="/FrageUndAntwort/Antwort">
				<p>Gib hier deine Antwort ein: *</p>
				<textarea name="answer" cols="100" rows="10" ></textarea>
		        <br />
		        <input type="hidden" name="qId" value="${question.ID}" />
				<input type="submit" value="Antworten" name="postSubmit" />
			</form>
		</c:if>
	</div>
</div>

	
<%@ include file="../inc/footer.jsp"%>