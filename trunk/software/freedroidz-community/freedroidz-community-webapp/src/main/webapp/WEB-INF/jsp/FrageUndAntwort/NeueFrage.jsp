 <%@page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>
<%@page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ include file="FUAInc.jsp"%>

<div id="neueFrageMainDiv">
	<h3>Neuer Thread</h3>
	
	<p>
		Geben Sie hier ihre Frage ein.
		Felder die mit * gekennzeichnet sind, sind Pflichtfelder.
	</p>

	<h3>${errorMessage}</h3>

	<form method="POST" action="/FrageUndAntwort/NeueFrage">
		<p>Titel der Frage *</p>
		<p><input type="text" maxlength="50" name="questionTitle" /></p>
		<p>Gib hier deine Frage ein: *</p>
		<p><textarea name="question" cols="100" rows="10" ></textarea></p>
		<p>
			Kategorie *
		</p>
		<p><select name="categorie">
			<option value="null">- Bitte auswählen -</option>
            <option value="buildingRobots">Roboter bauen</option>
	        <option value="programming">Programmieren</option>
        </select></p>

		<p><input type="submit" value="Posten" name="postSubmit" /></p>
	</form>
</div>

<%@ include file="../inc/footer.jsp"%>