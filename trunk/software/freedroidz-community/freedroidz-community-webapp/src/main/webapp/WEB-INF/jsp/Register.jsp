<%@page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>
<%@page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ include file="inc/login_header.jsp"%>

<div id="register">
          <h3>Registrieren</h3>
          
          <p>
			<font color="#ff0000">${errorMessage}</font>
          </p>
          <p>
            Felder, die mit * gekennzeichnet sind, sind Pflichtfelder und müssen deshalb ausgefüllt werden.
          </p>
          <div>
            <form:form modelAttribute="user" method="post" enctype="multipart/form-data" >
            <table>
            <tr>
            	<td>
              <p>
                <label for="username">Benutzername: *</label>
                </p>
              </td>
              <td>
              <p>
                <form:input path="username" />
              </p>
              </td>
            </tr>
            <tr>
            	<td>
              <p>
                <label for="newPassword">Passwort: *</label>
                </p>
              </td>
              <td>
              <p>
				<form:input path="newPassword" type="password" />
	          </p>
              </td>
            </tr>
            <tr>
            	<td>
              <p>
                <label for="newPassword2">Passwort erneut eingeben: *</label>
                </p>
              </td>
              <td>
              <p>
				<form:input path="newPassword2" type="password" />
	          </p>
              </td>
            </tr>
            <tr>
            	<td>
              <p>
                <label for="firstname">Vorname:</label>
                </p>
              </td>
              <td>
              <p>
                <form:input path="firstname" />
              </p>
              </td>
            </tr>
            <tr>
            	<td>
              <p>
                <label for="lastname">Nachname:</label>
                </p>
              </td>

              <td>
              <p>
              	<form:input path="lastname" />
              </p>
              </td>
            </tr>
            <tr>
              <td>
              <p>
                <label for="email">E-Mail: *</label>
                </p>
              </td>
              <td>
              <p>
                <form:input path="email" />
              </p>
              </td>
            </tr>
            <tr>
            	<td>
              <p>
                <label for="about">Über dich:</label>
                </p>
              </td>

              <td>
              <p>
                <form:input path="about" />
              </p>
              </td>
            </tr>
            <tr>
            	<td>
              <p>
              	<label for="image">Profilbild:</label>
                </p>
              </td>
              	
              <td>
              <p>
              	<form:input path="image" type="file" name="image" size="40" maxlength="100000" accept="text/*" />
              </p>
              </td>
            </tr>
            <tr>
            	<td>
              <p>
                <input type="submit" value="Registrieren!"/>
              </p>
              </td>
            </tr>
              </table>
            </form:form>
          </div>
</div>
<%@ include file="inc/footer.jsp"%>
      