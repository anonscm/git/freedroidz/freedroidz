<%@page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>
<%@page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ include file="inc/login_header.jsp"%>


<div id="register">
	<h3>Willkommen auf freedroidz-community!</h3> 
	
	<p>
		${message}
	</p>
</div>

<%@ include file="inc/footer.jsp"%>