<%@page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>
<%@page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ include file="BenutzerInc.jsp"%>

<div id="mainDiv">
	<h3>Benutzersuche</h3>
	
	<div id="userSuche">
		<p>
			Bitte gib hier den Namen des Benutzers ein, des gesucht werden soll.
		</p>
		
		<form method="GET" action="/Benutzer/Suchen" >
			<input type="text" name="userName" />
			<input type="submit" name="submitSearch" value="Suchen" />
		</form>
	</div>
	
	<div id="suchergebnisse">
		<table>
			<c:forEach var="user" items="${userList}">
				<tr>
					<td>
						<img src="/Benutzer/Bild?username=${user.username}" width="50" height="50" alt="profilbild" />
					</td>
					<td>
						<a href="/Benutzer/Details?userId=${user.username}" >${user.username}</a>
					</td>
				</tr>
			</c:forEach>
		</table>
		${auswahlLeiste}
	</div>
</div>
<%@ include file="../inc/footer.jsp"%>