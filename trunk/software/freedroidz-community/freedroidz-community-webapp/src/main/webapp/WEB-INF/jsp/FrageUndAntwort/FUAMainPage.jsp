 <%@page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>
<%@page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ include file="FUAInc.jsp"%>

<div id="mainDiv">
	<h3>Frage & Antwort</h3>
	<p align="left">
		Hier kannst du Fragen stellen, solltest du mal nicht weiter wissen.
		Aber du kannst Fragen auch gerne beantworten, sollten andere eine Frage haben.
		Wenn du eine Frage gestellt hast, bekommst du automatisch bescheid, wenn man dir geantwortet hat.
	</p>
	
	<div id="suche">
		<form action="/FrageUndAntwort/Suche" method="POST">
			<table>
				<tr>
					<td>Suche: </td>
					<td><input type="text" name="suchBegriff" /></td>
					<td><input type="submit" value="Suchen" /></td>
				</tr>
			</table>
		</form>
	</div>
	
	<div id="liste">
			<c:forEach var="question" items="${questions}">
				<div class="vorschau">
					<div class="vorschauLinks">
						<p>
							<a href="/FrageUndAntwort/Frage?qId=${question.ID}">${question.title}</a>
						</p>
						<p>${question.date}</p>
						<p>Autor: ${question.author}</p>
					</div>
					<div class="vorschauRechts">
					<p>
							<c:if test="${question.isOpen}">
								Offen
							</c:if>
							<c:if test="${question.isOpen == false}">
								Geschlossen
							</c:if>
						</p>
						<p>
							Antworten: ${question.numberOfAnwsers}
						</p>
					</div>
				</div>
			</c:forEach>
		${auswahlLeiste}
	</div>
</div>
<%@ include file="../inc/footer.jsp"%>