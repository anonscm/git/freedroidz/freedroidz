
 <%@page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>
<%@page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ include file="ProgrammeInc.jsp"%>

<div id="mainDiv">
		<h3>Programme Hochladen</h3>
          <p>
            Hier kannst du dein Programme hochladen und sie anderen zeigen!
            Beachte, dass du sie damit unter GPL lizensierst. Damit ist sichergestellt,
            dass andere dein Programm benutzen dürfen und du die Programme anderer benutzen kannst!
          </p>
          <p>
          	${errorMessage}
          </p>
          
          <div id="newProgramm">
            <form:form modelAttribute="program" method="post" enctype="multipart/form-data" >
              <table>
             	<tr>
             		<td>
		              <p>
		                <label for="name">Name:</label>
		              </p>
		            </td>
		            <td>
		              <p>
						<form:input path="name" />
		              </p>
		            </td>
		         </tr>
	             <tr>
	             	<td>
		              <p>
		                <label for="description">Beschreibung:</label>
			          </p>
		          	</td>
		          	<td>
		              <p>
						<form:input path="description" />
					  </p>
				  	</td>
				  </tr>
	              <tr>
	              	<td>
		              <p>
		                <label for="difficulty">Komplexität:</label>
					  </p>
					</td>
					<td>
						<p>
			                <form:select path="difficulty">
			                  <form:option value="Anfaenger">Anfänger</form:option>
			                  <form:option value="Unterstufe">Unterstufe</form:option>
			                  <form:option value="Oberstufe">Oberstufe</form:option>
			                  <form:option value="Fortgeschrittene">Fortgeschrittene</form:option>
			                  <form:option value="Entwickler">Entwickler</form:option>
			                </form:select>
			            </p>
			        </td>
			      </tr>
			      <tr>
			      	<td>
		              <p>
		                <label for="category">Kategorie:</label>
		              </p>
	                </td>
	                <td>
	              	  <p>	
		                <form:select path="category">
		                  <form:option value="Anfaenger">Anfänger</form:option>
		                  <form:option value="Spass">Spaß</form:option>
		                  <form:option value="Unterricht">Unterricht</form:option>
		                  <form:option value="Wissenschaftlich">Wissenschaftlich</form:option>
		                </form:select>
		              </p>
		            </td>
		          </tr>
		          <tr>
		          	<td>
		              <p>
		                <label for="aufwand">Aufwand (in Minuten):</label>
					  </p>
					</td>
					<td>
					  <p>
	                	<form:input path="aufwand" />
	              	  </p>
	              	</td>
	             </tr>
	             <tr>
	             	<td>
		              <p>
		                <label for="file">Datei:</label>
	 				  </p>
	 				</td>
	 				<td>
		 				<p>
		                  <form:input path="sourceCode" type="file" name="file" size="40" maxlength="100000" accept="text/*"/>
		                </p>
	                </td>
	              </tr>
	              <tr>
	              	<td>
		              <p>
		                <input type="submit" name="upload" value="Hochladen"/>
		              </p>
	              	</td>
	              </tr>
	         </table>
	        </form:form>
          </div>
</div>
<%@ include file="../inc/footer.jsp"%>