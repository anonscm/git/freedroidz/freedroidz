<%@page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>
<%@page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ include file="inc/login_header.jsp"%>

	<div id="index">
		<h2>Willkommen auf freedroidz-community!</h2> 
		<p>
			Auf dieser Seite kannst du dich mit anderen freedroidz'lern austauschen und deine Programme hochladen,
			sowie Programme von anderen ausprobieren!
		</p>
	</div>

<%@ include file="inc/footer.jsp"%>