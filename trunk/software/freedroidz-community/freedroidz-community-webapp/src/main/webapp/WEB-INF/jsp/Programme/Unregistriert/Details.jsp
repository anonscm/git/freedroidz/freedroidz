
 <%@page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>
<%@page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ include file="ProgrammeInc.jsp"%>

<div id="mainDiv">
	<h3>Programm: ${programm.name}</h3>
	
	<p>${programm.description}</p>
		<ul>
			<li>Aufwand: ${programm.aufwand}</li>
			<li>Komplexit&auml;t: ${programm.difficulty}</li>
		</ul>
	<p>Quellcode:</p>
	<pre>${programm.sourceCode}</pre>
</div>

<%@ include file="../../inc/footer.jsp"%>