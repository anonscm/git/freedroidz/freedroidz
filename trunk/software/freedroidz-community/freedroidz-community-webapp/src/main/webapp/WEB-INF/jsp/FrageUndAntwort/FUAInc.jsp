<%@page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>
<%@page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
  	<link rel="shortcut icon" content="image/x-icon" href="/images/favicon.png">
    <link href="/css/community.css" type="text/css" rel="stylesheet"/>
    <link href="/css/header.css" type="text/css" rel="stylesheet" />
    <link href="/css/FrageUndAntwort/fua.css" type="text/css" rel="stylesheet" />
  </head>
  <body>
  	<h1>freedroidz-community</h1>
  	<div id="centered_page">
  		<div id="header">
  			<div id="logo">
            	<a href="/"><img src="/images/logo_complete.png" alternative="logo.png" /></a>
			</div>
			
			<!-- später gegen bilder austauschen -->
			<div id="navigation">
				<a href="/">Home</a> &nbsp;&nbsp;&nbsp;&nbsp;
				<a href="/Programme?acutallyPage=1">Programme</a> &nbsp;&nbsp;&nbsp;&nbsp;
				<a href="/FrageUndAntwort?actuallyPage=1">Frage und Antwort</a> &nbsp;&nbsp;&nbsp;&nbsp;
				<a href="/Benutzer/Profil">Persönlich</a> &nbsp;&nbsp;&nbsp;&nbsp;
            	<a href="/j_spring_security_logout">Abmelden</a>
			</div>
			
		</div>
		
		<div id="naviLeft">
			<table>
				<tr>
					<td>
						<h4>Frage und Antwort</h4>
					</td>
				</tr>
				<tr>
					<td>
			    		<a href="/FrageUndAntwort">Alle Fragen</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href="/FrageUndAntwort/Offen?actuallyPage=1">Offene Fragen</a>
					</td>
				</tr>
				<tr>
					<td>
			    		<a href="/FrageUndAntwort/Beantwortet?actuallyPage=1">Beantwortete Fragen</a>
					</td>
				</tr>
				<tr>
					<td>
			    		<a href="/FrageUndAntwort/NeueFrage">Neue Frage stellen</a>
					</td>
				</tr>
				<tr>
					<td>
			    		<a href="/Benutzer/MeineFragen?actuallyPage=1">Meine Fragen</a>
					</td>
				</tr>
				<tr>
					<td>
			    		<a href="/Benutzer/Notifications?actuallyPage=1">Ungelesene Antworten (${unreadAnswers})</a>
					</td>
				</tr>
		    </table>
		</div>
		