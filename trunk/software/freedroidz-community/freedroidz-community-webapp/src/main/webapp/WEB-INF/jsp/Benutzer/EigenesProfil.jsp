<%@page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>
<%@page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ include file="BenutzerInc.jsp"%>

<div id="mainDiv">
	<h3>Dies ist dein Profil</h3>
	
	<div id="profilbild">
		<img src="/Benutzer/Bild?username=${currentUser.username}"  width="200" height="200" alt="profilbild"/>
	</div>
	
	<div id="profilDetails">
		<table>
			<tr>
				<td>
					<p>Benutzername:</p>
				</td>
				<td>
					<p>${currentUser.username}</p>
				</td>
			</tr>
			<tr>
				<td>
					<p>Vorname:</p>
				</td>
				<td>
					<p>${currentUser.firstname}</p>
				</td>
			</tr>
			<tr>
				<td>
					<p>Nachname:</p>
				</td>
				<td>
					<p>${currentUser.lastname}</p>
				</td>
			</tr>
			<tr>
				<td>
					<p>E-Mail:</p>
				</td>
				<td>
					<p>${currentUser.email}</p>
				</td>
			</tr>
			<tr>
				<td>
					<p>Über mich:</p>
				</td>
				<td>
					<p>${currentUser.about}</p>
				</td>
			</tr>
		</table>
	</div>
	
	<a href="/Benutzer/Loeschen">Profil löschen</a>
</div>

<%@ include file="../inc/footer.jsp"%>