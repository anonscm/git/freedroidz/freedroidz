<%@page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>
<%@page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ include file="ProgrammeInc.jsp"%>

<div id="rightDiv">
	<div id="fixedRight">
		<a href="/Programme/Download?programId=${program.id}">Download</a>
	</div>
</div>
<div id="mainDiv">
	<h2>${program.name}</h2>
	
	<div id="description">
		<h3>Beschreibung:</h3>
		<p>
			${program.description}
		</p>
	</div>
	<div id="code">
		<h3>Code:</h3>
		<p>
			${sourceCode}
		</p>
	</div>
</div>
	
<%@ include file="../inc/footer.jsp"%>