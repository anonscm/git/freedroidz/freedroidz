 <%@page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>
<%@page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ include file="inc/header.jsp"%>

		<div id="eingeloggt">
			<h2>Du bist eingeloggt</h2>
	        <p>
	            Wunderbar, jetzt kannst du direkt los legen und Programme hoch- und runterladen und
	            Fragen stellen und benatworten.
			</p>
		</div>
	        
	    <div id="left_mid">
	        <h4>Lade dein Programm hoch!</h4>
	        <p>
	            Auf freedroidz-community kannst du deine Programme hochladen und sie anderen zeigen!
	            <br/>
	            <br/>
	            <a href="/Programme/Hochladen">-&gt; Programme hochladen</a>
	        </p>
	    </div>
	    
	    <div id="mid">
	        <h4>Schaue dir Programme anderer an...</h4>
	        <p>
	            Du suchst Ideen und Anregungen? Dann schau dir doch mal an, was andere gemacht haben.
	            <br/>
	            <br/>
	            <a href="/Programme">Durchsuche Programme</a>
	        </p>
	    </div>
	    
		<div id="right_mid">
		    <h4>Du suchst Hilfe?</h4>
		    <p>
		        Wenn du Hilfe brauchst, schaue zuerst in
		        <a href="https://evolvis.org/plugins/mediawiki/wiki/freedroidz/index.php/Lejos_Anleitungen" target="_blank">unserem Wiki</a>
		        oder stelle deine <a href="FrageUndAntwort">Frage</a> direkt an andere Nutzer.	              
		    </p>       
	    </div>
	        
	    
<%@ include file="inc/footer.jsp"%>