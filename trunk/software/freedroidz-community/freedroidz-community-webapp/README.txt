Ans laufen bekommen:

### Vorbereitung:

Variante 1)
- MongoDB muss installiert sein. Bei tarent-desktop muss das aus dem Internet runtergeladen werden, beim aktuellen Ubuntu reicht einfach
apt-get install mongodb
- MongoDB muss laufen


Variante 2)
- im Backend den DBManager so umschreiben, dass er Java-Implementierungen für die DBs nimmt. 
JavaUserDB ist ein Beispiel, analog könnten die anderen implementiert werden, sollte kein großer Aufwand sein. 



### Bauen

cd pfad/zu/freedroidz-community
mvn clean install 

### Starten

cd freedroidz-community-webapp

mvn jetty:run

!!!Achtung: nie "mvn clean install jetty:run" machen, das klappt nicht!

- Die webapp sollte nun unter http://localhost:8080 erreichbar sein.

