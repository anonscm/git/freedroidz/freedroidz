/**
 *
 */
package org.freedroidz.studio

import org.eclipse.swt.graphics.Rectangle

/**
 * @author <a href="mailto:jan.ehrhardt@tarent.de">Jan Ehrhardt</a>
 */
package object application {

  var screen: Rectangle = null

}
