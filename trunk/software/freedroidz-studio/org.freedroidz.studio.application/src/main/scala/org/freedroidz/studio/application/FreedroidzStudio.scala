/**
 *
 */
package org.freedroidz.studio.application

import org.eclipse.equinox.app.IApplication.EXIT_OK
import org.eclipse.equinox.app.IApplication.EXIT_RESTART
import org.eclipse.equinox.app.IApplication
import org.eclipse.equinox.app.IApplicationContext
import org.eclipse.ui.PlatformUI.RETURN_RESTART
import org.eclipse.ui.PlatformUI.createAndRunWorkbench
import org.eclipse.ui.PlatformUI.createDisplay
import org.eclipse.ui.PlatformUI.getWorkbench
import org.eclipse.ui.PlatformUI.isWorkbenchRunning

/**
 * @author <a href="mailto:jan.ehrhardt@tarent.de">Jan Ehrhardt</a>
 */
class FreedroidzStudio extends IApplication {

  def start(context: IApplicationContext): Object = {
    val display = createDisplay

    screen = display.getClientArea

    try {
      val returnCode = createAndRunWorkbench(display, FreedroidzStudioWorkbenchAdvisor)

      if (returnCode == RETURN_RESTART) EXIT_RESTART
      else EXIT_OK
    } finally {
      display.dispose
    }
  }

  def stop: Unit = {
    if (isWorkbenchRunning) {
      val display = getWorkbench.getDisplay
      display.syncExec(() => { if (!display.isDisposed) display.dispose })
    }
  }

  implicit def asRunnable(f: () => Unit): Runnable = new Runnable() { def run = f }

}
