/**
 *
 */
package org.freedroidz.studio.application

import org.eclipse.ui.IPageLayout
import org.eclipse.ui.IPerspectiveFactory

/**
 * @author <a href="mailto:jan.ehrhardt@tarent.de">Jan Ehrhardt</a>
 */
class MainPerspective extends IPerspectiveFactory {

  def createInitialLayout(layout: IPageLayout): Unit = {}

}
