/**
 *
 */
package org.freedroidz.studio.application

import org.eclipse.swt.graphics.Point
import org.eclipse.ui.application.ActionBarAdvisor
import org.eclipse.ui.application.IActionBarConfigurer
import org.eclipse.ui.application.IWorkbenchWindowConfigurer
import org.eclipse.ui.application.WorkbenchWindowAdvisor

/**
 * @author <a href="mailto:jan.ehrhardt@tarent.de">Jan Ehrhardt</a>
 */
class FreedroidzStudioWindowAdvisor(config: IWorkbenchWindowConfigurer) extends WorkbenchWindowAdvisor(config) {

  val horizontalMargin = 50

  val verticalMargin = 30

  val windowWidth = screen.width - 3 * horizontalMargin

  val windowHeight = screen.height - 5 * verticalMargin

  val minimumWindowWidth = 1280

  override def createActionBarAdvisor(config: IActionBarConfigurer): ActionBarAdvisor = {
    new ActionBarAdvisor(config)
  }

  override def preWindowOpen = {
    val config = getWindowConfigurer

    config.setInitialSize(new Point(windowWidth, windowHeight))
    config.setShowCoolBar(false)
    config.setShowPerspectiveBar(false)
    config.setShowStatusLine(false)
    config.setTitle("freedroidz studio")
  }

  override def postWindowOpen = {
    val shell = getWindowConfigurer.getWindow.getShell
    shell.setLocation(horizontalMargin, verticalMargin)

    if (minimumWindowWidth > windowWidth) shell.setMaximized(true)
  }

}
