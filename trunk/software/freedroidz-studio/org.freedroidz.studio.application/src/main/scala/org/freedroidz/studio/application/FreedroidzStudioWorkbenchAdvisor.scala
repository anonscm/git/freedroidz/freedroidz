/**
 *
 */
package org.freedroidz.studio.application

import org.eclipse.ui.application._

/**
 * @author <a href="mailto:jan.ehrhardt@tarent.de">Jan Ehrhardt</a>
 */
object FreedroidzStudioWorkbenchAdvisor extends WorkbenchAdvisor {

  def getInitialWindowPerspectiveId = "org.freedroidz.studio.application.perspective"

  override def createWorkbenchWindowAdvisor(config: IWorkbenchWindowConfigurer): WorkbenchWindowAdvisor = {
    new FreedroidzStudioWindowAdvisor(config)
  }

}
