/**
 *
 */
package org.freedroidz.studio.tools.ui.views

import org.eclipse.swt.SWT.NONE
import org.eclipse.swt.widgets.{ Composite, Label }
import org.eclipse.ui.part.ViewPart

/**
 * @author <a href="mailto:jan.ehrhardt@tarent.de">Jan Ehrhardt</a>
 */
class ControlCenter extends ViewPart {

  def createPartControl(parent: Composite): Unit = {
    val label = new Label(parent, NONE)
    label.setText("Hello freedroidz!")
  }

  def setFocus(): Unit = {}

}