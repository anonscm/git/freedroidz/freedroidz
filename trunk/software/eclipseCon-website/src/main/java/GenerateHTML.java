import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

import org.apache.commons.io.IOUtils;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.RuntimeSingleton;
import org.apache.velocity.runtime.parser.ParseException;
import org.apache.velocity.runtime.parser.node.SimpleNode;

public class GenerateHTML {

	private static final String TAG = "HTML-Generator";

	public static void main(String[] args) throws Exception {
		GenerateHTML generateHTML = new GenerateHTML();
		Velocity.init();
		
		generateHTML.run(args[0]);
	}

	private void run(String pathname) throws Exception {
		FileOutputStream out = new FileOutputStream(pathname);
		
		
		String NXT0Sensors = generateChart("NXT0Sensors","NXT0 - Sensors", new String[]{"Sound (in dB)","Ultrasonic (in cm)", "High-Fives!"}, new String[]{"NXT0_PORT2_SOUND","NXT0_PORT3_SONIC", "NXT0_PORT1_TOUCH_COUNT"});
		String NXT0Motor = generateChart("NXT0Motor","NXT0 - Motors", new String[]{"TachoCount A (in degrees)","TachoCount B (in degrees)","TachoCount C (in degrees)"}, new String[]{"NXT0_MOTORA_TACHO","NXT0_MOTORB_TACHO","NXT0_MOTORC_TACHO"});
		
		
		String NXT1Sensors = generateChart("NXT1Sensors","NXT1 - Sensors", new String[]{"Sound (in dB)","Ultrasonic (in cm)", "High-Fives!", "Color"}, new String[]{"NXT1_PORT2_SOUND","NXT1_PORT3_SONIC", "NXT1_PORT1_TOUCH_COUNT", "NXT1_PORT4_COLOR"});
		String NXT1Motor = generateChart("NXT1Motor","NXT1 - Motors", new String[]{"TachoCount A (in degrees)","TachoCount B (in degrees)","TachoCount C (in degrees)"}, new String[]{"NXT1_MOTORA_TACHO","NXT1_MOTORB_TACHO","NXT1_MOTORC_TACHO"});
		
		
		String html = generateHTML(NXT0Sensors + NXT0Motor + NXT1Sensors + NXT1Motor);
	
		out.write(html.getBytes());
		out.flush();
		out.close();
		
	}
	

	private String generateHTML(String charts) throws Exception {
		InputStream wrapper = GenerateHTML.class.getResourceAsStream("wrapper.vm");

		Context context = new VelocityContext();
		
		context.put("fillInChartsHere", charts);
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		IOUtils.copy(wrapper, out);
		return runVelocity(context, out.toString());
	}

	private String generateChart(String id, String title, String[] captions, String[] sensorIDs) throws ParseErrorException, MethodInvocationException, ResourceNotFoundException, IOException {
		InputStream chartTemplate = GenerateHTML.class.getResourceAsStream("googleChartTemplate.vm");

		Context context = new VelocityContext();
		
		context.put("id", id);
		context.put("captions", captions);
		context.put("sensorIds", sensorIDs);
		context.put("title", title);
		
		context.put("dollar", "$");
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		IOUtils.copy(chartTemplate, out);
		return runVelocity(context, out.toString());
	}

	public String runVelocity(final Context baseContext, final String template) throws IOException, ParseErrorException,
			MethodInvocationException, ResourceNotFoundException {

		final Writer writer = new StringWriter();
	
		Velocity.evaluate(baseContext, writer, TAG, template);
		return writer.toString();
	}
}
